//
//  TvinciSDK.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 7/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "Tvinci.h"
#import "SmartLogs.h"

// @"http://widevine.prd.tvincidns.com/proxy.ashx?gid=147";
//  http://widevine.prd.tvincidns.com/
// lase used http://widevine.stg.tvincidns.com/proxy.ashx?gid=147
// NSString *const DRMServerURLString = @"http://widevine.prd.tvincidns.com/proxy.ashx?gid=147";
//NSString *const DRMServerURLStringForKanguroo = @"http://widevine.prd.tvincidns.com/proxy.ashx?gid=144";


// Old Proxy: @"http://widevine.dev.tvincidns.com/proxy.ashx?gid=134";
// NSString *const TVPortalID = @"tvincimediacorp";//@"tvincimediacorp"/@"tvinci" ;/ "tvinciottchile"
//NSString *const TVPortalIDForKanguroo = @"tvinciottchile";


@implementation TvinciSDK
SYNTHESIZE_SINGLETON_FOR_CLASS(TvinciSDK);



+(void) takeOff
{
//    ASLog(@"[TvinciSDK takeOff]");
   
    [[TVConfigurationManager sharedTVConfigurationManager] downloadConfigurationFile];
   // [[self sharedTvinciSDK] initializeWidevine];
}

//DRMServerURLString;
// @property (nonatomic, copy) NSString * TVPortalID;
+(void) takeOffWithWidevineDRMServerURL:(NSString*)DRMServerURL andTVPortalID:(NSString*)PortalID
{
   
    [[TVConfigurationManager sharedTVConfigurationManager] downloadConfigurationFile];
    [[self sharedTvinciSDK] initializeWidevineWithDRMServerURL:DRMServerURL andTVPortalID:PortalID];
}

#pragma mark - Session Monitoring
-(void) registerForSignInNotifications
{
    [self unregisterFromSignInNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    
    [center addObserver:self 
               selector:@selector(signInCompleted:) 
                   name:TVSessionManagerSignInCompletedNotification 
                 object:mngr];
}

-(void) unregisterFromSignInNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    [center removeObserver:self name:TVSessionManagerSignInCompletedNotification object:mngr];
}

-(void) registerForSignOutNotifications
{
    [self unregisterFromSignOutNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    
    [center addObserver:self 
               selector:@selector(signOutCompleted:) 
                   name:TVSessionManagerLogoutCompletedNotification
                 object:mngr];
}

-(void) unregisterFromSignOutNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVSessionManager *mngr = [TVSessionManager sharedTVSessionManager];
    [center removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:mngr];
}

-(void) signInCompleted : (NSNotification *) notification
{

}

-(void) signOutCompleted : (NSNotification *) notification
{
#if !TARGET_IPHONE_SIMULATOR
    // Reset the user data

#endif
}

#pragma mark - DRM Initializations



@end