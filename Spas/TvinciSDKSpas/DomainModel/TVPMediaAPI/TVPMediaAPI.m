//
//  TVPMediaAPI.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPMediaAPI.h"
#import "SynthesizeSingleton.h"
#import "TVPAPIRequest.h"
#import "SmartLogs.h"
#import "TVinciUtils.h"
#import "TVPictureSize.h"
#import "TVSessionManager.h"
#import "TVInitObject.h"
#import "SmartLogs.h"
#import "NSDate+Format.h"
#import "NSDate+BKAdditions.h"



@implementation TVPMediaAPI 


+(TVPAPIRequest *) requestForActionDone : (TVMediaAction) action 
                                mediaID : (NSInteger) mediaID 
                              mediaType : (NSString *) mediaType 
                             extraValue : (NSInteger) extraValue
                               delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameActionDone];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    
    NSString *actionString = TVNameForMediaAction(action);
    [request.postParameters setObjectOrNil:actionString forKey:@"action"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:extraValue] forKey:@"extraVal"];
    return request;
}

+(TVPAPIRequest *) requestForAddUserSocialAction : (TVSocialAction) action 
                                      onPlatform : (TVSocialPlatform) platform 
                                       withMedia :(NSInteger) mediaID 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate {
    NSURL *URL = [self URLForMethodName:MethodNameAddUserSocialAction];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForSocialAction(action) forKey:@"action"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(platform) forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:[NSString stringWithFormat:@"%d", mediaID] forKey:@"iMediaID"];
    return request;
}

+(TVPAPIRequest *) requestForChargeMediaWithPrepaid : (NSInteger)mediaFileID 
                                              price : (CGFloat) price
                                           currency : (NSString *) currency
                                      ppvModuleCode : (NSString *) ppvModuleCode
                                          cuponCode : (NSString *) copunCode
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate {
    NSURL *URL = [self URLForMethodName:MethodNameChargeMediaWithPrepaid];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    
    
    // Shefyg - added the needed post parameters
    [request.postParameters setObjectOrNil:[NSNumber numberWithFloat:price] forKey:@"price"];
    [request.postParameters setObjectOrNil:currency forKey:@"currency"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithFloat:mediaFileID] forKey:@"mediaFileID"];
    [request.postParameters setObjectOrNil:ppvModuleCode forKey:@"ppvModuleCode"];
    [request.postParameters setObjectOrNil:copunCode forKey:@"couponCode"];
    
   // ASLogInfo(@"This method is not fully implemented yet!");
    return request;
}

+(TVPAPIRequest *) requestForChargeUserForMediaFile : (NSInteger)mediaFileID
                                              price : (CGFloat) price
                                           currency : (NSString *) currency
                                      ppvModuleCode : (NSString *) ppvModuleCode
                                          cuponCode : (NSString *) copunCode
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate {
    NSURL *URL = [self URLForMethodName:MethodNameChargeUserForMediaFile];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    ASLogInfo(@"This method is not fully implemented yet!");
    return request;
}

+(TVPAPIRequest *) requestForChargeUserForMediaSubsciption : (NSString *)subscriptionID
                                                     price : (CGFloat) price
                                                  currency : (NSString *) currency
                                                 cuponCode : (NSString *) copunCode
                                           extraParameters : (NSString *) extraParameters
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate {
    NSURL *URL = [self URLForMethodName:MethodNameChargeUserForMediaSubscription];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    ASLogInfo(@"This method is not fully implemented yet!");
    return request;
    
}

+(TVPAPIRequest *) requestForGetAutoCompleteSearch : (NSString *) searchText
                                    fromMediaTypes : (NSArray *) mediaTypes
                                        startIndex : (NSInteger) startIndex
                                        recordsNum : (NSInteger ) recordsNum
                                          delegate : (id<ASIHTTPRequestDelegate>)delegate {
    if (searchText.length > 0)
    {
        if (mediaTypes != nil)
        {
            NSURL *URL = [self URLForMethodName:MethodNameGetAutoCompleteSearch];
            TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
            [request.postParameters setObjectOrNil:searchText forKey:@"prefixText"];
            [request.postParameters setObjectOrNil:mediaTypes forKey:@"iMediaTypes"];
            [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:startIndex] forKey:@"pageIdx"];
            [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:recordsNum] forKey:@"pageSize"];
            return request;
        }
        else
        {
            [NSException raise:NSInvalidArgumentException
                        format:@"Media types must be a valid array. Pass an empty array to specify \"All Types\""];
        }
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"searchText cannot be empty"];
    }
    return nil;
}

+(TVPAPIRequest *) requestForGetAutoCompleteSearchList : (NSString *) searchText
                                        fromMediaTypes : (NSArray *) mediaTypes
                                              delegate : (id<ASIHTTPRequestDelegate>)delegate  __attribute__((deprecated)){
    if (searchText.length > 0)
    {
        if (mediaTypes != nil)
        {
            NSURL *URL = [self URLForMethodName:MethodNameGetAutoCompleteSearchList];
            TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
            [request.postParameters setObjectOrNil:searchText forKey:@"prefixText"];
            [request.postParameters setObjectOrNil:mediaTypes forKey:@"iMediaTypes"];
            return request;
        }
        else 
        {
            [NSException raise:NSInvalidArgumentException 
                        format:@"Media types must be a valid array. Pass an empty array to specify \"All Types\""];
        }
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException 
                    format:@"searchText cannot be empty"];
    }
    return nil;
}

+(TVPAPIRequest *) requestForGetCategory : (NSString *) categoryID
                                delegate : (id<ASIHTTPRequestDelegate>) delegate 
{
    if (categoryID != nil)
    {
        NSURL *URL = [self URLForMethodName:MethodNameGetCategory];
        TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
        [request.postParameters setObjectOrNil:categoryID forKey:@"categoryID"];
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"Category ID must be specified"];
        return nil;
    }
}

+(TVPAPIRequest *) requestForGetChannelMediaList : (NSInteger) channelID 
                                     pictureSize : (TVPictureSize *) pictureSize 
                                        pageSize : (NSInteger) pageSize 
                                       pageIndex : (NSInteger) pageIndex 
                                         orderBy : (TVOrderBy) orderBy 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate 
{
    NSURL *URL = [self URLForMethodName:MethodNameGetChannelMediaList];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    
    [request.postParameters setObjectOrNil: [NSNumber numberWithInteger:channelID] forKey:@"ChannelID"];
    NSString *orderByString = TVNameForOrderBy(orderBy);
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:orderByString forKey:@"orderBy"];
    return request;
}

+(TVPAPIRequest *) requestForGetChannelMediaList : (NSInteger) channelID
                                     pictureSize : (TVPictureSize *) pictureSize
                                        pageSize : (NSInteger) pageSize
                                       pageIndex : (NSInteger) pageIndex
                                         orderBy : (TVOrderBy) orderBy
                                      mediaCount : (NSInteger) mediaCount
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate {
    NSURL *URL = [self URLForMethodName:MethodNameGetChannelMediaListWithMediaCount];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    ASLogInfo(@"This method is not fully implemented yet!");
    return request;
}

+(TVPAPIRequest *) requestForGetFullCategory : (NSString *) categoryID
{
    TVPictureSize * pictureSize = [TVPictureSize pictureSizeWithSize:CGSizeMake(192, 108)];
    
    if (categoryID != nil)
    {
        NSURL *URL = [self URLForMethodName:MethodNameGetFullCategory];
        TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
        [request.postParameters setObjectOrNil:categoryID forKey:@"categoryID"];
        [request.postParameters setObjectOrNil:[pictureSize name] forKey:@"picSize"];
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"Category ID must be specified"];
        return nil;
    }
}

+(TVPAPIRequest *) requestForGetUserTransactionHistory : (NSInteger) startFromIndex 
                                  numberOfItemsToFetch : (NSInteger) count 
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserTransactionHistory];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    NSNumber *startFrom = [NSNumber numberWithInteger:startFromIndex];
    NSNumber *howMany = [NSNumber numberWithInteger:count];
    [request.postParameters setObjectOrNil:startFrom forKey:@"start_index"];
    [request.postParameters setObjectOrNil:howMany forKey:@"pageSize"];
    return request;
}

+(TVPAPIRequest *) requestForGetUserPermittedSubscriptions
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserPermittedSubscriptions];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
    return request;
}

+(TVPAPIRequest *) requestForGetUserPermittedItems
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserPermittedItems];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
    return request;
}

+(TVPAPIRequest *) requestForGetDomainPermittedItems
{
    NSURL *URL = [self URLForMethodName:MethodNameGetDomainPermittedItems];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
    return request;
}


+(TVPAPIRequest *) requestForGetUserExpiredSubscriptionsWithBatchSize : (NSInteger) numberOfItems 
                                                             delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserExpiredSubscriptions];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    NSNumber *count = [NSNumber numberWithInteger:numberOfItems];
    [request.postParameters setObjectOrNil:count forKey:@"iTotalItems"];
    return request;
}

+(TVPAPIRequest *) requestForGetUserFavorites : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserFavorites];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestForGetAreMediasFavorites: (NSArray *)mediaIDsArray delegate: (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameAreMediasFavorite];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:mediaIDsArray forKey:@"mediaIds"];

    return request;
}

+(TVPAPIRequest *) requestForGetPrepaidBallanceWithCurrencyCode : (NSString *) currencyCode
                                                       delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    if (currencyCode != nil)
    {
        NSURL *URL = [self URLForMethodName:MethodNameGetPrepaidBalance];
        TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
        [request.postParameters setObjectOrNil:currencyCode forKey:@"currencyCode"];
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"Currency code must be specified"];
        return nil;
    }
}

+(TVPAPIRequest *) requestForGetRecommendedMediasWithPictureSize : (TVPictureSize *) pictureSize 
                                                        pageSize : (NSInteger) pageSize 
                                                       pageIndex : (NSInteger) pageIndex 
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetRecommendedMedias];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:[pictureSize name] forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    return request;
}

+(TVPAPIRequest *) requestForSearchMediaWithText : (NSString *) text
                                       mediaType : (NSString *) mediaType
                                     pictureSize : (TVPictureSize *) pictureSize
                                        pageSize : (NSInteger) pageSize
                                       pageIndex : (NSUInteger) pageIndex
                                         orderBy : (TVOrderBy) orderBy
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    if (text.length > 0)
    {
        TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMedia] delegate:delegate];
        [request.postParameters setObjectOrNil:text forKey:@"text"];
        [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
        [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:orderBy] forKey:@"orderBy"];
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"Search text cannot be empty"];
    }
    return nil;
}

/*
 "tagName": "",
 "value": "",
 "mediaType": 0,
 "picSize": "",
 "pageSize": 0,
 "pageIndex": 0,
 "orderBy": "None || Added || Views || Rating || ABC || Meta"
 */

+(TVPAPIRequest *) requestForSearchMediaByTag : (NSString *) tagName
                                  wantedValue : (NSString *) wantedValue
                                    mediaType : (NSString *) mediaType
                                  pictureSize : (TVPictureSize *) pictureSize
                                     pageSize : (NSInteger) pageSize
                                    pageIndex : (NSUInteger) pageIndex
                                      orderBy : (TVOrderBy) orderBy
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    if (tagName.length > 0 && wantedValue.length > 0)
    {
        TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByTag] delegate:delegate];
        [request.postParameters setObjectOrNil:tagName forKey:@"tagName"];
        [request.postParameters setObjectOrNil:wantedValue forKey:@"value"];
        [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
        [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
        [request.postParameters setObjectOrNil: TVNameForOrderBy(orderBy) forKey:@"orderBy"];
        return request;
    }
    else
    {
        [NSException raise:NSInvalidArgumentException format:@"Tag Value and Tag Name cannot be empty"];
        return nil;
    }
}

+(TVPAPIRequest *) requestForSearchMediaByMetaDataPairs : (NSDictionary *) metaDataPairs
                                             orTagPairs : (NSDictionary *) tagPairs
                                              mediaType : (NSString *) mediaType
                                            pictureSize : (TVPictureSize *) pictureSize
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                                orderBy : (TVOrderBy) orderBy
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByMetasTags] delegate:delegate];
    NSMutableArray *tagPairsArray = [NSMutableArray array];
    NSMutableArray *metaDataPairsArray = [NSMutableArray array];
    
    for (id key in tagPairs.allKeys)
    {
        id value = [tagPairs objectForKey:key];
        NSDictionary *singlePair = [NSDictionary dictionaryWithObjectsAndKeys:value,@"Value",key,@"Key",nil];
        [tagPairsArray addObject:singlePair];
    }
    
    for (id key in metaDataPairs.allKeys)
    {
        id value = [metaDataPairs objectForKey:key];
        NSDictionary *singlePair = [NSDictionary dictionaryWithObjectsAndKeys:value,@"Value",key,@"Key",nil];
        [metaDataPairsArray addObject:singlePair];
    }
    
    [request.postParameters setObjectOrNil:tagPairsArray forKey:@"tagPairs"];
    [request.postParameters setObjectOrNil:metaDataPairsArray forKey:@"metaPairs"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:orderBy] forKey:@"orderBy"];
    return request;
}

//MethodNameSearchMediaByMetasTagsExact
+(TVPAPIRequest *) requestForSearchMediaByMetasTagsExactWithPairs : (NSDictionary *) metaDataPairs
                                             orTagPairs : (NSDictionary *) tagPairs
                                              mediaType : (NSString *) mediaType
                                            pictureSize : (TVPictureSize *) pictureSize
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                                orderBy : (TVOrderBy) orderBy
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByMetasTagsExact] delegate:delegate];
    NSMutableArray *tagPairsArray = [NSMutableArray array];
    NSMutableArray *metaDataPairsArray = [NSMutableArray array];
    
    for (id key in tagPairs.allKeys)
    {
        id value = [tagPairs objectForKey:key];
        NSDictionary *singlePair = [NSDictionary dictionaryWithObjectsAndKeys:value,@"Value",key,@"Key",nil];
        [tagPairsArray addObject:singlePair];
    }
    
    for (id key in metaDataPairs.allKeys)
    {
        id value = [metaDataPairs objectForKey:key];
        NSDictionary *singlePair = [NSDictionary dictionaryWithObjectsAndKeys:value,@"Value",key,@"Key",nil];
        [metaDataPairsArray addObject:singlePair];
    }
    
    [request.postParameters setObjectOrNil:tagPairsArray forKey:@"tagPairs"];
    [request.postParameters setObjectOrNil:metaDataPairsArray forKey:@"metaPairs"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:TVNameForOrderBy(orderBy) forKey:@"orderBy"];
    
    return request;
}

+(TVPAPIRequest *) requestForGetRelatedMediasWithMediaID: (NSString *) mediaID
                                              mediaType : (NSString *) mediaTypeID
                                            pictureSize : (TVPictureSize *) pictureSize
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSUInteger) pageIndex
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetRelatedMedias] delegate:delegate];
    [request.postParameters setObjectOrNil:mediaID forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:mediaTypeID forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    return request;
}

+(TVPAPIRequest *) requestForIsItemPurchased : (NSInteger) fileID 
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameIsItemPurchased] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"iFileID"];
    NSString *userGuid = [TVSessionManager sharedTVSessionManager].sharedInitObject.siteGUID;
    [request.postParameters setObjectOrNil:userGuid forKey:@"sUserGuid"];
    return request;
}

+(TVPAPIRequest *) requestForGetPeopleWhoWatchedMedia : (NSInteger) mediaID 
                                          mediaTypeID : (NSString *) mediaTypeID 
                                          pictureSize : (TVPictureSize *) pictureSize 
                                             pageSize : (NSInteger) pageSize 
                                            pageIndex : (NSInteger) pageIndex 
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetPeopleWhoWatched] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:mediaTypeID forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    return request;
}

+(TVPAPIRequest *) requestForGetMediaLicenseLink : (NSInteger) fileID 
                                         baseURL : (NSURL *) baseURL 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaLicenseLink] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"mediaFileID"];
    NSString *baseLink = [baseURL absoluteString];
    [request.postParameters setObjectOrNil:baseLink forKey:@"baseLink"];
    return request;
}

+(TVPAPIRequest *) requestForGetLicensedLinksWithFileID:(NSInteger) fileID
                                                baseURL:(NSURL *) baseURL
                                               delegate:(id<ASIHTTPRequestDelegate>)delegate
{
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetLicensedLinks] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"mediaFileID"];
    NSString *baseLink = [baseURL absoluteString];
    [request.postParameters setObjectOrNil:baseLink forKey:@"baseLink"];
    return request;
}

+(TVPAPIRequest *) requestForGetMediaMarkForMediaID : (NSInteger) mediaID 
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaMark] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"iMediaID"];
    return request;
}

+(TVPAPIRequest *) requestForGetMediaInfoWithMediaID : (NSInteger) mediaID 
                                         pictureSize : (TVPictureSize *) pictureSize 
                                  includeDynamicInfo : (BOOL) includeDynamicInfo 
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaInfo] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"MediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:0] forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:NO] forKey:@"withDynamic"];
    return request;
}

+(TVPAPIRequest *) requestForGetMediaInfoWithMediaID : (NSInteger) mediaID
                                           mediaType : (NSInteger) mediaType
                                         pictureSize : (TVPictureSize *) pictureSize
                                  includeDynamicInfo : (BOOL) includeDynamicInfo
                                            delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaInfo] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"MediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaType] forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:NO] forKey:@"withDynamic"];
    return request;
}

+(TVPAPIRequest *) requestForGetMediasInfoWithMediaIDs : (NSArray *) mediaIDs 
                                           pictureSize : (TVPictureSize *) pictureSize 
                                    includeDynamicInfo : (BOOL) includeDynamicInfo 
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediasInfo] delegate:delegate];
    [request.postParameters setObjectOrNil:mediaIDs forKey:@"MediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:0] forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:NO] forKey:@"withDynamic"];
    return request;
}


+(TVPAPIRequest *) requestForGetItemsPricesWithCoupons : (NSArray *) fileIDsArray
                                          LanguageCode : (NSString *)langCode
                                            couponCode : (NSString *)couponCode
                                            onlyLowest : (BOOL)onlyLowest
                                           countryCode : (NSString *)countryCode
                                            deviceName : (NSString *)deviceName
                                              userGuid : (NSString *)userGuid
                                              delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetItemsPricesWithCoupons] delegate:delegate];
    [request.postParameters setObjectOrNil:fileIDsArray  forKey:@"nMediaFiles"];
    [request.postParameters setObjectOrNil:userGuid      forKey:@"sUserGUID"];
    [request.postParameters setObjectOrNil:couponCode    forKey:@"sCouponCode"];
    [request.postParameters setObjectOrNil:countryCode   forKey:@"sCountryCd2"];
    [request.postParameters setObjectOrNil:langCode      forKey:@"sLanguageCode3"];
    [request.postParameters setObjectOrNil:deviceName    forKey:@"sDeviceName"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:onlyLowest] forKey:@"bOnlyLowest"];

    return request;
}

+(TVPAPIRequest *) requestForGetItemPriceReason : (NSInteger) fileID
                                       delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetItemPriceReason] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"iFileID"];
    return request;
}

+(TVPAPIRequest *) requestForMediaHitWithMediaID : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID 
                               locationInSeconds : (NSTimeInterval) location 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameMediaHit] delegate:delegate];
    [request.postParameters setObjectOrNil:typeID forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"iMediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"iFileID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:(int)location] forKey:@"iLocation"];
    return request;
}

+(TVPAPIRequest *) requestForMediaMarkWithAction : (TVMediaPlayerAction) action
                                         onMedia : (NSInteger) mediaID
                                     mediaTypeID : (NSString *) typeID
                                          fileID : (NSInteger) fileID 
                               locationInSeconds : (NSTimeInterval) location 
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameMediaMark] delegate:delegate];
    NSString *actionName = TVNameForMediaPlayerAction(action);
    
    // TBD - delete this when not needed (used for debug issue QCSDK-107)
//    if([actionName isEqualToString:@"stop"] || [actionName isEqualToString:@"finish"]){
//        ASLog2Debug(@"action name = %@ ",actionName);////////
//    }
//    ASLog2Debug(@"action name = %@ ",actionName);////////
    
    
    [request.postParameters setObjectOrNil:actionName forKey:@"Action"];
    [request.postParameters setObjectOrNil:typeID forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"iMediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"iFileID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:(int)location] forKey:@"iLocation"];
    return request;
}

+(TVPAPIRequest *) requestForGetUserItems : (TVUserItemType) userItemType 
                                mediaType : (NSString *) mediaType
                              pictureSize : (TVPictureSize *) pictureSize
                                 pageSize : (NSInteger) pageSize
                                pageIndex : (NSUInteger) pageIndex
                                  delegate:  (id<ASIHTTPRequestDelegate>) delegate
{
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserItems] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserItemType(userItemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"start_index"];
    
    return request;
}

+(TVPAPIRequest *) requestForCheckGeoBlockForMedia : (NSInteger) mediaID 
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameCheckGeoBlockForMedia];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"iMediaID"];
    return request;
}

+(TVPAPIRequest*)requestForGetEPGMultiChannelProgram:(NSArray *) epgIDs
                                        pictureSize : (TVPictureSize *) pictureSize
                                               oUnit:(NSString *)oUnit
                                          fromOffset:(NSInteger)fromOffset
                                            toOffset:(NSInteger)toOffset
                                           utcOffset:(NSInteger)utcOffset
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetEPGMultiChannelProgram];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:epgIDs forKey:@"sEPGChannelID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"sPicSize"];
    [request.postParameters setObjectOrNil:oUnit forKey:@"oUnit"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fromOffset] forKey:@"iFromOffset"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:toOffset]  forKey:@"iToOffset"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:utcOffset]  forKey:@"iUTCOffSet"];
    
    return request;
    
}

/*
 sEPGChannelID: "",
 sPicSize: "",
 oUnit: "Days || Hours || Current",
 iFromOffset: 0,
 iToOffset: 0
 */
+(TVPAPIRequest*)requestForGetEPGChannelsPrograms:(NSString *) epgID
                                        pictureSize : (TVPictureSize *) pictureSize
                                        oUnit:(NSString *)oUnit
                                       fromOffset:(NSInteger)fromOffset
                                        toOffset:(NSInteger)toOffset
                                        utcOffset:(NSInteger)utcOffset
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetEPGChannelsPrograms];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:epgID forKey:@"sEPGChannelID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"sPicSize"];
    [request.postParameters setObjectOrNil:oUnit forKey:@"oUnit"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fromOffset] forKey:@"iFromOffset"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:toOffset]  forKey:@"iToOffset"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:utcOffset]  forKey:@"iUTCOffSet"];
    
    return request;

}
/* order by None or Added or Views or Rating or ABC or Meta*/


+(TVPAPIRequest*)requestForGetEPGChannelsWithPictureSize:(TVPictureSize *) pictureSize
                                                  orderBy:(TVOrderBy) orderBy
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    
    NSURL *URL = [self URLForMethodName:MethodNameGetEPGChannels];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    
    NSString *orderByString = TVNameForOrderBy(orderBy);
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"sPicSize"];
    [request.postParameters setObjectOrNil:orderByString forKey:@"orderBy"];
    
    return request;
}

+(TVPAPIRequest *) requestForRateMediaWithMediaID:(NSInteger)mediaID
                                mediaType:(NSString *)mediaType
                                extraValue:(NSInteger)extraValue
                                delegate:(id<ASIHTTPRequestDelegate>)delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameRateMedia];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
   // NSString *actionString = TVNameForMediaAction(action);
  //  [request.postParameters setObjectOrNil:actionString forKey:@"action"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:extraValue] forKey:@"extraVal"];
    return request;
}



+(TVPAPIRequest *) requestForGetUserPermittedPackagesWithDelegate:(id<ASIHTTPRequestDelegate>)delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetUserPermittedPackages];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    return request;
}

/*
 SearchMediaByMeta
 -----------------
 <metaName>string</metaName>
 <value>string</value>
 <mediaType>int</mediaType>
 <picSize>string</picSize>
 <pageSize>int</pageSize>
 <pageIndex>int</pageIndex>
 <orderBy>None or Added or Views or Rating or ABC or Meta</orderBy>
 */
+(TVPAPIRequest *) requestForSearchMediaByMeta: (NSString *)metaName
                                             value:(NSString *)value
                                         mediaType: (NSString *)mediaType
                                   pictureSize : (TVPictureSize *) pictureSize
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSUInteger) pageIndex
                                           orderBy : (TVOrderBy) orderBy
                                          delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSString *orderByString = TVNameForOrderBy(orderBy);
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByMeta] delegate:delegate];
    [request.postParameters setObjectOrNil:metaName forKey:@"metaName"];
    [request.postParameters setObjectOrNil:value forKey:@"value"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:orderByString forKey:@"orderBy"];
    
    return request;
}

/*
 <iBaseID>long</iBaseID>
 <picSize>string</picSize>
 <pageSize>int</pageSize>
 <pageIndex>int</pageIndex>
 */

+(TVPAPIRequest *) requestForGetMediasInPackage: (long)baseId
                                  pictureSize : (TVPictureSize *) pictureSize
                                     pageSize : (NSInteger) pageSize
                                    pageIndex : (NSUInteger) pageIndex
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediasInPackage] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:baseId] forKey:@"iBaseID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    
    return request;
}

+(TVPAPIRequest *) requestForCustomDownloadFileWithFileURL : (NSURL *) url
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate{
    
    TVPAPIRequest *request = [self requestWithURL:url delegate:delegate];
    return request;
    
}


+(TVPAPIRequest *) requestForGetItemPrice : (NSArray *) fileIDs
                                       delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetItemPrices] delegate:delegate];
    [request.postParameters setObjectOrNil:fileIDs forKey:@"fileIds"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:YES] forKey:@"bOnlyLowest"];

    return request;
}

+(TVPAPIRequest *) requestForCheckIsMediaFavorite : (NSInteger) mediaId
                                 delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameIsMediaFavorite] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaId] forKey:@"mediaID"];
    return request;
}

//+(TVPAPIRequest *) requestForGetChannelMultiFilterWithChannelID:(NSInteger) channelID
//                                                   pictureSize : (TVPictureSize *) pictureSize
//                                                      pageSize : (NSInteger) pageSize
//                                                     pageIndex : (NSUInteger) pageIndex
//                                                       orderBy : (TVOrderBy) orderBy
//                                                       metaName: (NSString *) metaName
//                                                       orderDir: (TVOrderDirection ) orderDir
//                                                         metas : (NSArray *) metaData
//                                                          tags : (NSArray *) tags
//                                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
//{
//    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetChannelMultiFilter] delegate:delegate];
//    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:channelID] forKey:@"ChannelID"];
//    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
//    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
//    [request.postParameters setObjectOrNil:TVNameForOrderBy(orderBy) forKey:@"orderBy"];
//    [request.postParameters setObjectOrNil:metaName forKey:@"metaName"];
//    [request.postParameters setObjectOrNil:TVNameForOrderDirection(orderDir) forKey:@"orderDir"];
//    [request.postParameters setObjectOrNil:metaData forKey:@"metas"];
//    [request.postParameters setObjectOrNil:tags forKey:@"tags"];
//    
//    return request;
//}

+(TVPAPIRequest *) requestForGetChannelMultiFilterWithChannelID:(NSInteger) channelID
                                                   pictureSize : (TVPictureSize *) pictureSize
                                                      pageSize : (NSInteger) pageSize
                                                     pageIndex : (NSUInteger) pageIndex
                                                       orderBy : (TVOrderBy) orderBy
                                                       orderDir: (TVOrderDirection ) orderDir
                                                          tags : (NSArray *) tags
                                                        catWith: (TVCutWith)catWith
                                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetChannelMultiFilter] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:channelID] forKey:@"ChannelID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:TVNameForOrderBy(orderBy) forKey:@"orderBy"];
    [request.postParameters setObjectOrNil:TVNameForOrderDirection(orderDir) forKey:@"orderDir"];
    [request.postParameters setObjectOrNil:tags forKey:@"tagsMetas"];
    [request.postParameters setObjectOrNil:TVNameForCutWith(catWith) forKey:@"cutWith"];

    
    return request;
}

+(TVPAPIRequest *) requestForGetOrderedChannelMultiFilterChannelID : (NSInteger) channelID
                                                       pictureSize : (TVPictureSize *) pictureSize
                                                          pageSize : (NSInteger) pageSize
                                                         pageIndex : (NSUInteger) pageIndex
                                                           orderBy : (TVMultiFilterOrderBy) orderBy
                                                          orderDir : (TVChannelMFOrderDirection) orderDir
                                                        orderValue : (NSString *)orderValue
                                                              tags : (NSArray *) tags
                                                           catWith : (TVCutWith)catWith
                                                          delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSMutableDictionary *orderObj = [NSMutableDictionary dictionary];
    [orderObj setObject:TVNameForMultiFilterOrderBy(orderBy) forKey:@"m_eOrderBy"];
    [orderObj setObject:TVNameForChannelMFOrderDirection(orderDir) forKey:@"m_eOrderDir"];
    [orderObj setObject:orderValue forKey:@"m_sOrderValue"];
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetOrderedChannelMultiFilter] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:channelID] forKey:@"ChannelID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:orderObj forKey:@"orderObj"];
    [request.postParameters setObjectOrNil:tags forKey:@"tagsMetas"];
    [request.postParameters setObjectOrNil:TVNameForCutWith(catWith) forKey:@"cutWith"];
    
    return request;
}

+(TVPAPIRequest*)requestForGetEPGChannelProgrammeByDates:(NSString *) channelID
                                            pictureSize : (TVPictureSize *) pictureSize
                                                fromDate:(NSString*)fromDate
                                                 toODate:(NSString*)toDate
                                               utcOffset:(NSInteger)utcOffset
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate {
    
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGChannelProgrammeByDates] delegate:delegate];
    [request.postParameters setObjectOrNil:channelID forKey:@"channelID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    
    [request.postParameters setObjectOrNil:fromDate forKey:@"fromDate"];
    [request.postParameters setObjectOrNil:toDate  forKey:@"toDate"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:utcOffset]  forKey:@"utcOffset"];
    
    return request;
    
}

+(TVPAPIRequest*)requestForGetEPGLicensedLinkForFileID:(NSInteger)  fileID
                                             EPGItemID:(NSInteger)  itemID
                                             startTime:(NSDate *)   startTime
                                             basicLink:(NSString*)  basicLink
                                                userIP:(NSString *) userIP
                                              refferer:(NSString *) refferer
                                            countryCd2:(NSString *) countryCd2
                                         languageCode3:(NSString *) languageCode3
                                            deviceName:(NSString *) deviceName
                                            formatType:(EPGLicensedLinkFormatType) format
                                              delegate:(id<ASIHTTPRequestDelegate>) delegate
{
    
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGLicensedLink] delegate:delegate];
    
    NSString *dateString = [startTime stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss.sss" andGMT:-2] ;
    
    [request.postParameters setObjectOrNil:dateString forKey:@"startTime"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"mediaFileID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:itemID] forKey:@"EPGItemID"];
    [request.postParameters setObjectOrNil:basicLink forKey:@"basicLink"];
    [request.postParameters setObjectOrNil:userIP forKey:@"userIP"];
    [request.postParameters setObjectOrNil:refferer forKey:@"refferer"];
    [request.postParameters setObjectOrNil:countryCd2 forKey:@"countryCd2"];
    [request.postParameters setObjectOrNil:languageCode3 forKey:@"languageCode3"];
    [request.postParameters setObjectOrNil:deviceName forKey:@"deviceName"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInt:format] forKey:@"formatType"];
    
    
    return request;
}

+(TVPAPIRequest *) requestForGetLastWatchedMediasByMediaID : (NSInteger) mediaID
                                                 mediaType : (NSString *) mediaType
                                               pictureSize : (TVPictureSize *) pictureSize
                                                  pageSize : (NSInteger) pageSize
                                                 pageIndex : (NSInteger) pageIndex
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetLastWatchedMedias] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:mediaType forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[pictureSize name] forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    
    return request;
}


+(TVPAPIRequest *) requestForSendToFriendWithMediaID: (NSInteger) mediaID
                                          senderName: (NSString *) senderName
                                         senderEmail: (NSString *) senderEmail
                                      recipientEmail: (NSString *) recipientEmail
                                             message:(NSString *) message
                                           delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSendToFriend] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:senderName forKey:@"senderName"];
    [request.postParameters setObjectOrNil:senderEmail forKey:@"senderEmail"];
    [request.postParameters setObjectOrNil:recipientEmail forKey:@"toEmail"];
    [request.postParameters setObjectOrNil:message forKey:@"msg"];
    
    return request;
}

+(TVPAPIRequest *) requestForAddCommentWithMediaID: (NSInteger) mediaID
                                         mediaType: (NSInteger) mediaType
                                            writer: (NSString *) writer
                                            header: (NSString *) header
                                         subheader: (NSString *) subheader
                                           content: (NSString *) content
                                        autoActive: (BOOL) autoActive
                                         delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddComment] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:autoActive] forKey:@"autoActive"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaType] forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:writer forKey:@"writer"];
    [request.postParameters setObjectOrNil:header forKey:@"header"];
    [request.postParameters setObjectOrNil:subheader forKey:@"subheader"];
    [request.postParameters setObjectOrNil:content forKey:@"content"];
    
    return request;
}


+(TVPAPIRequest *) requestForMediaLicenseData : (NSInteger) fileID
                                   andMediaID : (NSInteger) mediaID
                                     delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaLicenseData] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"iMediaFileID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"iMediaID"];
    return request;
}

+(TVPAPIRequest *) requestForGetSearchEPG : (NSString *) text
                                  picSize : (TVPictureSize*) pictureSize
                                  pageSize: (NSInteger ) pageSize
                                 pageIndex: (NSInteger ) pageIndex
                                   orderBy: (TVOrderBy ) orderBy
                                 delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchEPG] delegate:delegate];
    
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:text forKey:@"text"];
    [request.postParameters setObjectOrNil:text forKey:@"text"];
    [request.postParameters setObjectOrNil:text forKey:@"text"];
    [request.postParameters setObjectOrNil:TVNameForOrderBy(orderBy) forKey:@"orderBy"];
    return request;
    
}


+(TVPAPIRequest *) requestForGetMediaCommentsWithMediaID : (NSInteger ) mediaID
                                                pageSize : (NSInteger) pageSize
                                               pageIndex : (NSInteger ) pageIndex
                                                delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaComments] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    return request;
}

+(TVPAPIRequest*)requestForGetTranslations:(id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetTranslations] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestforGetRecommendationsByGalleryWithMediaID:(NSInteger ) mediaID
                                                        pictureSize:(TVPictureSize *) pictureSize
                                                      parentalLevel:(NSInteger) parentalLevel
                                          recommendationGalleryType:(TVRecommendationGalleryType) galleryType
                                                          delegate : (id<ASIHTTPRequestDelegate>) delegate

{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetRecommendationsByGallery] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:pictureSize.name forKey:@"picSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:parentalLevel] forKey:@"parentalLevel"];
    [request.postParameters setObjectOrNil:TVNameForRecommendationGalleryType(galleryType) forKey:@"galleryType"];
    
    return request;
}

+(TVPAPIRequest *) requestforAddEPGCommentWithEPGProgramID:(NSInteger ) epgProgramID
                                               contentText:(NSString *) contentText
                                                    header:(NSString *) headerStr
                                                 subHeader:(NSString *) subHeader
                                                    writer:(NSString *) writer
                                                autoActive:(BOOL) isAutoActive
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate {
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddEPGComment] delegate:delegate];

    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:epgProgramID] forKey:@"epgProgramID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isAutoActive] forKey:@"autoActive"];
    [request.postParameters setObjectOrNil:contentText forKey:@"contentText"];
    [request.postParameters setObjectOrNil:subHeader forKey:@"subHeader"];
    [request.postParameters setObjectOrNil:headerStr forKey:@"header"];
    [request.postParameters setObjectOrNil:writer forKey:@"writer"];

    return request;
}


+(TVPAPIRequest *) requestforGetEPGCommentsListEPGProgramID:(NSInteger ) epgProgramID
                                                  pageSize : (NSInteger) pageSize
                                                 pageIndex : (NSInteger ) pageIndex
                                                  delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGCommentList] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:epgProgramID] forKey:@"epgProgramID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];


    return request;
}

#warning Not Tested Yet
+(TVPAPIRequest *) requestforGetEPGCommentWithEPGProgramID:(NSInteger ) epgProgramID
                                                 pageSize : (NSInteger) pageSize
                                                pageIndex : (NSInteger) pageIndex
                                              commentType : (TVEPGCommentType) commentType
                                                 delegate : (id<ASIHTTPRequestDelegate>) delegate {
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGCommentList] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:epgProgramID] forKey:@"epgProgramID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:TVNameForEPGCommentBy(commentType) forKey:@"commentType"];

    
    return request;
}

+(TVPAPIRequest*)requestForGetDomainPermittedItems:(id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainPermittedItems] delegate:delegate];
    return request;
}

+(TVPAPIRequest*)requestForGetDomainPermittedSubscriptions:(id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainPermittedSubscriptions] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestforGetSubscriptionIDsContainingMediaID : (NSInteger) mediaId
                                                        fileID : (NSInteger) fileId
                                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetSubscriptionIDsContainingMediaFile] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaId] forKey:@"iMediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileId] forKey:@"iFileID"];

    return request;
}

+(TVPAPIRequest *) requestForSearchEPGProgramsWithText : (NSString *) text
                                        pageSize : (NSInteger) pageSize
                                       pageIndex : (NSUInteger) pageIndex
                                        delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    if (text.length > 0)
    {
        TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchEPGPrograms] delegate:delegate];
        [request.postParameters setObjectOrNil:text forKey:@"searchText"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
        return request;
    }
    else
    {
        [NSException raise:NSInvalidArgumentException format:@"Search text cannot be empty"];
    }
    return nil;
}

+(TVPAPIRequest *) requestForSearchMediaOrList : (NSArray *)orList
                                       AndList : (NSArray *)andList
                                     mediaType : (NSString *)mediaType
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSUInteger) pageIndex
                                       orderBy : (TVOrderByAndOrList) orderBy
                                      orderDir : (TVOrderDirectionByAndOrList)orderDir
                                         exact : (BOOL) exact
                                     orderMeta : (NSString*) orderMeta
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSString *orderByString = TVNameForOrderByAndOrList(orderBy);
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByAndOrList] delegate:delegate];
    if (orList != nil)
    {
        [request.postParameters setObject:orList forKey:@"orList"];
    }
    if (andList != nil)
    {
        [request.postParameters setObject:andList forKey:@"andList"];
    }
    [request.postParameters setObject:TVNameForOrderDirectionByAndOrList(orderDir) forKey:@"orderDir"];
    [request.postParameters setObject:orderMeta forKey:@"orderMeta"];
    [request.postParameters setObject:mediaType forKey:@"mediaType"];
    [request.postParameters setObject:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObject:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObject:[NSNumber numberWithBool:exact] forKey:@"exact"];
    [request.postParameters setObject:orderByString forKey:@"orderBy"];
    return request;
}

+(TVPAPIRequest *) requestForSearchEPGByAndOrList : (NSArray *)orList
                                       AndList : (NSArray *)andList
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSUInteger) pageIndex
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchEPGByAndOrList] delegate:delegate];
    if (orList != nil)
    {
        [request.postParameters setObject:orList forKey:@"orList"];
    }
    if (andList != nil)
    {
        [request.postParameters setObject:andList forKey:@"andList"];
    }
    [request.postParameters setObject:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObject:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    return request;
}

+(TVPAPIRequest *) requestforGetEPGAutoComplete:(NSString *) searchText
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSInteger ) pageIndex
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGAutoComplete] delegate:delegate];
    
    [request.postParameters setObjectOrNil:searchText forKey:@"searchText"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    
    
    return request;
}

+(TVPAPIRequest *) requestforGetAssetsStatsWithAssetsIDs:(NSArray *) assetsIDs
                                    assetType  : (TVAssetType) assetType
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSInteger ) pageIndex
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetAssetsStats] delegate:delegate];
    
    [request.postParameters setObjectOrNil:assetsIDs forKey:@"assetsIDs"];
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    
    
    return request;
}

+(TVPAPIRequest *) requestforGetAssetsStatsForTimePeriodWithAssetsIDs:(NSArray *) assetsIDs
                                             assetType  : (TVAssetType) assetType
                                               pageSize : (NSInteger) pageSize
                                              pageIndex : (NSInteger ) pageIndex
                                              startTime : (NSDate *)  startTime
                                                endTime : (NSDate *) endTime
                                               delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetAssetsStatsForTimePeriod] delegate:delegate];
    
    NSString *startString = [startTime stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss.sss"];
    NSString *endString = [endTime stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss.sss"];
    [request.postParameters setObjectOrNil:assetsIDs forKey:@"assetsIDs"];
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObjectOrNil:startString forKey:@"startTime"];
    [request.postParameters setObjectOrNil:endString forKey:@"endTime"];

    return request;
}

//NSString *TVNameForOrderBy(TVOrderBy orderBy)

+(TVPAPIRequest *) requestForSearchMediaByTypesWithText : (NSString *)text
                                       AndMediaTypes : (NSArray *)mediaTypes
                                        pictureSize : (TVPictureSize *) pictureSize
                                      pageSize : (NSInteger) pageSize
                                     pageIndex : (NSUInteger) pageIndex
                                       orderBy : (TVOrderBy) orderBy
                                      delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSString *orderByString = TVNameForOrderBy(orderBy);
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSearchMediaByTypes] delegate:delegate];
    [request.postParameters setObject:text forKey:@"text"];
    [request.postParameters setObject:mediaTypes forKey:@"mediaType"];
    [request.postParameters setObjectOrNil:[pictureSize name] forKey:@"picSize"];
    [request.postParameters setObject:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    [request.postParameters setObject:[NSNumber numberWithUnsignedInteger:pageIndex] forKey:@"pageIndex"];
    [request.postParameters setObject:orderByString forKey:@"orderBy"];
    return request;
}

@end

#pragma mark - Constants for the API method names

NSString * const MethodNameAddEPGComment = @"AddEPGComment";
NSString * const MethodNameGetEPGCommentList = @"GetEPGCommentsList";
NSString * const MethodNameGetRecommendationsByGallery = @"GetRecommendationsByGallery";
NSString * const MethodNameSearchEPG = @"SearchEPG";
NSString * const MethodNameGetMediaLicenseData = @"GetMediaLicenseData";
NSString * const MethodNameGetEPGChannelProgrammeByDates = @"GetEPGChannelProgrammeByDates";
NSString * const MethodNameGetChannelMultiFilter = @"GetChannelMultiFilter";
NSString * const MethodNameGetOrderedChannelMultiFilter = @"GetOrderedChannelMultiFilter";

NSString * const MethodNameRateMedia = @"RateMedia";
NSString * const MethodNameGetEPGMultiChannelProgram = @"GetEPGMultiChannelProgram";
NSString * const MethodNameGetEPGChannelsPrograms = @"GetEPGChannelsPrograms";
NSString * const MethodNameGetEPGChannels = @"GetEPGChannels";
NSString * const MethodNameActionDone = @"ActionDone";
NSString * const MethodNameAddUserSocialAction = @"AddUserSocialAction";
NSString * const MethodNameChargeMediaWithPrepaid = @"ChargeMediaWithPrepaid";
NSString * const MethodNameChargeUserForMediaFile = @"ChargeUserForMediaFile";
NSString * const MethodNameChargeUserForMediaSubscription = @"ChargeUserForMediaSubscription";
NSString * const MethodNameGetAutoCompleteSearchList = @"GetAutoCompleteSearchList";
NSString * const MethodNameGetAutoCompleteSearch = @"GetAutoCompleteSearch";
NSString * const MethodNameGetCategory = @"GetCategory";
NSString * const MethodNameGetChannelMediaList = @"GetChannelMediaList";
NSString * const MethodNameGetChannelMediaListWithMediaCount = @"GetChannelMediaListWithMediaCount";
NSString * const MethodNameGetFullCategory = @"GetFullCategory";
NSString * const MethodNameGetItemPriceReason = @"GetItemPriceReason";
NSString * const MethodNameGetItemPrices = @"GetItemPrices";
NSString * const MethodNameGetLastWatchedMedias = @"GetLastWatchedMedias";
NSString * const MethodNameGetLastWatchedMediaByPeriod = @"GetLastWatchedMediaByPeriod";
NSString * const MethodNameGetMediaComments = @"GetMediaComments";
NSString * const MethodNameGetMediaInfo = @"GetMediaInfo";
NSString * const MethodNameGetMediaLicenseLink = @"GetMediaLicenseLink";
NSString * const MethodNameGetLicensedLinks = @"GetLicensedLinks";
NSString * const MethodNameGetMediaMark = @"GetMediaMark";
NSString * const MethodNameGetMediasByMostAction = @"GetMediasByMostAction";
NSString * const MethodNameGetMediasByRating = @"GetMediasByRating";
NSString * const MethodNameGetMediasInPackage = @"GetMediasInPackage";
NSString * const MethodNameGetMediasInfo = @"GetMediasInfo";
NSString * const MethodNameGetNMostSearchedText = @"GetNMostSearchedText";
NSString * const MethodNameGetPeopleWhoWatched = @"GetPeopleWhoWatched";
NSString * const MethodNameGetPrepaidBalance = @"GetPrepaidBalance";
NSString * const MethodNameGetRecommendedMedias = @"GetRecommendedMedias";
NSString * const MethodNameGetRelatedMediaWithMediaCount = @"RelatedMediaWithMediaCount";
NSString * const MethodNameGetRelatedMedias = @"GetRelatedMedias";
NSString * const MethodNameGetSubscriptionMedia = @"GetSubscriptionMedia";
NSString * const MethodNameGetSubscriptionsContainingMediaFile = @"GetSubscriptionsContainingMediaFile";
NSString * const MethodNameGetSubscriptionsPrices = @"GetSubscriptionsPrices";
NSString * const MethodNameGetUserExpiredItems = @"GetUserExpiredItems";
NSString * const MethodNameGetUserExpiredSubscriptions = @"GetUserExpiredSubscriptions";
NSString * const MethodNameGetUserFavorites = @"GetUserFavorites";
NSString * const MethodNameAreMediasFavorite = @"AreMediasFavorite";
NSString * const MethodNameGetUserItems = @"GetUserItems";
NSString * const MethodNameGetUserPermittedSubscriptions = @"GetUserPermitedSubscriptions";
NSString * const MethodNameGetUserPermittedItems = @"GetUserPermittedItems";

NSString * const MethodNameGetUserSocialMedia = @"GetUserSocialMedia";
NSString * const MethodNameGetUserTransactionHistory = @"GetUserTransactionHistory";
NSString * const MethodNameGetVoteRatio = @"GetVoteRatio";
NSString * const MethodNameIsItemPurchased = @"IsItemPurchased";
NSString * const MethodNameGetItemsPricesWithCoupons = @"GetItemsPricesWithCoupons";
NSString * const MethodNameIsMediaFavorite = @"IsMediaFavorite";
NSString * const MethodNameIsUserSocialActionPerformed = @"IsUserSocialActionPerformed";
NSString * const MethodNameIsUserVoted = @"IsUserVoted";
NSString * const MethodNameMediaHit = @"MediaHit";
NSString * const MethodNameMediaMark = @"MediaMark";
NSString * const MethodNameSearchMedia = @"SearchMedia";
NSString * const MethodNameSearchMediaByMeta = @"SearchMediaByMeta";
NSString * const MethodNameSearchMediaByMultiMeta = @"SearchMediaByMultiMeta";
NSString * const MethodNameSearchMediaByMetasTags = @"SearchMediaByMetasTags";
NSString * const MethodNameSearchMediaByMetaWithMediaCount = @"SearchMediaByMetaWithMediaCount";
NSString * const MethodNameSearchMediaByTag = @"SearchMediaByTag";
NSString * const MethodNameSearchMediaByMultiTag = @"SearchMediaByMultiTag";
NSString * const MethodNameSearchMediaWithMediaCount = @"SearchMediaWithMediaCount";
NSString * const MethodNameCheckGeoBlockForMedia = @"CheckGeoBlockForMedia";
NSString * const MethodNameGetUserPermittedPackages = @"GetUserPermittedPackages";
NSString * const MethodNameSearchMediaByMetasTagsExact = @"SearchMediaByMetasTagsExact";
NSString * const MethodNameGetEPGLicensedLink = @"GetEPGLicensedLink";
NSString * const MethodNameSendToFriend = @"SendToFriend";
NSString * const MethodNameAddComment = @"AddComment";
NSString * const MethodNameGetTranslations = @"GetTranslations";

NSString * const MethodNameGetAssetsStats = @"GetAssetsStats";
NSString * const MethodNameGetAssetsStatsForTimePeriod = @"GetAssetsStatsForTimePeriod";

//SearchEPGPrograms
NSString * const MethodNameGetDomainPermittedItems = @"GetDomainPermittedItems";
NSString * const MethodNameGetDomainPermittedSubscriptions = @"GetDomainPermittedSubscriptions";
NSString * const MethodNameGetSubscriptionIDsContainingMediaFile = @"GetSubscriptionIDsContainingMediaFile";
NSString * const MethodNameSearchEPGPrograms = @"SearchEPGPrograms";
NSString * const MethodNameSearchMediaByAndOrList = @"SearchMediaByAndOrList";
NSString * const MethodNameSearchEPGByAndOrList = @"SearchEPGByAndOrList";

NSString * const MethodNameGetEPGAutoComplete = @"GetEPGAutoComplete";
NSString * const MethodNameSearchMediaByTypes = @"SearchMediaByTypes";

