
//
//  TVConfigurationManager.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/15/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVConfigurationManager.h"
#import "TVInitObject.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "SynthesizeSingleton.h"
#import "ASIHTTPRequest.h"
#import "JSONKit.h"
#import "SmartLogs.h"
#import "TVSessionManager.h"
#import "SmartLogs.h"
#import "TVConstants.h"
#import "TVOperator.h"
#import "TVUniqueIDPovider.h"
#import "AFNetworking.h"

// #import "TvinciUDID.h"
// #import "OpenUDID.h"

#define USING_OPEN_UDID YES

#define DMS_BASE_URL @"http://dms.tvinci.com/api.svc"
#define MAKE_CATEGORIES_LOADABLE(UNIQUE_NAME) @interface FORCELOAD_##UNIQUE_NAME @end @implementation FORCELOAD_##UNIQUE_NAME @end
MAKE_CATEGORIES_LOADABLE(NSString_JSONKitDeserializing);

#define TV_USERNAME @"dms"
#define TV_PASSWORD @"tvinci"
#define TV_APPNAME [[NSBundle mainBundle] bundleIdentifier]
#define TV_APPVERSION [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
//#define TV_UDID USING_OPEN_UDID ? [OpenUDID value] :[TvinciUDID value]
//#define TV_UDID [OpenUDID value]

#define Unregister              @"unregistered"
#define Status                  @"status"
#define Success                 @"success"
#define Already_Registered      @"alreadyregistered"

NSString *const TVConfigurationManagerNeedsForceUpdate = @"TVConfigurationManagerNeedsForceUpdate";
NSString *const TVConfigurationManagerReadyToLoadConsistantData = @"TVConfigurationManagerReadyToLoadConsistantData";
NSString *const TVConfigurationManagerDidLoadConfigurationNotification = @"TVConfigurationManagerDidLoadConfigurationNotification";
NSString *const TVConfigurationManagerDidFailToLoadConfigurationNotification = @"TVConfigurationManagerDidFailToLoadConfigurationNotification";
NSString *const TVConfigMainMenuIDKey = @"MainMenuID";
NSString *const TVConfigMainCategoryIDKey = @"MainCategoryID";
NSString *const TVConfigSmallPictureSizeKey = @"SmallPicSize";
NSString *const TVConfigMediumPictureSizeKey = @"MediumPicSize";
NSString *const TVConfigLargePictureSizeKey = @"LargePicSize";
NSString *const TVConfigHighDefinitionKey = @"HD";
NSString *const TVConfigStandatdDefinitionKey = @"SD";
NSString *const TVConfigLogoURLKey = @"LogoURL";
NSString *const DxPers_URLKey = @"DxPers_URL";
NSString *const LicenseServer_URLKey = @"LicenseServer";
NSString *const TVConfigWVPortalID = @"WVPortalID";
NSString *const TVConfigWVProxyURL = @"WVProxyURL";
NSString *const TVConfigGatewaysKey = @"Gateways";
NSString *const TVConfigGatewayURLKey = @"JsonGW";
NSString *const TVConfigTokenKey = @"token";
NSString *const TVConfigTokenStringKey = @"key";
NSString *const TVConfigTokenValidKey = @"valid";
NSString *const TVConfigAllowBrowseModeKey = @"AllowBrowseMode";
NSString *const ConfigurationFileName = @"config.js";
NSString *const TVConfigInitObjectKey = @"InitObj";
NSString *const TVConfigInitObjectPostKey = @"initObj";
NSString *const TVConfigFileFormatsKey = @"FilesFormat";
NSString *const TVConfigMainFileFormatKey = @"Main";
NSString *const TVConfigParams = @"params";
NSString *const TVConfigTrailerFileFormatKey = @"Trailer";//  Trailer
NSString *const TVConfigSubtitlesFileFormatKey = @"Subtitles"; 
NSString *const TVConfigWidevineFileFormatKey =  @"iPad Main";// old: @"iPhone Main";
NSString *const TVConfigWidevineFileFormatKeyForIPad =  @"iPad Main";
NSString *const TVConfigWidevineFileFormatKeyForIPhone =  @"iPhone Main";
NSString *const TVConfigFacebookURLKey = @"FacebookURL";
NSString *const TVConfigMediaTypesKey = @"MediaTypes";
NSString *const TVConfigPrepaidActiveKey = @"PREPAID_ACTIVE";
NSString *const TVMediaTypeMovie = @"Movie";
NSString *const TVMediaTypeEpisode = @"Episode";
NSString *const TVMediaTypeCast = @"Cast";
NSString *const TVMediaTypeSeries = @"Series";
NSString *const TVMediaTypePerson = @"Person";
NSString *const TVMediaTypePackage = @"Package";
NSString *const TVMediaTypePrepaid = @"Prepaid";
NSString *const TVMediaTypeLive = @"Live";
NSString *const TVMediaTypeLinear = @"Linear";
NSString *const TVMediaTypeSports = @"Sports";
NSString *const TVMediaTypeMusic = @"Music";
NSString *const TVMediaTypeKaraoke = @"Karaoke";
NSString *const TVMediaTypeTest = @"Test";
NSString *const TVMediaTypeAny = @"0";
NSString *const MediaTypesFileName = @"MediaTypes.json";
NSString *const TVFacebookLoginStatusKey = @"status";
NSString *const TVFacebookLoginStatusError = @"ERROR";
NSString *const TVFacebookLoginStatusOK = @"OK";
NSString *const TVFacebookLoginStatusMergeOK = @"MERGEOK";
NSString *const TVFacebookLoginStatusNewUser = @"NEWUSER";
NSString *const TVFacebookLoginStatusMerge = @"MERGE";
NSString *const TVFacebookLoginStatusMinFriends = @"MINFRIENDS";
NSString *const TVFacebookLoginStatusUserNotExist = @"NOTEXIST";

NSString *const TVFacebookLoginActionRegister = @"register";
NSString *const TVFacebookLoginActionLogout = @"logout";
NSString *const TVFacebookLoginActionGetData = @"getdata";
NSString *const TVOperators = @"Operators";

NSString *const TVconfigForcedUpdateURLKey = @"ForcedUpdateURL";
NSString *const TVconfigForcedUpdateURLiPhoneKey = @"iPhone";
NSString *const TVconfigForcedUpdateURLiPadKey = @"iPad";
NSString *const TVconfigVersionInformationKey = @"version";
NSString *const TVconfigVersionInformationIsforceupdateKey = @"isforceupdate";
NSString *const TVconfigMenuPlatfromKey = @"MenuPlatforms";





// Can be any valid URL
NSString *const TVFacebookLoginCallbackURL = @"http://localhost/fbCallback.aspx";

@interface TVConfigurationManager ()
{
    long long nextConfingLifeCycle;
}
@property (nonatomic, assign, readwrite) TVConfigurationState state;
@property (retain ,readwrite) NSDictionary *configuration;
@property (retain , readwrite) NSURL *logoURL;
//@property (retain , readwrite) NSURL *gatewayURL;
@property (retain, readwrite) NSURL *facebookLoginURL;
@property (assign, readwrite) BOOL allowBrowseMode;
@property (assign, readwrite) NSInteger mainMenuID;
@property (assign, readwrite) NSInteger mainCategoryID;
@property (retain, readwrite) NSDictionary *fileFormatNames;
@property (retain, readwrite) NSDictionary *mediaTypes;
@property (retain, readwrite) NSString *configToken;
@property (nonatomic, retain) AFHTTPRequestOperationManager *configurationRequest;
@property (nonatomic, retain) ASIHTTPRequest *registerDeviceRequest;
@property (retain, readwrite) NSURL *DxPersonlizationUrl;
@property (retain, readwrite) NSURL *LicenseServerUrl;
@property (retain, nonatomic) NSString *WVPortalID;
@property (retain, nonatomic) NSURL *WVProxyUrl;
@property (retain, readwrite) NSArray *OperatorsArr;
@property (retain,readwrite, nonatomic) NSString * internalVersion;


@end

@implementation TVConfigurationManager


/*!
 @abstract Tells the application what type of unique identifier to use
 */
static UDIDType udidType;


SYNTHESIZE_SINGLETON_FOR_CLASS(TVConfigurationManager);
@synthesize logoURL = _logoURL;
@synthesize gatewayURL = _gatewayURL;
@synthesize configuration = _configuration;
@synthesize defaultInitObject = _defaultInitObject;
@synthesize allowBrowseMode = _allowBrowseMode;
@synthesize mainMenuID = _mainMenuID;
@synthesize fileFormatNames = _fileFormatNames;
@synthesize facebookLoginURL = _facebookLoginURL;
@synthesize mediaTypes = _mediaTypes;
@synthesize state = _state;
@synthesize prepaidActive = _prepaidActive;
@synthesize configToken = _configToken;
@synthesize DxPersonlizationUrl = _DxPersonlizationUrl;
@synthesize LicenseServerUrl = _LicenseServerUrl;
@synthesize OperatorsArr = _OperatorsArr;

#pragma mark - Memory

-(void) dealloc
{
    [self.configurationRequest cancel];
    [self.registerDeviceRequest clearDelegatesAndCancel];
    self.registerDeviceRequest = nil;
    self.configurationRequest = nil;
    self.mediaTypes = nil;
    self.facebookLoginURL = nil;
    self.logoURL = nil;
    self.gatewayURL = nil;
    self.configuration = nil;
    self.defaultInitObject = nil;
    self.OperatorsArr = nil;
    self.LicenseServerUrl = nil;
    [super dealloc];
}

#pragma mark - Initialization
- (id)init
{
    if (self = [super init])
    {
        nextConfingLifeCycle = -1;
        self.state = TVConfigurationStateNotConfigured;
        udidType = OpenUDIDType;
        _internalVersion = [[NSString alloc] initWithFormat:@"2.9.4"];
    }
    return self;
}


- (void)parseOperators : (NSDictionary *)params {
    
    NSArray* tempArr = [params objectOrNilForKey:TVOperators];
    NSMutableArray* tempMutArr = [[[NSMutableArray alloc] initWithCapacity:tempArr.count] autorelease];
    
    for (NSDictionary* aDic in tempArr) {
        TVOperator* operator = [[TVOperator alloc] initWithDictionary:aDic];
        [tempMutArr addObject:operator];
    }
    
    _OperatorsArr = [[NSArray alloc] initWithArray:tempMutArr];
    tempArr = nil;
}


- (void)parseDXPersonalisation : (NSDictionary *)params {
    
    NSString *DxPersonalizationURLString = [params objectOrNilForKey:DxPers_URLKey];
    
    if (DxPersonalizationURLString)
    {
        self.DxPersonlizationUrl = [NSURL URLWithString:DxPersonalizationURLString];
    }
}

- (void)parseGateways : (NSDictionary *)params {
    NSArray *gateways = [params objectOrNilForKey:TVConfigGatewaysKey];
    
    for (NSDictionary *gateway in gateways)
    {
        if ([gateway objectOrNilForKey:TVConfigGatewayURLKey])
        {
            NSString* gatewayURLString = [gateway objectOrNilForKey:TVConfigGatewayURLKey];
            self.gatewayURL = (gatewayURLString != nil) ? [NSURL URLWithString:gatewayURLString] : nil;
            continue;
        }
    }
}

-(void) parseWVInfo:(NSDictionary*) params
{
    NSDictionary * wvParams = [self parsArrayOfPairsToOneDictionary:[params objectForKey:@"Widevine"]];
    self.WVProxyUrl = [NSURL URLWithString:[wvParams objectForKey:TVConfigWVProxyURL]];
    self.WVPortalID = [wvParams objectForKey:TVConfigWVPortalID];
}

- (void)parseLicenseServer : (NSDictionary *)params {
    NSString *LicenseServerUrlString = [params objectOrNilForKey:LicenseServer_URLKey];
    
    if (LicenseServerUrlString)
    {
        self.LicenseServerUrl = [NSURL URLWithString:LicenseServerUrlString];
    }
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    @synchronized(self)
    {
       // ASLogInfo(@"CMdictionary = %@",dictionary);
        self.configuration = dictionary;
        
        NSDictionary *params = [dictionary objectOrNilForKey:TVConfigParams];
        
        
        NSString *logoURLString = [params objectOrNilForKey:TVConfigLogoURLKey];
        
        self.logoURL = [NSURL URLWithString:logoURLString];
        
        [self parseDXPersonalisation:params];
        [self parseOperators:params];
        [self parseGateways:params];
        [self parseLicenseServer:params];
        
        NSDictionary *token = [dictionary objectOrNilForKey:TVConfigTokenKey];
            
        self.configToken = [token objectForKey:TVConfigTokenStringKey];
        nextConfingLifeCycle = [[token objectForKey:TVConfigTokenValidKey]longLongValue];
        
        NSArray *initObj;// = [params objectOrNilForKey:TVConfigInitObjectKey];
        NSMutableArray *mutableInitObj = [NSMutableArray arrayWithArray:[params objectOrNilForKey:TVConfigInitObjectKey]];
        NSString * uniqueKey = [TVConfigurationManager getTVUDID];
        [mutableInitObj addObject:[NSDictionary dictionaryWithObject:uniqueKey forKey:@"udid"]];
        
        
        if ([[TVLanguageManager sharedLanguageManager] currentLanguage] != LanguageType_None)
        {
            NSString * languageString = stringForLanguageType([[TVLanguageManager sharedLanguageManager] currentLanguage]);
            NSInteger i =0 ;
            for (NSDictionary *param in  mutableInitObj)
            {
                
                if ([param objectOrNilForKey:@"Locale"])
                {
                    NSMutableArray * localeElements = [NSMutableArray arrayWithArray:[param objectOrNilForKey:@"Locale"]];
                    int j = 0;
                    NSDictionary * localeLanguageDict;
                    for (NSDictionary * dict in localeElements)
                    {
                        
                        if ([dict objectForKey:@"LocaleLanguage"])
                        {
                            localeLanguageDict = [NSMutableDictionary dictionaryWithObject:languageString forKey:@"LocaleLanguage"];
                            break;
                        }
                        j++;
                    }
                    
                    localeElements[j] = localeLanguageDict;
                    mutableInitObj[i] = [NSDictionary dictionaryWithObject:localeElements forKey:@"Locale"];
                    
                    break;
                }
                i++;
                
            }
            
        }
        
        
        
        initObj = [NSArray arrayWithArray:mutableInitObj];
        
        if (self.defaultInitObject)
        {
            [self.defaultInitObject setAttributesFromArray:initObj];
        }
        else
        {
            self.defaultInitObject = [[[TVInitObject alloc] initWithArray:initObj] autorelease];
        }
        
        self.theInitObjArray = initObj;

        self.allowBrowseMode = [[params objectOrNilForKey:TVConfigAllowBrowseModeKey] boolValue];
        
        self.mainMenuID = [[params objectOrNilForKey:TVConfigMainMenuIDKey] integerValue];
        
        self.mainCategoryID =[[params objectOrNilForKey:TVConfigMainCategoryIDKey]integerValue];
        
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        
        NSArray *fileFormats = [params objectOrNilForKey:TVConfigFileFormatsKey];
        
        for (NSDictionary *format in fileFormats)
        {
            NSString *key = [[format allKeys]objectAtIndex:0];
            
            [dic setObject:[format objectForKey:key] forKey:key];
        }
        
        if  ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
        {
            [dic setObject:TVConfigWidevineFileFormatKeyForIPad forKey:TVConfigMainFileFormatKey];
            
        }else if ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPhone)
        {
            [dic setObject:TVConfigWidevineFileFormatKeyForIPhone forKey:TVConfigMainFileFormatKey];
        }
        
        self.fileFormatNames = dic;
        
        NSString *facebookURLString = [params objectOrNilForKey:TVConfigFacebookURLKey];
        self.facebookLoginURL = (facebookURLString.length > 0) ? [NSURL URLWithString:facebookURLString] : nil;
        
        NSArray *mediaTypes = [params objectOrNilForKey:TVConfigMediaTypesKey];
        [self parseMediaTypes:mediaTypes];
        
        NSNumber *prepaidActiveNum = [dictionary objectForKey:TVConfigPrepaidActiveKey];
        
        if(prepaidActiveNum){
            self.prepaidActive = [prepaidActiveNum boolValue];
        }
        else{
            self.prepaidActive = kPrepaidActiveDefaultValue;
        }
        
        [self parseWVInfo:params];
        
        self.dictionaryVersionInformation = [dictionary objectForKey:TVconfigVersionInformationKey];
        self.dictionaryForceDownloadLinks = [self parsArrayOfPairsToOneDictionary:[params objectOrNilForKey:TVconfigForcedUpdateURLKey]];
        
        self.menuPlatformID = [params objectForKey:TVconfigMenuPlatfromKey];
        
    }
}

#pragma mark - Configuration

-(id) objectForKey : (NSString *) key
{
    @synchronized(self)
    {
        return [self.configuration objectOrNilForKey:key];
    }
}

-(BOOL) parseConfigurationJSON : (NSString *) json error : (NSError **) error
{
    @synchronized(self)
    {
        if (json != nil)
        {
            NSDictionary *configuration = [json objectFromJSONStringWithParseOptions:JKParseOptionNone error:error];
            if (configuration != nil)
            {
                [self setAttributesFromDictionary:configuration];
                
                if (self.gatewayURL == nil)
                {
                    if (error != NULL)
                    {
                        (*error) = [NSError errorWithDomain:@"Bad configuration file. No Gateways" code:0 userInfo:nil];
                    }
                }
            }
        }
        else 
        {
            if (error != NULL)
            {
                (*error) = [NSError errorWithDomain:@"No JSON string" code:0 userInfo:nil];
            }
        }
        
        return error != NULL;
    }
}

-(BOOL) parseConfigurationJSON : (NSDictionary *) configuration
{
    @synchronized(self)
    {
        
        BOOL result = NO;
        
        if (configuration != nil)
        {
            
            [self setAttributesFromDictionary:configuration];
            
            if (self.gatewayURL != nil)
            {
                result = YES;
            }
        }

        
        return result;
    }
}

-(void) notifySuccess
{
    [[NSNotificationCenter defaultCenter] postNotificationName:TVConfigurationManagerReadyToLoadConsistantData
                                                        object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:TVConfigurationManagerDidLoadConfigurationNotification
                                                        object:self];
}

-(void) notifyFailure
{
    [[NSNotificationCenter defaultCenter] postNotificationName:TVConfigurationManagerDidFailToLoadConfigurationNotification 
                                                        object:self];
}

-(void) parseMediaTypes : (NSArray *) mediaTypes
{
    NSMutableDictionary *tempMediaTypes = [NSMutableDictionary dictionary];
    for (NSDictionary *pair in mediaTypes)
    {
        NSString *mediaTypeID = [[pair allKeys]lastObject];
        NSString *mediaTypeName = [[pair allValues]lastObject];
        if (mediaTypeID != nil && mediaTypeName != nil)
        {
            // In my dictionary, the ID is the value and the name is the key. (Because the ID is dynamic and changes between clients)
            [tempMediaTypes setObject:mediaTypeID forKey:mediaTypeName];
        }
    }
    self.mediaTypes = [NSDictionary dictionaryWithDictionary:tempMediaTypes];
}

#pragma mark - API

- (NSURL *)getConfigURL
{
     NSString * uniqueKey = [TVConfigurationManager getTVUDID];
    ASLogInfo(@"TV_UDID = %@",uniqueKey);
    NSString * requestString =[NSString stringWithFormat:@"%@/getconfig?username=%@&password=%@&appname=%@&cver=%@&udid=%@&platform=iOS",self.dmsBaseURL,self.tvUserName,self.tvAppPassword,TV_APPNAME,TV_APPVERSION,uniqueKey];
    return [NSURL URLWithString:requestString];
}

- (NSURL *)getRegisterURL
{
    NSString * uniqueKey = [TVConfigurationManager getTVUDID];
    ASLogInfo(@"TV_UDID = %@",uniqueKey);
    NSString *requestString =[NSString stringWithFormat:@"%@/register?username=%@&password=%@&appname=%@&cver=%@&udid=%@&platform=iOS",self.dmsBaseURL,self.tvUserName,self.tvAppPassword,TV_APPNAME,TV_APPVERSION,uniqueKey];
    return [NSURL URLWithString:requestString];
}

- (void)prepareConfigurationRequest:(NSURL*)configURL {
    
    self.configurationRequest = [[AFHTTPRequestOperationManager alloc] init];
    self.configurationRequest.responseSerializer = [AFJSONResponseSerializer serializer];
    self.configurationRequest.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    self.configurationRequest.securityPolicy.allowInvalidCertificates = YES;
    
}

-(void)registerDeviceWithCompletionBlock: (void (^)(bool succeeded, bool deviceRegistered)) completion {

    //  If the request is not proccessing, this is to prevent multiple calls of the registerDevice request.
    if (self.registerDeviceRequest.inProgress == NO) {
        
        [self prepareRegisterDeviceRequest];
        
        [self.registerDeviceRequest setFailedBlock:^{
            bool deviceRegistered = (self.state == TVConfigurationStateProperlyConfigured);
            completion(NO, deviceRegistered);
        }];
        
        [self.registerDeviceRequest setCompletionBlock:^{
            
            NSError *error = nil;
            NSString *json = [self.registerDeviceRequest responseString];
            NSDictionary *JSON = [json objectFromJSONStringWithParseOptions:JKParseOptionNone error:&error];
            BOOL success = NO;
            BOOL deviceRegistered = NO;
            self.state = TVConfigurationStateUnregistered;
            
            if (error == nil) { // Only if no error found
                success = YES;
                NSString* status = [JSON objectForKey:Status];
                deviceRegistered = ([status isEqualToString:Already_Registered]||[status isEqualToString:Success]);
                
                if (deviceRegistered) {
                    self.state = TVConfigurationStateProperlyConfigured;
                }
            }
            
            //  deviceRegistered = YES, only if (status = Already_Registered or Success)
            completion(success, deviceRegistered);
        }];
        
        [self.registerDeviceRequest startAsynchronous];
    }else{
        bool deviceRegistered = (self.state == TVConfigurationStateProperlyConfigured);
        completion(NO, deviceRegistered);
    }

}

-(void) downloadConfigurationFileWithCompletionBlock: (void (^)(bool succeeded, bool deviceRegistered)) completion {
    
    //  If the request is not proccessing, this is to prevent multiple calls of the getConfig request.
    if (!self.configurationRequest)
    {
        NSURL* configURL = [self getConfigURL];
        
        // ASLogInfo(@"DMS configURL = %@",configURL);
        
        if ([configURL scheme] != nil && [configURL host] != nil) {
            //  Only if the configURL is valid.

            //  Prepare request and fire it
            [self prepareConfigurationRequest:configURL];
            
            [self.configurationRequest POST:[configURL absoluteString] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                
                NSDictionary *json = (NSDictionary *)responseObject;
                NSString * registrationStatus = [json objectForKey:Status];
                
                // ASLogInfo(@"configuration = \n\n%@\n\n",[json description]);
                //  Check if device is unregistered
                if ([registrationStatus isEqualToString:Unregister])
                {
                    self.state = TVConfigurationStateUnregistered;
                    completion(YES, NO);
                }
                else
                {
                    self.state = TVConfigurationStateProperlyConfigured;
                    BOOL configurationParsed = [self parseConfigurationJSON:json];
                    completion(configurationParsed, YES);
                }
                
                self.configurationRequest = nil;

                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                
                bool deviceRegistered = (self.state == TVConfigurationStateProperlyConfigured);
                completion(NO, deviceRegistered);
                
                self.configurationRequest = nil;

                
            }];
            
            
        }else{
            //  Case of URL is not valid.
            bool deviceRegistered = (self.state == TVConfigurationStateProperlyConfigured);
            completion(NO, deviceRegistered);
        }
    }
}

- (void)prepareRegisterDeviceRequest {
    [self.registerDeviceRequest clearDelegatesAndCancel];
    
//    NSString *requestString =[NSString stringWithFormat:@"%@/register?username=%@&password=%@&appname=%@&cver=%@&udid=%@&platform=iOS",
//                              self.dmsBaseURL,TV_USERNAME,TV_PASSWORD,TV_APPNAME,TV_APPVERSION,[TVConfigurationManager getTVUDID]];
    NSURL *registerDeviceURL = [self getRegisterURL];
    
    self.registerDeviceRequest = [ASIHTTPRequest requestWithURL:registerDeviceURL];
    [self.registerDeviceRequest setRequestMethod:@"POST"];
}

-(void) downloadConfigurationFile {
    
    [self downloadConfigurationFileWithCompletionBlock:^(bool succeeded, bool deviceRegistered) {
        if (!succeeded) {
            //  Download config Failure
            [self notifyFailure];
            return ;
        }
        
        if (deviceRegistered) {
            //  Successful scenario
            [self notifySuccess];
            return;
        }
        
        //  Config file was downloadd but device not registered yet.
        [self registerDevice];
    }];
}

-(void)registerDevice {
    //  Register Device
    [self registerDeviceWithCompletionBlock:^(bool succeeded, bool deviceRegistered) {
        
        //  Only if device was successfully registered and request was succeeded
        if (!(succeeded && deviceRegistered)) {
            [self notifyFailure];
            return ;
        }
        
        [self downloadConfigurationFileWithCompletionBlock:^(bool succeeded, bool deviceRegistered) {
            
            if (succeeded && deviceRegistered) {
                //  Download config Failure
                [self notifySuccess];
                return ;
            }
            //  Download config Success
            [self notifyFailure];
        }];
        
    }];
}

#pragma mark - Config lifecycle

-(BOOL)checkConfigLifeCycle
{
    long long currentTime =[[NSDate date] timeIntervalSince1970];
    
    long long  refreshTime = nextConfingLifeCycle;
    
    if (refreshTime<=currentTime)
    {
        if (!self.configurationRequest && (refreshTime != -1))
            [self downloadConfigurationFile];
        return NO;
    }else
        return YES;
    
}

#pragma mark - Facebook Login reltated
-(NSString *) groupID
{
    NSString *groupID = nil;
    NSRange range =[self.defaultInitObject.APIUsername rangeOfString:@"tvpapi_"];
    if (range.location != NSNotFound)
    {
        groupID = [self.defaultInitObject.APIUsername substringFromIndex:(range.location + range.length)];
    }
    return groupID;
}

-(NSURL *) facebookURLWithAction : (NSString *) action siteGUID : (NSString *) siteGUID
{

    NSString *URLString=nil;//    = TVFacebookLoginBaseURL;
    
    // shefyg: Adding code to use URL from Configuration
    if(self.facebookLoginURL != nil){
        URLString = [self.facebookLoginURL absoluteString];
    }
    
    URLString = [URLString stringByAppendingFormat:@"?action=%@",action];
    URLString = [URLString stringByAppendingString:@"&platform=3"];
    URLString = [URLString stringByAppendingString:@"&domain=1"];
    URLString = [URLString stringByAppendingFormat:@"&groupId=%@",[self groupID]];
    if (siteGUID.length > 0)
    {
        URLString = [URLString stringByAppendingFormat:@"&siteGuid=%@",siteGUID];
    }
    
    if ([action isEqualToString:TVFacebookLoginActionGetData])
    {
        URLString = [URLString stringByAppendingFormat:@"&callbackURL=%@",TVFacebookLoginCallbackURL];
    }
    
    return [NSURL URLWithString:URLString];
}

+(BOOL) urlStringHasFacebookLoginBaseURL:(NSString *)urlStr
{
    return [urlStr hasPrefix:[[TVConfigurationManager sharedTVConfigurationManager].facebookLoginURL absoluteString]];
}

#pragma mark - unique UDID

+(NSString *) getTVUDID
{
    return [TVUniqueIDPovider getUniqueIDByType:udidType];
}

-(void)setUDIDType:(UDIDType)type
{
    udidType = type;
}


#pragma mark - getters & setters

-(NSString *) dmsBaseURL
{
    if (_dmsBaseURL == nil)
    {
        self.dmsBaseURL = DMS_BASE_URL;
    }
    return _dmsBaseURL;
}

-(NSString *) tvUserName
{
    if (_tvUserName == nil)
    {
        _tvUserName = TV_USERNAME;
    }
    return _tvUserName;
}

-(NSString *)tvAppPassword
{
    if (_tvAppPassword == nil)
    {
        _tvAppPassword = TV_PASSWORD;
    }
    return _tvAppPassword;
}

/**
 *	This function created for fixing the problem that DMS sending for example :
 *
 DMS SENDING  :
 
 
 {
 forgotPassword = "http://www.google.com";
 },
 {
 selfCare = "http://www.google.com";
 }
 
 INSTED OF:
 {
 forgotPassword = "http://www.google.com";
 selfCare = "http://www.google.com";;
 }
 
 *
 *	@return	FIXED DICTIONARY
 */



-(NSDictionary *) parsArrayOfPairsToOneDictionary:(NSArray *) array
{
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    for (NSDictionary * pair in array)
    {
         NSString * theKey = [pair.allKeys lastObject];
        [dictionary setObject:[pair objectForKey:theKey] forKey:theKey];
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

@end
