//
//  EpgDownloadOrganizer.h
//  TvinciSDK
//
//  Created by iosdev1 on 2/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PaginationArray;

@interface EpgDownloadOrganizer : NSObject
@property (nonatomic, retain) PaginationArray * currChannelMenager;
@property (nonatomic ,retain) NSMutableArray * currHaursArray;

@end
