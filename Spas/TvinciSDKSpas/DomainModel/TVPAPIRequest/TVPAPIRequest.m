//
//  TvpAPIRequest.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPAPIRequest.h"
#import "JSONKit.h"
#import "TVInitObject.h"
#import "TVSessionManager.h"
#import "TVConfigurationManager.h"
#import "BaseTVPClient.h"
#import "TVNetworkQueue.h"
#import "AFHTTPRequestOperationManager.h"


#define defaultTimeoutInterval 30




@interface TVPAPIRequest ()

@property (nonatomic , retain, readwrite) NSMutableDictionary *postParameters;
@property (nonatomic , retain, readwrite) id JSONResponse;



#pragma mark - AFNetwork Additions
@property (nonatomic, retain) AFHTTPRequestOperation * coreRequest;
@property (strong, nonatomic) NSMutableDictionary * dictionaryHeader;
@property (readwrite, nonatomic, strong) NSError *error;

@property (retain, nonatomic) AFHTTPRequestOperationManager* operationManager;



@end

@implementation TVPAPIRequest
@synthesize postParameters = _postParameters;
@synthesize JSONResponse = _JSONResponse;



#pragma mark - Memory
-(void) dealloc
{

    self.JSONResponse = nil;
    self.postParameters = nil;
    [super dealloc];
}

#pragma mark - Getters
-(NSMutableDictionary *) postParameters
{
    if (_postParameters == nil)
    {
        _postParameters = [[NSMutableDictionary alloc] init];
    }
    return _postParameters;
}

-(NSMutableDictionary *) dictionaryHeader
{
    if (_dictionaryHeader == nil)
    {
        _dictionaryHeader = [[NSMutableDictionary alloc] init];
    }
    return _dictionaryHeader;
}

-(NSError *)error
{
   return [self.coreRequest error];
}

-(NSString *)responseString
{
    return [self.coreRequest responseString];
}


#pragma mark - Init

+(TVPAPIRequest *) requestWithURL:(NSURL *) url
{
    TVPAPIRequest * request = [[TVPAPIRequest alloc] initWithURL:url];
    return [request autorelease];
    
}

-(id) initWithURL:(NSURL *)newURL
{
    if (self = [super init])
    {
        self.url = newURL;
        self.requestMethod = RequestMethod_POST;
        [self addRequestHeader:@"Content-type" value:@"application/json;charset=utf-8"];
        self.operationManager = [[AFHTTPRequestOperationManager alloc] init];
    }
    return self;
}

#pragma mark - other methods

-(void) cancel
{
    [self.coreRequest cancel];
    [self.operationManager.operationQueue cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addRequestHeader:(NSString *)header value:(NSString *)value
{
    [self.dictionaryHeader setObject:value forKey:header];
}



-(void) parseResponseToJSONFormat
{
    NSString *responseString = [self.coreRequest responseString];
    // Try to parse it as a valid JSON
    id parsed = [responseString objectFromJSONString];
    if (parsed == nil)
    {
        // Not a valid JSON, but not an empty string, so what is it?
        if (responseString.length > 0)
        {
            // Strip extra " sign in case of response that contains only JSON string like "hello"
            parsed = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            // Check for boolean value.
            if ([parsed isEqualToString:@"true"])
            {
                parsed = [NSNumber numberWithBool:YES];
            }
            else if ([parsed isEqualToString:@"false"])
            {
                parsed = [NSNumber numberWithBool:NO];
            }
            else
            {
                // Check for number
                
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                NSNumber *number = [formatter numberFromString:parsed];
                parsed = (number != nil) ? number : parsed;
                [formatter release];
            }
        }
    }
    self.JSONResponse = parsed;
}

-(void) bakeAndParseResponseToJSONFormat
{
    NSString *responseString = [self.coreRequest responseString];
    // Try to parse it as a valid JSON
    id parsed = [responseString objectFromJSONString];
    if (parsed == nil)
    {
         responseString = [responseString stringByReplacingOccurrencesOfString:@"''" withString:@"\""];
         id parsed = [responseString objectFromJSONString];
        if (parsed != nil)
        {
            self.JSONResponse = parsed;
            return;
        }
        // Not a valid JSON, but not an empty string, so what is it?
        if (responseString.length > 0)
        {
            // Strip extra " sign in case of response that contains only JSON string like "hello"
            parsed = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            // Check for boolean value.
            if ([parsed isEqualToString:@"true"])
            {
                parsed = [NSNumber numberWithBool:YES];
            }
            else if ([parsed isEqualToString:@"false"])
            {
                parsed = [NSNumber numberWithBool:NO];
            }
            else
            {
                // Check for number
                
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                NSNumber *number = [formatter numberFromString:parsed];
                parsed = (number != nil) ? number : parsed;
                [formatter release];
            }
        }
    }
    self.JSONResponse = parsed;
}


-(id) JSONResponse
{
    /*
    if (_JSONResponse == nil)
    {
        _JSONResponse = self.coreRequest.response;
        
    }
     */
    if (![_JSONResponse isKindOfClass:[NSArray class]] && ![_JSONResponse isKindOfClass:[NSDictionary class]]) {
        _JSONResponse = [NSArray array];
    }
    return _JSONResponse;
}

-(id) bakedJSONResponse
{
    if (_JSONResponse == nil)
    {
        [self bakeAndParseResponseToJSONFormat];
    }
    return _JSONResponse;
}

- (NSString *) JSONResponseJSONOrString {

    if (_JSONResponse && [_JSONResponse isKindOfClass:[NSString class]]) {
        return _JSONResponse;
    }
    return @"";
    
}

#pragma mark - Debug Utils
-(NSString *) description{
    /*
    NSString *body = [[self postParameters] JSONString] ;
    
    return [NSString stringWithFormat:@"\n\n---------------\nRequest url:\n %@\n\
            Request PostBody:\n %@\n \
            PostParams:\n %@\n\
            Response:\n%@\n\n---------------\n"
            ,self.url,body,self.postParameters,[self JSONResponse]];
    
    */
    return @"";
    
}

-(NSString *) debugPostBudyString
{
    NSString * body = [[NSString alloc] initWithData:[[self.coreRequest request] HTTPBody] encoding:NSUTF8StringEncoding];
    return [body autorelease];
}


#pragma mark - AFNetwork adaptetion


-(void) adjustOperationManager
{
    self.operationManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    self.operationManager.securityPolicy.allowInvalidCertificates = YES;
    
    for (NSString * key in self.dictionaryHeader.allKeys)
    {
        [self.operationManager.requestSerializer setValue:[self.dictionaryHeader objectForKey:key] forHTTPHeaderField:key];
    }
    
    self.operationManager.requestSerializer = [[AFJSONRequestSerializer alloc] init]; // very important to send the body as json
}

-(void) adjustInitObject
{
    if ([self.postParameters objectForKey:TVConfigInitObjectPostKey] == nil)
    {
        if (self.excludeInitObj == NO)
        {
            NSDictionary *initObjectDictionary = nil;
            if (self.customInitObject)
            {
                initObjectDictionary = [self.customInitObject JSONObject];
            }
            else
            {
                initObjectDictionary = [[TVSessionManager sharedTVSessionManager].sharedInitObject JSONObject];
            }
            
            [self.postParameters setObject:initObjectDictionary forKey:TVConfigInitObjectPostKey];
        }
    }
}



-(void) registerForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkRequestDidStart:) name:AFNetworkingOperationDidStartNotification object:self.coreRequest];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkRequestDidFinish:) name:AFNetworkingOperationDidFinishNotification object:self.coreRequest];

    
}

-(void) prepareRequest
{
    [self prepareRequestWithStartedBlock:self.startedBlock failedBlock:self.failedBlock completionBlock:self.completionBlock];
}

-(void) startAsynchronous
{
    [self prepareRequestWithStartedBlock:self.startedBlock failedBlock:self.failedBlock completionBlock:self.completionBlock];
    [self sendRequest];
    
}


-(void) prepareRequestWithStartedBlock:(TVBasicBlock) startedBlock failedBlock:(TVBasicBlock) failedBlock completionBlock:(TVBasicBlock) completionBlock
{
    //self.requestDelegate = networkQeueu;
    
    if (startedBlock)
    {
        startedBlock();
    }

    [self adjustOperationManager];
    [self adjustInitObject];

    
    NSDictionary *parameters = self.postParameters;
    
    void (^requestCompletion)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        self.JSONResponse = responseObject;
        
        if (completionBlock)
        {
            completionBlock();
        }
    };
    
    void (^requestFailed)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
        self.error = error;
        self.responseStatusCode = [operation.response statusCode];
        
        if (self.responseStatusCode == 200) {
            
            if (operation.responseData) {
                self.JSONResponse = [[NSString alloc] initWithData:operation.responseData encoding:NSASCIIStringEncoding];
            }
            if (completionBlock)
            {
                completionBlock();
            }
        } else {
            if (failedBlock)
            {
                failedBlock();
            }
        }

    };

    
    self.operationManager.requestSerializer.timeoutInterval = self.timeOutSeconds>0?self.timeOutSeconds:defaultTimeoutInterval;
   
    NSMutableURLRequest *request = [self.operationManager.requestSerializer requestWithMethod:self.requestMethod == RequestMethod_POST?@"POST":@"GET" URLString:[self.url absoluteString] parameters:parameters error:nil];
    self.coreRequest = [self.operationManager HTTPRequestOperationWithRequest:request success:requestCompletion failure:requestFailed];
    
}


-(void) networkRequestDidStart:(NSNotification *) notification
{
//    NSLog(@"request started:%@ request counter rivka = %ld",self.url,(long)self.counter);
//    self.counter++;
    if ([self.requestDelegate respondsToSelector:@selector(requestDidStart:)])
    {
        [self.requestDelegate requestDidStart:self];
    }
}

-(void) networkRequestDidFinish:(NSNotification *) notification
{
//    NSLog(@"request finished:%@ request counter rivka = %ld",self.url,(long)self.counter);
    if ([self.requestDelegate respondsToSelector:@selector(requestDidFinish:)])
    {
        [self.requestDelegate requestDidFinish:self];
    }
}

-(void) sendRequest
{
    [self prepareRequest];
    [self registerForNotification];
    [self.operationManager.operationQueue addOperation:self.coreRequest];
}

@end
