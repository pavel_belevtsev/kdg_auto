//
//  TVCompanionAPI.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/8/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVCompanionAPI.h"

const NSString * TVCompanionItemType_Live =  @"live";
const NSString * TVCompanionItemType_VOD =  @"vod";

const NSString * TVCompanionPlaybackType_StartOver =  @"start_over";
const NSString * TVCompanionPlaybackType_CatchUp =  @"catch_up";
const NSString * TVCompanionPlaybackType_PLTV =  @"pltv";

const NSString * TVCompanionSuffixURLConst =  @"currentmedia";

@implementation TVCompanionAPI


+(TVPAPIRequest *) requestForSendPlayToSTBIp : (NSString *) deviceIp
                               playback_type : (NSString *) playback_type
                                epgProgramId : (NSString *) EpgProgramID
                                    siteGuid : (NSString *) siteGuid
                                    itemType : (NSString *) itemType
                                     mediaId : (NSString *) mediaId
                                      fileId : (NSString *) fileId
                                        port : (NSString *) port
                                    domainId : (NSInteger) domainId
                                   mediaMark : (NSInteger) seconds
                                doRequestPin : (BOOL) requestPin
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/%@", deviceIp, port, TVCompanionSuffixURLConst]];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters removeAllObjects];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:[mediaId integerValue]] forKey:@"mediaid"];
    //    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:playbackTimeSeconds] forKey:@"play_time"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:seconds] forKey:@"mediamark"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainId] forKey:@"domainid"];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:requestPin] forKey:@"requestpin"];
    
    [request.postParameters setObjectOrNil:playback_type forKey:@"playback_type"];
    [request.postParameters setObjectOrNil:EpgProgramID forKey:@"programid"];
    [request.postParameters setObjectOrNil:itemType forKey:@"item_type"];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"userid"];
    [request.postParameters setObjectOrNil:fileId forKey:@"fileid"];
    
    return request;
}

+(TVPAPIRequest *) requestForSendPlayToSTBWithBaseUrl : (NSString *) baseUrlString
                                        playback_type : (NSString *) playback_type
                                         epgProgramId : (NSString *) EpgProgramID
                                             siteGuid : (NSString *) siteGuid
                                             itemType : (NSString *) itemType
                                              mediaId : (NSString *) mediaId
                                               fileId : (NSString *) fileId
                                             domainId : (NSInteger) domainId
                                            mediaMark : (NSInteger) seconds
                                         doRequestPin : (BOOL) requestPin
                                             delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", baseUrlString, TVCompanionSuffixURLConst]];
    
    TVPAPIRequest *request = [[TVPAPIRequest alloc] initWithURL:URL];
    request.excludeInitObj = YES;

    [request.postParameters removeAllObjects];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:[mediaId integerValue]] forKey:@"mediaid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:[siteGuid integerValue]] forKey:@"userid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:[fileId integerValue]] forKey:@"fileid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:seconds] forKey:@"mediamark"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainId] forKey:@"domainid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:[EpgProgramID integerValue]] forKey:@"programid"];
    
    NSString* requestPinFinalStr = @"false";
    if (requestPin) {
        requestPinFinalStr = @"true";
    }
    [request.postParameters setObjectOrNil:requestPinFinalStr forKey:@"requestpin"];
    
    [request.postParameters setObjectOrNil:playback_type forKey:@"playback_type"];

    if (itemType) {
        [request.postParameters setObjectOrNil:itemType forKey:@"item_type"];
    }

    
    return request;
}

+(TVPAPIRequest *) requestForSendPlayToNetgemSTBIp : (NSString *) deviceIp
                                playbackUrl:(NSString *) mediaUrl
                               playback_type : (NSString *) playback_type
                                epgProgramId : (NSString *) EpgProgramID
                                    siteGuid : (NSString *) siteGuid
                                    itemType : (NSString *) itemType
                                     mediaId : (NSString *) mediaId
                                      fileId : (NSString *) fileId
                                        port : (NSString *) port
                                    domainId : (NSInteger) domainId
                                   mediaMark : (NSInteger) seconds
                                doRequestPin : (BOOL) requestPin
                                    delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    
    NSURL * urlToServer = nil;
    
    if (seconds>0)
    {
        urlToServer = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Live/External/play?url=%@&position=%ld", deviceIp,mediaUrl,(long)seconds]];
    }
    else
    {
        urlToServer = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Live/External/play?url=%@", deviceIp,mediaUrl]];
    }

    
    TVPAPIRequest *request = [self requestWithURL:urlToServer delegate:delegate];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request setRequestMethod:RequestMethod_GET];
    request.excludeInitObj = YES;
    
    return request;
}

@end
