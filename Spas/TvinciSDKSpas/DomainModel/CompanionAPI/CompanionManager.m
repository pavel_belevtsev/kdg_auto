 //
//  CompanionManager.m
//  TvinciSDK
//
//  Created by Tarek Issa on 3/17/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "CompanionManager.h"
#import "GCDAsyncUdpSocket.h"
#import "Reachability.h"
#import <sys/utsname.h>



//  General SSD values
static NSUInteger const kDefaultSSDPPort        = 1900;
static NSString *const kDefaultSSDPHost         = @"239.255.255.250";

//  Default SSD values
static NSString *const kDefaultSSDPFullHost     = @"239.255.255.250:1900";
static NSString *const kDefaultSSDPMethod       = @"M-SEARCH";
static NSString *const kDefaultSSDPMan          = @"\"ssdp:discover\"";
static NSString *const kDefaultSSDPSearchTarget = @"upnp:rootdevice";
static NSUInteger const kDefaultSSDPMaxWaitTime = 4;


@interface CompanionManager () <GCDAsyncUdpSocketDelegate> {
    
}

@property (nonatomic, retain) GCDAsyncUdpSocket *SSDPSocket;
@property (nonatomic, retain) NSMutableArray *devicesArray;

@end


@implementation CompanionManager
@synthesize delegate;


+ (CompanionManager *)sharedManager {
    
    static id instance = nil;
	@synchronized (self)
    {
		if (instance == nil)
		{
			instance = [[self alloc] init];
		}
	}
	return instance;
}


-(void) dealloc
{
    [self.SSDPSocket pauseReceiving];
    [self.SSDPSocket close];
    self.SSDPSocket = nil;
    self.devicesArray = nil;
    [super dealloc];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.SSDPSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self
                                                    delegateQueue:dispatch_get_main_queue()];
    
    NSError *error = nil;
    if ([self.SSDPSocket enableBroadcast:TRUE error:&error]) {
        ASLog2Debug(@"Broadcast enabled!");
    } else {
        ASLog2Debug(@"Error enabling broadcast: %@", [error localizedDescription]);
    }
    
    if ([self.SSDPSocket bindToPort:0 error:&error]) {
        ASLog2Debug(@"Binded to port!");
    } else {
        ASLog2Debug(@"Error binding: %@", [error localizedDescription]);
    }
    
    if ([self.SSDPSocket joinMulticastGroup:kDefaultSSDPHost error:&error]) {
        ASLog2Debug(@"Multicast group joined!");
    } else {
        ASLog2Debug(@"Error joining multicast group: %@", [error localizedDescription]);
    }
}

+ (NSString *) calculateUserAgent {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *result = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];

    return [NSString stringWithFormat:@"iOS/%@ UPnP/1.1 %@", [UIDevice currentDevice].systemVersion, result];
}

- (void)searchForDevices {

    Reachability *wifiReach = [[Reachability reachabilityForLocalWiFi] retain];
    [wifiReach startNotifier];
    
    if (![wifiReach isReachableViaWiFi]) {
        [self notifyFailtureWithError:[NSError errorWithDomain:@"Not Connected to WiFi" code:0 userInfo:nil]];
        [wifiReach stopNotifier];
        return;
    }
    [wifiReach stopNotifier];

    NSString *requestString = [CompanionManager makeSSDPRequestWithMethod:kDefaultSSDPMethod
                                                                     host:kDefaultSSDPFullHost
                                                                      man:kDefaultSSDPMan
                                                          maximumWaitTime:kDefaultSSDPMaxWaitTime
                                                             searchTarget:kDefaultSSDPSearchTarget
                                                                userAgent:[CompanionManager calculateUserAgent]];
    ASLog2Debug(@"Sending request: %@", requestString);
    NSData *requestData = [requestString dataUsingEncoding:NSUTF8StringEncoding];
    self.devicesArray = nil;
    self.devicesArray   = [[[NSMutableArray alloc] init] autorelease];
    [self.SSDPSocket sendData:requestData toHost:kDefaultSSDPHost port:kDefaultSSDPPort withTimeout:-1 tag:1];
    
    //  Set the completion behaviour.
    if ([self.SSDPSocket beginReceiving:NULL]) {
        SEL selector = @selector(completeSearch);
        ASLog2Debug(@"Waiting for responses...");
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:selector object:nil];
        [self performSelector:selector withObject:nil afterDelay:kDefaultSSDPMaxWaitTime];
    }
}

-(void) completeSearch
{
    [self.SSDPSocket pauseReceiving];
    
    NSMutableArray* resultArr = [NSMutableArray arrayWithCapacity:self.devicesArray.count];
    for (NSString* string in self.devicesArray) {
        //  Creat a new TVCompanionDevice from the string data received.
        TVCompanionDevice* device = [[TVCompanionDevice alloc] initWithString:string];
        [resultArr addObject:device];
    }
    [self notifyFinishWithDevicesArray:resultArr];
}

- (void)downloadXMLDataForDevices:(NSArray*)devices WithCompletionBlock : (void (^)(NSArray* downloadedDevices)) completion {
    __block int counter = (int)devices.count;
    NSMutableArray* mutArr = [NSMutableArray array];

    //  In case of no devices were found, so just complete with an empty array.
    if (devices.count == 0) {
        completion([NSArray array]);
        return;
    }
    
    for (TVCompanionDevice *device in devices) {
        
        //  Download XML file of the devices, if founded
        [device downloadXmlDataWithCompletionBlock:^(NSError *error) {
            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
            counter--;
            
            if (!error) {
                [mutArr addObject:device];
                //  Do what needs to do.
                //completion(error);
            }
            
            //  If downloaded all devioces XML data, so finish
            
            [pool drain];
            if (counter == 0) {
                self.devicesItems = [NSArray arrayWithArray:mutArr];
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(mutArr);
                });
            }
        }];
    }
}

#pragma mark - GCDAsyncUdpSocketDelegate

/**
 * Called when the datagram with the given tag has been sent.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    ASLog2Debug(@"Data sent successfully!");
}

/**
 * Called if an error occurs while trying to send a datagram.
 * This could be due to a timeout, or something more serious such as the data being too large to fit in a sigle packet.
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag
       dueToError:(NSError *)error
{
    ASLog2Debug(@"Data did not sent! Error: %@", [error localizedDescription]);
    if (error) {
        [self notifyFailtureWithError:error];
    }
}

/**
 * Called when the socket has received the requested datagram.s
 **/
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSString *response = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    [self.devicesArray addObject:response];
    NSLog(@"Did receive response: %@", response);
}

/**
 * Called when the socket is closed.
 **/
- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    ASLog2Debug(@"Socket closed");
    if (error) {
        [self notifyFailtureWithError:error];
    }
}

- (void)notifyFailtureWithError:(NSError *)error {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(companionSearchFinishedWithError:)]) {
            [self.delegate companionSearchFinishedWithError:error];
        }
    }
}

- (void)notifyFinishWithDevicesArray:(NSArray *)resultArr {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(companionSearchFinishedWithDevices:)]) {
            [self.delegate companionSearchFinishedWithDevices:resultArr];
        }
    }
}


#pragma mark - Static Methods
+(NSString *) makeSSDPRequestWithMethod:(NSString *)method
                                   host:(NSString *)host
                                    man:(NSString *)man
                        maximumWaitTime:(NSUInteger)maxWaitTimeInSeconds
                           searchTarget:(NSString *)st
                              userAgent:(NSString *)userAgent
{
    // Limit MX to [1-5] as per UPnP specifications
    NSUInteger mx = MAX(MIN(5, maxWaitTimeInSeconds), 1);
    NSString *requestString = [NSString stringWithFormat:
                               @"\%@ * HTTP/1.1\r\nHOST: %@\r\nMAN: %@\r\nMX: %lu\r\nST: %@\r\nUSER-AGENT: %@\r\n\r\n"
                               , method, host, man, (unsigned long)mx, st, userAgent];
    return requestString;
}

+(NSString *) makeSSDPSearchRequestWithMan:(NSString *) man
                           maximumWaitTime:(NSUInteger) maxWaitTimeInSeconds
                              searchTarget:(NSString *) st
{
    return [self makeSSDPRequestWithMethod:kDefaultSSDPMethod
                                      host:kDefaultSSDPFullHost
                                       man:man
                           maximumWaitTime:maxWaitTimeInSeconds
                              searchTarget:st
                                 userAgent:[CompanionManager calculateUserAgent]];
}

#pragma mark - Pairing and Unpairing
- (BOOL)pairDevice:(TVCompanionDevice *)device {
    if (device) {
        if ([device isKindOfClass:[TVCompanionDevice class]]) {
            self.devicePaired = nil;
            self.devicePaired = device;
            [self postNotification:TVNotificationCompanionDevicePaired userInfo:nil];
            return YES;
        }
    }

    return NO;
}

- (BOOL)unPairCurrentDevice {
    self.devicePaired = nil;
    [self postNotification:TVNotificationCompanionDeviceUnPaired userInfo:nil];
    return YES;
}


-(BOOL)isDevicePaired {
    if (self.devicePaired) {
        return YES;
    }
    return NO;
}

@end
