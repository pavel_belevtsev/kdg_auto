//
//  EpgManager.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/10/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "EpgManager.h"
#import "SynthesizeSingleton.h"
#import "TVinci.h"
#import "TVinciUI.h"
#import "TVConstants.h"
#import "ASIHTTPRequest.h"
#import "TVSessionManager.h"
#import "TVNetworkQueue.h"
#import "SmartLogs.h"

NSString *const TVDateFormat = @"yyyy-MM-dd";
NSString *const MainMenuFileName1 = @"MainMenu.js";

#define INIT_SELEDCTED_CHANNEL_GROP_Id -1

#define NUMBERS_OF_DASY 6
// for Anga
//#define EPD_ID 535
// For Toggle
 #define EPD_ID 463


#define IPHONE_EPG_ID 461

#define TODAY 0
#define EPG_ID_NAME @"epg_id"

#define DAY_NUMBER @"day_number_"

#define SAVES_GALLERY @"Gallery_complate_downlaod"

#define SAVES_CHANNELS @"channels_complate_downlaod"

#define HAVE_CHASH @"have_Gallery_cash"

#define MAP_SITE @"allMapSite"

//self.realChannels = [[TVCache sharedTVCache] objectForKey:SAVES_CHANNELS];

@implementation EpgManager

#pragma mark - Singlton
SYNTHESIZE_SINGLETON_FOR_CLASS(EpgManager);
@synthesize realChannels;
@synthesize allPrograms;
@synthesize todayPrograms;
@synthesize galleryItems;
@synthesize progressHUD=_progressHUD;
@synthesize networkQueue = _networkQueue;
@synthesize realPrograms;
@synthesize currentlyLoudingProgrames;
@synthesize selectedDayDate;
@synthesize dayNumber;
@synthesize oneDayPrograms;
@synthesize  epgId;
@synthesize selectedChannelGropId;
@synthesize currentlyPresentedChannel;
@synthesize isAllChannels;
@synthesize oneChannelProgram;
@synthesize loadOnlyChannels;
@synthesize programesPerDayForChanelID = _programesPerDayForChanelID;
@synthesize showReadyPrograme;
@synthesize numberOfChannelsWithOutProgrames;
@synthesize isAllProgrames;
@synthesize pendingLoadingDueToAppResign = _pendingLoadingDueToAppResign;

#pragma mark - memory

-(void)dealloc
{
    self.progressHUD=nil;
    [self unregisterForAppStateNotifications];
    [self unregisterForSwipeNotifications];
    [_programesPerDayForChanelID release];
    [currentlyPresentedChannel release];
    [oneDayPrograms release];
    [selectedDayDate release];
    [realChannels release];
    [allPrograms release];
    [todayPrograms release];
    [galleryItems release];
    [_networkQueue release];
    [realPrograms release];
    [currentlyLoudingProgrames release];
    
    [super dealloc];
}

#pragma mark - init

- (id)init
{
    self = [super init];
    if (self) {
        _pendingLoadingDueToAppResign = NO;
        [self registerForAppStateNotifications];
        if ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
        {
            [self registerForSwipeNotifications];
        }
    }
    return self;
}

#pragma mark - Notifications handle

-(void) registerForAppStateNotifications
{
    [self unregisterForAppStateNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppResignedActive:) name:TVNotificationName_AppWillResigneActive object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppDidBecomeActive:) name:TVNotificationName_AppDidBecomeActive object:nil];
}

-(void) unregisterForAppStateNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:TVNotificationName_AppWillResigneActive object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self  name:TVNotificationName_AppDidBecomeActive object:nil];
}

-(void) handleAppResignedActive:(NSNotification *) notification
{
    
    ASLog2Debug(@"");////////
    
    // 20. check state of loading
    //      40. if in middle:
    
    //               40.3 stop all requests
    //               40.6 mark loading pending
    
    //      50. if havent start loading or after loading
    //              50.4 mark not pending loading
    
    if(self.progressHUDShow)
    {
        _pendingLoadingDueToAppResign = YES;
        self.isAllProgrames = NO;
        self.isApp=YES;
        self.progressHUDShow =NO;
        self.isStartLoading=NO;
        [self.networkQueue cleen];
    }
    else{
        _pendingLoadingDueToAppResign = NO;
    }
    
    
}

-(void)cleanALLRequest
{
    self.isApp=YES;
    // self.isAllProgrames = NO;
    [EpgManager sharedEpgManager].isStartLoading=NO;
    [self.progressHUD dismiss];
    [SVProgressHUD dismiss];
    [self.networkQueue cleen];
    self.isStartLoading=NO;
     self.progressHUDShow =NO;
}

-(void) handleAppDidBecomeActive:(NSNotification *) notification{
    ASLog2Debug(@"");////////
    
    if(_pendingLoadingDueToAppResign)
    {
        [self cleanAllPrametars];
        self.isApp=NO;
        [self.progressHUD showWithStatus:NSLocalizedString(@"Loading...", nil) maskType:SVProgressHUDMaskTypeNone];
        if (self.isMyZoonOpen == YES)
        {
            [self.progressHUD dismiss];
        }
        self.progressHUDShow=YES;
    }
    
}

-(void)cleanAllPrametars
{
    //self.showReadyPrograme = YES;
    //self.isAllChannels = NO;
    // self.isAllProgrames = NO;
   // self.loadOnlyChannels = NO;
    self.progressHUDShow =NO;
    _pendingLoadingDueToAppResign = NO;
    [self.programesPerDayForChanelID removeAllObjects];
    self.numberOfChannelsWithOutProgrames=0;
}

#pragma mark - Getters

-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(SVProgressHUD *) progressHUD
{
    if (_progressHUD == nil)
    {
        _progressHUD = [[SVProgressHUD alloc] init];
    }
    return _progressHUD;
}


#pragma mark - Asynchronic Dangerous Stuff

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

#pragma mark - start load data

//-----------------------------------------------------------------------------------------------------------------
//  start load data
//-----------------------------------------------------------------------------------------------------------------
-(void)loadData
{
    self.isMyZoonOpen = NO; // if mt zoon open need chnage place
    self.isStartLoading=YES;
    self.isApp=NO;
    self.isAllProgrames=NO;
    self.dayNumber = TODAY;
    self.pendingLoadingDueToAppResign=NO;
    if(self.programesPerDayForChanelID == nil)
    {
        self.programesPerDayForChanelID = [NSMutableDictionary dictionary];
    }

    NSString * responseString =  [[TVCache sharedTVCache] objectForKey:MAP_SITE];
    NSDictionary *parsedJSON = [responseString objectFromJSONString];
    
    NSArray * pages  = [parsedJSON objectForKey:@"PagesInfo"];
    NSDictionary *EPGPageDic = [self findEPGPageFromPages:pages];
    if (EPGPageDic != nil)
    {
        TVPage *page = [[TVPage alloc]initWithDictionary:EPGPageDic];
        [self downloadGalleryForPage:page];
        [page release];
    }
    else
    {
        if  ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
        {
            [self downloadMainMenuFromSiteMap];
        }
    }
}

-(void)downloadMainMenuFromSiteMap
{
    static NSString *DynamicItemsKey = @"MainMenuDynamnicItems";
  
    ASLog2Debug(@"[EPGManager][Request][SiteAPI] requestForGetSiteMap");
        __block TVPAPIRequest *request = [TVPSiteAPI requestForGetSiteMap:nil];
        
        [request setStartedBlock:^{
                       
        }];
        
        [request setFailedBlock:^{
            self.isStartLoading=NO;
        }];
    
    
        [request setCompletionBlock:^{
            ASLog2Debug(@"requestForGetSiteMap: \n %@",request);////////
            
            NSString *responseString = request.responseString;
            
            NSString *cachesDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
            NSString *filePath = [cachesDir stringByAppendingPathComponent:MainMenuFileName1];
            [responseString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
            
            NSArray * dynamicMenuItems = [self parseMenuItemsResponseFromMapSite:responseString];
            if (dynamicMenuItems != nil)
            {
                [[TVCache sharedTVCache] setObject:dynamicMenuItems forKey:DynamicItemsKey];
            }
            if (responseString != nil)
            {
                [[TVCache sharedTVCache] setObject:responseString forKey:MAP_SITE];
            }
            [self loadData];
            
        }];
        
        [self sendRequest:request];
    
}

-(NSArray *) parseMenuItemsResponseFromMapSite : (NSString *) JSONString
{
    NSDictionary *parsedJSON = [JSONString objectFromJSONString];
    
    NSArray * menus  = [parsedJSON objectForKey:@"Menues"];
    
    NSMutableArray *temp = [NSMutableArray array];
    
    for (NSDictionary * tempMenu in menus)
    {
        NSInteger menuID = [[tempMenu objectForKey:@"ID"] integerValue];
        //ASLog@"menuID =  %d",menuID);
// warning only for test replace menu id to hard code 47
        if ( menuID==[[TVConfigurationManager sharedTVConfigurationManager] mainMenuID]){
        //if ( menuID==47){                //ASLog@"menu =  \n%@",tempMenu);
         //   ASLog(@"tempMenu = \n%@",tempMenu);
            NSArray *menuItemsAsDictionary = [tempMenu objectForKey:@"MenuItems"];
            
            for (NSDictionary *dictionary in menuItemsAsDictionary)
            {
                //ASLog2Debug(@"\n\n menuItem: \n%@\n\n",dictionary);////////
                TVMenuItem *item = [[TVMenuItem alloc] initWithDictionary:dictionary];
                ASLog2Debug(@"MenuItem: %@ vodCategoryID = %@",item, item.vodCategoryID);////////
                [temp addObject:item];
                [item release];
            }
            
        }
    }
    
    return [NSArray arrayWithArray:temp];
    
}

-(void)loadDataForOneDayWithDay:(NSInteger)theDayNumber
{
    if(self.programesPerDayForChanelID == nil)
    {
        self.programesPerDayForChanelID = [NSMutableDictionary dictionary];
    }
    
    self.dayNumber = theDayNumber;

    NSString * responseString =  [[TVCache sharedTVCache] objectForKey:MAP_SITE];
    NSDictionary *parsedJSON = [responseString objectFromJSONString];
    
    NSArray * pages  = [parsedJSON objectForKey:@"PagesInfo"];
    NSDictionary *EPGPageDic = [self findEPGPageFromPages:pages];
    if (EPGPageDic != nil){
        TVPage *page = [[TVPage alloc]initWithDictionary:EPGPageDic];
        [self downloadGalleryForPage:page];
        [page release];
    }
    
}

-(NSDictionary *)findEPGPageFromPages:(NSArray *)pages{
    NSDictionary *EPGPage= nil;
    if  ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
    {
        self.epgId = EPD_ID;
    }else if ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPhone)
    {
           self.epgId = IPHONE_EPG_ID;
    }

    for (NSDictionary * page in pages) {
        if ([[page objectForKey:@"ID"]integerValue] == self.epgId)
        {
            EPGPage = page;
            break;
        }
    }
    return EPGPage;
}

-(void) downloadGalleryForPage:(TVPage *)item
{
    self.progressHUDShow = NO;
    self.selectedChannelGropId=INIT_SELEDCTED_CHANNEL_GROP_Id;
    NSString * dateString = [self takeTodayTime];
    NSString * chache_gallery_string = [NSString stringWithFormat:@"%@_%@_%@",SAVES_GALLERY,[item.mainGalleriesIDs firstObject],dateString ];
    self.galleryItems = [[TVCache sharedTVCache] objectForKey:chache_gallery_string];
    if (self.galleryItems == nil)
    {
        ASLog2Debug(@"[EPGManager][Request][SiteAPI] requestForGetGalleyContent");////////

        __block TVPAPIRequest * request =  [TVPSiteAPI requestForGetGalleyContent : [[item.mainGalleriesIDs firstObject]longValue]
                                                                           atPage :  self.epgId
                                                                      pictureSize : CGSizeZero
                                                                         pageSize : 100
                                                                   startFromIndex : 0
                                                                         delegate : nil];
        [request setStartedBlock:^{
            [self.progressHUD showWithStatus:NSLocalizedString(@"Loading...", nil) maskType:SVProgressHUDMaskTypeNone];
            if (self.isMyZoonOpen == YES)
            {
                // hide
                [self.progressHUD dismiss];
                [SVProgressHUD toggleShowHideHudView:YES];
            }
            self.progressHUDShow = YES;
            
        }];
        
        [request setFailedBlock:^{
            
            [self.progressHUD dismissWithError:nil];
            [self.progressHUD explicitlyDismiss];
            [SVProgressHUD explicitlyDismiss];
             self.progressHUDShow = NO;
            self.isStartLoading=NO;
//            ASLog2Debug(@"Showed error for request failed");////////
        }];
        
        [request setCompletionBlock:^{
            
            // ASLog2Debug(@"requestForGetGalleyContent:\n %@",request);////////
            
            self.galleryItems = [self parseGetGalleryContentResponse:request];
            //[EpgManager sharedEpgManager].galleryItems = self.galleryItems;
            if (self.galleryItems != nil)
            {
                [[TVCache sharedTVCache] setObject:self.galleryItems forKey:chache_gallery_string];
            }
            [self downloadChannelMediaListWithChannelID:[[self.galleryItems objectAtIndex:0] TVMChannelID]];
        }];
        [self sendRequest:request];
    }
    else
    {
        //[EpgManager sharedEpgManager].galleryItems = self.galleryItems;
        [self downloadChannelMediaListWithChannelID:[[self.galleryItems objectAtIndex:0] TVMChannelID]];
    }
}

-(void) downloadChannelMediaListWithChannelID:(NSInteger)channelID
{
    self.isStartLoading=YES;
    self.isApp=NO;
    self.numberOfChannelsWithOutProgrames=0;
    self.selectedChannelGropId = channelID;
    NSString * dateString = [self takeTodayTime];
    NSString * chache_channel_string = [NSString stringWithFormat:@"%@_%d_%@",SAVES_CHANNELS,channelID,dateString];
    NSArray *oldMediaList = [[TVCache sharedTVCache] objectForKey:chache_channel_string];
    
    if (oldMediaList == nil)
    {
        ASLog2Debug(@"[EPGManager][Request][Media] - requestForGetChannelMediaList");////////

        __block TVPAPIRequest *request =  [TVPMediaAPI requestForGetChannelMediaList:channelID
                                                                         pictureSize:[TVPictureSize iPadEpgChnnel128X128]
                                                                            pageSize:100
                                                                           pageIndex:0
                                                                             orderBy:TVOrderByNoOrder
                                                                            delegate:nil];
        [request setStartedBlock:^{
            [self showLoader];
        }];

        [request setFailedBlock:^{
            [self.progressHUD dismissWithError:nil];
            [self.progressHUD explicitlyDismiss];
            self.progressHUDShow = NO;
            self.isStartLoading = NO;
        }];

        [request setCompletionBlock:^{
            ASLog2Debug(@"requestForGetChannelMediaList:\n %@",request);////////
            NSArray *mediaList = [self parseGetChannelMediaListResponse:request];
            if (mediaList != nil)
            {
                [[TVCache sharedTVCache] setObject:mediaList forKey:chache_channel_string];
                [[TVCache sharedTVCache] setObject:mediaList forKey:HAVE_CHASH];
            }
            [self completionLoadChannelsWithChannels:mediaList andChannelID:channelID];
        }];

        [self sendRequest:request];
    }
    else
    {
        [self showLoader];
        [self completionLoadChannelsWithChannels:oldMediaList andChannelID:channelID];
    }
}


-(void)completionLoadChannelsWithChannels:(NSArray*)mediaList andChannelID:(NSInteger)channelID
{
   
    
    if (self.selectedChannelGropId == [[self.galleryItems objectAtIndex:0] TVMChannelID])
    {
        self.realChannels = mediaList;
        self.currentlyPresentedChannel=mediaList;
        self.isAllChannels = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:EpgLoadAllChannels object:nil];
    }
    else
    {
        self.currentlyPresentedChannel=mediaList;
        self.isAllChannels=NO;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:EpgLoadCurrectlyChannels object:nil];
    
    
    
    if (self.realPrograms == nil)
    {
        self.realPrograms = [NSMutableDictionary dictionary];
    }else{
        [self.realPrograms removeAllObjects];
    }
    
    
    if (self.loadOnlyChannels == NO)
    {
        //  Fills the array to know when we finish load all programs,
        //  After loading the program will remove the id from the array,
        // and when the array is empty we know that all programs of all channels loaded.
        
        NSMutableArray * temp =  [[NSMutableArray alloc]init];
        self.currentlyLoudingProgrames = temp;
        [temp release];
        
        for (TVMediaItem * item  in mediaList)
        {
            if ([item.ExternalIDs objectForKey:@"epg_id"  ] !=nil)
            {
                NSArray * tempArr = [item.ExternalIDs objectForKey:@"epg_id"];
                [self.currentlyLoudingProgrames addObject:(NSString*)[tempArr firstObject]];
            }
        }
        self.numberOfItemsWithEPGid = [self.currentlyLoudingProgrames count];
        if (self.numberOfItemsWithEPGid >0)
        {
            NSInteger index =0;
            
            for (TVMediaItem * item  in self.currentlyPresentedChannel)
            {
                if ([item.ExternalIDs objectForKey:@"epg_id"  ] !=nil){
                    NSArray * tempArr = [item.ExternalIDs objectForKey:@"epg_id"];
                    [self getProgramsForChnnelID:(NSString*)[tempArr firstObject] andIndexOfChannel:index forOneDay:self.dayNumber];
                }
                index++;
                
            }
        }
        else if (self.numberOfItemsWithEPGid ==0 && self.isApp==NO)
        {
            self.isAllProgrames=NO;

            NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
            [userInfo setObject:[NSNumber numberWithInt:-1] forKey:@"ChannelNumber"];
            [userInfo setObject:[NSNumber numberWithInt:[self.currentlyLoudingProgrames count]] forKey:@"MoreChannels"];
            [userInfo setObject:[NSNumber numberWithBool:self.isApp] forKey:@"AppResignedActive"];
            [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneDayProgrames object:nil userInfo:userInfo];

            self.progressHUDShow = NO;
            [self.progressHUD explicitlyDismiss];
            [SVProgressHUD explicitlyDismiss];

        }
    }
    else  // loading only channels!
    {
        self.loadOnlyChannels = NO;
        self.realChannels = mediaList;
        self.currentlyPresentedChannel = mediaList;
        self.isAllChannels = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:EpgLoadOnlyChannelsLoading object:nil];
        self.progressHUDShow = NO;
        self.isStartLoading = NO;
        [self.progressHUD explicitlyDismiss];
        [SVProgressHUD explicitlyDismiss];
    }

}

-(int) getChannelIndexForChannelMediaId:(NSString *)channelMediaId{
    NSInteger index =0;
    
    for (TVMediaItem * item  in self.currentlyPresentedChannel)
    {
        if ([item.mediaID isEqualToString:channelMediaId] ){
            return index;
        }
        index++;
        
    }
    
    return -1;
}

-(void)getProgramsForChnnelID:(NSString *)chnnelID andIndexOfChannel:(NSInteger)index forOneDay:(NSInteger)day{
     NSString * dateString = [self takeTodayTime];
    NSString *programe_save_string = [NSString stringWithFormat:@"programne_save_string_%@_%d_%@",chnnelID,day,dateString];
    //ASLog(@"programe_save_string = %@",programe_save_string);
    
    NSArray *oldMediaList = [[TVCache sharedTVCache] objectForKey:programe_save_string];
    if (oldMediaList == nil)
    {
        // ATTENTION: currently utcOffset is 7 to match TV time
        ASLog2Debug(@"[EPGManager][Request]: requestForGetEPGChannelsPrograms");

        __block TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelsPrograms:chnnelID
                                                                           pictureSize : [TVPictureSize fullPictureSize]
                                                                                  oUnit:@"Days"
                                                                             fromOffset:day
                                                                               toOffset:(day+1)
                                                                               utcOffset:7
                                                                               delegate:nil];
        [request setStartedBlock:^
        {
            [self showLoader];
        }];
        
        [request setFailedBlock:^
        {
            ASLog(@"GetProgramsForChnnelID error = \n%@",request.error);
            self.numberOfChannelsWithOutProgrames++;
            [self syscTvGuideWithChannelId:chnnelID andIndexOfChannel:index];
        }];
        
        [request setCompletionBlock:^
        {
            NSArray * channelItems = [request JSONResponse];
            if (channelItems != nil)
            {
                [[TVCache sharedTVCache] setObject:channelItems forKey:programe_save_string];
            }
            [self completionLoadProgramesWithChannels:channelItems andChannelID:chnnelID andIndexOfChannel:index forOneDay:day];
        }];
        
        [self sendRequest:request];
    }
    else
    {
        [self showLoader];
        [self completionLoadProgramesWithChannels:oldMediaList andChannelID:chnnelID andIndexOfChannel:index forOneDay:day];
    }
}


// To get one program details

-(void)getCurrentProgramForChnnelID:(NSString *)chnnelID andIndexOfChannel:(NSInteger)index forOneDay:(NSInteger)day{
    //NSString * dateString = [self takeTodayTime];
    //NSString *programe_save_string = [NSString stringWithFormat:@"programne_save_string_%@_%d_%@",chnnelID,day,dateString];
    //ASLog(@"programe_save_string = %@",programe_save_string);
    
    // for now
    NSArray *oldMediaList = nil;//[[TVCache sharedTVCache] objectForKey:programe_save_string];
    if (oldMediaList == nil)
    {
        // ATTENTION: currently utcOffset is 7 to match TV time
        ASLog2Debug(@"[EPGManager][Request]: requestForGetEPGChannelsPrograms");
        
        __block TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelsPrograms:chnnelID
                                                                           pictureSize : [TVPictureSize fullPictureSize]
                                                                                  oUnit:@"Days"
                                                                             fromOffset:day
                                                                               toOffset:(day+1)
                                                                              utcOffset:7
                                                                               delegate:nil];
        [request setStartedBlock:^
         {
           
         }];
        
        [request setFailedBlock:^
         {
             ASLog(@"GetProgramsForChnnelID error = \n%@",request.error);
         }];
        
        [request setCompletionBlock:^
         {
             ASLog2Debug(@"request:\n%@",request);////////
             
         }];
        
        [self sendRequest:request];
    }
    else
    {
        
    }
}




-(void)completionLoadProgramesWithChannels:(NSArray*)channelItems andChannelID:(NSString *)chnnelID andIndexOfChannel:(NSInteger)index forOneDay:(NSInteger)day
{
    NSMutableArray * temp = [NSMutableArray array];
    if ([channelItems count]==0)
    {
        self.numberOfChannelsWithOutProgrames++;
        ASLog(@"numberOfChannelsWithOutProgrames= %d",self.numberOfChannelsWithOutProgrames);
    }
    
    for (NSDictionary *programDic in channelItems)
    {
        TVEPGProgram *program = [[TVEPGProgram alloc] initWithDictionary:programDic];
        if (program != nil)
        {
            if ([TVSessionManager sharedTVSessionManager].appTypeUsed == TVAppType_iPad2 && self.dayNumber == TODAY)
            {
                NSInteger today = [self takeDayfromDate:[NSDate date]];
                NSInteger progameDay = [self takeDayfromDate:program.startDateTime];
                if (today == progameDay)
                {
                    [temp addObject:program];
                }
            }
            else
            {
                [temp addObject:program];
            }
        }
        [program release];
    }
    if (temp != nil)
    {
        [self.realPrograms setObject:temp forKey:chnnelID];
    }
    //
    [self syscTvGuideWithChannelId:chnnelID andIndexOfChannel:index];
}


-(void)syscTvGuideWithChannelId:(NSString *)channelID andIndexOfChannel:(NSInteger)index
{
    if (self.showReadyPrograme == YES)
    {
        // to know that finish louded all channels programes
        [self removeItemFromCurrentlyLoudingProgramesWith:channelID];
        // contain programes pre channel until now.
        self.oneDayPrograms = self.realPrograms;
        // insert date to dictinory so sent with Notification.
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setObject:[NSNumber numberWithInt:index] forKey:@"ChannelNumber"];
        [userInfo setObject:[NSNumber numberWithInt:[self.currentlyLoudingProgrames count]] forKey:@"MoreChannels"];
        [userInfo setObject:[NSNumber numberWithBool:self.isApp] forKey:@"AppResignedActive"];
        //
        if ((self.dayNumber == TODAY) && (self.selectedChannelGropId == [[self.galleryItems objectAtIndex:0] TVMChannelID]))
        {
            self.todayPrograms = self.realPrograms;
            if ([self.currentlyLoudingProgrames count] == 0 && self.isApp == NO)
            {
                self.isAllProgrames = YES;
                self.isStartLoading = NO;
            }
            else
            {
                self.isAllProgrames = NO;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadTodayProgrames object:nil userInfo:userInfo];
        }//
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneDayProgrames object:nil userInfo:userInfo];
        }
        // close progressHUD.
        if ([self.currentlyLoudingProgrames count]==0 && self.isApp==NO)
        {
            self.progressHUDShow = NO;
            [self.progressHUD explicitlyDismiss];
            [SVProgressHUD explicitlyDismiss];
        }
    }
    else
    {
        [self removeItemFromCurrentlyLoudingProgramesWith:channelID];
        // if array is empty == all programes louded.
        if ([self.currentlyLoudingProgrames count] ==0)
        {
            @synchronized (self)
            {
                self.isStartLoading = NO;
                self.progressHUDShow = NO;
                self.oneDayPrograms = self.realPrograms;
                if ((self.dayNumber == TODAY) && (self.selectedChannelGropId == [[self.galleryItems objectAtIndex:0] TVMChannelID]))
                {
                    self.todayPrograms = self.realPrograms;
                     self.isAllProgrames=YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadTodayProgrames object:nil];
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneDayProgrames object:nil];
                }
                [self.progressHUD dismiss];
                [SVProgressHUD dismiss];
                [self.progressHUD explicitlyDismiss];
                [SVProgressHUD explicitlyDismiss];
                self.progressHUDShow = NO;
            }
        }
    }
}


-(void)removeItemFromCurrentlyLoudingProgramesWith:(NSString *)channelID
{
    for (NSString * currectChannelId in self.currentlyLoudingProgrames)
    {
        if ([currectChannelId isEqualToString: channelID ])
        {
            if (currectChannelId !=nil)
            {
                [self.currentlyLoudingProgrames removeObject:currectChannelId];
                break;
            }
        }
    }
}

#pragma mark - get programes to curr show channel

-(void)getProgramsToOneChnnelID:(NSString *)chnnelID{
    
    // ATTENTION: currently utcOffset is 7 to match TV time
    NSInteger day=0;
    NSString * dateString = [self takeTodayTime];
    NSString *programe_save_string = [NSString stringWithFormat:@"programne_save_string_%@_%d_%@",chnnelID,day,dateString];
   // ASLog(@"programe_save_string = %@",programe_save_string);
    NSArray *oldMediaList = [[TVCache sharedTVCache] objectForKey:programe_save_string];
    //     <oUnit>Days or Hours or Current</oUnit>
    if (oldMediaList==nil)
    {
        ASLog2Debug(@"[EPGManager][Request][Media] - requestForGetEPGChannelsPrograms");////////
        __block TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelsPrograms:chnnelID
                                                                           pictureSize : [TVPictureSize fullPictureSize]
                                                                                  oUnit:@"Days"
                                                                             fromOffset:0
                                                                               toOffset:1
                                                                              utcOffset:7
                                                                               delegate:nil];
        
        
        [request setStartedBlock:^{
          //  [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading...", nil) maskType:SVProgressHUDMaskTypeNone];
            
        }];
        
        [request setFailedBlock:^{
            [SVProgressHUD dismissWithError:nil];
            [SVProgressHUD explicitlyDismiss];
            self.progressHUDShow = NO;
            //ASLog@"GetProgramsForChnnelID error = \n%@",request.error);
            //
            [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneChannelProgrames object:nil];
        }];
        
        [request setCompletionBlock:^{
          
            NSArray * channelItems = [request JSONResponse];
            if (channelItems != nil)
            {
                [[TVCache sharedTVCache] setObject:channelItems forKey:programe_save_string];
            }
            [self completionLoadProgramsToOneChannelWithChannels:channelItems andChannelID:chnnelID];
            
            
             [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneChannelProgrames object:nil];
            
               }];
        
        [self sendRequest:request];
    }
    else
    {
         [self completionLoadProgramsToOneChannelWithChannels:oldMediaList andChannelID:chnnelID];
    }
}


-(void)getCurrentProgramInChnnelID:(NSString *)chnnelID andReturnWithBaseNotificationName:(NSString *)notifBaseName{
    
    // ATTENTION: currently utcOffset is 7 to match TV time
    NSInteger day=0;
    NSString * dateString = [self takeTodayTime];
    NSString *programe_save_string = [NSString stringWithFormat:@"programne_save_string_%@_%d_%@",chnnelID,day,dateString];
    // ASLog(@"programe_save_string = %@",programe_save_string);
    NSArray *oldMediaList = [[TVCache sharedTVCache] objectForKey:programe_save_string];
    
    __block TVEPGProgram *prog = [self findCurrentProgramFromArray:oldMediaList];
   
    
    
    //     <oUnit>Days or Hours or Current</oUnit>
    if (prog == nil)
    {
        ASLog2Debug(@"[EPGManager][Request][Media] - requestForGetEPGChannelsPrograms");////////
        __block TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelsPrograms:chnnelID
                                                                           pictureSize : [TVPictureSize fullPictureSize]
                                                                                  oUnit:@"Current"
                                                                             fromOffset:0
                                                                               toOffset:1
                                                                              utcOffset:7
                                                                               delegate:nil];
        
        
        [request setStartedBlock:^{
            //  [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading...", nil) maskType:SVProgressHUDMaskTypeNone];
            
        }];
        
        [request setFailedBlock:^{
            [SVProgressHUD dismissWithError:nil];
            [SVProgressHUD explicitlyDismiss];
            self.progressHUDShow = NO;
           
            [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_%@",notifBaseName,@"Failed"] object:nil userInfo:nil];
        }];
        
        [request setCompletionBlock:^{
            
            NSArray * channelItems = [request JSONResponse];
            if (channelItems != nil)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_%@",notifBaseName,@"Complete"] object:nil userInfo:nil];

            }
//            [self completionLoadProgramsToOneChannelWithChannels:channelItems andChannelID:chnnelID];
        }];
        
        [self sendRequest:request];
    }
    else
    {
//        [self completionLoadProgramsToOneChannelWithChannels:oldMediaList andChannelID:chnnelID];
    }
}


-(TVEPGProgram *)findCurrentProgramFromArray:(NSArray *)programs
{
    TVEPGProgram * prog = nil;
    NSDate * date = [NSDate date];
    
    NSInteger indexOfProgram = NSNotFound;
    NSDate *lastDateBefore = nil;
    for(int i=1; i < programs.count ; i++){
        TVEPGProgram *program =(TVEPGProgram *) [programs objectAtIndex:i];
        NSComparisonResult resultOfStartDate = [program.startDateTime compare:date];
        // ASLog(@"%@"  -  @"" ,program.startDateTime, date)
        BOOL showStartsAfterTheGivenTime = (resultOfStartDate == NSOrderedAscending || resultOfStartDate == NSOrderedSame);
        BOOL showStartsBeforeTheGivenTime = (resultOfStartDate == NSOrderedDescending);
        
        BOOL showEndsBeforeGivenTime = ([program.endDateTime compare:date] == NSOrderedDescending) || ([program.endDateTime compare:date] == NSOrderedSame);
        BOOL lastProgramAndTimeIsAfter = (i == (programs.count - 1)) && (showEndsBeforeGivenTime == NO);
        BOOL firstProgramAndTimeIsBefore = (i == 0) && (showStartsAfterTheGivenTime == NO);
        
        if( (showEndsBeforeGivenTime && showStartsAfterTheGivenTime) || firstProgramAndTimeIsBefore || lastProgramAndTimeIsAfter){
            indexOfProgram = i;
            break;
        }
        else{
            if(showStartsBeforeTheGivenTime && (lastDateBefore == nil || [lastDateBefore compare:program.startDateTime] == NSOrderedDescending)){
                lastDateBefore = program.startDateTime;
                indexOfProgram = i;
            }
        }
    }
    
    if (indexOfProgram != NSNotFound)
    {
        prog = [programs objectAtIndex:indexOfProgram ];
    }
    return prog;
}



-(void)completionLoadProgramsToOneChannelWithChannels:(NSArray*)channelItems andChannelID:(NSString *)chnnelID
{
    self.isStartLoading=NO;
    NSMutableArray * temp = [NSMutableArray array];
    for (NSDictionary *programDic in channelItems)
    {
        TVEPGProgram *program = [[TVEPGProgram alloc] initWithDictionary:programDic];
        if (program != nil)
        {
            [temp addObject:program];
        }
        [program release];
    }
    if (self.oneChannelProgram == nil)
    {
        self.oneChannelProgram = [NSMutableArray array];
    }
    self.oneChannelProgram= [NSMutableArray arrayWithArray:temp];
    ASLog(@" self.oneChannelProgram count =  %i",  [self.oneChannelProgram count]);
    //
    [[NSNotificationCenter defaultCenter] postNotificationName:EpgFinishLoadOneChannelProgrames object:nil];
    
    [SVProgressHUD explicitlyDismiss];
    self.progressHUDShow = NO;
}


#pragma mark - Utilty

-(NSString *)takeEpgIdFromMediaItem:(TVMediaItem *)theMediaItem
{
    NSString * chanelID =@"";
    if ([theMediaItem.ExternalIDs objectForKey:@"epg_id"  ] !=nil)
    {
        NSArray * tempArr = [theMediaItem.ExternalIDs objectForKey:EPG_ID_NAME];
        chanelID = (NSString*)[tempArr firstObject];
    }
    return chanelID;
}

-(NSString*)takeTodayTime
{
    NSDate * date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = TVDateFormat;
    NSString * dateString = [formatter stringFromDate:date];
    [formatter release];
    return dateString;
}

-(void)showLoader
{
    if (self.loadOnlyChannels == YES)
    {
        self.progressHUDShow = YES;
    }
    if (self.progressHUDShow == NO)
    {
        if ([TVSessionManager sharedTVSessionManager].appTypeUsed != TVAppType_iPhone)
        {
            
            [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading...", nil) maskType:SVProgressHUDMaskTypeNone];
            if (self.isMyZoonOpen == YES)
            {
                // hide
                [SVProgressHUD toggleShowHideHudView:YES];
            }
            
        }
        self.progressHUDShow = YES;
    }
}

-(BOOL)chackIfHaveChannelCash
{
    BOOL isCahs = NO;
     NSArray *oldMediaList = [[TVCache sharedTVCache] objectForKey:HAVE_CHASH];
    if (oldMediaList != nil){
        isCahs=YES;
    }
    return isCahs;
}

-(BOOL)allProgramsHaveBeenLoaded
{
    return self.isAllProgrames;
}

-( TVGalleryItem *) findGalleryForSelectedChannelGropId
{
    TVGalleryItem * item=nil;
    for (TVGalleryItem * tvItem in self.galleryItems) {
        
        if ([tvItem TVMChannelID] == self.selectedChannelGropId) {
            item=tvItem;
            return item;
        }
    }
    return item;
}

-(TVEPGProgram *)findCurrectProgram
{
    TVEPGProgram * prog = nil;
    NSDate * date = [NSDate date];
    for (TVEPGProgram * p in self.todayPrograms)
    {
        NSDate * programStartTime =p.startDateTime;
        NSDate * prpgramEndTime = p.endDateTime;
        if (([date compare:programStartTime]  == NSOrderedDescending) &&( [date compare:prpgramEndTime] == NSOrderedAscending))
        {
            prog = p;
            break;
        }
        
    }
    return prog;
}

-(NSInteger)findPlaceOfCurrectProgram
{
    NSDate * date = [NSDate date];
    NSInteger index=0;
    for (TVEPGProgram * p in self.todayPrograms)
    {
        NSDate * programStartTime =p.startDateTime;
        NSDate * prpgramEndTime = p.endDateTime;
        if (([date compare:programStartTime]  == NSOrderedDescending) &&( [date compare:prpgramEndTime] == NSOrderedAscending))
        {
            break;
        }
        index++;
    }
    
    return index;
}


-(NSInteger)takeDayfromDate:(NSDate *)theDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString * myYearString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:theDate]];
    [df release];
    NSInteger day = [myYearString integerValue];
    ASLog(@"day = %d",day);
    return day;
}

-(NSDate *)takeSingapurTimeWIthDate:(NSDate *)sourceDate
{
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"SGT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval =  sourceGMTOffset - destinationGMTOffset;
    
    NSDate* currentDateWithOffset = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
    
    return currentDateWithOffset;
}

#pragma mark - Parsing

-(NSArray *) parseGetGalleryContentResponse : (TVPAPIRequest *)request
{
    NSArray *items = [request JSONResponse];
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *topGalleryItemDictionary in items)
    {
        TVGalleryItem *galleryItem = [[TVGalleryItem alloc] initWithDictionary:topGalleryItemDictionary];
        if (galleryItem != nil)
        {
            [temp addObject:galleryItem];
        }
        [galleryItem release];
    }
    return [NSArray arrayWithArray:temp];
}

-(NSArray *)  parseGetChannelMediaListResponse: (TVPAPIRequest *) request
{
    NSArray * mediaItems = [request JSONResponse];
    NSMutableArray * tempMediaItems = [NSMutableArray array];
    
    for ( NSDictionary * mediaItemDictionary in mediaItems )
    {
        TVMediaItem * mediaItem = [[TVMediaItem alloc] initWithDictionary:mediaItemDictionary];
        [[AsyncImageLoader sharedAsyncImageLoader] loadImageAtURL:mediaItem.pictureURL];
        
        // Attention - for now always adding
        if(YES || (mediaItem.mainFile.fileURL.path !=nil || mediaItem.trailerFile.fileURL.path != nil)){
            [tempMediaItems addObject:[mediaItem  autorelease]];
            
        }
    }
    
    return [NSArray arrayWithArray:tempMediaItems];
}


#pragma mark - Consts strings

NSString *const EpgLoadAllChannels = @"EPG_All_CHANELS_LOAD";
NSString *const EpgLoadCurrectlyChannels = @"EPG_ONE_CHANELS_LOAD";
NSString *const EpgLoadOnlyChannelsLoading = @"EPG_ONLY_CHANELS_LOAD";

NSString *const EpgFinishLoadTodayProgrames = @"EPG_FINISH_LOAD";
NSString *const EpgFinishLoadOneDayProgrames = @"EPG_ONE_DAY_FINISH_LOAD";

NSString *const EpgFinishLoadOneChannelProgrames = @"EPG_ONE_CHANNEL_PROGRAM_FINISH_LOAD";


#pragma mark - not used code

-(void) progamesFromDayWithDate:(NSDate *)date
{
    self.selectedDayDate =date;
    //    [self.realPrograms release];
    //    self.realPrograms=nil;
    
    if(self.realPrograms != nil){
        [self.realPrograms removeAllObjects];
    }
    else{
        self.realPrograms = [NSMutableDictionary dictionary];
    }
    
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSString * dateString = [formatter stringFromDate:date];
    
    for (TVMediaItem * item in self.currentlyPresentedChannel)
    {
        NSString * chnaelID=@"";
        if ([item.ExternalIDs objectForKey:@"epg_id"  ] !=nil)
        {
            NSArray * tempArr = [item.ExternalIDs objectForKey:@"epg_id"];
            chnaelID = (NSString*)[tempArr firstObject];
        }
        NSDictionary *temp = [self.programesPerDayForChanelID objectForKey:chnaelID];
        NSArray * progrmaes = [temp objectForKey:dateString];
        if(progrmaes !=nil)
        {
            [self.realPrograms setObject:[NSArray arrayWithArray:progrmaes] forKey:chnaelID];
        }
    }
    
    [formatter release];
}

-(void)orderProgramesByDaysWithAllProgramePerChanel:(NSArray *)allProgram andChannelID:(NSString *)channelID
{
    
    if ([allProgram count] >0)
    {
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        NSMutableArray * temp = [NSMutableArray array];
        NSDate *date1 = nil;
        NSDate * time;
        TVEPGProgram *firstProgram = [allProgram firstObject];
        if (firstProgram != nil){
            time = firstProgram.startDateTime;
        }else{
            time= [NSData data];
        }
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger components = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
        NSDateComponents *firstComponents = [calendar components:components fromDate:time];
        // ASLog(@"allProgram %d for channelID %@",[allProgram count],channelID);
        for ( TVEPGProgram *program in allProgram)
        {
            NSDateComponents *secondComponents = [calendar components:components fromDate:program.startDateTime];
            
            date1 = [calendar dateFromComponents:firstComponents];
            NSDate *date2 = [calendar dateFromComponents:secondComponents];
            NSComparisonResult result = [date1 compare:date2];
            if (result == NSOrderedSame)
            {
                [temp addObject:program];
            }
            else
            {
                // insert programes per one day to dictionry thay key is date!
                NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd-MM-yyyy"];
                NSString * dateString = [formatter stringFromDate:date1];
                [formatter release];
                if (temp != nil){
                    [dic setObject:[NSArray arrayWithArray:temp] forKey:dateString];
                }
                
                temp = [NSMutableArray array];
                
                firstComponents = [calendar components:components fromDate:date2];
            }
        }
        
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSString * dateString = [formatter stringFromDate:date1];
        [formatter release];
        if (temp != nil)
        {
            [dic setObject:temp forKey:dateString];
        }
        if (dic != nil)
        {
            [self.programesPerDayForChanelID setObject:dic forKey:channelID];
        }
        //ASLog(@" programesPerDayForChanelID %@",self.programesPerDayForChanelID );
    }
}


-(void) registerForSwipeNotifications
{
    [self unregisterForSwipeNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(handleMasterAboutToSwipe:)
                   name:TVNotificationName_MasterAboutToRevealContent
                 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myZoonOpen) name:kUserInfo_myZoonOpenNow object:nil];
}

-(void) unregisterForSwipeNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:TVNotificationName_MasterAboutToRevealContent object:nil];
    [center removeObserver:self name:kUserInfo_myZoonOpenNow object:nil];

}

-(void) handleMasterAboutToSwipe:(NSNotification *)notification
{
    NSDictionary * userInfo = notification.userInfo;
    // ASLog(@"LastContentPosition key= %@",[userInfo objectForKey:TVNotificationInfoKey_RevealSide]);
    if ([[userInfo objectForKey:TVNotificationInfoKey_RevealSide] isEqualToString:TVNotificationInfoValue_RevealTop])
    {
        [self myZoonOpen];
    }
    else
    {
        self.isMyZoonOpen = NO;
        self.progressHUDShow = NO;
    }
}

-(void)myZoonOpen
{
    self.progressHUDShow = YES;
    self.isMyZoonOpen = YES;
    [self.progressHUD explicitlyDismiss];
    [SVProgressHUD explicitlyDismiss];
}
@end

