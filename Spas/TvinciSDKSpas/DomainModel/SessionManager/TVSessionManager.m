//
//  TVSessionManager.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSessionManager.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVConfigurationManager.h"
#import "SynthesizeSingleton.h"
#import "TVDeviceFamily.h"
#import "TVPAPIRequest.h"
#import "TVInitObject.h"
#import "TVinciUtils.h"
#import "TVPSiteAPI.h"
#import "SmartLogs.h"
#import "TVDomain.h"
#import "JSONKit.h"
#import "TVUser.h"
#import "TVPDomainAPI.h"
#import "SmartLogs.h"
#import "SVProgressHUD.h"


NSString *const TVUserDataKey = @"UserData";
NSString *const TVSessionManagerKnownUsersKey = @"ExistingUsers";
NSString *const TVSessionManagerUsernameKey = @"TVinciUsername";
NSString *const TVSessionManagerPasswordKey = @"TVinciPassword";
NSString *const InitObjectKey = @"initObj";
NSString *const CurrentDomainKey = @"TVinciDomain";
NSString *const CurrentDeviceKey = @"TVinciDevice";
NSString *const TVSessionManagerCurrentUserKey = @"TVinciCurrentUser";

@interface TVSessionManager () <ASIHTTPRequestDelegate>

// @property (nonatomic,readwrite,copy)  NSDictionary  * userData;
@property (retain , readwrite) TVUser *currentUser;
//@property (retain , readwrite) TVDomain *currentDomain;
@property (nonatomic, retain , readwrite) TVDevice *currentDevice;
@property (nonatomic , retain , readwrite) TVInitObject *sharedInitObject;
@property (nonatomic, copy , readwrite) NSString *username;
@property (nonatomic, copy , readwrite) NSString *password;

-(void) parseSignInResponse : (TVPAPIRequest *) loginRequest;
-(void) getCurrentUserDetails;
-(void) getDomainInfo;

-(void) postNotification:(NSString *)notification error : (NSError *)error;

-(void) storeSiteGUID : (NSString *) siteGUID
             domainID : (NSNumber *) domainID;


-(void) registerForConfigurationNotifications;
-(void) unregisterFromConfigurationNotifications;
@end

@implementation TVSessionManager
@synthesize username = _username;
@synthesize password = _password;
@synthesize currentUser = _currentUser;
@synthesize sharedInitObject = _sharedInitObject;
@synthesize currentDomain = _currentDomain;
@synthesize currentDevice = _currentDevice;
@synthesize knownUsers = _knownUsers;

@synthesize showedSignInFailedError = _showedSignInFailedError;

@synthesize appTypeUsed = _appTypeUsed;
@synthesize lastPreferedLoginType = _lastPreferedLoginType, lastLoginIsForNewUser = _lastLoginIsForNewUser;
@synthesize sessionFlags = _sessionFlags;
@synthesize sessionAddedData = _sessionAddedData;

#pragma mark - Memory -

-(void) dealloc
{
    [self unregisterFromConfigurationNotifications];
    self.currentDevice = nil;
    self.currentDomain = nil;
    self.username = nil;
    self.password = nil;
    self.currentUser = nil;
    self.sharedInitObject = nil;
    
    [_sessionFlags release];
    [_sessionAddedData release];
    [super dealloc];
}

#pragma mark - Getters

-(TVDevice *) getCurrentDeviceInCurrentDomainInFamiliy:(kDeviceFamilyID) deviceFamilyID
{
    TVDevice * device =nil;
    if (self.currentDomain != nil)
    {
        
        int expectedDeviceFamilyID = deviceFamilyID;
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                
                device = [found.devices objectAtIndex:deviceIndex];
                self.currentDevice = device;
                return device;
            }
        }
    }
    
    return device;
}



-(TVDevice *) findCurrentDeviceInDomain:(NSInteger ) domainID
{
    if (self.currentDomain != nil)
    {
        
        int expectedDeviceFamilyID = domainID;
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                return [found.devices objectAtIndex:deviceIndex];
            }
        }
    }
    
    return nil;
    
}

-(TVDevice *)  findCurrentDeviceInCurrentDomainInALlFamiliesID
{
    
    TVDevice * device =nil;
    for (TVDeviceFamily * deviceFamily  in self.currentDomain.deviceFamilies)
    {
        device = [self findCurrentDeviceInDomain:deviceFamily.familyID];
        if (device) {
            break;
        }
    }
    
    self.currentDevice = device;
    return device;
}


-(void) findCurrentDeviceInCurrentDomain __attribute__((deprecated))
{
    if (self.currentDomain != nil)
    {
        
        int expectedDeviceFamilyID = kDeviceFamilyID_Smartphone;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            expectedDeviceFamilyID = kDeviceFamilyID_Tablet;
        }
        else
        {
            expectedDeviceFamilyID = kDeviceFamilyID_Smartphone;
        }
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                self.currentDevice = [found.devices objectAtIndex:deviceIndex];
            }
        }
    }
}

-(TVDevice *) currentDevice
{
    @synchronized (self)
    {
        if (_currentDevice == nil)
        {
            [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
        }
        return _currentDevice;
    }
}


-(void) loadUserData
{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableData *data = [userDefaults objectForKey:TVUserDataKey];
    if (data != nil)
    {
        NSDictionary *myDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if (myDictionary != nil)
        {
            // ASLogInfo(@"Loaded Saved userData: %@",myDictionary);
            self.userData = myDictionary;
        }
        else
        {
            self.userData = [NSDictionary dictionary];
        }
        
    }
    //    NSMutableData *data = [[NSMutableData alloc] init];
}

-(void) loadSharedInitObject
{
    self.sharedInitObject = [TVConfigurationManager sharedTVConfigurationManager].defaultInitObject;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *siteGUID = [userDefaults objectForKey:TVConfigSiteGUIDKey];
    NSNumber *domainID = [userDefaults objectForKey:TVConfigDomainIDKey];
    if (siteGUID != nil)
    {
        // ASLogInfo(@"Loaded Saved siteGUID: %@",siteGUID);
        self.sharedInitObject.siteGUID = siteGUID;
    }
    else
    {
        self.sharedInitObject.siteGUID = @"0";
    }
    
    if (domainID != nil)
    {
        // ASLogInfo(@"Loaded Saved domainID: %@",domainID);
        self.sharedInitObject.domainID = [domainID integerValue];
    }
}

-(void) loadCurrentUser
{
    ASLogInfo(@"loadCurrentUser");
    NSDictionary *userDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:TVSessionManagerCurrentUserKey];
    if (userDictionary != nil)
    {
        self.currentUser = [[[TVUser alloc] initWithDictionary:userDictionary] autorelease];
        ASLogInfo(@"Current User loaded: %@ %@",self.currentUser.firstName, self.currentUser.lastName);
    }
    else
    {
        ASLogInfo(@"No saved current user. Getting details.");
    }
    
    // ***      depicated !     ***
    // Download again anyway because the user might update his profile from time to time.
    // [self getCurrentUserDetails];
}

-(void) saveCurrentUser
{
    NSDictionary *userDictionary = [self.currentUser keyValueRepresentation];
    if (userDictionary != nil)
    {
        // ASLogInfo(@"Saving current user: %@",userDictionary);
        [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:TVSessionManagerCurrentUserKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self updateCurrentUserRememberMe];
    }
    else
    {
        ASLogInfo(@"Cannot save current user. Key value representation is invalid (nil dictionary)");
    }
    
}

/**********************
 updateCurrentUserRememberMe
 ----------------------------
 
 - if current user rememberMe is ACTIVE_NOT_VERIFIED set it to VERIFIED
 *********************/

-(void) updateCurrentUserRememberMe{
    // 10. check if knownUserRememberMe data exists
    
    NSMutableDictionary *knownUserRememberMe = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kKnownUserRememberMeKey]];
    if(knownUserRememberMe == nil){
        return;
    }
    // 20. check if has data on user
    if([[knownUserRememberMe allKeys]  containsObject:self.currentUser.siteGUID] == NO){
        return;
    }
    
    int rememberMeState= [[knownUserRememberMe objectForKey:self.currentUser.siteGUID] intValue];
    // 30. check if ACTIVE_NOT_VERIFIED - so need to update
    if(rememberMeState != kRememberMeState_ACTIVE_NOT_VERIFIED){
        return;
    }
    // 40. verify
    [knownUserRememberMe setObject:[NSNumber numberWithInt:kRememberMeState_VERIFIED] forKey:self.currentUser.siteGUID];
    
    // 50. saving
    [[NSUserDefaults standardUserDefaults] setObject:knownUserRememberMe forKey:kKnownUserRememberMeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSArray *) knownUsers
{
    @synchronized (self)
    {
        if (_knownUsers == nil)
        {
            [self loadKnownUsers];
        }
        return _knownUsers;
    }
}

-(TVDeviceState) deviceState
{
    if (self.currentDomain != nil)
    {
        // If there is a domain info, but the current device is not in it, then the device is not registered.
        ASLogInfo(@"self.currentDevice.deviceState = %d",self.currentDevice.deviceState);
        return (self.currentDevice != nil) ? self.currentDevice.deviceState : TVDeviceStateNotRegistered;
    }
    else
    {
        // No Domain, So no devie info at all.
        return TVDeviceStateUnknown;
    }
}

-(TVAuthenticationState) authenticationState
{
    return [self.sharedInitObject belongsToValidDomain] ? TVAuthenticationStateSignedIn : TVAuthenticationStateNotSignedIn;
}

-(BOOL) isSignedIn
{
    return [self authenticationState] == TVAuthenticationStateSignedIn;
}

-(BOOL) isDeviceReady
{
    return [self deviceState] == TVDeviceStateActivated;
}

#pragma mark - Setters

#pragma mark - Users
-(void) loadKnownUsers
{
    NSArray *JSONUsers = [[NSUserDefaults standardUserDefaults] objectForKey:TVSessionManagerKnownUsersKey];
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *userAsDictionary in JSONUsers)
    {
        TVUser *user = [[TVUser alloc] initWithDictionary:userAsDictionary];
        if (user != nil)
        {
            [temp addObject:user];
        }
        [user release];
    }
    self.knownUsers = [NSArray arrayWithArray:temp];
}

-(BOOL) removeUserFromKnownUsersList:(TVUser *)userToRemove
{
    BOOL result = NO;
    if ([self.knownUsers containsObject:userToRemove])
    {
        // Remove from users array
        NSMutableArray *temp = [NSMutableArray arrayWithArray:self.knownUsers];
        [temp removeObject:userToRemove];
        self.knownUsers = [NSArray arrayWithArray:temp];
        result = [self saveKnownUsersToUserDefaults];
    }
    return result;
}

-(void) addUserToKnownUsersList : (TVUser *) userToAdd
{
    if (userToAdd.username != nil)
    {
        if ([self.knownUsers containsObject:userToAdd] == NO)
        {
            NSMutableArray *temp = [NSMutableArray arrayWithArray:self.knownUsers];
            [temp addObject:userToAdd];
            self.knownUsers = [NSArray arrayWithArray:temp];
            [self saveKnownUsersToUserDefaults];
        }
    }
}

-(BOOL) saveKnownUsersToUserDefaults
{
    NSMutableArray *usersAsDictionaries = [NSMutableArray array];
    for (TVUser *user in self.knownUsers)
    {
        NSDictionary *userAsDictionary = [user keyValueRepresentation];
        if (userAsDictionary != nil)
        {
            [usersAsDictionaries addObject:userAsDictionary];
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:usersAsDictionaries] forKey:TVSessionManagerKnownUsersKey];
    return [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Lifecycle
-(id) init
{
    if (self = [super init])
    {
        ASLog2Debug(@"[showedSignInFailedError] = NO");////////
        self.showedSignInFailedError = NO;
        
        [self registerForConfigurationNotifications];
        [self loadSharedInitObject];
        [self loadUserData];
        if ([self.sharedInitObject belongsToValidDomain])
        {
            self.appTypeUsed = TVAppType_Unknown;
            self.lastPreferedLoginType = kPreferedLoginType_Unknown;
            self.lastLoginIsForNewUser = NO;
            [self loadCurrentUser];

        }
    }
    return self;
}

#pragma mark - Sign In / Sign Out

-(void) signInWithToken:(NSString *) token
{
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestforSignInWithToken:token delegate:nil];
    
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
        
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"%@",request.debugPostBudyString);
        NSLog(@"%@",request.responseString);
        
        [self parseSignInResponse:request];
    }];
    
    [request startAsynchronous];
}



-(void) signInSecureWithUserName:(NSString *) userName securePassword:(NSString *) securePassword
{
    self.username = userName;
    self.password = securePassword;
    
    ASLog2Debug(@"[Request][SiteAPI] requestForSignInWithUserName");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestforSigninSecureWithUserName:self.username
                                                                           password:self.password
                                                                           delegate:nil];
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
        
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"%@",request.debugPostBudyString);
        NSLog(@"%@",request.responseString);
        
        [self parseSignInResponse:request];
    }];
    
    [request startAsynchronous];
}

-(void) signInWithUsername:(NSString *)username password:(NSString *)password
{
    self.username = username;
    self.password = password;
    
    ASLog2Debug(@"[Request][SiteAPI] requestForSignInWithUserName");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestForSignInWithUserName:self.username
                                                                     password:self.password
                                                                     delegate:self];
    
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
        
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"%@",request.debugPostBudyString);
        NSLog(@"%@",request.responseString);
        
        [self parseSignInResponse:request];
    }];
    
    [request startAsynchronous];
}

-(void) ssoSignInWithUsername:(NSString *)username password:(NSString *)password
{
    self.username = username;
    self.password = password;
    
    ASLogInfo(@"[Request][SiteAPI] requestForSSOSigninWithUserName");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestForSSOSigninWithUserName:self.username
                                                                     password:self.password
                                                                     delegate:self];
    
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
        
    }];
    
    [request setCompletionBlock:^{
//        NSLog(@"%@",request);
        [self parseSignInResponse:request];
    }];
    
    [request startAsynchronous];
}

-(void) signInAfterFBWithUsername:(NSString *)username andPasswordFromFBSiteGUID:(NSString *)password
{
    
    self.username = username;
    self.password = password;
    
    if([password isKindOfClass:[NSNumber class]]){
        self.password = [NSString stringWithFormat:@"%@",password];
    }
    
    ASLog2Debug(@"[Request][SiteAPI] requestForSignInWithUserName");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestForSignInWithUserName:self.username
                                                                     password:self.password
                                                                     delegate:self];
    
    [request setStartedBlock:^{
        //ASLog@"Singing in...");
    }];
    
    [request setFailedBlock:^{
        
        
        // TBD - show error for error of sign in  - Check if it works
        
        
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
        //        ASLog2Debug(@"Showed error for request failed");////////
        //        ASLog2Debug(@"\nRequst \n\npostBody: %@\n\n postParams: %@\n\n",request.postBody,request.postParameters);////////
    }];
    
    [request setCompletionBlock:^{
        
        
        // TBD - handle Login status check here - and show FB error if needed.
        //       if OK continue with parseSignInResponse
        
        NSDictionary *response = [request JSONResponse];
        NSInteger loginStatus = [[response objectOrNilForKey:@"LoginStatus"] integerValue];
        
        if(loginStatus == TVLoginStatusOK){
            //        ASLog2Debug(@"\nRequst \n\npostBody: %@\n\n postParams: %@\n\n",request.postBody,request.postParameters);////////
            [self parseSignInResponse:request];
        }
        else{
            // post FB error
            
            NSString *errorMessage = [NSString stringWithFormat:@"You have not linked your Facebook to your %@ account. Please visit %@ to activate the link.",APPLICATION_NAME,APPLICATION_NAME];
            NSDictionary * errDictionary = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusUserDoesNotExist userInfo:errDictionary];
            
            //ASLog@"Facebook Login Failed: %@",error);
            
            
            //            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:error forKey:TVErrorKey];
            [[NSNotificationCenter defaultCenter] postNotificationName:TVSessionManagerSignInFailedNotification object:self userInfo:userInfo];
            //            ASLog2Debug(@"[showedSignInFailedError] = YES");////////
            //            [TVSessionManager sharedTVSessionManager].showedSignInFailedError = YES;
            
            
        }
        
        
        
    }];
    
    [request startAsynchronous];
    
}

-(void) signInWithFacebookResponse : (NSDictionary *) JSONResponse
{
    NSString *encryptedSiteGUID = [JSONResponse objectOrNilForKey:@"data"];
    
    // NEW CODE for signIn
    NSString *tvinciName = [JSONResponse objectOrNilForKey:@"tvinciName"];
    
    
    if (encryptedSiteGUID != nil)
    {
        ASLog2Debug(@"[Request][SiteAPI] requestForGetClearSiteGUIDFromEncryptedSiteGUID");////////
        
        __block TVPAPIRequest *request = [TVPSiteAPI requestForGetClearSiteGUIDFromEncryptedSiteGUID:encryptedSiteGUID
                                                                                            delegate:self];
        [request setFailedBlock:^{
            NSError *error = [request error];
            //ASLog@"Facebook Login Failed: %@",error);
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            ASLogError(error);
        }];
        
        [request setCompletionBlock:^{
            
            NSString *clearSiteGUID;
            
            if([[request JSONResponse] isKindOfClass:[NSNumber class]]){
                ASLog2Debug(@"");////////
                clearSiteGUID = [request JSONResponseJSONOrString];
            }
            else{
                clearSiteGUID = ([[request JSONResponse] length] > 0) ? [request JSONResponseJSONOrString] : [JSONResponse objectOrNilForKey:@"siteGuid"];
            }
            
            ASLogInfo(@"Clear siteGUID: %@",clearSiteGUID);
            if ([[request JSONResponse] isKindOfClass:[NSNumber class]] || clearSiteGUID.length > 0)
            {
                /****************************************************
                 Fix for FB login (shefyg)
                 --------------------------
                 The FB login did not do SignIn with the clearGUID as password (as needed - according to Arik Gaisler)
                 There for there was no test on the SignIn status, just on siteGUID and Domain ID (simple length test)
                 
                 The only thing that is really different when going with the old code through call to getDomainIDFromUserDetailsWithSiteGUID,
                 is an error code back from FB error.
                 
                 so the solution , is new function (signInWithUserName:AndPasswordFromFBSiteGUID).
                 
                 That would do the signIn function call the same as in regular signIn, and throw errors that match the fact that this is FB login.
                 
                 *****************************************************/
                
                // NEW CODE: using signInAfterFBWithUsername:andPasswordFromFBSiteGUID:
                [self signInAfterFBWithUsername:tvinciName andPasswordFromFBSiteGUID:clearSiteGUID];
                
                
                // OLD CODE: (KEEP IT FOR FUTURE REF - AND TO USE AGIAN IF NEEDED)
                //                [self getDomainIDFromUserDetailsWithSiteGUID:clearSiteGUID];
            }
            else
            {
                //                NSString *requestBody = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
                //ASLog@"Body: %@",requestBody);
                NSString *errorMessage = [NSString stringWithFormat:@"You have not linked your Facebook to your %@ account. Please visit %@ to activate the link.",APPLICATION_NAME,APPLICATION_NAME];
                
                
                NSDictionary * errDictionary = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
                NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusUserDoesNotExist userInfo:errDictionary];
                //ASLog@"Facebook Login Failed: %@",error);
                [self postNotification:TVSessionManagerSignInFailedNotification error:error];
                
                ASLog2Debug(@"[showedSignInFailedError] = YES");////////
                [TVSessionManager sharedTVSessionManager].showedSignInFailedError = YES;
            }
        }];
        
        [request startAsynchronous];
    }
    else
    {
        NSString *errorMessage = NSLocalizedString(@"Login Error: No SiteGUID", nil);
        NSDictionary * errDictionary = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusUserDoesNotExist userInfo:errDictionary];
        //ASLog@"Facebook Login Failed: %@",error);
        [self postNotification:TVSessionManagerSignInFailedNotification error:error];
    }
}



-(void) resetSessionInfo
{
    self.username = nil;
    self.password = nil;
    self.sharedInitObject.siteGUID = nil;
    self.sharedInitObject.domainID = 0;
    self.currentDevice = nil;
    self.currentDomain = nil;
    self.currentUser = nil;
    self.lastOpeningSessionDate = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:TVSessionManagerUsernameKey];
    [defaults removeObjectForKey:TVSessionManagerPasswordKey];
    [defaults removeObjectForKey:InitObjectKey];
    [defaults removeObjectForKey:CurrentDeviceKey];
    [defaults removeObjectForKey:CurrentDomainKey];
    [defaults removeObjectForKey:TVConfigSiteGUIDKey];
    [defaults removeObjectForKey:TVUserDataKey];
    [defaults removeObjectForKey:TVConfigDomainIDKey];
    [defaults removeObjectForKey:TVSessionManagerCurrentUserKey];
    [defaults synchronize];
    
    NSArray *cookiesArray = [NSArray arrayWithArray:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [cookiesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:obj];
    }];
}

-(void) logout
{
    @synchronized (self)
    {
        ASLog2Debug(@"[Request][SiteAPI] requestForSignOut");////////
        
        __block TVPAPIRequest *request = [TVPSiteAPI requestForSignOut:self];
        [request setStartedBlock:^{
            // [SVProgressHUD showWithStatus:NSLocalizedString(@"Signing out...", nil) maskType:SVProgressHUDMaskTypeGradient];
        }];
        
        
        [request setFailedBlock:^{
            //ASLog@"Logout Failed: %@",[request error]);
            [self postNotification:TVSessionManagerLogoutFailedNotification error:[request error]];
        }];
        
        [request setCompletionBlock:^{
            //  [SVProgressHUD dismiss];
            
            [self logOutCompleted];
        }];
        
        [request startAsynchronous];
    }
}

/*
 (void) logoutFromIPNO - makes a logout but keeps some consistant data:
 
 -  Made for extra facilities and features made by Tvinci Backend
 by exentding to Multi User option.
 
 -  Used by projects such Eutelsat, for multi user feature.
 
 
 Scenario for example:
 When logout is made, there still an option for
 login with another user from the same domain,
 so domain information should not be deleted.
 
 */
-(void) logoutFromIPNO {
    
    @synchronized (self)
    {
        ASLog2Debug(@"[Request][SiteAPI] requestForSignOut");////////
        
        __block TVPAPIRequest *request = [TVPSiteAPI requestForSignOut:self];
        [request setStartedBlock:nil];
        
        [request setFailedBlock:^{
            [self postNotification:TVSessionManagerLogoutFailedNotification error:[request error]];
        }];
        
        [request setCompletionBlock:^{
            //  [SVProgressHUD dismiss];
            [self logOutIPNOCompleted];
        }];
        
        [request startAsynchronous];
    }
    
}

- (BOOL)isSignedInToIPNO {
    if (self.userData) {
        return YES;
    }
    return NO;
}

-(void) resetSessionIPNOInfo
{
//    self.username = nil;
//    self.password = nil;
    self.currentUser = nil;
    self.userData = nil;
    self.sharedInitObject.siteGUID = nil;
    //  Remove all unwaned consistant data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults removeObjectForKey:TVSessionManagerUsernameKey];
//    [defaults removeObjectForKey:TVSessionManagerPasswordKey];
    [defaults removeObjectForKey:TVUserDataKey];
    [defaults removeObjectForKey:TVSessionManagerCurrentUserKey];
    [defaults synchronize];
    
    //  Remove coockies
    NSArray *cookiesArray = [NSArray arrayWithArray:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [cookiesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:obj];
    }];
}

-(void)logOutIPNOCompleted
{
    [self resetSessionIPNOInfo];
    [self postNotification:TVSessionManagerLogoutCompletedNotification userInfo:nil];
}


-(void)logOutCompleted
{
    [self resetSessionInfo];
    ASLogInfo(@"Logged Out");
    [self postNotification:TVSessionManagerLogoutCompletedNotification userInfo:nil];
}

#pragma mark - Domain And Device
-(void) checkDeviceState
{
    if (self.deviceState == TVDeviceStateUnknown)
    {
        [self getDomainInfo];
    }
}

-(void) forceCheckDeviceState{
    
    [self getDomainInfo];
    
}


-(void) getDomainInfo
{
    static BOOL inProgress = NO;
    if (inProgress == NO)
    {
        inProgress = YES;
        ASLogInfo(@"[Request][SiteAPI] requestForGetDomainInfo");////////
        
        __block TVPAPIRequest *request = [TVPDomainAPI requestForGetDomainInfo:self];
        
        [request setFailedBlock:^{
            //ASLog@"GetDomainInfo Failed: %@",[request error]);
            [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];
            inProgress = NO;
        }];
        
        [request setCompletionBlock:^{
            
            NSDictionary *response = [request JSONResponse];
            self.currentDomain = [[[TVDomain alloc] initWithDictionary:response] autorelease];
            [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
            if (self.currentDomain != nil)
            {
                ASLogInfo(@"Domain Exist. Device State Available");
                // If we have domain, we know the device state.
                [self postNotification:TVSessionManagerDeviceStateAvailableNotification userInfo:nil];
            }
            inProgress = NO;
        }];
        
        [request startAsynchronous];
    }
}

-(void) registerDevice
{
    ASLogDebug(@"[Request][SiteAPI] requestForAddDeviceToDomainWithDeviceName");////////
    NSInteger deviceBrand;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        deviceBrand = kDeviceBrand_iPad;
    }
    else
    {
        deviceBrand = kDeviceBrand_iPhone;
        
    }
    
    __block TVPAPIRequest *request = [TVPDomainAPI requestForAddDeviceToDomainWithDeviceName:[UIDevice currentDevice].name
                                                                               deviceBrandID:deviceBrand
                                                                                    delegate:self];
    
    [request setFailedBlock:^{
        ASLogDebug(@"request  = %@",request);
        [self postNotification:TVSessionManagerDeviceRegistrationFailedNotification error:[request error]];
    }];
    
    [request setCompletionBlock:^{
      
        [self parseRegisterDeviceResponse:request];
        
    }];
    
    [request startAsynchronous];
}


-(void) parseRegisterDeviceResponse : (TVPAPIRequest *) request
{
    ASLogDebug(@"Device registration finished ");
    NSDictionary *response = [request JSONResponse];
    ASLogDebug(@"request  = %@",request);
    
    self.currentDomain = [[[TVDomain alloc] initWithDictionary:[response objectForKey:TVDomainKey]] autorelease];
    ASLogDebug(@"self.currentDomain= %@",self.currentDomain);////////
    
    [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
    NSNumber* DomainResponseStatus = [response objectForKey:kAPIResponseKey_DomainResponseStatusKey];
    if (self.currentDevice != nil)
    {
        [self postNotification:TVSessionManagerDeviceRegistrationCompletedNotification userInfo:nil];
    }
    else
    {
        // handling known error
        // checking the response status
        if([DomainResponseStatus intValue] == kDomainResponseStatus_DeviceAlreadyExists)
        {
            ASLogDebug(@"[Device][Registration] Device already exists on another domain");////////
            NSString *errorStr = NSLocalizedString(@"Dialog Message: Device registered for another user", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification error:error];
            return;
        }
        else
        {
            NSString *errorStr = NSLocalizedString(@"Login Error: Device Registration Failed", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedNotification error:error];
        }
    }
}

-(void) activateDevice
{
    
    ASLog2Debug(@"[Request][SiteAPI] requestForChangeDeviceDomainStatus");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestForChangeDeviceDomainStatus:YES delegate:self];
    
    [request setFailedBlock:^{
        //ASLog@"Device Activation Failed: %@",[request error]);
        [self postNotification:TVSessionManagerDeviceActivationFailedNotification error:[request error]];
    }];
    
    [request setCompletionBlock:^{
        NSDictionary *response = [request JSONResponse];
        NSNumber* DomainResponseStatus = [response objectForKey:kAPIResponseKey_DomainResponseStatusKey];
        self.currentDomain = [[[TVDomain alloc] initWithDictionary:[response objectForKey:TVDomainKey]] autorelease];
        [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
        if ([self deviceState] == TVDeviceStateActivated)
        {
            [self postNotification:TVSessionManagerDeviceActivationCompletedNotification userInfo:nil];
        }
        else
        {
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      NSLocalizedString(@"Login Error: Device Activation Failed", nil), NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceActivationFailedNotification error:error];
        }
    }];
    
    [request startAsynchronous];
}


-(void) getDomainIDFromUserDetailsWithSiteGUID : (NSString *) siteGUID
{
    ASLog2Debug(@"");
    ASLogDebug(@"[Request][SiteAPI] requestForGetUserDetails");////////
    
    __block TVPAPIRequest *request = [TVPSiteAPI requestForGetUserDetails:siteGUID delegate:self];
    [request setStartedBlock:^{
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Logging in...", nil) maskType:SVProgressHUDMaskTypeGradient];    }];
    
    [request setFailedBlock:^{
        //ASLog@"GetUserDetails Failed: %@",request.error);
        [ SVProgressHUD dismiss];
    }];
    
    [request setCompletionBlock:^{
        [ SVProgressHUD dismiss];
        NSError *error = nil;
        NSNumber *domainID = nil;
        NSDictionary *response = [request JSONResponse];
        NSDictionary *user = [response objectOrNilForKey:@"m_user"];
        self.currentUser = [[[TVUser alloc] initWithDictionary:user] autorelease];
        
        
        
        if (self.currentUser.username != nil)
        {
            [self addUserToKnownUsersList:self.currentUser];
            [self saveCurrentUser];
            [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
            
            
            
        }
        domainID = [user objectOrNilForKey:@"m_domianID"];
        [self storeSiteGUID:siteGUID domainID:domainID];
        if ([self.sharedInitObject belongsToValidDomain])
        {
            [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
            [self getDomainInfo];
        }
        else
        {
            NSString *errorTitle = NSLocalizedString(@"Dialog title: Facebook connect unavailable", nil);
            NSString *errorMessage = NSLocalizedString(@"Dialog body: Please log in using your Access ID", nil);
            
            // shefyg - added facebook error because want specific title and error
            NSDictionary * errDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:errorMessage,errorTitle,nil] forKeys:[NSArray arrayWithObjects:NSLocalizedDescriptionKey,@"userErrorTitle",nil]];
            
            error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusUserDoesNotExist userInfo:errDictionary];
        }
        
        if (error != nil)
        {
            // shefyg - changed because want specific error
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            
            
        }
    }];
    
    [request startAsynchronous];
}



#pragma mark - Notifications

-(void) postNotification:(NSString *)notification error:(NSError *)error
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:error forKey:TVErrorKey];
    [self postNotification:notification userInfo:userInfo];
}



-(void) storeSiteGUID : (NSString *) siteGUID
             domainID : (NSNumber *) domainID
{
    self.sharedInitObject.siteGUID = siteGUID;
    self.sharedInitObject.domainID = [domainID integerValue];
    if (siteGUID != nil && domainID != nil)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:siteGUID forKey:TVConfigSiteGUIDKey];
        [userDefaults setObject:domainID forKey:TVConfigDomainIDKey];
        [userDefaults synchronize];
    }
}


-(void) storeUserData : (NSDictionary *)userDataDict
{
    if (userDataDict != nil)
    {
        /** data is ready now, and you can use it **/
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userDataDict];
        if (data != nil) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:data forKey:TVUserDataKey];
            [userDefaults synchronize];
        }
    }
}

#pragma mark - Configuration

-(void) configurationReady : (NSNotification *) notification
{
    ASLogDebug(@"Cofiguration File Ready. Updating initObj...");
}

-(void) loadConsistantData : (NSNotification *) notification
{
    ASLogDebug(@"Consistant Data Loaded...");
    [self loadSharedInitObject];
    [self loadUserData];
    if ([self.sharedInitObject belongsToValidDomain])
    {
        self.appTypeUsed = TVAppType_Unknown;
        self.lastPreferedLoginType = kPreferedLoginType_Unknown;
        self.lastLoginIsForNewUser = NO;
        [self loadCurrentUser];
    }
}

-(void) registerForConfigurationNotifications
{
    [self unregisterFromConfigurationNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVConfigurationManager *mngr = [TVConfigurationManager sharedTVConfigurationManager];
    [center addObserver:self
               selector:@selector(configurationReady:)
                   name:TVConfigurationManagerDidLoadConfigurationNotification
                 object:mngr];
    [center addObserver:self
               selector:@selector(loadConsistantData:)
                   name:TVConfigurationManagerReadyToLoadConsistantData
                 object:nil];
    
}

-(void) unregisterFromConfigurationNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVConfigurationManager *mngr = [TVConfigurationManager sharedTVConfigurationManager];
    [center removeObserver:self
                      name:TVConfigurationManagerDidLoadConfigurationNotification
                    object:mngr];
    [center removeObserver:self
                      name:TVConfigurationManagerReadyToLoadConsistantData
                    object:nil];
}

#pragma mark - TVPAPIRequest Delegate -

-(void) parseSignInResponse : (TVPAPIRequest *) loginRequest
{
    NSDictionary *response = [loginRequest JSONResponse];
    NSNumber *domainID = [response objectOrNilForKey:TVConfigDomainIDKey];
    NSString *siteGUID = [response objectOrNilForKey:TVConfigSiteGUIDKey]; //TVLoginStatusCustomError
    
    NSInteger loginStatus = TVLoginStatusCustomError;
    if ([response objectOrNilForKey:@"LoginStatus"])
    {
        loginStatus = [[response objectOrNilForKey:@"LoginStatus"] integerValue];
    }
    
    self.userData = [response objectForKey:TVUserDataKey];
    
    ASLogDebug(@"Login Status: %ld",(long)loginStatus);
    ASLogDebug(@"SiteGUID: %@",siteGUID);
    ASLogDebug(@"DomainID: %@",domainID);
    
    NSString *errorMessage = nil;
    
    if (loginStatus == TVLoginStatusOK || loginStatus == TVLoginStatusDeviceNotRegistered)
    {
        if( self.assertLoginBlock && self.assertLoginBlock(response) == NO)
        {
            NSDictionary *info = [NSDictionary dictionaryWithObject:response forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusCustomError userInfo:info];
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            return;
        }
        
        [self storeSiteGUID:siteGUID domainID:domainID];
        [self storeUserData:self.userData];
        [self storeOpeningSessionDate];
        
        if ([self.sharedInitObject belongsToValidDomain])
        {
            // Report SignIn Completed
            [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
            // So far so good.
            [self getDomainInfo];
            // Not necessary for the login process, but still good to have.
            
            [self updateUserDataFromSignInResponse:loginRequest];
           // [self getCurrentUserDetails];
        }
        else
        {
            // For some reason siteGUID or DomainID is missing
            errorMessage = NSLocalizedString(@"Login Error: No SiteGUID", nil);
        }
    }
    else
    {
        errorMessage = ErrorMessageForLoginStatus(loginStatus);
    }
    
    if (errorMessage!= nil)
    {
        ASLogInfo(@"login errorMessage = %@", errorMessage);
        NSDictionary *info = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:loginStatus userInfo:info];
        [self postNotification:TVSessionManagerSignInFailedNotification error:error];
    }
}

#pragma mark - User Details -

-(void)updateUserDataFromSignInResponse : (TVPAPIRequest *) loginRequest
{
    NSDictionary *response = [loginRequest JSONResponse];

    NSDictionary *user = [response objectOrNilForKey:@"UserData"];
    if (user != nil)
    {
        self.currentUser = [[[TVUser alloc] initWithDictionary:user] autorelease];
    }
    
    if(_lastLoginIsForNewUser)
    {
        self.currentUser.preferedLoginType = _lastPreferedLoginType;
    }
    
    if (self.currentUser != nil)
    {
        [self addUserToKnownUsersList:self.currentUser];
        [self saveCurrentUser];
        [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
    }
    else
    {
        [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
    }

}

#pragma mark - Session Flags and data

-(void) addFlag:(NSString *)flag
{
    if(!_sessionFlags)
    {
        self.sessionFlags = [NSMutableArray array];
    }
    
    if(![_sessionFlags containsObject:flag])
    {
        [_sessionFlags addObject:flag];
    }
    
}

-(BOOL) hasFlag:(NSString *)flag{
    if(!_sessionFlags){
        return NO;
    }
    return [_sessionFlags containsObject:flag];
}

-(void) addAddedDataArray:(NSArray *)data forKey:(NSString *)key{
    if(!_sessionAddedData){
        self.sessionAddedData = [NSMutableDictionary dictionary];
    }
    [_sessionAddedData setObject:data forKey:key];
}

-(BOOL) hasAddedDataForKey:(NSString *)key{
    if(_sessionAddedData && [_sessionAddedData objectOrNilForKey:key] !=nil){
        return YES;
    }
    return NO;
}


-(NSArray *) takeAddedDataForKey:(NSString *)key{
    if([self hasAddedDataForKey:key]){
        return [_sessionAddedData objectOrNilForKey:key];
    }
    return nil;
}



-(NSString *) getSessionCurrency{
    
    if([self hasFlag:kSessionFlag_KANGUROO]){
        return @"CLP";
    }
    else{
        return @"UNKNOWN";
    }
    
}

#pragma mark - Singlton
SYNTHESIZE_SINGLETON_FOR_CLASS(TVSessionManager);


#pragma mark - IPNO Operators

-(void) setOprator:(TVIPNOperator *)tvoperator
{
    if (tvoperator.operatorUsername != nil && tvoperator.operatorPassword != nil)
    {
        //        self.sharedInitObject.APIUsername = tvoperator.operatorUsername;
        //        self.sharedInitObject.APIPassword = tvoperator.operatorPassword;
    }
}

-(BOOL) isUserConnectedToFacebook
{
    return self.currentUser.faceBookToken.length>0;
}


#pragma mark - dates

-(void) storeOpeningSessionDate
{
    self.lastOpeningSessionDate = [NSDate date];
}

@end




// Error Domains
NSString * const TVSignInErrorDomain = @"TVSignInErrorDomain";
NSString * const TVDeviceRegistrationErrorDomain = @"TVDeviceRegistrationErrorDomain";
NSString * const TVDeviceActivationErrorDomain = @"TVDeviceActivationErrorDomain";
NSString * const TVLogoutErrorDomain = @"TVLogoutErrorDomain";

// Facebook Login
NSString * const facebookResposeStatus = @"status";

// Device Sign In Notification
NSString * const TVSessionManagerSignInCompletedNotification = @"TVSessionManagerSignInCompletedNotification";
NSString * const TVSessionManagerSignInFailedNotification = @"TVSessionManagerSignInFailedNotification";


// Logout Notification
NSString * const TVSessionManagerLogoutCompletedNotification = @"TVSessionManagerLogoutCompletedNotification";
NSString * const TVSessionManagerLogoutFailedNotification = @"TVSessionManagerLogoutFailedNotification";

// Device Registration Notification
NSString * const TVSessionManagerDeviceRegistrationCompletedNotification = @"TVSessionManagerDeviceRegistrationCompletedNotification";
NSString * const TVSessionManagerDeviceRegistrationFailedNotification = @"TVSessionManagerDeviceRegistrationFailedNotification";
NSString * const TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification = @"TVSessionManagerDeviceRegistrationFailedWithErrorNotification";


// Device Activation Notification
NSString * const TVSessionManagerDeviceActivationCompletedNotification = @"TVSessionManagerDeviceActivationCompletedNotification";
NSString * const TVSessionManagerDeviceActivationFailedNotification = @"TVSessionManagerDeviceActivationFailedNotification";

NSString * const TVSessionManagerDeviceStateAvailableNotification = @"TVSessionManagerDeviceStateAvailableNotification";
NSString * const TVSessionManagerUserDetailsAvailableNotification = @"TVSessionManagerUserDetailsAvailableNotification";

NSString * const TVSessionManagerCurrentUserDetailsErrorNotification = @"TVSessionManagergetCurrentUserDetailsErrorNotification";



//#pragma mark - User Details
//
//-(void) getCurrentUserDetails
//{
//    NSString *siteGUID = self.sharedInitObject.siteGUID;
//    ASLogInfo(@"[Request][SiteAPI] requestForGetUserDetails");////////
//    
//    __block TVPAPIRequest *request = [TVPSiteAPI requestForGetUserDetails:siteGUID delegate:self];
//    
//    [request setStartedBlock:^{
//        ASLogDebug(@"Getting user details");
//    }];
//    
//    [request setFailedBlock:^{
//        ASLogError(request.error);
//        ASLog2Debug(@"[Request][RequestFailed] - getCurrentUserDetails");////////
//        
//        [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
//        
//        
//        
//    }];
//    
//    [request setCompletionBlock:
//     ^{
//         if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
//         {
//             NSDictionary *response = [request JSONResponse];
//             NSDictionary *user = [response objectOrNilForKey:@"m_user"];
//             self.currentUser = [[[TVUser alloc] initWithDictionary:user] autorelease];
//             
//             if(_lastLoginIsForNewUser){
//                 self.currentUser.preferedLoginType = _lastPreferedLoginType;
//             }
//             
//             
//             if (self.currentUser != nil)
//             {
//                 [self addUserToKnownUsersList:self.currentUser];
//                 [self saveCurrentUser];
//                 [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
//             }else{
//                 [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
//                 
//             }
//             
//         }
//     }];
//    
//    [request startAsynchronous];
//}