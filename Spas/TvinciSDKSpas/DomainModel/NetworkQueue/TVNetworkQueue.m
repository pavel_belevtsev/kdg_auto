//
//  TVNetworkQueu.m
//  TVinci
//
//  Created by Avraham Shukron on 6/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVNetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "SmartLogs.h"
#import "SmartLogs.h"
#import "TVConfigurationManager.h"


#import "TVPAPIRequest.h"


@interface TVNetworkQueue ()

@property (retain, nonatomic) NSMutableArray * arrayRequests;

@end

@implementation TVNetworkQueue

-(instancetype)init
{
    if (self = [super init])
    {
        self.arrayRequests = [NSMutableArray array];
    }
    
    return self;
}


#pragma mark - Memory
-(void) dealloc
{
    [self cleen];
    [super dealloc];
}


-(void)cleen
{
    for (TVPAPIRequest * request in self.arrayRequests)
    {
        [request cancel];
    }
    
    [self.arrayRequests removeAllObjects];
    
}


#pragma mark - Asynchronic Dangerous Stuff
-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        if ([[TVConfigurationManager sharedTVConfigurationManager]checkConfigLifeCycle])
        {
            request.requestDelegate = self;
            [request sendRequest];
        }else{
            [self performSelector:@selector(sendRequest:) withObject:request afterDelay:1.0];
        }
    }
}


-(void) requestDidStart:(TVPAPIRequest *) request
{
    [self.arrayRequests addObject:request];
}

-(void) requestDidFinish:(TVPAPIRequest *) request
{
    [self.arrayRequests removeObject:request];
}


@end
