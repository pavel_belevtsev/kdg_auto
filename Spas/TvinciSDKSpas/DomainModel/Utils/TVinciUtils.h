//
//  TVinciUtils.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/16/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVConstants.h"

typedef void (^SimpleBlock)();
typedef void (^SimpleErrorBlock)(NSError *error);

NSString *TVPictureSizeStringFromCGSize(CGSize size);
NSDictionary * parsArrayOfPairsToOneDictionary(NSArray * array);
NSDictionary * parsArrayOfPairsKeyValueToOneDictionary(NSArray * array);


#define APPLICATION_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey]


#define kNotificationName_RecentUserSignInFailed @"kNotificationName_RecentUserSignInFailed"

#define kNotificationKey_rightContentReady @"kNotificationKey_rightContentReady"
#define kNotificationKey_SignInFailedError @"kNotificationKey_SignInFailedError"
#define kNotificationKey_StartEditFaivorites @"kNotificationKey_StartEditFaivorites"
#define kNotificationKey_StopEditFaivorites @"kNotificationKey_StopEditFaivorites"