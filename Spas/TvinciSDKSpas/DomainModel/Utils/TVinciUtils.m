//
//  TVinciUtils.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/16/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVinciUtils.h"

NSString *const CGSizeStringFormat = @"%dX%d";

NSString *TVPictureSizeStringFromCGSize(CGSize size)
{
    NSString *sizeString = [NSString stringWithFormat:CGSizeStringFormat , (int)size.width , (int)size.height];
    if (CGSizeEqualToSize(size, CGSizeZero))
    {
        // Cannot download full-size images in iphone, they'r way too big
        //return TVImageSizeFull;
    }
    return sizeString;
}


NSDictionary * parsArrayOfPairsToOneDictionary(NSArray * array)
{
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    for (NSDictionary * pair in array)
    {
        NSString * theKey = [pair.allKeys lastObject];
        [dictionary setObject:[pair objectForKey:theKey] forKey:theKey];
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}




#define kKey  @"key"
#define kValue  @"value"
NSDictionary * parsArrayOfPairsKeyValueToOneDictionary(NSArray * array)
{
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    for (NSDictionary * pair in array)
    {
        NSString * theKey = [pair objectForKey:kKey];
        NSString * theValue = [pair objectForKey:kValue];
        [dictionary setObject:theValue forKey:theKey];
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


