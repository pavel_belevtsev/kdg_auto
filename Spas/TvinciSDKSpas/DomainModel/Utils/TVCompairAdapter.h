//
//  TVCompairAdapter.h
//  TvinciSDK
//
//  Created by Tarek Issa on 5/5/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TVDeviceFamily;


@interface TVCompairAdapter : NSObject

/*!
 *  Retrns an array of TVCompanionDevice's from 'devicesArray'  and 'stbDeviceFamily', that have in common.
 *
 *  @param devicesArray         - Array of  TVCompanionDevice's.
 *  @param deviceFamiliesArray  - Array of TVDeviceFamily which include a list of devices-udids.
 */
+ (BOOL)UDIDManipulationFor:(NSString *)udidOne and:(NSString *)udidTwo;

@end
