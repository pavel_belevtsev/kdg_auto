//
//  TvinciKeychainWrapper.m
//  PersistenceID
//
//  Created by Rivka S. Peleg on 2/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "TvinciKeychainWrapper.h"
#import <Security/Security.h>


@implementation TvinciKeychainWrapper

static NSString *serviceName = @"com.tvinci";


+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier sync:(BOOL) sync
{
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrAccount];
    [searchDictionary setObject:serviceName forKey:(id)kSecAttrService];
    [searchDictionary setObject:(id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(id)kSecAttrAccessible];
    
    if (sync)
    {
        [searchDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecAttrSynchronizable];
    }
    
    return searchDictionary;
}

+ (NSString *)searchKeychainCopyMatching:(NSString *)identifier sync:(BOOL) sync {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    
    // Add search attributes
    [searchDictionary setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    
    NSData *result = nil;
    /*OSStatus status = */ SecItemCopyMatching((CFDictionaryRef)searchDictionary,
                                               (CFTypeRef *)&result);
    
    [searchDictionary release];
    
    //NSLog(@"%d",(int)status);
    NSString * resultString = nil;
    if (result)
    {
        resultString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    }
    
    
    return [resultString autorelease];
}

+ (BOOL)createKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier sync:sync];
    
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:valueData forKey:(id)kSecValueData];
    
    OSStatus status = SecItemAdd((CFDictionaryRef)dictionary, NULL);
    [dictionary release];
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (BOOL)updateKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [updateDictionary setObject:valueData forKey:(id)kSecValueData];
    
    OSStatus status = SecItemUpdate((CFDictionaryRef)searchDictionary,
                                    (CFDictionaryRef)updateDictionary);
    
    [searchDictionary release];
    [updateDictionary release];
    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (void)deleteKeychainValue:(NSString *)identifier sync:(BOOL) sync {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    SecItemDelete((CFDictionaryRef)searchDictionary);
    [searchDictionary release];
}


+ (BOOL)setKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync
{
    NSString * result = [TvinciKeychainWrapper searchKeychainCopyMatching:identifier sync:sync];
    if (result == nil)
    {
        return [TvinciKeychainWrapper createKeychainValue:value forIdentifier:identifier sync:sync];
    }
    else
    {
        return [TvinciKeychainWrapper updateKeychainValue:value forIdentifier:identifier sync:sync];
    }
}

@end
