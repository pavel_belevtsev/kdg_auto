//
//  TvinciKeychainWrapper.h
//  PersistenceID
//
//  Created by Rivka S. Peleg on 2/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TvinciKeychainWrapper : NSObject

+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier sync:(BOOL) sync;
+ (NSString *)searchKeychainCopyMatching:(NSString *)identifier sync:(BOOL) sync;
+ (BOOL)createKeychainValue:(NSString *) value forIdentifier:(NSString *)identifier sync:(BOOL) sync;
+ (BOOL)updateKeychainValue:(NSString *) value forIdentifier:(NSString *)identifier sync:(BOOL) sync;
+ (void)deleteKeychainValue:(NSString *) identifier sync:(BOOL) sync;
+ (BOOL)setKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier sync:(BOOL) sync;

@end
