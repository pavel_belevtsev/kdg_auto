//
//  TVUniqueIDPovider.m
//  TvinciSDK
//
//  Created by Israel on 2/20/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVUniqueIDPovider.h"
#import "TvinciUDID.h"
#import "OpenUDID.h"
#import "SmartLogs.h"


@implementation TVUniqueIDPovider

static NSString * temporaryUDID = nil;

+(void) setTemporaryPredefinedUDID:(NSString *) udidValue
{
    temporaryUDID = udidValue;
}
+(NSString *)getUniqueIDByType:(UDIDType)udidType
{
    switch (udidType)
    {
        case OpenUDIDType:
        {
            
            ASLogInfo(@"OpenUDIDUniqueKey = %@",[OpenUDID value]);
            return [OpenUDID value];
        }
            break;
        case TvinciUDIDType:
        {
            ASLogInfo(@"TvinciUDIDUniqueKey = %@",[TvinciUDID value]);
            return [TvinciUDID value];
        }
            break;
        case TemporaryPredefinedUDIDType:
            return temporaryUDID;
            break;
        default:
            ASLogInfo(@"OpenUDIDUniqueKey = %@",[OpenUDID value]);
            return [OpenUDID value];;
    }
}

@end
