#import "TVConstants.h"

#import <math.h>

NSString * const TVErrorKey = @"TVSessionManagerErrorKey";
NSString * const TVImageSizeFull = @"full";

NSString * const TVPageLayoutHome = @"Home";
NSString * const TVPageLayoutMyZone = @"MyZone";
NSString * const TVPageLayoutUserDetailLabel = @"UserDetailLabel";


NSString * const TVPageLayoutAllMedias = @"AllMedias";
NSString * const TVPageLayoutVOD = @"VOD";
NSString * const TVPageLayoutLogOut = @"Log Out";
NSString * const TVPageLayoutSignIn = @"Sign In";
NSString * const TVPageLayoutEPG = @"EPG";
NSString * const TVPageLayoutMovies = @"Movies";
NSString * const TVPageLayoutShows = @"Shows";
NSString * const TVPageLayoutRecordings = @"Recordings";
NSString * const TVPageLayoutLive = @"Live";
NSString * const TVPageLayoutLiveTV = @"LiveTV";
NSString * const TVPageLayoutSpotlight2 = @"Spotlight + 2";
NSString * const TVPageLayoutSpotlight4 = @"Spotlight + 4";
NSString * const TVPageLayoutSpotlight2X3P4 = @"Spotlight 2:3 + 4 items";


NSString * const TVPageLayoutGrid2x2 = @"Grid 2X2";
NSString * const TVPageLayoutGrid3x3 = @"Grid 3x3";

NSString *const TVNotificationName_AudioLanguageChanged = @"TVNotificationName_AudioLanguageChanged";
NSString *const TVNotificationName_BackPressedOnCustomIPhoneNav = @"TVNotificationName_BackPressedOnCustomIPhoneNav";

NSString *const kUserInfoKey_AudioLanguageSelectedIndex = @"kUserInfoKey_AudioLanguageSelected";

NSString *const kPackageInfoKey_isRenewing = @"kPackageInfoKey_isRenewing";
NSString *const kPackageInfoKey_endDate = @"kPackageInfoKey_endDate";

NSString * const TVNotificationName_MasterAboutToRevealContent = @"TVNotificationName_MasterAboutToRevealContent";
//NSString * const TVNotificationInfoKey_UIGestureRecognizerStateBegan
NSString * const TVNotificationName_MasterAboutUIGestureRecognizerStateBegan = @"TVNotificationName_MasterAboutUIGestureRecognizerStateBegan";

NSString * const TVNotificationInfoValue_RevealLeft = @"TVNotificationInfoValue_RevealLeft";
NSString * const TVNotificationInfoValue_RevealRight = @"TVNotificationInfoValue_RevealRight";
NSString * const TVNotificationInfoValue_RevealTop = @"TVNotificationInfoValue_RevealTop";
NSString * const TVNotificationInfoValue_RevealBottom = @"TVNotificationInfoValue_RevealBottom";
NSString * const TVNotificationInfoValue_RevealCenter = @"TVNotificationInfoValue_RevealCenter";
NSString * const TVNotificationInfoKey_RevealSide = @"TVNotificationInfoKey_RevealSide";
NSString * const TVNotificationInfoKey_LastContentPosition = @"TVNotificationInfoKey_LastContentPosition";
NSString * const TVNotificationInfoKey_UIGestureRecognizerStateBegan = @"TVNotificationInfoKey_UIGestureRecognizerStateBegan";


NSString * const TVNotificationName_AppWillResigneActive = @"TVNotificationName_AppWillResigneActive";
NSString * const TVNotificationName_AppDidBecomeActive = @"TVNotificationName_AppDidBecomeActive";
NSString * const kNotificationName_WillCloseLiteBox = @"kNotificationName_WillCloseLiteBox";
NSString *const kUserInfoKey_LiteBoxClosingType = @"kUserInfoKey_LiteBoxClosingType";
NSString *const kUserInfoValue_LiteBoxClosingType_Close = @"kUserInfoValue_LiteBoxClosingType_Close";
NSString *const kUserInfoValue_LiteBoxClosingType_PlayFull = @"kUserInfoValue_LiteBoxClosingType_PlayFull";
NSString *const kUserInfoValue_LiteBoxClosingType_PlayTrailer = @"kUserInfoValue_LiteBoxClosingType_PlayTrailer";

NSString * const TVNotificationName_LiveMediaCreated = @"TVNotificationName_LiveMediaCreated";

NSString * const TVNotificationCompanionDevicePaired = @"TVNotificationName_CompanionDevicePaired";
NSString * const TVNotificationCompanionDeviceUnPaired = @"TVNotificationName_CompanionDeviceUnPaired";

// myZoonOpenNow
NSString *const kUserInfo_myZoonOpenNow = @"myZoonOpenNow";

NSString *const kUserInfoValue_TVMediaItemViewController_back = @"kUserInfoValue_TVMediaItemViewController_back";

NSString * const TVNotificationName_FinishedMediaPlayOnFullScreenWithCustom = @"TVNotificationName_FinishedMediaPlayOnFullScreenWithCustom";
NSString * const TVNotificationName_FinishedSeeingMovie = @"TVNotificationName_FinishedSeeingMovie";
NSString * const TVNotificationName_myAppDidBecomeActive = @"TVNotificationName_myAppDidBecomeActive";


NSString * const kNotificationName_HomePageLoadPage = @"kNotificationName_HomePageLoadPage";

// gestures with 2 fingures

NSString * const kNotificationName_SwipedUpWith2Fingures = @"kNotificationName_SwipedUpWith2Fingures";
NSString * const kNotificationName_SwipedDownWith2Fingures = @"kNotificationName_SwipedDownWith2Fingures";




NSString * const kSessionFlag_KANGUROO = @"KANGUROO";

NSString * const kSessionFlag_AppForIphone = @"kSessionFlag_AppForIphone";

NSString * const kSessionFlag_UsingOmniture = @"kSessionFlag_UsingOmniture";
NSString * const kSessionFlag_UsingGoogleAnalitics = @"kSessionFlag_UsingGoogleAnalitics";
NSString * const kSessionFlag_ShowSwipesInstructions = @"kSessionFlag_ ShowSwipesInstructions";
NSString * const kSessionFlag_UseDeviceFamilyID_Tablet = @"kSessionFlag_UseDeviceFamilyID_Tablet";
NSString * const kSessionFlag_UseDeviceFamilyID_Smartphone = @"kSessionFlag_UseDeviceFamilyID_Smartphone";

NSString * const kSessionFlag_UseDeviceFamilyForToggle = @"kSessionFlag_UseDeviceFamilyForToggle";

NSString * const kSessionFlag_ToggleDevice = @"kSessionFlag_ToggleDevice";

NSString * const kSessionFlag_EnableBrowseMode = @"kSessionFlag_EnableBrowseMode";
NSString * const kSessionFlag_BrowseModeKeepingLocationWhenLogingOutOrIn = @"kSessionFlag_IsBrowseModeKeepingLocationWhenLogingOutOrIn";

NSString * const kSessionFlag_UseNavLogo = @"kSessionFlag_UseNavLogo";
NSString * const kSessionFlag_UseNavDarkBG = @"kSessionFlag_UseNavDarkBG";
NSString * const kSessionFlag_UseNavDarkBackBt = @"kSessionFlag_UseNavDarkBackBt";

NSString * const kSessionFlag_UseDefaultLoginLogoutIconNames = @"kSessionFlag_UseDefaultLoginLogoutIconNames";

NSString * const kSessionFlag_UseStyleOrange = @"kSessionFlag_UseStyleOrange";
NSString * const kSessionFlag_UseBusinessModelForSpotlightTag = @"kSessionFlag_UseBusinessModelForSpotlightTag";
NSString * const kSessionFlag_UseSplash4inchsImage = @"kSessionFlag_UseSplash4inchsImage";
NSString * const kSessionFlag_IgnoreConcurent = @"kSessionFlag_IgnoreConcurent";

NSString * const kSessionFlag_UsingStartOver = @"kSessionFlag_UsingStartOver";

NSString * const kSessionFlag_AppLanguage_Germen = @"kSessionFlag_AppLanguage_Germen";
NSString * const kSessionFlag_CompanionModeEnabled = @"kSessionFlag_CompanionModeEnabled";


// Session flags for spotlight
NSString * const kSessionFlag_SpotlightUsePromotionText = @"kSessionFlag_SpotlightUsePromotionText";



// Session flags for player (MediaItem)
NSString * const kSessionFlag_MediaPlayer_SkipCheckingRules = @"kSessionFlag_MediaPlayer_SkipCheckingRules";


// Session flags for PRESENTATION

NSString * const  kSessionFlag_PRESENTATION_replaceLiveURL = @"kSessionFlag_PRESENTATION_replaceLiveURL";

NSString * const  kSessionFlag_PRESENTATION_replaceVodUrlStart = @"kSessionFlag_PRESENTATION_replaceVodUrlStart";



NSString * const kSessionAddedData_RandomSessionID = @"kSessionAddedData_RandomSessionID";
NSString * const kSessionAddedData_CustomLiveURL = @"kSessionAddedData_CustomLiveURL";
NSString * const kSessionAddedData_CustomVodUrlStartReplace = @"kSessionAddedData_CustomVodUrlStartReplace";
NSString * const kSessionAddedData_4InchesSplashImageName = @"kSessionAddedData_4InchesSplashImageName";
NSString * const kSessionAddedData_4InchesSplashImagePositionY = @"kSessionAddedData_4InchesSplashImageOffsetY";
NSString * const kSessionAddedData_PlayerDoneBtContentEdgeInsets = @"kSessionAddedData_PlayerDoneBtContentEdgeInsets";
NSString * const kSessionAddedData_MediaItemKey_iPhoneTrailer = @"kSessionAddedData_MediaItemKey_iPhoneTrailer";

NSString * const TVNotificationInfoKey_WillAnimateRotation = @"WillAnimateRotationToInterfaceOrientation";

NSString * const kCellInfoDictKey_titleFrame_originX = @"kCellInfoDictKey_titleFrame_originX";
NSString * const kCellInfoDictKey_titleFrame_originY = @"kCellInfoDictKey_titleFrame_originY";
NSString * const kCellInfoDictKey_titleIsHidden = @"kCellInfoDictKey_titleIsHidden";
NSString * const kCellInfoDictKey_SeconderyTitleFrame_originY = @"kCellInfoDictKey_SeconderyTitleFrame_originY";
NSString * const kCellInfoDictKey_titleFrame_sizeHeight = @"kCellInfoDictKey_titleFrame_sizeHeight";
NSString * const kCellInfoDictKey_addFrameToSelectedCell = @"kCellInfoDictKey_addFrameToSelectedCell";
NSString * const kCellInfoDictKey_bgColorForTitle = @"kCellInfoDictKey_bgColorForTitle";
NSString * const kCellInfoDictKey_titleFrame_numberOfLines = @"kCellInfoDictKey_titleFrame_numberOfLines";
NSString * const kCellInfoDictKey_ImageSize = @"kCellInfoDictKey_ImageSize";
NSString * const kCellInfoDictKey_tileIsAnimated = @"kCellInfoDictKey_tileIsAnimated";
NSString * const kCellInfoDictKey_SelectedFrameHeight = @"kCellInfoDictKey_SelectedFrameHeight";
NSString * const kCellInfoDictKey_SelectedPictureFrameHeight = @"kCellInfoDictKey_SelectedPictureFrameHeight";

NSString * const kItemInfoDictKey_nibName = @"kItemInfoDictKey_nibName";
NSString * const kItemInfoDictKey_pictureSizeName = @"kItemInfoDictKey_pictureSizeName";
NSString * const kItemInfoDictKey_ShowDesription = @"kItemInfoDictKey_ShowDesription";

NSString *const TVNotificationName_LoginApper = @"TVNotificationName_LoginApper";

NSString * const TVNotificationInfo_MenuComplateDownlaud = @"TVNotificationInfo_MenuComplateDownlaud";
/**************
 For user defaults
 *****************/
NSString *const kUserDefaultsKey_guestSeenSwipesInstructions = @"kUserDefaultsKey_guestSeenSwipesInstructions";
NSString *const kUserDefaultsKey_liveMinutesOffset = @"kUserDefaultsKey_liveMinutesOffset";
NSString *const kUserDefaultsKey_companionModeDeviceIP = @"kUserDefaultsKey_companionModeDeviceIP";

/*!
 The String value for each enum defined in TVPSiteAPI.h  numberOfLines
 */


NSString * TVNameForSocialAction(TVSocialAction action)
{
    static NSString *const SocialActionsNames[] = {@"UNKNOWN",@"LIKE",@"UNLIKE",@"SHARE",@"POST",@"WATCHES",@"RATES",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(SocialActionsNames)/sizeof(SocialActionsNames[0]));
    if (action < length)
    {
        name = SocialActionsNames[action];
    }
    return name;
}


NSString *TVNameForMediaAction(TVMediaAction action)
{
    static NSString *const MediaActionNames[] = {@"Rate",@"Vote",@"Recommend",@"Share",@"AddFavorite",@"RemoveFavorite",@"Comment",@"Record",@"Reminder",@"Watch",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(MediaActionNames)/sizeof(MediaActionNames[0]));
    if (action < length)
    {
        name = MediaActionNames[action];
    }
    return name;
}

NSString *TVNameForMediaPlayerAction(TVMediaPlayerAction action)
{
    static NSString *const MediaActionNames[] = {@"none",@"stop",@"finish",@"pause",@"play",@"first_play",@"load",@"bitrate_change",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(MediaActionNames)/sizeof(MediaActionNames[0]));
    if (action < length)
    {
        name = MediaActionNames[action];
    }
    return name;
}

NSString *TVNameForSocialPlatform(TVSocialPlatform platform)
{
    static NSString *const SocialPlatformsNames[] = {@"UNKNOWN",@"FACEBOOK",@"GOOGLE",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(SocialPlatformsNames)/sizeof(SocialPlatformsNames[0]));
    if (platform < length)
    {
        name = SocialPlatformsNames[platform];
    }
    return name;
}

NSString *TVNameForOrderBy(TVOrderBy orderBy)
{
    static NSString *const OrderByValueNames[] = {@"None",@"Added",@"Views",@"Rating",@"ABC",@"Meta",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderByValueNames)/sizeof(OrderByValueNames[0]));
    if (orderBy < length)
    {
        name = OrderByValueNames[orderBy];
    }
    return name;
}

NSString *TVNameForMultiFilterOrderBy(TVMultiFilterOrderBy orderBy)
{
    static NSString *const OrderByValueNames[] = {@"ID",@"RELATED",@"META",@"NONE",@"VOTES_COUNT",@"CREATE_DATE",@"NAME",@"START_DATE",@"LIKE_COUNTER",@"RATING",@"VIEWS",@"RANDOM",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderByValueNames)/sizeof(OrderByValueNames[0]));
    if (orderBy < length)
    {
        name = OrderByValueNames[orderBy];
    }
    return name;
}

NSString *TVNameForOrderByAndOrList(TVOrderByAndOrList orderBy)
{
    static NSString *const OrderByValueNames[] = {@"NONE", @"ID", @"RELATED", @"META", @"VOTES_COUNT", @"CREATE_DATE", @"NAME", @"START_DATE", @"LIKE_COUNTER", @"RATING", @"VIEWS" , @"RANDOM" ,nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderByValueNames)/sizeof(OrderByValueNames[0]));
    if (orderBy < length)
    {
        name = OrderByValueNames[orderBy];
    }
    return name;
}


NSString *TVNameForCutWith(TVCutWith orderBy)
{
    static NSString *const OrderByValueNames[] = {@"OR",@"AND",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderByValueNames)/sizeof(OrderByValueNames[0]));
    if (orderBy < length)
    {
        name = OrderByValueNames[orderBy];
    }
    return name;
}

NSString *TVNameForChannelMFOrderDirection(TVChannelMFOrderDirection orderDirection)
{
    static NSString *const OrderDirectionValueNames[] = {@"ASC",@"DESC",@"NONE",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderDirectionValueNames)/sizeof(OrderDirectionValueNames[0]));
    if (orderDirection < length)
    {
        name = OrderDirectionValueNames[orderDirection];
    }
    return name;
}
NSString *TVNameForOrderDirection(TVOrderDirection orderDirection)
{
    static NSString *const OrderDirectionValueNames[] = {@"",@"Asc",@"desc",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderDirectionValueNames)/sizeof(OrderDirectionValueNames[0]));
    if (orderDirection < length)
    {
        name = OrderDirectionValueNames[orderDirection];
    }
    return name;
}


NSString *TVNameForOrderDirectionByAndOrList(TVOrderDirectionByAndOrList orderDirection)
{
    static NSString *const OrderDirectionValueNames[] = {@"",@"ASC",@"DESC",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(OrderDirectionValueNames)/sizeof(OrderDirectionValueNames[0]));
    if (orderDirection < length)
    {
        name = OrderDirectionValueNames[orderDirection];
    }
    return name;
}


NSString *TVNameForSocialPrivacy(TVUserActionPrivacy privacy)
{
    static NSString *const ActionPrivacyNames[] = {@"UNKNOWN",@"EVERYONE",@"ALL_FRIENDS",@"FRIENDS_OF_FRIENDS",@"SELF",@"CUSTOM",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(ActionPrivacyNames)/sizeof(ActionPrivacyNames[0]));
    if (privacy < length)
    {
        name = ActionPrivacyNames[privacy];
    }
    return name;
}

NSString *TVNameForUserPrivacy(TVUserPrivacy privacy) {
    static NSString *const ActionPrivacyNames[] = {@"UNKNOWN",@"ALLOW",@"DONT_ALLOW",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(ActionPrivacyNames)/sizeof(ActionPrivacyNames[0]));
    if (privacy < length)
    {
        name = ActionPrivacyNames[privacy];
    }
    return name;
    
}

NSString *TVNameForEPGCommentBy(TVEPGCommentType commentType) {
    static NSString *const CommentTypeNames[] = {@"All",@"Users",@"Reviews",@"News",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(CommentTypeNames)/sizeof(CommentTypeNames[0]));
    if (commentType < length)
    {
        name = CommentTypeNames[commentType];
    }
    return name;
    
}

NSString *TVNameForWatchListItemTypeBy(TVListItemType itemType){
    static NSString *const ItemTypeNames[] = {@"All",@"Media",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(ItemTypeNames)/sizeof(ItemTypeNames[0]));
    if (itemType < length)
    {
        name = ItemTypeNames[itemType];
    }
    return name;
    
}

NSString *TVNameForWatchListTypeBy(TVListType listType) {
    static NSString *const ItemTypeNames[] = {@"All",@"Watch",@"Purchase",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(ItemTypeNames)/sizeof(ItemTypeNames[0]));
    if (listType < length)
    {
        name = ItemTypeNames[listType];
    }
    return name;
    
}

NSString *TVNameForEPGLicensedLinkBy(EPGLicensedLinkFormatType format) {
    static NSString *const FormatTypeNames[] = {@"Catchup",@"StartOver",@"LivePause",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(FormatTypeNames)/sizeof(FormatTypeNames[0]));
    if (format < length)
    {
        name = FormatTypeNames[format];
    }
    return name;
    
}



NSString *ErrorMessageForLoginStatus(TVLoginStatus status)
{
    NSString *error = nil;
    switch (status)
    {
        case TVLoginStatusOK:
            break;
        case TVLoginStatusUserExists:
            error = NSLocalizedString(@"Login Error: User Already Exists", nil);
            break;
        case TVLoginStatusDeviceNotRegistered:
            error = NSLocalizedString(@"Login Error: Device Not Registered", nil);
            break;
        case TVLoginStatusInsideLockTime:
            error = NSLocalizedString(@"Login Error: Inside Lock Time", nil);
            break;
        case TVLoginStatusNotImplementedYet:
            error = NSLocalizedString(@"Login Error: Not Implemented Yet", nil);
            break;
        case TVLoginStatusSessionLoggedOut:
            error = NSLocalizedString(@"Login Error: Not Implemented Yet", nil);
            break;
        case TVLoginStatusUserAllreadyLoggedIn:
            error = NSLocalizedString(@"Login Error: Session Logged Out", nil);
            break;
        case TVLoginStatusUserDoubleLogIn:
            error = NSLocalizedString(@"Login Error: Double Log In", nil);
            break;
        case TVLoginStatusUserDoesNotExist:
            error = NSLocalizedString(@"Login Error: User Does Not Exist", nil);
            break;
        case TVLoginStatusUserNotActivated:
            error = NSLocalizedString(@"Login Error: Account Not Activated", nil);
            break;
        case TVLoginStatusWrongPasswordOrUserName:
            error = NSLocalizedString(@"Login Error: Wrong Password Or Username", nil);
            break;
        case TVLoginStatusUserRemovedFromDomain:
            error = NSLocalizedString(@"Login Error: User Removed From Domain", nil);
            break;
        case TVLoginStatusErrorOnSaveUser:
            error = NSLocalizedString(@"Login Error: Error On Save User", nil);
            break;
        case TVLoginStatusUserNotMasterApproved:
            error = NSLocalizedString(@"Login Error: User Not Master Approved", nil);
            break;
        case TVLoginStatusUserTypeNotExist:
            error = NSLocalizedString(@"Login Error: User Type Not Exist", nil);
            break;
        case TVLoginStatusErrorOnUpdatingUserType:
            error = NSLocalizedString(@"Login Error: Error On Updating User Type", nil);
            break;
        case TVLoginStatusUserEmailAlreadyExists:
            error = NSLocalizedString(@"Login Error: User Email Already Exists", nil);
            break;
        case TVLoginStatusCustomError:
            error = NSLocalizedString(@"Login Error: Custom Error", nil);
            break;
        default:
            error = @"";
            break;
    }
    return error;
}

static NSString *const UserItemNames[] = {@"Rental",@"Subscription",@"Package",@"Favorite",@"All",@"Recommended",nil};
NSString * TVNameForUserItemType (TVUserItemType type )
{
    NSString * name = nil;
    NSInteger length = (sizeof(UserItemNames)/sizeof(UserItemNames[0]));
    if (type < length)
    {
        name = UserItemNames[type];
    }
    return name;
}

static NSString *const NotificationTypeNames[] = {@"All",@"Push",@"Alert",@"Live",@"Pull",@"All",nil};
NSString * TVNameForNotificationType (TVNotificationType type)
{
    NSString * name = nil;
    NSInteger length = (sizeof(NotificationTypeNames)/sizeof(NotificationTypeNames[0]));
    if (type < length)
    {
        name = NotificationTypeNames[type];
    }
    return name;
}


static NSString *const NotificationViewStatusNames[] = {@"Unread",@"Read",@"All",nil};
NSString * TVNameForNotificationViewStatus (TVNotificationViewStatus type)
{
    NSString * name = nil;
    NSInteger length = (sizeof(NotificationViewStatusNames)/sizeof(NotificationViewStatusNames[0]));
    if (type < length)
    {
        name = NotificationViewStatusNames[type];
    }
    return name;
}



static NSString *const FacebookStatusNames[] = {@"UNKNOWN",@"OK",@"ERROR",@"NOACTION",@"NOTEXIST",@"CONFLICT",@"MERGE",@"MERGEOK",@"NEWUSER",@"MINFRIENDS",@"INVITEOK",@"INVITEERROR",@"ACCESSDENIED",@"WRONGPASSWORDORUSERNAME",nil};

NSString * TVNameForFacebookStatus (TVFacebookStatus status)
{
    NSString * name = nil;
    NSInteger length = (sizeof(FacebookStatusNames)/sizeof(FacebookStatusNames[0]));
    if (status < length)
    {
        name = FacebookStatusNames[status];
    }
    return name;
}


TVFacebookStatus TVFacebookStatusForName (NSString * name)
{
    TVFacebookStatus status = TVFacebookStatusUNKNOWN;
    NSInteger length = (sizeof(FacebookStatusNames)/sizeof(FacebookStatusNames[0]));
    
    for (int i =0 ; i<length ; i++)
    {
        if ([FacebookStatusNames[i] isEqualToString:name])
        {
            status = i;
            break;
        }
    }
    
    return status;
}


static NSString *const userActionNames[] = {@"UNKNOWN",@"LIKE",@"UNLIKE",@"SHARE",@"POST",@"WATCHES",@"WANTS_TO_WATCH",@"RATES",@"FOLLOWS",@"UNFOLLOW",nil};

NSString * TVNameForUserAction (TVUserSocialActionType actions)
{
    NSString * name = nil;
    NSInteger length = (sizeof(userActionNames)/sizeof(userActionNames[0]));
    NSInteger index = (NSInteger)log2(actions);
    if (index < length)
    {
        name = userActionNames[index];
    }
    return name;
    
}

static NSString *const assetTypesNames[] = {@"UNKNOWN",@"MEDIA",@"PROGRAM",@"EPG",nil};

NSString * TVNameForAssetType (TVAssetType assetType)
{
    NSString * name = nil;
    NSInteger length = (sizeof(assetTypesNames)/sizeof(assetTypesNames[0]));
    if (assetType < length)
    {
        name = assetTypesNames[assetType];
    }
    return name;
    
}

static NSString *const SocialActionResponseStatusNames[] = {@"UNKNOWN",@"OK",@"ERROR",@"UNKNOWN_ACTION",@"INVALID_ACCESS_TOKEN",@"INVALID_PLATFORM_REQUEST",@"MEDIA_DOESNT_EXISTS",@"MEDIA_ALREADY_LIKED",@"INVALID_PARAMETERS", @"USER_DOES_NOT_EXIST", @"NO_FB_ACTION", @"EMPTY_FB_OBJECT_ID", @"MEDIA_ALEADY_FOLLOWED", @"CONFIG_ERROR", @"MEDIA_ALREADY_RATED", @"NOT_ALLOWED",nil};

NSString * TVNameForSocialActionResponseStatus (TVSocialActionResponseStatus reponseStatus)
{
    NSString * name = nil;
    NSInteger length = (sizeof(SocialActionResponseStatusNames)/sizeof(SocialActionResponseStatusNames[0]));
    if (reponseStatus < length)
    {
        name = SocialActionResponseStatusNames[reponseStatus];
    }
    return name;
    
}


static NSString *const RecommendationGalleryTypeNames[] = {@"HomeVODPersonal",@"HomeVODPromotions",@"HomeLivePersonal",@"HomeLivePromotions",@"CatalogVODPromotions",@"CategoryCatalogVODPromotions",@"MovieRelated",@"SeriesRelated",@"EpisodeRelated",@"PersonalRecommendationsVOD",@"PersonalRecommendationsLive",@"EndOfMovie",@"EndOfEpisode",@"VODAction",@"VODDrama",@"VODComedy",@"VODThriller",@"VODDoco",@"VODKids",nil};

NSString * TVNameForRecommendationGalleryType (TVRecommendationGalleryType galleryType)
{
    NSString * name = nil;
    NSInteger length = (sizeof(RecommendationGalleryTypeNames)/sizeof(RecommendationGalleryTypeNames[0]));
    if (galleryType < length)
    {
        name = RecommendationGalleryTypeNames[galleryType];
    }
    return name;
    
}

