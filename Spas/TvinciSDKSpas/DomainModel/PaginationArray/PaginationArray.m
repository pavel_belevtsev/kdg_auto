//
//  PaginationArray.m
//  TvinciSDK
//
//  Created by iosdev1 on 1/31/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//


#import "PaginationArray.h"

#define NONE -1
#define Empty 0
#define PreparingToDownload 1
#define DownloadSuccessful 2
@interface PaginationArray ()
@property (nonatomic,retain) NSString * pageCollectionKey;
@property (nonatomic,retain) NSString * lastPageKey;
@property (nonatomic,retain) NSString * mediaItemsKey;
@end

@implementation PaginationArray

#pragma mark - memory

-(void)dealloc
{
    [_pageCollection release];
    [_className release];
    [_mediaItems release];
    [_pageCollectionKey release];
    [_lastPageKey release];
    [_mediaItemsKey release];
    [super dealloc];
}

-(void)ClearAllParameters
{
    [self initPageCollection];
    self.lastDownloadPage=0;
}

#pragma mark - init


-(id)initWithClassName:(NSString*)theClassName andChannelId:(NSInteger)unickNum andMinimumPage:(NSInteger)minPage
{
    if (self = [super init])
    {
        self.numOfMinumumPageToDownload = minPage;
        self.numberOfPages = 0;
        self.currMainDownloadPage = 0;
        self.lastDownloadPage =0;
        self.className = theClassName;
        self.unickKey = unickNum;
        self.pageCollectionKey = [NSString stringWithFormat:@"pageCollectionKey_%@_%d",theClassName,unickNum];
        self.lastPageKey = [NSString stringWithFormat:@"lastPageKey_%@_%d",theClassName,unickNum];
        self.mediaItemsKey = [NSString stringWithFormat:@"mediaItemsKey_%@_%d",theClassName,unickNum];
        [self initMediaItems];
        [self initPageCollection];
    }
    return self;
}

-(void)initMediaItems
{
        NSArray * temp = [[NSArray alloc] init];
        self.mediaItems = temp;
        [temp release];
}

-(void)initPageCollection
{
    if (self.pageCollection == nil)
    {
        NSMutableArray * temp = [[NSMutableArray alloc]init];
        self.pageCollection=temp;
        [temp release];
    }
    else
    {
        [self.pageCollection removeAllObjects];
    }
    
    for (int index=0; index<self.numberOfPages;index++)
    {
        [self.pageCollection addObject:[NSNumber numberWithInt:Empty]];
    }
}

#pragma mark - Utilty

-(void)insertDateToPage:(NSInteger)page withMediaItems:(NSArray*)mediaList toMediaCacheSave:(BOOL)save
{
    [self addObjectToMediaIems:mediaList toChaseSave:save];
    
    if (page == 0 || self.numberOfPages == 0)
    {
        [self insertNumberOfPagesAndInitPageCollection];
        
        for(int index = 0; index <= self.numOfMinumumPageToDownload; index++)
        {
            [self replaceObjectAtIndex:index WithState:PreparingToDownload toChashSave:YES];
        }
    }
    
    NSInteger stateValue = (mediaList !=nil) ? DownloadSuccessful : Empty;
    
    [self replaceObjectAtIndex:page WithState:stateValue toChashSave:YES];
    
    ASLog(@"paginationMangar = %@",self);
    
    self.isDownloadNow = NO;
}


-(NSInteger)checkIfNeedToDownloadMore
{
    for (int index=0; index<self.pageCollection.count; index++)
    {
        NSInteger state = [[self.pageCollection objectAtIndex:index]integerValue];
        if (state== PreparingToDownload)
        {
            return index;
        }
    }
    return NONE;
}

-(BOOL)CheckIfCanDownloadMore
{
    NSInteger num = 0;
    for (int index=0; index<self.pageCollection.count; index++)
    {
        NSInteger state = [[self.pageCollection objectAtIndex:index]integerValue];
        if (state== PreparingToDownload)
        {
            num++;
        }
    }
    if (num > 0)
    {
        return NO;
    }
    else
    {
        return YES;
    }

}

-(void)restoreDataAppDidBecomeActiveWithMediaItems:(NSArray*)medias
{
    self.mediaItems=medias;
    self.numberOfPages = [self pageCollectionCount];
    [self pageCollectionUpdate];
    NSInteger lastPage=[self takeLastPage];
    self.currMainDownloadPage=lastPage;
    self.lastDownloadPage=lastPage;
    [self saveAllInChash];
}

-(BOOL)isComplateDownlaod
{
    if ([self.pageCollection count]==0)
    {
        return NO;
    }
    NSInteger index=0;
    for (NSNumber * item in self.pageCollection)
    {
        NSInteger state = [item integerValue];
        if (state == DownloadSuccessful)
        {
            index++;
        }
    }
    if (index == [self.pageCollection count])
    {
        self.currMainDownloadPage= [self.pageCollection count]-1;
        self.lastDownloadPage = [self.pageCollection count]-1;
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)pageCollectionUpdate
{
    if(([self.mediaItems count]/self.numberOfItemsInPage)-1 > self.lastDownloadPage)
    {
        for(int index=0 ; index < ([self.mediaItems count]/self.numberOfItemsInPage) ; index++)
        {
            [self replaceObjectAtIndex:index WithState:DownloadSuccessful toChashSave:NO];
        }
    }
    self.lastDownloadPage = [self takeLastPage];
    self.numberOfPages =[self pageCollectionCount];
    self.currMainDownloadPage = self.lastDownloadPage;
    if(([self.mediaItems count] / self.numberOfItemsInPage) < self.lastDownloadPage)
    {
        [self insertNumberOfPagesAndInitPageCollection];
    }
    [self saveAllInChash];
}

-(void)insertNumberOfPagesAndInitPageCollection
{
    NSInteger num=0;
    if (self.mediaItems != nil && [self.mediaItems count]>0)
    {
        TVMediaItem * firstItem = [self.mediaItems objectAtIndex:0];
        self.numberOfItems = firstItem.totalItems;
        num = firstItem.totalItems / self.numberOfItemsInPage;
        num = ( firstItem.totalItems % self.numberOfItemsInPage == 0)? num : num+1;
    }
    if (self.numberOfItemsInPage >5)
    {
        num = (num > 25) ? 25 : num;
    }
    else
    {
        num = (num > 35) ? 35 : num;
    }
    self.numberOfPages = num;
    [self initPageCollection];
}

-(void)addObjectToMediaIems:(NSArray *)items toChaseSave:(BOOL)isSave
{
    if(self.mediaItems == nil)
    {
        [self initMediaItems];
    }
    self.mediaItems = [self.mediaItems arrayByAddingObjectsFromArray:items];
    if (isSave)
    {
        [self saveMediaItemsInChash];
    }
}

-(void)replaceObjectAtIndex:(NSInteger)place WithState:(NSInteger)state toChashSave:(BOOL)isSave
{
    if (place < [self.pageCollection count])
    {
        [self.pageCollection replaceObjectAtIndex:place withObject:[NSNumber numberWithInt:state]];
    }
    if (isSave)
    {
        [self savePageCollectionInChash];
    }
}

-(NSInteger)stateOfObjectInPageCollectionAtIndex:(NSInteger)place
{
    if (place < [self.pageCollection count])
    {
        return [[self.pageCollection objectAtIndex:place] integerValue];
    }
    return NONE;
}

-(void)prepareAllItemsToDownload
{
    for(int index =0; index< [self pageCollectionCount]; index++)
    {
        if ([self stateOfObjectInPageCollectionAtIndex:index] == Empty)
        {
            [self replaceObjectAtIndex:index WithState:PreparingToDownload toChashSave:YES];
        }
    }

}

-(void)cancelDownload
{
    for(int index =0; index< [self pageCollectionCount]; index++)
    {
        if ([self stateOfObjectInPageCollectionAtIndex:index] == PreparingToDownload)
        {
            [self replaceObjectAtIndex:index WithState:Empty toChashSave:YES];
        }
    }
    
}

-(NSInteger)takeLastPage
{
    NSInteger lastPage=0;
    for (int index=0; index < [self.pageCollection count];index++){
        NSInteger isDownload = [[self.pageCollection objectAtIndex:index] integerValue];
        if (isDownload == DownloadSuccessful)
        {
            lastPage=index;
        }
    }
    return lastPage;
}

-(NSInteger)pageCollectionCount
{
    return [self.pageCollection count];
}

-(void)updateLastlastDownloadPage
{
    self.lastDownloadPage = [self takeLastPage];
}

-(NSArray *)SearchPagesThatReadyToDownloadWithLastPage:(NSInteger)lastPage
{
    NSMutableArray * pageToDownload = [NSMutableArray array];
    for (int index=0; index < lastPage ;index++)
    {
        NSInteger isDownload = [[self.pageCollection objectAtIndex:index] integerValue];
        if (isDownload == PreparingToDownload)
        {
            [pageToDownload addObject:[NSNumber numberWithInt:index]];
        }
    }
    return [NSArray arrayWithArray:pageToDownload];
}

-(BOOL)isNeedToDownloadPageWithPageNum:(NSInteger)currPageNum andCurrShowPage:(NSInteger)currShowPage
{
     NSInteger pageState = [self stateOfObjectInPageCollectionAtIndex:currPageNum];
    if (pageState==Empty && currShowPage+1  >= self.numOfMinumumPageToDownload && currPageNum < self.numberOfPages)
    {
        return YES;
    }
    return NO;
}

-(BOOL)isNeedToDownloadPageWithPageNum:(NSInteger)currPageNum 
{
    NSInteger pageState = [self stateOfObjectInPageCollectionAtIndex:currPageNum];
    if (pageState == Empty && currPageNum < self.numberOfPages)
    {
        return YES;
    }
    return NO;
}

#pragma mark - Chash Manger

-(void)savePageCollectionInChash
{
    if (self.pageCollection != nil)
    {
        [[TVCache sharedTVCache] setObject:self.pageCollection forKey:self.pageCollectionKey];
    }
}

-(void)loadPageCollectionFromChash
{
    self.pageCollection = [[TVCache sharedTVCache] objectForKey:self.pageCollectionKey];
}

-(void)saveLastDownloadPageInChash
{
    [[TVCache sharedTVCache] setObject:[NSNumber numberWithInt:self.lastDownloadPage] forKey:self.lastPageKey];
}

-(void)loadLastDownloadPageFromChash
{
    NSNumber * temp = [[TVCache sharedTVCache] objectForKey:self.lastPageKey];
    self.lastDownloadPage= [temp integerValue];
}

-(void)saveMediaItemsInChash
{
    if (self.mediaItems != nil)
    {
        [[TVCache sharedTVCache] setObject:self.mediaItems forKey:self.mediaItemsKey];
    }
}

-(void)loadMediaItemsFromChash
{
    self.mediaItems = [[TVCache sharedTVCache] objectForKey:self.mediaItemsKey];
}

-(void)removeMediaItemsFromChash
{
     [[TVCache sharedTVCache] removeObjectForKey:self.mediaItemsKey];
}

-(void)removeLastDownloadPageFromChash
{
    [[TVCache sharedTVCache] removeObjectForKey:self.lastPageKey];

}

-(void)removePageCollectionFromChash
{
    [[TVCache sharedTVCache] removeObjectForKey:self.pageCollection];
}

-(void)saveAllInChash
{
    [self saveMediaItemsInChash];
    [self savePageCollectionInChash];
    [self saveLastDownloadPageInChash];
}

-(void)loadAllFromChash
{
    [self loadLastDownloadPageFromChash];
    [self loadMediaItemsFromChash];
    [self loadPageCollectionFromChash];
}

-(void)removeAllInChash
{
    [self removeLastDownloadPageFromChash];
    [self removeMediaItemsFromChash];
    [self removePageCollectionFromChash];
}

#pragma mark - Description

-(NSString *) description
{
    return [NSString stringWithFormat:@"mediaItemsKey=%@ numberOfPages=%d lastDownloadPage=%d pageCollection=%@ media items count=%d" ,self.mediaItemsKey,self.numberOfPages,self.lastDownloadPage, self.pageCollection, [self.mediaItems count]];
}

@end
