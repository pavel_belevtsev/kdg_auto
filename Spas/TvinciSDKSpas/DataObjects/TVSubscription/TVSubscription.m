//
//  TVSubscription.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 9/3/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSubscription.h"
#import "SmartLogs.h"
#import "NSDictionary+NSNullAvoidance.h"
/*
 {
 "m_bIsSubRenewable" = 1;
 "m_bRecurringStatus" = 1;
 "m_dCurrentDate" = "2012-09-03T08:11:02.48";
 "m_dEndDate" = "2012-09-22T15:53:10.353";
 "m_dLastViewDate" = "2000-01-01T00:00:00";
 "m_dPurchaseDate" = "2012-08-22T15:53:11.31";
 "m_nCurrentUses" = 0;
 "m_nMaxUses" = 1;
 "m_nSubscriptionPurchaseID" = 7502;
 "m_paymentMethod" = 7;
 "m_sDeviceName" = "";
 "m_sDeviceUDID" = "";
 "m_sSubscriptionCode" = 320;
 }
 */
NSString *const TVSubscriptionDateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";
NSString *const TVSubscriptionOtherDateFormat = @"yyyy-MM-dd HH:mm:ss";

NSString *const TVSubscriptionIsRenewableKey = @"m_bIsSubRenewable";
NSString *const TVSubscriptionEndDateKey = @"m_dEndDate";
NSString *const TVSubscriptionLastViewDateKey = @"m_dLastViewDate";
NSString *const TVSubscriptionPurchaseDateKey = @"m_dPurchaseDate";
NSString *const TVSubscriptionCurrentUsesKey = @"m_nCurrentUses";
NSString *const TVSubscriptionMaxUsesKey = @"m_nMaxUses";
NSString *const TVSubscriptionSubscriptionPurchaseIDKey = @"m_nSubscriptionPurchaseID";
NSString *const TVSubscriptionPaymentMethodKey = @"m_paymentMethod";
NSString *const TVSubscriptionDeviceNameKey  = @"m_sDeviceName";
NSString *const TVSubscriptionDeviceUDIDKey = @"m_sDeviceUDID";
NSString *const TVSubscriptionSubscriptionCodeKey = @"m_sSubscriptionCode";

NSString *const TVSubscriptionCurrentDate = @"m_dCurrentDate";
NSString *const TVSubscriptionNextRenewalDate = @"m_dNextRenewalDate";
NSString *const TVSubscriptionRecurringStatus = @"m_bRecurringStatus";


@implementation TVSubscription
@synthesize isRenewable = _isRenewable;
@synthesize endDate = _endDate;
@synthesize lastViewDate = _lastViewDate;
@synthesize purchaseDate = _purchaseDate;
@synthesize currentUses = _currentUses;
@synthesize maxUses = _maxUses;
@synthesize subscriptionPurchaseID = _subscriptionPurchaseID;
@synthesize paymentMethod = _paymentMethod;
@synthesize subscriptionCode = _subscriptionCode;
@synthesize deviceName = _deviceName;
@synthesize deviceUDID = _deviceUDID;

-(void) dealloc
{
    [_endDate release];
    [_lastViewDate release];
    [_purchaseDate release];
    [_deviceName release];
    [_deviceUDID release];
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.isRenewable = [[dictionary objectOrNilForKey:TVSubscriptionIsRenewableKey] boolValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = TVSubscriptionDateFormat;
    
    NSString *dateString = [dictionary objectOrNilForKey:TVSubscriptionEndDateKey];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    // Old code -time should change timezone from outside SDK
//    self.endDate =[self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.endDate = [formatter dateFromString:dateString];
    ASLog(@"dateString = %@   <===> self.endDate = %@",dateString,self.endDate);
    if (self.endDate==nil)
    {
        formatter.dateFormat = TVSubscriptionOtherDateFormat;
        self.endDate = [formatter dateFromString:dateString];
        ASLog(@"dateString = %@   <===> self.endDate = %@",dateString,self.endDate);
    }
    
    dateString = [dictionary objectOrNilForKey:TVSubscriptionLastViewDateKey];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    // Old code -time should change timezone from outside SDK
//    self.lastViewDate =[self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.lastViewDate =[formatter dateFromString:dateString];
    
    dateString = [dictionary objectOrNilForKey:TVSubscriptionPurchaseDateKey];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    // Old code -time should change timezone from outside SDK
//    self.purchaseDate =[self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.purchaseDate =[formatter dateFromString:dateString];
    
    self.currentUses = [[dictionary objectOrNilForKey:TVSubscriptionCurrentUsesKey] integerValue];
    self.maxUses = [[dictionary objectOrNilForKey:TVSubscriptionMaxUsesKey] integerValue];
    self.subscriptionPurchaseID = [[dictionary objectOrNilForKey:TVSubscriptionSubscriptionPurchaseIDKey] integerValue];
    self.paymentMethod = [[dictionary objectOrNilForKey:TVSubscriptionPaymentMethodKey] integerValue];
    self.subscriptionCode = [[dictionary objectOrNilForKey:TVSubscriptionSubscriptionCodeKey] integerValue];
    self.deviceName = [dictionary objectOrNilForKey:TVSubscriptionDeviceNameKey];
    self.deviceUDID = [dictionary objectOrNilForKey:TVSubscriptionDeviceUDIDKey];
    
    self.recurringStatus = [[dictionary objectOrNilForKey:TVSubscriptionRecurringStatus] boolValue];
    
    dateString = [dictionary objectOrNilForKey:TVSubscriptionCurrentDate];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    self.currentDate =[formatter dateFromString:dateString];

    dateString = [dictionary objectOrNilForKey:TVSubscriptionNextRenewalDate];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    // Old code -time should change timezone from outside SDK
    //    self.lastViewDate =[self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.nextRenewalDate =[formatter dateFromString:dateString];

    [formatter release];
}

/*
 @property (nonatomic, retain) NSDate *currentDate;
 @property (nonatomic, retain) NSDate *nextRenewalDate;
 @property (nonatomic, assign) BOOL recurringStatus;
 NSString *const TVSubscriptionCurrentDate = @"m_dCurrentDate";
 NSString *const TVSubscriptionNextRenewalDate = @"m_dNextRenewalDate";
 NSString *const TVSubscriptionRecurringStatus = @"m_bRecurringStatus";
 */

//-(NSDate *)takeLocalTimeWIthDate:(NSDate *)sourceDate
//{
//    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];;    
//    ASLog(@"destinationTimeZone = %@",destinationTimeZone);
//    NSDate * date = [NSDate dateWithTimeInterval:[destinationTimeZone secondsFromGMT] sinceDate:sourceDate];
//    return date;
//}
//
//-(NSDate *)takeSingapurTimeWIthDate:(NSDate *)sourceDate
//{
//    
//    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"SGT"];
//    //NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];;
//    //ASLog(@"destinationTimeZone = %@",destinationTimeZone);
//    NSDate * date = [NSDate dateWithTimeInterval:[sourceTimeZone secondsFromGMT] sinceDate:sourceDate];
//    return date;
//}

//-(NSString *) description
//{
//    
//    return [NSString stringWithFormat:@"subscriptionPurchaseID = %@ Type=%@ Name=%@ MainFile=%@ TrailerFile=%@" ,self.subscriptionPurchaseID,self.mediaTypeName,self.name, self.mainFile.fileURL.path, self.trailerFile.fileURL.path];
//}
@end
