//
//  TVDevice.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVDevice.h"
#import "NSDictionary+NSNullAvoidance.h"

NSString *const TVDeviceActivationDateKey = @"m_activationDate";
NSString *const TVDeviceBrandKey = @"m_deviceBrand";
NSString *const TVDeviceBrandIDKey = @"m_deviceBrandID";
NSString *const TVDeviceFamilyKey = @"m_deviceFamily";
NSString *const TVDevice_FamilyIDKey = @"m_deviceFamilyID";
NSString *const TVDeviceNameKey = @"m_deviceName";
NSString *const TVDeviceUDIDKey = @"m_deviceUDID";
NSString *const TVDeviceDomainIDKey = @"m_domainID";
NSString *const TVDeviceIDKey = @"m_id";
NSString *const TVDevicePINKey = @"m_pin";
NSString *const TVDeviceStateKey = @"m_state";

@implementation TVDevice

@synthesize deviceID = _deviceID;
@synthesize UDID = _UDID;
@synthesize deviceFamilyID = _deviceFamilyId;
@synthesize deviceName = _deviceName;
@synthesize brandID = _brandId;
@synthesize activationDate = _activationDate;
@synthesize deviceState = _deviceState;
@synthesize familyName = _familyName;
@synthesize domainID = _domainID;
@synthesize brandName = _brandName;

#pragma mark - Memory
-(void) dealloc
{
    self.UDID = nil;
    self.familyName = nil;
    self.deviceName = nil;
    self.brandName = nil;
    self.activationDate = nil;
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    self.deviceID = [[dictionary objectOrNilForKey:TVDeviceIDKey] integerValue];
    //NSLog(@"self.deviceID = %ld",(long)self.deviceID);
    self.UDID = [dictionary objectOrNilForKey:TVDeviceUDIDKey];
    // NSLog(@"UDID = %@",self.UDID);
    
    self.deviceFamilyID = [[dictionary objectOrNilForKey:TVDevice_FamilyIDKey] integerValue];
    self.deviceName = [dictionary objectOrNilForKey:TVDeviceNameKey];
    self.brandID = [[dictionary objectOrNilForKey:TVDeviceBrandIDKey] integerValue];
    self.activationDate = [dictionary objectOrNilForKey:TVDeviceActivationDateKey];
    self.deviceState = (TVDeviceState)[[dictionary objectOrNilForKey:TVDeviceStateKey] integerValue];
    self.familyName = [dictionary objectOrNilForKey:TVDeviceFamilyKey];
    self.domainID = [[dictionary objectOrNilForKey:TVDeviceDomainIDKey] integerValue];
    self.brandName = [dictionary objectOrNilForKey:TVDeviceBrandKey];
    self.devicePin = [dictionary objectOrNilForKey:TVDevicePINKey];
}

-(NSString *) description{
    NSMutableString *description = [NSMutableString stringWithFormat:@"\n--------------\nTVDevice:\n\ndeviceID: %ld\n",(long)_deviceID];
    [description appendFormat:@"UDID: %@",_UDID];
    [description appendFormat:@"deviceFamilyID: %ld",(long)_deviceFamilyId];
    [description appendFormat:@"deviceName: %@",_deviceName];
    [description appendFormat:@"brandID: %ld",(long)_brandId];
    [description appendFormat:@"activationDate: %@",_activationDate];
    [description appendFormat:@"deviceState: %d",_deviceState];
    [description appendFormat:@"familyName: %@",_familyName];
    [description appendFormat:@"domainID: %ld",(long)_domainID];
    [description appendFormat:@"brandName: %@",_brandName];
    
    
    
    
    return description;
}

@end
