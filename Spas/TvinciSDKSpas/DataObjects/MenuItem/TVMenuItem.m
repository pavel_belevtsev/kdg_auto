//
//  TVMenuItem.m
//  Kanguroo
//
//  Created by Avraham Shukron on 4/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVMenuItem.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "JSONKit.h"
#import "SmartLogs.h"

NSString *const TVMenuItemNameKey = @"Name";
NSString *const TVMenuItemMenuTypeKey = @"MenuType";
NSString *const TVMenuItemRuleIDKey = @"RuleID";
NSString *const TVMenuItemIDKey = @"ID";
NSString *const TVMenuItemPageIDKey = @"PageID";
NSString *const TVMenuItemPageLayoutKey = @"PageLayout";
NSString *const TVMenuItemCultureKey = @"Culture";
NSString *const TVMenuItemChildrenKey = @"Children";
NSString *const TVMenuItemURLKey = @"URL";
NSString *const TVMenuItemTopGalleryIDKey = @"TopGalleryID";
NSString *const TVMenuItemGalleryIDKey = @"GalleryID";
NSString *const TVMenuItemMainGalleryIDKey = @"MainGalleryID";
NSString *const TVMenuItemIconURLKey = @"IconURL";
NSString *const TVMenuItemIconKey = @"Icon";
NSString *const TVMenuItemChannelIDKey = @"ChannelID";
NSString * const TVMenuIremVODCategoryIdKey = @"VODCategoryID";


@implementation TVMenuItem
@synthesize menuItemID = _menuItemID;
@synthesize menuType = _menuType;
@synthesize ruleID = _ruleID;
@synthesize name = _name;
@synthesize culture = _culture;
@synthesize children = _children;
@synthesize URL = _URL;
//Extra info
@synthesize galleryID = _galleryID;
@synthesize topGalleryID = _topGalleryID;
@synthesize mainGalleryID = _mainGalleryID;
@synthesize pageID = _pageID;
@synthesize pageLayout = _pageLayout;
@synthesize iconURL = _iconURL;
@synthesize iconName = _iconName;
@synthesize channelID = _channelID;
@synthesize vodCategoryID;

#pragma mark - Memory
-(void) dealloc
{
    self.iconName = nil;
    self.name = nil;
    self.URL = nil;
    self.culture = nil;
    self.children = nil;
    self.iconURL = nil;
    self.pageLayout = nil;
    [vodCategoryID release];
    [super dealloc];
}

-(NSString *) description{
    return [NSString stringWithFormat:@"[TVMenuItem name: %@ menuType: %li]",self.name, (long)self.menuType];
}

#pragma mark - Getters
-(TVMenuItemType) type
{
    if (self.children.count > 0) return TVMenuItemTypeRoot;
    else if (self.galleryID > 0) return TVMenuItemTypeGallery;
    else if (self.channelID > 0) return TVMenuItemTypeChannel;
    else return TVMenuItemTypeUnknown;
}

//VODCategoryID

#pragma mark - Initializations

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    // ASLogInfo(@"[MenuItem][Attributes] %@",dictionary);////////
    self.menuItemID = [[dictionary objectOrNilForKey:TVMenuItemIDKey] integerValue];
    self.name = [dictionary objectOrNilForKey:TVMenuItemNameKey];
    self.culture = [dictionary objectOrNilForKey:TVMenuItemCultureKey];
    self.pageID = [[dictionary objectOrNilForKey:TVMenuItemPageIDKey] integerValue];
    self.menuType = [[dictionary objectOrNilForKey:TVMenuItemMenuTypeKey] integerValue];
    self.ruleID = [[dictionary objectOrNilForKey:TVMenuItemRuleIDKey] integerValue];
    NSString *URLString = [dictionary objectOrNilForKey:TVMenuItemURLKey];
    self.URL = [NSURL URLWithString:URLString];
    
    // addtional field to resolve duble qouts('') insted of " and  unneccessry "
    
    self.layout = [self parseURLToLayoutWithString:URLString];
    if (self.layout)
    {
        self.uniqueName = [self.layout objectOrNilForKey:@"Type"];
    }
    
   // NSLog(@"self.name = %@ \n URLString = %@ \n self.layout = %@",self.name,URLString,self.layout);
    URLString = [URLString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    URLString = [URLString stringByReplacingOccurrencesOfString:@"''" withString:@"\""];
    NSDictionary *extra = [URLString objectFromJSONString];
    if (extra != nil)
    {
        self.galleryID = [[extra objectOrNilForKey:TVMenuItemGalleryIDKey] integerValue];
        self.topGalleryID = [[extra objectOrNilForKey:TVMenuItemTopGalleryIDKey] integerValue];
        self.mainGalleryID = [[extra objectOrNilForKey:TVMenuItemMainGalleryIDKey] integerValue];
        self.pageID = [[extra objectOrNilForKey:TVMenuItemPageIDKey] integerValue];
        self.pageLayout = [extra objectOrNilForKey:TVMenuItemPageLayoutKey];
        NSString *iconURLString = [extra objectOrNilForKey:TVMenuItemIconURLKey];
        self.iconURL = [NSURL URLWithString:iconURLString];
        self.iconName = [extra objectOrNilForKey:TVMenuItemIconKey];
        self.channelID = [[extra objectOrNilForKey:TVMenuItemChannelIDKey] integerValue];
        self.vodCategoryID = [extra objectOrNilForKey:TVMenuIremVODCategoryIdKey];
    }
   // NSLog(@"extra = %@",extra);

    NSMutableArray *temp = [NSMutableArray array];
    NSArray *childrenArray = [dictionary objectOrNilForKey:TVMenuItemChildrenKey];
    for (NSDictionary *itemAsDictionary in childrenArray)
    {
        TVMenuItem *child = [[TVMenuItem alloc] initWithDictionary:itemAsDictionary];
        if (child != nil)
        {
            [temp addObject:child];
        }
        [child release];
    }
    self.children = [NSArray arrayWithArray:temp];
}


-(id) parseURLToLayoutWithString:(NSString *) string
{
    string = [string stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]];
    
    string = [string stringByReplacingOccurrencesOfString:@"''" withString:@"\""];
    
    NSRange firstRange = NSMakeRange(0, 1);
    NSRange lastRange = NSMakeRange(string.length-1, 1);
    
    NSString * stringWithReplaveOfThefirstChar = [string stringByReplacingCharactersInRange:firstRange withString:@"{"];
    NSString * stringWithReplaveOfThefLastChar = [stringWithReplaveOfThefirstChar stringByReplacingCharactersInRange:lastRange withString:@"}"];
    
    NSString * result = [stringWithReplaveOfThefLastChar objectFromJSONString];
    
    return result;
}

@end