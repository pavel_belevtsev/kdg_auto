//
//  TVFacebookUser.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVFacebookUser.h"

@implementation TVFacebookUser

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSString * status = [dictionary objectForKey:@"status"];
    NSString * siteGuid = [dictionary objectForKey:@"siteGuid"];
    NSString * tvinciName = [dictionary objectForKey:@"tvinciName"];
    NSString * facebookName = [dictionary objectForKey:@"facebookName"];
    NSString * pictureURL = [dictionary objectForKey:@"pic"];
    NSString * data = [dictionary objectForKey:@"data"];
    NSString * minFriends = [dictionary objectForKey:@"minFriends"];
    NSDictionary * facebookUser = [dictionary objectForKey:@"fbUser"];
    NSString * token = [dictionary objectForKey:@"token"];
    
    
    
    self.status = TVFacebookStatusForName(status);
    self.siteGuid = siteGuid;
    self.tvinciName = tvinciName;
    self.facebookName = facebookName;
    self.pictureURL = [NSURL URLWithString:pictureURL];
    self.data = data;
    self.minFriends = minFriends;
    self.facebookUser = facebookUser;
    self.token = token;
}

-(NSString *)description
{
    return @"";
}

@end
