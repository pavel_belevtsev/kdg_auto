//
//  TVConfigurationObject.m
//  tvinci iPad
//
//  Created by Alexander Israel on 1/3/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "TVConfigurationObject.h"
#import "NSDictionary+NSNullAvoidance.h"

@interface TVConfigurationObject ()


@end

NSString * const TVDMSToken = @"token";
NSString * const TVDMSRegistrationStatus = @"status";
NSString * const TVDMSRegistrationRegistered = @"registered";
NSString * const TVDMSRegistrationUnregistered = @"unregistered";
NSString * const TVDMSUdid = @"udid";
NSString * const TVDMSAppVersion = @"version";
NSString * const TVDMSParams = @"params";
NSString * const TVDMSInitObj = @"InitObj";
NSString * const TVDMSFacebookURL = @"FacebookURL";
NSString * const TVDMSGateways = @"Gateways";
NSString * const TVDMSGatewaySite = @"Site";
NSString * const TVDMSGatewayMedia = @"Media";
NSString * const TVDMSGatewayDomain = @"Domain";
NSString * const TVDMSGatewayPricing = @"Pricing";
NSString * const TVDMSGatewayGetMenu = @"JsonGW";
NSString * const TVDMSInitObjTopMenuID = @"TopMenuID";
NSString * const TVDMSInitObjServayURL = @"SurveyURL";

@implementation TVConfigurationObject

- (id)init
{
    self = [super init];
    if (self) {
        self.topMenuItems = [[NSMutableArray alloc]init];
    }
    return self;
}


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    self.token = [dictionary objectOrNilForKey:TVDMSToken];
    self.registrationStatus = [dictionary objectOrNilForKey:TVDMSRegistrationStatus];
    self.udid = [dictionary objectOrNilForKey:TVDMSUdid];
    self.appVersion = [dictionary objectOrNilForKey:TVDMSAppVersion];
    
    NSDictionary* params = [dictionary objectOrNilForKey:TVDMSParams];
    
    self.initObj = [TVInitObj initObjWithArray:[params objectOrNilForKey:TVDMSInitObj]];
    self.initObjDic = [params objectOrNilForKey:TVDMSInitObj];
    self.topMenuID = [params objectOrNilForKey:TVDMSInitObjTopMenuID];
    
    self.facebookURL = [params objectOrNilForKey:TVDMSFacebookURL];
    self.survayURL = [params objectOrNilForKey:TVDMSInitObjServayURL];
    
    NSArray *gateways = [params objectOrNilForKey:TVDMSGateways];
    
    for (NSDictionary *gateway in gateways)
    {
        
        if ([gateway objectOrNilForKey:TVDMSGatewaySite])
        {
            self.siteGateway = [gateway objectOrNilForKey:TVDMSGatewaySite];
        }
        
        if ([gateway objectOrNilForKey:TVDMSGatewayMedia])
        {
            self.mediaGateway = [gateway objectOrNilForKey:TVDMSGatewayMedia]; //@"http://192.168.16.180/tvpapi/ws/media/service.asmx"; // [gateway objectOrNilForKey:TVDMSGatewayMedia];
        }
        
        if ([gateway objectOrNilForKey:TVDMSGatewayDomain])
        {
            self.domainGateway = [gateway objectOrNilForKey:TVDMSGatewayDomain];
        }
        
        if ([gateway objectOrNilForKey:TVDMSGatewayPricing])
        {
            self.pricingGateway = [gateway objectOrNilForKey:TVDMSGatewayPricing];
        }

        if ([gateway objectOrNilForKey:TVDMSGatewayGetMenu])
        {
            self.getMenuGateway = [gateway objectOrNilForKey:TVDMSGatewayGetMenu];
        }
    }

    
}


- (void)dealloc
{
    [_topMenuItems release];
    [_topMenuID release];
    [super dealloc];
}

@end
