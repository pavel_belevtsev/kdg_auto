//
//  TVChannel.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 9/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVChannel.h"
#import "NSDictionary+NSNullAvoidance.h"

NSString *const TVChannelTitleKey = @"Title";
NSString *const TVChannelChannelIDKey = @"ChannelID";

@implementation TVChannel
@synthesize channelID = _channelID;
@synthesize title = _title;

-(void) dealloc
{
    [_title release];
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.title = [dictionary objectOrNilForKey:TVChannelTitleKey];
    self.channelID = [[dictionary objectOrNilForKey:TVChannelChannelIDKey] integerValue];
}

@end
