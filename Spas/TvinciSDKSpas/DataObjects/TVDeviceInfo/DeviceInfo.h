//
//  DeviceInfo.h
//  TvinciSDK
//
//  Created by Israel on 4/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"


/*
 "m_oDevice" =     {
 "m_activationDate" = "2014-03-31T12:38:12.863";
 "m_deviceBrand" = "<null>";
 "m_deviceBrandID" = 3;
 "m_deviceFamily" = "";
 "m_deviceFamilyID" = 4;
 "m_deviceName" = "Stas Shulgins iPad";
 "m_deviceUDID" = d0b2433dd5e73ab7181dd89e633d3e0f193bfa40;
 "m_domainID" = 139039;
 "m_id" = 102529;
 "m_pin" = "";
 "m_state" = 4;
 };
 "m_oDeviceResponseStatus" = 4;
 */

@interface DeviceInfo : BaseModelObject

@property (nonatomic, retain) NSDate * activationDate;
@property (nonatomic, retain) NSString * deviceBrand;
@property NSInteger deviceBrandID;
@property (nonatomic, retain) NSString * deviceFamily;
@property NSInteger deviceFamilyID;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) NSString * deviceUDID;
@property NSInteger m_domainID;
@property NSInteger m_id;
@property (nonatomic, retain) NSString * pin;
@property NSInteger state;
@property kDomainResponseStatus deviceResponseStatus;

@end
