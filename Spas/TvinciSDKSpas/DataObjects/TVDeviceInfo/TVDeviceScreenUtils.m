//
//  TVDeviceScreenUtils.m
//  TvinciSDK
//
//  Created by Aviv Alluf on 9/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVDeviceScreenUtils.h"

@implementation TVDeviceScreenUtils
+ (BOOL)isRetina
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
        && [[UIScreen mainScreen] scale] >= 2.0) {
        // Retina
        return YES;
    }
    return NO;
}

@end

