//
//  DeviceInfo.m
//  TvinciSDK
//
//  Created by Israel Berezin on 4/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "DeviceInfo.h"

const NSString * DeviceActivationDateKey = @"m_activationDate";
const NSString * DeviceBrandKey = @"m_deviceBrand";
const NSString * DeviceBrandIDKey = @"m_deviceBrandID";
const NSString * DeviceFamilyKey = @"m_deviceFamily";
const NSString * DeviceFamilyIDKey = @"m_deviceFamilyID";
const NSString * DeviceNameKey =  @"m_deviceName";
const NSString * DeviceUDIDKey =  @"m_deviceUDID";
const NSString * DeviceDomainIDKey = @"m_domainID";
const NSString * DeviceIdKey = @"m_id";
const NSString * DevicePinKey = @"m_pin";
const NSString * DeviceStateKey = @"m_state";
const NSString * DeviceResponseStatusKey = @"m_oDeviceResponseStatus";
const NSString * DeviceInfoKey = @"m_oDevice";

@implementation DeviceInfo


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSDictionary * deviceInfo = [dictionary objectForKey:DeviceInfoKey];
    NSString * dateString = [deviceInfo objectForKey:DeviceActivationDateKey];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    self.activationDate = [dateFormatter dateFromString:dateString];
    [dateFormatter release];

    self.deviceBrand = [deviceInfo objectForKey:DeviceBrandKey];
    self.deviceBrandID = [[deviceInfo objectForKey:DeviceBrandIDKey] intValue];
    self.deviceFamily = [deviceInfo objectForKey:DeviceFamilyKey];
    self.deviceFamilyID = [[deviceInfo objectForKey:DeviceFamilyIDKey] intValue];
    self.deviceName = [deviceInfo objectForKey:DeviceNameKey];
    self.deviceUDID = [deviceInfo objectForKey:DeviceUDIDKey];
    self.m_domainID = [[deviceInfo objectForKey:DeviceDomainIDKey] intValue];
    self.m_id = [[deviceInfo objectForKey:DeviceIdKey] intValue];
    self.pin = [deviceInfo objectForKey:DevicePinKey];
    self.state = [[deviceInfo objectForKey:DeviceStateKey] intValue];
    self.deviceResponseStatus = [[dictionary objectForKey:DeviceResponseStatusKey] intValue];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"DeviceName = %@, DeviceUDID =%@, ActivationDate  = %@, DeviceBrandID = %ld DeviceFamilyID = %ld, m_id = %ld, deviceResponseStatus = %d",self.deviceName,self.deviceUDID, self.activationDate,(long)self.deviceBrandID,(long)self.deviceFamilyID,(long)self.m_id,self.deviceResponseStatus];
}

-(void)dealloc
{
    [_activationDate dealloc];
    [_deviceBrand release];
    [_deviceFamily release];
    [_deviceName release];
    [_deviceUDID release];
    [_pin release];
    
    [super dealloc];
}
@end
