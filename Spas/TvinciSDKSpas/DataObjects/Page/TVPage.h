//
//  TVPage.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

typedef enum 
{
	PageTokenUnknown = 0,
	PageTokenHomepage,
	PageTokenMediaPage,
	PageTokenMediaList,
	PageTokenMyZone,
	PageTokenStatic,
	PageTokenPage404,
	PageTokenPackage,
	PageTokenMyPlayList,
	PageTokenLinearChannel,
	PageTokenDynamic,
	PageTokenShowPage,
	PageTokenShowsLobby,
	PageTokenAllTags,
	PageTokenMeta,
	PageTokenPurchase,
	PageTokenLoginJoin,
	PageTokenPackagesLobby,
	PageTokenMessages,
	PageTokenArticle,
	PageTokenSearch
} PageToken;

typedef enum 
{
    TVHomeWorldTypeSpotligth4 = 1,
    TVHomeWorldTypeSpotlight2 = 2,
    TVHomeWorldTypeGrid2x2 = 3,
    TVHomeWorldTypeGrid3x3 = 4,
    TVHomeWorldTypeSpotligth2x3P4 = 5,
}TVHomeWorldType;

#define TVHomeWorldTypesStrings [NSArray arrayWithObjects:@"",@"TVHomeWorldTypeSpotligth4",@"TVHomeWorldTypeSpotlight2", @"TVHomeWorldTypeGrid2x2",@"TVHomeWorldTypeGrid3x3",@"TVHomeWorldTypeSpotligth2x3P4",@"?",@"?", nil]


@interface TVPage : BaseModelObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger pageId;
@property (nonatomic, assign) NSInteger pageMetaDataId;
@property (nonatomic, copy) NSString *pageDescription;
@property (nonatomic, copy) NSString *breadCrumbText;
@property (nonatomic, copy) NSString *pageRelativePath;
@property (nonatomic, assign) BOOL hasPlayer;
@property (nonatomic, assign) BOOL hasCarousel;
@property (nonatomic, assign) BOOL hasMiddleFooter;
@property (nonatomic, assign) BOOL playerAutoPlay;
@property (nonatomic, assign) PageToken pageToken;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, assign) NSInteger playerChannel;
@property (nonatomic, assign) NSInteger playerTreeCategory;
@property (nonatomic, assign) NSInteger carouselChannel;
@property (nonatomic, assign) NSInteger sideProfileID;
@property (nonatomic, assign) NSInteger bottomProfileID;
@property (nonatomic, assign) NSInteger menuID;
@property (nonatomic, assign) NSInteger footerID;
@property (nonatomic, assign) NSInteger middleFooterID;
@property (nonatomic, assign) NSInteger profileID;
@property (nonatomic, assign) BOOL isProtected;
@property (nonatomic, retain) NSArray *children;
@property (nonatomic, retain) NSArray *mainGalleries;
@property (nonatomic, retain) NSArray *topGalleries;
@property(nonatomic,retain) NSArray * mainGalleriesIDs;

@property (nonatomic, readonly) TVHomeWorldType homeWorldType;
@end