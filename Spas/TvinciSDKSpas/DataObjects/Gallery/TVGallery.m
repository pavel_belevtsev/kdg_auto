//
//  TVGallery.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVGallery.h"
#import "TVGalleryItem.h"
#import "SmartLogs.h"

#define GalleryTypeUnkownString @"Unknown"

#define GalleryIDKey @"GalleryID"
#define GalleryTypeKey @"UIGalleryType"
#define GalleryFamilyIDKey @"FamilyID"
#define GalleryMainDescriptionKey @"MainDescription"
#define GalleryGroupTitleKey @"GroupTitle"
#define GalleryLocationKey @"GalleryLocation"
#define GalleryTVMUserKey @"TVMUser"
#define GalleryTVMPasswordKey @"TVMPass"
#define GalleryOrderKey @"GalleryOrder"
#define GalleryMainCultureKey @"MainCulture"
#define GalleryLinksHeaderKey @"LinksHeader"
NSString *const TVGalleryGalleryItemsKey = @"GalleryItems";

@interface TVGallery ()
+ (NSArray *) galleryTypeNames;

@property (nonatomic ,assign , readwrite) NSString *galleryTypeString;
@end

@implementation TVGallery

@synthesize galleryItems = _galleryItems;
@synthesize galleryID = _galleryId;
@synthesize galleryType = _galleryType;
@synthesize galleryLocation = _location;
@synthesize galleryOrder = _order;
@synthesize mainCulture = _mainCulture;
@synthesize linksHeader = _linksHeader;
@synthesize title = _groupTitle;
@synthesize mainDescription = _mainDescription;
@synthesize familyId = _familyId;
@synthesize initialized = _initialized;
@synthesize channels = _channels;

#pragma mark - Memory
-(void) dealloc
{
    self.galleryItems = nil;
    self.galleryLocation = nil;
    self.linksHeader = nil;
    self.title = nil;
    self.mainCulture = nil;
    self.mainDescription = nil;
    [super dealloc];
}

#pragma mark - Getters
+(NSArray *) galleryTypeNames
{
    static NSArray *galleryTypeNames = nil;
    if (galleryTypeNames == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            galleryTypeNames = [[NSArray alloc] initWithObjects:@"Caousel",
                                @"XPack",
                                @"Free",
                                @"Top",
                                @"Player",
                                @"Spotlight",
                                @"MovingGallery",
                                @"RandomGallery",
                                @"Cards",
                                @"MovieFinder",
                                @"RelPackages",
                                @"MyZoneMiniFeed",
                                @"PeopleWhoWatched",
                                @"BannerBig",
                                @"Banner",
                                @"LinearChannel",
                                @"Multi",
                                @"MostViewed",
                                @"EditorialComments",
                                @"FreeFlash",
                                @"AdsGallery",
                                nil];
        });
    }
    return galleryTypeNames;
}

-(NSString *) galleryTypeString
{
    if (self.galleryType < [TVGallery galleryTypeNames].count && self.galleryType >= 0)
    {
        return [[TVGallery galleryTypeNames] objectAtIndex:self.galleryType];
    }
    return GalleryTypeUnkownString;
}

#pragma mark - Setters
-(void) setGalleryTypeString:(NSString *)typeString
{
    NSArray *typeNames = [TVGallery galleryTypeNames];
    NSInteger type = [typeNames indexOfObject:typeString];
    if (type != NSNotFound)
    {
        self.galleryType = type;
    }
    self.galleryType = UIGalleryTypeUnknown;
}

#pragma mark - Parsing

-(NSArray *) parseGalleryItems : (NSArray *) itemsTpParse
{
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *itemAsDictionary in itemsTpParse)
    {
        TVGalleryItem *item = [[TVGalleryItem alloc] initWithDictionary:itemAsDictionary];
        if (item != nil)
        {
            [temp addObject:item];
        }
        [item release];
    };
    return [NSArray arrayWithArray:temp];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    // ASLog2Debug(@"[TVGallery] \n%@",dictionary);////////
    
    self.galleryID = [[dictionary objectForKey:GalleryIDKey] longLongValue];
    NSString *galleryType = [dictionary objectForKey:GalleryTypeKey];
    self.galleryTypeString = galleryType;
    self.familyId = [[dictionary objectForKey:GalleryFamilyIDKey] longLongValue];
    self.mainDescription = [dictionary objectForKey:GalleryMainDescriptionKey];
    self.title = [dictionary objectForKey:GalleryGroupTitleKey];
    self.galleryLocation = [dictionary objectForKey:GalleryLocationKey];
    self.galleryOrder = [[dictionary objectForKey:GalleryOrderKey] intValue];
    self.mainCulture = [dictionary objectForKey:GalleryMainCultureKey];
    self.linksHeader = [dictionary objectForKey:GalleryLinksHeaderKey];
    NSArray *galleryItems = [dictionary objectForKey:TVGalleryGalleryItemsKey];
    self.galleryItems = [self parseGalleryItems:galleryItems];
}
@end
