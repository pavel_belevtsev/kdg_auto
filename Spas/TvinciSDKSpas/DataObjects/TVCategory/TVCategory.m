//
//  TVCategory.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVCategory.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVChannel.h"

@implementation TVCategory

NSString *const TVCategoryTitleKey = @"Title";
NSString *const TVCategoryIDKey = @"ID";
NSString *const TVCategoryChannelsKey = @"Channels";
NSString *const TVCategoryInnerCategoriesKey = @"InnerCategories";
NSString *const TVCategoryImageKey = @"PicURL";


@synthesize title = _title;
@synthesize ID = _ID;
@synthesize channels = _channels;
@synthesize subCategories = _subCategories;
@synthesize pictureURL = _pictureURL;

- (void)dealloc
{
    [_title release];
    [_ID release];
    [_channels release];
    [_subCategories release];
    [super dealloc];
}

#pragma mark - Initialization
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.title = [dictionary objectOrNilForKey:TVCategoryTitleKey];
    self.ID = [dictionary objectOrNilForKey:TVCategoryIDKey];
    self.pictureURL =[dictionary objectOrNilForKey:TVCategoryImageKey];
    
    NSArray *channelsAsDictionaries = [dictionary objectOrNilForKey:TVCategoryChannelsKey];
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *dic in channelsAsDictionaries)
    {
        TVChannel *channel = [[TVChannel alloc] initWithDictionary:dic];
        if (channel != nil)
        {
            [temp addObject:channel];
        }
        [channel release];
    }
    self.channels = [NSArray arrayWithArray:temp];
    
    NSArray *subCategoriesAsDictionaries = [dictionary objectOrNilForKey:TVCategoryInnerCategoriesKey];
    temp = [NSMutableArray array];
    
    for(NSDictionary *dic in subCategoriesAsDictionaries)
    {
        TVCategory *subCategory = [[TVCategory alloc] initWithDictionary:dic];
        if(subCategory != nil)
        {
            [temp addObject:subCategory];
        }
        [subCategory release];
    }
    self.subCategories = [NSArray arrayWithArray:temp];
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"Category: %@, ID: %@\n SubCategories: %@",self.title,self.ID , self.subCategories];
}
@end