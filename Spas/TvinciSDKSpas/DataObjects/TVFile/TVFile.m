//
//  TVFile.m
//  TVinci
//
//  Created by Avraham Shukron on 6/4/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVFile.h"
#import "NSDictionary+NSNullAvoidance.h"

NSString *const TVFileIDKey = @"FileID";
NSString *const TVFileBaseURLKey = @"URL";
NSString *const TVFileDurationKey = @"Duration";
NSString *const TVFileFormatKey = @"Format";
NSString *const TVFileEncryptionKey = @"Encryption";

NSString *const TVFileLanguageKey = @"language";
NSString *const TVFileCoGuidKey = @"CoGuid";
NSString *const TVFileIsDefaultLangKey = @"IsDefaultLang";


@interface TVFile ()

//  Added for Tvinci SDK 2.5 Catwoman-Daredevil
@property (retain, nonatomic) NSString *language;
@property (retain, nonatomic) NSString *CoGuid;
@property (assign) BOOL IsDefaultLang;



@end

@implementation TVFile
@synthesize fileID = _fileID;
@synthesize format = _format;
@synthesize duration = _duration;
@synthesize fileURL = _fileURL;
@synthesize encriptionType = _encriptionType;

-(void) dealloc
{
    self.fileID = nil;
    self.format = nil;
    [_fileURL release];
    [_breakProvider release];
    [_postProvider release];
    [_preProvider release];
    [_breakPoints release];
    [super dealloc];
}

- (id)copyWithZone:(NSZone *)zone {
    
    TVFile* newFile     = [[[self class] allocWithZone:zone] init];
    if (newFile) {
        newFile.fileID        = [_fileID copyWithZone:zone];
        newFile.format        = [_format copyWithZone:zone];
        newFile.fileURL       = [_fileURL copyWithZone:zone];
        newFile.duration      = _duration;
        
        newFile.encriptionType    = _encriptionType;
        newFile.breakProvider     = _breakProvider;
        newFile.postProvider      = _postProvider;
        newFile.breakPoints       = _breakPoints;
        newFile.preProvider       = _preProvider;
    }
    return newFile;
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    self.fileID = [dictionary objectOrNilForKey:TVFileIDKey];
    self.format = [dictionary objectOrNilForKey:TVFileFormatKey];
    NSString *URLString = [dictionary objectOrNilForKey:TVFileBaseURLKey];
    self.fileURL = (URLString.length > 0)? [NSURL URLWithString:URLString] : nil;
    self.duration = [[dictionary objectOrNilForKey:TVFileDurationKey] doubleValue];
    self.encriptionType =  (TVEncryptionType)[[dictionary objectOrNilForKey:TVFileEncryptionKey] integerValue];
    
    self.IsDefaultLang = [[dictionary objectOrNilForKey:TVFileIsDefaultLangKey] boolValue];
    self.language = [dictionary objectOrNilForKey:TVFileLanguageKey];
    
    self.CoGuid = [dictionary objectOrNilForKey:TVFileCoGuidKey];
    
    NSDictionary *  test = [dictionary objectOrNilForKey:@"PreProvider"];
    if (test != nil)
    {
        self.preProvider = [[[AdProvider alloc]initWithAdType:PreRoll] autorelease];
        [self.preProvider setAttributesFromDictionary:[dictionary objectOrNilForKey:@"PreProvider"]];
    }
    else
    {
        self.preProvider = nil;
    }
    test = [dictionary objectOrNilForKey:@"BreakProvider"];
    if (test != nil)
    {
        self.breakProvider = [[[AdProvider alloc]initWithAdType:MidRoll] autorelease];
        [self.breakProvider setAttributesFromDictionary:[dictionary objectOrNilForKey:@"BreakProvider"]];
    }
    else
    {
        self.breakProvider =nil;
    }
    
    test = [dictionary objectOrNilForKey:@"PostProvider"];
    if (test != nil)
    {
        self.postProvider = [[[AdProvider alloc]initWithAdType:PostRoll] autorelease];
        [self.postProvider setAttributesFromDictionary:[dictionary objectOrNilForKey:@"PostProvider"]];
    }
    else
    {
        self.postProvider = nil;
    }
    self.breakPoints = [NSArray arrayWithArray:[dictionary objectOrNilForKey:@"BreakPoints"]];
    // NSLog(@"self.breakPoints = %@",self.breakPoints);
}

-(BOOL)isFileHeveAnyAds
{
    if ((self.preProvider != nil) || (self.breakProvider != nil && [self.breakPoints count] > 0) || (self.postProvider != nil))
    {
        return  YES;
    }
    return  NO;
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"fileID = %@ format = %@ duration = %f fileURL = %@ encriptionType = %u \n" ,self.fileID,self.format,self.duration, self.fileURL, self.encriptionType];
}
@end
