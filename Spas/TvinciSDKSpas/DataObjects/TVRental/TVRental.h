//
//  TVRental.h
//  TvinciSDK
//
//  Created by iosdev1 on 11/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TvinciSDK.h"





@interface TVRental : BaseModelObject
{
    
}


- (BOOL)isRental;

@property (nonatomic ,retain)   NSDate * currentDate;
@property (nonatomic ,retain)   NSDate * endDate;
@property (nonatomic ,retain)   NSDate * purchaseDate;
@property (nonatomic)           NSInteger currentUses;
@property (nonatomic)           NSInteger maxUses;
@property (nonatomic)           NSInteger mediaFileID;
@property (nonatomic)           NSInteger mediaID;
@property (nonatomic)           NSInteger purchaseMethod;
@property (nonatomic,copy)      NSString * deviceName;
@property (nonatomic,copy)      NSString * deviceUDID;

@end

extern NSString *const  TVRentalCurrentDate;
extern NSString *const TVRentalEndDateKey;
extern NSString *const TVRentalPurchaseDateKey;
extern NSString *const TVRentalCurrentUsesKey;
extern NSString *const TVRentalMaxUsesKey;
extern NSString *const TVRentalMediaFileID;
extern NSString *const TVRentalMediaID;
extern NSString *const TVRentalPurchaseMethod;
extern NSString *const TVRentalDeviceNameKey;
extern NSString *const TVRentalDeviceUDIDKey;