//
//  TVSubSocialComment.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSubSocialComment.h"
#import "NSDate+Format.h"

NSString *const SrverDateFormat2 = @"yyyy-MM-dd HH:mm:ssZZZZ";
NSString *const TVSocialFeedCommentCreateDateKey2    = @"CreateDate";

@interface TVSubSocialComment ()
{
    
}
@property (retain, readwrite) NSDate *createDate;


@end


@implementation TVSubSocialComment


- (void)dealloc
{
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    NSString *creationDateString = [dictionary objectOrNilForKey:TVSocialFeedCommentCreateDateKey2];
    self.createDate =  [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:creationDateString andFormat:SrverDateFormat2];
    
}

@end
