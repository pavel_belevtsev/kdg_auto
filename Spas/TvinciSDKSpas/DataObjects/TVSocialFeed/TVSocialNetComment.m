//
//  TVSocialNetComment.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialNetComment.h"
#import "TVSubSocialComment.h"

NSString *const TVSocialFeedCommentItemLinkKey = @"FeedItemLink";
NSString *const TVSocialCommentsKey         = @"Comments";

@interface TVSocialNetComment ()
{
    
}

@property (retain, readwrite) NSArray *subComments;
@property (retain, readwrite) NSURL *feedItemLinkUrl;

@end


@implementation TVSocialNetComment



- (void)dealloc
{
    self.feedItemLinkUrl = nil;
    self.subComments = nil;
    [super dealloc];
}


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    self.feedItemLinkUrl    = [NSURL URLWithString:[dictionary objectOrNilForKey:TVSocialFeedCommentItemLinkKey]];
    NSMutableArray* tempArr = [NSMutableArray array];
    NSArray*  comments = [dictionary objectOrNilForKey:TVSocialCommentsKey];
    for (int i = 0; i < comments.count; i++) {
        NSDictionary* dic = comments[i];
        TVSubSocialComment* subComment = [[[TVSubSocialComment alloc] initWithDictionary:dic] autorelease];
        [tempArr addObject:subComment];
    }
	self.subComments = [NSArray arrayWithArray:tempArr];
    
}

-(NSString*)description
{
    NSString * superD = [super description];
    return [NSString stringWithFormat:@"self.feedItemLinkUrl= %@, subComments = %@ , %@",self.feedItemLinkUrl,self.subComments,superD];
}
@end
