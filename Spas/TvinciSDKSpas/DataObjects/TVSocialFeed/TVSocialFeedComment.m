//
//  TVSocialFeedComment.m
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialFeedComment.h"
#import "NSDate+Format.h"

NSString *const SrverDateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZ";
NSString *const TwitterDateFormat = @"dd/MM/yyyy HH:mm:ssZZZZ";

NSString *const TVSocialFeedCommentTitleKey  = @"Title";
NSString *const TVSocialFeedCommentBodyKey    = @"Body";
NSString *const TVSocialFeedCommentCreatorNameKey    = @"CreatorName";
NSString *const TVSocialFeedCommentCreateDateKey    = @"CreateDate";
NSString *const TVSocialFeedCommentCreatorImageUrlKey    = @"CreatorImageUrl";
NSString *const TVSocialFeedCommentPopularityCounterKey    = @"PopularityCounter";


@interface TVSocialFeedComment ()
{
    
}
@property (retain, readwrite) NSString *commentBody;
@property (retain, readwrite) NSString *commentTitle;
@property (retain, readwrite) NSString *creatorName;
@property (retain, readwrite) NSDate *createDate;
@property (retain, readwrite) NSURL *creatorUrl;
@property (assign, readwrite) NSInteger popularityCounter;


@end



@implementation TVSocialFeedComment


- (void)dealloc
{
    self.commentBody = nil;
    self.commentTitle = nil;
    self.creatorName = nil;
    self.creatorUrl = nil;
    self.createDate = nil;
    
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];

    self.commentBody        = [dictionary objectOrNilForKey:TVSocialFeedCommentBodyKey];
    self.commentTitle       = [dictionary objectOrNilForKey:TVSocialFeedCommentTitleKey];
    self.creatorName        = [dictionary objectOrNilForKey:TVSocialFeedCommentCreatorNameKey];
    self.creatorUrl         = [NSURL URLWithString:[dictionary objectOrNilForKey:TVSocialFeedCommentCreatorImageUrlKey]];
    self.popularityCounter  = [[dictionary objectOrNilForKey:TVSocialFeedCommentPopularityCounterKey] integerValue];
    
    NSString *creationDateString = [dictionary objectOrNilForKey:TVSocialFeedCommentCreateDateKey];
    
    self.createDate =  [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:creationDateString andFormat:SrverDateFormat];
    
    if (self.createDate == nil)
    {
        self.createDate =  [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:creationDateString andFormat:TwitterDateFormat];
    }
}

/*

 */
-(NSString *)description
{
    return [NSString stringWithFormat:@"commentBody = %@ , commentTitle = %@ , creatorName = %@ , createDate = %@ , creatorUrl = %@ , popularityCounter = %ld",self.commentBody,self.commentTitle,self.creatorName,self.createDate,self.creatorUrl,(long)self.popularityCounter];
}
@end
