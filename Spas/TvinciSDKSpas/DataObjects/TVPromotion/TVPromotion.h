//
//  TVPromotion.h
//  TvinciSDK
//
//  Created by Tarek Issa on 12/12/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVPromotion : BaseModelObject {
    
}

/*!
 @abstract Promotion URL.
 */
@property (retain, readonly) NSURL *    PromotionURL;
@property (retain, readonly) NSString * PromotionText;

@end
