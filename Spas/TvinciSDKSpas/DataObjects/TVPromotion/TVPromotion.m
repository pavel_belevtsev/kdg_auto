//
//  TVPromotion.m
//  TvinciSDK
//
//  Created by Tarek Issa on 12/12/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVPromotion.h"

NSString *const TVPromotionLink = @"PromotionLink";

@interface TVPromotion ()
{

}

@property (retain, readwrite) NSURL *PromotionURL;
@property (retain, readwrite) NSString *PromotionText;

@end


@implementation TVPromotion
@synthesize PromotionURL = _PromotionURL;
@synthesize PromotionText = _PromotionText;

- (void)dealloc
{
    self.PromotionText = nil;
    self.PromotionURL = nil;
    [super dealloc];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary {
    NSString* PromotionStr = [dictionary objectOrNilForKey:TVPromotionLink];
    if (PromotionStr) {
        _PromotionURL = [[NSURL alloc] initWithString:PromotionStr];
    }
#warning should take this from dictionary, now it is not supported.
    _PromotionText = [[NSString alloc] initWithString:@"This is a temporary text"];
}


@end
