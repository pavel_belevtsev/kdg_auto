//
//  TVFacebookConfig.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/19/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVFacebookConfig.h"

#define KEY_APP_ID @"appId"
#define KEY_SCOPE @"scope"

@implementation TVFacebookConfig


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.appID = [dictionary objectForKey:KEY_APP_ID];
    NSString * scopeString = [dictionary objectForKey:KEY_SCOPE];
    self.scopeElements =[scopeString componentsSeparatedByString:@","];
}

-(void) dealloc
{
    [_scopeElements release];
    [_appID release];
    [super dealloc];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"Facebook configuration app id:%@ scope elements:%@",self.appID,self.scopeElements];
}

@end
