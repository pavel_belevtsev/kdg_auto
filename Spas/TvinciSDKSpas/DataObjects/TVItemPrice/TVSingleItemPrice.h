//
//  TVSingleItemPrice.h
//  TvinciSDK
//
//  Created by Tarek Issa on 11/27/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface PriceObj : BaseModelObject {
    
}

@property (nonatomic, assign) float price;
@property (nonatomic, assign) NSInteger CurrencyID;
@property (nonatomic, assign) NSString* CurrencySign;
@property (nonatomic, assign) NSString* CurrencyCD2;
@property (nonatomic, assign) NSString* CurrencyCD3;

@end





@interface TVSingleItemPrice : BaseModelObject {
    
}
@property (nonatomic, assign) TVPriceType priceReason;
@property (nonatomic, assign) NSInteger couponStatus;
@property (nonatomic, assign) BOOL SubscriptionOnly;
@property (nonatomic, retain) NSString* PPVmoduleCode;
@property (nonatomic, retain) NSString* relevantSub;
@property (nonatomic, retain) NSString* relevantPP;
@property (nonatomic, retain) PriceObj* price;
@property (nonatomic, retain) PriceObj* fullPrice;
@property (nonatomic, retain) NSString* ppvLanguageCode3;
@property (nonatomic, retain) NSString* ppvValue;
@property (nonatomic, retain) NSString* firstDeviceName;

@end

extern NSString *const TVItemPrice_sPPVModuleCodeKey;
extern NSString *const TVItemPrice_bSubscriptionOnlyKey;
extern NSString *const TVItemPrice_oPriceKey;
extern NSString *const TVItemPrice_dPriceKey;
extern NSString *const TVItemPrice_oCurrencyKey;
extern NSString *const TVItemPrice_oFullPriceKey;
extern NSString *const TVItemPrice_PriceReasonKey;
extern NSString *const TVItemPrice_relevantSubKey;
extern NSString *const TVItemPrice_relevantPPKey;
extern NSString *const TVItemPrice_oPPVDescriptionKey;
extern NSString *const TVItemPrice_couponStatusKey;
extern NSString *const TVItemPrice_CurrencyKey;
extern NSString *const TVItemPrice_CurrencyCD3Key;
extern NSString *const TVItemPrice_CurrencyCD2Key;
extern NSString *const TVItemPrice_CurrencySignKey;
extern NSString *const TVItemPrice_CurrencyIDKey;
extern NSString *const TVItemPrice_ppvLanguageKey;
extern NSString *const TVItemPrice_ppvValueKey;


