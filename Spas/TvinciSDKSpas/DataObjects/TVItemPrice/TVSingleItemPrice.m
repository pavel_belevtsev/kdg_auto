//
//  TVSingleItemPrice.m
//  TvinciSDK
//
//  Created by Tarek Issa on 11/27/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVSingleItemPrice.h"




NSString *const TVItemPrice_sPPVModuleCodeKey  = @"m_sPPVModuleCode";
NSString *const TVItemPrice_bSubscriptionOnlyKey  = @"m_bSubscriptionOnly";
NSString *const TVItemPrice_oPriceKey        = @"m_oPrice";
NSString *const TVItemPrice_dPriceKey        = @"m_dPrice";
NSString *const TVItemPrice_oCurrencyKey     = @"m_oCurrency";
NSString *const TVItemPrice_oFullPriceKey    = @"m_oFullPrice";
NSString *const TVItemPrice_PriceReasonKey   = @"m_PriceReason";
NSString *const TVItemPrice_relevantSubKey   = @"m_relevantSub";
NSString *const TVItemPrice_relevantPPKey    = @"m_relevantPP";
NSString *const TVItemPrice_oPPVDescriptionKey   = @"m_oPPVDescription";
NSString *const TVItemPrice_CurrencyKey      = @"m_oCurrency";
NSString *const TVItemPrice_CurrencyCD3Key   = @"m_sCurrencyCD3";
NSString *const TVItemPrice_CurrencyCD2Key   = @"m_sCurrencyCD2";
NSString *const TVItemPrice_CurrencySignKey  = @"m_sCurrencySign";
NSString *const TVItemPrice_CurrencyIDKey    = @"m_nCurrencyID";
NSString *const TVItemPrice_ppvLanguageKey   = @"m_sLanguageCode3";
NSString *const TVItemPrice_ppvValueKey      = @"m_sValue";
NSString *const TVItemPrice_couponStatusKey  = @"m_couponStatus";
NSString *const TVItemPrice_FirstDeviceNameFoundKey  = @"m_sFirstDeviceNameFound";

@implementation TVSingleItemPrice
@synthesize priceReason     = _priceReason;
@synthesize SubscriptionOnly = _SubscriptionOnly;
@synthesize PPVmoduleCode   = _PPVmoduleCode;
@synthesize relevantSub     = _relevantSub;
@synthesize relevantPP      = _relevantPP;
@synthesize couponStatus    = _couponStatus;
@synthesize price           = _price;
@synthesize fullPrice       = _fullPrice;
@synthesize ppvLanguageCode3 = _ppvLanguageCode3;
@synthesize ppvValue        = _ppvValue;
@synthesize firstDeviceName = _firstDeviceName;

#pragma mark - Memory
-(void) dealloc
{
    _firstDeviceName = nil;
    _PPVmoduleCode = nil;
    _relevantSub = nil;
    _relevantPP = nil;
    _price = nil;
    _fullPrice = nil;
    _ppvLanguageCode3 = nil;
    _ppvValue = nil;
    [_price release];
    [_fullPrice release];
    [super dealloc];
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"[Price: %ld  Full Price: %ld]", (long)_price.price, (long)_fullPrice.price];
}

#pragma mark - Initializations

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    ASLogDebug(@"%@", dictionary);
    _priceReason        = (TVPriceType)[[dictionary objectOrNilForKey:TVItemPrice_PriceReasonKey] integerValue];
    _SubscriptionOnly   = [[dictionary objectOrNilForKey:TVItemPrice_bSubscriptionOnlyKey] boolValue];
    _PPVmoduleCode      = [dictionary objectOrNilForKey:TVItemPrice_sPPVModuleCodeKey];
    
#warning Assuming _relevantPP is NSSTring, need to ask backend what is the exact type of it
    //    _relevantSub        = [dictionary objectOrNilForKey:TVItemPrice_relevantSubKey];
#warning Assuming _relevantPP is NSSTring, need to ask backend what is the exact type of it
    //    _relevantPP         = [dictionary objectOrNilForKey:TVItemPrice_relevantPPKey];
    _couponStatus       = [[dictionary objectOrNilForKey:TVItemPrice_couponStatusKey] integerValue];
    _price              = [[PriceObj alloc] initWithDictionary:[dictionary objectOrNilForKey:TVItemPrice_oPriceKey]];
    _fullPrice          = [[PriceObj alloc] initWithDictionary:[dictionary objectOrNilForKey:TVItemPrice_oFullPriceKey]];
    _firstDeviceName    = [dictionary objectOrNilForKey:TVItemPrice_FirstDeviceNameFoundKey];
    
    NSArray* ppvDexcriptionArr = [dictionary objectOrNilForKey:TVItemPrice_oPPVDescriptionKey];
    
    if ([ppvDexcriptionArr count] > 0)
    {
        NSDictionary* ppvDexcriptionDic = [ppvDexcriptionArr objectAtIndex:0];
        
        _ppvLanguageCode3   = [ppvDexcriptionDic objectOrNilForKey:TVItemPrice_ppvLanguageKey];
        _ppvValue           = [ppvDexcriptionDic objectOrNilForKey:TVItemPrice_ppvValueKey];
    }
}


@end





@implementation PriceObj
@synthesize price       = _price;
@synthesize CurrencyID  = _CurrencyID;
@synthesize CurrencySign = _CurrencySign;
@synthesize CurrencyCD2 = _CurrencyCD2;
@synthesize CurrencyCD3 = _CurrencyCD3;

-(void) dealloc
{
    _CurrencySign = nil;
    _CurrencyCD2 = nil;
    _CurrencyCD3 = nil;
    [super dealloc];
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"%f", _price];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    _price       = [[dictionary objectOrNilForKey:TVItemPrice_dPriceKey] floatValue];
    NSDictionary* currencyDic = [dictionary objectOrNilForKey:TVItemPrice_CurrencyKey];
    if (currencyDic) {
        _CurrencyID       = [[currencyDic objectOrNilForKey:TVItemPrice_CurrencyIDKey] integerValue];
        _CurrencySign       = [currencyDic objectOrNilForKey:TVItemPrice_CurrencySignKey];
        _CurrencyCD2       = [currencyDic objectOrNilForKey:TVItemPrice_CurrencyCD2Key];
        _CurrencyCD3       = [currencyDic objectOrNilForKey:TVItemPrice_CurrencyCD3Key];
        
    }
    
}

@end
