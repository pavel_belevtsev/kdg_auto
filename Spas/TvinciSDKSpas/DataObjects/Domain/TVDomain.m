//
//  TVDomain.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/10/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVDomain.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVDeviceFamily.h"
#import "SmartLogs.h"
#import "NSDate+Format.h"
#import "TVHomeNetworkInfo.h"

NSString *const TVDomainIDKey = @"m_nDomainID";
NSString *const TVDomainDomainStatusKey = @"m_DomainStatus";
NSString *const TVDomainUserIDsKey = @"m_UsersIDs";
NSString *const TVDomainDeviceFamiliesKey = @"m_deviceFamilies";
NSString *const TVDomainMasterGUIDsKey = @"m_masterGUIDs";
NSString *const TVDomainGroupIDKey = @"m_nGroupID";
NSString *const TVDomainIsActiveKey = @"m_nIsActive";
NSString *const TVDomainLimitKey = @"m_nLimit";
NSString *const TVDomainStatusKey = @"m_nStatus";
NSString *const TVDomainDescriptionKey = @"m_sDescription";


NSString *const TVDomainDeviceLimitKey = @"m_nDeviceLimit";
NSString *const TVDomainUserLimitKey = @"m_nUserLimit";
NSString *const TVDomainConcurrentLimitKey = @"m_nConcurrentLimit";

NSString *const TVDomainNextUserActionFreqKey = @"m_NextUserActionFreq";
NSString *const TVDomainSSOOperatorIDKey = @"m_nSSOOperatorID";
NSString *const TVDomainRestrictionKey = @"m_DomainRestriction";
NSString *const TVDomainfrequencyFlagKey = @"m_frequencyFlag";
NSString *const TVDomainNextActionFreqKey = @"m_NextActionFreq";
NSString *const TVDomainPendingUsersIDsKey = @"m_PendingUsersIDs";
NSString *const TVDomainDefaultUsersIDsKey = @"m_DefaultUsersIDs";

NSString *const TVDomainHomeNetworksKey = @"m_homeNetworks";
NSString *const TVDomainCoGuidKey = @"m_sCoGuid";


NSString *const TVDomainNameKey = @"m_sName";
NSString *const TVDomainKey = @"m_oDomain";

@implementation TVDomain
@synthesize limit;
@synthesize groupID;
@synthesize userIDs;
@synthesize domainID;
@synthesize isActive;
@synthesize domainName;
@synthesize masterGUIDs;
@synthesize domainStatus;
@synthesize deviceFamilies;
@synthesize domainDescription;
@synthesize status;

-(NSString *) description
{
    return [NSString stringWithFormat:@"domainID = %ld , domainStatus = %ld",(long)self.domainID,(long)self.domainStatus];
}

-(void) dealloc
{
    self.domainName = nil;
    self.domainDescription = nil;
    self.userIDs = nil;
    self.deviceFamilies = nil;
    self.masterGUIDs = nil;
    [super dealloc];
}

-(NSArray *) parseDeviceFamilies : (NSArray *) deviceFamiliesAsDictionary
{
    NSMutableArray *temp = [NSMutableArray array];
    
    for (NSDictionary *familyDictionary in deviceFamiliesAsDictionary)
    {
        TVDeviceFamily *family = [[TVDeviceFamily alloc] initWithDictionary:familyDictionary];
        if (family != nil)
        {
            [temp addObject:family];
        }
        [family release];
    }
    return [NSArray arrayWithArray:temp];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    ASLog2Debug(@"DOmain Dictionary: %@", dictionary);////////
    
    [super setAttributesFromDictionary:dictionary];
    self.limit = [[dictionary objectOrNilForKey:TVDomainLimitKey] integerValue];
    self.groupID = [[dictionary objectOrNilForKey:TVDomainGroupIDKey] integerValue];
    //NSLog(@"groupID = %ld",(long)self.groupID);
    
    self.userIDs = [dictionary objectOrNilForKey:TVDomainUserIDsKey];
    self.domainID = [[dictionary objectOrNilForKey:TVDomainIDKey] integerValue];
    self.isActive = [[dictionary objectOrNilForKey:TVDomainIsActiveKey] boolValue];
    self.domainName = [dictionary objectOrNilForKey:TVDomainNameKey];
    self.masterGUIDs = [dictionary objectOrNilForKey:TVDomainMasterGUIDsKey];
    
    self.domainStatus = [[dictionary objectOrNilForKey:TVDomainDomainStatusKey] integerValue];
    // NSLog(@"domainStatus = %ld",(long)self.domainStatus);
    
    self.deviceFamilies = [self parseDeviceFamilies:[dictionary objectOrNilForKey:TVDomainDeviceFamiliesKey]];
    self.domainDescription = [dictionary objectOrNilForKey:TVDomainDescriptionKey];
    self.status = [[dictionary objectOrNilForKey:TVDomainStatusKey] integerValue];
    
    self.deviceLimit = [[dictionary objectOrNilForKey:TVDomainDeviceLimitKey] integerValue];
    self.userLimit = [[dictionary objectOrNilForKey:TVDomainUserLimitKey] integerValue];
    self.concurrentLimit = [[dictionary objectOrNilForKey:TVDomainConcurrentLimitKey] integerValue];
    self.frequencyFlag = [[dictionary objectOrNilForKey:TVDomainfrequencyFlagKey] integerValue];
    self.domainRestriction = [[dictionary objectOrNilForKey:TVDomainRestrictionKey] integerValue];
    
    self.ssoOperatorID = [[dictionary objectOrNilForKey:TVDomainSSOOperatorIDKey] integerValue];
    self.defaultUsersIDs = [dictionary objectOrNilForKey:TVDomainDefaultUsersIDsKey];
    self.pendingUsersIDs = [dictionary objectOrNilForKey:TVDomainPendingUsersIDsKey];
    
    NSString *dateString = [dictionary objectOrNilForKey:TVDomainNextActionFreqKey];
    
    self.nextActionFreq =  [NSDate FriendlyDateTimeByString:dateString];
    
    dateString = [dictionary objectOrNilForKey:TVDomainNextUserActionFreqKey];
    
    self.nextUserActionFreq =  [NSDate FriendlyDateTimeByString:dateString];
    
    self.coGuid = [[dictionary objectOrNilForKey:TVDomainCoGuidKey] integerValue];
    
    NSArray * netHomes = [dictionary objectOrNilForKey:TVDomainHomeNetworksKey];

    NSMutableArray * tempArr = [NSMutableArray array];
    for (NSDictionary * dic in netHomes) {
        TVHomeNetworkInfo * home = [[TVHomeNetworkInfo alloc] initWithDictionary:dic];
        if (home)
        {
            [tempArr addObject:home];
        }
    }
    self.homeNetworks = [NSArray arrayWithArray:tempArr];
    
    ASLogInfo(@"self.homeNetworks = %@",self.homeNetworks);
    /*
     
     NSDate *;      //
     NSDate *nextUserActionFreq;  //
     
     
     // Old code - time should change timezone from outside SDK
     //    self.currentDate = [self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
     
     */
    
    
}
@end
