//
//  AdRequestContentMetadata.m
//  TvinciSDK
//
//  Created by iosdev1 on 6/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "AdRequestContentMetadata.h"

@implementation AdRequestContentMetadata

-(void)dealloc
{
    [_category release];
    [_contentIdentifier release];
    [_contentPartner release];
    [_flags release]; 
    [_tags release];
    
    [super dealloc];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"\n tags = %@ \n category = %@ \n contentIdentifier = %@ \n",self.tags,self.category,_contentIdentifier];
    //contentPartner=%@ //_contentPartner
}
@end
