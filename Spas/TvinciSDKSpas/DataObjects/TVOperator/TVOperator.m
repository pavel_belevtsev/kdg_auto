//
//  TVOperator.m
//  TvinciSDK
//
//  Created by Tarek Issa on 12/12/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVOperator.h"


NSString *const MainMenuIDKeyName = @"MainMenuID";


@interface TVOperator ()
{
}
@property (retain, readwrite) NSString *OperatorID;
@property (retain, readwrite) NSArray *menuIDs;

@end


@implementation TVOperator
@synthesize OperatorID = _OperatorID;

- (void)dealloc
{
    self.OperatorID = nil;
    self.menuIDs = nil;
    [super dealloc];
}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary {
    NSArray* keysArr = [dictionary allKeys];
    if (keysArr) {
        NSString* key = [keysArr lastObject];
        _OperatorID = [[NSString alloc] initWithString:key];
        NSArray* menuIdsArr = [dictionary objectForKey:key];
        NSMutableArray* tmpMutArr = [[[NSMutableArray alloc] initWithCapacity:menuIdsArr.count] autorelease];
        for (NSDictionary* dict in menuIdsArr) {
            NSString* value = [dict objectOrNilForKey:MainMenuIDKeyName];
            if (value) {
                [tmpMutArr addObject:value];
            }
        }
        _menuIDs = [[NSArray alloc] initWithArray:tmpMutArr];
    }
}

@end
