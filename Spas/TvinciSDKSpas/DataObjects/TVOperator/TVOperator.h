//
//  TVOperator.h
//  TvinciSDK
//
//  Created by Tarek Issa on 12/12/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVOperator : BaseModelObject {
    
}

@property (retain, readonly) NSString *OperatorID;
@property (retain, readonly) NSArray *menuIDs;


@end
