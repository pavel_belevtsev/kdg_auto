//
//  TVSocialUtils.m
//  TvinciSDK
//
//  Created by Tarek Issa on 1/15/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialUtils.h"
#import "TvinciSDK.h"

#define KeyValue @"obj:url"


NSString * const TVNDoUserActionObjUrlKey = @"obj:url";
NSString * const TVNDoUserActionKeyKey = @"key";
NSString * const TVNDoUserActionValueKey = @"value";

@implementation TVSocialUtils



+ (TVPAPIRequest *)RequestForDoLikeForMediaItem : (TVMediaItem*)mediaItem delegate : (id<ASIHTTPRequestDelegate>) delegate
{
    if (!mediaItem || !mediaItem.mediaID )
    {
        return nil;
    }
    
    if ([mediaItem.mediaWebLink absoluteString] == nil)
    {
        mediaItem.mediaWebLink =[NSURL URLWithString:@""];
    }
    NSArray * arrayAddtionalInfo = @[@{TVNDoUserActionKeyKey : TVNDoUserActionObjUrlKey, TVNDoUserActionValueKey : [mediaItem.mediaWebLink absoluteString]}];
    
    TVPAPIRequest* request =[TVPSocialAPI requestForDoUserActionWithUserAction:TVUserSocialActionLIKE
                                                                     assetType:TVAssetType_MEDIA
                                                                       assetID:mediaItem.mediaID.integerValue
                                                                   extraParams:arrayAddtionalInfo
                                                                SocialPlatform:TVSocialPlatformFacebook delegate:delegate];
    return request;
}

@end
