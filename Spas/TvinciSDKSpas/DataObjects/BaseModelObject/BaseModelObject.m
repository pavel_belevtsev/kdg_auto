//
//  BaseModelObject.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "ASIHTTPRequest.h"
#import "NSDictionary+NSNullAvoidance.h"
@interface BaseModelObject ()
@end

@implementation BaseModelObject

-(void) dealloc
{
    [self unregisterForMemoryWarningNotifications];
    [super dealloc];
}

#pragma mark - Getters

#pragma mark - Initialization
-(id) initWithDictionary:(NSDictionary *)dictionary
{
    if (self = [self init])
    {
        [self setAttributesFromDictionary:[dictionary dictionaryByRemovingNSNulls]];
    }
    return self;
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    // Leave for subclasses to implement
}

-(void) registerForMemoryWarningNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMemoryWarning) 
                                                 name:UIApplicationDidReceiveMemoryWarningNotification 
                                               object:nil];
}

-(void) unregisterForMemoryWarningNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIApplicationDidReceiveMemoryWarningNotification 
                                                  object:nil];
}

-(void) didReceiveMemoryWarning
{
}

#pragma mark - KVC

-(NSDictionary *) keyValueRepresentation
{
    return [self dictionaryWithValuesForKeys:[self attributeKeys]];
}

-(NSArray *) attributeKeys
{
    return [NSArray array];
}

#pragma mark - Notification Sending
-(void) postNotification : (NSString *) notification 
                userInfo : (NSDictionary *) userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notification 
                                                        object:self userInfo:userInfo];
}

#pragma mark - Asynchronic Dangerous Stuff
@end
