//
//  TVPictureSize.m
//  TVinci
//
//  Created by Avraham Shukron on 5/30/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPictureSize.h"

@interface TVPictureSize ()
@property (nonatomic , copy , readwrite) NSString *name;
@property (nonatomic , assign, readwrite)  CGSize size;
@end

@implementation TVPictureSize
@synthesize name = _name;
@synthesize size = _size;

#pragma mark - Memory
-(void) dealloc
{
    self.name = nil;
    [super dealloc];
}

#pragma mark - Getter
-(NSString *) name
{
    if (_name == nil)
    {
        return [NSString stringWithFormat:@"%dX%d",(int)self.size.width,(int)self.size.height];
    }
    else 
    {
        return _name;
    }
}

#pragma mark - API
-(id) initWithSize : (CGSize) size
{
    if (self = [super init])
    {
        self.name = nil;
        self.size = size;
    }
    return self;
}

-(id) initWithName : (NSString *) name
{
    if (self = [super init])
    {
        self.name = name;
    }
    return self;
}

+(id) pictureSizeWithSize : (CGSize) size
{
    return [[[TVPictureSize alloc] initWithSize:size] autorelease];
}

+(id) pictureSizeWithName : (NSString *) name
{
    return [[[TVPictureSize alloc] initWithName:name] autorelease];
}

+(TVPictureSize *) iPhoneSpotlightItemPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(244, 137)];
    }
    return aSize;
}

+(TVPictureSize *) iPhoneItemPosterPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(320 , 180)];
    }
    return aSize;
}

+(TVPictureSize *) iPhoneItemPosterPictureSize320X480
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(320 , 480)];
    }
    return aSize;
}

+(TVPictureSize *) iPhoneItemCellPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(158, 89)];
    }
    return aSize;
}

+(TVPictureSize *) iPhonePurchaseItemPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(244, 137)];
    }
    return aSize;
}

+(TVPictureSize *) iPhone2x3HomeGalleryPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(121, 182)];
    }
    return aSize;
}

+(TVPictureSize *) iPhone2x3GalleryPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(150, 225)];
    }
    return aSize;
}

+(TVPictureSize *) fullPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithName:@"full"];
        
    }
    return aSize;
}

+(TVPictureSize *) fullIpadPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(956, 538)];
    }
    return aSize;
}

+(TVPictureSize *) iPadMovieTilePictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        //aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(200, 300)];
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(361, 203)];
    }
    return aSize;
}

+(TVPictureSize *) iPadSeriesTilePictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        //aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(266, 150)];
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(361, 203)];
    }
    return aSize;
}

+(TVPictureSize *) iPadItemCellPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(144, 81)];
    }
    return aSize;
}

+(TVPictureSize *) iPadSpotlightSmallPictureSize252x142
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(252, 142)];
    }
    return aSize;
}


+(TVPictureSize *) iPadHome3x3GridPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(282, 159)];
    }
    return aSize;
}

+(TVPictureSize *) iPadMyZoneTile246X368
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(246, 368)];
    }
    return aSize;
}


+(TVPictureSize *) iPadHome2x2GridPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(444, 250)];
    }
    return aSize;
}

+(TVPictureSize *) iPadMegaSpotlightPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(1024,652)];
    }
    return aSize;
}

+(TVPictureSize *) iPadSpotlightSmallPictureSize
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(230 , 130)];
    }
    return aSize;
}

+(TVPictureSize *) iPadEpgChnnel50X50
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(50, 50)];
    }
    return aSize;
}

+(TVPictureSize *) iPadEpgChnnel45X45
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(45, 45)];
    }
    return aSize;
}

+(TVPictureSize *) iPadEpgChnnel128X128
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(128, 128)];
    }
    return aSize;
}

+(TVPictureSize *) iPadEpgChnnel100X100
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(100, 100)];
    }
    return aSize;
}


+(TVPictureSize *) iPadGrig169X253
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(169, 253)];
    }
    return aSize;
}

+(TVPictureSize *) iPadShowPage256X384
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(256, 384)];
    }
    return aSize;
}

+(TVPictureSize *) vodInfo564X318
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(564, 318)];
    }
    return aSize;
}

+(TVPictureSize *) vodInfo888X500
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(888 , 500)];
    }
    return aSize;
}

+(TVPictureSize *) vodInfo460X260
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(460, 260)];
    }
    return aSize;
}


+(TVPictureSize *) iPadShowPage145X216
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(145, 216)];
    }
    return aSize;
}

+(TVPictureSize *) iPadHomePage397X596
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(397, 596)];
    }
    return aSize;
}


+(TVPictureSize *) iPadHomePage191X286
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(191, 286)];
    }
    return aSize;
}

+(TVPictureSize *) iPadLightBoxPage219X329
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(219, 329)];
    }
    return aSize;
}

+(TVPictureSize *) iPadLiveChannleImage219X123
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(219, 123)];
    }
    return aSize;
}


+(TVPictureSize *) iPadMyZoonPage123X184
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(123, 184)];
    }
    return aSize;
}

+(TVPictureSize *) iPadDetailUnderPlayer105X157
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(105, 157)];
    }
    return aSize;
}

+(TVPictureSize *) iPadRlated137X206
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(137, 206)];
    }
    return aSize;
}

+(TVPictureSize *) iPadRlated186X104
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(186, 104)];
    }
    return aSize;
}

+(TVPictureSize *) iPadRlated153X86
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(153, 86)];
    }
    return aSize;
}

+(TVPictureSize *) iPadRlated222X125
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(222, 125)];
    }
    return aSize;
}

+(TVPictureSize *) iPadPackagePromotionPictureSize652x412
{
    static TVPictureSize *aSize = nil;
    if (aSize == nil)
    {
        aSize = [[TVPictureSize alloc] initWithSize:CGSizeMake(652,412)];
    }
    return aSize;
}

-(NSString *) description
{
    return [self name];
}
@end
