//
//  TVCompanionDevice.m
//  TvinciSDK
//
//  Created by Tarek Issa on 3/18/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVCompanionDevice.h"
#import <XMLReader.h>
#import "TVCompanionAPI.h"


#define MESSAGE_CACHE_CONTROL   [@"CACHE-CONTROL:" lowercaseString]
#define MESSAGE_DATE            [@"DATE:" lowercaseString]
#define MESSAGE_EXT             [@"EXT:" lowercaseString]
#define MESSAGE_LOCATION        [@"LOCATION:" lowercaseString]
#define MESSAGE_OPT             [@"OPT:" lowercaseString]
#define MESSAGE_SERVER          [@"SERVER:" lowercaseString]
#define MESSAGE_X_User_Agent    [@"X-User-Agent:" lowercaseString]
#define MESSAGE_ST              [@"ST:" lowercaseString]
#define MESSAGE_USN             [@"USN:" lowercaseString]


@interface TVCompanionDevice ()

@property (retain) NSString *CacheControl;
@property (retain) NSDate *date;
@property (retain) NSString *ext;
@property (retain) NSURL *location;
@property (retain) NSString *OPT;
@property (retain) NSString *NLS;
@property (retain) NSString *server;
@property (retain) NSString *userAgent;
@property (retain) NSString *ST;
@property (retain) NSString *USN;
@property (retain) NSString *friendlyName;
@property (retain) NSString *basUrlString;
//@property (retain) NSString *udid;
@property (retain) NSString *ipAddress;
@property (retain) NSString *port;


@end




@implementation TVCompanionDevice


- (void)dealloc
{
    self.CacheControl = nil;
    self.friendlyName = nil;
    self.userAgent = nil;
    self.ipAddress = nil;
    self.location = nil;
    self.server = nil;
    self.udid   = nil;
    self.port   = nil;
    self.date   = nil;
    self.USN    = nil;
    self.OPT    = nil;
    self.NLS    = nil;
    self.ext    = nil;
    self.ST     = nil;
    
    [super dealloc];
}

- (instancetype)initWithString:(NSString *)string
{
    self = [super init];
    if (self) {
        [self setup:string];
    }
    return self;
}

- (void)setup:(NSString*)string
{
    NSArray* arr = [string componentsSeparatedByString:@"\n"];
    for (NSString* peice in arr) {
        
        peice = [peice stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        peice = [peice stringByReplacingOccurrencesOfString:@": " withString:@":"];
        NSString* finalStr = [NSString stringWithString:peice];
        
        peice = [peice lowercaseString];
        if ([self is:MESSAGE_CACHE_CONTROL substringOf:peice]) {
            
            self.CacheControl = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_CACHE_CONTROL.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_DATE substringOf:peice]) {
            
            NSString* dateString = [peice stringByReplacingOccurrencesOfString:MESSAGE_DATE withString:@""];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // this is imporant - we set our input date format to match our input string
            // if format doesn't match you'll get nil from your string, so be careful
            [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZZ"];
            self.date = [dateFormatter dateFromString:dateString];
            [dateFormatter release];
            continue;
        }
        
        if ([self is:MESSAGE_EXT substringOf:peice]) {
            self.ext = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_EXT.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_LOCATION substringOf:peice])
        {
            NSString * text = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_LOCATION.length) withString:@""];
            self.location =  [NSURL URLWithString:[finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_LOCATION.length) withString:@""]];
            NSArray* arr1 = [text componentsSeparatedByString:@":"];
            if (arr1.count>1)
            {
                self.ipAddress = [NSString stringWithFormat:@"%@:%@",[arr1 objectAtIndex:0] ,[arr1 objectAtIndex:1]];
                ASLogInfo(@"ipAddress => %@",self.ipAddress);
            }
            continue;
        }
        
        if ([self is:MESSAGE_OPT substringOf:peice]) {
            self.OPT = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_OPT.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_SERVER substringOf:peice]) {
            self.server = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_SERVER.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_X_User_Agent substringOf:peice]) {
            self.userAgent = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_X_User_Agent.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_ST substringOf:peice]) {
            self.ST = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_ST.length) withString:@""];
            continue;
        }
        
        if ([self is:MESSAGE_USN substringOf:peice]) {
            self.USN = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_USN.length) withString:@""];
            NSString* temp = [finalStr stringByReplacingCharactersInRange:NSMakeRange(0, MESSAGE_USN.length) withString:@""];
            
            
            NSArray* arr = [[temp lowercaseString] componentsSeparatedByString:@"uuid:"];
            //  Is there any istance of "uudid"...
            if (arr.count > 0) {
                temp = [arr lastObject];
            }
            arr = [[temp lowercaseString] componentsSeparatedByString:@"::"];
            
            self.udid = [arr firstObject];
            
            continue;
        }
    }
}

-(NSString *)spoofRegex:(NSString *)regExPattern fromString:(NSString *)string {
    NSError* error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:&error];
    if (error) {
        return nil;
    }
    NSArray* arr = [regex matchesInString:string options:0 range:NSMakeRange(0, [string length])];
    
    NSTextCheckingResult *match = [arr firstObject];
    NSRange matchRange = [match range];
    return [string substringWithRange:matchRange];
}

- (BOOL)is:(NSString*)substring substringOf:(NSString*)string {
    if ([string hasPrefix:substring])
        return YES;
    
    return NO;
}

- (BOOL)parseXMLString:(NSString *)xmlStr {
    NSError *error = NULL;
    NSDictionary* xmlDic = [XMLReader dictionaryForXMLString:xmlStr error:&error];
    if (!error) {
        //  Url Base
        //        NSString* tempUrlBase = [[xmlDic objectOrNilForKey:@"root"] objectOrNilForKey:@"URLBase"];
        id urlBaseObj = [[xmlDic objectOrNilForKey:@"root"] objectOrNilForKey:@"URLBase"];
        NSString* tempUrlBase;
        if ([urlBaseObj isKindOfClass:[NSDictionary class]]) {
            tempUrlBase = [urlBaseObj objectOrNilForKey:@"text"];
        }else{
            tempUrlBase = urlBaseObj;
        }
        if (tempUrlBase) {
            
            tempUrlBase = [tempUrlBase stringByReplacingOccurrencesOfString:@"\r" withString:@""];
            tempUrlBase = [tempUrlBase stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            tempUrlBase = [tempUrlBase stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            self.basUrlString = [NSString stringWithString:tempUrlBase];
            
            NSString* ipRegExPattern = @"[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+";
            if ([self spoofRegex:ipRegExPattern fromString:tempUrlBase])
            {
                self.ipAddress = [self spoofRegex:ipRegExPattern fromString:tempUrlBase];
            }
            NSString* portRegExPattern = @":[0-9]+";
            self.port = [[self spoofRegex:portRegExPattern fromString:tempUrlBase] stringByReplacingOccurrencesOfString:@":" withString:@""];
        }
        return YES;
    }
    return NO;
}


- (void)changeFriendlyNameTo:(NSString *)newName {
    //  Parse Friendly Name
    self.friendlyName = newName;
}

- (void) downloadXmlDataWithCompletionBlock : (void (^)(NSError* error)) completion {
    
#warning isUrlReachable is not working properly. Always returns YES.
    //    if (![self isUrlReachable:url]) {
    //        ASLog2Debug(@"Not reachable");
    //
    //        NSError *error = [NSError errorWithDomain:url code:0 userInfo:nil];
    //        completion(nil, error);
    //        return ;
    //    }
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:[self.location absoluteString]]];
    [request setTimeoutInterval:3];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        NSError *error = NULL;
        
        NSHTTPURLResponse *responseCode = nil;
        NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
        
        if([responseCode statusCode] != 200){
            ASLog2Debug(@"Error getting %@, HTTP status code %li", url, (long)[responseCode statusCode]);
            completion(error);
            return ;
        }
        
        NSString* xmlStr = [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
        ASLog(@"xmlStr: %@", xmlStr);
        [self parseXMLString:xmlStr];
        completion(nil);
        return;
    }];
}


- (BOOL)isUrlReachable:(NSString *)urlStr {
    NSLog(@"urlStr: %@", urlStr);
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:2];
    NSURLResponse *response = nil;
    NSError *error = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([(NSHTTPURLResponse *)response statusCode] == 200) {
        NSLog(@"Good");
        return YES;
    }
    NSLog(@"Not Good");
    return NO;
}

//-(NSString *)udid_serverFormat {
//    if ([self.udid rangeOfString:@":"].location == NSNotFound) {
//        return [self.udid stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    }
//    return self.udid;
//}

/*
 @property (retain,readonly) NSString *friendlyName;
 @property (retain,readonly) NSString *basUrlString;
 @property (retain,readonly) NSString *CacheControl;
 @property (retain,readonly) NSString *userAgent;
 @property (retain,readonly) NSString *ipAddress;
 @property (retain,readonly) NSString *server;
 @property (retain,readonly) NSString *port;
 @property (retain,readwrite) NSString *udid;
 //@property (retain,readonly) NSString *udid_serverFormat;
 @property (retain,readonly) NSString *USN;
 @property (retain,readonly) NSString *ext;
 @property (retain,readonly) NSString *OPT;
 @property (retain,readonly) NSString *NLS;
 @property (retain,readonly) NSString *ST;
 @property (retain,readonly) NSDate *date;
 @property (retain,readonly) NSURL *location;*/

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@ , %@ , %@ , %@ , %@",self.friendlyName,self.basUrlString,self.ipAddress,self.server,self.port];
}
@end
