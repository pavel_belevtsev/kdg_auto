//
//  TVUser.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVUser.h"
#import "NSDictionary+NSNullAvoidance.h"

NSString *const TVUserUsernameKey = @"m_sUserName";
NSString *const TVUserFirstNameKey = @"m_sFirstName";
NSString *const TVUserLastNameKey = @"m_sLastName";
NSString *const TVUserEmailKey = @"m_sEmail";
NSString *const TVUserAddressKey = @"m_sAddress";
NSString *const TVUserCityKey = @"m_sCity";
NSString *const TVUserStateKey = @"m_State";
NSString *const TVUserCountryKey = @"m_Country";
NSString *const TVUserZipCodeKey = @"m_sZip";
NSString *const TVUserPhoneNumberKey = @"m_sPhone";
NSString *const TVUserFacebookIDKey = @"m_sFacebookID";
NSString *const TVUserFacebookImageKey = @"m_sFacebookImage";
NSString *const TVUserAffiliateCodeKey = @"m_sAffiliateCode";
NSString *const TVUserIsFacebookImagePermittedKey = @"m_bIsFacebookImagePermitted";
NSString *const TVUSerPreferedLoginType = @"TVUSerPreferedLoginType";
NSString *const TVUserDynamicDataKey = @"m_oDynamicData";
NSString *const TVUserBasicInfoKey = @"m_oBasicData";
NSString *const TVUserSiteGUIDKey = @"m_sSiteGUID";
NSString *const TVUserPurchasePinKey = @"m_oBasicData";
NSString *const TVUserPurchasePinEnableKey = @"m_sSiteGUID";

// addtional Keys

NSString * const TVUserExternalUserDynamicData =@"userDynamicData";
NSString * const TVUserExternalSiteGuidKey = @"sSiteGuid";
NSString * const TVUserExternalObjectIDKey = @"m_nObjecrtID";
NSString * const TVUserExternalStateNameKey = @"m_sStateName";
NSString * const TVUserExternalStateCodeKey = @"m_sStateCode";
NSString * const TVUserExternalCountryNameKey = @"m_sCountryName";
NSString * const TVUserExternalCountryCodeKey = @"m_sCountryCode";
NSString * const TVUserExternalCoGuidKeyKey = @"m_CoGuid";
NSString * const TVUserExternalTokenKey =@"m_ExternalToken";
NSString * const TVUserExternalFacebookTokenKey =@"m_sFacebookToken";
NSString * const TVUserExternalUserTypeKey =@"m_UserType";
NSString * const TVUserExternalIDKey =@"ID";
NSString * const TVUserExternalDescriptionKey =@"Description";
NSString * const TVUserExternalIsDefaultKey =@"IsDefault";
NSString * const TVUserExternalUserBasicData = @"userBasicData";





@implementation TVUser
@synthesize username = _username;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize email = _email;
@synthesize address = _address;
@synthesize city = _city;
@synthesize state = _state;
@synthesize country = _country;
@synthesize zipCode = _zipCode;
@synthesize phoneNumber = _phoneNumber;
@synthesize facebookID = _facebookID;
@synthesize facebookImage = _facebookImage;
@synthesize affiliateCode = _affiliateCode;
@synthesize isFacebookImagePermitted = _isFacebookImagePermitted;
@synthesize dynamicData = _dynamicData;
@synthesize siteGUID = _siteGUID;
@synthesize basicData = _basicData;
@synthesize isAllowNewsLetter;
@synthesize preferedLoginType = _preferedLoginType;


#pragma mark - Memory

-(void) dealloc
{
    self.username = nil;
    self.firstName = nil;
    self.lastName = nil;
    self.email = nil;
    self.address = nil;
    self.city = nil;
    self.state = nil;
    self.country = nil;
    self.zipCode = nil;
    self.phoneNumber = nil;
    self.facebookID = nil;
    self.facebookImage = nil;
    self.affiliateCode = nil;
    self.purchasePin = nil;
    self.coGuid = nil;
    self.externalToken = nil;
    self.faceBookToken = nil;
    self.userType =nil;
    self.basicData = nil;
    self.dynamicData = nil;
    self.siteGUID = nil;

    [super dealloc];
}


-(void) populateBasicInfoFromDictionary : (NSDictionary *) dictionary
{
    self.basicData = dictionary;
    if (dictionary != nil)
    {
        self.username = [dictionary objectOrNilForKey:TVUserUsernameKey];
        self.firstName = [dictionary objectOrNilForKey:TVUserFirstNameKey];
        self.lastName = [dictionary objectOrNilForKey:TVUserLastNameKey];
        self.email = [dictionary objectOrNilForKey:TVUserEmailKey];
        self.address = [dictionary objectOrNilForKey:TVUserAddressKey];
        self.city = [dictionary objectOrNilForKey:TVUserCityKey];
        self.state = [dictionary objectOrNilForKey:TVUserStateKey];
        self.country = [dictionary objectOrNilForKey:TVUserCountryKey];
        self.zipCode = [dictionary objectOrNilForKey:TVUserZipCodeKey];
        self.phoneNumber = [dictionary objectOrNilForKey:TVUserPhoneNumberKey];
        self.facebookID = [dictionary objectOrNilForKey:TVUserFacebookIDKey];
        self.facebookImage = [dictionary objectOrNilForKey:TVUserFacebookImageKey];
        self.affiliateCode = [dictionary objectOrNilForKey:TVUserAffiliateCodeKey];
        self.isFacebookImagePermitted = [[dictionary objectOrNilForKey:TVUserIsFacebookImagePermittedKey] boolValue];
        self.coGuid = [dictionary objectForKey:TVUserExternalCoGuidKeyKey];
        self.externalToken = [dictionary objectForKey:TVUserExternalTokenKey];
        self.faceBookToken = [dictionary objectForKey:TVUserExternalFacebookTokenKey];
        self.userType = [dictionary objectForKey:TVUserExternalUserTypeKey];
        
        if([[dictionary allKeys] containsObject:TVUSerPreferedLoginType]){
            self.preferedLoginType = [[dictionary objectOrNilForKey:TVUSerPreferedLoginType] intValue];
        }
        else{
            self.preferedLoginType = kPreferedLoginType_Unknown;
        }
        self.isAllowNewsLetter=YES;
    }
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSDictionary *basicInfo = [dictionary objectOrNilForKey:TVUserBasicInfoKey];
    [self populateBasicInfoFromDictionary:basicInfo];
    NSDictionary *dynamicOld = [dictionary objectOrNilForKey:TVUserDynamicDataKey];
    NSMutableDictionary *dynamicNew = [NSMutableDictionary dictionary];
    for (NSString *key in [dynamicOld allKeys])
    {
        id value = [dynamicOld objectOrNilForKey:key];
        [dynamicNew setObjectOrNil:value forKey:key];
    }
    self.dynamicData = [NSDictionary dictionaryWithDictionary:dynamicNew];
    self.siteGUID = [dictionary objectOrNilForKey:TVUserSiteGUIDKey];
    
    
    
}

-(BOOL) isEqual:(id)object
{
    if ([object isKindOfClass:[TVUser class]])
    {
        return [self.username isEqual:((TVUser *)object).username];
    }
    return NO;
}


-(NSDictionary *) keyValueRepresentation
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionary];
    [temp setObjectOrNil:self.siteGUID forKey:TVUserSiteGUIDKey];
    [temp setObjectOrNil:self.dynamicData forKey:TVUserDynamicDataKey];
    NSMutableDictionary *basicInfo = [NSMutableDictionary dictionary];
    [basicInfo addEntriesFromDictionary:self.basicData];
    [basicInfo setObjectOrNil:self.username forKey:TVUserUsernameKey];
    [basicInfo setObjectOrNil:self.firstName forKey:TVUserFirstNameKey];
    [basicInfo setObjectOrNil:self.lastName forKey:TVUserLastNameKey];
    [basicInfo setObjectOrNil:self.email forKey:TVUserEmailKey];
    [basicInfo setObjectOrNil:self.address forKey:TVUserAddressKey];
    [basicInfo setObjectOrNil:self.city forKey:TVUserCityKey];
    [basicInfo setObjectOrNil:self.state forKey:TVUserStateKey];
    [basicInfo setObjectOrNil:self.country forKey:TVUserCountryKey];
    [basicInfo setObjectOrNil:self.zipCode forKey:TVUserZipCodeKey];
    [basicInfo setObjectOrNil:self.phoneNumber forKey:TVUserPhoneNumberKey];
    [basicInfo setObjectOrNil:self.facebookID forKey:TVUserFacebookIDKey];
    [basicInfo setObjectOrNil:self.facebookImage forKey:TVUserFacebookImageKey];
    [basicInfo setObjectOrNil:self.affiliateCode forKey:TVUserAffiliateCodeKey];
    [basicInfo setObjectOrNil:self.coGuid forKey:TVUserExternalCoGuidKeyKey];
    [basicInfo setObjectOrNil:[NSNumber numberWithBool:self.isFacebookImagePermitted] forKey:TVUserIsFacebookImagePermittedKey];
    [basicInfo setObjectOrNil:[NSNumber numberWithInt:_preferedLoginType] forKey:TVUSerPreferedLoginType];
    [basicInfo setObjectOrNil:self.externalToken forKey:TVUserExternalTokenKey];
    [basicInfo setObjectOrNil:self.faceBookToken forKey:TVUserExternalFacebookTokenKey];
    [basicInfo setObjectOrNil:self.userType forKey:TVUserExternalUserTypeKey];
    [temp setObjectOrNil:basicInfo forKey:TVUserBasicInfoKey];
    return [NSDictionary dictionaryWithDictionary:temp];
}

-(NSDictionary *) keyValueRepresentationTemporary
{
    NSMutableDictionary *temp = [NSMutableDictionary dictionary];
    [temp setObjectOrNSNull:self.siteGUID forKey:TVUserExternalSiteGuidKey];
    
    
    NSMutableArray * array = [NSMutableArray array];
    
    for (NSDictionary  * dynamicDataItemDict in [self.dynamicData objectForKey:@"m_sUserData"])
    {
        NSMutableDictionary * dynamicDataItemDictConverted = [NSMutableDictionary dictionary];
        
        [dynamicDataItemDictConverted setObjectOrNSNull:[dynamicDataItemDict objectForKey:@"m_sDataType"] forKey:@"m_sDataTypeField"];
        
        [dynamicDataItemDictConverted setObjectOrNSNull:[dynamicDataItemDict objectForKey:@"m_sValue"] forKey:@"m_sValueField"];
        
        [array addObject:dynamicDataItemDictConverted];
       
    }
    
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithObject:array forKey:@"m_sUserDataField"];
    
       
    [temp setObjectOrNSNull:dictionary forKey:TVUserExternalUserDynamicData];
    
    
    NSMutableDictionary *basicInfo = [NSMutableDictionary dictionary];
    [basicInfo addEntriesFromDictionary:self.basicData];
    [basicInfo setObjectOrNSNull:self.username forKey:@"m_sUserNameField"];
    [basicInfo setObjectOrNSNull:self.firstName forKey:@"m_sFirstNameField"];
    [basicInfo setObjectOrNSNull:self.lastName forKey:@"m_sLastNameField"];
    [basicInfo setObjectOrNSNull:self.email forKey:@"m_sEmailField"];
    [basicInfo setObjectOrNSNull:self.address forKey:@"m_sAddressField"];
    [basicInfo setObjectOrNSNull:self.city forKey:@"m_sCityField"];
    [basicInfo setObjectOrNSNull:self.state forKey:@"m_StateField"];
    [basicInfo setObjectOrNSNull:self.country forKey:@"m_CountryField"];
    [basicInfo setObjectOrNSNull:self.zipCode forKey:@"m_sZipField"];
    [basicInfo setObjectOrNSNull:self.phoneNumber forKey:@"m_sPhoneField"];
    [basicInfo setObjectOrNSNull:self.facebookID forKey:@"m_sFacebookIDField"];
    [basicInfo setObjectOrNSNull:self.facebookImage forKey:@"m_sFacebookImageField"];
    [basicInfo setObjectOrNSNull:self.affiliateCode forKey:@"m_sAffiliateCodeField"];
    [basicInfo setObjectOrNSNull:self.coGuid forKey:@"m_CoGuidField"];
    [basicInfo setObjectOrNSNull:[NSNumber numberWithBool:self.isFacebookImagePermitted] forKey:@"m_bIsFacebookImagePermittedField"];
    [basicInfo setObjectOrNil:[NSNumber numberWithInt:_preferedLoginType] forKey:TVUSerPreferedLoginType];
    [basicInfo setObjectOrNil:self.externalToken forKey:@"m_ExternalTokenField"];
    [basicInfo setObjectOrNil:self.faceBookToken forKey:@"m_sFacebookTokenField"];
    
    dictionary = [NSMutableDictionary dictionaryWithDictionary:self.userType];

    [dictionary setObjectOrNSNull:[self.userType objectForKey:@"ID"] forKey:@"idField"];
    [dictionary setObjectOrNSNull:[self.userType objectForKey:@"Description"] forKey:@"descriptionField"];
    [dictionary setObjectOrNSNull:[self.userType objectForKey:@"IsDefault"] forKey:@"isDefaultField"];
    
    [basicInfo setObjectOrNSNull:dictionary forKey:@"m_UserTypeField"];
    [temp setObjectOrNil:basicInfo forKey:TVUserExternalUserBasicData];
    return [NSDictionary dictionaryWithDictionary:temp];
}


-(NSURL *) facebookPictureURLForSizeType : (NSString *) type
{
    NSURL *toRetutn = nil;
    if (self.facebookID.length > 0)
    {
        type = (type.length > 0) ? type : TVUserFacebookPictureTypeSquare;
        NSString *URLString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=%@",self.facebookID,type];
        toRetutn = [NSURL URLWithString:URLString];
    }
    return toRetutn;
}

@end

NSString *const TVUserFacebookPictureTypeSmall = @"small";
NSString *const TVUserFacebookPictureTypeNormal = @"normal";
NSString *const TVUserFacebookPictureTypeLarge = @"large";
NSString *const TVUserFacebookPictureTypeSquare = @"square";
