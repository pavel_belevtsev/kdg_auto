//
//  TVMenuCell.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/6/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVMenuItem;
@class AsyncImageView;
@interface TVMenuCell : UITableViewCell
@property (nonatomic , retain) TVMenuItem *menuItem;
@property (retain, nonatomic) IBOutlet AsyncImageView *imgMenuItemImage;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UIImageView *imgCellBgImage;
@property (nonatomic, retain) UIImage *defaultImage;
@property (nonatomic, retain) UIImage *highlightedImage;
-(void) update;
@end
