//
//  ShadowedTableView.h
//  YouAppi
//
//  Created by Avraham Shukron on 2/21/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShadowedTableView : UITableView
@property (nonatomic , retain) IBOutlet UIView *topShadowView;
@property (nonatomic , retain) IBOutlet UIView *bottomShadowView;

@property (nonatomic , retain) IBOutlet UIView *topOverscrollGlowView;
@property (nonatomic , retain) IBOutlet UIView *bottomOverscrollGlowView;

@property (nonatomic , assign) CGFloat shadowRadius;

-(void) setup;
@end
