//
//  TVNetworkQueu.h
//  TVinci
//
//  Created by Avraham Shukron on 6/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "TVPAPIRequest.h"

@interface TVNetworkQueue : NSObject<TVPAPIRequestDelegate>



-(void) sendRequest : (TVPAPIRequest *) request;
-(void) cleen;


@end
