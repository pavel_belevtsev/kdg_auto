// UIImage+Resize.h
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

// Extends the UIImage class to support resizing/cropping
@interface UIImage (Resize)

- (UIImage *)croppedImage:(CGRect)bounds;

- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;

-(UIImage *) resizableImageWith1x1StretchableArea;
+(UIImage *) resizableImageWith1x1StretchableAreaWithName : (NSString *) imageName;
-(UIImage *) resizableImageWith1xHightStretchableArea;
+(UIImage *) resizableImageWith1xHeightStretchableAreaWithName : (NSString *) imageName;
-(UIImage *) resizableImageWithSizeForStretchableArea:(CGSize)size;
+(UIImage *) resizableImageWithSizeForStretchableArea:(CGSize)size WithName : (NSString *) imageName;
@end
