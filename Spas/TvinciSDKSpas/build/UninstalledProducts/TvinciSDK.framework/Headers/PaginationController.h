//
//  PaginationController.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 12/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "PaginationTag.h"


typedef void (^PaginationCompletionBlock)(NSArray * items);
typedef void (^PaginationFailedBlock)();

typedef void (^FetchBlock)(NSInteger pageIndex , NSInteger pageSize ,PaginationCompletionBlock completion ,PaginationFailedBlock failed);


@class PaginationController ;

@protocol PaginationControllerDelegate <NSObject>
-(void) paginationController:(PaginationController *) controller
refreshAndScrollToRowAtIndex:(NSInteger) finalLocationToShowIndex;

@optional
-(void) paginationController:(PaginationController *) controller didStartLodingNewPage:(NSInteger) pageIndex;
-(void) paginationController:(PaginationController *) controller didFinishLodingNewPage:(NSInteger) pageIndex;
@end

@interface PaginationController : NSObject


@property (assign, nonatomic) id<PaginationControllerDelegate> delegate;
@property (copy, nonatomic)  FetchBlock fetchBlock;


-(id)initWithPageSize:(NSInteger) pageSize
     initialPageIndex:(NSInteger) pageIndex
           fetchBlock:(FetchBlock) fetchBlock
          startOffset:(NSInteger) offset
             delegate:(id<PaginationControllerDelegate>) delegate;

-(id) objectAtIndex:(NSInteger) index;
-(NSInteger) count;


@end
