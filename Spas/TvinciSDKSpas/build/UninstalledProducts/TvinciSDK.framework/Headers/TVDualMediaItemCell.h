//
//  TVDualMediaItemCell.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVSelectableMediaItemView.h"
@interface TVDualMediaItemCell : UITableViewCell
@property (retain, nonatomic) IBOutlet TVSelectableMediaItemView *leftMediaItemView;
@property (retain, nonatomic) IBOutlet TVSelectableMediaItemView *rightMediaItemView;
@property (nonatomic, retain) NSDictionary *cellInfoDict;

-(void) setDidSelectItemBlock:(ActionBlock)didSelectItemBlock;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;

+(TVDualMediaItemCell *) cell;
+(TVDualMediaItemCell *)cellWithInfoDictionary:(NSDictionary *)dic;

@end
