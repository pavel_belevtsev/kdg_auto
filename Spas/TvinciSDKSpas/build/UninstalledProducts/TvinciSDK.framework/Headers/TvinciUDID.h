//
//  TvinciUDID.h
//  PersistenceID
//
//  Created by Rivka S. Peleg on 2/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TvinciUDID : NSObject

+ (NSString*) value;
+ (void) clear;

@end
