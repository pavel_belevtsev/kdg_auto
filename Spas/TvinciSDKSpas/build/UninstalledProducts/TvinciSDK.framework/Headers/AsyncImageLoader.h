//
//  AsyncImageLoader.h
//  AsyncImageLoader
//
//  Created by Avraham Shukron on 1/1/11.
//  Copyright 2011 Avraham Shukron. All rights reserved.
//
//  
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

extern NSString *const AsyncImageLoaderImageLoadedNotification;
extern NSString *const AsyncImageLoaderIsIdleNotification;
extern NSString *const AsyncImageLoaderImageFailedNotification;

extern NSString *const AsyncImageLoaderImageKey;
extern NSString *const AsyncImageLoaderErrorKey;
extern NSString *const AsyncImageLoaderURLStringKey;

@interface AsyncImageLoader : NSObject 

+(NSString *) imageLoadedNotificationNameForURL : (NSURL *) imageURL;
+(NSString *) imageFailedNotificationNameForURL : (NSURL *) imageURL;

+ (AsyncImageLoader *)sharedAsyncImageLoader;

// Will be called on Memory Warning
- (void) emptyCache;
- (void) loadImageAtURL:(NSURL *)url;
- (void) enqueueURL:(NSURL *)url;
- (void) resume;
- (void) suspend;
- (NSString *) pathForImageURL:(NSURL *)aURL;
- (void) cacheImageInDisk : (UIImage *) anImage forURL : (NSURL *)aURL;
@end
