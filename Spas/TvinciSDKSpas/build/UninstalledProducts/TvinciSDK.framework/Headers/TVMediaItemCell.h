//
//  TVMediaItemCellCell.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    kMediaItemType_defualt=1,
    kMediaItemType_ipad2Related
}kindeOfItemCellType;

@class TVMediaItem;
@class TVPictureSize;
@class TVSubscription;
@class TVRental;

@interface TVMediaItemCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblItemTitle;
@property (retain, nonatomic) IBOutlet UIButton *btnItemImage;
@property (retain, nonatomic) IBOutlet UILabel *lblItemSubtitle;
@property (nonatomic, retain) TVPictureSize *pictureSize;
@property (nonatomic , retain) TVMediaItem *mediaItem;
@property (nonatomic, readonly) NSURL *pictureURL;
@property (retain, nonatomic) IBOutlet UILabel *lblDownText;
@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain ,nonatomic) TVSubscription * subscription;
@property(nonatomic) kindeOfItemCellType cellType;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

+(TVMediaItemCell *) cell;
-(void) updateCellWithPackageInfo:(NSDictionary *)packageInfo;
-(void) updateCellWithRental:(TVRental *)rentalInfo;
-(void) textForTitleInRental;
-(void) unregisterFromImageNotifications;
-(void) updateUI;


-(NSString *) textForSubtitle
;
@end
