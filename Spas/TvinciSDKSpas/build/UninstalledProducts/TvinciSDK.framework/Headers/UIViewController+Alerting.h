//
//  UIViewController+Alerting.h
//  YouAppi
//
//  Created by Avraham Shukron on 2/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alerting)
-(void) presentAlertWithTitle : (NSString *) title message : (NSString *) message;
-(void) presentAlertWithError : (NSError *) error;
-(void) presentAlertWithException : (NSException *) exception;
-(void) presentAlertWithTitle : (NSString *) title message : (NSString *) message andDelegate:(id)delegate;
@end
