//
//  TVSpotlightItemStyle2View.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSelectableMediaItemView.h"

@class TVLinearLayout;
@interface TVSpotlightItemStyle2View : TVSelectableMediaItemView
@property (retain, nonatomic) IBOutlet TVLinearLayout *metadataLine;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;

@property (nonatomic, retain) NSDictionary *infoDictForItem;
@end
