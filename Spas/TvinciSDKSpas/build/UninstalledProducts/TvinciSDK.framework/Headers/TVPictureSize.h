//
//  TVPictureSize.h
//  TVinci
//
//  Created by Avraham Shukron on 5/30/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVPictureSize : BaseModelObject
@property (nonatomic , copy , readonly) NSString *name;
@property (nonatomic , assign, readonly)  CGSize size;

-(id) initWithSize : (CGSize) size;
-(id) initWithName : (NSString *) name;

+(id) pictureSizeWithSize : (CGSize) size;
+(id) pictureSizeWithName : (NSString *) name;

+(TVPictureSize *) iPhoneSpotlightItemPictureSize;
+(TVPictureSize *) iPhoneItemPosterPictureSize;
+(TVPictureSize *) iPhoneItemCellPictureSize;
+(TVPictureSize *) iPhonePurchaseItemPictureSize;
+(TVPictureSize *) iPhone2x3HomeGalleryPictureSize;
+(TVPictureSize *) iPhone2x3GalleryPictureSize;

+(TVPictureSize *) iPadMovieTilePictureSize;
+(TVPictureSize *) iPadSeriesTilePictureSize;
+(TVPictureSize *) iPadItemCellPictureSize;
+(TVPictureSize *) iPadHome3x3GridPictureSize;
+(TVPictureSize *) iPadHome2x2GridPictureSize;
+(TVPictureSize *) iPadMegaSpotlightPictureSize;
+(TVPictureSize *) iPadSpotlightSmallPictureSize;
+(TVPictureSize *) fullPictureSize;
+(TVPictureSize *) fullIpadPictureSize;
+(TVPictureSize *) iPadLiveChannleImage219X123;


+(TVPictureSize *) iPadEpgChnnel50X50;
+(TVPictureSize *) iPadEpgChnnel45X45;
+(TVPictureSize *) iPadEpgChnnel100X100;


+(TVPictureSize *) iPadEpgChnnel128X128;

+(TVPictureSize *) iPadGrig169X253;
+(TVPictureSize *) iPadShowPage256X384;
+(TVPictureSize *) iPadShowPage145X216;
+(TVPictureSize *) iPadHomePage397X596;
+(TVPictureSize *) iPadHomePage191X286;
+(TVPictureSize *) iPadLightBoxPage219X329;
+(TVPictureSize *) iPadMyZoonPage123X184;
+(TVPictureSize *) iPadDetailUnderPlayer105X157;
+(TVPictureSize *) iPadRlated137X206;
+(TVPictureSize *) iPadRlated186X104;
+(TVPictureSize *) iPadRlated153X86;
+(TVPictureSize *) iPadRlated222X125;


+(TVPictureSize *) iPadSpotlightSmallPictureSize252x142;
+(TVPictureSize *) vodInfo564X318;
+(TVPictureSize *) vodInfo460X260;
+(TVPictureSize *) vodInfo888X500;
+(TVPictureSize *) iPadMyZoneTile246X368;
+(TVPictureSize *) iPhoneItemPosterPictureSize320X480;


+(TVPictureSize *) iPadPackagePromotionPictureSize652x412;
@end
