//
//  ASNavigationBar.h
//  ExLibris
//
//  Created by Avraham Shukron on 1/4/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TVMenuButtonNavigationController;
@interface TVNavigationBar : UINavigationBar
@property (nonatomic , retain) UIImage *backgroundImage;
-(void) setup;

+(UINavigationController *) navigationControllerWithCustomBar;
@end
