//
//  NSDate+Format.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDate (Format)
+ (NSDate *) dateFromString : (NSString *) dateString withFormat : (NSString *) formart;
+ (NSDate *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString andFormat:(NSString *)dateFormat;

+ (NSDate *)FriendlyDateTimeByString:(NSString *)dateString;

- (NSString *) stringWithDateFormat : (NSString *) format;

-(NSString *) stringWithDateFormat : (NSString *)format andGMT:(NSInteger)gmt;

@end
