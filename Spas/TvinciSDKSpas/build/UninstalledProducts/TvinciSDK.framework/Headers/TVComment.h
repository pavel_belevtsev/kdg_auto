//
//  TVComment.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/6/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVComment : BaseModelObject

@property (retain, nonatomic) NSString * author;
@property (retain, nonatomic) NSString * header;
@property (retain, nonatomic) NSString * content;
@property (retain, nonatomic) NSString * userpicUrl;
@property (retain, nonatomic) NSDate * addedDate;


@end
