//
//  TVLoginViewControllerDelegate.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 7/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModalViewControllerDelegate.h"
@protocol TVLoginViewControllerDelegate <ModalViewControllerDelegate>

@required
-(void) loginViewControllerDidLogin:(UIViewController *)sender;
-(void) loginViewControllerDidCancel:(UIViewController *)sender;
-(void) loginViewController:(UIViewController *)sender didFailWithError:(NSError *)error; 


@end
