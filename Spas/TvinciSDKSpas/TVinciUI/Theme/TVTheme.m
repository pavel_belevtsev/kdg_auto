//
//  TVTheme.m
//  TVinci
//
//  Created by Avraham Shukron on 6/27/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVTheme.h"
#import "SmartLogs.h"
#import "NSDictionary+NSNullAvoidance.h"



NSString * const NavigationBarTintColorKey = @"NavigationBarTintColor";
NSString * const PrimaryTextColorKey = @"PrimaryTextColor";
NSString * const SecondaryTextColorKey = @"SecondaryTextColor";
NSString * const PrimaryAccentColorKey = @"PrimaryAccentColor";
NSString * const SecondaryAccentColorKey = @"SecondaryAccentColor";
NSString * const TextShadowColorKey = @"TextShadowColor";
NSString * const MetaDataTextColorKey = @"MetaDataTextColor";

NSString * const TextSizeLargeKey = @"TextSizeLarge";
NSString * const TextSizeMediumKey = @"TextSizeMedium";
NSString * const TextSizeSmallKey = @"TextSizeSmall";
NSString * const TextSizeTinyKey = @"TextSizeTiny";

NSString * const MainFontKey = @"MainFontName";

NSString *const ThemeFileName = @"Theme.plist";

@interface TVTheme ()
@property (nonatomic, retain) NSDictionary *theme;
@end

@implementation TVTheme
@synthesize theme = _theme;

#pragma mark - Getters
-(NSDictionary *) theme
{
    if (_theme == nil)
    {
        NSURL *fileURL = [[NSBundle mainBundle] URLForResource:ThemeFileName withExtension:nil];
        _theme = [[NSDictionary alloc] initWithContentsOfURL:fileURL];
    }
    return _theme;
}

-(UIColor *) colorForKey : (NSString *) key
{
    return [self colorFromString:[self.theme objectForKey:key]];
}

-(UIColor *) colorFromString : (NSString *) rgbString
{
    UIColor *color = nil;
    if (rgbString.length > 0)
    {
        NSArray *components = [rgbString componentsSeparatedByString:@","];
        if (components.count > 2)
        {
            NSInteger red = [[components objectAtIndex:0] integerValue];
            NSInteger green = [[components objectAtIndex:1] integerValue];
            NSInteger blue = [[components objectAtIndex:2] integerValue];
            NSInteger alpha = 256;
            if (components.count > 3)
            {
                alpha = [[components objectAtIndex:3] integerValue];
            }
            
            color = [UIColor colorWithRed:((float)red/256.0) 
                                    green:((float)green/256.0) 
                                     blue:((float)blue/256.0) 
                                    alpha:((float)alpha/256.0)];
        }
        else 
        {
            ASLogDebug(@"Invalid RGB format: %@. Valid format is RR,GG,BB,AA or RR,GG,BB", rgbString);
        }
    }
    return color;
}

-(UIFont *) mainFont
{
    return [self mainFontWithSize:[UIFont systemFontSize]];
}

-(UIFont *) mainFontWithSize : (CGFloat) textSize
{
    NSString *fontName = [self.theme objectOrNilForKey:MainFontKey];
    UIFont *toReturn = [UIFont fontWithName:fontName size:textSize];
    return (toReturn != nil) ? toReturn : [UIFont systemFontOfSize:textSize];
}

#pragma mark - Singleton
static TVTheme *_mainTheme = nil;

+(TVTheme *) mainTheme
{
    if (_mainTheme == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _mainTheme = [[TVTheme alloc] init];
        });
    }
    return _mainTheme;
}

-(CGFloat) floatForKey:(NSString *)key
{
    return [[self.theme objectForKey:key] floatValue];
}

@end
