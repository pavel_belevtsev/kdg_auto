
//
//  TVMenuCell.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/6/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVMenuCell.h"
#import "TVMenuItem.h"
#import "AsyncImageView.h"
#import "SmartLogs.h"
#import "TVinci.h"
#import "TVConstants.h"

@implementation TVMenuCell
@synthesize imgMenuItemImage;
@synthesize lblTitle;
@synthesize menuItem = _menuItem;
@synthesize defaultImage = _defaultImage, highlightedImage = _highlightedImage;
#pragma mark - Memory
- (void)dealloc 
{
    [_highlightedImage release];
    [_defaultImage release];
    self.imgMenuItemImage = nil;
    self.lblTitle = nil;
    self.menuItem = nil;
    [_imgCellBgImage release];
    [super dealloc];
}

#pragma mark - Setters
-(void) setMenuItem:(TVMenuItem *)menuItem
{
    if (_menuItem != menuItem)
    {
        [_menuItem release];
        _menuItem = [menuItem retain];
        if (_menuItem != nil)
        {
            [self update];
        }
    }
}

-(void) update
{
    self.lblTitle.text = self.menuItem.name;
    ASLog2Debug(@"%@",self.lblTitle.text);////////
    if ([self.menuItem.iconURL scheme] != nil)
    {
        [self.imgMenuItemImage loadImageAtURL:self.menuItem.iconURL andPlaceholderImage:nil];
    }
    else 
    {
       // NSString *defaultImageName = [NSString stringWithFormat:@"Menu %@",self.menuItem.iconName];
        NSString *defaultImageName;
        
        defaultImageName = [NSString stringWithFormat:@"%@.png",self.menuItem.iconName];
        
        // handle Kanguroo
        if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO])
        {
            defaultImageName = [NSString stringWithFormat:@"Menu %@.png",self.menuItem.iconName];
        }
        

        NSString *highlightedImageName = [NSString stringWithFormat:@"Menu %@ Highlighted",self.menuItem.iconName];
         ASLog2Debug(@"[Icon][Menu] name: '%@' iconName: '%@' default '%@'  highlighted '%@'   ",self.menuItem.name,self.menuItem.iconName,defaultImageName,highlightedImageName );////////
        
        self.defaultImage = [UIImage imageNamed:defaultImageName];
        self.highlightedImage = [UIImage imageNamed:highlightedImageName];
        
        CGFloat imageMarginFromLeft = 13;
        CGSize imageSize = _defaultImage.size;
        CGRect imageFrame = CGRectZero;
        imageFrame.origin.x = imageMarginFromLeft;
        imageFrame.origin.y = (self.bounds.size.height - imageSize.height) / 2;
        imageFrame.size = imageSize;
        
        
        self.imgMenuItemImage.frame = imageFrame;
        [self.imgMenuItemImage setImage:_defaultImage];
        [self.imgMenuItemImage setHighlightedImage:_highlightedImage];
        
        self.imgMenuItemImage.contentMode = UIViewContentModeScaleAspectFit;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];


    // Configure the view for the selected state
}
@end
