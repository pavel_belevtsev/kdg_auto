//
//  TVTitleValueself.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVTitleValueCell.h"

@implementation TVTitleValueCell

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:reuseIdentifier];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    CGRect titleFrame = self.textLabel.frame;
    titleFrame.origin.x = 0;
    titleFrame.size.width = (self.contentView.bounds.size.width / 2) - 5;
    self.textLabel.frame = titleFrame;
    
    CGRect valueFrame = self.detailTextLabel.frame;
    valueFrame.size.width = (self.contentView.bounds.size.width / 2) - 5;
    valueFrame.origin.x = self.contentView.bounds.size.width - valueFrame.size.width;
    self.detailTextLabel.frame = valueFrame;
}
@end
