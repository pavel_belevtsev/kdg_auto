//
//  TVSegmentedControl.h
//  Kanguroo
//
//  Created by Avraham Shukron on 6/14/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVSegmentedControl : UIControl
/*!
 * @param items An NSArray object containing NSString objects that will be the title for each section
 */
-(id) initWithItems : (NSArray *) items;
-(void) removeAllSegments;
-(void) removeSegmentAtIndex : (NSUInteger) index animated:(BOOL)animated;
-(void) insertSegment : (NSString *) title atIndex : (NSUInteger) index;

@property (nonatomic, assign, readonly) NSUInteger numberOfSegments;
@property (nonatomic, assign) NSUInteger selectedSegmentIndex;

@property (nonatomic, copy) NSString *rightBackgroundImageName;
@property (nonatomic, copy) NSString *middleBackgroundImageName;
@property (nonatomic, copy) NSString *leftBackgroundImageName;
@property (nonatomic, copy) NSString *rightSelectedBackgroundImageName;
@property (nonatomic, copy) NSString *middleSelectedBackgroundImageName;
@property (nonatomic, copy) NSString *leftSelectedBackgroundImageName;

@property (nonatomic, readonly) NSArray *buttons;
@end
