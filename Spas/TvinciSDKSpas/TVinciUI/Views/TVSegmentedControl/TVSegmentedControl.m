//
//  TVSegmentedControl.m
//  Kanguroo
//
//  Created by Avraham Shukron on 6/14/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSegmentedControl.h"

@interface TVSegmentedControl ()
@property (nonatomic, retain, readwrite) NSMutableArray *privateButtons;
@end

@implementation TVSegmentedControl

@synthesize privateButtons = _privateButtons;

@synthesize numberOfSegments = _numberOfSegments;
@synthesize selectedSegmentIndex = _selectedSegmentIndex;

@synthesize rightBackgroundImageName = _rightSegmentBackgroundImage;
@synthesize rightSelectedBackgroundImageName = _rightSegmentSelectedBackgroundImage;
@synthesize leftBackgroundImageName = _leftSegmentBackgroundImage;
@synthesize leftSelectedBackgroundImageName = _leftSegmentSelectedBackgroundImage;
@synthesize middleBackgroundImageName = _middleSegmentBackgroundImage;
@synthesize middleSelectedBackgroundImageName = _middleSegmentSelectedBackgroundImage;

#pragma mark - Memory
-(void) dealloc
{
    self.privateButtons = nil;
    self.rightBackgroundImageName = nil;
    self.rightSelectedBackgroundImageName = nil;
    self.middleSelectedBackgroundImageName = nil;
    self.middleBackgroundImageName = nil;
    self.leftBackgroundImageName = nil;
    self.leftSelectedBackgroundImageName = nil;
    [super dealloc];
}

- (id)initWithItems:(NSArray *)items
{
    if (self = [super initWithFrame:CGRectZero])
    {
        
    }
    return self;
}

#pragma mark - API

-(void) removeAllSegments
{
    [self.privateButtons removeAllObjects];
}

-(void) removeSegmentAtIndex:(NSUInteger)index animated:(BOOL)animated
{
    
}

-(void) insertSegment:(NSString *)title atIndex:(NSUInteger)index
{
    
}

-(NSArray *) buttons
{
    return [NSArray arrayWithArray:self.privateButtons];
}

#pragma mark - Layout
-(void) layoutSubviews
{
    
}

#pragma mark - Utilities
-(UIImage *) resizableImageWithName : (NSString *) imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    if (image != nil)
    {
        CGFloat halfWidth = image.size.width / 2;
        CGFloat halfHeight = image.size.height / 2;
        
        if ([image respondsToSelector:@selector(resizableImageWithCapInsets:)])
        {
            // iOS > 5
            UIEdgeInsets insets = UIEdgeInsetsMake(halfHeight, halfWidth, halfHeight, halfHeight);
            image = [image resizableImageWithCapInsets:insets];
        }
        else 
        {
            // iOS < 5
            image = [image stretchableImageWithLeftCapWidth:halfWidth topCapHeight:halfHeight];
        }
    }
    return image;
}
@end
