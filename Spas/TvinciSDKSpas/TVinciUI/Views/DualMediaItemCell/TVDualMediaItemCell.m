//
//  TVDualMediaItemCell.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVDualMediaItemCell.h"
#import "TVMediaItem.h"
#import "AsyncImageLoader.h"
#import "TVSessionManager.h"

#define MARGINS_LEFT 8
#define MARGINS_RIGHT 8
#define MARGINS_TOP 9

@interface TVDualMediaItemCell ()
@property (nonatomic, copy) ActionBlock didSelectItemBlock;
@end

@implementation TVDualMediaItemCell
@synthesize didSelectItemBlock = _didSelectItemBlock;
@synthesize leftMediaItemView = _leftMediaItemView;
@synthesize rightMediaItemView = _rightMediaItemView;
@synthesize cellInfoDict  = _cellInfoDict;

-(void) dealloc
{
    [_cellInfoDict dealloc];
    [_didSelectItemBlock release];
    [_leftMediaItemView release];
    [_rightMediaItemView release];
    [_loadingActivityIndicator release];
    [super dealloc];
}

#pragma mark - Setters

#pragma mark - Setup
-(void) setup
{
    NSInteger marginsLeft = MARGINS_LEFT;
    NSInteger marginsRight = MARGINS_RIGHT;

    // if cell it not ready ignore setup for now. (happends on first awakeFromXib)
    if (self.cellInfoDict == nil)
    {
        return;
    }
    TVPictureSize *size = nil;
    if ([self.cellInfoDict objectForKey:kCellInfoDictKey_ImageSize] != nil)
    {
        size = [self.cellInfoDict objectForKey:kCellInfoDictKey_ImageSize];
        marginsLeft =10;
        marginsRight=10;
    }
    else
    {
         size = [TVPictureSize iPhone2x3GalleryPictureSize];
    }
    
    if ([self.cellInfoDict objectForKey:kCellInfoDictKey_bgColorForTitle] != nil)
    {
        _leftMediaItemView.titleLabel.backgroundColor = [self.cellInfoDict objectForKey:kCellInfoDictKey_bgColorForTitle];
        _rightMediaItemView.titleLabel.backgroundColor = [self.cellInfoDict objectForKey:kCellInfoDictKey_bgColorForTitle];
    }
    
    CGRect frame = CGRectZero;
    frame.size = size.size;
    frame.origin = CGPointMake(marginsLeft, MARGINS_TOP);
    _leftMediaItemView = [[TVSelectableMediaItemView alloc] initWithFrame:frame];
    self.leftMediaItemView.cellInfoDict=self.cellInfoDict;
    
    [self addSubview:_leftMediaItemView];
    NSLog(@"self.bounds.size.width = %f",self.bounds.size.width);
    NSLog(@"frame.size.width = %f",frame.size.width);
    
    frame.origin = CGPointMake(self.bounds.size.width - marginsRight - frame.size.width, MARGINS_TOP);
    _rightMediaItemView = [[TVSelectableMediaItemView alloc] initWithFrame:frame];
    self.rightMediaItemView.cellInfoDict=self.cellInfoDict;
    [self addSubview:_rightMediaItemView];
    
    self.leftMediaItemView.pictureSize = size;
    self.rightMediaItemView.pictureSize = size;
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, self.leftMediaItemView.bounds.size.height - size.size.height, 0);
    self.leftMediaItemView.imageButtonEdgeInsets = insets;
    self.rightMediaItemView.imageButtonEdgeInsets = insets;
    
    [self.rightMediaItemView setDidSelectItemBlock:^(TVSelectableMediaItemView *sender, TVMediaItem *item) {
        if (self.didSelectItemBlock != nil)
        {
            self.didSelectItemBlock(sender,item);
        }
    }];
    
    [self.leftMediaItemView setDidSelectItemBlock:^(TVSelectableMediaItemView *sender, TVMediaItem *item) {
        if (self.didSelectItemBlock != nil)
        {
            self.didSelectItemBlock(sender,item);
        }
    }];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        [self setup];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

#pragma mark - UI

#pragma mark - Utils
+(TVDualMediaItemCell *) cell
{
    TVDualMediaItemCell *cell = nil;
    NSString *nibName = NSStringFromClass(self);
    NSArray *content = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id something in content)
    {
        if ([something isKindOfClass:self])
        {
            cell = something;
            break;
        }
    }
    cell.loadingActivityIndicator.hidden=YES;
    cell.cellInfoDict = [NSDictionary dictionary];
    [cell setup];
    
    return cell;
}

+(TVDualMediaItemCell *)cellWithInfoDictionary:(NSDictionary *)dic;
{
    TVDualMediaItemCell *cell = nil;
    NSString *nibName = NSStringFromClass(self);
    NSArray *content = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id something in content)
    {
        if ([something isKindOfClass:self])
        {
            cell = something;
            break;
        }
    }
    cell.cellInfoDict = [NSDictionary dictionaryWithDictionary:dic];
    cell.loadingActivityIndicator.hidden=YES;
    [cell setup];
    return cell;
}
@end
