//
//  ASNavigationBar.m
//  ExLibris
//
//  Created by Avraham Shukron on 1/4/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVNavigationBar.h"
#import "TVTheme.h"
#import "TVSessionManager.h"
#import "TVConstants.h"

//#define NAV_BAR_BACKGROUND_IMAGE @"navigation_bar_background"
// #define NAV_BAR_BACKGROUND_IMAGE @"app_new_top_bar"

#define NAV_BAR_BACKGROUND_IMAGE @"app_new_top_bar"

// used to be "top_bar" - testing
#define NAV_BAR_BACKGROUND_IMAGE_IPAD @"top_bar"
#define NAV_BAR_BACKGROUND_IMAGE_IPAD_DARK @"navigationBarStreatchableBg"

@implementation TVNavigationBar
@synthesize backgroundImage = _backgroundImage;

-(void) setBackgroundImage:(UIImage *)backgroundImage
{
    if (_backgroundImage != backgroundImage)
    {
        [_backgroundImage release];
        _backgroundImage = [backgroundImage retain];
        [self setNeedsDisplay];
        [self setNeedsLayout];
    }
}

-(UIImage *) backgroundImage
{
    if (_backgroundImage == nil)
    {
        if  ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
        {
            if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_UseNavDarkBG]){
                self.backgroundImage = [UIImage imageNamed:NAV_BAR_BACKGROUND_IMAGE_IPAD_DARK];
            }
            else{
                self.backgroundImage = [UIImage imageNamed:NAV_BAR_BACKGROUND_IMAGE_IPAD];
            }
        }
        else
        {
            self.backgroundImage = [UIImage imageNamed:NAV_BAR_BACKGROUND_IMAGE];
        }
    }
    return _backgroundImage;
}


-(void) drawRect:(CGRect)rect
{
    [self.backgroundImage drawInRect:rect];
}
 

-(void) setup
{
    self.barStyle = UIBarStyleBlack;
    self.tintColor = [[TVTheme mainTheme] colorForKey:NavigationBarTintColorKey];
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

-(void) layoutSubviews
{
    // Center each view vertically. Useful when the the custom image have different heigt than 44 points.
    [super layoutSubviews];
    for (UIView *subview in self.subviews)
    {
        CGRect frame = subview.frame;
        frame.origin.y = (self.bounds.size.height - subview.bounds.size.height) / 2+1;
        subview.frame = frame;
    }
}

#pragma mark - Utility
+(UINavigationController *) navigationControllerWithCustomBar
{
    NSString *nibName = NSStringFromClass(self);
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    UINavigationController *controller = nil;
    for (id item in contents)
    {
        if ([item isKindOfClass:[UINavigationController class]])
        {
            controller = item;
        }
    }
    return controller;
}
@end
