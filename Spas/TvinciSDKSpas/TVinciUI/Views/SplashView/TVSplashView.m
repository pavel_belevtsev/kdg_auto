//
//  TVSplashView.m
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSplashView.h"
#import "SmartLogs.h"
#import <QuartzCore/QuartzCore.h>
#import "TvinciSDK.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
@interface TVSplashView()
@property (nonatomic, retain) NSMutableArray *dependencies;
@property (nonatomic, copy) NSString *finishMessage;
@property (nonatomic, retain) NSTimer *dismissTimer;
@end

@implementation TVSplashView
@synthesize splashImage = _splashImage;
@synthesize statusLabel = _statusLabel;
@synthesize activityIndicator = _activityIndicator;
@synthesize finishMessage = _finishMessage;
@synthesize dismissTimer = _dismissTimer;
@synthesize dependencies = _dependencies;

-(void) invalidateTimer
{
    [self.dismissTimer invalidate];
    self.dismissTimer = nil;
}

#pragma mark - Memory
- (void)dealloc 
{
    [self invalidateTimer];
    [_splashImage release];
    [_statusLabel release];
    [_activityIndicator release];
    [_lblVersion release];
  
    [super dealloc];
}

#pragma mark - Initializations
-(NSMutableArray *) dependencies
{
    if (_dependencies == nil)
    {
        _dependencies = [[NSMutableArray alloc] init];
    }
    return _dependencies;
}


-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}



-(void) setup
{
    // test
    
    
    [self.dependencies removeAllObjects];
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
    // ASLog(@"app version = %@",version);
    self.lblVersion.text = [NSString stringWithFormat:@"Version %@",version];
    
//    UIImage * image= [UIImage imageNamed:@"splashFourInch"] ;
//    if (IS_IPHONE5 && image != nil)
//    {
//        CGRect frame = self.splashImage.frame;
//        frame.origin.y=128;
//        frame.size.height=568;
//        self.splashImage.frame=frame;
//        self.splashImage.image = image ;
//    }
    
    [self update4InchLook];
    
  
}

#pragma mark - Singleton

static TVSplashView *_sharedView = nil;
+(TVSplashView *) sharedTVSplashView
{
    if (_sharedView == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSString *nibName = NSStringFromClass(self);
            NSArray *content = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
            for (id item in content)
            {
                if ([item isKindOfClass:self])
                {
                    _sharedView = [item retain];
                    break;
                }
            }
        });
    }
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleShortVersionString"];
   // ASLog(@"app version = %@",version);
    _sharedView.lblVersion.text = [NSString stringWithFormat:@"Version %@",version];
    UIImage * image= [UIImage imageNamed:@"splashFourInch"] ;
    if (IS_IPHONE5 && image != nil)
    {
        CGRect frame = _sharedView.splashImage.frame;
        frame.origin.y=-10;
        _sharedView.splashImage.frame=frame;
        _sharedView.splashImage.image = image ;
    }

    if(IS_IPHONE5){
        [_sharedView update4InchLook];
    }
    
    return _sharedView;
}

#pragma mark - API

-(void) addDependency:(NSString *)dependencyName
{
    [self invalidateTimer];
    if (dependencyName != nil)
    {
        //ASLogDebug(@"Added dependency: %@",dependencyName);
        [self.dependencies addObject:dependencyName];
    }
}

-(void) removeDependency:(NSString *)dependencyName
{
    [self invalidateTimer];
    if (dependencyName != nil)
    {
        //ASLogDebug(@"Removed dependency: %@",dependencyName);
        [self.dependencies removeObject:dependencyName];
    }
    if ([self.dependencies count] == 0)
    {
        //ASLogDebug(@"Dismiss in 0.5");
        self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(dismiss) userInfo:nil repeats:NO];
    }
}


-(void) removeAllDependency
{
    [self invalidateTimer];
            //ASLogDebug(@"Removed dependency: %@",dependencyName);
    [self.dependencies removeAllObjects];
    
   
    //ASLogDebug(@"Dismiss in 0.5");
    self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(dismiss) userInfo:nil repeats:NO];
    
}

-(void) setStatus:(NSString *)status
{
    self.statusLabel.text = status;
    [self.statusLabel setNeedsDisplay];
    [self.statusLabel setNeedsLayout];
}

-(void) showInView : (UIView *) container withInitialMessage : (NSString *) initialMessage finishMessgae : (NSString *) finishMessage
{
    [self invalidateTimer];
    self.finishMessage = finishMessage;
    if (self.superview != container)
    {
        self.frame = container.bounds;
        [container addSubview:self];
    }
    [self setStatus:initialMessage];
    
    
    // animateLoading
    if(self.imgLoading){
        [self rotateLoading];
    }
    
}

- (void)rotateLoading {
    // This is to handle multiple calls
    if(self.imgLoading != nil){
        if(self.imgLoading && self.imgLoading.userInteractionEnabled == NO){
            return;
        }
        
        CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        fullRotation.fromValue = [NSNumber numberWithFloat:0];
        fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
        fullRotation.duration = 1;
        fullRotation.repeatCount = 3000;
        [self.imgLoading.layer addAnimation:fullRotation forKey:@"360"];
    
    }
    
//    
//    // the 
//    __block int rotateOffset = 90;
////    self.imgLoading.transform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(-rotateOffset));
//
//    float animDelay = 0;
//    
//    [UIView animateWithDuration:0.7
//                          delay:animDelay
//                        options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat)//| UIViewAnimationOptionAutoreverse)
//                     animations:^ {
//                         self.imgLoading.transform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(rotateOffset));
//                         
//                         
//                    }
//                     completion:^(BOOL finished){
//                         rotateOffset+=90;
//                     }
//     ];
//    
//    // For catching the last one - try
//    //    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationKey_StartEditFaivorites object:nil];
}

-(void) update4InchLook{
    if(IS_IPHONE5 && [[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_UseSplash4inchsImage]){
        UIImage *splashImg = [UIImage imageNamed:[[[TVSessionManager sharedTVSessionManager] takeAddedDataForKey:kSessionAddedData_4InchesSplashImageName] firstObject]];
        [self.splashImage setImage:splashImg];
        
        if([[TVSessionManager sharedTVSessionManager] hasAddedDataForKey:kSessionAddedData_4InchesSplashImagePositionY]){
            CGRect frm = self.splashImage.frame;
            frm.origin.y = [[[[TVSessionManager sharedTVSessionManager] takeAddedDataForKey:kSessionAddedData_4InchesSplashImagePositionY] firstObject] intValue];
            self.splashImage.frame=frm;
        }
        
//        CGRect frm;//= self.frame;
//        frm.size.height = 568;
//        self.frame = frm;
        
//        CGRect frm = self.splashImage.frame;
//        frm.size =splashImg.size;
//        self.splashImage.frame=frm;
//        
        
    }
}

-(void) dismiss
{
    if (self.superview != nil)
    {
        [self.dependencies removeAllObjects];
        [self setStatus:self.finishMessage];
        NSTimeInterval duration = 0.4;
        [UIView animateWithDuration:duration delay:0 options:(UIViewAnimationOptionCurveEaseIn) animations:^{
            self.transform = CGAffineTransformMakeScale(1.5, 1.5);
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.transform = CGAffineTransformIdentity;
            self.alpha = 1;
            UIView *superview = self.superview;
            [self removeFromSuperview];
            [superview setNeedsDisplay];
            [superview setNeedsLayout];
        }];
    }
}
@end
