//
//  SwipeViewScrollBarView.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 2/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "SwipeViewScrollBarView.h"
#import "UIImage+Resize.h"
#import "SmartLogs.h"

@implementation SwipeViewScrollBarView
@synthesize imgCapsule = _imgCapsule, numOfItems = _numOfItems;

#pragma mark - lifecycle
- (void)dealloc {
    [_imgCapsule release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

-(void) setupCapsuleForNumberOfItems:(int)numberOfItems{
    
    _numOfItems = numberOfItems;
    
    int width = MAX(320/numberOfItems, _imgCapsule.image.size.width*4);
    
    CGRect frm = _imgCapsule.frame;
    frm.size = CGSizeMake(width,  _imgCapsule.frame.size.height);
    _imgCapsule.frame = frm;
    
    
    self.imgCapsule.image = [self.imgCapsule.image resizableImageWith1xHightStretchableArea];
    
}

-(void) setup{
    self.imgCapsule = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scrollCapsul"]];
    [self addSubview:_imgCapsule];
    self.imgCapsule.alpha = 0.7;

}

-(void) updateToIndex:(int)index{
    [UIView animateWithDuration:0.3 delay:0
                        options:(UIViewAnimationOptionCurveEaseInOut)
                     animations:^{
                         CGRect frm = self.imgCapsule.frame;
                         // ASLog2Debug(@"frm.origin: (%f,%f) index: %i numOfItems: %i",frm.origin.x, frm.origin.y,index, _numOfItems);////////
                         frm.origin = CGPointMake(-(_imgCapsule.frame.size.width/2) + ((_imgCapsule.frame.size.width/2) +300)*((float)index/(float)_numOfItems),  self.imgCapsule.frame.origin.y);
                        // ASLog2Debug(@"frm.origin2: (%f,%f) index: %i numOfItems: %i",frm.origin.x, frm.origin.y,index, _numOfItems);////////
                         self.imgCapsule.frame = frm;
                     }
                     completion:^(BOOL finished) {
                        
                     } ];

}

@end
