//
//  TVMediaItemCellCell.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVMediaItemCell.h"
#import "TVMediaItem.h"
#import "AsyncImageLoader.h"
#import "TVPictureSize.h"
#import "SmartLogs.h"
#import "TVRental.h"

NSString *const DefaultItemImageName = @"default_item_image16x9";



@interface TVMediaItemCell ()
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
-(void) registerForImageNotifications;
-(void) unregisterFromImageNotifications;
-(void) updateUI;
-(void) showDefaultImage;
-(void) revealButtonAnimated;
@end

@implementation TVMediaItemCell
@synthesize backgroundImage;
@synthesize lblItemTitle;
@synthesize btnItemImage;
@synthesize lblItemSubtitle;
@synthesize mediaItem = _mediaItem;
@synthesize pictureSize = _pictureSize;
@synthesize lblDownText;
@synthesize bgImage;
@synthesize cellType;
@synthesize subscription;

#pragma mark - Memory

- (void)dealloc
{
    
    [subscription release];
    [self unregisterFromImageNotifications];
    [_pictureSize release];
    self.lblItemTitle = nil;
    self.lblItemSubtitle = nil;
    self.backgroundImage = nil;
    self.btnItemImage = nil;
    self.mediaItem = nil;
    [lblDownText release];
    [bgImage release];
    [_activityIndicator release];
    [super dealloc];
}

#pragma mark - Initializers
-(void) setup
{
    // Default Value
    self.pictureSize = [TVPictureSize iPhoneItemCellPictureSize];
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

#pragma mark - Setters

-(void) setMediaItem:(TVMediaItem *)mediaItem
{
    if (mediaItem != _mediaItem)
    {
        [_mediaItem release];
        _mediaItem = [mediaItem retain];
        
        if (_mediaItem != nil)
        {
            [self updateUI];
        }
    }
}

#pragma mark - Getters

-(NSString *) textForTitle
{
    NSString *titleString = nil;
    if ([self.mediaItem isEpisode])
    {
        NSString * episodeName= [[self.mediaItem metaData] objectForKey:@"Episode name"];
        NSString * slashEpisodName;
        if(episodeName == nil){
            episodeName = @"";
            slashEpisodName = @"";
        }
        else{
            slashEpisodName = [NSString stringWithFormat:@"- %@",episodeName];
        }
        
        NSString *seasonNumber = [self.mediaItem.metaData objectForKey:TVMetaDataSeasonNumber];
        seasonNumber = (seasonNumber.length > 0) ? seasonNumber : nil;
        NSString *episodeNumber = [self.mediaItem.metaData objectForKey:TVMetaDataEpisodeNumber];
        episodeNumber = (episodeNumber.length > 0) ? episodeNumber : NSLocalizedString(@"NA", nil);
        
        NSString *seasonAndEpisodeString = [NSString stringWithFormat:@"%@ %@ %@" ,NSLocalizedString(@"Episode", nil),episodeNumber,slashEpisodName];
        if ([seasonNumber integerValue] > 0)
        {
            seasonAndEpisodeString = [NSString stringWithFormat:@"%@ %@, %@", NSLocalizedString(@"Season", nil), seasonNumber, seasonAndEpisodeString];
        }
        titleString = seasonAndEpisodeString;
    }
    else
    {
        titleString = self.mediaItem.name;
    }
    return titleString;
}

-(NSString *) textForSubtitle
{
    NSString *subtitleString = nil;
    if ([self.mediaItem isEpisode])
    {
        NSString *seriesName = [[self.mediaItem.tags objectForKey:TVTagSeriesName] lastObject];
        subtitleString = seriesName;
    }
    else
    {
        subtitleString = [[self.mediaItem.tags objectForKey:TVTagGenre] lastObject];
        subtitleString = (subtitleString.length == 0) ? @"" : [subtitleString uppercaseString];
        subtitleString = (![subtitleString isEqualToString:@"UNKNOWN"] && ![subtitleString isEqualToString:@"UNKOWN"]) ? subtitleString : @"";
    }
    return [subtitleString uppercaseString];
}

-(NSURL *) pictureURL
{
    return [self.mediaItem pictureURLForPictureSize:self.pictureSize];
}

#pragma mark - UI
/*!
 * Calculates the required size for the labels and center them vertically in the cell.
 */

-(void) layoutLabels
{
    
    self.lblItemTitle.text = [self textForTitle];
    //self.lblItemSubtitle.text = [self textForSubtitle];
    
    if(self.cellType != kMediaItemType_ipad2Related)
    {
         self.lblItemSubtitle.text = [self textForSubtitle];
        [self calculateHeightForTitleAndSubTitleRows];
    }
    else{
        
        // show Geners
        NSMutableString *genersStr = [NSMutableString string];
        BOOL first= YES;
        for(NSString *tmpGen in [self.mediaItem.tags objectForKey:TVTagGenre])
        {
            tmpGen = (tmpGen.length == 0) ? @"" : tmpGen;
            NSString * test = [tmpGen uppercaseString];
            if ((tmpGen !=nil) && (![tmpGen isEqualToString:@""]) && (![test isEqualToString:@"UNKNOWN"] && ![test isEqualToString:@"UNKOWN"]))
            {
                if (first)
                {
                        first=NO;
                        [genersStr appendFormat:@"%@",tmpGen];
                }else{
                    [genersStr appendFormat:@"   %@",tmpGen];
                }
            }
        }
        if ([self.mediaItem isEpisode]) {
            self.lblItemSubtitle.text = [self textForSubtitle];
        }else{
            self.lblItemSubtitle.text = @"";

        }
        //self.lblDownText.font = [UIFont fontWithName:@"Helvetica-Neue" size:12];
        self.lblDownText.textColor = [UIColor colorWithRed:145.0/255.0 green:145.0/255.0 blue:145.0/255.0 alpha:1];
        self.lblDownText.text = [NSString stringWithString:genersStr];
    }
}

-(void) updateUI
{
    [self.btnItemImage setAlpha:0];
    [self layoutLabels];
    
    NSURL *URL = [self pictureURL];
    if ([URL scheme] != nil)
    {
        [self registerForImageNotifications];
        [[AsyncImageLoader sharedAsyncImageLoader] loadImageAtURL:URL];
        
    
    }
    else 
    {
        [self unregisterFromImageNotifications];
        [self showDefaultImage];
    }
    
}

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self.backgroundImage setHighlighted:selected];
}

#pragma mark - Notifications

-(void) registerForImageNotifications
{
    [self unregisterFromImageNotifications];
    AsyncImageLoader *loader = [AsyncImageLoader sharedAsyncImageLoader];
    NSURL *URL = [self pictureURL];
    if ([URL host] != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(imageLoaded:) 
                                                     name:[AsyncImageLoader imageLoadedNotificationNameForURL:URL] 
                                                   object:loader];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(imageFailed:) 
                                                     name:[AsyncImageLoader imageFailedNotificationNameForURL:URL]
                                                   object:loader];
    }
}

-(void) unregisterFromImageNotifications
{
    AsyncImageLoader *loader = [AsyncImageLoader sharedAsyncImageLoader];
    NSURL *URL = [self pictureURL];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[AsyncImageLoader imageLoadedNotificationNameForURL:URL] 
                                                  object:loader];
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:[AsyncImageLoader imageFailedNotificationNameForURL:URL]
                                                  object:loader];
}



-(void) imageFailed : (NSNotification *) notification
{
    [self unregisterFromImageNotifications];
    [self showDefaultImage];
}

-(void) imageLoaded : (NSNotification *) notification
{
    [self unregisterFromImageNotifications];
    UIImage *image = [notification.userInfo objectForKey:AsyncImageLoaderImageKey];
    [self.btnItemImage setBackgroundImage:image forState:UIControlStateNormal];
    [self revealButtonAnimated];
}

#pragma mark - Item Image
-(void) revealButtonAnimated
{
    self.btnItemImage.alpha = 0;
    self.btnItemImage.transform = CGAffineTransformMakeScale(1.5, 1.5);
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut) animations:^{
        self.btnItemImage.alpha = 1;
        self.btnItemImage.transform = CGAffineTransformMakeScale(1,1);
    }
     completion:^(BOOL finished) {
     }];
}

-(void) showDefaultImage
{
    UIImage *image = [UIImage imageNamed:DefaultItemImageName];
    [self.btnItemImage setBackgroundImage:image forState:UIControlStateNormal];
    [self revealButtonAnimated];
}

#pragma mark - Utility

-(void) updateCellWithPackageInfo:(NSDictionary *)packageInfo{

    ASLog2Debug(@"packageInfo: %@",packageInfo);////////
    
#warning TBD - check strings, localize and check dates
    NSString *expRenewStr;
    
    if([packageInfo objectForKey:kPackageInfoKey_endDate] && [packageInfo objectForKey:kPackageInfoKey_isRenewing]){
        if([[packageInfo objectForKey:kPackageInfoKey_isRenewing] boolValue] ){
            expRenewStr = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Renews at", nil),[packageInfo objectForKey:kPackageInfoKey_endDate]];
        }
        else{
            expRenewStr = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Expires at", nil),[packageInfo objectForKey:kPackageInfoKey_endDate]];
        }
    }
    else{
        expRenewStr = @"";
    }
    
    self.lblItemSubtitle.text = expRenewStr;
}

-(void) updateCellWithRental:(TVRental *)rentalInfo{
    
       
    NSString * infoMssage = NSLocalizedString(@"Expires in ", nil) ;
    
    ASLog(@"rentalInfo.endDate = %@",rentalInfo.endDate);
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    NSString *endDate = [df stringFromDate:rentalInfo.endDate];
    endDate = [endDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    [df release];
    if(endDate != nil)
    {
        infoMssage = [infoMssage stringByAppendingString:endDate];
    }
    self.lblItemSubtitle.text = infoMssage;
}


-(void) textForTitleInRental
{
    NSString *titleString = nil;
    if ([self.mediaItem isEpisode])
    {
        NSString *seriesName = [[self.mediaItem.tags objectForKey:TVTagSeriesName] lastObject];
        titleString = seriesName;
        
        NSString *seasonNumber = [self.mediaItem.metaData objectForKey:TVMetaDataSeasonNumber];
        seasonNumber = (seasonNumber.length > 0) ? seasonNumber : nil;
        NSString *episodeNumber = [self.mediaItem.metaData objectForKey:TVMetaDataEpisodeNumber];
        episodeNumber = (episodeNumber.length > 0) ? episodeNumber : NSLocalizedString(@"NA", nil);
        
        NSString *seasonAndEpisodeString = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Episode", nil),episodeNumber];
        if ([seasonNumber integerValue] > 0)
        {
            seasonAndEpisodeString = [NSString stringWithFormat:@"%@ %@, %@", NSLocalizedString(@"Season", nil), seasonNumber, seasonAndEpisodeString];
        }
        
        titleString = [titleString stringByAppendingString:[NSString stringWithFormat:@" - %@",seasonAndEpisodeString]];
    }
    else
    {
        titleString = self.mediaItem.name;
    }
    self.lblItemTitle.text =  titleString;
    [self calculateHeightForTitleAndSubTitleRows];
}

//
-(void)calculateHeightForTitleAndSubTitleRows
{
    CGFloat spaceBetweenTitleAndSubtitle = 5;
    
    CGSize titleConstraint = CGSizeMake(self.lblItemTitle.bounds.size.width, self.frame.size.height - self.lblItemSubtitle.bounds.size.height);
    CGSize titleSize = [self.lblItemTitle.text sizeWithFont:self.lblItemTitle.font constrainedToSize:titleConstraint];
    
    CGFloat totalLabelsHeightIncludingSpace = (titleSize.height + spaceBetweenTitleAndSubtitle + self.lblItemSubtitle.bounds.size.height);
    CGFloat yOriginForTitleLabel = (self.bounds.size.height - totalLabelsHeightIncludingSpace) / 2;
    
    CGRect titleFrame = self.lblItemTitle.frame;
    titleFrame.size.height = titleSize.height;
    titleFrame.origin.y = yOriginForTitleLabel;
    self.lblItemTitle.frame = titleFrame;
    
    CGRect subtitleFrame = self.lblItemSubtitle.frame;
    subtitleFrame.origin.y = titleFrame.origin.y + titleSize.height + spaceBetweenTitleAndSubtitle;
    subtitleFrame.origin.x = titleFrame.origin.x;
    self.lblItemSubtitle.frame = subtitleFrame;
}

+(TVMediaItemCell *) cell
{
    TVMediaItemCell *cell = nil;
    NSString *nibName = NSStringFromClass(self);
    NSArray * content = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for( id object in content)
    {
        if ([object isKindOfClass:self]) 
        {
            cell = object;
            break;
        }
    }
    return cell;
}
@end
