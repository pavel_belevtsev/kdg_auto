//
//  TVMenuButtonNavigationController.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVMenuButtonNavigationController.h"
#import "TVNavigationBar.h"
#import "SmartLogs.h"
#import "TVConstants.h"
#import "TvinciSDK.h"
@interface TVMenuButtonNavigationController ()

@end

@implementation TVMenuButtonNavigationController
@synthesize menuButton = _menuButton;

-(void) dealloc
{
    [_menuButton release];
    [super dealloc];
}

#pragma mark - Orientation

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}
// pre-iOS 6 support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}




-(void) setMenuButton:(UIButton *)menuButton
{
    if (_menuButton != menuButton)
    {
        [_menuButton release];
        _menuButton = [menuButton retain];
        if (_menuButton != nil)
        {
            // Add it and center it vertically.
            CGRect frame = _menuButton.bounds;
            frame.origin.y = (self.navigationBar.bounds.size.height - frame.size.height) / 2;
            frame.origin.x = 0;
            _menuButton.frame = frame;
            [self.navigationBar addSubview:_menuButton];
        }
    }
}

-(void) pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
   
        [super pushViewController:viewController animated:animated];
    
        ASLog2Debug(@"[MainMenuBt] Showing Menu button");////////
        [self setMenuButtonHidden:YES animated:animated];
   
}

-(UIViewController *) popViewControllerAnimated:(BOOL)animated
{
    // shefyg - for notifying back when needed
    [[NSNotificationCenter defaultCenter] postNotificationName:TVNotificationName_BackPressedOnCustomIPhoneNav object:nil];
    
    // The menu button should be shown only if there is only one ViewController in the stack
    UIViewController *toReturn = [super popViewControllerAnimated:animated];
    BOOL hide = (self.viewControllers.count > 1);
    
    ASLog2Debug(@"[MainMenuBt] Showing/Hidding Menu button");////////
    [self setMenuButtonHidden:hide animated:animated];
    
    
    
    return toReturn;
}

-(void) setMenuButtonHidden : (BOOL) hidden animated : (BOOL) animated
{
    if(self.menuButton.hidden != hidden)
    {
        NSTimeInterval duration = (animated) ? 0.3 : 0;
        CGFloat alpha = hidden ? 0.5 : 1;
        CGFloat transition = self.menuButton.bounds.size.width + 10;
        
        
        
        if (hidden) transition *= -1;
        CGRect frame = self.menuButton.frame;
        frame.origin.x += transition;
        if (frame.origin.x > 10)
        {
            ASLog(@"Menu Button is not in it place");
            if ([TVSessionManager sharedTVSessionManager].appTypeUsed ==TVAppType_iPad2)
            {
                frame.origin.x = 0;
            }
            else
            {
                frame.origin.x = 1.5;
            }
        }
        [UIView animateWithDuration:duration animations:^{
            self.menuButton.alpha = alpha;
            self.menuButton.frame = frame;
        } completion:^(BOOL finished) {
            self.menuButton.hidden = hidden;
        }];
        
    }
    
    ASLog2Debug(@"self.menuButton.hidden = %d", self.menuButton.hidden);
    ASLog2Debug(@"self.menuButton = %@", self.menuButton);
}


-(void) showMenuButtonHidden
{
    NSTimeInterval duration =  0;
    CGFloat alpha = 1;
    CGFloat transition = self.menuButton.bounds.size.width + 10;
    
    //if (hidden) transition *= -1;
    CGRect frame = self.menuButton.frame;
    frame.origin.x += transition;
    
    [UIView animateWithDuration:duration animations:^{
        self.menuButton.alpha = alpha;
        self.menuButton.frame = frame;
    } completion:^(BOOL finished) {
        self.menuButton.hidden = NO;
    }];
    
    ASLog2Debug(@"self.menuButton.hidden = %d", self.menuButton.hidden);
}


// shefyg - trying to solve the issue with not showing MainMenu button
-(void) setViewControllers:(NSArray *)viewControllers
{    
    //int lastCount = [self.viewControllers count];
    [super setViewControllers:viewControllers];
    
    ASLog2Debug(@"[MainMenuBt] Showing/hidding Menu button");////////
    [self setMenuButtonHidden:(viewControllers.count > 1) animated:YES];
}

#pragma mark - Utility
+(TVMenuButtonNavigationController *) menuButtonNavigationControllerWithCustomBar
{
    TVMenuButtonNavigationController *controller = nil;
    NSString *nibName = NSStringFromClass(self);
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    
    for (id item in contents)
    {
        if ([item isKindOfClass:[self class]])
        {
            controller = item;
        }
    }
    return controller;
}
@end
