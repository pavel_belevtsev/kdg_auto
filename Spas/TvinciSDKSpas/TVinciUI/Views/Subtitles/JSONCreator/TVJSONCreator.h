//
//  JSONCreator.h
//  SubtitlesTest
//
//  Created by quickode on 11/20/12.
//  Copyright (c) 2012 quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"


#define KEY_START_TIME @"startMillis"
#define KEY_END_TIME @"endMillis"
#define KEY_SUBTITLE_TEXT @"text"

#define KEY_SOURCE_LNG_URL @"lng_url"
#define KEY_LANGUAGE_IDENTIFIER @"lng_id"

#define VideoURL @"videoURL"
#define SUBTITLES_LANGUAGES_LIST @"subtitles_list"


@interface TVJSONCreator : NSObject

+(TVJSONCreator *)sharedTVJSONCreator;
-(NSString *) JSONSubtitlesFromXML:(NSString *) xmlSubtitles;
-(NSArray *) arraySmilFromXML:(NSString *) xmlSmil ;
-(NSMutableDictionary*)smilToDictionary:(NSString*)smilURL;

@end
