//
//  TVSubtitleFactory.m
//  SubtitleDemo
//
//  Created by Alexander Israel on 11/14/12.
//
//

#import "TVSubtitleFactory.h"
#import "SBJson.h"
#import "SmartLogs.h"

@implementation SubRipItem

@synthesize startTime, endTime, text, uniqueID, index;
@dynamic startTimeString, endTimeString;

- (id)init {
    self = [super init];
    if (self) {
        uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    return self;
}

-(NSString *)startTimeString {
    return [self _convertCMTimeToString:self.startTime];
}

-(NSString *)endTimeString {
    return [self _convertCMTimeToString:self.endTime];
}

-(NSString *)_convertCMTimeToString:(CMTime)theTime {
    // Need a string of format "hh:mm:ss". (No milliseconds.)
    NSInteger seconds = (NSInteger)(theTime.value / theTime.timescale);
    NSDate *date1 = [NSDate new];
    NSDate *date2 = [NSDate dateWithTimeInterval:seconds sinceDate:date1];
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *converted = [[NSCalendar currentCalendar] components:unitFlags fromDate:date1 toDate:date2 options:0];
    
    NSMutableString *str = [NSMutableString stringWithCapacity:6];
    if ([converted hour] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%ld:", (long)[converted hour]];
    if ([converted minute] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%ld:", (long)[converted minute]];
    if ([converted second] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%ld", (long)[converted second]];
    
    [date1 release];
    return str;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@ ---> %@\n%@", self.startTimeString, self.endTimeString, self.text];
}

-(NSInteger)startTimeInSeconds {
    if (self.startTime.value==0)
        return 0;
    
    return (NSInteger)(self.startTime.value / self.startTime.timescale);
}

-(NSInteger)endTimeInSeconds {
    if (self.endTime.value==0)
        return 0;
    
    return (NSInteger)(self.endTime.value / self.endTime.timescale);
}

-(BOOL)containsString:(NSString *)str {
    NSRange searchResult = [self.text rangeOfString:str options:NSCaseInsensitiveSearch];
    if (searchResult.location == NSNotFound) {
        if ([str length] < 9) {
            searchResult = [[self startTimeString] rangeOfString:str options:NSCaseInsensitiveSearch];
            if (searchResult.location == NSNotFound) {
                searchResult = [[self endTimeString] rangeOfString:str options:NSCaseInsensitiveSearch];
                if (searchResult.location == NSNotFound) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    } else {
        return true;
    }
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeCMTime:startTime forKey:@"startTime"];
    [encoder encodeCMTime:endTime forKey:@"endTime"];
    [encoder encodeObject:text forKey:@"text"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    self = [self init];
    self.startTime = [decoder decodeCMTimeForKey:@"startTime"];
    self.endTime = [decoder decodeCMTimeForKey:@"endTime"];
    self.text = [decoder decodeObjectForKey:@"text"];
    return self;
}


@end

@implementation TVSubtitleFactory

-(SubRipItem*)indexOfSubRipItemWithStartTime:(CMTime)theTime
{
    NSInteger desiredTimeInSeconds = (NSInteger)(theTime.value / theTime.timescale);
    
    SubRipItem *currentObj = [self.subtitleItems objectAtIndex:self.currentSubtitleIndex];
    
    SubRipItem *returnValue=currentObj;
    
    if ((desiredTimeInSeconds >= [currentObj startTimeInSeconds]) && (desiredTimeInSeconds <= [currentObj endTimeInSeconds])) {
        return returnValue;
    }else{
        
        returnValue=nil;
        
        SubRipItem *nextObj = [self.subtitleItems objectAtIndex:(self.currentSubtitleIndex+1)];
        
        if(desiredTimeInSeconds >= [(SubRipItem *)nextObj startTimeInSeconds]  &&  desiredTimeInSeconds <= [(SubRipItem *)nextObj endTimeInSeconds])
        {
            self.currentSubtitleIndex = nextObj.index;
            returnValue =  [self.subtitleItems objectAtIndex:self.currentSubtitleIndex];
        }
        
        return returnValue;
    }
    
}


-(void)populateFromJson:(NSString *)json delay:(NSInteger)delay{
    
    self.currentSubtitleIndex = 0;
    
    [self.subtitleItems removeAllObjects];
    
    if (self.subtitleItems==nil)
        self.subtitleItems =[NSMutableArray array];
    
    __block  NSMutableArray *arr = [json JSONValue];
    
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        SubRipItem *item = [[SubRipItem new]autorelease];
        
        NSDictionary *line =[arr objectAtIndex:idx];
        
        long miliStart = [[line objectForKey:@"startMillis"]longValue];
        long secStart = miliStart - delay;//-3600000;//yes
        
        CMTime millisecondsCMTimeStart = CMTimeMake(secStart,1000);
        item.startTime = millisecondsCMTimeStart;
        
        long miliEnd = [[line objectForKey:@"endMillis"]longValue];
        long secEnd = miliEnd - delay;//-3600000;//yes
        
        CMTime millisecondsCMTimeStartEnd = CMTimeMake(secEnd,1000);
        item.endTime = millisecondsCMTimeStartEnd;
        
        NSString*string = [line objectForKey:@"text"];
        
        item.text=[string stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
        
        item.index = idx;
        
        [self.subtitleItems addObject:item];
    }];
}




-(SubRipItem*)indexOfSubRipItemAfterSeekWithStartTime:(CMTime)theTime{
    __block  NSInteger desiredTimeInSeconds = (NSInteger)(theTime.value / theTime.timescale);
    
    NSUInteger index = [self.subtitleItems indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ((desiredTimeInSeconds >= [(SubRipItem *)obj startTimeInSeconds]) && (desiredTimeInSeconds <= [(SubRipItem *)obj endTimeInSeconds]))
        {
            return YES;
        }else {
            return NO;
        }
    }];
    
    if (index!=NSIntegerMax)
    {
        self.currentSubtitleIndex=index;
        
        return [self.subtitleItems objectAtIndex:index];
        
    }else {
        return nil;
    }
    
}

-(void) startVideoTimer
{
    // Update the UI 5 times per second on the main queue
    // Keep a strong reference to _timer in ARC
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(_timer, DISPATCH_TIME_NOW, (1.0 / 5.0) * NSEC_PER_SEC, 0.25 * NSEC_PER_SEC);
    
    dispatch_source_set_event_handler(_timer, ^{
        [self.delegate updateVisibleSubtitles];
    });
    
    // Start the timer
    dispatch_resume(_timer);
}

-(void) stopVideoTimer
{
    if (_timer!=nil) {
        dispatch_source_cancel(_timer);
        _timer=nil;
    }
}

-(void)pauseVideoTimer
{
    dispatch_suspend(_timer);
}

-(void)resumeVideoTimer
{
    dispatch_resume(_timer);
}


- (void)dealloc
{
    [self stopVideoTimer];
    [super dealloc];
}


/*EXAMPLE
 
#import "TVSubtitleFactory.h"
 
@interface ViewController : UIViewController <TVSubtitleFactoryDelegate>
{
    TVSubtitleFactory *subFactory;
}
@property (retain, nonatomic) NSArray *subtitleItems;
@property (retain, nonatomic) TVSubtitleFactory *subFactory;


@end
 
@implementation ViewController
 
 - (void)viewDidLoad
 {
 
 player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:@"http://switch3.castup.net/cunet/gm.asp?ai=246&ar=I4000411111_iphone_hd&ak=null&device=iphone&streamnametype=1"]];
 
 
 NSString *path = [[NSBundle mainBundle]pathForResource:@"subs" ofType:@"json"];
 NSString *json = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
 self.subtitleItems = [TVSubtitleFactory populateFromJson:json delay:3600000];
 
 subFactory = [[TVSubtitleFactory alloc]init];
 [subFactory setDelegate:self];
 
 
 [player play];
 [subFactory startVideoTimer];
 
 [super viewDidLoad];
 
 }

 -(void) updateVisibleSubtitles
 {
 NSTimeInterval time =[player currentPlaybackTime];
 
 long longtime = time*1000;
 
 self.movieTimeStamp.text=[NSString stringWithFormat:@"%f",time];
 
 if (time!=0) {
 CMTime timecm = CMTimeMake(longtime, 1000);
 
 SubRipItem *itm=nil;
 
 if (NO) {
 itm = [subFactory indexOfSubRipItemAfterSeekWithStartTime:timecm array:self.subtitleItems];
 }else
 itm = [subFactory indexOfSubRipItemWithStartTime:timecm array:self.subtitleItems];
 
 if (itm!=nil)
 {
 self.subtitle.hidden=NO;
 self.subtitle.text=[NSString stringWithFormat:@"%@",itm.text];//\u200F
 
 }else {
 self.subtitle.text=@"";
 self.subtitle.hidden=YES;
 }
 
 }
 }


*/


@end
