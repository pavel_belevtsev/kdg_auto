//
//  FLImageView.m
//  FullyLoaded
//
//  Created by Anoop Ranganath on 1/1/11.
//  Copyright 2011 Anoop Ranganath. All rights reserved.
//
//  
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "AsyncImageView.h"
#import "AsyncImageLoader.h"

@interface AsyncImageView()

@property (nonatomic, readwrite, retain) NSURL *imageURL;
@property (nonatomic, readwrite, retain) UIActivityIndicatorView *activityIndicatorView;

-(void) unregisterFromNotificationCenter;
- (void)populateImage:(UIImage *)anImage;
- (void)setLoading:(BOOL)isLoading;
- (void)configureActivityIndicatorView;
-(void) setup;

@end

@implementation AsyncImageView
@synthesize showsLoadingActivity = _showsLoadingActivity;
@synthesize imageURL;
@synthesize activityIndicatorView;
@synthesize delegate = _delegate;
@synthesize myPlaceholderImage;

#pragma mark - Memory
- (void)dealloc
{
    self.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[self unregisterFromNotificationCenter];
    self.imageURL = nil;
    self.activityIndicatorView = nil;
    self.myPlaceholderImage=nil;
    [super dealloc];
}

#pragma mark Setup
-(void) setup
{
    self.showsLoadingActivity = YES;
}

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder 
{
    self = [super initWithCoder:aDecoder];
    if (self) 
    {
        [self setup];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

#pragma mark - Notifications
-(void) registerForNotifications
{
    AsyncImageLoader *loader = [AsyncImageLoader sharedAsyncImageLoader];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(imageLoaded:)
                                                 name:[AsyncImageLoader imageLoadedNotificationNameForURL:self.imageURL] 
                                               object:loader];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(imageFailed:)
                                                 name:[AsyncImageLoader imageFailedNotificationNameForURL:self.imageURL]
                                               object:loader];
}

-(void) unregisterFromNotificationCenter
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[AsyncImageLoader imageLoadedNotificationNameForURL:self.imageURL] object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[AsyncImageLoader imageFailedNotificationNameForURL:self.imageURL] object:nil];
}

- (void)imageLoaded : (NSNotification *) notification 
{
    [self unregisterFromNotificationCenter];
    NSDictionary *userInfo = notification.userInfo;
    NSString *URLString = [userInfo objectForKey:AsyncImageLoaderURLStringKey];
    
    if ([[self.imageURL absoluteString] isEqualToString:URLString])
    {
        UIImage *image = [userInfo objectForKey:AsyncImageLoaderImageKey];
        if (image) 
        {
            [self setLoading:NO];
            [self populateImage:image];
        }
    }
}

- (void) imageFailed : (NSNotification *) notification
{
//    ASLog(@"Failed To Load Image at URL: %@",self.imageURL);
    
    [self unregisterFromNotificationCenter];
    
    if (self.placeHolderURL!=nil && self.placeHolderURLWasUsed==NO)
    {
        self.placeHolderURLWasUsed =YES;
        [self loadImageAtURL:self.placeHolderURL placeholderImage:self.myPlaceholderImage];
    }
    else
    {

        if (self.myPlaceholderImage!=nil)
        {
            [self populateImage:self.myPlaceholderImage];
        }
        
        [self setLoading:NO];
        
        if ([self.delegate respondsToSelector:@selector(asyncImageView:didFailToLoadImageWithError:)])
        {
            NSError *error = [notification.userInfo objectForKey:AsyncImageLoaderErrorKey];
            [self.delegate asyncImageView:self didFailToLoadImageWithError:error];
        }
    }
}

#pragma mark - API
- (void)loadImageAtURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage 
{
    self.myPlaceholderImage=nil;
    if ([self.imageURL isEqual:url] == NO)
    {
        if ([url scheme] && [url host])
        {
            self.imageURL = url;
            [self unregisterFromNotificationCenter];
            self.image = nil;
            
            if(self.showsLoadingActivity)
            {
                [self setLoading:YES];
            }
            [self populateImage:placeholderImage];
            
            [self registerForNotifications];
            [[AsyncImageLoader sharedAsyncImageLoader] loadImageAtURL:self.imageURL];
        }
        else
        {
            self.imageURL = nil;
            [self populateImage:placeholderImage];
        }
    }
}

- (void)loadImageAtURL:(NSURL *)url andPlaceholderImage:(UIImage *)placeholderImage
{
    self.myPlaceholderImage=placeholderImage;
    
    if ([self.imageURL isEqual:url] == NO)
    {
        if ([url scheme] && [url host])
        {
            self.imageURL = url;
            [self unregisterFromNotificationCenter];
            self.image = nil;
            
            if(self.showsLoadingActivity)
            {
                [self setLoading:YES];
            }
            //[self populateImage:placeholderImage];
            
            [self registerForNotifications];
            //            [[AsyncImageLoader sharedAsyncImageLoader] performSelectorInBackground:@selector(loadImageAtURL:) withObject:self.imageURL];
            [[AsyncImageLoader sharedAsyncImageLoader] loadImageAtURL:self.imageURL];
        }
        else
        {
            self.imageURL = nil;
            [self populateImage:placeholderImage];
        }
    }
}


- (void) loadImageAtURL:(NSURL *)url placeholderURL:(NSURL *) placeHolderURL andPlaceholderImage:(UIImage *)placeholderImage
{
    self.placeHolderURL = placeHolderURL;
    self.placeHolderURLWasUsed = NO;
    [self loadImageAtURL:url placeholderImage:placeholderImage];
}

#pragma mark - Overrides

- (void)setShowsLoadingActivity:(BOOL)shouldShowActivity
{
    if (_showsLoadingActivity != shouldShowActivity)
    {
        _showsLoadingActivity = shouldShowActivity;
        
        if(shouldShowActivity)
        {
            if(self.activityIndicatorView == nil)
            {
                [self configureActivityIndicatorView];
            }
        }
        else
        {
            if (self.activityIndicatorView) {
                [self.activityIndicatorView removeFromSuperview];
                self.activityIndicatorView = nil;
            }
            
        }
    }
}

#pragma mark - Private
- (void)populateImage:(UIImage *)anImage 
{
    self.alpha = 0;
    self.image = anImage;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1;
    }];
    
    if ([self.delegate respondsToSelector:@selector(asyncImageView:didFinishLoadingImage:)])
    {
        [self.delegate asyncImageView:self didFinishLoadingImage:anImage];
    }
}


// if YES, shows and animates the activity indicator at the center of the view
- (void)setLoading:(BOOL)isLoading 
{
    if(isLoading)
    {
        [self.activityIndicatorView startAnimating];
    }
    else 
    {
        [self.activityIndicatorView stopAnimating];
    }
}

// sets up self.activityIndicatorView and adds it as a subview
- (void)configureActivityIndicatorView
{
    self.activityIndicatorView =
    [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
    
    // center the indicator
    CGRect activityIndicatorFrame = self.activityIndicatorView.frame;
    activityIndicatorFrame.origin.x = (self.frame.size.width / 2.f) - (activityIndicatorFrame.size.width / 2.f);
    activityIndicatorFrame.origin.y = (self.frame.size.height / 2.f) - (activityIndicatorFrame.size.height / 2.f);
    self.activityIndicatorView.frame = activityIndicatorFrame;
    self.activityIndicatorView.backgroundColor = [UIColor clearColor];
    self.activityIndicatorView.hidesWhenStopped = YES;
    
    [self addSubview:self.activityIndicatorView];
}
@end
