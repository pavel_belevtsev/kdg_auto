//
//  TVSubscriptionCell.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVSubscriptionCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (retain, nonatomic) IBOutlet UILabel *lblItemTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblItemSubtitle;
@property (retain, nonatomic) IBOutlet UIButton *btnSubscriptionType;
@property (retain, nonatomic) IBOutlet UIButton *btnItemImage;
@property (retain, nonatomic) IBOutlet UILabel *lblExpirationDate;
@end
