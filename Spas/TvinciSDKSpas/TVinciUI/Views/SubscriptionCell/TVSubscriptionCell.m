//
//  TVSubscriptionCell.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/8/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSubscriptionCell.h"


@implementation TVSubscriptionCell
@synthesize backgroundImage;
@synthesize lblItemTitle;
@synthesize lblItemSubtitle;
@synthesize btnSubscriptionType;
@synthesize btnItemImage;
@synthesize lblExpirationDate;

-(void) setup
{
    UIImage *bg = [self.btnSubscriptionType backgroundImageForState:UIControlStateNormal];
    bg = [bg stretchableImageWithLeftCapWidth:8 topCapHeight:6];
    [self.btnSubscriptionType setBackgroundImage:bg forState:UIControlStateNormal];
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        [self setup];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)dealloc 
{
    self.lblItemTitle = nil;
    self.lblItemSubtitle = nil;
    self.btnItemImage = nil;
    self.btnSubscriptionType = nil;
    self.lblExpirationDate = nil;
    self.backgroundImage = nil;
    [super dealloc];
}

-(void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    [self.backgroundImage setHighlighted:selected];
}
@end
