//
//  TVSelectableMediaItemView.m 
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSelectableMediaItemView.h"
#import "TVMediaItem.h"
#import "AsyncImageLoader.h"
#import "SmartLogs.h"
#import "TVSessionManager.h"
#import "SmartLogs.h"
#import "TVSubscription.h"
#import "TVRental.h"
#import "TVConstants.h"

#define DEFAULT_ITEM_IMAGE @"default_item_image"
#define DEFAULT_ITEM_IMAGE_16X9 @"default_item_image16x9"

#define DEFAULT_LOAD_ITEM_IMAGE @"default_item_imageWhithFrame"
#define DEFAULT_LOAD_ITEM_IMAGE_16X9 @"default_item_image16x9WhiteFrame"

#define BADGE_BUTTON_HEIGHT 14

// changed from 5 to 0
#define BADGE_BUTTON_MARGINS 5
#define BADGE_BUTTON_BACKGROUND_IMAGE_NAME @"green_badge_background"
#define TITLE_FONT_NAME @"HelveticaNeue-Bold"
#define TITLE_FONT_SIZE 13
#define CLOCK_IMAGE_NAME @"ic_clock"

#define DELETE_BUTTON_IMAGE_NAME @"delete_button"


@interface TVSelectableMediaItemView ()
@property (nonatomic, copy) ActionBlock didSelectItemBlock;
@property (nonatomic, copy) ActionBlock didEditItemBlock;
@property (nonatomic ,copy)  ActionWithSubscriptionBlock didSelectItemWithSubscriptionItemBlock;
@property (nonatomic, retain) UIButton *deleteButton;
@property (nonatomic, copy) ActionBlock deleteItemBlock;

@property (nonatomic, readonly) NSURL *pictureURL;



@end

@implementation TVSelectableMediaItemView
@synthesize didSelectItemWithSubscriptionItemBlock = _didSelectItemWithSubscriptionItemBlock;
@synthesize didSelectItemBlock = _didSelectItemBlock;
@synthesize deleteItemBlock = _deleteItemBlock;
@synthesize didEditItemBlock=_didEditItemBlock;
@synthesize mediaItem = _mediaItem;
@synthesize imageButton = _imageButton;
@synthesize pictureSize = _pictureSize;
@synthesize badgeButton = _badgeButton;
@synthesize titleLabel = _titleLabel;
@synthesize imageButtonEdgeInsets = _imageButtonEdgeInsets;
@synthesize deleteButton = _deleteButton;
@synthesize titleEdgeInsets = _titleEdgeInsets;
@synthesize titelView = _titelView;
@synthesize likeCount = _likeCount;
@synthesize likeImage = _likeImage;
@synthesize deleteButoon = _deleteButoon;
@synthesize expirationDateView = _expirationDateView;
@synthesize showExpirationDate = _showExpirationDate;
@synthesize expirationDateLabel = _expirationDateLabel;
@synthesize subscriptionItem;
@synthesize secondaryTitleLabel = _secondaryTitleLabel;
@synthesize selectedSelectoreWasAdded = _selectedSelectoreWasAdded;
@synthesize cellInfoDict = _cellInfoDict;
@synthesize imageFrame = _imageFrame;
/*
 -(void) setDidSelectItemBlock:(ActionBlock)didSelectItemBlock;
 
 -(void) setDidSelectItemWithSubscriptionItemBlock:(ActionWithSubscriptionBlock)didSelectItemBlock;
 
 
 -(void) setDidEditItemBlock:(ActionBlock)didEditItemBlock;
 
 -(void) setDeleteItemBlock:(ActionBlock)deleteItemBlock;
 */
#pragma mark - Memory

-(void)dealloc
{
    [_cellInfoDict release];
    [subscriptionItem release];
    [self unregisterFromImageNotifications];
    [_badgeButton release];
    [_titleLabel release];
    [_mediaItem release];
    [_didSelectItemBlock release];
    [_didSelectItemWithSubscriptionItemBlock release];
    [_deleteItemBlock release];
    [_didEditItemBlock release];
    [_imageButton release];
    [_pictureSize release];
    [_deleteButton release];
    [_titelView release];
    [_likeCount release];
    [_likeImage release];
    [_deleteButoon release];
    [_lblPackagesInfo release];
    [_secondaryTitleLabel release];
    [super dealloc];
}

#pragma mark - Getter

-(void) createDeleteButton
{
    UIImage *deleteImage = [UIImage imageNamed:DELETE_BUTTON_IMAGE_NAME];
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setImage:deleteImage forState:UIControlStateNormal];
    CGRect frame = CGRectZero;
    frame.size = deleteImage.size;
    frame.origin.x = BADGE_BUTTON_MARGINS;
    frame.origin.y = (self.imageButton.bounds.size.height - BADGE_BUTTON_MARGINS - deleteImage.size.height);
    self.deleteButton.frame = frame;
    [self.deleteButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
}

-(UIButton *) deleteButton
{
    if (_deleteButton == nil)
    {
        [self createDeleteButton];
    }
    return _deleteButton;
}

-(NSURL *) pictureURL
{
    //ASLog2Debug(@"[Picture][URL] Getting pictureURL for %@",self.mediaItem);////////
    if (self.mediaItem) {
        NSURL *url = [self.mediaItem pictureURLForPictureSize:self.pictureSize];
//        ASLog2Debug(@"[Selected][pictureURL]%@",url);////////
        return url;//[self.mediaItem pictureURLForPictureSize:self.pictureSize];
    }else
    {
        return nil;
    }
  
}

-(CGRect) selectedPictureFrame{
    CGRect frm = self.bounds;
    if([_cellInfoDict objectOrNilForKey:kCellInfoDictKey_SelectedPictureFrameHeight]){
        frm.size.height = [[_cellInfoDict objectOrNilForKey:kCellInfoDictKey_SelectedPictureFrameHeight] floatValue];
    }
    
    return frm;
    
}

#pragma mark - Setters

-(void) setMediaItem:(TVMediaItem *)mediaItem
{
    // NSLog(@"[mediaItem]");////////
    if (_mediaItem != mediaItem)
    {
        [self unregisterFromImageNotifications];
        [_mediaItem release];
        _mediaItem = [mediaItem retain];
        //ASLog(@"%@", [self class]);
        [self update];
    }
}

- (void)updateImageButtonFrameWithNewEdgeInsets
{
    
    CGRect frame = [self selectedPictureFrame];//self.bounds;
    frame.size.width -= (self.imageButtonEdgeInsets.right + self.imageButtonEdgeInsets.left);
    frame.size.height -= (self.imageButtonEdgeInsets.top + self.imageButtonEdgeInsets.bottom);
    frame.origin.x = self.imageButtonEdgeInsets.left;
    frame.origin.y = self.imageButtonEdgeInsets.top;
    self.imageButton.frame = frame;
}

-(void) setImageButtonEdgeInsets:(UIEdgeInsets)imageButtonEdgeInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(self.imageButtonEdgeInsets, imageButtonEdgeInsets) == NO)
    {
        _imageButtonEdgeInsets = imageButtonEdgeInsets;
        [self updateImageButtonFrameWithNewEdgeInsets];
    }
}

-(void) updateTitleFrameWithNewEdgeInsets
{
    [self update];
    
    CGRect titleFrame = self.titleLabel.frame;
    CGRect availableFrame = self.bounds;
    
    CGFloat widthLimitedByInsets = availableFrame.size.width - (self.titleEdgeInsets.right + self.titleEdgeInsets.left);
    titleFrame.size.width = MIN(titleFrame.size.width, widthLimitedByInsets);
    
    CGFloat heightLimitedByInsets = availableFrame.size.height - (self.titleEdgeInsets.top + self.titleEdgeInsets.bottom);
    titleFrame.size.height = MIN(titleFrame.size.height, heightLimitedByInsets);
    
    // Title is anchored to bootom-left.
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO] && [[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_AppForIphone]){
        titleFrame.origin.x = 0;
    }
    else{
        titleFrame.origin.x = self.titleEdgeInsets.left;
    }
    titleFrame.origin.y = (availableFrame.size.height - titleFrame.size.height - self.titleEdgeInsets.bottom);
    
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO] && [[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_AppForIphone]){
        titleFrame.origin.y += 9;
    }
    
    
    // ATTENTION  - here we take frame values from cellInfoDict (which provided from the viewController - OUTSIDE)
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_originY]){
        // TBD
        titleFrame.origin.y = [[self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_originY] floatValue];
    }
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_originX]){
        // TBD
        titleFrame.origin.x = [[self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_originX] floatValue];
    }
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight]){
        // TBD
        titleFrame.size.height = [[self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight] floatValue];
    }
    
    self.titleLabel.frame = titleFrame;
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_bgColorForTitle]){
        // TBD
        [self.titleLabel setBackgroundColor:[self.cellInfoDict objectForKey:kCellInfoDictKey_bgColorForTitle]];
    }
    
    CGRect secTitleFrame = self.secondaryTitleLabel.frame;
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_SeconderyTitleFrame_originY]){
        // TBD
        secTitleFrame.origin.y = [[self.cellInfoDict objectForKey:kCellInfoDictKey_SeconderyTitleFrame_originY] floatValue];
    }
    
    self.secondaryTitleLabel.frame = secTitleFrame;
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_titleIsHidden]){
        // TBD
        self.titleLabel.hidden = [[self.cellInfoDict objectForKey:kCellInfoDictKey_titleIsHidden] boolValue];
    }
    
}

-(void) setTitleEdgeInsets:(UIEdgeInsets)titleEdgeInsets
{
    if (UIEdgeInsetsEqualToEdgeInsets(self.titleEdgeInsets, titleEdgeInsets) == NO)
    {
        _titleEdgeInsets = titleEdgeInsets;
        [self updateTitleFrameWithNewEdgeInsets];
    }
}

-(void)setCellInfoDict:(NSDictionary *)cellInfoDict
{
    if(_cellInfoDict != cellInfoDict){
        _cellInfoDict = [cellInfoDict retain];
        if(_cellInfoDict != nil){
            [self buildUIFromScratch];
        }
    }
    
}

#pragma mark - Getters

-(UILabel *) titleLabel
{
    if (_titleLabel == nil)
    {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont fontWithName:TITLE_FONT_NAME size:TITLE_FONT_SIZE];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.shadowColor = [UIColor blackColor];
        _titleLabel.shadowOffset = CGSizeMake(0, -1);
        _titleLabel.numberOfLines = 2;
        
        _titleLabel.adjustsFontSizeToFitWidth = NO;
        _titleLabel.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 0);
    }
    return _titleLabel;
}

-(UIView *) expirationDateView
{
    if (_expirationDateView == nil)
    {
        _expirationDateView = [[UIView alloc] init];
        _expirationDateView.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin);
        UIImage *bg = [UIImage imageNamed:BADGE_BUTTON_BACKGROUND_IMAGE_NAME];
        if ([bg respondsToSelector:@selector(resizableImageWithCapInsets:)])
        {
            bg = [bg resizableImageWithCapInsets:UIEdgeInsetsMake((bg.size.height / 2), (bg.size.width / 2), (bg.size.height / 2), (bg.size.width / 2))];
        }
        else
        {
            bg = [bg stretchableImageWithLeftCapWidth:(bg.size.width / 2) topCapHeight:(bg.size.height/2)];
        }
        
        //backgroundImage
        UIImageView * bgImageView = [[UIImageView alloc] initWithImage:bg];
        CGRect frame = bgImageView.frame;
        frame.origin = CGPointZero;
        bgImageView.frame = frame;
        
        //textLabel
        self.expirationDateLabel = [[[UILabel alloc] init] autorelease];
        self.expirationDateLabel.font = [UIFont boldSystemFontOfSize:9];
        self.expirationDateLabel.textColor = [UIColor blackColor];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMMM dd"];
        NSString *dateString = [dateFormat stringFromDate:self.mediaItem.expirationDate];
        [dateFormat release];
        
        self.expirationDateLabel.text = dateString; //@"test";
        self.expirationDateLabel.backgroundColor = [UIColor clearColor];
        [self.expirationDateLabel sizeToFit];
        
        //clockImage
        UIImageView * clockImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:CLOCK_IMAGE_NAME]];
        
        //calculate bg Image sizes
        CGFloat bgWidth = self.expirationDateLabel.frame.size.width + clockImageView.frame.size.width + BADGE_BUTTON_MARGINS*3;
        frame = bgImageView.frame;
        frame.size.width = bgWidth;
        frame.origin = CGPointZero;
        bgImageView.frame=frame;
        
        //calculating expirationDateView frame
        frame = CGRectZero;
        frame.origin.x = BADGE_BUTTON_MARGINS;
        frame.origin.y = (self.imageButton.bounds.size.height - BADGE_BUTTON_HEIGHT - BADGE_BUTTON_MARGINS);
        frame.size.height = BADGE_BUTTON_HEIGHT;
        frame.size.width = bgImageView.frame.size.width;
        _expirationDateView.frame = frame;
        
        //calculating clock image sizes
        frame = clockImageView.frame;
        frame.origin = CGPointZero;
        frame.origin.x +=BADGE_BUTTON_MARGINS;
        clockImageView.frame=frame;
        clockImageView.center = CGPointMake(clockImageView.center.x, _expirationDateView.frame.size.height/2);
        
        //calculating label sizes
        frame = self.expirationDateLabel.frame;
        frame.origin.x += clockImageView.frame.origin.x + clockImageView.frame.size.width +BADGE_BUTTON_MARGINS;
        self.expirationDateLabel.frame=frame;
        
        [_expirationDateView addSubview:bgImageView];
        [_expirationDateView addSubview:clockImageView];
        [_expirationDateView addSubview:self.expirationDateLabel];
        
        [_expirationDateLabel release];
        [bgImageView release];
        [clockImageView release];
        
    }
    return _expirationDateView;
}


-(UIButton *) badgeButton
{
    if (_badgeButton == nil)
    {
        _badgeButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        _badgeButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin);
        _badgeButton.userInteractionEnabled = NO;
        UIImage *bg = [UIImage imageNamed:BADGE_BUTTON_BACKGROUND_IMAGE_NAME];
        if ([bg respondsToSelector:@selector(resizableImageWithCapInsets:)])
        {
            bg = [bg resizableImageWithCapInsets:UIEdgeInsetsMake((bg.size.height / 2), (bg.size.width / 2), (bg.size.height / 2), (bg.size.width / 2))];
        }
        else
        {
            bg = [bg stretchableImageWithLeftCapWidth:(bg.size.width / 2) topCapHeight:(bg.size.height/2)];
        }
        [_badgeButton setBackgroundImage:bg forState:UIControlStateNormal];
        _badgeButton.titleLabel.font = [UIFont boldSystemFontOfSize:9];
        [_badgeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        CGRect frame = CGRectZero;
        frame.origin.x = BADGE_BUTTON_MARGINS;
        frame.origin.y = (self.imageButton.bounds.size.height - BADGE_BUTTON_HEIGHT - BADGE_BUTTON_MARGINS);
        frame.size.height = BADGE_BUTTON_HEIGHT;
        _badgeButton.frame = frame;
    }
    return _badgeButton;
}

-(UIButton *) imageButton
{
    if (_imageButton == nil)
    {
        _imageButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        _imageButton.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        _imageButton.backgroundColor = [UIColor clearColor];
        [_imageButton addTarget:self action:@selector(selected:) forControlEvents:UIControlEventTouchUpInside];
        _selectedSelectoreWasAdded = YES;
        _imageButton.adjustsImageWhenHighlighted = YES;
        //  [_imageButton addSubview:self.badgeButton];
    }
    else{
        // ASLog2Debug(@"[imageButton] ImageButton is not nil");////////
    }
    return _imageButton;
}

-(NSString *) description{
    
    return [NSString stringWithFormat:@"\n<[Selected]\n mediaItem: %@  has selected: %d ",self.mediaItem, _selectedSelectoreWasAdded];
}

#pragma mark - Setup

-(void) setup
{
    self.pictureSize = [TVPictureSize iPhoneSpotlightItemPictureSize];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self.imageButton addGestureRecognizer:longPress];
    [longPress release];
    
}

-(void) buildUIFromScratch
{
    
    _imageButtonEdgeInsets = UIEdgeInsetsZero;
    [self updateImageButtonFrameWithNewEdgeInsets];
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO]){
        _titleEdgeInsets = UIEdgeInsetsMake(0, BADGE_BUTTON_MARGINS, BADGE_BUTTON_MARGINS, 0);
    }
    else{
        _titleEdgeInsets = UIEdgeInsetsMake(0, BADGE_BUTTON_MARGINS, BADGE_BUTTON_MARGINS, 0);
    }
    [self updateTitleFrameWithNewEdgeInsets];
    self.backgroundColor = [UIColor clearColor];
    
    if([[_cellInfoDict allKeys]containsObject:kCellInfoDictKey_tileIsAnimated]
       && [[_cellInfoDict objectForKey:kCellInfoDictKey_tileIsAnimated] boolValue] == NO){
        self.imageButton.frame = [self selectedPictureFrame];//self.bounds;
    }

    
 
    [self addSubview:self.imageButton];
    [self addSubview:self.titleLabel];
    
    if(self.showExpirationDate )
    {
        [self addSubview:self.expirationDateView];
    }
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_numberOfLines])
    {
        _titleLabel.numberOfLines = [[self.cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_numberOfLines] intValue];
    }
    
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_SelectedFrameHeight])
    {
        CGRect selectedFrm = self.frame;
        selectedFrm.size.height = [[self.cellInfoDict objectForKey:kCellInfoDictKey_SelectedFrameHeight] floatValue];
        self.frame = selectedFrm;
        
    }
    
    
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        // Initialization code
        [self buildUIFromScratch];
        [self setup];
    }
    return self;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    
    [self setup];
    
}

#pragma mark - Action

-(IBAction)selected:(id)sender
{
 
    if (self.didSelectItemBlock != nil)
    {
        self.didSelectItemBlock(self, self.mediaItem);
    }
    else{
        ASLog2Debug(@"For some reasone - non didSelectBlock");////////
    }
}

- (IBAction)deleteFromFavorites:(id)sender {
    if (self.deleteItemBlock != nil)
    {
        self.deleteItemBlock(self, self.mediaItem);
    }
    
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture {
    if ( gesture.state == UIGestureRecognizerStateBegan ) {
        //ASLog@"Long Press");
        if (self.didEditItemBlock != nil)
        {
            self.didEditItemBlock(self, self.mediaItem);
        }
    }
}

-(void) update
{
   // ASLog2Debug(@"[Selectable] mediaItem: %@", self.mediaItem);
    if (self.mediaItem != nil)
    {
        [self loadItemImage];
        [self setTitle:self.mediaItem.name];
        
      //  [self insertImageFrame];
        
        if([self.mediaItem isEpisode] && [[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO])
        {
            NSNumber *episodeNumber = [self.mediaItem getMediaItemEpisodeNumber];
            NSNumber *seasonNumber = [self.mediaItem getMediaItemSeasonNumber];
            [self insertSecondaryTitleLabelWithText:[NSString stringWithFormat:@"%@ %@, %@ %@", NSLocalizedString(@"Season", nil),seasonNumber, NSLocalizedString(@"Episode", nil),episodeNumber]];
            
        }
        else if([self.mediaItem isSeries])
        {
            self.secondaryTitleLabel.text =@"";
        }
        else{
            self.secondaryTitleLabel.text =@"";
        }
        
    }
    else
    {
        [self.imageButton setBackgroundImage:nil forState:UIControlStateNormal];
        [self setTitle:nil];
        self.secondaryTitleLabel.text =@"";
    }
    // ASLog2Debug(@"after: %@", self.titleLabel.text);
}

#pragma mark - UI

-(void) loadItemImage
{
    
    // ASLog2Debug(@"[Selectable][image] itmeName:%@",self.mediaItem.name);////////
    [self.imageButton setAlpha:0];
    [self showDefaultImage];
    NSURL *URL = [self pictureURL];
//    ASLog2Debug(@"[Selectable][Image][URL] image url: %@", URL);////////
    if (URL != nil)
    {
         // ASLog2Debug(@"[Selectable][Image][URL] image url: %@ for %@", URL, self.mediaItem.name);////////
        [self registerForImageNotifications];
        [[AsyncImageLoader sharedAsyncImageLoader] loadImageAtURL:URL];
    }
    else
    {
         // ASLog2Debug(@"[Selectable][Image][URL] image url: %@ for %@", URL, self.mediaItem.name);////////
        [self unregisterFromImageNotifications];
    }
}

-(void) setBadge:(NSString *)badge
{
    CGRect frame = self.badgeButton.frame;
    if (badge.length > 0)
    {
        CGSize constraint = CGSizeMake(self.imageButton.bounds.size.width - (3*BADGE_BUTTON_MARGINS), self.badgeButton.bounds.size.height);
        CGSize fullSize = [badge sizeWithFont:self.badgeButton.titleLabel.font constrainedToSize:constraint];
        frame.size.width = fullSize.width + BADGE_BUTTON_MARGINS;
    }
    else
    {
        frame.size.width = 0;
    }
    self.badgeButton.frame = frame;
    [self.badgeButton setTitle:badge forState:UIControlStateNormal];
}

-(void) setTitle:(NSString *)title
{
    //appTypeUsed
    CGRect frame = self.titleLabel.frame;
    
    if ([TVSessionManager sharedTVSessionManager].appTypeUsed != TVAppType_iPad2 )
    {
        //        CGRect frame = self.titleLabel.frame;
        CGRect availableFrame = self.bounds;
        CGFloat widthLimitedByInsets = availableFrame.size.width - (self.titleEdgeInsets.right + self.titleEdgeInsets.left);
        CGFloat heightLimitedByInsets = availableFrame.size.height - (self.titleEdgeInsets.top + self.titleEdgeInsets.bottom);
        CGSize constraint = CGSizeMake(widthLimitedByInsets, heightLimitedByInsets);
        CGSize actual = [title sizeWithFont:self.titleLabel.font constrainedToSize:constraint];
        frame.size.height = actual.height;
        
        if([_cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight]){
            frame.size.height = [[_cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight] floatValue];
        }
        
        self.titleLabel.frame = frame;
    }
    if ([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO] && [TVSessionManager sharedTVSessionManager].appTypeUsed == TVAppType_iPad2 &&([self.superview class] == [TVDualMediaItemCell class]))
    {
       
        CGRect availableFrame = self.bounds;
        CGFloat widthLimitedByInsets = availableFrame.size.width - (self.titleEdgeInsets.right + self.titleEdgeInsets.left);
        CGFloat heightLimitedByInsets = availableFrame.size.height - (self.titleEdgeInsets.top + self.titleEdgeInsets.bottom);
        CGSize constraint = CGSizeMake(widthLimitedByInsets, heightLimitedByInsets);
        CGSize actual = [title sizeWithFont:self.titleLabel.font constrainedToSize:constraint];
        frame.size.height = actual.height;
        
        if([_cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight]){
            frame.size.height = [[_cellInfoDict objectForKey:kCellInfoDictKey_titleFrame_sizeHeight] floatValue];
        }
        
        
        self.titleLabel.frame = frame;
        
    }
    self.titleLabel.text = title;
}

-(void)insertPackageInfo:(TVSubscription*)subscriptionInfo
{
    //    CGRect titleFrame = self.titleLabel.frame;
    //    CGRect packageInfoFrame = self.lblPackagesInfo.frame;
    //    titleFrame.origin.y= self.lblPackagesInfo.frame.origin.y;
    //    packageInfoFrame.origin.y= self.titleLabel.frame.origin.y;
    //    self.titleLabel.frame = titleFrame;
    //    self.lblPackagesInfo.frame = packageInfoFrame;
    self.lblPackagesInfo.hidden =NO;
    NSString * infoMssage;
    if (subscriptionInfo.isRenewable == YES){
        infoMssage = @"Renews at ";
    }else{
        infoMssage = NSLocalizedString(@"Expires in ", nil);
    }
    ASLog(@"subscriptionItem.endDate = %@",subscriptionItem.endDate);
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    NSString *endDate = [df stringFromDate:subscriptionItem.endDate];
    endDate = [endDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    [df release];
    if(endDate != nil)
    {
        infoMssage = [infoMssage stringByAppendingString:endDate];
    }
    self.lblPackagesInfo.text = infoMssage;
}

-(void)insertSecondaryTitleLabelWithText:(NSString *)text
{
    if(!_secondaryTitleLabel)
    {
        CGRect frm = _titleLabel.frame;
        frm.size.height=20;
        if([_cellInfoDict objectOrNilForKey:kCellInfoDictKey_SeconderyTitleFrame_originY]){
            frm.origin.y = [[_cellInfoDict objectOrNilForKey:kCellInfoDictKey_SeconderyTitleFrame_originY] floatValue];
        }
        self.secondaryTitleLabel = [[[UILabel alloc] initWithFrame:frm]  autorelease];
        [self addSubview:_secondaryTitleLabel];
        
        _secondaryTitleLabel.backgroundColor = [UIColor clearColor];
        _secondaryTitleLabel.font = [UIFont fontWithName:TITLE_FONT_NAME size:TITLE_FONT_SIZE-1];
        _secondaryTitleLabel.textColor = [UIColor lightGrayColor];
        _secondaryTitleLabel.shadowColor = [UIColor blackColor];
        _secondaryTitleLabel.shadowOffset = CGSizeMake(0, -1);
        _secondaryTitleLabel.numberOfLines = 1;
        _secondaryTitleLabel.adjustsFontSizeToFitWidth = NO;
        
        
        
        
    }
    _secondaryTitleLabel.text = text;
}


-(void)insertRentalInfo:(TVRental*)rentalInfo
{
    //    CGRect titleFrame = self.titleLabel.frame;
    //    CGRect packageInfoFrame = self.lblPackagesInfo.frame;
    //    titleFrame.origin.y= self.lblPackagesInfo.frame.origin.y;
    //    packageInfoFrame.origin.y= self.titleLabel.frame.origin.y;
    //    self.titleLabel.frame = titleFrame;
    //    self.lblPackagesInfo.frame = packageInfoFrame;
    self.lblPackagesInfo.hidden =NO;
    NSString * infoMssage = NSLocalizedString(@"Expires in ", nil);
    
    ASLog(@"rentalInfo.endDate = %@",rentalInfo.endDate);
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
    NSString *endDate = [df stringFromDate:rentalInfo.endDate];
    endDate = [endDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    [df release];
    if(endDate != nil)
    {
        infoMssage = [infoMssage stringByAppendingString:endDate];
    }
    self.lblPackagesInfo.text = infoMssage;
    self.lblPackagesInfo.hidden=NO;
}

-(void) showLike{
    self.likeCount.hidden=NO;
    self.likeCount.alpha=1;
    self.likeImage.hidden=NO;
    self.likeImage.alpha=1;
}
#pragma mark - Notifications

-(void) registerForImageNotifications
{
    // NSLog(@"[ImageNotifications][Register]");////////
    
    [self unregisterFromImageNotifications];
    NSURL *imageURL = [self pictureURL];
    AsyncImageLoader *loader = [AsyncImageLoader sharedAsyncImageLoader];
    if ([imageURL host] != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(imageLoaded:)
                                                     name:[AsyncImageLoader imageLoadedNotificationNameForURL:imageURL]
                                                   object:loader];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(imageFailed:)
                                                     name:[AsyncImageLoader imageFailedNotificationNameForURL:imageURL]
                                                   object:loader];
    }
}

-(void) unregisterFromImageNotifications
{
    // ASLog2Debug(@"[ImageNotifications][unregister]");////////
    AsyncImageLoader *loader = [AsyncImageLoader sharedAsyncImageLoader];
    NSURL *imageURL = [self pictureURL];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:[AsyncImageLoader imageLoadedNotificationNameForURL:imageURL]
                                                  object:loader];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:[AsyncImageLoader imageFailedNotificationNameForURL:imageURL]
                                                  object:loader];
}

-(void) imageFailed : (NSNotification *) notification
{
   // ASLogDebug(@"Failed to load Image at URL: %@",notification.name);
    
   

    NSString *defaultImageName = @"default_program_image";
    
    // added by shefyg - for 2x3
    if(self.imageButton && self.imageButton.frame.size.width < self.imageButton.frame.size.height){
        defaultImageName = @"default_program_image_2x3";
    }
    
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO] && [[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_AppForIphone] ){
        defaultImageName = @"default_item_image";
    }
    
    UIImage *defaultImage = [UIImage imageNamed:defaultImageName];
    [self.imageButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
    
    [self unregisterFromImageNotifications];
}

-(void) imageLoaded : (NSNotification *) notification
{
   // ASLog2Debug(@"[Selectable][imageLoaded] ");////////
    
    [self unregisterFromImageNotifications];
    UIImage *image = [notification.userInfo objectForKey:AsyncImageLoaderImageKey];
    [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
    
    if(self.cellInfoDict !=nil &&
       [self.cellInfoDict class] == [NSDictionary class] &&
       [[self.cellInfoDict allKeys] containsObject:kCellInfoDictKey_addFrameToSelectedCell] &&
       [self.cellInfoDict objectForKey:kCellInfoDictKey_addFrameToSelectedCell])
    {
        [self.imageButton setContentMode:UIViewContentModeScaleToFill];
        [self insertImageFrame];
    }
    [self revealButtonAnimated];
    
}


-(void)insertImageFrame
{
    if([self.cellInfoDict objectForKey:kCellInfoDictKey_addFrameToSelectedCell])
    {
        self.imageFrame = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:[self.cellInfoDict objectForKey:kCellInfoDictKey_addFrameToSelectedCell]]]autorelease];
        _imageFrame.frame = self.imageButton.frame;
        self.imageButton.imageView.image = [self.imageButton.imageView.image resizableImageWith1x1StretchableArea];
//        CGRect frame = self.imageButton.frame;
//        frame.origin.x +=3;
//        frame.origin.y +=2;
//        frame.size.width -=6;
//        frame.size.height -=12;
        CGRect frame = _imageFrame.frame;
        frame.origin.x -=3;
        frame.origin.y -=2;
        frame.size.width +=6;
        frame.size.height +=12;
        _imageFrame.frame =frame;
        [_imageFrame setContentMode:UIViewContentModeScaleToFill];
        
        [self bringSubviewToFront:self.imageButton];
        [self addSubview:_imageFrame];
        [self bringSubviewToFront:_imageFrame];
        
    }
}

#pragma mark - Item Image

-(void) revealButtonAnimated
{
    // ASLog2Debug(@"[revealButtonAnimated][] ");
  
    self.imageButton.alpha = 0;
    [UIView animateWithDuration:0.2 delay:0 options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut) animations:^{
        self.imageButton.alpha = 1;
    }
                     completion:^(BOOL finished) {
                         if(_imageFrame){
                             [self bringSubviewToFront:_imageFrame];
                         }
                     }];
}

-(void) showDefaultImage
{
    NSString *imgName = DEFAULT_ITEM_IMAGE;
    if(self.pictureSize.size.width == 320 && self.pictureSize.size.height == 180)
    {
        imgName =DEFAULT_ITEM_IMAGE_16X9 ;
    }
    
    UIImage *image = [UIImage imageNamed:imgName];
    [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.imageButton setContentMode:UIViewContentModeScaleAspectFit];
    [self revealButtonAnimated];
}



-(void) showLoadImage
{
    NSString *imgName = DEFAULT_ITEM_IMAGE; //DEFAULT_LOAD_ITEM_IMAGE;
    if(self.pictureSize.size.width == 320 && self.pictureSize.size.height == 180)
    {
        imgName = DEFAULT_ITEM_IMAGE_16X9; //  DEFAULT_LOAD_ITEM_IMAGE_16X9;
    }
    
    UIImage *image = [UIImage imageNamed:imgName];
    [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.imageButton setContentMode:UIViewContentModeScaleAspectFit];
    
//    // making sure the frame is on top
//    if(_imageFrame){
//        [self bringSubviewToFront:_imageFrame];
//    }
    
}


#pragma mark - Delete Button

-(void) delete:(id)sender
{
    if (self.deleteItemBlock != nil)
    {
        self.deleteItemBlock(self, self.mediaItem);
    }
}

-(void) setDeleteButtonHidden : (BOOL) hidden animated : (BOOL) animated
{
    //Set hidden to YES explicitly if there is no media item
    hidden = (self.mediaItem == nil) ? YES : hidden;
    
    NSTimeInterval duration = (animated) ? 0.1 : 0;
    
    CGRect frame = self.deleteButton.frame;
    frame.origin.x = (-1 * frame.size.width);
    if (hidden == NO)
    {
        self.deleteButton.alpha = 0;
        [self.imageButton addSubview:self.deleteButton];
        frame.origin.x = BADGE_BUTTON_MARGINS;
    }
    
    [UIView animateWithDuration:duration animations:^{
        self.deleteButton.frame = frame;
        self.deleteButton.alpha = hidden ? 0 : 1;
    } completion:^(BOOL finished) {
        if (hidden)
        {
            [self.deleteButton removeFromSuperview];
        }
    }];
}
- (IBAction)selectedMyZoonItem:(id)sender {
    if (self.didSelectItemWithSubscriptionItemBlock != nil)
    {
        self.didSelectItemWithSubscriptionItemBlock(self, self.mediaItem,self.subscriptionItem);
    }
    
}
@end
