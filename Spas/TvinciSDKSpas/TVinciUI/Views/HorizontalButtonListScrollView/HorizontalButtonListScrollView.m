//
//  RSScrollView.m
//  Halacha
//
//  Created by Yoni Colb on 5/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HorizontalButtonListScrollView.h"
#import <QuartzCore/QuartzCore.h>

@interface  HorizontalButtonListScrollView ()
-(void) setup;
@end

@implementation HorizontalButtonListScrollView
@synthesize buttons = _buttons;
@synthesize margins = _margins;
@synthesize numberOfVisibleButtons = _numberOfVisibleButtons;

-(void) dealloc
{
    [_buttons release];
    [super dealloc];
}

-(void) layoutButtons
{
    for (UIView *subView in self.subviews)
    {
        [subView removeFromSuperview];
    }
    
    if (self.buttons.count > 0)
    {
        CGFloat maxVisibleButtonsNonAtomic = self.numberOfVisibleButtons;
        if (self.numberOfVisibleButtons % 2 != 0)
        {
            maxVisibleButtonsNonAtomic = self.numberOfVisibleButtons - 0.5;
        }
        CGFloat actualyVisibleButtons = MIN(self.buttons.count,maxVisibleButtonsNonAtomic);
        
        CGFloat availableWidth = (self.bounds.size.width - ((self.buttons.count - 1) * self.margins));
        CGFloat widthOfButton = (availableWidth / actualyVisibleButtons);
        CGFloat heightOfButton = self.bounds.size.height;
        CGFloat yOriginOfButton = (self.bounds.size.height - heightOfButton) / 2;
        
        for (NSInteger i = 0; i < self.buttons.count ; i++)
        {
            UIButton *button = [self.buttons objectAtIndex:i];
            CGFloat xOriginOfButton = (i * (widthOfButton + self.margins));
            CGRect frame = CGRectMake(xOriginOfButton, yOriginOfButton, widthOfButton, heightOfButton);
            button.frame = frame;
            [self addSubview:button];
            [button addTarget:self action:@selector(centerButton:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        CGFloat totalWidth = (widthOfButton * self.buttons.count + (self.margins * (self.buttons.count - 1)));
        CGSize contentSize = self.contentSize;
        contentSize.width = totalWidth;
        self.contentSize = contentSize;
    }
}

-(IBAction)centerButton:(id)sender
{
    UIView *view = sender;
    CGPoint contentoffset =  self.contentOffset;
    contentoffset.x = view.frame.origin.x + view.frame.size.width / 2 - self.bounds.size.width/2;
    contentoffset.x = MIN(contentoffset.x , self.contentSize.width - self.bounds.size.width);
    contentoffset.x = MAX(0, contentoffset.x);
    
    [self setContentOffset:contentoffset animated:YES];
}

-(void) setButtons:(NSArray *)buttons 
{    
    if( _buttons != buttons )
    {
        [_buttons release];
        _buttons = [buttons retain];
        [self layoutButtons];
    }
}

-(void) setup
{
    [super setup];
    self.numberOfVisibleButtons = 3;
    self.margins = 0.0;
    self.shadowRadius = 30;
    self.showsScrollShadows = YES;
    self.showsOverScrollGlow = YES;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}
@end