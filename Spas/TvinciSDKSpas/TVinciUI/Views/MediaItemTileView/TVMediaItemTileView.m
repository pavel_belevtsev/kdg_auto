//
//  TVMoviesSelectItemView.m
//  Tvinci2.0iPad
//
//  Created by Quickode Ltd. on 7/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVMediaItemTileView.h"
#import "TVMediaItem.h"
#import "AsyncImageView.h"
#import "TVPictureSize.h"
#import "TVConfigurationManager.h"
#import "ASIHTTPRequest.h"
#import "AsyncImageLoader.h"
#import "SmartLogs.h"
#import "TVSessionManager.h"
NSString *const ShowNibExtension = @"_Show";
NSString *const MovieNibExtension =  @"_Show";//@"_Movie";  // changed because now both movie and show are 16X9

@interface TVMediaItemTileView () <ASImageViewDelegate>
@end

@implementation TVMediaItemTileView
@synthesize lblLikesCount;

#pragma mark - Memory
- (void)dealloc 
{
    [lblLikesCount release];
    [super dealloc];
}

#pragma mark - Utility Factory Method
+(TVMediaItemTileView *) mediaItemTileViewForMediaItem : (TVMediaItem *) item
{
    NSString *nibName = NSStringFromClass(self);
    NSNumber *showTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeSeries];
    if ([item.mediaTypeID integerValue] == [showTypeID integerValue])
    {
        nibName = [nibName stringByAppendingString:ShowNibExtension];
    }
    else 
    {
        nibName = [nibName stringByAppendingString:MovieNibExtension];
    }
    
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for(id object in  contents)
    {
        if([object isKindOfClass:self])
        {
            return object;
        }
    }
    return nil;
}

#pragma mark - UI
-(void) update
{
    //ASLog(@"%@", [self class]);
    [super update];
    self.lblLikesCount.text = [NSString stringWithFormat:@"%d",self.mediaItem.likeCounter];
    if ([self.mediaItem isMovie])
    {
        self.titleLabel.text=self.mediaItem.name;
    }
}

-(void) setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

#pragma mark - Utility
-(void) setup
{
    [super setup];
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_KANGUROO]){
        
    }
    else{
        self.pictureSize = [TVPictureSize iPhoneSpotlightItemPictureSize];
    }
    
}
@end
