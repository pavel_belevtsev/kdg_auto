//
//  TVMoviesSelectItemView.h
//  Tvinci2.0iPad
//
//  Created by Quickode Ltd. on 7/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TVinciUtils.h"
#import "TVSelectableMediaItemView.h"

@interface TVMediaItemTileView : TVSelectableMediaItemView
@property (retain, nonatomic) IBOutlet UILabel *lblLikesCount;
+(TVMediaItemTileView *) mediaItemTileViewForMediaItem : (TVMediaItem *) item;
@end
