//
//  TVBillingCell.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/9/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVBillingCell.h"
#import "UIImage+Resize.h"
#import "TVTransaction.h"
#import "TVLinearLayout.h"
#import "NSDate+TimeZoneShift.h"
#import "SmartLogs.h"

@implementation TVBillingCell
@synthesize btnBadge;
@synthesize lblTitle;
@synthesize lblAmount;
@synthesize lblSubtitle;
@synthesize transaction = _transaction;
@synthesize metaLayout;

#pragma mark - Memory
- (void)dealloc 
{
    [_transaction release];
    self.btnBadge = nil;
    self.lblAmount = nil;
    self.lblSubtitle = nil;
    self.lblTitle = nil;
    [metaLayout release];
    [_imgCellBg release];
    [super dealloc];
}

-(void) setTransaction:(TVTransaction *)transaction
{
    if (_transaction != transaction)
    {
        [_transaction release];
        _transaction = [transaction retain];
        if (_transaction != nil)
        {
            [self updateOutlets];
        }
    }
}

#pragma mark - Setup
-(void) updateOutlets
{
    if (self.transaction != nil)
    {
        
        NSString *newtitle = NSLocalizedString([kTransactionItemTypesArray objectAtIndex:self.transaction.itemType], nil);
        [self.btnBadge setTitle:newtitle forState:UIControlStateNormal];
        
        // adjusting size
        CGSize requiredSize = [[self.btnBadge titleForState:UIControlStateNormal] sizeWithFont:self.btnBadge.titleLabel.font];
        CGRect frm = self.btnBadge.frame;
        frm.size.width = requiredSize.width+10;
        self.btnBadge.frame = frm;

        
        
       // self.btnBadge.titleLabel.text= [kTransactionItemTypesArray objectAtIndex:self.transaction.itemType];
        
        self.lblTitle.text = self.transaction.purchasedItemName;
        self.lblAmount.text = [NSString stringWithFormat:@"%.2f%@",self.transaction.price , self.transaction.currencySign];
        
//        // Updating layout
//        
//        CGSize lblTitleSize = [self.lblTitle sizeWithFont:self.lblTitle.font];
//        
//        float btnBadgeX = self.lblTitle.frame.origin.x + lblTitleSize.width + 5;
//        
//        self.btnBadge.frame = CGRectMake(0, 0, textSize.width + backButton.titleEdgeInsets.left + 5, bg.size.height);
        
        
    }
}

-(void) updateTransactionTimeWithFixedDate:(NSDate *)date{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"dd MMM yyyy, hh:mm a";
    NSLocale *usLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
    [formatter setLocale:usLocale];

    
    NSString *dateFormatted = [formatter stringFromDate:date];//[formatter stringFromDate:self.transaction.actionDate];
    
    ASLog2Debug(@"Date formatted: %@",dateFormatted);////////
    self.lblSubtitle.text = [dateFormatted uppercaseString];
    [formatter release];
}


-(void) setup
{
    UIImage *resizable = [[self.btnBadge backgroundImageForState:UIControlStateNormal] resizableImageWith1x1StretchableArea];
    [self.btnBadge setBackgroundImage:resizable forState:UIControlStateNormal];
    self.metaLayout.margins = 5.0;
}

-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    return [[TVBillingCell cellWithTransaction:nil] retain];
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

+(TVBillingCell *) cellWithTransaction:(TVTransaction *)transaction
{
    TVBillingCell *cell = nil;
    NSString *nibName = NSStringFromClass([self class]);
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            cell = object;
            cell.transaction = transaction;
            break;
        }
    }
    return cell;
}
@end
