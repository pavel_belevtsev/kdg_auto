//
//  ShadowedScrollView.m
//  YouAppi
//
//  Created by Avraham Shukron on 2/21/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "ShadowedScrollView.h"
#import "ASGradientView.h"
#import "TVTheme.h"
@interface ShadowedScrollView ()
- (void) setup;
- (void)positionBottomShadow;
- (void)positionTopShadow;
-(void) positionRightShadow;
-(void) positionLeftShadow;

-(void) positionTopOverscrollGlowView;
-(void) positionBottomOverscrollGlowView;
-(void) positionLeftOverscrollGlowView;
-(void) positionRightOverscrollGlowView;
-(void) createDefaultViews;
@end

@implementation ShadowedScrollView
@synthesize topShadowView  = _topShadowView;
@synthesize bottomShadowView = _bottomShadowView;
@synthesize leftShadowView = _leftShadowView;
@synthesize rightShadowView = _rightShadowView;

@synthesize bottomOverscrollGlowView = _bottomOverscrollGlowView;
@synthesize topOverscrollGlowView = _topOverscrollGlowView;
@synthesize leftOverscrollGlowView = _leftOverscrollGlowView;
@synthesize rightOverscrollGlowView = _rightOverscrollGlowView;
@synthesize shadowRadius;
@synthesize showsScrollShadows = _showsScrollShadows;
@synthesize showsOverScrollGlow = _showsOverScrollGlow;

-(void) dealloc
{
    self.topOverscrollGlowView = nil;
    self.bottomOverscrollGlowView = nil;
    self.leftOverscrollGlowView = nil;
    self.rightOverscrollGlowView = nil;
    
    self.leftShadowView = nil;
    self.rightShadowView= nil;
    self.topShadowView = nil;
    self.bottomShadowView = nil;
    [super dealloc];
}

#pragma mark - Setters
-(void) setTopShadowView:(UIView *)topShadowView
{
    if (topShadowView != _topShadowView)
    {
        [_topShadowView removeFromSuperview];
        [_topShadowView release];
        _topShadowView = [topShadowView retain];
    }
}

-(void) setBottomShadowView:(UIView *)bottomShadowView
{
    if (_bottomShadowView != bottomShadowView)
    {
        [_bottomShadowView removeFromSuperview];
        [_bottomShadowView release];
        _bottomShadowView = [bottomShadowView retain];
    }
}

-(void) setTopOverscrollGlowView:(UIView *)topOverscrollGlowView
{
    if (_topOverscrollGlowView != topOverscrollGlowView)
    {
        [_topOverscrollGlowView removeFromSuperview];
        [_topOverscrollGlowView release];
        _topOverscrollGlowView = [topOverscrollGlowView retain];
    }
}

-(void) setBottomOverscrollGlowView:(UIView *)bottomOverscrollGlowView
{
    if (_bottomOverscrollGlowView != bottomOverscrollGlowView)
    {
        [_bottomOverscrollGlowView removeFromSuperview];
        [_bottomOverscrollGlowView release];
        _bottomOverscrollGlowView = [bottomOverscrollGlowView retain];
    }
}

-(void) setRightShadowView:(UIView *)rightShadowView
{
    if (_rightShadowView != rightShadowView)
    {
        [_rightShadowView removeFromSuperview];
        [_rightShadowView release];
        _rightShadowView = [rightShadowView retain];
    }
}

-(void) setLeftShadowView:(UIView *)leftShadowView
{
    if (_leftShadowView != leftShadowView)
    {
        [_leftShadowView removeFromSuperview];
        [_leftShadowView release];
        _leftShadowView = [leftShadowView retain];
    }
}

-(void) setRightOverscrollGlowView:(UIView *)rightOverscrollGlowView
{
    if (_rightOverscrollGlowView != rightOverscrollGlowView)
    {
        [_rightOverscrollGlowView removeFromSuperview];
        [_rightOverscrollGlowView release];
        _rightOverscrollGlowView = [rightOverscrollGlowView retain];
    }
}

-(void) setLeftOverscrollGlowView:(UIView *)leftOverscrollGlowView
{
    if (_leftOverscrollGlowView != leftOverscrollGlowView)
    {
        [_leftOverscrollGlowView removeFromSuperview];
        [_leftOverscrollGlowView release];
        _leftOverscrollGlowView = [leftOverscrollGlowView retain];
    }
}

-(void) setShowsScrollShadows:(BOOL)showsScrollShadows
{
    if (_showsScrollShadows != showsScrollShadows)
    {
        _showsScrollShadows = showsScrollShadows;
        BOOL hidden = (_showsScrollShadows == NO);
        self.topShadowView.hidden = hidden;
        self.bottomShadowView.hidden = hidden;
        self.rightShadowView.hidden = hidden;
        self.leftShadowView.hidden = hidden;
    }
}

-(void) setShowsOverScrollGlow:(BOOL)showsOverScrollGlow
{
    if (_showsOverScrollGlow != showsOverScrollGlow)
    {
        _showsOverScrollGlow = showsOverScrollGlow;
        BOOL hidden = (_showsOverScrollGlow == NO);
        self.topOverscrollGlowView.hidden = hidden;
        self.bottomOverscrollGlowView.hidden = hidden;
        self.rightOverscrollGlowView.hidden = hidden;
        self.leftOverscrollGlowView.hidden = hidden;
    }
}
#pragma mark - Setup
-(void) setup
{
    [self createDefaultViews];
    self.shadowRadius = 10;
    self.showsOverScrollGlow = YES;
    self.showsScrollShadows = NO;
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(void) createDefaultViews
{
    // Overscroll Views
    ASGradientView *topOverscrollGlow = [[ASGradientView alloc] init];
    ASGradientView *bottomOverscrollGlow = [[ASGradientView alloc] init];
    ASGradientView *leftOverscrollGlow = [[ASGradientView alloc] init];
    ASGradientView *rightOverscrollGlow = [[ASGradientView alloc] init];
    // Colors
    CGColorRef startColor = [[UIColor clearColor] CGColor];
    CGColorRef endColor = [[[TVTheme mainTheme] colorForKey:PrimaryAccentColorKey] CGColor];
    CGColorRef lightGray = [[UIColor colorWithWhite:0 alpha:0.5] CGColor];
    NSArray *overscrollColors = [NSArray arrayWithObjects:(id)endColor, (id)startColor, nil];
    NSArray *shadowColors = [NSArray arrayWithObjects:(id) lightGray , (id)startColor, nil];
    
    CAGradientLayer *layer = topOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(0.5, 0);
    layer.endPoint = CGPointMake(0.5, 1);
    
    layer = bottomOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(0.5, 1);
    layer.endPoint = CGPointMake(0.5, 0);
    
    layer = leftOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(0, 0.5);
    layer.endPoint = CGPointMake(1, 0.5);
    
    layer = rightOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(1, 0.5);
    layer.endPoint = CGPointMake(0, 0.5);
    
    self.topOverscrollGlowView = topOverscrollGlow;
    self.bottomOverscrollGlowView = bottomOverscrollGlow;
    self.rightOverscrollGlowView = rightOverscrollGlow;
    self.leftOverscrollGlowView = leftOverscrollGlow;
    
    [leftOverscrollGlow release];
    [rightOverscrollGlow release];
    [topOverscrollGlow release];
    [bottomOverscrollGlow release];
    
    // Shadow views
    
    ASGradientView *topShadow = [[ASGradientView alloc] init];
    ASGradientView *bottomShadow = [[ASGradientView alloc] init];
    ASGradientView *leftShadow = [[ASGradientView alloc] init];
    ASGradientView *rightShadow = [[ASGradientView alloc] init];
    
    layer = topShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(0.5, 0);
    layer.endPoint = CGPointMake(0.5, 1);
    
    layer = bottomShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(0.5, 1);
    layer.endPoint = CGPointMake(0.5, 0);
    
    layer = leftShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(0, 0.5);
    layer.endPoint = CGPointMake(1, 0.5);
    
    layer = rightShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(1, 0.5);
    layer.endPoint = CGPointMake(0, 0.5);
    
    self.topShadowView = topShadow;
    self.bottomShadowView = bottomShadow;
    self.leftShadowView = leftShadow;
    self.rightShadowView = rightShadow;
    [rightShadow release];
    [leftShadow release];
    [topShadow release];
    [bottomShadow release];
}

#pragma mark - Positioning the views
- (void)positionBottomShadow
{
    if (self.showsScrollShadows)
    {
        CGRect frame = self.bottomShadowView.frame;
        CGFloat moreToScroll = (self.contentSize.height - (self.contentOffset.y + self.bounds.size.height));
        moreToScroll = MAX(0, moreToScroll);
        moreToScroll = MIN(self.shadowRadius, moreToScroll);
        frame.size.height = moreToScroll;
        frame.size.width = self.frame.size.width;
        CGFloat yOrigin = (self.contentOffset.y + self.bounds.size.height - frame.size.height);
        frame.origin = CGPointMake(self.contentOffset.x, yOrigin);
        self.bottomShadowView.frame = frame;
        if (self.bottomShadowView.superview == nil)
        {
            [self addSubview:self.bottomShadowView];
        }
        
        [self bringSubviewToFront:self.bottomShadowView];
    }
    else
    {
        [self.topShadowView removeFromSuperview];
    }
}

-(void) positionTopShadow
{
    if (self.showsScrollShadows)
    {
        CGFloat yOffset = self.contentOffset.y;
        yOffset = MIN(yOffset, self.shadowRadius);
        yOffset = MAX(0, yOffset);
        
        CGRect frame = self.topShadowView.frame;
        frame.origin = self.contentOffset;
        frame.size.height = yOffset;
        frame.size.width = self.frame.size.width;
        self.topShadowView.frame = frame;
        
        if (self.topShadowView.superview == nil)
        {
            [self addSubview:self.topShadowView];
        }
        [self bringSubviewToFront:self.topShadowView];
    }
    else
    {
        [self.bottomShadowView removeFromSuperview];
    }
}

-(void) positionLeftShadow
{
    if (self.showsScrollShadows)
    {
        CGFloat xOffset = self.contentOffset.x;
        xOffset = MIN(xOffset, self.shadowRadius);
        xOffset = MAX(0, xOffset);
        
        CGRect frame = self.leftShadowView.frame;
        frame.origin = self.contentOffset;
        frame.size.height = self.frame.size.height;
        frame.size.width = xOffset;
        self.leftShadowView.frame = frame;
        
        if (self.leftShadowView.superview == nil)
        {
            [self addSubview:self.leftShadowView];
        }
        [self bringSubviewToFront:self.leftShadowView];
    }
    else
    {
        [self.leftShadowView removeFromSuperview];
    }
}

-(void) positionRightShadow
{
    if (self.showsScrollShadows)
    {
        CGRect frame = self.rightShadowView.frame;
        CGFloat moreToScroll = (self.contentSize.width - (self.contentOffset.x + self.bounds.size.width));
        moreToScroll = MAX(0, moreToScroll);
        moreToScroll = MIN(self.shadowRadius, moreToScroll);
        frame.size.height = self.bounds.size.height;
        frame.size.width = moreToScroll;
        CGFloat xOrigin = (self.contentOffset.x + self.bounds.size.width - moreToScroll);
        frame.origin = CGPointMake(xOrigin, self.contentOffset.y);
        self.rightShadowView.frame = frame;
        if (self.rightShadowView.superview == nil)
        {
            [self addSubview:self.rightShadowView];
        }
        
        [self bringSubviewToFront:self.rightShadowView];
    }
    else
    {
        [self.rightShadowView removeFromSuperview];
    }
}

-(void) positionBottomOverscrollGlowView
{
    CGFloat overscrollBottom = (self.contentOffset.y + self.bounds.size.height) - self.contentSize.height;
    if (overscrollBottom >= 0 && self.contentSize.height > 0)
    {
        CGRect frame = self.bottomOverscrollGlowView.frame;
        frame.origin = CGPointMake(0, self.contentSize.height);
        frame.size.height = overscrollBottom;
        frame.size.width = self.bounds.size.width;
        self.bottomOverscrollGlowView.frame = frame;
        
        if (self.bottomOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.bottomOverscrollGlowView];
        }
    }
    else
    {
        [self.bottomOverscrollGlowView removeFromSuperview];
    }
}

-(void) positionTopOverscrollGlowView
{
    CGFloat overscrollTop = (self.contentOffset.y * -1);
    if (overscrollTop >= 0)
    {
        CGRect frame = self.topOverscrollGlowView.frame;
        frame.size.height = overscrollTop;
        frame.size.width = self.bounds.size.width;
        frame.origin = CGPointMake(self.contentOffset.x , -overscrollTop);
        self.topOverscrollGlowView.frame = frame;
        
        if (self.topOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.topOverscrollGlowView];
        }
    }
    else
    {
        [self.topOverscrollGlowView removeFromSuperview];
    }
}

-(void) positionLeftOverscrollGlowView
{
    CGFloat leftOverscroll = (self.contentOffset.x * -1);
    if (leftOverscroll >= 0)
    {
        CGRect frame = self.leftOverscrollGlowView.frame;
        frame.size.width = leftOverscroll;
        frame.size.height = self.bounds.size.height;
        frame.origin = CGPointMake(-leftOverscroll, self.contentOffset.y);
        self.leftOverscrollGlowView.frame = frame;
        
        if (self.leftOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.leftOverscrollGlowView];
        }
    }
    else
    {
        [self.leftOverscrollGlowView removeFromSuperview];
    }
}

-(void) positionRightOverscrollGlowView
{
    CGFloat overscrollRight = (self.contentOffset.x + self.bounds.size.width) - self.contentSize.width;
    if (overscrollRight >= 0 && self.contentSize.width > 0)
    {
        CGRect frame = self.rightOverscrollGlowView.frame;
        frame.origin = CGPointMake(self.contentSize.width, self.contentOffset.y);
        frame.size.width = overscrollRight;
        frame.size.height = self.frame.size.height;
        self.rightOverscrollGlowView.frame = frame;
        
        if (self.rightOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.rightOverscrollGlowView];
        }
    }
    else
    {
        [self.rightOverscrollGlowView removeFromSuperview];
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    if (self.showsScrollShadows)
    {
        [self positionTopShadow];
        [self positionBottomShadow];
        [self positionLeftShadow];
        [self positionRightShadow];
    }
    
    [self positionBottomOverscrollGlowView];
    [self positionTopOverscrollGlowView];
    [self positionLeftOverscrollGlowView];
    [self positionRightOverscrollGlowView];
}
@end