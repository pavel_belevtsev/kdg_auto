//
//  TVGenreCell.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVGenreCell.h"

@implementation TVGenreCell
@synthesize lblTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc 
{
    [lblTitle release];
    [super dealloc];
}

+(TVGenreCell *) genreCell
{
    NSString *nibName = NSStringFromClass(self);
    return [[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] lastObject];
}
@end
