//
//  AsyncImageButton.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/19/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsyncImageButton : UIButton
- (void)loadImageAtURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage;
@end
