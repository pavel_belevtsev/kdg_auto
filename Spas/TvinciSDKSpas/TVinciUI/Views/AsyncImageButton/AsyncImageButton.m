//
//  AsyncImageButton.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/19/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "AsyncImageButton.h"
#import "AsyncImageView.h"

@interface AsyncImageButton () <ASImageViewDelegate>
@end

@implementation AsyncImageButton
@synthesize imageView = _asyncImageView;
-(UIImageView *) imageView
{
    if (_asyncImageView == nil)
    {
        _asyncImageView = [[AsyncImageView alloc] initWithFrame:self.bounds];
        _asyncImageView.contentMode = UIViewContentModeScaleAspectFill;
        _asyncImageView.clipsToBounds = YES;
        ((AsyncImageView*)_asyncImageView).delegate = self;
        [self addSubview:_asyncImageView];
    }
    return _asyncImageView;
}

-(void) loadImageAtURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage
{
    [(AsyncImageView *)self.imageView loadImageAtURL:url placeholderImage:placeholderImage];
}

-(void) asyncImageView:(AsyncImageView *)sender didFinishLoadingImage:(UIImage *)image
{
//    self.imageView.image = nil;
//    [self setImage:image forState:UIControlStateNormal];
//    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.imageView.frame = self.bounds;
}

@end
