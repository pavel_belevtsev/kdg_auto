//
//  ShadowedTableView.m
//  YouAppi
//
//  Created by Avraham Shukron on 2/21/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "ShadowedTableView.h"
#import "ASGradientView.h"
#import "TVTheme.h"

@interface ShadowedTableView ()
-(void) setup;
- (void)positionBottomShadow;
- (void)positionTopShadow;

-(void) positionTopOverscrollGlowView;
-(void) positionBottomOverscrollGlowView;
-(void) createDefaultViews;
@end

@implementation ShadowedTableView
@synthesize topShadowView  = _topShadowView;
@synthesize bottomShadowView = _bottomShadowView;
@synthesize bottomOverscrollGlowView = _bottomOverscrollGlowView;
@synthesize topOverscrollGlowView = _topOverscrollGlowView;
@synthesize shadowRadius;

-(void) dealloc
{
    self.topOverscrollGlowView = nil;
    self.bottomOverscrollGlowView = nil;
    self.topShadowView = nil;
    self.bottomShadowView = nil;
    [super dealloc];
}

#pragma mark - Setters
-(void) setTopShadowView:(UIView *)topShadowView
{
    if (topShadowView != _topShadowView)
    {
        [_topShadowView removeFromSuperview];
        [_topShadowView release];
        _topShadowView = [topShadowView retain];
    }
}

-(void) setBottomShadowView:(UIView *)bottomShadowView
{
    if (_bottomShadowView != bottomShadowView)
    {
        [_bottomShadowView removeFromSuperview];
        [_bottomShadowView release];
        _bottomShadowView = [bottomShadowView retain];
    }
}

-(void) setTopOverscrollGlowView:(UIView *)topOverscrollGlowView
{
    if (_topOverscrollGlowView != topOverscrollGlowView)
    {
        [_topOverscrollGlowView removeFromSuperview];
        [_topOverscrollGlowView release];
        _topOverscrollGlowView = [topOverscrollGlowView retain];
    }
}

-(void) setBottomOverscrollGlowView:(UIView *)bottomOverscrollGlowView
{
    if (_bottomOverscrollGlowView != bottomOverscrollGlowView)
    {
        [_bottomOverscrollGlowView removeFromSuperview];
        [_bottomOverscrollGlowView release];
        _bottomOverscrollGlowView = [bottomOverscrollGlowView retain];
    }
}

#pragma mark - Setup

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(void) setup
{
    [self createDefaultViews];
    self.shadowRadius = 10;
}

-(void) createDefaultViews
{
    // Overscroll Views
    ASGradientView *topOverscrollGlow = [[ASGradientView alloc] init];
    ASGradientView *bottomOverscrollGlow = [[ASGradientView alloc] init];
    // Colors
    CGColorRef startColor = [[UIColor clearColor] CGColor];
    CGColorRef endColor = [[[TVTheme mainTheme] colorForKey:PrimaryAccentColorKey] CGColor];
    CGColorRef lightGray = [[UIColor colorWithWhite:0 alpha:0.5] CGColor];
    NSArray *overscrollColors = [NSArray arrayWithObjects:(id)endColor, (id)startColor, nil];
    NSArray *shadowColors = [NSArray arrayWithObjects:(id) lightGray , (id)startColor, nil];
    
    CAGradientLayer *layer = topOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(0.5, 0);
    layer.endPoint = CGPointMake(0.5, 1);
    
    layer = bottomOverscrollGlow.gradientLayer;
    layer.colors = overscrollColors;
    layer.startPoint = CGPointMake(0.5, 1);
    layer.endPoint = CGPointMake(0.5, 0);
    
    self.topOverscrollGlowView = topOverscrollGlow;
    self.bottomOverscrollGlowView = bottomOverscrollGlow;
    [topOverscrollGlow release];
    [bottomOverscrollGlow release];
    
    // Shadow views
    
    ASGradientView *topShadow = [[ASGradientView alloc] init];
    ASGradientView *bottomShadow = [[ASGradientView alloc] init];
    
    layer = topShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(0.5, 0);
    layer.endPoint = CGPointMake(0.5, 1);
    
    layer = bottomShadow.gradientLayer;
    layer.colors = shadowColors;
    layer.startPoint = CGPointMake(0.5, 1);
    layer.endPoint = CGPointMake(0.5, 0);
    
    self.topShadowView = topShadow;
    self.bottomShadowView = bottomShadow;
    [topShadow release];
    [bottomShadow release];
}

#pragma mark - Positioning the views
- (void)positionBottomShadow
{
    CGRect frame = self.bottomShadowView.frame;
    CGFloat moreToScroll = (self.contentSize.height - (self.contentOffset.y + self.bounds.size.height));
    moreToScroll = MAX(0, moreToScroll);
    moreToScroll = MIN(self.shadowRadius, moreToScroll);
    frame.size.height = moreToScroll;
    frame.size.width = self.frame.size.width;
    CGFloat yOrigin = (self.contentOffset.y + self.bounds.size.height - frame.size.height);
    frame.origin = CGPointMake(self.contentOffset.x, yOrigin);
    self.bottomShadowView.frame = frame;
    if (self.bottomShadowView.superview == nil)
    {
        [self addSubview:self.bottomShadowView];
    }
    
    [self bringSubviewToFront:self.bottomShadowView];
}

-(void) positionTopShadow
{
    CGFloat yOffset = self.contentOffset.y;
    yOffset = MIN(yOffset, self.shadowRadius);
    yOffset = MAX(0, yOffset);
    
    CGRect frame = self.topShadowView.frame;
    frame.origin = self.contentOffset;
    frame.size.height = yOffset;
    frame.size.width = self.frame.size.width;
    self.topShadowView.frame = frame;
    
    if (self.topShadowView.superview == nil)
    {
        [self addSubview:self.topShadowView];
    }
    [self bringSubviewToFront:self.topShadowView];
}

-(void) positionBottomOverscrollGlowView
{
    CGFloat overscrollBottom = (self.contentOffset.y + self.bounds.size.height) - self.contentSize.height;
    if (overscrollBottom >= 0)
    {
        CGRect frame = self.bottomOverscrollGlowView.frame;
        frame.origin = CGPointMake(0, self.contentSize.height);
        frame.size.height = overscrollBottom;
        frame.size.width = self.bounds.size.width;
        self.bottomOverscrollGlowView.frame = frame;
        
        if (self.bottomOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.bottomOverscrollGlowView];
        }
    }
    else
    {
        [self.bottomOverscrollGlowView removeFromSuperview];
    }
}

-(void) positionTopOverscrollGlowView
{
    CGFloat overscrollTop = (self.contentOffset.y * -1);
    if (overscrollTop >= 0)
    {
        CGRect frame = self.topOverscrollGlowView.frame;
        frame.size.height = overscrollTop;
        frame.size.width = self.bounds.size.width;
        frame.origin = CGPointMake(self.contentOffset.x , -overscrollTop);
        self.topOverscrollGlowView.frame = frame;
        
        if (self.topOverscrollGlowView.superview == nil)
        {
            [self addSubview:self.topOverscrollGlowView];
        }
    }
    else
    {
        [self.topOverscrollGlowView removeFromSuperview];
    }
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    [self positionTopShadow];
    [self positionBottomShadow];
    [self positionBottomOverscrollGlowView];
    [self positionTopOverscrollGlowView];
}
@end
