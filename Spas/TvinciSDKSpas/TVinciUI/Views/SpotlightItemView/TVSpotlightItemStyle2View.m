//
//  TVSpotlightItemStyle2View.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSpotlightItemStyle2View.h"
#import "TVMediaItem.h"
#import "AsyncImageLoader.h"
#import "TVLinearLayout.h"
#import "UIImage+Resize.h"
#import "TVSessionManager.h"
#import "TVConstants.h"
#import "SmartLogs.h"
#define ButtonBackgroundImageName @"green_badge_background"

@implementation TVSpotlightItemStyle2View
@synthesize metadataLine;
@synthesize contentView = _contentView;
@synthesize descriptionLabel;
@synthesize infoDictForItem = _infoDictForItem;

#pragma mark - Memory
- (void)dealloc 
{
    [_infoDictForItem release];
    [_contentView release];
    [metadataLine release];
    [descriptionLabel release];
    [_loadingActivityIndicator release];
    [super dealloc];
}

-(void) setup
{
    self.pictureSize = [TVPictureSize iPhoneItemPosterPictureSize];
    
    if(_infoDictForItem != nil){
        if([_infoDictForItem objectForKey:kItemInfoDictKey_pictureSizeName]){
            NSString *picSizeName = [_infoDictForItem objectForKey:kItemInfoDictKey_pictureSizeName];
            if([picSizeName isEqualToString:@"iPhoneItemPosterPictureSize320X480"]){
                self.pictureSize = [TVPictureSize iPhoneItemPosterPictureSize320X480];
            }
            else if ([picSizeName isEqualToString:@"iPhone2x3GalleryPictureSize"]){
                self.pictureSize = [TVPictureSize iPhone2x3GalleryPictureSize];
            }
            else if ([picSizeName isEqualToString:@"iPadHomePage397X596"]){
                self.pictureSize = [TVPictureSize iPadHomePage397X596];
            }
        }
    }
    
    
    self.backgroundColor = [UIColor clearColor];
    NSString *nibName = NSStringFromClass([self class]);
    
    if(_infoDictForItem && [_infoDictForItem objectForKey:kItemInfoDictKey_nibName]){
        nibName = [_infoDictForItem objectForKey:kItemInfoDictKey_nibName];
    }
    
    [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    self.contentView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    [self addSubview:self.contentView];
    self.metadataLine.margins = 8;
    
    UIImage *bg = [[UIImage imageNamed:ButtonBackgroundImageName] resizableImageWith1x1StretchableArea];
    [self.badgeButton setBackgroundImage:bg forState:UIControlStateNormal];
}

-(void) update
{
    //ASLog(@"%@", [self class]);
    [super update];
    [self populateMetaDataLine];
}

-(void) setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

-(void) populateMetaDataLine
{
   
    
    NSArray *subviews = [NSArray arrayWithArray:self.metadataLine.subviews];
    for (UIView *subview in subviews)
    {
        [subview removeFromSuperview];
    }
    
    NSString *firstGenre=nil;
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_UseBusinessModelForSpotlightTag]){
        firstGenre = [[self.mediaItem.tags objectForKey:TVTagBusinessModel] firstObject];
        if(firstGenre == nil){
            
        }
        ASLog2Debug(@"[bussiness model]%@ %p First genre %@",self.mediaItem,self, firstGenre);////////
    }
    else{
        firstGenre = [[self.mediaItem.tags objectForKey:TVTagGenre] firstObject];
        firstGenre = [firstGenre uppercaseString];
        if ([firstGenre isEqualToString:@"UNKOWN"] || [firstGenre isEqualToString:@"UNKNOWN"])
        {
            firstGenre=@"";
        }

        if(firstGenre == nil || [firstGenre isEqualToString:@""]){
            self.badgeButton.hidden = YES;
        }
    }

    [self.badgeButton setTitle:firstGenre forState:UIControlStateNormal];
    
    CGRect buttonFrame = self.badgeButton.frame;

        
    CGSize constraint = CGSizeMake(self.bounds.size.width, buttonFrame.size.height);
    CGSize textSize = ([firstGenre isEqualToString:@""]) ? CGSizeZero : [firstGenre sizeWithFont:self.badgeButton.titleLabel.font constrainedToSize:constraint];
    textSize.width = MAX(textSize.width, 40);
    
    if(textSize.width > 0){
        buttonFrame.size.width = textSize.width + 5;
    }
    else{
        buttonFrame.size.width=0;
    }
    
    self.badgeButton.frame = buttonFrame;
    [self.metadataLine addSubview:self.badgeButton];

    ASLog2Debug(@" [badgeLenght] %@ %@ %d",self.mediaItem,self.badgeButton.titleLabel.text,self.badgeButton.titleLabel.text.length);////////
    if(self.badgeButton.titleLabel.text == nil || self.badgeButton.titleLabel.text.length < 1){
        [self.badgeButton setHidden:YES];
    }else{
       
        [self.badgeButton setHidden:NO];
    }
    
    
    if([[TVSessionManager sharedTVSessionManager] hasFlag:kSessionFlag_SpotlightUsePromotionText]){
        self.descriptionLabel.text = [self.mediaItem.metaData objectForKey:TVMetaDataShortSummary];
    }
    else{
        self.descriptionLabel.text = self.mediaItem.mediaDescription;
    }
    
    BOOL showDescription = YES;
    if(_infoDictForItem != nil){
        if([_infoDictForItem objectForKey:kItemInfoDictKey_ShowDesription]){
            showDescription = [[_infoDictForItem objectForKey:kItemInfoDictKey_ShowDesription] boolValue];
        }
    }

    if(showDescription){
        CGRect labelFrame = self.descriptionLabel.frame;
        labelFrame.size.width = self.metadataLine.bounds.size.width - buttonFrame.size.width - self.metadataLine.margins;
        self.descriptionLabel.frame = labelFrame;
        [self.metadataLine addSubview:descriptionLabel];
    }
    
}


@end
