//
//  TVSpotlightItemView.m
//  Kanguroo
//
//  Created by Avraham Shukron on 5/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSpotlightItemView.h"
#import "TVMediaItem.h"
#import "AsyncImageLoader.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+RoundedCorner.h"
#import "UIImage+Resize.h"

@interface TVSpotlightItemView ()
@end

@implementation TVSpotlightItemView
@synthesize contentView;
@synthesize imgOverlay;
@synthesize imgBackground;

#pragma mark - Memory
- (void)dealloc 
{
    self.contentView = nil;
    [imgOverlay release];
    [imgBackground release];
    [_activityIndicator release];
    [super dealloc];
}

#pragma mark - Initialization

-(void) setup
{
    [self.titleLabel setHidden:YES];
    self.pictureSize = [TVPictureSize iPhoneSpotlightItemPictureSize];
    self.backgroundColor = [UIColor clearColor];
    NSString *nibName = NSStringFromClass([self class]);
    
    [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    self.contentView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
    [self addSubview:self.contentView];
    
    [self.imageButton addTarget:self action:@selector(selected:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *overlay = [self.imgOverlay.image resizableImageWith1x1StretchableArea];
    self.imgOverlay.image = overlay;
    
    UIImage *bg = [self.imgBackground.image resizableImageWith1x1StretchableArea];;
    self.imgBackground.image = bg;
}

-(void) imageLoaded : (NSNotification *) notification
{
    [self unregisterFromImageNotifications];
    UIImage *image = [notification.userInfo objectForKey:AsyncImageLoaderImageKey];
    image = [image roundedCornerImage:10 borderSize:0];
    [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
    [self revealButtonAnimated];
}
@end
