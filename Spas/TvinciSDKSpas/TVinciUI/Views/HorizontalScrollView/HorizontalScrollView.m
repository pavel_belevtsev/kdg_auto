//
//  HorizontalScrollView.m
//  Kanguroo
//
//  Created by Avraham Shukron on 4/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "HorizontalScrollView.h"

@interface HorizontalScrollView ()
@property (nonatomic ,assign) CGPoint firstTouchLocation;
@end
@implementation HorizontalScrollView
@synthesize firstTouchLocation;

-(BOOL) pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    for (UIView *subview in self.subviews)
    {
        CGPoint pointInSubview = [self convertPoint:point toView:subview];
        if ([subview pointInside:pointInSubview withEvent:event])
        {
            return YES;
        }
    }
    return NO;
}
@end
