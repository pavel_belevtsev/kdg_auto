//
//  HorizontalScrollView.h
//  Kanguroo
//
//  Created by Avraham Shukron on 4/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HorizontalScrollView : UIScrollView

@end
