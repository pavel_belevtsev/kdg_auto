//
//  PaginationTag.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 12/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "PaginationTag.h"


@implementation PaginationTag

-(id) initWithPaginationType:(PaginationTagType) paginationType pageIndex:(NSInteger) pageIndex pageSize:(NSInteger) pageSize
{
    self = [super init];
    if(self)
    {
        self.paginationTagType = paginationType;
        self.pageIndex= pageIndex;
        self.pageSize= pageSize;
    }
    
    return self;
}

@end
