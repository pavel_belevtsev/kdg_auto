//
//  PaginationController.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 12/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "PaginationController.h"


#define activeFetchRequestsKey(PageIndex,PageSize)  [NSString stringWithFormat:@"PageIndex:%d,PageSize:%d",PageIndex,PageSize]

@interface PaginationController ()

@property (retain, nonatomic) NSMutableArray * arrayItems;
@property (assign, nonatomic) NSInteger pageSize;
@property (retain, nonatomic) NSMutableArray * arrayActiveFetchRequests;
@property (assign, nonatomic) NSInteger startOffsetCount;
@property (assign, nonatomic) BOOL endOfList;
@end

@implementation PaginationController

-(id)initWithPageSize:(NSInteger) pageSize
     initialPageIndex:(NSInteger) pageIndex
           fetchBlock:(FetchBlock) fetchBlock
          startOffset:(NSInteger) offset
             delegate:(id<PaginationControllerDelegate>) delegate;
{
    self = [super init];
    if (self)
    {
        self.pageSize =pageSize;
        self.fetchBlock = fetchBlock;
        self.startOffsetCount = offset;
        self.delegate = delegate;
        self.arrayActiveFetchRequests = [NSMutableArray array];
        
        [self loadFirstPage];
    }
    
    return self;
}

-(void) loadFirstPage
{
    __block PaginationController * blockSelf = self;
    __block NSInteger blockIndex = 0;
    __block NSInteger pageIndex = 0;
    
    if(self.fetchBlock)
    {
        [self.arrayActiveFetchRequests addObject:activeFetchRequestsKey(pageIndex,blockSelf.pageSize)];
        
        if ([self.delegate respondsToSelector:@selector(paginationController:didStartLodingNewPage:)])
        {
            [self.delegate paginationController:self didStartLodingNewPage:pageIndex];
        }
        
        self.fetchBlock(pageIndex,blockSelf.pageSize,
                        ^(NSArray *nextItems)
                        {
                            [nextItems retain];
                            [blockSelf addItems:nextItems forPage:pageIndex];
                            [blockSelf.delegate paginationController:blockSelf refreshAndScrollToRowAtIndex:blockIndex];
                            [nextItems release];
                            
                            [blockSelf.arrayActiveFetchRequests removeObject:activeFetchRequestsKey(pageIndex,blockSelf.pageSize)];
                            
                            if ([self.delegate respondsToSelector:@selector(paginationController:didFinishLodingNewPage:)])
                            {
                                [self.delegate paginationController:self didFinishLodingNewPage:pageIndex];
                            }
                        },
                        ^{
                            // show error
                        });
    }

}

-(id) objectAtIndex:(NSInteger) index
{
    // out of bounds
    if (index>=self.arrayItems.count )
    {
        return nil;
    }
    
    // if index is not last index
    if (index != self.arrayItems.count-1 || self.endOfList == YES)
    {
        return [self.arrayItems objectAtIndex:index];
    }
    
    // if there is more items to load and we reached the last index
    id item = [self.arrayItems objectAtIndex:index];
    
    // index is the last index lets load more
    
    
    
    __block PaginationController * blockSelf = self;
    __block NSInteger blockIndex = index;
    __block NSInteger pageIndex = index/self.pageSize+1;
    
    if ([self.arrayActiveFetchRequests containsObject:activeFetchRequestsKey(pageIndex,blockSelf.pageSize)])
    {
        return item ;
    }
    
    if(self.fetchBlock)
    {
        [self.arrayActiveFetchRequests addObject:activeFetchRequestsKey(pageIndex,blockSelf.pageSize)];
        
        if ([self.delegate respondsToSelector:@selector(paginationController:didStartLodingNewPage:)])
        {
            [self.delegate paginationController:self didStartLodingNewPage:pageIndex];
        }
        
        self.fetchBlock(pageIndex,blockSelf.pageSize,
                        ^(NSArray *nextItems)
                        {
                            [nextItems retain];
                            [blockSelf addItems:nextItems forPage:0];
                            [blockSelf.delegate paginationController:blockSelf refreshAndScrollToRowAtIndex:blockIndex];
                            [nextItems release];
                            
                            [blockSelf.arrayActiveFetchRequests removeObject:activeFetchRequestsKey(pageIndex,blockSelf.pageSize)];
                            
                            if ([self.delegate respondsToSelector:@selector(paginationController:didFinishLodingNewPage:)])
                            {
                                [self.delegate paginationController:self didFinishLodingNewPage:pageIndex];
                            }
                        },
                        ^{
                            // show error
                        });
    }

    return item;
}

-(void) addItems:(NSArray *) array forPage:(NSInteger) pageIndex
{
    
    if ((pageIndex==0 && array.count<(self.pageSize-self.startOffsetCount))||(pageIndex!=0 && array.count<self.pageSize ))
    {
        self.endOfList = YES;
    }
    
    [array retain];
    NSArray * firstArray = self.arrayItems;
    NSArray * secondArray = array;
    NSMutableArray * completedArray = [NSMutableArray arrayWithArray:firstArray];
    [completedArray addObjectsFromArray:secondArray];
    self.arrayItems = completedArray;
    [array release];
}


-(NSInteger) count
{
    return self.arrayItems.count;
}

-(void) dealloc
{
    [_arrayActiveFetchRequests release];
    [_arrayItems release];
    [super dealloc];
}

@end
