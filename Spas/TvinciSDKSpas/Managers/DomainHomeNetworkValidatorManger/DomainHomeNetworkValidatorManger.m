//
//  DomainHomeNetworkValidatorManger.m
//  TvinciSDK
//
//  Created by Israel Berezin on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//


#if !TARGET_IPHONE_SIMULATOR

#import "DomainHomeNetworkValidatorManger.h"
#import "MacAdressDiscovery.h"
#import "TVHomeNetworkInfo.h"


@implementation DomainHomeNetworkValidatorManger

static DomainHomeNetworkValidatorManger *sharedInstace = nil;


- (void)foo {
    [[DomainHomeNetworkValidatorManger sharedDomainHomeNetworkValidatorManger] checkIfuserInDomainNetwork];
}

+ (DomainHomeNetworkValidatorManger *)sharedDomainHomeNetworkValidatorManger
{
    @synchronized(self)
    {
        if(sharedInstace == nil)
        {
            sharedInstace = [[DomainHomeNetworkValidatorManger alloc] init];
        }
    }
    
    return sharedInstace;
}

-(void)takeOff
{
    NSLog(@"startWorkWithDomainHomeNetworkValidatorManger");
}

-(id) init
{
    if (self = [super init])
    {
         [self getDomainHomeNetworks];
    }
    
    return self;
}

-(void)reInit
{
    [self getDomainHomeNetworks];
}

-(userNetworkInDomainStatus)checkIfuserInDomainNetwork
{
    if (self.discobery == nil)
    {
        self.discobery = [[[MacAdressDiscovery alloc] init] autorelease];
    }
    NSString * macAddress = [self.discobery getMacAddressWith3GStatus:NO];
    
    ASLogDebug(@"macAddress = %@",macAddress);
    
    if (self.isReady == NO)
    {
        return notReady;
    }

    for (TVHomeNetworkInfo * info in self.userHomeNetworks)
    {
        if ([macAddress isEqualToString:info.uid])
        {
            return userNetworkIsInDomain;
        }
    }

    return userNetworkIsNotInDomain;
}

-(void)getDomainHomeNetworks
{
    self.isReady = NO;
    TVPAPIRequest * request = [TVPDomainAPI requestForGetDomainHomeNetworksWithdelegate:nil];
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request.error);
        NSLog(@"%@",request.debugPostBudyString);
    }];
    
    [request setCompletionBlock:^{
        
        ASLogDebug(@"%@",request);
        NSArray * arr =[request JSONResponse];
        NSMutableArray * dataArr = [NSMutableArray array];
        for (NSDictionary * dic in arr)
        {
            TVHomeNetworkInfo * home = [[TVHomeNetworkInfo alloc] initWithDictionary:dic];
            if (home != nil)
            {
                [dataArr addObject:home];
            }
        }
        ASLogDebug(@"getDomainHomeNetworks dataArr = %@",dataArr);
        self.userHomeNetworks  = [NSArray arrayWithArray:dataArr];
        self.isReady =YES;
//        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [[NSNotificationCenter defaultCenter] postNotificationName:GetDomainHomeNetworksReady object:nil userInfo:nil];

    }];
    
    [self sendRequest:request];
}

#pragma mark - Asynchronic Dangerous Stuff

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}


-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

@end

#endif
