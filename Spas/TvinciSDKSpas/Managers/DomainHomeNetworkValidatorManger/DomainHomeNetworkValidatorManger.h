//
//  DomainHomeNetworkValidatorManger.h
//  TvinciSDK
//
//  Created by Israel Berezin on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MacAdressDiscovery.h"
#import "TVNetworkQueue.h"


#if !TARGET_IPHONE_SIMULATOR

typedef enum{
    userNetworkIsInDomain = 0,
    userNetworkIsNotInDomain,
    notReady
}userNetworkInDomainStatus;

#define GetDomainHomeNetworksReady @"getDomainHomeNetworksIsReady"

@interface DomainHomeNetworkValidatorManger : NSObject

@property (nonatomic, retain) NSArray * userHomeNetworks;

@property (nonatomic, retain)  MacAdressDiscovery * discobery;

@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;

@property BOOL isReady;

+ (DomainHomeNetworkValidatorManger *) sharedDomainHomeNetworkValidatorManger;

-(void)takeOff;
-(void)reInit;

-(userNetworkInDomainStatus)checkIfuserInDomainNetwork;

@end

#endif
