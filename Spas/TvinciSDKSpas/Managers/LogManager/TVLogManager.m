//
//  LogManager.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 7/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVLogManager.h"
#import "SmartLogs.h"





//NSString * TVNameForSocialAction(TVSocialAction action)
//{
//    static NSString *const SocialActionsNames[] = {@"UNKNOWN",@"LIKE",@"UNLIKE",@"SHARE",@"POST",nil};
//    NSString *name = nil;
//    NSInteger length = (sizeof(SocialActionsNames)/sizeof(SocialActionsNames[0]));
//    if (action < length)
//    {
//        name = SocialActionsNames[action];
//    }
//    return name;
//}





@interface TVLogManager ()

@property (assign, nonatomic) LOG_LEVEL level;
@property (assign, nonatomic) NSUInteger groupMask;
@property (assign, nonatomic) NSUInteger outputMask;
@end

@implementation TVLogManager

static TVLogManager * sharedInstance = nil;

+(TVLogManager *) sharedLogManager
{
     @synchronized(self)
    {
        if(sharedInstance ==nil)
        {
            sharedInstance = [[TVLogManager alloc] init];
        }
    }
    
    return sharedInstance;

}



-(void) setLevel:(LOG_LEVEL) logLevel
{
    if (_level != logLevel)
    {
        _level = logLevel;
    }
}

-(void) setOutputMask:(NSUInteger) outputMask
{
    if (_outputMask != outputMask)
    {
        _outputMask = outputMask;
    }
}

-(void) setGroupMask:(NSUInteger) groupMask
{
    if (_groupMask != groupMask)
    {
        _groupMask = groupMask;
    }
}

-(void) sendLogMessage:(NSString *) message group:(LOG_GROUP) group  level:(LOG_LEVEL) level
{
    /**
     *	log group is not in group
     */
    if ((self.groupMask & group) != group)
    {
        return;
    }
    
    /**
     *	log level is importent / grater or equal than the requested level
     */
    if (level < self.level) {
        
        return;
    }
    
    if ((self.outputMask & LOG_OUTPUT_CONSOLE) == LOG_OUTPUT_CONSOLE)
    {
        [self sendLogMessageToConsole:message group:group level:level];
    }
    
    if ((self.outputMask & LOG_OUTPUT_POPUP) == LOG_OUTPUT_POPUP)
    {
        [self sendLogMessageToPopup:message group:group level:level];
    }
    
    if ((self.outputMask & LOG_OUTPUT_FILE) == LOG_OUTPUT_FILE)
    {
        [self sendLogMessageToFile:message group:group level:level];
    }

}

-(void) sendLogMessageToConsole:(NSString *) message group:(LOG_GROUP) group  level:(LOG_LEVEL) level
{
    ASLog2Debug(@"%@",message);
}

-(void) sendLogMessageToPopup:(NSString *) message group:(LOG_GROUP) group  level:(LOG_LEVEL) level
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{

                            NSLog(@"%@",message);
                            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                            [alert release];
                   });
    
    
}

-(void) sendLogMessageToFile:(NSString *) message group:(LOG_GROUP) group  level:(LOG_LEVEL) level
{
    
}



@end
