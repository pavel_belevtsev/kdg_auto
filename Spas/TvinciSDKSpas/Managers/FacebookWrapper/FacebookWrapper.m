//
//  FacebookWrapper.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 4/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//


#ifdef NOT_FB
//Do nothing
#else

#import "FacebookWrapper.h"
#import "SVProgressHUD.h"
#import <FacebookSDK/FBRequest.h>
#import <FacebookSDK/FBRequestConnection.h>

@implementation FacebookWrapper

// Email permission
NSString * const FBPermissionEmail = @"email";

//Extended Profile Properties
NSString * const FBPermissionUserBirthday = @"user_birthday";
NSString * const FBPermissionUserEvents = @"user_events";
NSString * const FBPermissionUserGroups = @"user_groups";
NSString * const FBPermissionUserLikes = @"user_likes";
NSString * const FBPermissionUserLocation = @"user_location";
NSString * const FBPermissionUserPhotos = @"user_photos";
NSString * const FBPermissionUserRelationships = @"user_relationships";
NSString * const FBPermissionUserStatus = @"user_status";
NSString * const FBPermissionFriendsBirthday = @"friends_birthday";
NSString * const FBPermissionFriendsEvents = @"friends_events";
NSString * const FBPermissionFriendsGroups = @"friends_groups";
NSString * const FBPermissionFriendsLikes = @"friends_likes";
NSString * const FBPermissionFriendsLocation = @"friends_location";
NSString * const FBPermissionFriendsPhotos = @"friends_photos";
NSString * const FBPermissionFriendsRelationships = @"friends_relationships";
NSString * const FBPermissionFriendsStatus = @"friends_status";

//Extended Permissions

NSString * const FBPermissionReadFriendsList = @"read_friendlists";
NSString * const FBPermissionReadMailbox = @"read_mailbox";
NSString * const FBPermissionReadUserOnlinePresence = @"user_online_presence";
NSString * const FBPermissionReadFriendsOnlinePresence = @"friends_online_presence";
NSString * const FBPermissionReadStream = @"read_stream";
NSString * const FBPermissionWritePublishActions = @"publish_actions";
NSString * const FBPermissionWriteManageFriendsList = @"manage_friendlists";
NSString * const FBPermissionWriteManageNotifications = @"manage_notifications";
NSString * const FBPermissionWriteCreateEvent = @"create_event";

static FacebookWrapper * theInstance;

+(FacebookWrapper*) sharedFacebookWrapper
{
    if (theInstance == nil)
    {
        theInstance = [[FacebookWrapper alloc] init];
        theInstance.arrReadPermisions = @[FBPermissionEmail];
        theInstance.arrWritePermisions = @[FBPermissionWritePublishActions];
    }
    return theInstance;
}

-(FBSession *)activeSession
{
    return FBSession.activeSession;
}


-(void) facebookLoginWithCompletionBlock:(FBcompletion)completionBlock failedBlock:(FBFailedBlock) failedBlock
{
    
    if (! FBSession.activeSession.isOpen)
    {
        [FBSession openActiveSessionWithReadPermissions:self.arrReadPermisions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             if (error == nil && status == FBSessionStateOpen)
             {
                 self.activeSession = session;
                 completionBlock();
             }
             else
             {
                 failedBlock(FacebookSessionUnknownError,error.userInfo);
             }
         }];
    }
    else
    {
        completionBlock();
    }
    
    
}


-(void) getUserDetails:(void (^)(NSDictionary<FBGraphUser> *user))userDetailsBlock failedBlock:(FBFailedBlock)failedBlock
{
    void (^getDetailsBlock)() = ^
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeNone];
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
         {
             [SVProgressHUD dismiss];
             if (error)
             {
                 failedBlock(FacebookSessionUnknownError,error.userInfo);
             }
             else
             {
                 userDetailsBlock(user);
             }
         }];
    };
    
    if (self.activeSession.isOpen)
    {
        getDetailsBlock();
    }
    else
    {
        failedBlock(FacebookSessionSessionIsClosed,nil);
    }
}

-(void) getWritePermissionsWithCompletionBlock:(FBcompletion)completionBlock failedBlock:(FBFailedBlock)failedBlock
{
    dispatch_async(dispatch_get_current_queue(), ^{
        
        for (NSString* permission in self.arrWritePermisions)
        {
            if ([self.activeSession.permissions indexOfObject:permission] == NSNotFound)
            {
                [self.activeSession requestNewPublishPermissions:self.arrWritePermisions
                                                 defaultAudience:FBSessionDefaultAudienceEveryone
                                               completionHandler:^(FBSession *session, NSError *error)
                {
                                                   if (error == nil)
                                                   {
                                                       completionBlock();
                                                   }
                    else
                    {
                        failedBlock(FacebookSessionUnknownError,error.userInfo);
                    }
                                               }];
                return;
            }
        }
        // all permissions are exists
        completionBlock();
    });
    
}


-(void) publishPostToMyWallWithURL:(NSString*)url
                           message:(NSString*)message
                        imageTitle:(NSString*)imageTitle
                     imageSubTitle:(NSString*)imageSubTitle
                       description:(NSString*)description
                         imageLink:(NSString*)image
                   completionBlock:(FBcompletion)completionBlock
                       failedBlock:(FBFailedBlock) failedBlock

{
         NSDictionary * params = @{@"caption": imageSubTitle ? imageSubTitle : @"",
                                   @"message": message ? message : @"",
                                   @"name" :imageTitle ? imageTitle : @"",
                                   @"description":description ? description : @"",
                                   @"link":url ? url : @"",
                                   @"picture":image ? image : @""};
         
         [FBRequestConnection startWithGraphPath:@"me/feed"
                                      parameters:params
                                      HTTPMethod:@"POST"
                               completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
          {
              if (error)
              {
                  failedBlock(FacebookSessionUnknownError,error.userInfo);
              }
              else
              {
                  completionBlock();
              }
          }
        ];
}


-(void) publishPostToMyWallWithDescription:(NSString*)description completionBlock:(FBcompletion)completionBlock failedBlock:(FBFailedBlock) failedBlock
{
         [FBRequestConnection startForPostStatusUpdate:description
                                     completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                         if (!error)
                                         {
                                             completionBlock();
                                         }
                                         else
                                         {
                                             failedBlock(FacebookSessionUnknownError,error.userInfo);
                                         }
                                     }];
         
}


-(void) postToMyWallWithURL:(NSString*)url
                    message:(NSString*)message
                 imageTitle:(NSString*)imageTitle
              imageSubTitle:(NSString*)imageSubTitle
                description:(NSString*)description
                  imageLink:(NSString*)image
            completionBlock:(FBcompletion)completionBlock
                     failed:(FBFailedBlock)failedBlock
{
    if (self.activeSession.isOpen)
    {
        [self publishPostToMyWallWithURL:url
                                 message:message
                              imageTitle:imageTitle
                           imageSubTitle:imageSubTitle
                             description:description
                               imageLink:image
                         completionBlock:completionBlock
                             failedBlock:failedBlock];
    }
    else
    {
        failedBlock(FacebookSessionSessionIsClosed,nil);
    }
}


-(void) postToMyWallWithDescription:(NSString*)description
                            completionBlock:(FBcompletion)completionBlock
                                failedBlock:(FBFailedBlock) failedBlock
{
    if (self.activeSession.isOpen)
    {
        [self publishPostToMyWallWithDescription:description completionBlock:completionBlock failedBlock:failedBlock];
    }
    else
    {
        failedBlock(FacebookSessionSessionIsClosed,nil);
    }
}

-(void) closeSession
{
    [self.activeSession closeAndClearTokenInformation];
    self.activeSession = nil;
}



@end

#endif
