//
//  FacebookWrapper.h
//  TvinciSDK
//
//  Created by Zeev Blumentzvaig Edited By Rivka S. Peleg on 4/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//




#ifdef NOT_FB
//Do nothing
#else

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#define FACEBOOK_WRAPPER [FacebookWrapper sharedFacebookWrapper]

typedef enum FacebookSessionFailedReason
{
    FacebookSessionSessionIsClosed,
    FacebookSessionAppropriateWritePermissionNotFound,
    FacebookSessionAppropriateReadPermissionNotFound,
    FacebookSessionUnknownError,
    
}FacebookSessionFailedReason;

typedef void (^FBcompletion)();
typedef void (^FBError)();
typedef void (^FBFailedBlock)(FacebookSessionFailedReason failedReason,NSDictionary * info);



// Email permission
extern NSString * const FBPermissionEmail;

//Extended Profile Properties
extern NSString * const FBPermissionUserBirthday;
extern NSString * const FBPermissionUserEvents;
extern NSString * const FBPermissionUserGroups;
extern NSString * const FBPermissionUserLikes;
extern NSString * const FBPermissionUserLocation;
extern NSString * const FBPermissionUserPhotos;
extern NSString * const FBPermissionUserRelationships;
extern NSString * const FBPermissionUserStatus;
extern NSString * const FBPermissionFriendsBirthday;
extern NSString * const FBPermissionFriendsEvents;
extern NSString * const FBPermissionFriendsGroups;
extern NSString * const FBPermissionFriendsLikes;
extern NSString * const FBPermissionFriendsLocation;
extern NSString * const FBPermissionFriendsPhotos;
extern NSString * const FBPermissionFriendsRelationships;
extern NSString * const FBPermissionFriendsStatus;

//Extended Permissions
extern NSString * const FBPermissionReadFriendsList;
extern NSString * const FBPermissionReadMailbox;
extern NSString * const FBPermissionReadUserOnlinePresence;
extern NSString * const FBPermissionReadFriendsOnlinePresence;
extern NSString * const FBPermissionReadStream;
extern NSString * const FBPermissionWritePublishActions;
extern NSString * const FBPermissionWriteManageFriendsList;
extern NSString * const FBPermissionWriteManageNotifications;
extern NSString * const FBPermissionWriteCreateEvent;

@interface FacebookWrapper : NSObject


+(FacebookWrapper*) sharedFacebookWrapper;
/**accessToken for current facebook session .*/


/**current facebook session.*/
@property (retain, nonatomic) FBSession * activeSession;

/**array of all read permissions - just `email` by default.
 
 In order to change your default read permissions set this parameters in Appdelegate,
 and insert to this array one or more strings from FBPermission consts and then call "getWritePermissions"
 */
@property (retain, nonatomic) NSArray* arrReadPermisions;

/**array of all write permissions - just `publish_actions` by default.
 
 In order to change your default write permissions set this parameters in `Appdelegate`,
 and insert to this array one or more strings from `FBPermission` consts.
 
 example (for `email` and `user_birthday` permissions):
 
 FACEBOOK_MANAGER.arrReadPermisions = @[FBPermissionEmail, FBPermissionUserBirthday];
 
 */

@property (retain, nonatomic) NSArray* arrWritePermisions;


/**
 *  Login to facebook with arrReadPermisions permissions - by default is FBPermissionEmail only. operate on active session
 *  If session is already opened , completion block will called immediately
 *
 *  @param failedBlock  failed block , sending 2 parameters reason + message
 *  @param completionBlock completed and succesfull
 */
-(void) facebookLoginWithCompletionBlock:(FBcompletion)completionBlock failedBlock:(FBFailedBlock) failedBlock;
/**
 request for required write permissions by arrWritePermisions
 
 In order to change your default permissions set the arrWritePermisions in Appdelegate
 
 @param completionBlock called when finished get permissions
 */
-(void) getWritePermissionsWithCompletionBlock:(FBcompletion)completionBlock failedBlock:(FBFailedBlock) failedBlock;

/**
 Get details of current user (by requested permissions).
 
 If session is not open - first loginToFacebookWithCompletionBlock: called.
 
 When a user logs into your app and you request no additional permissions, the app will have access to only the user's public profile and also their friend list. Public profile refers to the following properties by default:
 
 - id
 - name
 - first_name
 - last_name
 - link
 - username
 - gender
 - locale
 - age_range
 
 Those parameters are getting by point after `FBGraphUser` instance - like
 
 NSString* FacebookID = user.id;
 
 Other fields that recievd by specific permissions, called by their key from NSDictionary - like
 
 NSString * EmailAddress = user[@"email"];
 
 @param userDetailsBlock called when request finished successfully.
 @param errorBlock called when request failed.
 */
-(void) getUserDetails:(void (^)(NSDictionary<FBGraphUser> *user))userDetailsBlock failedBlock:(FBFailedBlock) failedBlock;

/**
 Post to my wall.
 
 If session is not open - first loginToFacebookWithCompletionBlock: called automaticlly.
 
 If required write permissions not exist - first getWritePermissionsWithCompletionBlock: called automaticlly.
 
 @param url link that triggered when post clicked
 @param message text shown at the top of the post
 @param imageTitle text shown at top of details rectangle and get the url link
 @param imageSubTitle text shown under the image title
 @param description text shown at middle of details rectangle
 @param image link to image
 @param completionBlock called when posted Successfuly
 */
-(void) postToMyWallWithURL:(NSString*)url
                    message:(NSString*)message
                 imageTitle:(NSString*)imageTitle
              imageSubTitle:(NSString*)imageSubTitle
                description:(NSString*)description
                  imageLink:(NSString*)image
            completionBlock:(FBcompletion)completionBlock
                     failed:(FBFailedBlock)failedBlock;


/**
 Post to my wall.
 
 If session is not open - first loginToFacebookWithCompletionBlock: called automaticlly.
 
 If required write permissions not exist - first getWritePermissionsWithCompletionBlock: called automaticlly.
 
 Uesd:
 [FACEBOOK_MANAGER postTestOnlyToMyWallWithDescription:"@MediaDescription" completionBlock:^{
 NSLog(@"post to Facebook !");
 }];
 
 @param description text shown at middle of details rectangle
 @param completionBlock called when posted Successfuly
 
 */
-(void) postToMyWallWithDescription:(NSString*)description
                    completionBlock:(FBcompletion)completionBlock
                        failedBlock:(FBFailedBlock) failedBlock;

/**
 close the current session, initialize activeSession with nil and delete the accessToken.
 */

-(void) closeSession;




@end

#endif
