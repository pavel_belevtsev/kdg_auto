//
//  NSString+SGParse.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SGParse)
-(NSString *) stringWithoutDigits;
-(NSString *)stringByDecodingURLFormat;
+ (BOOL) confirmedNumericsOnlyInString:(NSString*)string ShouldShowWarning:(BOOL)shouldShowWarning ;
@end
