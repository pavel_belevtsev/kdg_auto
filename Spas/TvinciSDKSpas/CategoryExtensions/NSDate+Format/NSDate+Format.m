//
//  NSDate+Format.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "NSDate+Format.h"

NSString *const MediaItemExpirationDateFormat = @"yyyy-MM-dd'T'HH:mm:ss";

@implementation NSDate (Format)
+(NSDate *) dateFromString : (NSString *) dateString withFormat : (NSString *) format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    NSDate *toReturn = [formatter dateFromString:dateString];
    [formatter release];
    
    return toReturn;
}

+ (NSDate *)FriendlyDateTimeByString:(NSString *)dateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = MediaItemExpirationDateFormat;
    NSDate *toReturn = [formatter dateFromString:dateString];
    [formatter release];
    
    return toReturn;
}

-(NSString *) stringWithDateFormat : (NSString *) format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSString *toReturn = [formatter stringFromDate:self];
    [formatter release];
    return toReturn;
}


-(NSString *) stringWithDateFormat : (NSString *)format andGMT:(NSInteger)gmt
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:gmt]];
    NSString *toReturn = [formatter stringFromDate:self];
    [formatter release];
    return toReturn;
}

+ (NSDate *)userVisibleDateTimeStringForRFC3339DateTimeString:(NSString *)rfc3339DateTimeString andFormat:(NSString *)dateFormat
// Returns a user-visible date time string that corresponds to the
// specified RFC 3339 date time string. Note that this does not handle
// all possible RFC 3339 date time strings, just one of the most common
// styles.
{
    NSString *          userVisibleDateTimeString;
    NSDateFormatter *   rfc3339DateFormatter;
    NSLocale *          enUSPOSIXLocale;
    NSDate *            date;
    
    userVisibleDateTimeString = nil;
    
    // Convert the RFC 3339 date time string to an NSDate.
    
    rfc3339DateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    assert(rfc3339DateFormatter != nil);
    
    enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    assert(enUSPOSIXLocale != nil);
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:dateFormat];
    //    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    date = [rfc3339DateFormatter dateFromString:rfc3339DateTimeString];
    
    return date;
}

@end
