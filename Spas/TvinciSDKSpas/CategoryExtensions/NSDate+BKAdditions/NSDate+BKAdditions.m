//
//  NSDate+BKAdditions.m
//  Gifts
//
//  Created by Boris Korneev on 19/04/12.
//  Copyright (c) 2012 Quickode Ltd. All rights reserved.
//

#import "NSDate+BKAdditions.h"

#define CALENDAR [NSCalendar currentCalendar]

@implementation NSDate (BKAdditions)

- (BOOL)isToday {
    NSDate *today = [NSDate date];
    return ((self.month == today.month) && (self.day == today.day));
}

- (BOOL)isTomorrow {
    NSDate *today = [NSDate date];

    NSDateComponents *oneDayComponents = [[[NSDateComponents alloc] init] autorelease];
    oneDayComponents.day = 1;
    NSDate *tomorrow = [CALENDAR dateByAddingComponents:oneDayComponents toDate:today options:0];
    
    return ((self.month == tomorrow.month) && (self.day == tomorrow.day));
}

- (BOOL)isThisWeek {
    NSDate *today = [NSDate date];
    return (self.week == today.week);
}

- (BOOL)isThisMonth {
    NSDate *today = [NSDate date];
    return (self.month == today.month);
}

- (BOOL)isNextMonth {
    NSDate *today = [NSDate date];
    
    NSDateComponents *oneMonthComponents = [[[NSDateComponents alloc] init] autorelease];
    oneMonthComponents.month = 1;

    NSDate *nextMonthDate = [CALENDAR dateByAddingComponents:oneMonthComponents toDate:today options:0];

    return (self.month == nextMonthDate.month);
//    NSInteger nextMonthNormalized = (today.month + 1)%[CALENDAR maximumRangeOfUnit:NSMonthCalendarUnit].length;
//    
//    return (self.month == nextMonthNormalized);
}

- (BOOL)isDatePassed {
    NSDate *today = [NSDate date];
    
    NSDateComponents *deltaComponents = [CALENDAR components:NSDayCalendarUnit
                                                           fromDate:today
                                                             toDate:self
                                                            options:NSWrapCalendarComponents];
    
    return deltaComponents.day < 0;
}

#pragma mark -
-(NSInteger)year {
    NSDateComponents *selfComponents = [CALENDAR components:NSYearCalendarUnit fromDate:self];
    return selfComponents.year;
}

- (NSInteger)week {
    NSDateComponents *selfComponents = [CALENDAR components:NSWeekCalendarUnit fromDate:self];
    return selfComponents.week;
}

- (NSInteger)month {
    NSDateComponents *selfComponents = [CALENDAR components:NSMonthCalendarUnit fromDate:self];
    
    return selfComponents.month;
}

- (NSString *)monthName 
{
    NSDateFormatter * formmater = [[NSDateFormatter alloc] init];
    formmater.dateFormat = @"MMMM";
    NSString *monthName = [formmater stringFromDate:self];
    [formmater release];
    return monthName;
}

-(NSInteger)day {
    NSDateComponents *selfComponents = [CALENDAR components:NSDayCalendarUnit fromDate:self];
    return selfComponents.day;
}

-(NSInteger)hours {
    NSDateComponents *selfComponents = [CALENDAR components:NSHourCalendarUnit fromDate:self];
    return selfComponents.hour;
}

-(NSInteger)minutes {
    NSDateComponents *selfComponents = [CALENDAR components:NSMinuteCalendarUnit fromDate:self];
    return selfComponents.minute;
}

-(NSInteger)seconds {
    NSDateComponents *selfComponents = [CALENDAR components:NSSecondCalendarUnit fromDate:self];
    
    return selfComponents.second;
}
@end
