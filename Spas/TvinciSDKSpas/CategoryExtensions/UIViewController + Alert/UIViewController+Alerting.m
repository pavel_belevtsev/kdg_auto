//
//  UIViewController+Alerting.m
//  YouAppi
//
//  Created by Avraham Shukron on 2/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "UIViewController+Alerting.h"
#define LocalizedErrorTitle NSLocalizedString(@"Error", nil)
#define LocalizedCancelButton NSLocalizedString(@"OK", nil)

@implementation UIViewController (Alerting)
-(void) presentAlertWithTitle : (NSString *) title message : (NSString *) message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title 
                                                    message:message 
                                                   delegate:nil 
                                          cancelButtonTitle:LocalizedCancelButton
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}

-(void) presentAlertWithTitle : (NSString *) title message : (NSString *) message andDelegate:(id)delegate
{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:delegate
                                          cancelButtonTitle:LocalizedCancelButton
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
}


-(void) presentAlertWithError : (NSError *) error
{
    /// shefyg added this to enable flexible use of error with title
    if([[error.userInfo allKeys] containsObject:@"userErrorTitle"] ){
        [self presentAlertWithTitle:[error.userInfo objectForKey:@"userErrorTitle"] message:[error localizedDescription]];
    }
    else{
        
        //NSLocalizedString(@"An Error Occurred",nil)
//        [self presentAlertWithTitle:@"" message:NSLocalizedString(@"Please try again",nil)];
        [self presentAlertWithTitle:LocalizedErrorTitle message:[error localizedDescription]];
    }
}

-(void) presentAlertWithException : (NSException *) exception
{
    [self presentAlertWithTitle:LocalizedErrorTitle message:[exception reason]];
}

@end
