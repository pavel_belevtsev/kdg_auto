//
//  APSRangeArrayTest.m
//  Spas
//
//  Created by Rivka Peleg on 9/21/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "APSRangeArray.h"

@interface APSRangeArrayTest : XCTestCase

@end

@implementation APSRangeArrayTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    APSRange * range1 =[APSRange rangeWithStart:1 end:3];
    APSRange * range2 =[APSRange rangeWithStart:5 end:9];
    APSRange * range3 =[APSRange rangeWithStart:4 end:11];
    APSRange * range4 =[APSRange rangeWithStart:20 end:30];
    
    APSRangeArray * rangeArray = [APSRangeArray rangeArray];
    [rangeArray addRange:range1];
    [rangeArray addRange:range2];
    [rangeArray addRange:range3];
    [rangeArray addRange:range4];
    
    NSArray * intersection = [rangeArray intersection:[APSRange rangeWithStart:2 end:12]];
    NSArray * rangesIntersectRange = [rangeArray rangesIntersectRange:[APSRange rangeWithStart:2 end:12]];
    
    [rangeArray isRangeFullyContainedByAnother:[APSRange rangeWithStart:21 end:30]];

    
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
