//
//  APSEventMonitorWindow.h
//  Spas
//
//  Created by Aviv Alluf on 10/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTouchEventOutsideOfMonitoredView @"kTouchEventOutsideOfMonitoredView"


// APSEventMonitorWindow is a subclass of UIWindow designed to monitor touch events
@interface APSEventMonitorWindow : UIWindow

// all hits outside monitored view are posted as a special notification of type: kTouchEventOutsideOfMonitoredView
@property (strong, nonatomic) NSMutableArray *arrayMonitoredViews;
@property (strong, nonatomic) NSMutableArray *arrayIgnoreViews;

@end
