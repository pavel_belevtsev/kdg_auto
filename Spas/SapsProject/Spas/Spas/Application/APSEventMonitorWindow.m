//
//  APSEventMonitorWindow.m
//  Spas
//
//  Created by Aviv Alluf on 10/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEventMonitorWindow.h"

@implementation APSEventMonitorWindow



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _arrayMonitoredViews = [NSMutableArray array];
        _arrayIgnoreViews = [NSMutableArray array];
    }
    return  self;
}

- (void)sendEvent:(UIEvent *)event {
    if ([self.arrayMonitoredViews count]>0) {

        // we have a view to monitor
        NSSet *touches = [event allTouches];
        if([touches count] == 1) {
            UITouch *touch = [touches anyObject];
            if(touch.phase == UITouchPhaseBegan) {
                
                if ( ![self touchBeganInsideTargetView:touch.view andTargetViews:self.arrayMonitoredViews]) {
                    // we recieved a touch event outside of the monitored view
                    if (( [self.arrayIgnoreViews  count] == 0) ||
                        ![self touchBeganInsideTargetView:touch.view andTargetViews:self.arrayIgnoreViews]) {
                        // the event was not in a view we were told to ignore -> we posr a notification
                            [[NSNotificationCenter defaultCenter] postNotificationName:kTouchEventOutsideOfMonitoredView object:nil];
                    }

                }
            }
        }
    }
    // after monitoring the event we pass it to the UIWindow for standard event processing
    [super sendEvent:event];
}

- (BOOL) touchBeganInsideTargetView: (UIView*) touchedView andTargetViews:(NSArray*) targetViews
{
    // check if the view touched is the monitored view or a subview of one of the target views
    // I'll do this by traversing the superviews of the touched view
    
    for (UIView* targetView in targetViews) {
        UIView* view = touchedView;
        while (view != nil){
            if (targetView == view) // the view we are monitoring (or one of it's super views) was touched!
                return YES;
            view = view.superview;
        }

    }
    
    return NO;
}


@end
