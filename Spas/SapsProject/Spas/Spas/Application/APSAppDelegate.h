//
//  APSAppDelegate.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"

@interface APSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
/*
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
*/
@property(nonatomic, strong) id<GAITracker> tracker;


@property (retain, nonatomic) id  test;


//- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)setupGoogleAnalytics;

@end

