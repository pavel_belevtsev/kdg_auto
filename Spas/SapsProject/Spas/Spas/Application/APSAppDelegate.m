//
//  APSAppDelegate.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//


#import "APSAppDelegate.h"
#import "APSMainViewController.h"
#import <TvinciSDK/TvinciSDK.h>
#import "APSViewControllersFactory_iPad.h"
#import "APSViewControllersFactory_iPhone.h"
#import "APSConfigurationManager.h"
// #import <TvinciSDK/TvinciUDID.h>

#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "APSMethodsURLPipeline.h"
#import "APSSessionManager.h"
#import "APSEventMonitorWindow.h"
#import <SystemConfiguration/CaptiveNetwork.h>
//#import "Crittercism.h"
#import "APSRefreshProcess.h"
#import "APSSSLPinningManager.h"
#import <TvinciSDK/APSCoreDataStore.h>

#define TVSessionManagerUsernameKey  @"TVinciUsername"

#define TVSessionManagerPasswordKey  @"TVinciPassword"

typedef enum
{
    APSDmsServer_Staging = 0,
    APSDmsServer_Lab,
    APSDmsServer_Prod,
    APSDmsServer_PreProd,

}APSDmsServer;

static NSString *const kTrackingId = @"UA-54467443-1";
static NSString *const kAllowTracking = @"allowTracking";

@implementation APSAppDelegate
/*
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
*/

void uncaughtExceptionHandler(NSException *exception)
{

    NSArray * stackTrace = [NSThread callStackSymbols];
    NSLog(@"%@",stackTrace);
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat:@"%@",stackTrace]];
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Start:: Application did finish launching %@",[NSDate date]);
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
//    [TvinciUDID clear];
    
//    [Crittercism enableWithAppID:@"5491d9c43cf56b9e0457caeb"];
    
    [[APSSSLPinningManager manager] start];
    
    [APSMethodsURLPipeline becomeDefaultPipline:[[APSMethodsURLPipeline alloc] init]];
    
    [[APSConfigurationManager sharedTVConfigurationManager] setUDIDType:TvinciUDIDType];

    [TVEPGProgram setTimeReferenceToGMT];
  
    [self registerToNetworkChangeNotification];
    
    [self setupGoogleAnalytics];

    [self setupDMS];

    [self removeUserFromNSUserDefaults];
   
    if (is_iPad())
    {
        [[[APSViewControllersFactory_iPad alloc] init] becomeDefaultFactory];
    }
    else
    {
        [[[APSViewControllersFactory_iPhone alloc] init] becomeDefaultFactory];
    }

    // a subclass of UIWindow - APSEventMonitorWindow is setup to monitor touch events throughout the application
    // This is required in order to dismiss the volume on iPad when a view is touched outside of it's bounds.
    self.window = [[APSEventMonitorWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [[APSEPGManager sharedEPGManager] setLifeCycleTime:60 * 60 * 4];
    
    APSBaseViewController * mainViewController = [[APSViewControllersFactory defaultFactory] mainViewController];
    mainViewController.managedObjectContext = [[APSCoreDataStore defaultStore] managedObjectContext];
    self.window.rootViewController = mainViewController;
    
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    NSLog(@"End: Application did finish launching %@",[NSDate date]);
    
    return YES;
}

-(void)removeUserFromNSUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:TVSessionManagerUsernameKey];
    [defaults removeObjectForKey:TVSessionManagerPasswordKey];
//    NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys]);
//    NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allValues]);

}
     
-(void)setupDMS
{
    /* -------------------------------------------
          Set DMS server before compile app !!!
          -------------------------------------
        APSDmsServer_Staging , APSDmsServer_Lab,
        APSDmsServer_Prod , APSDmsServer_PreProd,
       ------------------------------------------- */
    APSDmsServer dmsServer = APSDmsServer_Prod;
    
    switch (dmsServer)
    {
        case APSDmsServer_Staging:
            [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"http://dms.tvinci.com/api.svc"];
            break;
        case APSDmsServer_Lab:
             [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"http://dms-lab.iptv.kabel-deutschland.de/api.svc"];
            break;
        case APSDmsServer_Prod:
            [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"https://dms-live.iptv.kabel-deutschland.de/api.svc"];

//            [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"https://api-live.iptv.kabel-deutschland.de/dms/api.svc"];
            break;
        case APSDmsServer_PreProd:
            [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"http://dms-pp.iptv.kabel-deutschland.de/api.svc"];
            break;
        default: // Prod
            [[TVConfigurationManager sharedTVConfigurationManager] setDmsBaseURL:@"https://dms-live.iptv.kabel-deutschland.de/api.svc"];
            break;
    }
    [[TVConfigurationManager sharedTVConfigurationManager] setTvUserName:@"kdg"];
    [[TVConfigurationManager sharedTVConfigurationManager] setTvAppPassword:@"DKNaAHvuuaTkhPN8rtTD"];
}

-(void)setupGoogleAnalytics
{
    // User must be able to opt out of tracking
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAllowTracking] == nil)
    {
        [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithBool:YES] forKey:@"allowTracking"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    
    // Initialize Google Analytics with a 120-second dispatch interval. There is a
    // tradeoff between battery usage and timely dispatch.
    [GAI sharedInstance].dispatchInterval = 120;
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    

    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    
    [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    
//    self.tracker = [[GAI sharedInstance] trackerWithName:@"YellowTV"
//                                              trackingId:kTrackingId];
    // Enable IDFA collection.
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    tracker.allowIDFACollection=NO;
}

-(void)registerToNetworkChangeNotification
{
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), //center
                                    NULL, // observer
                                    onNotifyCallback, // callback
                                    CFSTR("com.apple.system.config.network_change"), // event name
                                    NULL, // object
                                    CFNotificationSuspensionBehaviorDeliverImmediately);
}

static void onNotifyCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo)
{

    NSString* notifyName = (__bridge_transfer NSString*)name;
    // this check should really only be necessary if you reuse this one callback method
    //  for multiple Darwin notification events
    if ([notifyName isEqualToString:@"com.apple.system.config.network_change"]) {
        // use the Captive Network API to get more information at this point
        //  http://stackoverflow.com/a/4714842/119114
        
        CFArrayRef myArray =  CNCopySupportedInterfaces();
        NSDictionary * myDict =  (__bridge NSDictionary*) CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
//        
//        ASLogInfo(@"network_change = %@",myDict);
         if (myDict)
         {
             [[NSNotificationCenter defaultCenter] postNotificationName:APSChangeNetwortServerNotification object:nil userInfo:myDict];
         }
        
    }
    else
    {
        NSLog(@"intercepted %@", notifyName);
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    NSNotification *notification = [NSNotification notificationWithName:@"com.ineoquest.appwillresignactive" object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"applicationWillEnterForeground");
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    NSNotification *notification = [NSNotification notificationWithName:@"com.ineoquest.appwillenterforeground" object:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
     NSLog(@"applicationDidBecomeActive");
    
    [[APSRefreshProcess refreshProcess] refreshAppDataWithStartBlock:nil failedBlock:nil completionBlock:nil];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"application will terinate **************** 111111***********************************************Rivka *************");
}

/*
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Spas" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FailedBankCD.sqlite"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}
*/
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - application data


@end
