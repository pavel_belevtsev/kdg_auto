//
//  main.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APSAppDelegate class]));
    }
}
