//
//  APSDefinisions.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#ifndef Spas_APSDefinisions_h
#define Spas_APSDefinisions_h



#define chanenlIDKey @"Channel ID"

// to get property name 
#define NSStringFromProperty(prop) NSStringFromSelector(@selector(prop))
#define NSStringFromClassWithiPadSuffix(classParameter)  [NSString stringWithFormat:@"%@_iPad",NSStringFromClass(classParameter)]
#define NSStringFromClassWithiPhoneSuffix(classParameter) [NSString stringWithFormat:@"%@_iPhone",NSStringFromClass(classParameter)]

#define menuHieght 120
#endif
