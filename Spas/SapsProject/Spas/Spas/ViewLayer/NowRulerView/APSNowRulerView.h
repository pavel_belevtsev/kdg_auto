//
//  APSNowRulerView.h
//  Spas
//
//  Created by Rivka Peleg on 9/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSNowRulerView : UIView
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIView *topQuebeView;


+(APSNowRulerView *) nowRuler;
@end
