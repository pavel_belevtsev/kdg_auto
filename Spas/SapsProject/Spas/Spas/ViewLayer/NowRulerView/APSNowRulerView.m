//
//  APSNowRulerView.m
//  Spas
//
//  Created by Rivka Peleg on 9/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSNowRulerView.h"

@implementation APSNowRulerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.topQuebeView.layer.cornerRadius = 3;
    NSString * text = [NSLocalizedString(@"NOW", nil) uppercaseString];
    self.labelTitle.attributedText = [APSTypography attributedString:text withLetterSpacing:(2)];
    self.labelTitle.font = [APSTypography fontRegularWithSize:11];
    self.labelTitle.textColor = [UIColor whiteColor];
    
}


+(APSNowRulerView *) nowRuler
{
    NSString * nibName = NSStringFromClass([APSNowRulerView class]);
   return  [[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] lastObject];
}
@end
