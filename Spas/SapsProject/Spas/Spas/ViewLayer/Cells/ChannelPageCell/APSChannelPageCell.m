//
//  APSChannelPageCell.m
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSChannelPageCell.h"
#import "APSEpgMetaDataView.h"
#import "APSEpgMetaDataParser.h"

@implementation APSChannelPageCell

#pragma mark - Init -

+(APSChannelPageCell *) channelPageCell
{
    APSChannelPageCell *view = nil;
    NSString *nibName = @"APSChannelPageCell";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    view.backgroundColor = [UIColor clearColor];
    view.contentView.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Life -

- (void)awakeFromNib
{
    self.lblTime.font = [APSTypography fontMediumWithSize:12];
    self.lblTime.textColor = [APSTypography colorMediumGray];
    
    self.lblTitle.font = [APSTypography fontRegularWithSize:14];
    self.lblTitle.textColor = [APSTypography colorDrakGray];
    
    self.lblDescription.font = [APSTypography fontLightWithSize:14];
    self.lblDescription.textColor = [APSTypography colorMediumGray];
}

#pragma mark - UI - 

-(void)clearMode
{
    self.itemsView.hidden = YES;
}

-(void)showMode
{
    self.itemsView.hidden = NO;
}

-(void)updateCellWithChannelProgram:(APSTVProgram *)program
{
    self.program = program;
    
    self.lblTime.text = [self timeLblStingFormatWithStartTime:program.startDateTime EndTime:program.endDateTime];
    
    // time lbl color
    if ([APSEpgMetaDataView isProgramInCurrent:program])
    {
        self.lblTime.textColor = [APSTypography colorBrand];
    }
    else
    {
        self.lblTime.textColor = [APSTypography colorMediumGray];
    }
    self.lblTitle.text = self.program.name;
    self.lblDescription.text =  [APSEpgMetaDataParser findFiledText:@"subtitle" inProgram:self.program];
}


#pragma mark - setters -

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted)
    {
        self.alpha = 0.5;
    }
    else
    {
        self.alpha = 1.0;
    }
}

-(NSString*)timeLblStingFormatWithStartTime:(NSDate*)startTime EndTime:(NSDate*)endTime
{
    NSString * time= nil;
    NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:startTime];
    NSInteger hourStart = [componentsStart hour];
    NSInteger minuteStart = [componentsStart minute];
    
    NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:endTime];
    NSInteger hourEnd = [componentsEnd hour];
    NSInteger minuteEnd = [componentsEnd minute];
    //now
//    NSDate * now = [NSDate date];
    
//    if([now compare:startTime] == NSOrderedDescending && [now compare:endTime] == NSOrderedAscending)
//    {
//        NSString * now = NSLocalizedString(@"Now", nil);
//        time = [NSString stringWithFormat:@"%@ - %02d:%02d",now,hourEnd,minuteEnd];
//    }
//    else
//    {
        time =[NSString stringWithFormat:@"%02d:%02d-%02d:%02d",hourStart,minuteStart,hourEnd,minuteEnd];
//    }
    
    return time;
}
@end
