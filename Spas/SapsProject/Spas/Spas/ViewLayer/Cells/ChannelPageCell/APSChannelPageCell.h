//
//  APSChannelPageCell.h
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSChannelPageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *itemsView;

@property (strong, nonatomic) APSTVProgram * program;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *aaa;


+(APSChannelPageCell *) channelPageCell;
-(void)updateCellWithChannelProgram:(APSTVProgram *)program;
-(void)clearMode;
-(void)showMode;

@end
