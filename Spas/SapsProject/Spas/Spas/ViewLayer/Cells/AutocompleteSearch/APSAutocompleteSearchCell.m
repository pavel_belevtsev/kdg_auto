//
//  APSAutocompleteSearch.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSAutocompleteSearchCell.h"

@implementation APSAutocompleteSearchCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.labelText.font = [APSTypography fontRegularWithSize:19];
   // self.labelText.textColor = [APSTypography colorLightGray];
    
}

+(NSString *) nibName
{
    return @"APSAutocompleteSearchCell_iPhone";
//    if (is_iPad())
//    {
//        return @"APSAutocompleteSearchCell_iPhone";
//        //return @"APSAutocompleteSearchCell_iPad";
//    }
//    else
//    {
//        return @"APSAutocompleteSearchCell_iPhone";
//    }
}

+(CGSize) cellSize
{
    if (is_iPad())
    {
        return CGSizeMake(974, 21);
    }
    else
    {
        return CGSizeMake(303, 21);
    }
}

+(UIEdgeInsets ) cellEdgeInsets
{
    if (is_iPad())
    {
        return UIEdgeInsetsMake(24,0, 24, 50);
    }
    else
    {
        return UIEdgeInsetsMake(24, 17, 24,17 );
    }
}

+(UIEdgeInsets ) tableEdgeInsets
{
    if (is_iPad())
    {
        return UIEdgeInsetsMake(32, 50, 32, 50);
    }
    else
    {
        return UIEdgeInsetsMake(20, 17, 20, 17);
    }

}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
        [self setAlpha:0.5];
    }
    else
    {
        [self setAlpha:1.0];
    }
}


@end
