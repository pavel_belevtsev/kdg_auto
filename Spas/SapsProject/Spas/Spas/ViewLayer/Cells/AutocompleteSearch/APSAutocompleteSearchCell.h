//
//  APSAutocompleteSearch.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSAutocompleteSearchCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;


+(NSString *) nibName;
+(CGSize) cellSize;
+(UIEdgeInsets ) cellEdgeInsets;
+(UIEdgeInsets ) tableEdgeInsets;

@end
