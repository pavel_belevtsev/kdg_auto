//
//  APSMiniEPGChannel.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Israel Berezin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSMiniEpgBaseCell.h"

@interface APSMiniEPGChannelCell : APSMiniEpgBaseCell

@property (weak, nonatomic) IBOutlet UIImageView *channelImage;

@property (weak, nonatomic) IBOutlet UILabel *channelName;


-(void)updateCell:(TVMediaItem *) channelMedia andCellIndex:(NSInteger)cellIndex;

+(APSMiniEPGChannelCell *) miniEPGChannelCell;

@end
