//
//  APSMiniEPGChannel.m
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Israel Berezin. All rights reserved.
//

#import "APSMiniEPGChannelCell.h"

@implementation APSMiniEPGChannelCell


#pragma mark - init -

+(APSMiniEPGChannelCell *) miniEPGChannelCell
{
    if(is_iPad())
    {
        return [APSMiniEPGChannelCell miniEPGChannelCell_ipad];
    }
    else
    {
        return [APSMiniEPGChannelCell miniEPGChannelCell_iPhone];
    }
}

+(APSMiniEPGChannelCell *) miniEPGChannelCell_ipad
{
    APSMiniEPGChannelCell *view = nil;
    NSString *nibName =@"APSMiniEPGChannelCell_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

+(APSMiniEPGChannelCell *) miniEPGChannelCell_iPhone
{
    APSMiniEPGChannelCell *view = nil;
    NSString *nibName = @"APSMiniEPGChannelCell_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

#pragma mark - Life -

- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    self.channelName.textColor = [UIColor whiteColor];
}

#pragma mark - setters - 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
}

#pragma mark - UI

-(void)updateCell:(TVMediaItem *) channelMedia andCellIndex:(NSInteger)cellIndex
{
    self.channelMedia = channelMedia;
    
    NSString* channelLabel = [NSString stringWithFormat:@"%d %@",cellIndex+1, channelMedia.name];
    self.channelName.text = channelLabel;
  
    [self.channelImage setImageWithURL:[channelMedia pictureURLForSize:self.channelImage.frame.size] placeholderImage:nil];
}



@end
