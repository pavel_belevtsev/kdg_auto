//
//  APSSimpleTitledCell.h
//  Spas
//
//  Created by Rivka Peleg on 9/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSSimpleTitledCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImage;


+(NSString *) nibName;
+(CGSize ) cellSize;
+(UIEdgeInsets ) cellEdgeInsets;
@end
