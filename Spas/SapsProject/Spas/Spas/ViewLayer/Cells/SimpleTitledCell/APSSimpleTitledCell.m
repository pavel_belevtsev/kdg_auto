//
//  APSSimpleTitledCell.m
//  Spas
//
//  Created by Rivka Peleg on 9/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSimpleTitledCell.h"

@implementation APSSimpleTitledCell

- (void)awakeFromNib {
    // Initialization code
}


+(NSString *) nibName
{
    if(is_iPad())
    {
        return NSStringFromClassWithiPadSuffix([APSSimpleTitledCell class]);
    }
    else
    {
        return NSStringFromClassWithiPhoneSuffix([APSSimpleTitledCell class]);
    }
}

+(CGSize ) cellSize
{
    if (is_iPad())
    {
        return CGSizeMake(100, 44);
    }
    else
    {
        return CGSizeMake(320, 41);
    }
}

+(UIEdgeInsets ) cellEdgeInsets
{
    return  UIEdgeInsetsZero;
}

@end
