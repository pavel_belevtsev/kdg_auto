//
//  APSHomeProgramCollectionViewCell.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/AsyncImageView.h>
#import "APSButtonHighlighted.h"

@class APSTVProgram;
@class APSTVChannel;


@protocol APSProgramCollectionViewCellDelegate <NSObject>

- (void)programCellDidSelectProgramInfo:(APSTVProgram *)program channel:(TVMediaItem *)channel;

@end

@interface APSProgramCollectionViewCell : UICollectionViewCell <APSButtonHighlightedDelegate>

@property (strong, nonatomic) APSTVProgram * program;
@property (strong, nonatomic) TVMediaItem * channel;
@property (weak, nonatomic) IBOutlet UILabel *labelProgramName;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UIView *viewProgress;

@property (weak, nonatomic) IBOutlet UIImageView *imageGradient;
@property (weak, nonatomic) IBOutlet UIImageView *imageProgram;
@property (weak, nonatomic) IBOutlet UIImageView *imageChannel;
@property (weak, nonatomic) IBOutlet UILabel *labelImageChannelName;
@property (weak, nonatomic) IBOutlet APSButtonHighlighted *buttonInfo;

@property BOOL isPogramHaveImage;

-(void) setProgram:(APSTVProgram *) program;
-(void)setChannel:(TVMediaItem *)channel andProgram:(APSTVProgram *)program;

+(NSString *) nibName;
+(CGSize) cellSize;
+(UIEdgeInsets) cellEdgeInsets;

@property (nonatomic, weak) id <APSProgramCollectionViewCellDelegate> delegate;

@end
