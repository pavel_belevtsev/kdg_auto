//
//  APSHomeProgramCollectionViewCell.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSProgramCollectionViewCell.h"
#import <TvinciSDK/UIImageView+AFNetworking.h>
#import "APSEpgMetaDataView.h"

@interface APSProgramCollectionViewCell ()
@property CGRect originalImageChannelFrame;
@property UIColor* originalProgramImageBGColor;
@property CGRect originalLabelChannelNameFrame;
@end


@implementation APSProgramCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        ASLogInfo(@"< --- >")
    }
    return self;
}

-(void)awakeFromNib
{
    if (is_iPad())
    {
        self.labelProgramName.font = [APSTypography fontLightWithSize:17];
        self.labelTime.font = [APSTypography fontRegularWithSize:13];
    }
    else
    {
        self.labelProgramName.font = [APSTypography fontLightWithSize:14];
        self.labelTime.font = [APSTypography fontRegularWithSize:11];
    }
    
    self.labelProgramName.textColor = [APSTypography colorDrakGray];
    self.labelTime.textColor = [APSTypography colorLightGray];
    
    // save original locations & colors
    self.originalImageChannelFrame = self.imageChannel.frame;
    self.originalProgramImageBGColor = self.imageProgram.backgroundColor;
    self.originalLabelChannelNameFrame = self.labelImageChannelName.frame;
    self.labelImageChannelName.hidden = YES;
    
    self.buttonInfo.delegate = self;
    self.buttonInfo.exclusiveTouch = YES;
    
    self.viewProgress.backgroundColor = [APSTypography colorBrand];
    self.viewProgress.hidden = YES;
    
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageProgram.image = nil;
    self.imageChannel.image = nil;
    
    self.labelProgramName.text = nil;
    self.labelTime.text = nil;
    self.imageProgram.backgroundColor = self.originalProgramImageBGColor;
    self.imageChannel.frame = self.originalImageChannelFrame;
    self.labelImageChannelName.frame = self.originalLabelChannelNameFrame;
    self.labelImageChannelName.textAlignment = NSTextAlignmentLeft;
    self.labelImageChannelName.hidden = YES;
    self.imageGradient.hidden = YES;
    
    self.viewProgress.hidden = YES;
    
}

+(NSString *) nibName
{
    if (is_iPad())
    {
        return  @"APSProgramCollectionViewCell_iPad";
    }
    else
    {
        return  @"APSProgramCollectionViewCell_iPhone";
    }
}

+(CGSize) cellSize
{
    if (is_iPad())
    {
        return CGSizeMake(308, 224);
    }
    else
    {
        return CGSizeMake(148, 131);
    }
}

+(UIEdgeInsets) cellEdgeInsets
{
    if (is_iPad())
    {
        return UIEdgeInsetsMake(25, 25, 25, 25);
    }
    else
    {
        return UIEdgeInsetsMake(20, 8, 20, 8);
    }
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
        [self setAlpha:0.5];
    }
    else
    {
        [self setAlpha:1.0];
    }
}

-(void)setProgram:(APSTVProgram *)program
{
    if (program && _program != program)
    {
        _program = program;
    }
    if (program)
    {
        [self updateUIWithProgram:_program];
    }
    else
    {
        [self updateUIWithProgram:nil];
    }
}

-(void)setChannel:(TVMediaItem *)channel
{
    if (channel && _channel != channel)
    {
        _channel = channel;
    }
    
}

-(void)setChannel:(TVMediaItem *)channel andProgram:(APSTVProgram *)program
{
    [self setChannel:channel];
    [self setProgram:program];
    if (program == nil)
    {
        self.isPogramHaveImage = NO;
        [self updateUIWithChannel:channel];
    }
}

-(void) updateUIWithProgram:(APSTVProgram *) program
{
    UIImage * defualtImage = [UIImage imageNamed:@""];
//    ASLogInfo(@"%@",program.pictureURL);
    
    NSString * dateAndTimeString = @"";
    if (program.startDateTime)
        
    {
        dateAndTimeString = [NSString stringWithFormat:@"%@  |  %@",[APSUtils stringTimeFromDate:program.startDateTime toDate:program.endDateTime],[APSUtils stringDateFromDate:program.startDateTime]];
        dateAndTimeString = [dateAndTimeString uppercaseString];
    }
    
    self.labelTime.text= dateAndTimeString;
    
    if ([program.name length]>0) {
        self.labelProgramName.text = program.name;
    }else{
        self.labelProgramName.text = NSLocalizedString(@"No program details", nil);
    }
    
    self.labelImageChannelName.hidden = YES;
    
    CGSize imageSize = CGSizeMake(148, 83);
    if (is_iPad())
    {
        imageSize = CGSizeMake(308, 173);
    }
    NSURL * imageUrl = [program pictureURLForSizeWithRetinaSupport:imageSize];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:imageUrl];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"fasdf"]];
    
    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:program.pictureURL]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.imageProgram setImageWithURLRequest:request placeholderImage:defualtImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        self.isPogramHaveImage = YES;
        self.imageProgram.image = image;
        // show the gradient on top of the program image:
        self.imageGradient.hidden = NO;
        
        if (self.channel)
        {
            [self updateUIWithChannel:self.channel];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
        self.isPogramHaveImage = NO;
        if (self.channel)
        {
            [self updateUIWithChannel:self.channel];
        }
    }];
    
    self.buttonInfo.enabled = (_delegate != nil);
    
    self.viewProgress.hidden = YES;
    
    if ([APSEpgMetaDataView isProgramInCurrent:program]) {
        
        NSDate * now = [NSDate date];
        NSTimeInterval intervalFull = [program.endDateTime timeIntervalSinceReferenceDate] - [program.startDateTime timeIntervalSinceReferenceDate];
        NSTimeInterval intervalCurrent = [now timeIntervalSinceReferenceDate] - [program.startDateTime timeIntervalSinceReferenceDate];
        
        if ((intervalFull > 0) && (intervalCurrent > 0) && (intervalCurrent <= intervalFull)) {
            
            self.viewProgress.frame = CGRectMake(self.viewProgress.frame.origin.x, self.viewProgress.frame.origin.y, self.imageProgram.frame.size.width * intervalCurrent / intervalFull, self.viewProgress.frame.size.height);
            self.viewProgress.hidden = NO;
            
            
        }
        
    }
    
}

-(void) updateUIWithChannel:(TVMediaItem *) mediaItem
{
    if (!self.isPogramHaveImage)
    {
        self.imageProgram.backgroundColor = [APSTypography colorGrayWithValue:80];

        // no program image - we will position the channel image in the center:
        if (is_iPad()) {
            self.imageChannel.size = CGSizeMake(140, 70);
//            self.imageChannel.size = CGSizeMake(82, 42);
        }else {
            self.imageChannel.size = CGSizeMake(49, 25);
        }
        self.imageChannel.center = self.imageProgram.center;
    }
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"fdsaf"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self.channel pictureURLForSizeWithRetinaSupport:self.imageChannel.size]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.imageChannel setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        self.imageChannel.image = image;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
        
        // failed to load the channel image, will display the channel number + name instead
        NSString * channelNumber = [self.channel.metaData objectForKey:@"Channel number"];
        NSString* channelTitle = [NSString stringWithFormat:@"%@ %@",channelNumber,self.channel.name];
        self.labelImageChannelName.text = channelTitle;
        CGFloat fontSize = self.isPogramHaveImage ? 14 : is_iPad()? 26:15;
        self.labelImageChannelName.font = [APSTypography fontLightWithSize:fontSize];
//        self.labelImageChannelName.textColor = [APSTypography colorLightGray];
        self.labelImageChannelName.textColor = [APSTypography colorGrayWithValue:210];
        
        self.labelImageChannelName.hidden = NO;
        
        // set x position
        self.labelImageChannelName.x = is_iPad()? 8:7;
        // set y position
        if (self.isPogramHaveImage) {
            if (is_iPad()) {

            self.labelImageChannelName.y = self.imageChannel.y;
            }
            else{
                self.labelImageChannelName.y = self.imageChannel.y - 3;
            }
        }else{
            // no picture for program: we should display the channel name
            // in the center of the cell.
            self.labelImageChannelName.centerY = self.imageProgram.centerY;
            self.labelImageChannelName.textAlignment = NSTextAlignmentCenter;
        }
        self.labelImageChannelName.width = self.width - self.labelImageChannelName.x * 2;
        
    }];
    
}

#pragma mark - Info

- (IBAction)buttonInfoPressed:(UIButton *)button {
    
    if (_delegate && [_delegate respondsToSelector:@selector(programCellDidSelectProgramInfo:channel:)]) {
        
        [_delegate programCellDidSelectProgramInfo:_program channel:_channel];
        
    }
}

#pragma mark - APSButtonHighlightedDelegate

- (void)buttonSetHighlighted:(BOOL)highlighted {
    
    float alpha = (highlighted ? 0.5 : 1.0);
    
    if (self.isHighlighted) {
        alpha = 0.5;
    }
    
    self.labelProgramName.alpha = alpha;
    self.labelTime.alpha = alpha;
    
}

@end
