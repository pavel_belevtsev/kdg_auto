//
//  APSTabCollectionViewCell.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTabCollectionViewCell.h"

@implementation APSTabCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setSelected:NO];
    self.viewSelectedIndicator.backgroundColor = [APSTypography colorBrand];

    if (is_iPad())
    {
        self.labelTitle.font = [APSTypography fontRegularWithSize:14];
    }
    else
    {
        self.labelTitle.font = [APSTypography fontLightWithSize:26];
    }
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self setSelected:NO];
}
/* SYN-3559
-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected)
    {
        self.labelTitle.textColor = [APSTypography colorBrand];
        self.viewSelectedIndicator.hidden = NO;
    }
    else
    {
        self.viewSelectedIndicator.hidden = YES;
        if (is_iPad())
        {
             self.labelTitle.textColor = [UIColor lightGrayColor];
        }
        else
        {
            self.labelTitle.textColor = [UIColor whiteColor];
        }
    }
}
*/
+(NSString *) nibName
{
    if(is_iPad())
    {
        return NSStringFromClassWithiPadSuffix([APSTabCollectionViewCell class]);
    }
    else
    {
        return NSStringFromClassWithiPhoneSuffix([APSTabCollectionViewCell class]);
    }
}

+(CGSize ) size
{
    if(is_iPad())
    {
        return CGSizeMake(110, 44);
    }
    else
    {
       return CGSizeMake(320, 40);
    }
}

+(UIEdgeInsets) cellEdgeInsets
{
    if (is_iPad())
    {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    else
    {
        return UIEdgeInsetsMake(30, 30, 30, 0);
    }
    
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];

    if (highlighted)
    {
        self.alpha = 0.5;
    }
    else
    {
        self.alpha = 1.0;
    }
}



@end
