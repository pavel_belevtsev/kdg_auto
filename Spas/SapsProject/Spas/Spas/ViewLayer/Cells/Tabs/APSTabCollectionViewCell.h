//
//  APSTabCollectionViewCell.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSTabCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIView *viewSelectedIndicator;


+(NSString *) nibName;
+(CGSize ) size;
@end
