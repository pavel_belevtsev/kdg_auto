//
//  APSMiniEPGProgramsTableViewCell.m
//  Spas
//
//  Created by Aviv Alluf on 8/19/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define  NextProgrmeSpace 58
#define  iPadProgramWidth 655

#import "APSMiniEPGProgramsTableViewCell.h"
#import "APSMiniEPGProgramView.h"

@implementation APSMiniEPGProgramsTableViewCell 

#pragma mark - Init -

+(APSMiniEPGProgramsTableViewCell *) miniEPGCell
{
    if(is_iPad())
    {
        return [APSMiniEPGProgramsTableViewCell miniEPGChannelCell_ipad];
    }
    else
    {
        return [APSMiniEPGProgramsTableViewCell miniEPGChannelCell_iPhone];
    }
}

- (IBAction)didSelectChannel:(id)sender
{
    [self.delegate miniEPGProgramsTableViewCell:self didSelectChannel:self.channelMedia];
}



+(APSMiniEPGProgramsTableViewCell *) miniEPGChannelCell_ipad
{
    APSMiniEPGProgramsTableViewCell *view = nil;
    NSString *nibName = @"APSMiniEPGProgramsTableViewCell_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    view.backgroundColor = [UIColor clearColor];
    view.contentView.backgroundColor = [UIColor clearColor];
    return view;
}

+(APSMiniEPGProgramsTableViewCell *) miniEPGChannelCell_iPhone
{
    APSMiniEPGProgramsTableViewCell *view = nil;
    NSString *nibName = @"APSMiniEPGProgramsTableViewCell_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    view.backgroundColor = [UIColor clearColor];
    view.contentView.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark - Life -
- (void)awakeFromNib
{
    // Initialization code
    self.channelName.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    [self.channelName setFont:[APSTypography fontRegularWithSize:15]];

    self.touched = NO;
    [self.seperatorView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];

    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    self.carousel.type = iCarouselTypeLinear;
    self.carousel.bounces = NO;
    [self.carousel setDecelerationRate:0.5];
    [self.carousel setVertical:NO];
}

#pragma mark - Update UI -

-(void)updateCell:(TVMediaItem *) channelMedia andCellIndex:(NSInteger)cellIndex
{
    self.channelMedia = channelMedia;
    NSString * channelNumber = [channelMedia.metaData objectForKey:@"Channel number"];
    NSString* channelLabel = [NSString stringWithFormat:@"%@ %@",channelNumber, [channelMedia.name uppercaseString]];
    self.channelName.text = channelLabel;
    [self.channelImage setImageWithURL:[channelMedia pictureURLForSizeWithRetinaSupport:self.channelImage.frame.size] placeholderImage:nil];
}

-(void)updateCarouselContentOffset
{
    // contentOffset = carouselItemWidth - self.carousel.width + (nextProgarmSpace/2)
    
    NSInteger carouselItemWidth = self.frame.size.width - (self.channelView.frame.size.width + NextProgrmeSpace);
    NSInteger contentOffsetX =  carouselItemWidth - self.carousel.width + (NextProgrmeSpace/2);
    if (is_iPad())
    {
        NSInteger nextProgarmIpadSpace = self.carousel.width - iPadProgramWidth;
        carouselItemWidth = self.frame.size.width - (self.channelView.frame.size.width + nextProgarmIpadSpace);
        contentOffsetX =  carouselItemWidth - self.carousel.width + (nextProgarmIpadSpace/2);
    }
    
    self.carousel.contentOffset = CGSizeMake(contentOffsetX,0);
}

-(void)updateCellWithChannelProgramsArray:(NSArray *) programsArray
{
    self.touched = NO;
    
    self.pogramView.width = self.width - self.channelView.width;
    self.carousel.width = self.pogramView.width;
    self.activityIndicator.center = self.pogramView.center;
    
    if (programsArray != nil)
    {
        self.currChannelProgram = [NSArray arrayWithArray:programsArray];
        
        [self updateCarouselContentOffset];
        [self.carousel reloadData];
        self.touched=YES;
        [self.carousel scrollToItemAtIndex:[self.currChannelProgram count]-1 animated:NO];
        [self.carousel scrollToItemAtIndex:0 animated:NO];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

#pragma mark - Carousel -

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.currChannelProgram count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(APSMiniEPGProgramView *)programView
{
    if (programView == nil || ![programView isKindOfClass:[APSMiniEPGProgramView class]])
    {
        programView = [APSMiniEPGProgramView miniEPGProgramView];
    }
    if (!is_iPad())
    {
        programView.width = self.frame.size.width - (self.channelView.frame.size.width + NextProgrmeSpace);
    }
    
    programView.tag = index;

    APSTVProgram * program = [self.currChannelProgram objectAtIndex:index];

    programView.program = program;
    
    programView.backgroundColor = [UIColor clearColor];

    programView.alpha = 1.0;
    
    return programView;
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel;
{
    if (is_iPad())
    {
        return 655;
    }
    else
    {
        NSInteger width = self.frame.size.width - (self.channelView.frame.size.width + NextProgrmeSpace);
        return width;
    }
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
    return 3;
}

-(void)carouselWillBeginDragging:(iCarousel *)carousel
{
    self.touched = YES;
}

#define NON_CENTERED_ITEM_ALPHA 0.5

- (void)carouselDidScroll:(iCarousel *)carousel
{
    NSInteger currentItemIndex = [carousel currentItemIndex];//(self.carousel.scrollOffset+(self.carousel.itemWidth/2))/(self.carousel.itemWidth);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"delayAutoCloseMiniEPG" object:nil];
    
    
    if (self.touched)
    {
        CGFloat alphaIndex = NON_CENTERED_ITEM_ALPHA;
       // NSLog(@"carousel.scrollOffset = %f",carousel.scrollOffset);
        if (carousel.scrollOffset >= currentItemIndex*self.carousel.itemWidth)
        {
            alphaIndex = 1 - (NON_CENTERED_ITEM_ALPHA * ((self.carousel.scrollOffset - (currentItemIndex*self.carousel.itemWidth)) / (self.carousel.itemWidth/2)));
            [carousel itemViewAtIndex:currentItemIndex].alpha = alphaIndex;
        }
        else
        {
            alphaIndex = 1 - (NON_CENTERED_ITEM_ALPHA * (((currentItemIndex*self.carousel.itemWidth) - self.carousel.scrollOffset) / (self.carousel.itemWidth/2)));
            [carousel itemViewAtIndex:currentItemIndex].alpha = alphaIndex;
        }
        [carousel itemViewAtIndex:currentItemIndex - 1].alpha = NON_CENTERED_ITEM_ALPHA;
        [carousel itemViewAtIndex:currentItemIndex + 1].alpha = NON_CENTERED_ITEM_ALPHA;
        
    }
    else
    {
        [carousel itemViewAtIndex:currentItemIndex].alpha = 1;
        [carousel itemViewAtIndex:currentItemIndex - 1].alpha = NON_CENTERED_ITEM_ALPHA;
    }
}

#pragma mark - memory -

-(void)dealloc
{
    self.carousel.delegate = nil;
    self.carousel.dataSource = nil;
}

- (IBAction)fakeBouttonPush:(id)sender
{
    ASLogInfo(@"fakeBouttonPush");
}

@end
