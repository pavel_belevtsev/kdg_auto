//
//  APSMiniEPGProgramsTableViewCell.h
//  Spas
//
//  Created by Aviv Alluf on 8/19/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSMiniEpgBaseCell.h"
#import "iCarousel.h"

@class APSMiniEPGProgramsTableViewCell;


@interface APSMiniEPGProgramsTableViewCell : APSMiniEpgBaseCell <iCarouselDataSource, iCarouselDelegate,UIScrollViewDelegate>

@property (assign, nonatomic) id<APSMiniEPGProgramsTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *pogramView;

@property (weak, nonatomic) IBOutlet UIImageView *channelImage;
@property (weak, nonatomic) IBOutlet UIView *channelView;

@property (weak, nonatomic) IBOutlet UILabel *channelName;

@property (strong, nonatomic) IBOutlet iCarousel *carousel;

@property (assign, nonatomic) BOOL touched;



@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;

-(void)updateCell:(TVMediaItem *) channelMedia andCellIndex:(NSInteger)cellIndex;

-(void)updateCellWithChannelProgramsArray:(NSArray *) programsArray;

+(APSMiniEPGProgramsTableViewCell *) miniEPGCell;

- (IBAction)didSelectChannel:(id)sender;

@end
