//
//  APSMiniEpgBaseCell.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Israel Berezin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/UIImageView+AFNetworking.h>

@class APSMiniEpgBaseCell;

@protocol APSMiniEPGProgramsTableViewCellDelegate <NSObject>

-(void) miniEPGProgramsTableViewCell:(APSMiniEpgBaseCell *) miniEPGProgramsTableViewCell didSelectChannel:(TVMediaItem *) channel;

@end

@interface APSMiniEpgBaseCell : UITableViewCell

@property (strong, nonatomic) TVMediaItem * channelMedia;
@property (strong, nonatomic) NSArray * currChannelProgram;

@end
