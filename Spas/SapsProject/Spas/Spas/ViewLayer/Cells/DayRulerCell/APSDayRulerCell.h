//
//  APSDayRulerCell.h
//  Spas
//
//  Created by Rivka Peleg on 9/9/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSDayRulerCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UILabel *labelDay;
+(NSString *) nibName;
+(CGSize ) cellSize;
+(UIEdgeInsets) cellEdgeInsets;
@end
