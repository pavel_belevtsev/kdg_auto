//
//  APSDayRulerCell.m
//  Spas
//
//  Created by Rivka Peleg on 9/9/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSDayRulerCell.h"

@implementation APSDayRulerCell

- (void)awakeFromNib {
    // Initialization code
}


+(NSString *) nibName
{
    return NSStringFromClass([APSDayRulerCell class]);
}

+(CGSize ) cellSize
{
    return CGSizeMake(146, 48);
}

+(UIEdgeInsets) cellEdgeInsets
{
    return UIEdgeInsetsZero;
}





@end
