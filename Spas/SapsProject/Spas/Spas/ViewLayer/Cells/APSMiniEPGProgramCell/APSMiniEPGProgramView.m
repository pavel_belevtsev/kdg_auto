//
//  APSMiniEPGProgramCell.m
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMiniEPGProgramView.h"
#import "APSEpgMetaDataParser.h"
#import "APSEpgMetaDataView.h"

@interface APSMiniEPGProgramView()<UIScrollViewDelegate>
@property (retain, nonatomic) CAGradientLayer * gradient;

@end
@implementation APSMiniEPGProgramView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(APSMiniEPGProgramView *) miniEPGProgramView
{
    if(is_iPad())
    {
        return [APSMiniEPGProgramView miniEPGChannelCell_ipad];
    }
    else
    {
        return [APSMiniEPGProgramView miniEPGChannelCell_iPhone];
    }
}

+(APSMiniEPGProgramView *) miniEPGChannelCell_ipad
{
    APSMiniEPGProgramView *view = nil;
    NSString *nibName = @"APSMiniEPGProgramView";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

+(APSMiniEPGProgramView *) miniEPGChannelCell_iPhone
{
    APSMiniEPGProgramView *view = nil;
    NSString *nibName = @"APSMiniEPGProgramView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)awakeFromNib
{
    [self.seperatorView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    self.bgScrollView.contentSize=CGSizeMake(self.frame.size.width+100, 1000);
   // self.dataScrollView.size=CGSizeMake(self.dataScrollView.frame.size.width, 1000);
    self.dataScrollView.contentSize=CGSizeMake(self.dataScrollView.frame.size.width, 1000);
   if (is_iPad())
   {
       [self iPadUI];
   }
   else
   {
       [self iPhoneUI];
   }

}


-(void)iPadUI
{
    self.lblProgramTitle.font =[ APSTypography fontLightWithSize:22];
    self.lblProgramTitle.textColor = [UIColor whiteColor];
    self.lblProgramSubtitle.font =[ APSTypography fontLightWithSize:16];
    self.lblProgramSubtitle.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    
    self.lblTime.font = [APSTypography fontLightWithSize:16];
    self.lblTime.textColor = [APSTypography colorBrand];

}

-(void)iPhoneUI
{
    self.lblProgramTitle.font =[ APSTypography fontLightWithSize:18];
    self.lblProgramTitle.textColor = [UIColor whiteColor];
    self.lblProgramSubtitle.font =[ APSTypography fontLightWithSize:16];
    self.lblProgramSubtitle.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    
    self.lblTime.font = [APSTypography fontLightWithSize:14];
    self.lblTime.textColor = [APSTypography colorBrand];
}

-(void)setProgram:(APSTVProgram *)program
{
    _program = program;
    [self updateUIWithProgram:_program];
}

-(NSDictionary*)tempDada
{
    NSMutableDictionary * temp = [NSMutableDictionary dictionary];
    NSArray *actors = [NSArray arrayWithObjects:@"Israel Berezin",@"Rony Harpaz",@"Adi Mor", nil];
    NSString * season = @"1";
    NSString * director = @"Valery Guraviz";
    NSString * country = @"Israel";
    NSString * year = @"2014";
    NSString * genre = @"OTT Fronted";
    [temp setObject:season forKey:@"season"];
    [temp setObject:genre forKey:@"genre"];
    [temp setObject:actors forKey:@"actors"];
    [temp setObject:director forKey:@"director"];
    [temp setObject:country forKey:@"country"];
    [temp setObject:year forKey:@"year"];
    return temp;
}

-(void)updateUIWithProgram:(APSTVProgram *)program
{
    // clean master view.
//    for (UIView * view  in [self.dataScrollView.scrollView subviews])
    for (UIView * view  in [self.dataScrollView subviews])
    {
        [view removeFromSuperview];
    }
    
    // get other meta data.
    NSArray * orderKeys = [NSArray arrayWithObjects:@"genre",@"country",@"year",@"season", nil];
    NSArray *oneItemKayes = [NSArray arrayWithObjects:@"genre",nil];
    APSEpgMetaDataParser * mdp = [[APSEpgMetaDataParser alloc] initWithMetaData:program andFieldToShow:orderKeys andFieldToShowOnlyOneItem:oneItemKayes];
    NSDictionary * toShow = [mdp getAFieldToShowInApp];
    
    // title & sub & time
    self.lblProgramTitle.text = self.program.name;
     self.lblProgramSubtitle.text = @"";
    if ([program.metaData isKindOfClass:[NSArray class]])
    {
        for (NSDictionary * dic in program.metaData)
        {
            if ([[dic objectForKey:@"Key"] isEqualToString:@"subtitle"])
            {
                NSString* subtitle = [dic objectForKey:@"Value"];
                subtitle= [subtitle stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                self.lblProgramSubtitle.text = subtitle;
                ASLogInfo(@"%@",[dic objectForKey:@"Value"]);
                break;
            }
        }
    }
    
    self.lblTime.text = [APSEpgMetaDataView timeLblStingFormatWithStartTime:program.startDateTime EndTime:program.endDateTime];
    NSDictionary * temp = toShow;//[self tempDada];
    
    // time lbl color
    if ([APSEpgMetaDataView isProgramInCurrent:program])
    {
        self.lblTime.textColor = [APSTypography colorBrand];
    }
    else
    {
        self.lblTime.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    }
    
    // description
    self.descriptionView = nil;
    UIFont * descriptionTextFont =[APSTypography fontRegularWithSize:14]; //
    self.descriptionView = [APSEpgMetaDataView buildDescriptionViewWithText:program.programDescription andViewWidth:self.dataScrollView.width-20 andFont:descriptionTextFont andLineHeight:19.f andTextColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
    
    // other meta data
    NSInteger margin = 5;
    if (!is_iPad())
    {
        margin = 0;
    }
    self.metaDataView = nil;
    
    NSDictionary * UIDic = [self buildUiDictionary];
    self.metaDataView = [APSEpgMetaDataView buildUIWithMetaData:temp keysOrder:orderKeys andLayoutMargin:margin andUIDic:UIDic];
    
    NSArray * listOrderKeys = [NSArray arrayWithObjects:@"director",@"actors", nil];
    APSEpgMetaDataParser * listMdp = [[APSEpgMetaDataParser alloc] initWithMetaData:self.program andFieldToShow:listOrderKeys andFieldToShowOnlyOneItem:oneItemKayes];
    NSDictionary * listValuesToShow = [listMdp getAFieldToShowInApp];
    if ([[listValuesToShow allKeys] count] > 0)
    {
        [APSEpgMetaDataView buildUIListOnView:(TVLinearLayout *)self.metaDataView WithMetaData:listValuesToShow keysOrder:listOrderKeys andLayoutMargin:margin andUIDic:UIDic andListSize:4];
    }
    
    // data view init & setup
    
    NSInteger sapce = 15;
    if (is_iPad())
    {
        sapce = 23;
    }
    self.dataView = [[TVLinearLayout alloc] initWithFrame:CGRectMake(0, 0, self.dataScrollView.width, 1000)];
    self.dataView.orientation = TVLayoutOrientationVertical;
    self.dataView.margins = sapce;
    [self.dataView setBackgroundColor:[UIColor clearColor]];
    
    // clean data view
    for (UIView * view in [self.dataView subviews])
    {
        [view removeFromSuperview];
    }
    
    // add subviews to dataview
    [self.dataView addSubview:self.descriptionView];
    [self.dataView addSubview:self.metaDataView];
    NSInteger high =self.descriptionView.height + sapce + self.metaDataView.height;
    self.dataView.height = high;
//    self.dataScrollView.size = self.dataView.size;
    self.dataScrollView.contentSize = self.dataView.size;
    
    [self.dataScrollView addSubview:self.dataView];
    
    self.dataScrollView.layer.frame = self.dataScrollView.frame;// (CGRect){ { 10, 10 }, { 100, 100 } };
        
//   [self.dataScrollView.scrollView addSubview:self.dataView];
}

-(NSDictionary*)buildUiDictionary
{
    NSMutableDictionary * UIDic = [NSMutableDictionary dictionary];
    
    NSInteger viewWidth = self.dataScrollView.width;
    NSInteger lblTitleWidth = 100;
    UIFont * titleFont = [APSTypography fontRegularWithSize:12];
    UIFont * lblTextFont = [APSTypography fontRegularWithSize:12];
    BOOL lblTitleSizeToFit = YES;
    UIColor * lblTitleColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    UIColor * lblTextColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    float letterSpacing = 0.0;
    NSInteger multiLineHigh = 18;

    if (is_iPad())
    {
        lblTitleSizeToFit = NO;
        titleFont = [APSTypography fontRegularWithSize:10];
        letterSpacing = 1.5;
        multiLineHigh = 20;

    }
    
    [UIDic setObject:[NSNumber numberWithInt:viewWidth] forKey:MDVUIItemViewWidth];
    [UIDic setObject:[NSNumber numberWithInt:lblTitleWidth] forKey:MDVUIItemLblTitleWidth];
    [UIDic setObject:titleFont forKey:MDVUIItemLblTitleFont];
    [UIDic setObject:lblTextFont forKey:MDVUIItemLblTextFont];
    [UIDic setObject:[NSNumber numberWithBool:lblTitleSizeToFit] forKey:MDVUIItemLblTitleSizeToFit];
    [UIDic setObject:lblTitleColor forKey:MDVUIItemLblTitleColor];
    [UIDic setObject:lblTextColor forKey:MDVUIItemLblTextColor];
    [UIDic setObject:[NSNumber numberWithInteger:letterSpacing] forKey:MDVUIItemLblTitleLetterSpacing];
    
    [UIDic setObject:[NSNumber numberWithInteger:multiLineHigh] forKey:MDVUIItemViewMultiLineHigh];

    return [NSDictionary dictionaryWithDictionary:UIDic];
}

- (void)changeScrollBarColorFor:(UIScrollView *)scrollView
{
    for ( UIView *view in scrollView.subviews )
    {
        
        if (view.tag == 0 && [view isKindOfClass:UIImageView.class])
        {
           // NSLog(@" --------- %f - %f - %f - %f", view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            
            UIImageView *imageView = (UIImageView *)view;
            imageView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
            imageView.width = 3.0;
            imageView.layer.cornerRadius = 2.0;
        }
    }
}

#pragma mark - UIScrollViewDelegate -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self changeScrollBarColorFor:scrollView];
}





@end
