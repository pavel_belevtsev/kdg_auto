//
//  APSMiniEPGProgramCell.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVLinearLayout.h>
#import "APSScrollView.h"

@interface APSMiniEPGProgramView : UIView

+(APSMiniEPGProgramView *) miniEPGProgramView;

@property (nonatomic, strong) APSTVProgram * program;

@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@property (weak, nonatomic) IBOutlet UIScrollView *bgScrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *dataScrollView;

//@property (weak, nonatomic) IBOutlet APSScrollView *dataScrollView;


@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *lblProgramTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramSubtitle;

@property (strong, nonatomic) TVLinearLayout * dataView;
@property (strong, nonatomic)  UIView * descriptionView;
@property (strong, nonatomic ) UIView * metaDataView;
@end
