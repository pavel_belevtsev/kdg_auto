//
//  APSTimeIndicatorCell.h
//  Spas
//
//  Created by Rivka Peleg on 9/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSTimeIndicatorCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTime;


+(CGSize ) cellSize;
+(UIEdgeInsets )cellEdgeInsets;
+(NSString *) nibName;
@end
