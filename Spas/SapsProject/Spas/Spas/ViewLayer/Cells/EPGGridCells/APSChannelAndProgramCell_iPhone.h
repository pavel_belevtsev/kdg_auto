//
//  APSChannelAndProgramCell_iPhone.h
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
@class APSChannelAndProgramCell_iPhone;

@protocol APSChannelAndProgramCellDelegate <NSObject>

-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *) cell didselectChannel:(TVMediaItem *) mediaItem program:(APSTVProgram *) program;

-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *) cell didselectProgram:(APSTVProgram *) program channel:(TVMediaItem *) mediaItem;

-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *)cell didSelectFavorite:(UIButton *) favoriteButton toMediaItem:(TVMediaItem*)mediaItem;

@end


@interface APSChannelAndProgramCell_iPhone : UICollectionViewCell <UIScrollViewDelegate>



@property (weak, nonatomic) id<APSChannelAndProgramCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageChannel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFavorites;
@property (weak, nonatomic) IBOutlet UIImageView *loadIndicatorView;
@property (weak, nonatomic) IBOutlet UIView *programView;

@property (weak, nonatomic) IBOutlet UILabel *labelChannelNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelProgramName;
@property (weak, nonatomic) IBOutlet UILabel *labelProgramInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelPogramDuration;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIView *channelView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewFavorites;
@property (weak, nonatomic) IBOutlet UIButton *buttonHideFavorite;

@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;
@property (retain, nonatomic) TVMediaItem * channel;
@property (retain, nonatomic) APSTVProgram * program;

+(NSString *) nibName;
+(CGSize ) cellSize;
+(UIEdgeInsets ) cellEdgeInsets;

-(void)updateButtonHideFavoriteBackgroundColor;
-(void)updateUIThenSuccessFavoriteChange;

@end
