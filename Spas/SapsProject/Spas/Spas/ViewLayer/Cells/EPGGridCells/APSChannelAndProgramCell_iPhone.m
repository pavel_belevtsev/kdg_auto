//
//  APSChannelAndProgramCell_iPhone.m
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSChannelAndProgramCell_iPhone.h"
#import "UIImage+APSColoredImage.h"

@implementation APSChannelAndProgramCell_iPhone


-(void)prepareForReuse
{
    [super prepareForReuse];
    self.channel = nil;
    self.program = nil;
    self.imageChannel.image = nil;
}

- (void)awakeFromNib
{
    // Initialization code
    
    self.labelChannelNumber.font = [APSTypography fontRegularWithSize:12];
    self.labelChannelNumber.textColor = [UIColor colorWithWhite:153.0/255 alpha:1];
    
    self.labelProgramName.font = [APSTypography fontRegularWithSize:16];
    self.labelProgramName.textColor = [APSTypography colorMediumGray];
    
    self.labelProgramInfo.font = [APSTypography fontLightWithSize:14];
    self.labelProgramInfo.textColor = [APSTypography colorMediumGray];
    
    self.labelPogramDuration.font = [APSTypography fontLightWithSize:13];
    self.labelPogramDuration.textColor = [APSTypography colorMediumGray];
    
    UIImage * maximumImage = [[APSUtils imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    UIImage * minimumdImage = [[APSUtils imageWithColor:[APSTypography colorBrand] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    
    [self.progressView setProgressImage:minimumdImage];
    [self.progressView setTrackImage:maximumImage];
    
    self.progressView.progressTintColor = [APSTypography colorBrand];
    self.progressView.trackTintColor = [UIColor clearColor];
    
    self.scrollViewFavorites.contentSize = self.channelView.size;
    [self.scrollViewFavorites setContentOffset:CGPointMake(24, 0) animated:NO];
    

    UIImage * image = [UIImage imageNamed:@"favorite_star_mask_iphone"];
    UIImage * brandedImage1 = [UIImage coloredImageWithColor:[APSTypography colorBrand] bitMapImage:image];
    [self.buttonFavorite setImage:brandedImage1 forState:UIControlStateSelected];
    UIImage * brandedImage2 = [UIImage coloredImageWithColor:[UIColor clearColor] bitMapImage:image];
    [self.buttonFavorite setImage:brandedImage2 forState:UIControlStateNormal];
    
    UIImage * brandedImage3 = [UIImage coloredImageWithColor:[UIColor whiteColor] bitMapImage:image];
    [self.buttonHideFavorite setImage:brandedImage3 forState:UIControlStateSelected];
    UIImage * brandedImage4 = [UIImage coloredImageWithColor:[APSTypography colorLightGray] bitMapImage:image];
    [self.buttonHideFavorite setImage:brandedImage4 forState:UIControlStateNormal];
    
}

+(NSString *) nibName
{
    return NSStringFromClass([APSChannelAndProgramCell_iPhone class]);
}

+(CGSize ) cellSize
{
    return CGSizeMake(320, 82);
}

+(UIEdgeInsets ) cellEdgeInsets
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}



-(void)setChannel:(TVMediaItem *)channel
{
    if (_channel != channel)
    {
        _channel = channel;
        
        [self updateUIForChannel];
    }
}


-(void)setProgram:(APSTVProgram *)program
{
    if (_program != program)
    {
        _program = program;
    }
    [self updateUIForProgram];
}

-(void) updateUIForChannel
{
    self.labelChannelNumber.text = [self.channel.metaData objectOrNilForKey:chanenlIDKey];
    [self.imageChannel setImageWithURL:[self.channel pictureURLForSizeWithRetinaSupport:self.imageChannel.bounds.size] placeholderImage:nil];
}

-(void) updateUIForProgram
{
    if (self.program == nil)
    {
        self.programView.hidden = YES;
        
    }
    else
    {
        self.loadIndicatorView.hidden = YES;
        self.programView.hidden = NO;
        
        self.labelProgramName.text = self.program.name;
        NSString* programInfo = [self.program.theParsedMetaData objectForKey:@"subtitle"];
        self.labelProgramInfo.text = [programInfo stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        self.labelPogramDuration.text = [APSUtils stringTimeFromDate:self.program.startDateTime toDate:self.program.endDateTime];
    
        
        NSDate * now = [NSDate date];
        CGFloat timeFruction = [now timeIntervalSinceDate:self.program.startDateTime]/[self.program.endDateTime timeIntervalSinceDate:self.program.startDateTime];
        self.progressView.progress = timeFruction;

    }
}


- (IBAction)channelButtonSelected:(id)sender
{
    [self.delegate channelAndProgramCell:self didselectChannel:self.channel program:self.program];
}

- (IBAction)programButtonDidSelect:(id)sender
{
    [self.delegate channelAndProgramCell:self didselectProgram:self.program channel:self.channel];
}

#pragma mark - Favorites -

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    ASLogInfo(@"");
    if ([self.delegate respondsToSelector:@selector(channelAndProgramCell:didSelectFavorite:toMediaItem:)])
    {
        [self.delegate channelAndProgramCell:self didSelectFavorite:self.buttonHideFavorite toMediaItem:self.channel];
    }
}

// call if "channelAndProgramCell..." Success from iPhoneEPG
-(void)updateUIThenSuccessFavoriteChange
{
    [self updateButtonHideFavoriteBackgroundColor];
    
    self.buttonFavorite.alpha=0;
    
    self.buttonFavorite.selected = self.buttonHideFavorite.selected;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.buttonFavorite.alpha=1;
        
    } completion:^(BOOL finished) {
        
    }];

}

-(void)updateButtonHideFavoriteBackgroundColor
{
    if (self.buttonHideFavorite.selected)
    {
        [self.buttonHideFavorite setBackgroundColor:[APSTypography colorBrand]];
    }
    else
    {
        [self.buttonHideFavorite setBackgroundColor:[APSTypography colorDrakGray]];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.scrollViewFavorites setContentOffset:CGPointMake(24, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self.scrollViewFavorites setContentOffset:CGPointMake(24, 0) animated:YES];
}

@end
