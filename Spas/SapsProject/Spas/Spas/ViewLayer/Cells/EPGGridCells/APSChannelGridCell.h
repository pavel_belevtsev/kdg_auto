//
//  YESChannelCell.h
//  YES_iPad
//
//  Created by Rivka S. Peleg on 5/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewAligned.h"

@class RSHorizontalTableView;
@class APSChannelGridCell;

@protocol YESChannelCellDelegate <NSObject>

-(void) channelCell:(APSChannelGridCell *) sender didSelectShareForProgramAtIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex;
-(void) channelCell:(APSChannelGridCell *)cell didSelectBookMark:(UIButton *) bookMarkButton;
-(void) channelCell:(APSChannelGridCell *)cell didSelectFavorite:(UIButton *) favoriteButton toMediaItem:(TVMediaItem*)mediaItem;

-(void) channelCell:(APSChannelGridCell *) cell userDidSelectProgramIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex;
-(void) channelCell:(APSChannelGridCell *) cell userDidUnselectProgramIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex;
-(void) channelCell:(APSChannelGridCell *)cell userDidScrollToContentOffset:(NSInteger) xValue;
-(void) channelCellDidEndScroll:(APSChannelGridCell *)cell ;
-(void) channelCellDidstartScroll:(APSChannelGridCell *)cell ;
-(void) channelCellDidSelect:(APSChannelGridCell *)cell channelIndex:(NSInteger) channelIndex ;
-(void) channelCellDidSelectPlayChannel:(APSChannelGridCell *)cell;
-(void) channelCell:(APSChannelGridCell *)cell didSelectSetAReminder:(UIButton *)reminderButton channelIndex:(NSInteger ) channelIndex programIndex:(NSInteger) programIndex;

-(void) channelCell:(APSChannelGridCell *)cell didSelectRecoredButton:(UIButton *) sender channelIndex:(NSInteger ) channelIndex programIndex:(NSInteger) programIndex;

-(void) channelCell:(APSChannelGridCell *)cell didScrollSignificantChangeOfChannel:(TVMediaItem *) channel;

@end



@interface APSChannelGridCell : UITableViewCell

//delegate

@property (assign, nonatomic) id<YESChannelCellDelegate> delegate;

//channel info
@property (retain, nonatomic) IBOutlet UIImageViewAligned *imageChannelIcon;
@property (retain, nonatomic) IBOutlet UILabel *labelChannelNumber;
@property (retain, nonatomic) IBOutlet UIButton *buttonBookMark;

@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;

// channel programs
@property (retain, nonatomic) IBOutlet RSHorizontalTableView *horizontalTableView;
@property (retain, nonatomic) NSArray * cellsProperties ;
@property (retain, nonatomic) NSArray * arrayPrograms;
@property (assign, nonatomic) CGFloat widthOfOneMinute;
@property (assign, nonatomic) NSInteger integerTimeOffset;

@property (strong, nonatomic) TVMediaItem * mediaItem;
// handle selection
@property (assign, nonatomic) NSInteger selectedProgram;

@property (assign, nonatomic) CGFloat significantChangeWidth;

@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void) setChannel:(TVMediaItem *) mediaItem;

- (IBAction)favoritesPressed:(id)sender;
@end
