//
//  APSTimeIndicatorCell.m
//  Spas
//
//  Created by Rivka Peleg on 9/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTimeIndicatorCell.h"

@implementation APSTimeIndicatorCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.labelTime.font = [APSTypography fontRegularWithSize:13];
    self.labelTime.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    
}


+(CGSize ) cellSize
{
    return CGSizeZero;
}

+(UIEdgeInsets )cellEdgeInsets
{
    return UIEdgeInsetsZero;
}

+(NSString *) nibName
{
    return  NSStringFromClass([APSTimeIndicatorCell class]);
}
@end
