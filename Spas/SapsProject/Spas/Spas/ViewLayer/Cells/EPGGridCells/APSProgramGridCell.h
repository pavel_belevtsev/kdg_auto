//
//  YESProgramCell.h
//  YES_iPad
//
//  Created by Rivka S. Peleg on 5/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "RSHorizontalTableViewCell.h"

@class APSProgramGridCell;
@class YESProgressView;

@protocol YESProgramCellDelegate <NSObject>
-(void) didUnselectProgram :(APSProgramGridCell *) programCell  ;
-(void) programCellDidSelectWatchOnLive:(APSProgramGridCell *) programCell;
-(void) programCellDidSelectWatchOnVOD:(APSProgramGridCell *) programCell;
-(void) programCellDidSelectWatchTrailer:(APSProgramGridCell *) programCell;
-(void) programCell:(APSProgramGridCell *) programCell didSelectShareWithSender:(UIButton *) sender;
-(void) programCell:(APSProgramGridCell *)programCell didSelectSetAReminder:(UIButton*) reminderButton;
-(void) programCell:(APSProgramGridCell *)programCell didSelectRecoredButton:(UIButton *) sender;



@end

@interface APSProgramGridCell : RSHorizontalTableViewCell


@property (assign, nonatomic) IBOutlet id<YESProgramCellDelegate> delegate ;
@property (retain, nonatomic) IBOutlet UILabel *labelProgramDescription;
@property (retain, nonatomic) IBOutlet UILabel *labelProgramTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelProgramTime;
@property (retain, nonatomic) IBOutlet UIProgressView *progressView;

@property (retain, nonatomic) IBOutlet TVLinearLayout *buttonLayout;


-(void) setupCellWithProgram:(APSTVProgram *) prog selected:(BOOL) selected;
- (IBAction)share:(UIButton *)sender;

@end
