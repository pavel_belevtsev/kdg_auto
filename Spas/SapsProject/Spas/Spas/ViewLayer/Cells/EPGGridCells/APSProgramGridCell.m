//
//  YESProgramCell.m
//  YES_iPad
//
//  Created by Rivka S. Peleg on 5/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "APSProgramGridCell.h"

@interface APSProgramGridCell ()

@property (nonatomic, strong) IBOutlet UIButton *buttonLike;

@end

@implementation APSProgramGridCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/




-(void)awakeFromNib
{
    [super awakeFromNib];
    
    UIImage * maximumImage = [[APSUtils imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    UIImage * minimumdImage = [[APSUtils imageWithColor:[APSTypography colorBrand] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    
    [self.progressView setProgressImage:minimumdImage];
    [self.progressView setTrackImage:maximumImage];
    
    self.progressView.progressTintColor = [APSTypography colorBrand];
    self.progressView.trackTintColor = [UIColor clearColor];
    
//    if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
//    {
//        self.progressView.y+=7;
//    }
    
   
}

#pragma mark - touch blocker delegate methods

-(void) outerTouchWasReceived
{
    [self.delegate didUnselectProgram:self];
}



-(void) setupCellWithProgram:(APSTVProgram *) prog selected:(BOOL) selected
{
    BOOL hasMediaItem  = YES;
    if(prog.mediaID ==nil || [prog.mediaID isEqualToString:@""])
    {
        hasMediaItem = NO;
    }
    
    self.labelProgramTitle.text = prog.name;
    self.labelProgramTitle.font = [APSTypography fontRegularWithSize:16];
    self.labelProgramTitle.textColor = [APSTypography colorDrakGray];
    
    self.labelProgramDescription.text = [prog decodedValueForKey:@"subtitle"];
    self.labelProgramDescription.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    self.labelProgramDescription.font = [APSTypography fontRegularWithSize:13];
    
    [self.labelProgramTime setText: [NSString stringWithFormat:@"%@",[APSUtils stringTimeFromDate:prog.startDateTime toDate:prog.endDateTime]]];
    self.labelProgramTime.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    self.labelProgramTime.font = [APSTypography fontRegularWithSize:13];
    

    NSDate * now = [NSDate date];
    //now
    if([now compare:prog.startDateTime] == NSOrderedDescending && [now compare:prog.endDateTime] == NSOrderedAscending)
    {
        self.progressView.hidden = NO;
        CGFloat timeFruction = [now timeIntervalSinceDate:prog.startDateTime]/[prog.endDateTime timeIntervalSinceDate:prog.startDateTime];
        self.progressView.progress = timeFruction;

    }
    //previuse
    else if ([prog.endDateTime compare:now] == NSOrderedAscending )
    {
        self.progressView.hidden = YES;

    }
    //next
    else
    {
        self.progressView.hidden = YES;
        
    }
    
}

    


#pragma mark  actions

- (IBAction)share:(UIButton *)sender {
    [self.delegate programCell:self didSelectShareWithSender:sender];
}


- (IBAction)setAReminderButtonClicked:(UIButton *)sender
{
    //[self.delegate programCell:self didSelectSetAReminder:sender];
}

- (IBAction)recoredButtonPressed:(id)sender
{
    [self.delegate programCell:self didSelectRecoredButton:sender];
}


- (IBAction)watchProgramInLive:(id)sender {
   
    [self.delegate programCellDidSelectWatchOnLive:self];
}

- (IBAction)closeAdditionalInfo:(id)sender {
   
    [self.delegate didUnselectProgram:self];
}




@end
