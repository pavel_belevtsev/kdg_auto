
//
//  YESChannelCell.m
//  YES_iPad
//
//  Created by Rivka S. Peleg on 5/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "APSChannelGridCell.h"
#import "APSProgramGridCell.h"
#import "NSString+Time.h"
#import "RSHorizontalTableView.h"
#import "RSHorizontalTableViewCellProperties.h"
#import "UIImage+APSColoredImage.h"


#define CONTENT_WIDTH   self.widthOfOneMinute*24.0*60.0*7
#define BIG_WIDH 390

@interface APSChannelGridCell ()<RSHorizontalTableViewDelegate,RSHorizontalTableViewDataSource,YESProgramCellDelegate>


@property (retain, nonatomic) NSDate * dateReference ;
@property (assign, nonatomic) CGPoint  lastOffset;


 

@end

@implementation APSChannelGridCell

#pragma mark - deallocation

-(void) dealloc
{
    self.delegate = nil;
    self.horizontalTableView.delegate = nil;
}

#pragma mark - initlization 

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        [self setup];
    }
    
    return self;
}

-(void) setup
{
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit);
    self.dateReference = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:date]];
    
    
    
    self.selectedProgram  =-1;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - life sycle
-(void)awakeFromNib
{
    [super awakeFromNib];
    UIImage * image = [UIImage imageNamed:@"favorite_star_mask_ipad"];
    UIImage * brandedImage1 = [UIImage coloredImageWithColor:[APSTypography colorBrand] bitMapImage:image];
    [self.buttonFavorite setImage:brandedImage1 forState:UIControlStateSelected];
    UIImage * brandedImage2 = [UIImage coloredImageWithColor:[APSTypography colorLightGray] bitMapImage:image];
    [self.buttonFavorite setImage:brandedImage2 forState:UIControlStateNormal];
}


#pragma mark - setter


-(void) setIntegerTimeOffset:(NSInteger)integerTimeOffset
{
    if (!self.horizontalTableView.dragging && !self.horizontalTableView.decelerating)
    {
        
    if(_integerTimeOffset != integerTimeOffset)
    {
        _integerTimeOffset = integerTimeOffset;
        
        [self.horizontalTableView setContentOffset:CGPointMake(integerTimeOffset, 0)];
    }
        
    }
    else
    {
        NSLog(@"scroll view is bussy to stop");
    }
}


#pragma mark - RSHorizontal Table View Data Source

- (NSInteger)horizontalTableViewNumberOfColumns:(RSHorizontalTableView *)tableView
{
    return self.arrayPrograms.count;
}

- (RSHorizontalTableViewCell *)HorizontalTableView:(RSHorizontalTableView *)tableView cellForColumnAtIndex: (NSInteger)index
{
    
    NSString * identifier = NSStringFromClass([APSProgramGridCell class]);
    APSProgramGridCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil)
    {
        NSArray * array = [[NSBundle mainBundle] loadNibNamed:identifier owner:nil options:nil];
        for (id object in array)
        {
            if ([object isKindOfClass:[APSProgramGridCell class]])
            {
                cell = object;
                break;
            }
        }

        //cell = [[[NSBundle mainBundle] loadNibNamed:identifier owner:nil options:nil] lastObject];
    }
    
    cell.delegate = self;
    APSTVProgram* prog = self.arrayPrograms[index];
    [cell setupCellWithProgram:prog selected:(index==self.selectedProgram)];
    return cell;

}

#pragma mark - RSHorizontal Table View Delegate

- (CGFloat)horizontalTableView:(RSHorizontalTableView *)tableView widthForColumnAtIndex:(NSInteger)index
{
    return [self calculateWidthOfProgramatIndex:index];
}

- (CGFloat)horizontalTableView:(RSHorizontalTableView *)tableView xOriginForColumnAtIndex:(NSInteger)indexPath
{
   return [self calculatexOriginOfProgramatIndex:indexPath];
}

- (CGFloat) contentWidthForHorizontalTableView:(RSHorizontalTableView *)tableView
{
    return CONTENT_WIDTH;
}


-(NSArray *) horizontalTableViewCellsProperties:(RSHorizontalTableView *)tableView
{
    return self.cellsProperties;
}

- (void)horizontalTableView:(RSHorizontalTableView *)tableView didSelectColumnAtIndex:(NSInteger )indexPath
{
    
    [self.delegate channelCell:self userDidSelectProgramIndex:indexPath channelIndex:self.tag];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (self.horizontalTableView.dragging || self.horizontalTableView.decelerating)
    {
        if([self.delegate respondsToSelector:@selector(channelCell:userDidScrollToContentOffset:)])
            [self.delegate channelCell:self userDidScrollToContentOffset:scrollView.contentOffset.x] ;
    }
    
    if (fabsf(scrollView.contentOffset.x-self.lastOffset.x)>self.significantChangeWidth)
    {
        self.lastOffset = scrollView.contentOffset;
        [self.delegate channelCell:self didScrollSignificantChangeOfChannel:self.mediaItem];
    }

    
}


-(void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector:@selector(channelCellDidstartScroll:)])
        [self.delegate channelCellDidstartScroll:self];

}
-(void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{

        if([self.delegate respondsToSelector:@selector(channelCellDidEndScroll:)])
            [self.delegate channelCellDidEndScroll:self];

}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector:@selector(channelCellDidEndScroll:)])
        [self.delegate channelCellDidEndScroll:self];
}

-(void) scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector:@selector(channelCellDidstartScroll:)])
        [self.delegate channelCellDidstartScroll:self];
    
}


-(void)scrollViewDidScrollProgrammatically:(UIScrollView *)scrollView
{
    if([self.delegate respondsToSelector:@selector(channelCell:userDidScrollToContentOffset:)])
        [self.delegate channelCell:self userDidScrollToContentOffset:scrollView.contentOffset.x] ;
}

#pragma mark - width handeling
-(CGFloat) calculateWidthOfProgramatIndex:(NSInteger) index {

    if(self.selectedProgram == index)
    {
        return BIG_WIDH;
    }
    else
    {
        APSTVProgram* prog = self.arrayPrograms[index];
        NSInteger progLength = ([prog.endDateTime timeIntervalSinceDate:prog.startDateTime] / 60);
        CGFloat width = progLength * self.widthOfOneMinute;
        return width;
    }
	
}

-(CGFloat) calculatexOriginOfProgramatIndex:(NSInteger) index
{
    APSTVProgram* prog = self.arrayPrograms[index];
    
    NSInteger distanceSinceReferenceDateToEndOfProg = [prog.endDateTime timeIntervalSinceDate:self.dateReference];
    CGFloat xOrigin = CONTENT_WIDTH-distanceSinceReferenceDateToEndOfProg/60*self.widthOfOneMinute;
    
    
    if(self.selectedProgram == -1 )
    {
        return xOrigin;

    }
    else
    {
        if(self.selectedProgram == index)
        {
            return xOrigin;
        }
        else
        {
            APSTVProgram* selectedProg = self.arrayPrograms[self.selectedProgram];
            CGFloat originalWidth = ([selectedProg.endDateTime timeIntervalSinceDate:selectedProg.startDateTime]/60)*(self.widthOfOneMinute);
            CGFloat selectedProgramDifferenceFromOriginalWidth  = BIG_WIDH - originalWidth;
            
            if([prog.endDateTime compare:selectedProg.startDateTime] == NSOrderedAscending || [prog.endDateTime compare:selectedProg.startDateTime] == NSOrderedSame )
            {
                xOrigin+= selectedProgramDifferenceFromOriginalWidth;
                return xOrigin;
            }
            else
            {
                return xOrigin;
            }
        }
    }
    
}

#pragma mark - YES Program Cell Delegate methods

-(void)programCell:(APSProgramGridCell *)programCell didSelectShareWithSender:(UIButton *)sender
{
    NSInteger programIndex = [self.horizontalTableView indexPathForVisableCell:programCell];
    if (programIndex != NSNotFound)
    {
        [self.delegate channelCell:self
   didSelectShareForProgramAtIndex:programIndex
                      channelIndex:self.tag];
    }
}

-(void)programCell:(APSProgramGridCell *)programCell didSelectSetAReminder:(UIButton *)reminderButton
{
    NSInteger indexOfCell = [self.horizontalTableView indexPathForVisableCell:programCell];
    
    if(indexOfCell != NSNotFound)
    {
        [self.delegate channelCell:self didSelectSetAReminder:reminderButton channelIndex:self.tag programIndex:indexOfCell];
    }
}

-(void) didUnselectProgram :(APSProgramGridCell *) programCell
{
    [self.delegate channelCell:self userDidUnselectProgramIndex:self.selectedProgram channelIndex:self.tag];
}

-(void) programCellDidSelectWatchOnLive:(APSProgramGridCell *) programCell
{
    
    [self.delegate channelCellDidSelectPlayChannel:self];
    
}

-(void) programCellDidSelectWatchOnVOD:(APSProgramGridCell *) programCell
{
    NSInteger indexOfCell = [self.horizontalTableView indexPathForVisableCell:programCell];
    
    if(indexOfCell != NSNotFound)
    {
        // playMedia Item
    }

}

-(void) programCellDidSelectWatchTrailer:(APSProgramGridCell *) programCell
{
    NSInteger indexOfCell = [self.horizontalTableView indexPathForVisableCell:programCell];
    
    if(indexOfCell != NSNotFound)
    {
        // playMedia Item
    }

}


-(void) programCell:(APSProgramGridCell *)programCell didSelectRecoredButton:(UIButton *) sender
{
    NSInteger programIndex = [self.horizontalTableView indexPathForVisableCell:programCell];
    if ( programIndex != NSNotFound)
    {
        [self.delegate channelCell:self didSelectRecoredButton:sender channelIndex:self.tag programIndex:programIndex];
    }
    
}


#pragma mark - book marks

- (IBAction)bookmarkSelected:(UIButton *)sender
{
    [self.delegate channelCell:self didSelectBookMark:sender];
}



- (IBAction)didSelectChannel:(id)sender {
    
    [self.delegate channelCellDidSelect:self channelIndex:self.tag];
}




-(void) setChannel:(TVMediaItem *) channel
{
    self.mediaItem = channel;
    self.backgroundColor = [UIColor clearColor];
    
    NSURL * imageURL = [channel pictureURLForSizeWithRetinaSupport:self.imageChannelIcon.frame.size];
    [self.imageChannelIcon setImageWithURL:imageURL];

     self.labelChannelNumber.text = [NSString stringWithFormat:@"%@ %@",[channel.metaData objectForKey:chanenlIDKey],[channel.name uppercaseString]];
    self.labelChannelNumber.font = [APSTypography fontRegularWithSize:12];
    self.labelChannelNumber.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    self.labelChannelNumber.attributedText = [APSTypography attributedString:self.labelChannelNumber.text withLetterSpacing:0.5];
}

- (IBAction)favoritesPressed:(id)sender
{
    UIButton * btn = (UIButton*)sender;
    [self.delegate channelCell:self didSelectFavorite:btn toMediaItem:self.mediaItem];
}




@end
