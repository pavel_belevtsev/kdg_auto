//
//  APSAccountBaseCell.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSAccountBaseCell : UITableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andOffset:(CGFloat) offset;

@end
