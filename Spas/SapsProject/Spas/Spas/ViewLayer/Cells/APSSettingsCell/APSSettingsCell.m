//
//  APSSettingsCell.m
//  Spas
//
//  Created by Israel Berezin on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSettingsCell.h"

@implementation APSSettingsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(APSSettingsCell *) settingsCell
{
    APSSettingsCell *view = nil;
    NSString *nibName = NSStringFromClass(self);
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

+(APSSettingsCell *) iPhoneSettingsCell
{
    APSSettingsCell *view = nil;
    NSString *nibName = @"APSSettingsCell_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
        [self setBackgroundColor:[APSTypography colorTouchFeedbackBackground]];
    }
    else
    {
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    
}
@end
