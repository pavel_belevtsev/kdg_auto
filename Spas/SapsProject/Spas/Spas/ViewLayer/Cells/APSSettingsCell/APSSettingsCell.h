//
//  APSSettingsCell.h
//  Spas
//
//  Created by Israel Berezin on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSSettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIView *customSeparator;
@property (weak, nonatomic) IBOutlet UIImageView *disclaimerArrow;
@property (weak, nonatomic) IBOutlet UIView *seperatorLine;


+(APSSettingsCell *) iPhoneSettingsCell;
+(APSSettingsCell *) settingsCell;
@end
