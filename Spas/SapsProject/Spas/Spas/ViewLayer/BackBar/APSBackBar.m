//
//  APSBackBar.m
//  Spas
//
//  Created by Israel Berezin on 8/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBackBar.h"

@implementation APSBackBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(APSBackBar *) backBar
{
    NSString * className = nil;
    
    if (is_iPad())
    {
        className  = NSStringFromClassWithiPadSuffix([APSBackBar class]);
    }
    else
    {
        className = NSStringFromClassWithiPhoneSuffix([APSBackBar class]);
    }
    
    APSBackBar * topBar = [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:nil] lastObject];
    return topBar;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.labelTitle.textColor = [APSTypography colorBrand];
    self.labelTitle.font = [APSTypography fontRegularWithSize:17];
    
    self.btnBack.titleLabel.font = [APSTypography fontRegularWithSize:14];
    self.btnBack.titleLabel.textColor = [APSTypography colorLightGray];
    
    if (is_iPad())
    {
        [self.btnBack setTitle:[NSLocalizedString(@"BACK", nil) uppercaseString] forState:UIControlStateNormal];
        self.labelTitle.textColor = [APSTypography colorLightGray];
        self.labelTitle.font =  [APSTypography fontLightWithSize:20];
    }
}

-(void)addBottomShadow
{
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 2);
    shadow.startPoint = CGPointMake(1.0, 0.0);
    shadow.endPoint = CGPointMake(1.0, 1.0);
    shadow.colors = [NSArray arrayWithObjects:(id)[[[UIColor blackColor] colorWithAlphaComponent:0.2] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    [self.layer addSublayer:shadow];
    self.layer.masksToBounds = NO;
}


@end
