//
//  APSBackBar.h
//  Spas
//
//  Created by Israel Berezin on 8/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSTopBar.h"

@interface APSBackBar : APSTopBar

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

+(APSBackBar *) backBar;



@end
