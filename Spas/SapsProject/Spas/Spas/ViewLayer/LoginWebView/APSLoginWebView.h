//
//  APSLoginWebView.h
//  Spas
//
//  Created by Israel Berezin on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface APSLoginWebView : UIView <UIWebViewDelegate>


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *webViewActivityIndicator;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIView *webViewHeader;

@property (weak, nonatomic) IBOutlet UIButton *webViewBackButton;

@property (weak, nonatomic) IBOutlet UILabel *webViewHeaderTitle;

@property (assign, nonatomic) id<BaseViewControllerDelegate>  baseViewControllerDelegate;

+(APSLoginWebView *)loginWebView;

-(void)setupWebViewWithLink:(NSURL *)webLink andPageTitle:(NSString *)title;

-(void)showLoginWebViewOnView:(UIView *)view;
@end
