//
//  APSLoginWebView.m
//  Spas
//
//  Created by Israel Berezin on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSLoginWebView.h"

@implementation APSLoginWebView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(APSLoginWebView *)loginWebView
{
    if (is_iPad())
    {
        return [APSLoginWebView loginWebView_iPad];
    }
    else
    {
        return [APSLoginWebView loginWebView_iPhone];
    }
}

+(APSLoginWebView *) loginWebView_iPad
{
    APSLoginWebView *view = nil;
    NSString *nibName = @"APSLoginWebView_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

+(APSLoginWebView *) loginWebView_iPhone
{
    APSLoginWebView *view = nil;
    NSString *nibName = @"APSLoginWebView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    if (is_iPad())
    {
        self.webViewHeaderTitle.textColor = [APSTypography colorLightGray];
        self.webViewHeaderTitle.font = [APSTypography fontLightWithSize:20];
        self.webViewBackButton.titleLabel.font = [APSTypography fontRegularWithSize:14];
        self.webViewBackButton.titleLabel.textColor = [APSTypography colorLightGray];
        [self.webViewBackButton setTitle:[NSLocalizedString(@"Back", nil) uppercaseString] forState:UIControlStateNormal];
    }
    else
    {
        self.webViewHeaderTitle.textColor = [APSTypography colorBrand];
        self.webViewHeaderTitle.font = [APSTypography fontRegularWithSize:17];
    }

    [self setBackgroundColor:[APSTypography colorVeryLightGray]];
    
    [self addBottomShadow];

}

#pragma mark - webView -

-(void)setupWebViewWithLink:(NSURL *)webLink andPageTitle:(NSString *)title
{
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:webLink];
    self.webView.delegate = self;
    [self.webView loadRequest:requestObj];
    self.webViewHeaderTitle.text = title;
    self.webViewHeaderTitle.attributedText = [APSTypography attributedString:title withLetterSpacing:1.5];
}

-(void)addBottomShadow
{
    CAGradientLayer *shadow = [CAGradientLayer layer];
    shadow.frame = CGRectMake(0, self.webViewHeader.frame.size.height-1, self.webViewHeader.frame.size.width, 2);
    shadow.startPoint = CGPointMake(1.0, 0.0);
    shadow.endPoint = CGPointMake(1.0, 1.0);
    shadow.colors = [NSArray arrayWithObjects:(id)[[[UIColor blackColor] colorWithAlphaComponent:0.2] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    [self.webViewHeader.layer addSublayer:shadow];
    self.webViewHeader.layer.masksToBounds = NO;
}

-(void)showLoginWebViewOnView:(UIView *)view
{

    self.origin = CGPointMake(view.size.width , 0);

    [view addSubview:self];

    [UIView animateWithDuration:0.3 animations:^{
        self.origin = CGPointMake(0, 0);
        [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleDefault];
        
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)closeWebViewWindow:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        self.origin = CGPointMake(self.size.width, 0);
        [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleLightContent];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.webViewActivityIndicator.alpha = 1.0;
    self.webViewActivityIndicator.hidden = NO;
    [self.webViewActivityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.webViewActivityIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.webViewActivityIndicator stopAnimating];
}

@end
