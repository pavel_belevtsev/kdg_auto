//
//  APSZappingCoachMarks.m
//  Spas
//
//  Created by Israel Berezin on 11/11/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSZappingCoachMarks.h"

@implementation APSZappingCoachMarks

+(APSZappingCoachMarks *) zappingCoachMarks
{
    
    APSZappingCoachMarks *view = nil;
    NSString *nibName = @"APSZappingCoachMarks_iPhone";
    if (is_iPad())
    {
        nibName = @"APSZappingCoachMarks_iPad";
    }
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.7]];
    return view;
}


-(void)awakeFromNib
{
    [self setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.7]];
    if (is_iPad())
    {
        self.lblText.font = [APSTypography fontRegularWithSize:24];
    }
    else
    {
        self.lblText.font = [APSTypography fontRegularWithSize:16];
    }
    self.lblText.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    self.lblText.text = NSLocalizedString(@"Swipe up and down to browes through channel", nil);
}

-(void)showViewOnSuperView:(UIView *) superView withCompletion:(void(^)(void)) completion
{
    self.alpha = 0;
    [superView addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha =1.0;
    }
    completion:^(BOOL finished)
    {
        [self saveViewIsShow];
        if (completion)
        {
            completion();
        }
    }];
}

+(BOOL)needToShowView
{
    return ![[[NSUserDefaults standardUserDefaults] objectForKey:@"ZappingCoachMarksShow"] boolValue];
   // return YES;
}

-(void)saveViewIsShow
{
    [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithBool:YES] forKey:@"ZappingCoachMarksShow"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)dissmis:(id)sender
{
    self.alpha = 1.0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha =0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];

    if ([self.delegate respondsToSelector:@selector(zappingCoachMarks:didDismiss:)])
    {
        [self.delegate zappingCoachMarks:self didDismiss:sender];
    }
}


@end
