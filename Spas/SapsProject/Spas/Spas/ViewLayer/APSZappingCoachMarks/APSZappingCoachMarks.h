//
//  APSZappingCoachMarks.h
//  Spas
//
//  Created by Israel Berezin on 11/11/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>


@class APSZappingCoachMarks;

@protocol APSZappingCoachMarksDelegate <NSObject>

-(void) zappingCoachMarks:(APSZappingCoachMarks *) zappingCoachMarks didDismiss:(UIButton *) sender;

@end

@interface APSZappingCoachMarks : UIView

+(APSZappingCoachMarks *) zappingCoachMarks;

@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (nonatomic, assign) id<APSZappingCoachMarksDelegate> delegate;
-(void)showViewOnSuperView:(UIView *) superView withCompletion:(void(^)(void)) completion;

- (IBAction)dissmis:(id)sender;
+(BOOL)needToShowView;

@end
