//
//  APSTooltip.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTooltip.h"
#import "APSEventMonitorWindow.h"

@interface APSTooltip()

@property CGFloat offsetLabel;
@property CGRect  targetPosition;
@property CGPoint  originPoint;

@end

@implementation APSTooltip


+ (APSTooltip *) toolTip
{
    // load APSTooltip from xib
    NSArray * views =nil;
    APSTooltip * tooltip = nil;
    views =  [[NSBundle mainBundle] loadNibNamed:@"APSTooltip" owner:nil options:nil];
    
    for (UIView  * view in views)
    {
        if ([view isKindOfClass:[APSTooltip class]])
        {
            tooltip = (APSTooltip *)view;
            break;
        }
    }
    return tooltip;
}


- (void) showToolTipOnView:(UIView*) containingView atOrigin:(CGPoint) origin withCompletion:(void(^)(void)) completion
{
    [self setupBorderAndShadow];
    self.originPoint = origin;
    [self calcTargetPosition];
    self.y -=10;
    
    [containingView addSubview:self];

    UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseInOut;

    [UIView animateWithDuration:3.0 delay:0.0 usingSpringWithDamping:0.1 initialSpringVelocity:30 options:options animations:^{
        self.frame = self.targetPosition;
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }

    }];
}


- (IBAction)onPressButton:(id)sender {
    [self dismiss:nil];
}

-(void) calcTargetPosition
{
    // arrow head relative to self.
    CGPoint arrowHeadRelativeToSelf = CGPointMake(self.arrow.centerX ,
                                                  self.arrow.y+ self.arrow.height);

    // originPoint hold the new desired location of the arrow head.
    // Now calc the new origin
    CGPoint arrowHeadRelativeToContainer = self.originPoint;
    arrowHeadRelativeToContainer.x -= arrowHeadRelativeToSelf.x;
    arrowHeadRelativeToContainer.y -= arrowHeadRelativeToSelf.y;

    self.x = arrowHeadRelativeToContainer.x;
    self.y = arrowHeadRelativeToContainer.y;
    self.targetPosition = self.frame;
}

-(void)setupBorderAndShadow
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.layer.cornerRadius = 4.0;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    
//    // border
    [self.contentView.layer setBorderColor:[UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0].CGColor];
    [self.contentView.layer setBorderWidth:1.0f];
    
    // drop shadow
    [self.contentView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.contentView.layer setShadowOpacity:0.2];
    [self.contentView.layer setShadowRadius:4.0];
    [self.contentView.layer setShadowOffset:CGSizeMake(0, 0)];

    self.labelTooltip.textColor = [APSTypography colorMediumGray];
    self.labelTooltip.font = [APSTypography fontRegularWithSize:16];
    
    // register for "tooltip" behavior
    APSEventMonitorWindow* window = (APSEventMonitorWindow*)[UIApplication sharedApplication].delegate.window;
    [window.arrayMonitoredViews addObjectsFromArray:self.subviews];
    [window.arrayIgnoreViews addObject:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(dismiss:) name:kTouchEventOutsideOfMonitoredView object:nil];

}

- (void) dismiss:(NSNotification *) notification
{
    APSEventMonitorWindow* window = (APSEventMonitorWindow*)[UIApplication sharedApplication].delegate.window;
    [window.arrayMonitoredViews removeAllObjects];
    [window.arrayMonitoredViews removeAllObjects];

    if(self.delegate)
    {
        [self.delegate toolTip:self didDismiss:self];
    }
    [self removeFromSuperview];

}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
