//
//  APSTooltip.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSTooltip;

@protocol APSTooltipDelegate <NSObject>

-(void) toolTip:(APSTooltip *) tooltip didDismiss:(id) sender;

@end


@interface APSTooltip : UIView

@property (nonatomic, assign) id<APSTooltipDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *labelTooltip; // the label frame defined the frame of the tooltip
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIView *contentView;

+ (APSTooltip *) toolTip;

- (void) showToolTipOnView:(UIView*) containingView atOrigin:(CGPoint) origin withCompletion:(void(^)(void)) completion;

- (IBAction)onPressButton:(id)sender;

@end

