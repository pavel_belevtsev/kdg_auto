//
//  APSPlayerBar.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSPlayerBar.h"
#import "APSPurePlayerViewController.h"
#import "GCAgent.h"
#import "APSBrandedColorImageView.h"
#import "APSEventMonitorWindow.h"
#import "APSSessionManager.h"


@interface APSPlayerBar ()
@property (retain, nonatomic) GCAgent * timerAgentStreaming;
@property (retain, nonatomic) NSTimer * timerStreamingTimeChanged;
@property (nonatomic)  CGRect labelOriginalframe ;
@property (assign, nonatomic) CGFloat lastPitchScale;
@end

@implementation APSPlayerBar

-(void)dealloc
{
    [self.timerStreamingTimeChanged invalidate];
    self.timerStreamingTimeChanged = nil;
    [self unregisterForNotification];
}

+(APSPlayerBar *) playBarOfType:(PlayerBarType ) type 
{

    APSPlayerBar * playerBar = nil;
    
    if (type == PlayerBarType_FullScreen)
    {
        if (is_iPad())
        {
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"APSPlayerBar_full_screen_iPad" owner:nil options:nil];
            
            for (id nibObject in nibObjects) {
                if ([nibObject isKindOfClass:[APSPlayerBar class]]) {
                    playerBar = nibObject;
                }
            }

        }
        else
        {
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"APSPlayerBar_full_screen_iPhone" owner:nil options:nil];
            
            for (id nibObject in nibObjects) {
                if ([nibObject isKindOfClass:[APSPlayerBar class]]) {
                    playerBar = nibObject;
                }
            }
        }
        playerBar.playerBarType = PlayerBarType_FullScreen;
    }
    else
    {
        if (is_iPad())
        {
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"APSPlayerBar_minimized_screen_iPad" owner:nil options:nil];
            
            for (id nibObject in nibObjects) {
                if ([nibObject isKindOfClass:[APSPlayerBar class]]) {
                    playerBar = nibObject;
                }
            }
        }
        else
        {
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"APSPlayerBar_minimized_screen_iPhone" owner:nil options:nil];
            
            for (id nibObject in nibObjects) {
                if ([nibObject isKindOfClass:[APSPlayerBar class]]) {
                    playerBar = nibObject;
                }
            }
        }
        playerBar.playerBarType = PlayerBarType_MinimizedScreen;
        

    }

//    [playerBar setUpPlayerNetworkStatusAnimated:NO];
    return playerBar;
}

//-(void)setUpPlayerNetworkStatusAnimated:(BOOL) animated
//{
//    [self areaChanged:nil];
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.lastPitchScale = 1.f;
    self.blurView.layerColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
    self.blurView.blurRadius = 3;
    [self registerForNotification];
    
    self.labelOutOfNetwork.text = NSLocalizedString(@"Viewing is only aviailable at home or on a KDG enabled network", nil);
    if (is_iPad())
    {
        self.labelOutOfNetwork.font = [APSTypography fontRegularWithSize:17];
    }
    else
    {
        self.labelOutOfNetwork.font = [APSTypography fontRegularWithSize:14];
    }
    
    self.labelEndTime.font = [APSTypography fontRegularWithSize:11];
    [self.labelEndTime setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:self.playerBarType==PlayerBarType_FullScreen?0.8:0.6]];
    
    self.labelStartTime.font = [APSTypography fontRegularWithSize:11];
    [self.labelStartTime setTextColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:self.playerBarType==PlayerBarType_FullScreen?0.8:0.6]];

    
    self.labelOutOfNetwork.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    [self.sliderVideoDuration setThumbImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    [self.sliderVideoDuration setMaximumTrackImage:[APSUtils imageWithColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.14] size:CGSizeMake(1, 4)] forState:UIControlStateNormal];
    [self.sliderVideoDuration setMinimumTrackImage:[APSUtils imageWithColor:[APSTypography colorBrand] size:CGSizeMake(1, 4)] forState:UIControlStateNormal];
    // the following 2 flags verify that the color of the sliderVideoDuration is not modified due to the alpha value of disabled controls.
    self.sliderVideoDuration.enabled = YES;
    self.userInteractionEnabled = YES;
    
    UIImage * thumbImage = [APSUtils imageWithColor:[APSTypography colorBrand] radius:11];
//    UIImage * thumbImage = [[self offsetThumbImage:[APSUtils imageWithColor:[APSTypography colorBrand] radius:11]] stretchableImageWithLeftCapWidth:11 topCapHeight:11];

    UIImage * maximumImage = [[APSUtils imageWithColor:[UIColor colorWithRed:75.0/255.0 green:75.0/255.0 blue:75.0/255.0 alpha:0.14] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    UIImage * minimumdImage = [[APSUtils imageWithColor:[APSTypography colorBrand] size:CGSizeMake(1, 3)] stretchableImageWithLeftCapWidth:0 topCapHeight:1];
    
    [self.sliderVolume setThumbImage:thumbImage forState:UIControlStateNormal];
    [self.sliderVolume setMaximumTrackImage:maximumImage forState:UIControlStateNormal];
    [self.sliderVolume setMinimumTrackImage:minimumdImage forState:UIControlStateNormal];
    
    self.viewSliderContent.layer.cornerRadius = 4;
    self.viewSliderContent.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.viewSliderContent.layer.shadowOpacity = 0.2;
    self.viewSliderContent.layer.shadowRadius = 1;
    self.viewSliderContent.layer.shadowOffset = CGSizeMake(0, 0);
    
    self.buttonAllChannels.backgroundColor = [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1];
    [self.buttonAllChannels setTitleColor:[APSTypography colorMediumGray] forState:UIControlStateNormal];
    [self.buttonAllChannels setTitle:NSLocalizedString(@"ALL CHANNELS", nil) forState:UIControlStateNormal];
    self.buttonAllChannels.titleLabel.font = [APSTypography fontRegularWithSize:12];

    self.labelOriginalframe = self.labelOutOfNetwork.frame;
    
    
    
}


//- (UIImage*)offsetThumbImage:(UIImage*)image {
//	CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
//	UIGraphicsBeginImageContextWithOptions(imageRect.size, FALSE, 0.0);
//	[image drawInRect:CGRectMake(1, 0, image.size.width, image.size.height)];
//	image = UIGraphicsGetImageFromCurrentImageContext();
//	UIGraphicsEndImageContext();
//	return image;
//}


#pragma mark - settes & gettes

-(void)setDelegate:(id<APSPlayerBarDelegate>)delegate
{
    if (_delegate != delegate)
    {
        _delegate = delegate;
        
        [self updateUIComponentsAccordingToPresentor];
    }
}

-(void) updateUIComponentsAccordingToPresentor
{
    if ([self.delegate respondsToSelector:@selector(playerBarBlursUnderlayingView:)])
    {
        self.blurView.underlyingView = [self.delegate playerBarBlursUnderlayingView:self];
    }
    [self volumeChanged:nil];

 
}


-(void)setPlayerBarType:(PlayerBarType)playerBarType
{
    if (_playerBarType != playerBarType)
    {
        _playerBarType = playerBarType;
        [self customizeUIForPlayer];
    }
}

-(void) customizeUIForPlayer
{
    self.layer.shadowOffset = CGSizeMake(0, -1);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.25;
    self.layer.shadowRadius = 1;
}

#pragma mark - notification

-(void) registerForNotification
{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogOut:) name:TVSessionManagerLogoutCompletedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(programAvailable:) name:PurePlayerProgramOfCurrentStreamingAvailableNotificationName object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(streamingChanged:) name:PurePlayerStreamingChangedNotificationName object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:PurePlayerVolumeChangedNotificationName object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(areaChanged:) name:APSSessionManagerUserLoginStausUpdateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(tappedOutSideVolumeSlider:) name:kTouchEventOutsideOfMonitoredView object:nil];
}

-(void) unregisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(IBAction)minimize:(id)sender
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@"Minimize player from icon on bottom right" eventLabel:@""];
    [self animateOutVolumeSender:self.buttonVolume completion:nil];
    [self.delegate playerBar:self didSelectMinimizeVideo:sender];
}

-(IBAction)maximize:(id)sender
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@" Maximize multi task player" eventLabel:@""];
    [self animateOutVolumeSender:self.buttonVolume completion:nil];
    [self.delegate playerBar:self didSelectMaximizeVideo:sender];
}

- (IBAction)actionPinchMaximize:(id)sender {
    UIPinchGestureRecognizer *recognizer = (UIPinchGestureRecognizer *)sender;
    
    if (self.lastPitchScale > 1 && recognizer.state ==UIGestureRecognizerStateEnded){
        [self maximize:sender];
    }
    
    const CGFloat kMinimumScale = 1.00001f;
    if (recognizer.scale > kMinimumScale || recognizer.scale < 1) {
        self.lastPitchScale = recognizer.scale;
    }
    recognizer.scale = 1;
}


-(IBAction)infoButtonPressed:(id)sender
{
    [self.delegate playerBar:self didSelectInfoButton:sender];
}

-(IBAction)volumeButtonPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    // open
    if (sender.selected)
    {
        [self animateInVolume:sender completion:nil];
    }
    //close
    else
    {
        [self animateOutVolumeSender:sender completion:nil];
    }
    
}

-(IBAction)allChannelsButtonPressed:(id)sender
{
    [self.delegate playerBar:self didSelectAllChannelsButton:sender];
}

-(void)updateAllChannelsButtonSlected:(BOOL)selected
{
    if (selected)
    {
        [self.buttonAllChannels setBackgroundColor:[APSTypography colorBrand]];
        [self.buttonAllChannels setTitleColor:[APSTypography colorBrand] forState:UIControlStateNormal];
        [self.buttonAllChannels setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        [self.buttonAllChannels setBackgroundColor:[APSTypography colorVeryLightGray]];
        [self.buttonAllChannels setTitleColor:[APSTypography colorBrand] forState:UIControlStateNormal];
        [self.buttonAllChannels setTitleColor:[APSTypography colorMediumGray] forState:UIControlStateNormal];
    }
}

-(IBAction)closeButtonPressed:(id)sender
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@" Closing multi task player" eventLabel:@""];
    [self.purePlayerViewController stopPlaying];
    if ([self.delegate respondsToSelector:@selector(playerBar:didSelectClose:)])
    {
        [self.delegate playerBar:self didSelectClose:sender];
    }
}

- (IBAction)volumeSliderValueChanged:(UISlider *)sender
{
    [self.delegate playerBar:self volumeValueChanged:sender.value];
}

#pragma mark pure player

-(void)setPurePlayerViewController:(APSPurePlayerViewController *)purePlayerViewController
{
    _purePlayerViewController = purePlayerViewController;
    
    if(!purePlayerViewController.isInInsertionProcess && self.playerBarType == PlayerBarType_MinimizedScreen)
    {
        _purePlayerViewController.view.frame = CGRectMake(0, 0, self.viewVideoPlaceHolder.size.width, self.viewVideoPlaceHolder.size.height);
        [self.viewVideoPlaceHolder addSubview:purePlayerViewController.view];
        
        if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] == APSLoginStaus_OutOfNetwork)
        {
            [self hideRegularView:^{
                [self showOutOfNetworkViewCompletion:nil animated:NO];
            } animated:NO];
        }
    }
    
    [self streamingChanged:nil];
    [self programAvailable:nil];
    [self volumeChanged:nil];



}



#pragma mark - streaming change

-(void) streamingChanged:(NSNotification *) notification
{
    if( self.labelVideoName && [[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey] && self.purePlayerViewController.currentMediaItem.name)
    {
        self.labelVideoName.text = [NSString stringWithFormat:@"%@ %@",[[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey],self.purePlayerViewController.currentMediaItem.name];
        
        if (self.labelVideoName.text)
        {
            NSMutableAttributedString *st = [[NSMutableAttributedString alloc] initWithString:self.labelVideoName.text];
            
            NSRange channelNumberAndNameRange = [[st string] rangeOfString:[NSString stringWithFormat:@"%@ %@",[[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey],self.purePlayerViewController.currentMediaItem.name] options:NSCaseInsensitiveSearch];
            
            [st addAttribute:NSFontAttributeName value:[APSTypography fontRegularWithSize:is_iPad()?15:14] range:channelNumberAndNameRange];
            [st addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:is_iPad()?0.8:0.6] range:channelNumberAndNameRange];
            
            self.labelVideoName.attributedText = st;
        }
        self.labelStartTime.text = [self.purePlayerViewController startTimeText];
        self.labelEndTime.text = [self.purePlayerViewController endTimeText];
    }
    
}

-(void) programAvailable:(NSNotification *) notification
{
    if( self.labelVideoName && self.purePlayerViewController.currentProgram && [[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey] && self.purePlayerViewController.currentMediaItem.name)
    {
        
        NSString * channelNumberString = [[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey];
        NSString* channelTitle = self.purePlayerViewController.currentMediaItem.name;
        NSString* programTitle =self.purePlayerViewController.currentProgram.name;
        if (!channelTitle)
        {
            channelTitle = @"";
        }
        if (!channelNumberString)
        {
            channelNumberString = @"";
        }
        if (!programTitle)
        {
            programTitle = @"";
        }
        
        self.labelVideoName.text = [NSString stringWithFormat:is_iPad()?@"%@ %@  %@":@"%@ %@\n%@",channelNumberString,channelTitle,programTitle];
        
        NSString * name = [self.labelVideoName.text length]>0? self.labelVideoName.text:@"";
        NSMutableAttributedString *st = [[NSMutableAttributedString alloc] initWithString:name];
        NSRange channelNumberAndNameRange = [[st string] rangeOfString:[NSString stringWithFormat:@"%@ %@",[[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey],self.purePlayerViewController.currentMediaItem.name] options:NSCaseInsensitiveSearch];
        
        NSRange programNameRange = [[st string] rangeOfString:programTitle];
        
        [st addAttribute:NSFontAttributeName value:[APSTypography fontRegularWithSize:is_iPad()?15:14] range:channelNumberAndNameRange];
        [st addAttribute:NSFontAttributeName value:[APSTypography fontLightWithSize:is_iPad()?15:14] range:programNameRange];
        [st addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:is_iPad()?0.8:0.6] range:channelNumberAndNameRange];
        [st addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6] range:programNameRange];
        
        self.labelVideoName.attributedText = st;
    }
    
    self.labelStartTime.text = [self.purePlayerViewController startTimeText];
    self.labelEndTime.text = [self.purePlayerViewController endTimeText];
    [self streamingProgressTimeChanged];

    
}

-(void) streamingProgressTimeChanged
{
    CGFloat time =  [self.purePlayerViewController currentPlayBackTimeRealtiveToDuration];
    self.sliderVideoDuration.value = time;
    
    [self.timerStreamingTimeChanged invalidate];
    self.timerStreamingTimeChanged= nil;
    self.timerAgentStreaming = [[GCAgent alloc] initWithOwner:self selector:@selector(streamingProgressTimeChanged)];
    self.timerStreamingTimeChanged = [NSTimer scheduledTimerWithTimeInterval:3 target:self.timerAgentStreaming selector:@selector(selectorFired:) userInfo:nil repeats:NO];

}


#pragma mark volume
-(void) externalVolumeButtonClicked:(NSNotification *) notification
{
    self.sliderVolume.value = [self.delegate playerBarVolume:self];
}


#define volumeIconImage @"volume_icon_ipad"
#define volumeIconMuteImage @"volume_mute_icon_ipad"

-(void) volumeChanged :(NSNotification *) notification
{

    self.sliderVolume.value = [self.purePlayerViewController volume];
    
    if (self.sliderVolume.value>0)
    {
        [self.buttonVolume setImage:[UIImage imageNamed:volumeIconImage] forState:UIControlStateNormal];
        [self.buttonVolume setImage:[UIImage imageNamed:volumeIconImage] forState:UIControlStateSelected];
    }
    else
    {
        [self.buttonVolume setImage:[UIImage imageNamed:volumeIconMuteImage] forState:UIControlStateNormal];
        [self.buttonVolume setImage:[UIImage imageNamed:volumeIconMuteImage] forState:UIControlStateSelected];
    }
}

#pragma mark - area changed




-(void) areaChanged:(NSNotification *) notification
{
    if (self.playerBarType == PlayerBarType_MinimizedScreen)
    {
        
        if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] == APSLoginStaus_OutOfNetwork)
        {
            [self hideRegularView:^{
                [self showOutOfNetworkViewCompletion:nil animated:YES];
            } animated:NO];
        }
        else
        {
            [self hideOutOfNetworkViewCompletion:^{
                [self showRegularView:nil animated:YES];
            } animated:YES];
        }
    }
}




-(void) hideRegularView:(void(^)(void)) completion animated:(BOOL) animated
{
    CGRect originalFrame = self.regularContentView.frame;
    
    [UIView animateWithDuration:animated?0.3:0 animations:^{
        
        self.regularContentView.y = self.height;
        self.regularContentView.alpha = 0;
    } completion:^(BOOL finished) {
        
        self.regularContentView.hidden = YES;
        self.regularContentView.frame = originalFrame;
        
        if (completion)
        {
            completion();
        }
    }];
}

-(void) showRegularView:(void(^)(void)) completion animated:(BOOL) animated
{
    CGRect originalFrame = self.regularContentView.frame;
    self.regularContentView.y = self.height;
    self.regularContentView.hidden = NO;
    self.regularContentView.alpha = 0;

    [UIView animateWithDuration:animated?0.3:0 animations:^{
        
        self.regularContentView.alpha = 1;
        self.regularContentView.frame = originalFrame;
        
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}


-(void) showOutOfNetworkViewCompletion:(void(^)(void)) completion animated:(BOOL) animated
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",@"OutOfNetwork"]];
    self.outOfNetowrkContentView.hidden = NO;
    
    CGRect labelOriginalframe = self.labelOriginalframe;
    CGRect imageOriginalFrame = self.imageOutOfNetwork.frame;
    
    self.labelOutOfNetwork.y = self.height;
    self.imageOutOfNetwork.y = self.height +100;
    
    self.labelOutOfNetwork.alpha = 0;
    self.imageOutOfNetwork.alpha = 0;
    
    [UIView animateWithDuration:animated?0.3:0 animations:^{
       
        self.imageOutOfNetwork.alpha = 1;
        self.labelOutOfNetwork.alpha = 1;

        self.labelOutOfNetwork.frame = labelOriginalframe;
        self.imageOutOfNetwork.frame = imageOriginalFrame;
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}

-(void) hideOutOfNetworkViewCompletion:(void(^)(void)) completion animated:(BOOL) animated
{
    
    CGRect labelOriginalframe = self.labelOutOfNetwork.frame;
    CGRect imageOriginalFrame = self.imageOutOfNetwork.frame;
    
    [UIView animateWithDuration:animated?0.3:0 animations:^{
        
        self.labelOutOfNetwork.y = self.height;
        self.imageOutOfNetwork.y = self.height +100;
        
        self.labelOutOfNetwork.alpha = 0;
        self.imageOutOfNetwork.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        self.outOfNetowrkContentView.hidden = YES;
        self.imageOutOfNetwork.alpha = 1;
        self.labelOutOfNetwork.alpha = 1;
        
        self.labelOutOfNetwork.frame = labelOriginalframe;
        self.imageOutOfNetwork.frame = imageOriginalFrame;
        
        if (completion)
        {
            completion();
        }
    }];

    
}


#pragma mark - removeAllPopupsOfPlayerBar
-(void) removeAllPopupsOfPlayerBar
{
    [self.delegate playerBar:self toggleShowHideVolumeView:nil show:NO];
    [self.viewSliderContent removeFromSuperview];
}

#pragma mark - handle tapping out volume slider
-(void) tappedOutSideVolumeSlider:(NSNotification *) notification
{
    [self animateOutVolumeSender:self.buttonVolume completion:nil];
}


#pragma amrk - volume 

-(void) animateInVolume:(UIButton *) sender completion:(void(^)(void)) completion
{
    UIView * viewToAddSlider = [self.delegate playerBarBlursUnderlayingView:self];
    if ([self.delegate respondsToSelector:@selector(playerBar:toggleShowHideVolumeView:show:)])
    {
            [self.delegate playerBar:self toggleShowHideVolumeView:sender show:YES];
    }

    APSEventMonitorWindow* window = (APSEventMonitorWindow*)[UIApplication sharedApplication].delegate.window;
    [window.arrayMonitoredViews addObject: self.viewSliderContent];
    [window.arrayIgnoreViews addObject: sender];
    
    self.viewSliderContent.transform = CGAffineTransformMakeRotation(0);
    CGPoint center = [self.playerBarType==PlayerBarType_MinimizedScreen?self.regularContentView:self convertPoint:sender.center toView:viewToAddSlider];
    center.y= center.y-self.viewSliderContent.width*0.5-(self.playerBarType==PlayerBarType_MinimizedScreen?13:20)-sender.height*0.5;
    self.viewSliderContent.center = center;
    self.viewSliderContent.alpha =0;
    [viewToAddSlider addSubview:self.viewSliderContent];
    
    self.viewSliderContent.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    self.sliderVolume.value = [self.delegate playerBarVolume:self];
    
    
    if ([self.delegate respondsToSelector:@selector(playerBar:delayBy:)])
    {
        [self.delegate playerBar:self delayBy:sender];
    }
    
    
    [UIView animateWithDuration:0.1 animations:^{
        self.viewSliderContent.alpha =1;
        
        if (completion)
        {
            completion();
        }

    }];
}

-(void) animateOutVolumeSender:(id) sender completion:(void(^)(void)) completion
{
    APSEventMonitorWindow* window = (APSEventMonitorWindow*)[UIApplication sharedApplication].delegate.window;
    [window.arrayMonitoredViews removeAllObjects];
    [window.arrayIgnoreViews removeAllObjects];

    
    if ([self.delegate respondsToSelector:@selector(playerBar:toggleShowHideVolumeView:show:)])
    {
        [self.delegate playerBar:self toggleShowHideVolumeView:sender show:NO];
    }
    if ([self.delegate respondsToSelector:@selector(playerBar:stopDelay:)])
    {
        [self.delegate playerBar:self stopDelay:nil];
    }
    
    
    [UIView animateWithDuration:0.1 animations:^{
        self.viewSliderContent.alpha = 0;
        [self.viewSliderContent removeFromSuperview];
    }completion:^(BOOL finished) {
        
        self.viewSliderContent.transform = CGAffineTransformMakeRotation(0);
        self.buttonVolume.selected = NO;
       
        if (completion)
        {
            completion();
        }
    }];
}


#pragma mark - logout 
-(void) userDidLogOut:(NSNotification *) notification
{
    [self closeButtonPressed:nil];
}

@end
