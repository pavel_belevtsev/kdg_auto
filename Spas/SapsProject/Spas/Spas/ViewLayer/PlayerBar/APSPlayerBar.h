//
//  APSPlayerBar.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum PlayerBarType
{
    PlayerBarType_FullScreen,
    PlayerBarType_MinimizedScreen,
}PlayerBarType;

@class APSPlayerBar;

@protocol APSPlayerBarDelegate <NSObject>

-(void) playerBar:(APSPlayerBar *) playerbar didSelectClose:(UIButton *) sender;
-(void) playerBar:(APSPlayerBar *) playerbar didSelectMaximizeVideo:(UIButton *) sender;
-(void) playerBar:(APSPlayerBar *) playerbar didSelectMinimizeVideo:(UIButton *) sender;
-(void) playerBar:(APSPlayerBar *) playerbar volumeValueChanged:(CGFloat) value;
-(void) playerBar:(APSPlayerBar *) playerbar toggleShowHideVolumeView:(UIButton *) sender show:(BOOL)isShow;

-(CGFloat) playerBarVolume:(APSPlayerBar *) playerbar;
-(UIView *) playerBarBlursUnderlayingView:(APSPlayerBar *) playerbar;

@optional
-(void) playerBar:(APSPlayerBar *) playerbar didSelectInfoButton:(UIButton *) sender;
-(void) playerBar:(APSPlayerBar *) playerbar didSelectAllChannelsButton:(UIButton *) sender;

-(void) playerBar:(APSPlayerBar *)playerbar delayBy:(id) sender;
-(void) playerBar:(APSPlayerBar *)playerbar stopDelay:(id) sender;

@end

@interface APSPlayerBar : UIView

@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property (weak, nonatomic) IBOutlet id<APSPlayerBarDelegate> delegate;
//@property (weak, nonatomic) IBOutlet id<APSPlayerBarDelegate> delegate;
@property (assign, nonatomic) PlayerBarType playerBarType;


@property (weak, nonatomic) IBOutlet APSBlurView *blurView;
@property (weak, nonatomic) IBOutlet UIView *viewVideoPlaceHolder;
@property (weak, nonatomic) IBOutlet UILabel * labelVideoName;
@property (weak, nonatomic) IBOutlet UILabel * labelStartTime;
@property (weak, nonatomic) IBOutlet UILabel * labelEndTime;
@property (strong, nonatomic) IBOutlet UISlider * sliderVideoDuration;
@property (strong, nonatomic) IBOutlet UISlider * sliderVolume;
@property (strong, nonatomic) IBOutlet UIView * viewSliderContent;
@property (weak, nonatomic) IBOutlet UIButton * buttonVolume;





@property (retain, nonatomic) IBOutlet UIView * outOfNetowrkContentView;
@property (retain, nonatomic) IBOutlet UIView * regularContentView;


@property (weak, nonatomic) IBOutlet UILabel * labelOutOfNetwork;
@property (weak, nonatomic) IBOutlet UIImageView * imageOutOfNetwork;

@property (weak, nonatomic) IBOutlet APSGaryButton *buttonAllChannels;



@property (weak, nonatomic) APSPurePlayerViewController * purePlayerViewController;

+(APSPlayerBar *) playBarOfType:(PlayerBarType ) type;
-(IBAction)minimize:(id)sender;
-(IBAction)maximize:(id)sender;
-(IBAction)infoButtonPressed:(id)sender;
-(IBAction)volumeButtonPressed:(id)sender;
-(IBAction)allChannelsButtonPressed:(id)sender;
-(IBAction)closeButtonPressed:(id)sender;

-(void) streamingChanged:(NSNotification *) notification;

-(void) removeAllPopupsOfPlayerBar;

-(void)updateAllChannelsButtonSlected:(BOOL)selected;
-(void) animateOutVolumeSender:(id) sender completion:(void(^)(void)) completion;




@end
