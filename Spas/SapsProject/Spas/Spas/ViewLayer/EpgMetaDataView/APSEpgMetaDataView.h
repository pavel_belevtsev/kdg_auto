//
//  APSEpgMetaDataView.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TvinciSDK/TVLinearLayout.h>

@interface APSEpgMetaDataView : UIView

+(TVLinearLayout *)buildUIWithMetaData:(NSDictionary *)data keysOrder:(NSArray*)kays andLayoutMargin:(NSInteger)margins andUIDic:(NSDictionary *)UIDic;

+(UIView *)metadataTitle:(NSString *)title andText:(NSString*)bodyText  andViewWidth:(NSInteger)viewWidth;
+(UIView *)metadataTitle:(NSString *)title andArray:(NSArray*)array andViewWidth:(NSInteger)viewWidth;

+(NSString*)timeLblStingFormatWithStartTime:(NSDate*)startTime EndTime:(NSDate*)endTime;

+(UIView *)buildDescriptionViewWithText:(NSString*)text andViewWidth:(NSInteger)viewWidth andFont:(UIFont *) font andLineHeight:(float)lineHeight andTextColor:(UIColor*)textColor;


+(BOOL)isProgramInCurrentWithStartTime:(NSDate*)startTime EndTime:(NSDate*)endTime;
+(BOOL)isProgramInCurrent:(APSTVProgram *) program;

+(void)buildUIListOnView:(TVLinearLayout *)linearLayoutView  WithMetaData:(NSDictionary *)data keysOrder:(NSArray*)kays andLayoutMargin:(NSInteger)margins andUIDic:(NSDictionary *)UIDic andListSize:(NSInteger)listSize;
@end

extern NSString *const MDVUIItemLblTitleWidth;
extern NSString *const MDVUIItemLblTitleFont;
extern NSString *const MDVUIItemLblTextFont;
extern NSString *const MDVUIItemLblTitleColor;
extern NSString *const MDVUIItemLblTextColor;
extern NSString *const MDVUIItemLblTitleSizeToFit;
extern NSString *const MDVUIItemLblTitleLetterSpacing;
extern NSString *const MDVUIItemViewWidth;
extern NSString *const MDVUIItemViewMultiLineHigh;
