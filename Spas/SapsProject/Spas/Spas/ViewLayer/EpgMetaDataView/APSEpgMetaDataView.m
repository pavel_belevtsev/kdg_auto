//
//  APSEpgMetaDataView.m
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEpgMetaDataView.h"

@implementation APSEpgMetaDataView


#pragma  mark - build Description UI -

+(UIView *)buildDescriptionViewWithText:(NSString*)text andViewWidth:(NSInteger)viewWidth andFont:(UIFont *) font andLineHeight:(float)lineHeight andTextColor:(UIColor*)textColor
{
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 20)];
    lblTitle.numberOfLines = 0;
    [lblTitle setText:text];
    [lblTitle setFont:font];
    [lblTitle setTextColor:textColor];
    
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = lineHeight;
    style.maximumLineHeight = lineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (text)
    {
        lblTitle.attributedText = [[NSAttributedString alloc] initWithString:text
                                                                  attributes:attributtes];
    }
    [lblTitle sizeToFit];
    
    if (text == nil || [text isEqualToString:@""]|| [text isEqualToString:@" "])
    {
        lblTitle.hidden = YES;
    }
    else
    {
        lblTitle.hidden = NO;
    }
    
    return lblTitle;
}

#pragma  mark - build MetaData UI -

+(TVLinearLayout *)buildUIWithMetaData:(NSDictionary *)data keysOrder:(NSArray*)kays andLayoutMargin:(NSInteger)margins andUIDic:(NSDictionary *)UIDic
{
    
    NSInteger viewWidth = [[UIDic objectForKey:MDVUIItemViewWidth] integerValue];
    
    TVLinearLayout * linearLayoutView = [[TVLinearLayout alloc] init];
    linearLayoutView.orientation = TVLayoutOrientationVertical;
    linearLayoutView.margins = margins;
    [linearLayoutView setFrame:CGRectMake(0, 0, viewWidth, 1000)];
    [linearLayoutView setBackgroundColor:[UIColor clearColor]];
    
    // clean
    for (UIView * view in [linearLayoutView subviews])
    {
        [view removeFromSuperview];
    }
    
    for (NSString * key in kays)
    {
        if ([data objectForKey:key])
        {
            NSString* translatedKey = NSLocalizedString(key, nil);
            UIView * subView = nil;
            if ([[data objectForKey:key]  isKindOfClass:[NSArray class]])
            {
                subView = [APSEpgMetaDataView metadataTitle:translatedKey
                           
                                                   andArray:[data objectForKey:key] andViewWidth:viewWidth andUIDic:UIDic];
            }
            else if ([[data objectForKey:key]  isKindOfClass:[NSString class]])
            {
                subView = [APSEpgMetaDataView metadataTitle:key andText:[data objectForKey:key] andViewWidth:viewWidth andUIDic:UIDic];
            }
            if (subView)
            {
                [linearLayoutView addSubview:subView];
            }
        }
    }
    
    NSInteger viewHight=0;
    for (UIView * view in [linearLayoutView subviews])
    {
        viewHight += view.height;
    }
    viewHight += [[linearLayoutView subviews] count]*margins;
    [linearLayoutView setHeight:viewHight];
    
    [linearLayoutView setBackgroundColor:[UIColor clearColor]];
    return linearLayoutView;
}


+(UIView *)metadataTitle:(NSString *)title andText:(NSString*)bodyText  andViewWidth:(NSInteger)viewWidth andUIDic:(NSDictionary *)UIDic
{
    NSInteger lblTitleWidth       =   [[UIDic objectForKey:MDVUIItemLblTitleWidth] integerValue]; // NSInteger lblTitleWidth = 85;
    UIFont * lblTextFont          =   [UIDic objectForKey:MDVUIItemLblTextFont];
    UIColor * lblTextColor        =   [UIDic objectForKey:MDVUIItemLblTextColor];
    
    NSInteger lblTextWidth = viewWidth - lblTitleWidth;
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 18)];

    UILabel * lblTitle = [APSEpgMetaDataView buildMetaTitle:title andViewWidth:viewWidth andUIDic:UIDic];
    [view addSubview:lblTitle];
    
    UILabel * lblText = [[UILabel alloc] initWithFrame:CGRectMake(lblTitleWidth+1, 0, lblTextWidth, 18)];
    [lblText setFont:lblTextFont];
    [lblText setTextColor:lblTextColor];
    [lblText setText:bodyText];
    lblText.textAlignment = NSTextAlignmentLeft;
    [view addSubview:lblText];

    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

+(UIView *)metadataTitle:(NSString *)title andArray:(NSArray*)array andViewWidth:(NSInteger)viewWidth andUIDic:(NSDictionary *)UIDic
{
    NSString * allText = @"";
    NSMutableArray * validArray = [NSMutableArray array];
    
    for (NSString * text in array) {
        if (![text isEqualToString:@""] && ![text isEqualToString:@" "])
        {
            [validArray addObject:text];
        }
    }
    for (int index =0; index < [validArray count]; index++)
    {
        NSString * text = [validArray objectAtIndex:index];
        if (index == 0 )
        {
            allText = [NSString stringWithFormat:@"%@",text];
        }
        else
        {
            allText = [NSString stringWithFormat:@"%@, %@",allText,text];

        }
    }
    UIView * view = [APSEpgMetaDataView metadataTitle:title andText:allText andViewWidth:viewWidth andUIDic:UIDic];
    return view;
}

#pragma mark - UI Helprs -

+(NSArray *)buildValidDataArray:(NSArray *)array withSize:(NSInteger)size
{
    NSMutableArray * validArray = [NSMutableArray array];
    NSInteger num = 1;
    for (NSString * text in array)
    {
        if (![text isEqualToString:@""] && ![text isEqualToString:@" "])
        {
            [validArray addObject:text];
            num ++;
        }
        if (num > size)
        {
            break;
        }
    }
    return [NSArray arrayWithArray:validArray];
}


+(UILabel *) buildMetaTitle:(NSString *)title andViewWidth:(NSInteger)viewWidth andUIDic:(NSDictionary *)UIDic
{
    NSInteger lblTitleWidth       =   [[UIDic objectForKey:MDVUIItemLblTitleWidth] integerValue]; // NSInteger lblTitleWidth = 85;
    UIFont * titleFont            =   [UIDic objectForKey:MDVUIItemLblTitleFont];
    BOOL lblTitleSizeToFit        =   [[UIDic objectForKey:MDVUIItemLblTitleSizeToFit] boolValue];
    UIColor * lblTitleColor       =   [UIDic objectForKey:MDVUIItemLblTitleColor];
    float titleLetterSpacing  =   [[UIDic objectForKey:MDVUIItemLblTitleLetterSpacing] floatValue];
    
    UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, lblTitleWidth, 18)];
    lblTitle.textAlignment = NSTextAlignmentLeft;
    [lblTitle setTextColor:lblTitleColor];
    [lblTitle setFont:titleFont];
    
    if (titleLetterSpacing > 0)
    {
        NSString* translatedTitle = NSLocalizedString(title, nil);
        NSMutableAttributedString * attr = [APSTypography attributedString:[translatedTitle uppercaseString] withLetterSpacing:1.5];
        lblTitle.attributedText = attr;
    }
    else
    {
        [lblTitle setText:[NSString stringWithFormat:@"%@",[title uppercaseString]]];
    }
    
    if (lblTitleSizeToFit)
    {
        [lblTitle sizeToFit];
        [lblTitle layoutIfNeeded];
        lblTitle.height=18;
    }
    return lblTitle;
}

#pragma  mark - build List MetaData UI -

+(void)buildUIListOnView:(TVLinearLayout *)linearLayoutView  WithMetaData:(NSDictionary *)data keysOrder:(NSArray*)kays andLayoutMargin:(NSInteger)margins andUIDic:(NSDictionary *)UIDic andListSize:(NSInteger)listSize
{
    
    NSInteger viewWidth = [[UIDic objectForKey:MDVUIItemViewWidth] integerValue];

    for (NSString * key in kays)
    {
        if ([data objectForKey:key])
        {
            UIView * subView = nil;
            NSArray * validArray = [APSEpgMetaDataView buildValidDataArray:[data objectForKey:key] withSize:listSize];
         
            if ([validArray count] >0)
            {
                NSString* translatedKey = NSLocalizedString(key, nil);
                subView = [APSEpgMetaDataView listMetadataTitle:translatedKey andTexts:validArray andViewWidth:viewWidth andUIDic:UIDic];
            }
            
            if (subView)
            {
                [linearLayoutView addSubview:subView];
            }
        }
    }
    
    NSInteger viewHight=0;
    for (UIView * view in [linearLayoutView subviews])
    {
        viewHight += view.height;
    }
    viewHight += [[linearLayoutView subviews] count]*margins;
    [linearLayoutView setHeight:viewHight];
    
    [linearLayoutView setBackgroundColor:[UIColor clearColor]];
}



+(UIView *)listMetadataTitle:(NSString *)title andTexts:(NSArray*)bodyTextsArray  andViewWidth:(NSInteger)viewWidth andUIDic:(NSDictionary *)UIDic
{
    NSInteger lblTitleWidth       =   [[UIDic objectForKey:MDVUIItemLblTitleWidth] integerValue]; // NSInteger lblTitleWidth = 85;
    UIFont * lblTextFont          =   [UIDic objectForKey:MDVUIItemLblTextFont];
    UIColor * lblTextColor        =   [UIDic objectForKey:MDVUIItemLblTextColor];
    
    NSInteger lblTextWidth = viewWidth - lblTitleWidth;
    NSInteger multiLinehigh = [[UIDic objectForKey:MDVUIItemViewMultiLineHigh]integerValue];

    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, (multiLinehigh * [bodyTextsArray count]) )];
    
    UILabel * lblTitle = [APSEpgMetaDataView buildMetaTitle:title andViewWidth:viewWidth andUIDic:UIDic];
    [view addSubview:lblTitle];
    
    
    NSInteger high = 0;
    for (NSString * bodyText in bodyTextsArray)
    {
        UILabel * lblText = [[UILabel alloc] initWithFrame:CGRectMake(lblTitleWidth+1, high, lblTextWidth, 18)];
        [lblText setFont:lblTextFont];
        [lblText setTextColor:lblTextColor];
        [lblText setText:bodyText];
        lblText.textAlignment = NSTextAlignmentLeft;
        [view addSubview:lblText];
        high += multiLinehigh;
    }
    
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

#pragma mark - Time utilties -

+(NSString*)timeLblStingFormatWithStartTime:(NSDate*)startTime EndTime:(NSDate*)endTime
{
    NSString * time= nil;
    NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:startTime];
    NSInteger hourStart = [componentsStart hour];
    NSInteger minuteStart = [componentsStart minute];
    
    NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:endTime];
    NSInteger hourEnd = [componentsEnd hour];
    NSInteger minuteEnd = [componentsEnd minute];
    //now
    NSDate * now = [NSDate date];
    
    if([now compare:startTime] == NSOrderedDescending && [now compare:endTime] == NSOrderedAscending)
    {
        NSString * now = NSLocalizedString(@"Now", nil);
        time = [NSString stringWithFormat:@"%@ - %02d:%02d",now,hourEnd,minuteEnd];
    }
    else
    {
        time =[NSString stringWithFormat:@"%02d:%02d - %02d:%02d",hourStart,minuteStart,hourEnd,minuteEnd];
    }
    
    return time;
}

+(BOOL)isProgramInCurrent:(APSTVProgram *) program
{
    return [APSEpgMetaDataView isProgramInCurrentWithStartTime:program.startDateTime EndTime:program.endDateTime];
}

+(BOOL)isProgramInCurrentWithStartTime:(NSDate*)startTime EndTime:(NSDate*)endTime
{
    //now
    NSDate * now = [NSDate date];
    if([now compare:startTime] == NSOrderedDescending && [now compare:endTime] == NSOrderedAscending)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end

NSString *const MDVUIItemLblTitleWidth = @"lblTitleWidth";
NSString *const MDVUIItemLblTitleFont = @"titleFont";
NSString *const MDVUIItemLblTextFont = @"lblTextFont";
NSString *const MDVUIItemLblTitleColor = @"lblTitleColor";
NSString *const MDVUIItemLblTextColor = @"lblTextColor";
NSString *const MDVUIItemLblTitleSizeToFit = @"lblTitleSizeToFit";
NSString *const MDVUIItemLblTitleLetterSpacing = @"titleLetterSpacing";
NSString *const MDVUIItemViewWidth = @"viewWidth";
NSString *const MDVUIItemViewMultiLineHigh = @"multiLineHigh";

//    CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
//
//    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
//                                          font, NSFontAttributeName,NSParagraphStyleAttributeName , style,
//                                          nil];
//
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
//
//    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
//
//    if (requiredHeight.size.width > lblTitle.frame.size.width)
//    {
//        requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
//    }
//
//    CGRect newFrame = lblTitle.frame;
//    newFrame.size.height = requiredHeight.size.height;
//    lblTitle.frame = newFrame;


//    if (is_iPad())
//    {
//        //[lblTitle setFont:[APSTypography fontRegularWithSize:10]];
//        //NSMutableAttributedString * attr = [APSTypography attributedString:[title uppercaseString] withLetterSpacing:1.5];
//       // lblTitle.attributedText = attr;
//    }
//    else
//    {
//        [lblTitle setFont:[APSTypography fontRegularWithSize:12]];
//        [lblTitle setText:[NSString stringWithFormat:@"%@",[title uppercaseString]]];
//        [lblTitle sizeToFit];
//        [lblTitle layoutIfNeeded];
//        lblTitle.height=18;
//    }
