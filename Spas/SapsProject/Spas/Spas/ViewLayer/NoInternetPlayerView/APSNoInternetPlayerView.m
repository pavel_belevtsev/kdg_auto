//
//  APSNoInternetPlayerView.m
//  Spas
//
//  Created by Israel Berezin on 10/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSNoInternetPlayerView.h"
#import "APSSSLPinningManager.h"
#import "APSNetworkManager.h"

@implementation APSNoInternetPlayerView

+(APSNoInternetPlayerView *) noInternetPlayerView
{
    if (is_iPad())
    {
        return  [self noInternetPlayerView_iPad];
    }
    APSNoInternetPlayerView *view = nil;
    NSString *nibName = @"APSNoInternetPlayerView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    
    return view;    
}

+(APSNoInternetPlayerView *) noInternetPlayerView_iPad
{
   
    APSNoInternetPlayerView *view = nil;
    NSString *nibName = @"APSNoInternetPlayerView_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    
    return view;
}

-(void)awakeFromNib
{
    [self.lblNoInternetTitle setTextColor:[APSTypography colorLightGray]];
    [self.lblNoInternetTitle setFont:[APSTypography fontMediumWithSize:19.0]];
    
    [self.lblNoInternetDescription setTextColor:[APSTypography colorLightGray]];
    [self.lblNoInternetDescription setFont:[APSTypography fontRegularWithSize:16]];
    
    NSString *buttonText = [APSSSLPinningManager manager].SSLFailed ? NSLocalizedString(@"OK", nil) : [NSLocalizedString(@"Reload", nil) uppercaseString];
    
    [self.btnReloadVideo setTitle:buttonText forState:UIControlStateNormal];
    if(is_iPad())
    {
        self.btnReloadVideo.titleLabel.attributedText =[APSTypography attributedString:buttonText withLetterSpacing:1.5];
    }
    else
    {
        self.btnReloadVideo.titleLabel.attributedText =[APSTypography attributedString:buttonText withLetterSpacing:1.0];
    }
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.btnReloadVideo.titleLabel.font = [APSTypography fontRegularWithSize:15];

    APSNetworkStatus status = [[APSNetworkManager  sharedNetworkManager] currentNetworkStatus];
    
    BOOL backendIsUnreachable = (status != TVNetworkStatus_NoConnection);
    
    if (backendIsUnreachable) {
        
        self.lblNoInternetTitle.text = @"";
        self.lblNoInternetDescription.text = NSLocalizedString(@"Backend is unreachable", nil);
        
    } else {
        
        self.lblNoInternetTitle.text = NSLocalizedString(@"No Internet Connection", nil);
        if ([APSSSLPinningManager manager].SSLFailed) {
            self.lblNoInternetDescription.text = NSLocalizedString(@"Problem in communication with servers. Please check for the latest version (16)", nil);
        } else {
            self.lblNoInternetDescription.text = NSLocalizedString(@"Yellow TV is not available while you're offline. Please connect to the internet and try again.", nil);
        }

    }

}

- (IBAction)reloadVideo:(id)sender
{
    if ([APSSSLPinningManager manager].SSLFailed) {
        
        [[APSSSLPinningManager manager] goToAppStoreAndExit];
        
    } else {
        
        if ([self.delegate respondsToSelector:@selector(noInternetPlayerView:didSelectReload:)])
        {
            [self.delegate noInternetPlayerView:self didSelectReload:sender];
        }
        
    }

}

@end
