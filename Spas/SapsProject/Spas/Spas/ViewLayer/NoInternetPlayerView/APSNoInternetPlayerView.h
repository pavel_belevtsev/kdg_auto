//
//  APSNoInternetPlayerView.h
//  Spas
//
//  Created by Israel Berezin on 10/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>


@class APSNoInternetPlayerView;

@protocol APSNoInternetPlayerViewDelegate <NSObject>

-(void) noInternetPlayerView:(APSNoInternetPlayerView *) noInternetPlayerView didSelectReload:(UIButton *) sender;

@end

@interface APSNoInternetPlayerView : UIView

@property (nonatomic,assign) id<APSNoInternetPlayerViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternetTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternetDescription;

@property (weak, nonatomic) IBOutlet APSBrandedButton *btnReloadVideo;

+(APSNoInternetPlayerView *) noInternetPlayerView;

- (IBAction)reloadVideo:(id)sender;
@end
