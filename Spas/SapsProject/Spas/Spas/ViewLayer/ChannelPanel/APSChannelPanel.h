//
//  APSChannelPanel.h
//  Spas
//
//  Created by Israel Berezin on 8/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSBaseViewController.h"
#import "APSMiniEpgBaseCell.h"
@class APSChannelPanel;

@protocol APSChannelPanel <NSObject>

-(void) channelPanel:(APSChannelPanel *) channelPanel didSelectChannel:(TVMediaItem *) channel;

@end

@interface APSChannelPanel : APSBaseViewController <UITableViewDataSource, UITableViewDelegate,APSMiniEPGProgramsTableViewCellDelegate>

@property (assign, nonatomic) id<APSChannelPanel> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

//@property (strong, nonatomic) IBOutlet APSBlurView *blurView;

@property (retain, nonatomic) TVMediaItem * selectedChannelMedia;

@property (strong, nonatomic) NSArray* allChannels;
@property (assign , nonatomic) BOOL isChannelPanelVisible;

-(void)updateSelectedCellToChannel:(TVMediaItem *) channel;

@end
