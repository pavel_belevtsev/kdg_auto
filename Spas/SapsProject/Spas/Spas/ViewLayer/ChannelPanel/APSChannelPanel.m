//
//  APSChannelPanel.m
//  Spas
//
//  Created by Israel Berezin on 8/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSChannelPanel.h"
#import "APSChannelPanelTableViewCell.h"

@implementation APSChannelPanel



-(void)viewDidLoad
{
    [super viewDidLoad];
    self.allChannels =[[APSEPGManager sharedEPGManager] allChannels];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
    self.tableView.separatorColor = [UIColor colorWithWhite:1.0 alpha:0.2];
    [self.tableView reloadData];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.view setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.75]];
//
//    [self.blurView setLayerColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
//    self.blurView.blurRadius = 20;
//    [self.blurView setNeedsDisplay];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.allChannels.count*2;
}

// setup programms cell
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"APSChannelPanelTableViewCell";
    APSChannelPanelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [APSChannelPanelTableViewCell channelPanelCell];
    }

    NSInteger row = indexPath.row%self.allChannels.count;

    TVMediaItem * media = [self.allChannels objectAtIndex:row];

    [cell updateCellWithChannel:media];
     cell.backgroundColor = [UIColor clearColor];
    if ([self.selectedChannelMedia.epgChannelID isEqualToString:media.epgChannelID])
    {
        [cell updateCellUiIsSelectedCell:YES];
    }
    else
    {
        [cell updateCellUiIsSelectedCell:NO];
    }
    
    cell.delegate = self;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (is_iPad())
    {
        return 65;
    }
    else
    {
        return 70;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ASLogInfo(@"");
}

-(void)updateSelectedCellToChannel:(TVMediaItem *) channel
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"Channel panel"];
    self.selectedChannelMedia = channel;
    [self.tableView reloadData];
    NSInteger index = [self.allChannels indexOfObject:channel];
    if (index != NSNotFound && index < [self.allChannels count])
    {
        if (index >0) // keep selected sell in Second place from the top of show list.
        {
            index--;
        }
        else
        {
            index = [self.allChannels count]-1;
        }
        NSIndexPath * path = [NSIndexPath indexPathForRow:index inSection:0];
        [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
    }
}

-(void) miniEPGProgramsTableViewCell:(APSMiniEpgBaseCell *) miniEPGProgramsTableViewCell didSelectChannel:(TVMediaItem *) channel
{
     [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@"Play from channel panel" eventLabel:[NSString stringWithFormat:@"channel = %@",channel.name]];
    [self.delegate channelPanel:self didSelectChannel:channel];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
    CGFloat currentOffsetX = scrollView_.contentOffset.x;
    CGFloat currentOffSetY = scrollView_.contentOffset.y;
    CGFloat contentHeight = scrollView_.contentSize.height;
    
    if (currentOffSetY < (contentHeight / 8.0))
    {
        scrollView_.contentOffset = CGPointMake(currentOffsetX,(currentOffSetY + (contentHeight/2)));
    }
    if (currentOffSetY > ((contentHeight * 6)/ 8.0))
    {
        scrollView_.contentOffset = CGPointMake(currentOffsetX,(currentOffSetY - (contentHeight/2)));
    }
}


@end
