//
//  APSTextView.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTextView.h"

@implementation APSTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)awakeFromNib
{
    
    NSString * properyNaem = NSStringFromProperty(contentOffset);
    [self.textView addObserver:self forKeyPath:properyNaem options:NSKeyValueObservingOptionNew context:nil];
    self.gradient = [CAGradientLayer layer];
    self.gradient.frame = self.bounds;
    self.gradient.startPoint = CGPointMake(0.0f, 0.70f);
    self.gradient.endPoint = CGPointMake(0.0f, 1.0f);
    self.gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.4 alpha:1.0] CGColor],(id)[[UIColor colorWithWhite:0.3 alpha:8.0] CGColor],(id)[[UIColor colorWithWhite:0.2 alpha:7.0] CGColor],(id)[[UIColor clearColor] CGColor],nil];
}






- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    NSLog(@"%@",keyPath);
    
    if (self.textView.contentOffset.y+self.height<self.textView.contentSize.height)
    {
         [UIView animateWithDuration:0.05 animations:^{
        self.layer.mask =self.gradient;
         }];
        
    }
    else
    {
        [UIView animateWithDuration:0.05 animations:^{
            self.layer.mask = 0;
        }];
    }

}


- (void) dealloc
{
    NSString * properyNaem = NSStringFromProperty(contentOffset);
    [self.textView removeObserver:self forKeyPath:properyNaem];
}





@end
