//
//  APSSearchField.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSearchField.h"

@implementation APSSearchField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [APSTypography colorVeryLightGray];
    self.layer.cornerRadius = 2;
    

}





-(void)setActive:(BOOL)active
{
    if (_active != active)
    {
        _active = active;
        
        [self drawUI:active];
    }
}


-(void) drawUI:(BOOL) active
{
    self.buttonDeleteAll.hidden = !active;
}

-(IBAction)clearAll:(id)sender
{
    self.textfieldSearch.text = @"";
    if ([self.delegate respondsToSelector:@selector(searchField:didSelectDeleteAll:)])
    {
        [self.delegate searchField:self didSelectDeleteAll:sender];
    }
}

-(void)dealloc
{
}



@end
