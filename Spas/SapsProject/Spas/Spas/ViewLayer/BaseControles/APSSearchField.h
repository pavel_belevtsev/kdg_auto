//
//  APSSearchField.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSSearchField;

@protocol APSSearchFieldDelegate <NSObject>
-(void) searchField:(APSSearchField *)searchField didSelectDeleteAll:(id)sender;
@end

@interface APSSearchField : UIView

@property (assign, nonatomic) id<APSSearchFieldDelegate>delegate;

@property (strong, nonatomic) IBOutlet UITextField * textfieldSearch;
@property (strong, nonatomic) IBOutlet UIImageView * imageMagnifier;
@property (strong, nonatomic) IBOutlet UIButton * buttonDeleteAll;
@property (strong, nonatomic) IBOutlet UIButton * buttonCancel;


@property (assign, nonatomic) BOOL active;
-(IBAction)clearAll:(id)sender;


@end
