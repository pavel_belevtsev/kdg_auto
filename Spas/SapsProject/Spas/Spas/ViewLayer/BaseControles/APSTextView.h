//
//  APSTextView.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSTextView : UIView<UITextViewDelegate>

@property (retain, nonatomic) IBOutlet UITextView * textView;
@property (retain, nonatomic) CAGradientLayer * gradient;





@end
