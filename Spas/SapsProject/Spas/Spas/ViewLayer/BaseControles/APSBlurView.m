//
//  APSBlurView.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBlurView.h"

@implementation APSBlurView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    //self.layerColor = self.backgroundColor;
    self.blurRadius = self.blurRadius?self.blurRadius:14;
    self.layerColor = self.layerColor?self.layerColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
    self.tintColor = [UIColor blackColor];

}


-(void)setLayerColor:(UIColor *)layerColor
{
    if (_layerColor!=layerColor)
    {
        _layerColor = layerColor;
        [self updateLayerMaskColor];
    }
}

-(void) updateLayerMaskColor
{
    if (self.viewColoredLayer == nil)
    {
        self.viewColoredLayer = [[UIView alloc] init];
        self.viewColoredLayer.autoresizingMask = UIViewAutoresizingFlexibleWidth |
        UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        [self insertSubview:self.viewColoredLayer atIndex:0];
       
    }
    
    self.viewColoredLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    self.viewColoredLayer.backgroundColor = self.layerColor;
}

@end
