//
//  APSButton.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSButton.h"

@implementation APSButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

    [super drawRect:rect];
    
    // Drawing code
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 4;
    [self.layer setMasksToBounds:YES];
}


-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    BOOL toReturen = [super beginTrackingWithTouch:touch withEvent:event];
    self.alpha = 0.5;
    return toReturen;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    self.alpha = 1.0;
}


-(void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:[title uppercaseString] forState:state];
}
@end
