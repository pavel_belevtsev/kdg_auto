//
//  APSImageView.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBrandedColorImageView.h"
#import "UIImage+APSColoredImage.h"

@implementation APSBrandedColorImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setImage:self.image];
}


-(void)setImage:(UIImage *)image
{
    UIImage * brandedImage = [UIImage coloredImageWithColor:[APSTypography colorBrand] bitMapImage:image];
    [super setImage:brandedImage];
}









@end
