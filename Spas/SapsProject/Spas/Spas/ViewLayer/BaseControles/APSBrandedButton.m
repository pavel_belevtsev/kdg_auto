//
//  APSBrandedButton.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBrandedButton.h"

@implementation APSBrandedButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[APSTypography colorBrand]];
}

@end
