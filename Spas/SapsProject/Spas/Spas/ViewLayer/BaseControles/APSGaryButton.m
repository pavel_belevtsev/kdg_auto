//
//  APSGaryButton.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSGaryButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation APSGaryButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{

    [super awakeFromNib];
    [self.titleLabel setFont:[APSTypography  fontRegularWithSize:self.titleLabel.font.pointSize]];
    [self setTitleColor:[APSTypography colorMediumGray] forState:UIControlStateNormal];
    [self setBackgroundColor:[APSTypography colorVeryLightGray]];

}


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    
    if (is_iPad())
    {
        self.titleLabel.attributedText = [APSTypography attributedString:title withLetterSpacing:1.5];
    }
    else
    {
        self.titleLabel.attributedText = [APSTypography attributedString:title withLetterSpacing:1.0];
    }
}


-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
}
@end
