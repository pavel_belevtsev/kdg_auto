//
//  APSSlider.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSSlider : UISlider

@end
