//
//  APSScrollView.m
//  Spas
//
//  Created by Israel Berezin on 8/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSScrollView.h"

@implementation APSScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    NSString * propertyName = NSStringFromProperty(contentOffset);
    [self.scrollView addObserver:self forKeyPath:propertyName options:NSKeyValueObservingOptionNew context:nil];
    [self updateGradient];
    [self changeScrollBarColorFor:self.scrollView];
}

-(void)updateGradient
{
    self.gradient = [CAGradientLayer layer];
    self.gradient.frame = self.bounds;
    self.gradient.startPoint = CGPointMake(0.0f, 0.70f);
    self.gradient.endPoint = CGPointMake(0.0f, 1.0f);
    self.gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.4 alpha:1.0] CGColor],(id)[[UIColor colorWithWhite:0.3 alpha:8.0] CGColor],(id)[[UIColor colorWithWhite:0.2 alpha:7.0] CGColor],(id)[[UIColor clearColor] CGColor],nil];

}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (self.scrollView.contentOffset.y+self.height<self.scrollView.contentSize.height)
    {
        [UIView animateWithDuration:0.05 animations:^{
            self.layer.mask =self.gradient;
        }];
        
    }
    else
    {
        [UIView animateWithDuration:0.05 animations:^{
            self.layer.mask = 0;
        }];
    }
}


- (void)changeScrollBarColorFor:(UIScrollView *)scrollView
{
    for ( UIView *view in scrollView.subviews )
    {
        if (view.tag == 0 && [view isKindOfClass:UIImageView.class])
        {
            
            UIImageView *imageView = (UIImageView *)view;
            imageView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
            imageView.width = 3.0;
            imageView.layer.cornerRadius = 2.0;
            imageView.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:0.5].CGColor;
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (void) dealloc
{
    NSString * propertyName = NSStringFromProperty(contentOffset);
    [self.scrollView removeObserver:self forKeyPath:propertyName];
}



@end
