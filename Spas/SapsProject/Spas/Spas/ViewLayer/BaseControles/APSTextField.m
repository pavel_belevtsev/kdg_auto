//
//  APSTextField.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTextField.h"
#import <QuartzCore/QuartzCore.h>

@interface APSTextField()

@property (nonatomic, copy) NSString *placeHolderText;
@end

@implementation APSTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    if (is_iPad())
    {
        self.layer.cornerRadius = 4;
        self.font = [APSTypography fontRegularWithSize:17];
    }
    else
    {
        self.layer.cornerRadius = 3;
        self.font = [APSTypography fontRegularWithSize:16];
    }
    
    [self inactiveDesign];
    
}
- (void)drawRect:(CGRect)rect
{
    
    [super drawRect:rect];
    // Drawing code
    
}


-(BOOL)becomeFirstResponder
{
   BOOL enable = [super becomeFirstResponder];
    if (enable)
    {
        [self activeDesign];
    }
    return enable;
}

-(BOOL) resignFirstResponder
{
    BOOL enable = [super resignFirstResponder];
    if (enable)
    {
        [self inactiveDesign];
    }
    
    return enable;
}

-(void) activeDesign
{
    self.placeHolderText = self.placeholder;
    self.backgroundColor = [APSTypography colorVeryVeryLightGray];
    self.textColor = [APSTypography colorDrakGray];
    self.layer.borderColor = [[APSTypography colorBrand] CGColor];
    self.layer.borderWidth = 0.5;
    self.placeholder = @"";
    self.textAlignment = NSTextAlignmentLeft;
}

-(void) inactiveDesign
{
    self.backgroundColor = [APSTypography colorVeryLightGray];
    self.textColor = [APSTypography colorLightGray];
    self.layer.borderWidth = 0;
    if ([self.text length]==0)
    {
        self.placeholder = self.placeHolderText;
        self.textAlignment = NSTextAlignmentCenter;

    }
}


- (CGRect)textRectForBounds:(CGRect)bounds
{
    if (is_iPad())
    {
        CGRect newBounds = CGRectInset( bounds , 17 , 5 );
        return newBounds;
    }
    else
    {
        CGRect newBounds = CGRectInset( bounds , 10 , 5 );
        return newBounds;
    }
}

-(CGRect) editingRectForBounds:(CGRect)bounds
{
    if (is_iPad())
    {
        CGRect newBounds = CGRectInset( bounds , 17 , 5 );
        return newBounds;
    }
    else
    {
        CGRect newBounds = CGRectInset( bounds , 10 , 5 );
        return newBounds;
    }
}



@end
