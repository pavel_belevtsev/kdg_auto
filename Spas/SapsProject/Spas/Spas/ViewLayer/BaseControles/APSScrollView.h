//
//  APSScrollView.h
//  Spas
//
//  Created by Israel Berezin on 8/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSScrollView : UIView<UIScrollViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView * scrollView;
@property (retain, nonatomic) CAGradientLayer * gradient;

-(void)updateGradient;

@end
