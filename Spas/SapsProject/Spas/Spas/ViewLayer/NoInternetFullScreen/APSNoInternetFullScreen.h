//
//  APSNoInternetFullScreen.h
//  Spas
//
//  Created by Israel Berezin on 10/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>


@class APSNoInternetFullScreen;

@protocol noInternetFullScreenDelegate <NSObject>

-(void) noInternetFullScreen:(APSNoInternetFullScreen *) programFullView didSelectReload:(UIButton*)sender;

@end

@interface APSNoInternetFullScreen : UIView

+(APSNoInternetFullScreen *) noInternetFullScreen;

@property (assign, nonatomic) id<noInternetFullScreenDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *NoInternetTitle;

@property (weak, nonatomic) IBOutlet UILabel *NoInternetDescription;
@property (weak, nonatomic) IBOutlet APSBrandedButton *btnReload;

@end
