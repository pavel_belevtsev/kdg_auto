//
//  APSNoInternetFullScreen.m
//  Spas
//
//  Created by Israel Berezin on 10/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSNoInternetFullScreen.h"
#import "APSSSLPinningManager.h"
#import "APSNetworkManager.h"

@implementation APSNoInternetFullScreen

+(APSNoInternetFullScreen *) noInternetFullScreen
{
    if(is_iPad())
    {
        return [APSNoInternetFullScreen programFullView_ipad];
    }
    else
    {
        return [APSNoInternetFullScreen programFullView_iPhone];
    }
}

+(APSNoInternetFullScreen *) programFullView_ipad
{
    APSNoInternetFullScreen *view = nil;
    NSString *nibName = @"APSNoInternetFullScreen_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[APSTypography colorBackground]];
    return view;
}

+(APSNoInternetFullScreen *) programFullView_iPhone
{
    APSNoInternetFullScreen *view = nil;
    NSString *nibName = @"APSNoInternetFullScreen_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[APSTypography colorBackground]];
    return view;
}

-(void)awakeFromNib
{
    self.backgroundColor = [APSTypography colorBackground];
    
    [self.NoInternetTitle setTextColor:[APSTypography colorLightGray]];
    [self.NoInternetTitle setFont:[APSTypography fontMediumWithSize:19.0]];
    
    [self.NoInternetDescription setTextColor:[APSTypography colorLightGray]];
    [self.NoInternetDescription setFont:[APSTypography fontRegularWithSize:16]];
    
    
    NSString *buttonText = [APSSSLPinningManager manager].SSLFailed ? NSLocalizedString(@"OK", nil) : [NSLocalizedString(@"Reload", nil) uppercaseString];
    
    
    [self.btnReload setTitle:buttonText forState:UIControlStateNormal];
    if(is_iPad())
    {
        self.btnReload.titleLabel.attributedText =[APSTypography attributedString:buttonText withLetterSpacing:1.5];
    }
    else
    {
         self.btnReload.titleLabel.attributedText =[APSTypography attributedString:buttonText withLetterSpacing:1.0];
    }
    self.btnReload.titleLabel.font = [APSTypography fontRegularWithSize:15];
    
    
    APSNetworkStatus status = [[APSNetworkManager  sharedNetworkManager] currentNetworkStatus];

    BOOL backendIsUnreachable = (status != TVNetworkStatus_NoConnection);
    
    if (backendIsUnreachable) {
        
        self.NoInternetTitle.text = @"";
        self.NoInternetDescription.text = NSLocalizedString(@"Backend is unreachable", nil);
            
    } else {
        
        self.NoInternetTitle.text = NSLocalizedString(@"No Internet Connection", nil);
        
        if ([APSSSLPinningManager manager].SSLFailed) {
            self.NoInternetDescription.text = NSLocalizedString(@"Problem in communication with servers. Please check for the latest version (16)", nil);
        } else {
            self.NoInternetDescription.text = NSLocalizedString(@"Yellow TV is not available while you're offline. Please connect to the internet and try again.", nil);
        }
        
    }

}

- (IBAction)reloadApp:(id)sender
{
    
    if ([APSSSLPinningManager manager].SSLFailed) {
        
        [[APSSSLPinningManager manager] goToAppStoreAndExit];
        
    } else {
        
        if ([self.delegate respondsToSelector:@selector(noInternetFullScreen:didSelectReload:)])
        {
            [self.delegate noInternetFullScreen:self didSelectReload:sender];
        }
        
    }

}

@end
