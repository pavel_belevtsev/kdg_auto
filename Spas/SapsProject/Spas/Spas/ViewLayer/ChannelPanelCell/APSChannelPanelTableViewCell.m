//
//  APSChannelPanelTableViewCell_iPad.m
//  Spas
//
//  Created by Israel Berezin on 8/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSChannelPanelTableViewCell.h"
#import "APSTypography.h"

@implementation APSChannelPanelTableViewCell

+(APSChannelPanelTableViewCell *) channelPanelCell
{
    if(is_iPad())
    {
        return [APSChannelPanelTableViewCell channelPanelCell_ipad];
    }
    else
    {
        return [APSChannelPanelTableViewCell channelPanel_iPhone];
    }
}


+(APSChannelPanelTableViewCell *) channelPanelCell_ipad
{
    APSChannelPanelTableViewCell *view = nil;
    NSString *nibName = @"APSChannelPanelTableViewCell_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    view.backgroundColor = [UIColor clearColor];
    view.contentView.backgroundColor = [UIColor clearColor];
    return view;
}

+(APSChannelPanelTableViewCell *) channelPanel_iPhone
{
    APSChannelPanelTableViewCell *view = nil;
    NSString *nibName = @"APSChannelPanelTableViewCell_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    view.backgroundColor = [UIColor clearColor];
    view.contentView.backgroundColor = [UIColor clearColor];
    return view;
}


- (void)awakeFromNib
{
    if (is_iPad())
    {
        [self iPadUI];
    }
    else
    {
        [self iPhoneUI];
    }
}


-(void)iPadUI
{
    self.lblChannelNumber.font= [APSTypography fontRegularWithSize:13];
}

-(void)iPhoneUI
{
    self.lblChannelNumber.font= [APSTypography fontRegularWithSize:11];
}

-(void)updateCellWithChannel:(TVMediaItem*)channel
{
    self.channelMedia = channel;
    self.logoImage.image = nil;
    [self.logoImage setImageWithURL:[self.channelMedia pictureURLForSizeWithRetinaSupport:self.logoImage.size] placeholderImage:nil];
    NSString * channelNumber = [self.channelMedia.metaData objectForKey:@"Channel number"];
    [self.lblChannelNumber setText:channelNumber];
}

- (IBAction)didSelectChannel:(id)sender
{
    [self.delegate miniEPGProgramsTableViewCell:self didSelectChannel:self.channelMedia];
}

- (void)updateCellUiIsSelectedCell:(BOOL)selected
{
    if (selected)
    {
        [self.contentView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.1]];
       // [self setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.1]];
        self.lblChannelNumber.textColor = [UIColor whiteColor];

    }
    else
    {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        //[self setBackgroundColor:[UIColor clearColor]];
        self.lblChannelNumber.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];

    }
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if (highlighted)
    {
        self.alpha = 0.5;
    }
    else
    {
        self.alpha = 1.0;
    }
}
//-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    BOOL toReturen = [super beginTrackingWithTouch:touch withEvent:event];
//    self.alpha = 0.5;
//    return toReturen;
//}
//
//- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
//{
//    [super endTrackingWithTouch:touch withEvent:event];
//    self.alpha = 1.0;
//}
@end
