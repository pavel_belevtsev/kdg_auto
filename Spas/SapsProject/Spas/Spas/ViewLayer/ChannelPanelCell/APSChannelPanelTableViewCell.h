//
//  APSChannelPanelTableViewCell_iPad.h
//  Spas
//
//  Created by Israel Berezin on 8/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSMiniEpgBaseCell.h"

@interface APSChannelPanelTableViewCell : APSMiniEpgBaseCell

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

@property (weak, nonatomic) IBOutlet UILabel *lblChannelNumber;

@property (assign, nonatomic) id<APSMiniEPGProgramsTableViewCellDelegate> delegate;


+(APSChannelPanelTableViewCell *) channelPanelCell;
- (IBAction)didSelectChannel:(id)sender;
-(void)updateCellWithChannel:(TVMediaItem*)channel;
- (void)updateCellUiIsSelectedCell:(BOOL)selected;
@end
