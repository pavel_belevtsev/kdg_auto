//
//  APSWelcomeView.m
//  Spas
//
//  Created by Israel Berezin on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSWelcomeView.h"

@implementation APSWelcomeView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(APSWelcomeView *)welcomeView
{
    if (is_iPad())
    {
        return [APSWelcomeView loginWebView_iPad];
    }
    else
    {
        return [APSWelcomeView loginWebView_iPhone];
    }
}

+(APSWelcomeView *) loginWebView_iPad
{
    APSWelcomeView *view = nil;
    NSString *nibName = @"APSWelcomeView_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

+(APSWelcomeView *) loginWebView_iPhone
{
    APSWelcomeView *view = nil;
    NSString *nibName = @"APSWelcomeView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    return view;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 8;
    if(is_iPad())
    {
        [self setupIpadView];
    }
    else
    {
        [self setupIphoneView];
    }
    
    [self updateTextInUI];
}

-(void)updateTextInUI
{
    [self.ButtonWClink setTitle:[APSTypography stringCompanyWebHome] forState:UIControlStateNormal];
//    self.buttonStartWatching.titleLabel.attributedText = [APSTypography attributedString:[NSLocalizedString(@"START WATCHING", nil) uppercaseString]
//                                                               withLetterSpacing:1.5];
    [self.buttonStartWatching setTitle:[NSLocalizedString(@"START WATCHING", nil) uppercaseString] forState:UIControlStateNormal];
   
    NSString* str = [NSLocalizedString(@"START WATCHING", nil) uppercaseString];
    if(is_iPad())
    {
        self.buttonStartWatching.titleLabel.attributedText =[APSTypography attributedString:str withLetterSpacing:2.0];
        self.buttonStartWatching.titleLabel.font = [APSTypography fontRegularWithSize:17];
    }
    else
    {
        self.buttonStartWatching.titleLabel.attributedText =[APSTypography attributedString:str withLetterSpacing:2.0];
        self.buttonStartWatching.titleLabel.font = [APSTypography fontRegularWithSize:16];
    }
    
    
    self.lblWCtitle.text = NSLocalizedString(@"Welcome to KDG", nil);
    
    NSString* welcomeTextFormat = @"%@\r\r%@\r\r%@";
    NSString* string1 = NSLocalizedString(@"As a KDG subscriber,  you will have access to over 30 live channels at home.", nil);
    NSString* string2 = NSLocalizedString(@"This account will be used as the administartor for this paving household.", nil);
    NSString* string3 = NSLocalizedString(@"To change device, edit famliy setting and associate new users to this acount, please visit:", nil);

    self.lblWCfirstParagraph.text = [NSString stringWithFormat:welcomeTextFormat,  string1, string2,  string3];
    [self.lblWCfirstParagraph sizeToFit];
    
//    if(!is_iPad()){
//        self.lblWCfirstParagraph.attributedText.paragraphSpacing = 7;
//    }
}

-(void)setupIpadView
{

    self.lblWCtitle.font =[APSTypography fontMediumWithSize:23];
    self.lblWCtitle.textColor = [APSTypography colorMediumGray];
    
    self.lblWCfirstParagraph.font =[APSTypography fontMediumWithSize:18];
    self.lblWCfirstParagraph.textColor = [APSTypography colorLightGray];
    
    self.ButtonWClink.titleLabel.font =[APSTypography fontMediumWithSize:16];
    self.ButtonWClink.titleLabel.textColor = [APSTypography colorMediumGray];
    
}

-(void)setupIphoneView
{
    
    self.lblWCtitle.font =[APSTypography fontMediumWithSize:19];
    self.lblWCtitle.textColor = [APSTypography colorMediumGray];
    
    self.lblWCfirstParagraph.font =[APSTypography fontMediumWithSize:14];
    self.lblWCfirstParagraph.textColor = [APSTypography colorLightGray];
    
    self.ButtonWClink.titleLabel.font =[APSTypography fontMediumWithSize:16];
    self.ButtonWClink.titleLabel.textColor = [APSTypography colorMediumGray];
}

- (IBAction)startWatching:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(welcomeView:didSelectStart:)])
    {
        [self.delegate welcomeView:self didSelectStart:sender];
    }
}

@end
