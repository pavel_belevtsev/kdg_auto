//
//  APSWelcomeView.h
//  Spas
//
//  Created by Israel Berezin on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSButtonLink.h"

@class APSWelcomeView;


@protocol APSWelcomeViewDelegate <NSObject>

-(void) welcomeView:(APSWelcomeView *)welcomeView didSelectStart:(UIButton *) button;

@end

@interface APSWelcomeView : UIView

@property (assign, nonatomic) IBOutlet id<APSWelcomeViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblWCtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWCfirstParagraph;
@property (weak, nonatomic) IBOutlet APSButtonLink *ButtonWClink;
@property (weak, nonatomic) IBOutlet APSBrandedButton *buttonStartWatching;

+(APSWelcomeView *)welcomeView;

@end
