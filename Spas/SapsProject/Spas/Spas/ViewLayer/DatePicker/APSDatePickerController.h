//
//  APSDatePickerController.h
//  CustomDatePicker
//
//  Created by Aviv Alluf on 11/4/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// APSDatePickerController will take a UIPickerView and turn it into a customized "UIDatePicker" like class, displaying dates + time
// from current time with "numberOfDays" days forward.

@interface APSDatePickerController : NSObject<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) UIPickerView *pickerView;
@property NSInteger numberOfDays;   // number of days that will appear in the date picker, default is 7

@property(strong, nonatomic) NSDate* selectedDate;
- (void) setSelectedDate:(NSDate*) date animate: (BOOL) animate;

@end
