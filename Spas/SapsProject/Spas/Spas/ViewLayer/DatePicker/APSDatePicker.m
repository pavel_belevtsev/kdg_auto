//
//  APSDatePicker.m
//  Spas
//
//  Created by Rivka Peleg on 9/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSDatePicker.h"
#import "APSDatePickerController.h"
@interface APSDatePicker ()

@property (weak, nonatomic) IBOutlet UIButton *buttonNow;
@property (weak, nonatomic) IBOutlet UIButton *buttonGo;
@property (strong, nonatomic) APSDatePickerController* pickerController;
@end

@implementation APSDatePicker

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.buttonGo.titleLabel.font = [APSTypography fontRegularWithSize:self.buttonGo.titleLabel.font.pointSize];
    self.buttonNow.titleLabel.font = [APSTypography fontRegularWithSize:self.buttonGo.titleLabel.font.pointSize];
    [self.buttonGo  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.buttonNow  setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.buttonNow setTitle:NSLocalizedString(@"NOW", nil) forState:UIControlStateNormal];
    [self.buttonGo setTitle:NSLocalizedString(@"Go", nil) forState:UIControlStateNormal];

    self.pickerController = [[APSDatePickerController alloc] init];
    self.pickerController.pickerView = self.pickerView;
}

-(void) animateIn:(UIView *) superView withCompletion:(void(^)(void)) completion
{
    self.frame = CGRectMake(0, 0, superView.bounds.size.width, superView.bounds.size.height);
    self.viewDatePickerPlaceHolder.y = self.bounds.size.height;
    self.alpha = 0;
    [superView addSubview:self];

    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.viewDatePickerPlaceHolder.y = self.bounds.size.height-self.viewDatePickerPlaceHolder.size.height;
        }completion:^(BOOL finished) {
            
            if (completion)
            {
                completion();
            }
        }];

    }];
    
    
}

-(void) animateOut:(UIView *) superView withCompletion:(void(^)(void)) completion
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.viewDatePickerPlaceHolder.y = self.bounds.size.height;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 0;
        }completion:^(BOOL finished) {
            
             [self removeFromSuperview];
            if (completion)
            {
                completion();
            }
           
        }];

    }];
    

}

-(IBAction)now:(id)sender
{
    NSDate * now = [NSDate date];
    [UIView  animateWithDuration:0.3 animations:^{
        self.pickerController.selectedDate = now;
    } completion:^(BOOL finished) {
 [self.delegate datePicker:self didSelectDate:now];
    }];
    
}

-(IBAction)go:(id)sender
{
    [self.delegate datePicker:self didSelectDate:self.pickerController.selectedDate];
}

-(IBAction)tonight:(id)sender
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    components.minute = 0;
    components.second = 0;
    components.hour = 20;
    
    NSDate * date = [calendar dateFromComponents:components];
    
    [UIView  animateWithDuration:0.3 animations:^{
        [self.pickerController setSelectedDate:date animate:YES];
    } completion:^(BOOL finished) {
        [self.delegate datePicker:self didSelectDate:date];
    }];


    

}

-(IBAction)cancel:(id)sender
{
    [self.delegate  datePicker:self cancel:sender];
}

-(void)updateSelectTime:(NSDate *)date
{
    [self.pickerController setSelectedDate:date animate:YES];
}
@end
