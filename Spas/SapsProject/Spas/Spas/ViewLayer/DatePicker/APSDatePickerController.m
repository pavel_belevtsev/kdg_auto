//
//  APSDatePickerController.m
//  CustomDatePicker
//
//  Created by Aviv Alluf on 11/4/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "APSDatePickerController.h"

#define dayInSeconds  24*60*60
#define hourInSeconds  60*60
#define minuteInSeconds  60

@interface APSDatePickerController ()

@property NSArray* arrayOfDates;
@property NSDate* creationDate; // the date/time the view was loaded
@property NSDate* startOfDay;   // the date/time the view was loaded with hours/minutes

@end


@implementation APSDatePickerController

- (id) init
{
    self = [super init];
    if( self ) {
        _numberOfDays = 7;
    }
    return self;
}

- (void) setPickerView:(UIPickerView *)pickerView
{
    _pickerView = pickerView;
    
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    // save the date of the view creation
    self.creationDate = [NSDate date];
    
    // save the date of the view creation With zero seconds/minutes/hours i.e. start of day
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit) fromDate:self.creationDate];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    self.startOfDay =  [[NSCalendar currentCalendar] dateFromComponents:components];
    
    // create array of titles for the date component
    NSMutableArray* newArray = [NSMutableArray arrayWithCapacity:self.numberOfDays];
    // add "Today"
    newArray[0] = NSLocalizedString(@"Today", nil);
    
    // add the remaining dates
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EE d.MMM"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"Application Locale", nil)];
    
    for (int i = 1; i<= self.numberOfDays; i++) {
        NSDate* wantedDate = [[NSDate date] dateByAddingTimeInterval:dayInSeconds*i];
        newArray [i] = [dateFormatter stringFromDate:wantedDate];
    }
    
    self.arrayOfDates = newArray;
    [self setPickerToNowWithAnimation:NO];
    [self updateSelectedDate];
}

#pragma mark - UIPickerViewDataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3; // 0: date, 1: Hours, 2: Minutes
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0: // dates - 7 days
            return self.arrayOfDates.count;
            break;
        case 1: // hours
            return 24;
            break;
        case 2: // minutes
            return 60;
            break;
            
        default:
            break;
    }
    return 0;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat widthOfDayColomn = 60;
    switch (component) {
        case 0: // dates - 7 days
            return self.pickerView.frame.size.width - 2*widthOfDayColomn;
            break;
        case 1: // hours
        case 2: // minutes
            return widthOfDayColomn;
            break;
            
        default:
            break;
    }
    return 0;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* title = nil;
    switch (component) {
        case 0: // dates - 7 days
            title = self.arrayOfDates[row];
            break;
        case 1: // hours
            title = [NSString stringWithFormat:@"%02d", row];
//            title = [@(row+1) stringValue];
        case 2: // minutes
            title = [NSString stringWithFormat:@"%02d", row];
            break;
            
        default:
            break;
    }
    return title;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    lbl.adjustsFontSizeToFitWidth = YES;
    lbl.textAlignment=(component == 0)? NSTextAlignmentRight:NSTextAlignmentCenter;
    lbl.font=[UIFont systemFontOfSize:24];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // user selected today I want invalidate some of the hours.
    [self updateSelectedDate];
    if ([_selectedDate timeIntervalSinceDate:self.creationDate] <0) {
        [self setPickerToNowWithAnimation:YES];
    }
}

#pragma mark -
- (void) setPickerToNowWithAnimation: (BOOL) animate;
{
    NSDate* date = [NSDate date];
    NSDateComponents * components = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    [self.pickerView selectRow:0 inComponent:0 animated:animate];
    [self.pickerView selectRow:[components hour] inComponent:1 animated:animate];
    [self.pickerView selectRow:[components minute] inComponent:2 animated:animate];
    [self updateSelectedDate];
}

- (void) updateSelectedDate
{
    NSInteger selectedComponent0 = [self.pickerView selectedRowInComponent:0];
    NSInteger selectedComponent1 = [self.pickerView selectedRowInComponent:1];
    NSInteger selectedComponent2 = [self.pickerView selectedRowInComponent:2];
    
    _selectedDate = [self.startOfDay dateByAddingTimeInterval:
                            dayInSeconds * selectedComponent0 +
                            hourInSeconds * selectedComponent1 +
                            minuteInSeconds * selectedComponent2];
}

- (void) setSelectedDate:(NSDate*) date animate: (BOOL) animate
{
    NSDateComponents * components = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    [self.pickerView selectRow:0 inComponent:0 animated:animate];
    [self.pickerView selectRow:[components hour] inComponent:1 animated:animate];
    [self.pickerView selectRow:[components minute] inComponent:2 animated:animate];
    _selectedDate = date;
}

- (void) setSelectedDate:(NSDate*) date
{
    [self setSelectedDate:date animate:NO];
}

@end
