//
//  APSDatePicker.h
//  Spas
//
//  Created by Rivka Peleg on 9/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSDatePicker;

@protocol APSDatePickerDelegate <NSObject>

-(void) datePicker:(APSDatePicker *) datePicker didSelectDate:(NSDate *) date;
-(void) datePicker:(APSDatePicker *) datePicker cancel:(id) sender;


@end

@interface APSDatePicker : UIView


@property (weak, nonatomic) IBOutlet id<APSDatePickerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIPickerView * pickerView;
@property (weak, nonatomic) IBOutlet UIView * viewDatePickerPlaceHolder;


-(void) animateIn:(UIView *) superView withCompletion:(void(^)(void)) completion;
-(void) animateOut:(UIView *) superView withCompletion:(void(^)(void)) completion;

-(IBAction)now:(id)sender;
-(IBAction)go:(id)sender;
-(IBAction)tonight:(id)sender;
-(void)updateSelectTime:(NSDate *)date;

@end
