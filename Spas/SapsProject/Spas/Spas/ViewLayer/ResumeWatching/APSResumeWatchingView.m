//
//  APSResumeWatchingView.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSResumeWatchingView.h"

@implementation APSResumeWatchingView


+(APSResumeWatchingView *) resumeWatchingView
{
    NSArray * views =nil;
    APSResumeWatchingView * outOfNetowrkView = nil;
    if (is_iPad())
    {
        views =  [[NSBundle mainBundle] loadNibNamed:@"APSResumeWatching_iPad" owner:nil options:nil];
    }
    else
    {
        views =  [[NSBundle mainBundle] loadNibNamed:@"APSResumeWatching_iPhone" owner:nil options:nil] ;
    }
    
    for (UIView  * view in views)
    {
        if ([view isKindOfClass:[APSResumeWatchingView class]])
        {
            outOfNetowrkView = (APSResumeWatchingView *)view;
            break;
        }
    }
    
    return outOfNetowrkView;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    [self.buttonDismiss setTitle:NSLocalizedString(@"Resume watching", nil) forState:UIControlStateNormal];
    self.buttonDismiss.titleLabel.font = [APSTypography fontLightWithSize:is_iPad()?22:20];
    [self.buttonDismiss setTitleColor:[APSTypography colorBrand] forState:UIControlStateNormal];
    
    self.buttonDismiss.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.14];

    self.buttonDismiss.layer.cornerRadius = is_iPad()?30:25;
    [self.buttonDismiss.layer setMasksToBounds:YES];
}

- (void) viewDidLoad
{
    self.buttonDismiss.titleLabel.textColor = [APSTypography colorBrand];
}

- (IBAction)onResumeWatch:(id)sender {
    if ([self.delegate respondsToSelector:@selector(resumeWatch:)])
    {
        [self.delegate resumeWatch:self];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView == self) return nil;
    return hitView;
}

@end
