//
//  APSResumeWatchingView.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSPopupView.h"

@class APSResumeWatchingView;

@protocol APSResumeWatchingViewDelegate <NSObject>
-(void) resumeWatch:(id) sender;
@end

@interface APSResumeWatchingView : UIView

@property (assign, nonatomic) id<APSResumeWatchingViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton * buttonDismiss;

- (IBAction)onResumeWatch:(id)sender;

+(APSResumeWatchingView *) resumeWatchingView;



@end
