//
//  RSHorizontalTableViewCellProperties.h
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RSHorizontalTableViewCell;

@interface RSHorizontalTableViewCellProperties : NSObject
@property (assign, nonatomic) RSHorizontalTableViewCell * cell;
@property (assign, nonatomic) CGRect frame;
@property (assign, nonatomic) NSInteger index;

@end
