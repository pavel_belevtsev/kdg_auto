//
//  RSHorizontalTableViewCell.h
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSHorizontalTableViewCellProperties;

@interface RSHorizontalTableViewCell : UIView
@property (retain, nonatomic) NSString * reuseIdentifier;



@end
