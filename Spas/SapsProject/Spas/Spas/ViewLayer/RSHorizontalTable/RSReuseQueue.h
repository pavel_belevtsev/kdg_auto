//
//  RSReuseQueue.h
//  YES_iPad
//
//  Created by Rivka S. Peleg on 10/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSReuseQueue : NSObject



+ (instancetype)sharedReuseQueue;
-(id) pop;
-(void) push:(id) object;
-(id) lastObject;
-(NSInteger) size;
-(bool) isEmpty;

@end
