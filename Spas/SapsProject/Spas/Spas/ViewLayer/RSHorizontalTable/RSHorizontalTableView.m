//
//  RSHorizontalTableView.m
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import "RSHorizontalTableView.h"
#import "RSHorizontalTableViewCellProperties.h"
#import "RSHorizontalTableViewCell.h"
#import "RSReuseQueue.h"
#import <QuartzCore/QuartzCore.h>


@interface RSHorizontalTableView ()

@property (retain, nonatomic) NSMutableArray * arrayOfCellsProperties;
@property (retain, nonatomic) NSMutableArray * arrayOfVisableCellsProperties;
//@property (retain, atomic) NSMutableArray * reuseQueue;





@end


@implementation RSHorizontalTableView

#pragma mark - deallocation
-(void)dealloc
{
    // [_reuseQueue release];
    [_arrayOfVisableCellsProperties release];
    [_arrayOfCellsProperties release];
    [super dealloc];
}

#pragma mark - Initialization


-(id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self setup];
    }
    
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) setup
{
    self.clipsToBounds = YES;
    // self.reuseQueue = [[[NSMutableArray alloc] init] autorelease];
    self.arrayOfVisableCellsProperties = [[[NSMutableArray alloc] init] autorelease];
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator =NO;
    
}

#pragma mark - life sycle
-(void)awakeFromNib
{
    
    [self reloadData];
}


#pragma mark getters & setters

-(void)setContentOffset:(CGPoint)contentOffset
{
    CGPoint oldContentOffset = self.contentOffset;
    
    [super setContentOffset:contentOffset];
    
    if(self.delegate && (oldContentOffset.x != contentOffset.x || oldContentOffset.y != contentOffset.y))
    {
        [self handleVisableCells];
        
    }
    
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    CGSize contentSize =  self.contentSize;
    contentSize.height = self.frame.size.height;
    self.contentSize = contentSize;
    
}



#pragma mark - data Management

-(void) clearData
{
    
    for (RSHorizontalTableViewCellProperties * visableCellProperties in self.arrayOfVisableCellsProperties)
    {
        [[RSReuseQueue sharedReuseQueue] push:visableCellProperties.cell];
        //[self.reuseQueue addObject:visableCellProperties.cell];
        [visableCellProperties.cell removeFromSuperview];
        visableCellProperties.cell = nil;
        
    }
    
    [self.arrayOfCellsProperties removeAllObjects];
    [self.arrayOfVisableCellsProperties removeAllObjects];
    self.arrayOfCellsProperties = nil;
    self.arrayOfVisableCellsProperties = nil;
    
}


-(void) reloadData
{
     [self clearData];
    
    NSMutableArray * cellsPropertiesArray ;
    NSInteger numbersOfColumns = [self.dataSource horizontalTableViewNumberOfColumns:self];
    
    
    if ([self.delegate respondsToSelector:@selector(horizontalTableViewCellsProperties:)])
    {
        cellsPropertiesArray = [[self.delegate horizontalTableViewCellsProperties:self] mutableCopy];
    }
    else
    {
        
        cellsPropertiesArray = [[NSMutableArray alloc] init];
        NSInteger height = self.frame.size.height;
        NSInteger width = 0;
        
        NSInteger xOrigin = 0;
        
        for (NSInteger index = 0 ; index<numbersOfColumns; index++)
        {
            
            width = [self.delegate horizontalTableView:self widthForColumnAtIndex:index];
            //NSInteger xOrigin =  [self.delegate horizontalTableView:self xOriginForColumnAtIndex:index];
            
            RSHorizontalTableViewCellProperties * cellProperties = [[RSHorizontalTableViewCellProperties alloc] init];
            
            cellProperties.frame = CGRectMake(xOrigin, 0, width, height);
            cellProperties.index = index;
            
            xOrigin = xOrigin + width;
            
            [cellsPropertiesArray addObject:cellProperties];
            
            [cellProperties release];
        }
        
        
    }
    
    self.arrayOfCellsProperties = cellsPropertiesArray;
    self.arrayOfVisableCellsProperties = [NSMutableArray array];
    [cellsPropertiesArray release];
    
    
    // set contentsize:
    CGFloat contentWidth = [self.delegate contentWidthForHorizontalTableView:self];
    [self setContentSize:CGSizeMake(contentWidth, self.frame.size.height)];
    
    [self handleVisableCells];
    
}

-(void) handleVisableCells
{
    CGRect visableRect = CGRectMake(self.contentOffset.x, self.contentOffset.y, self.frame.size.width, self.frame.size.height);
    
    NSMutableIndexSet *indexSetToDelete = [NSMutableIndexSet indexSet];
    
    //remove cells with no need :
    for ( int i=0 ; i<self.arrayOfVisableCellsProperties.count ; i++ )
    {
        RSHorizontalTableViewCellProperties * visableCellProperties = self.arrayOfVisableCellsProperties[i];
        
        if (CGRectIntersectsRect(visableRect,visableCellProperties.frame) == NO)
        {
            // insert view to reuse
            [[RSReuseQueue sharedReuseQueue] push:visableCellProperties.cell];
            // [self.reuseQueue addObject:visableCellProperties.cell];
            [visableCellProperties.cell removeFromSuperview];
            visableCellProperties.cell = nil;
            
            [indexSetToDelete addIndex:i];
            
        }
    }
    
    [self.arrayOfVisableCellsProperties removeObjectsAtIndexes:indexSetToDelete];
    
    //add new cells that there is need
    
    NSArray * visableCells = [self cellPropertiesForElementsInRect:visableRect];
    
    for (RSHorizontalTableViewCellProperties * cellproperties in visableCells)
    {
        if (cellproperties.cell == Nil)
        {
            CGRect frame =  cellproperties.frame ;
            frame.size.height = self.height;
            cellproperties.frame = frame;
            
            RSHorizontalTableViewCell * cell = [self.dataSource HorizontalTableView:self cellForColumnAtIndex:cellproperties.index]  ;
            [cell.layer removeAllAnimations];
            [self addGestureRecognizerForCellIfneeded:cell];
            cell.frame = cellproperties.frame;
            [cell.layer removeAllAnimations];
            [self addSubview:cell];
            cellproperties.cell = cell;
            [self.arrayOfVisableCellsProperties addObject:cellproperties];
            
        }
    }
}

-(void) addGestureRecognizerForCellIfneeded:(RSHorizontalTableViewCell *) cell
{
    [cell removeGestureRecognizer:[[cell gestureRecognizers] lastObject]];
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    [cell addGestureRecognizer:tapGesture];
    [tapGesture release];
    
}


- (void)didTap:(UITapGestureRecognizer *)tapGesture
{
    NSInteger index = [self indexOFCell:(RSHorizontalTableViewCell *)tapGesture.view];
    
    if ([self.delegate respondsToSelector:@selector(horizontalTableView:didSelectColumnAtIndex:)])
    {
        [self.delegate horizontalTableView:self didSelectColumnAtIndex:index];
    }
}

-(NSInteger) indexOFCell:(RSHorizontalTableViewCell *) cell
{
    for (RSHorizontalTableViewCellProperties * cellProperties in self.arrayOfVisableCellsProperties)
    {
        if(cell == cellProperties.cell)
        {
            return cellProperties.index;
        }
    }
    
    return NSNotFound;
}




- (NSArray *)cellPropertiesForElementsInRect:(CGRect)rect
{
    NSMutableArray * visableCellsPropertiesArray = [[NSMutableArray alloc] init];
    for (RSHorizontalTableViewCellProperties * cellProperties in self.arrayOfCellsProperties)
    {
        if(CGRectIntersectsRect(rect,cellProperties.frame))
        {
            [visableCellsPropertiesArray addObject:cellProperties];
        }
    }
    
    return [visableCellsPropertiesArray autorelease];
}


#pragma mark - user handeling
- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier
{
    @synchronized(self)
    {
        id cell =nil;
        if( [[RSReuseQueue sharedReuseQueue] isEmpty] == NO/*self.reuseQueue.count >0*/)
        {
            cell = [[RSReuseQueue sharedReuseQueue] pop];
            /*[[[self.reuseQueue lastObject] retain] autorelease];
             [self.reuseQueue removeLastObject] ;*/
        }
        
        return cell;
    }
}


#pragma scroll option
- (void)scrollToRowAtIndexPath:(NSInteger)index atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    RSHorizontalTableViewCellProperties * cellProperties = self.arrayOfCellsProperties[index];
    NSInteger y =0;
    NSInteger x =0;
    CGPoint scrollPoint ;
    
    switch (scrollPosition) {
        case UITableViewScrollPositionBottom:
        case UITableViewScrollPositionTop:
        case UITableViewScrollPositionNone:
        case UITableViewScrollPositionMiddle:
        {
            
            x = cellProperties.frame.origin.x+cellProperties.frame.size.width/2-self.frame.size.width/2;
            scrollPoint = CGPointMake(x,y);
        }
            break;
            
        default:
            break;
    }
    
    
    if (animated)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self setContentOffset:scrollPoint];
        }
         
                         completion:^(BOOL finished) {
                             
                         }];
    }
    else
    {
        
        [self setContentOffset:scrollPoint];
        
        
    }
    
    if([self.delegate respondsToSelector:@selector(scrollViewDidScrollProgrammatically:)])
    {
        [self.delegate scrollViewDidScrollProgrammatically:self];
    }
    
    
}


- ( NSInteger )indexPathForVisableCell:( UIView *) cell
{
    for (RSHorizontalTableViewCellProperties * cellProperties in self.arrayOfVisableCellsProperties)
    {
        if (cellProperties.cell == cell)
        {
            return cellProperties.index;
        }
    }
    
    return NSNotFound;
}








@end
