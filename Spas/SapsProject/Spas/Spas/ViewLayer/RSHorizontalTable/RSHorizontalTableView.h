//
//  RSHorizontalTableView.h
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSHorizontalTableViewDataSource.h"
#import "RSHorizontalTableViewDelegate.h"

@interface RSHorizontalTableView : UIScrollView

@property (assign, nonatomic) IBOutlet id<RSHorizontalTableViewDataSource> dataSource;
@property (assign, nonatomic) IBOutlet id<RSHorizontalTableViewDelegate> delegate;


#pragma mark - handle Data
-(void) reloadData;
-(void) clearData;

#pragma mark - reuse
- (id)dequeueReusableCellWithIdentifier:(NSString *)identifier;


#pragma mark - scroll options
- (void)scrollToRowAtIndexPath:(NSInteger)index atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated;
- ( NSInteger )indexPathForVisableCell:( UIView *) cell;


@end
