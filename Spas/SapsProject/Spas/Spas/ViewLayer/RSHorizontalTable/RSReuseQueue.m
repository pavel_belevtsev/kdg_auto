//
//  RSReuseQueue.m
//  YES_iPad
//
//  Created by Rivka S. Peleg on 10/9/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "RSReuseQueue.h"

@interface RSReuseQueue ()

@property (retain, atomic ) NSMutableArray * reuseQueue;
@end

@implementation RSReuseQueue


+ (instancetype)sharedReuseQueue
{
    static RSReuseQueue *_sharedReuseQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedReuseQueue = [[RSReuseQueue alloc] init];
        _sharedReuseQueue.reuseQueue = [NSMutableArray array];
    });
    
    return _sharedReuseQueue;
}

-(id) pop
{
    @synchronized(self)
    {
        id object = [self.reuseQueue lastObject];
        [self.reuseQueue removeLastObject];
        return object;
    }
}
-(void) push:(id) object
{
    @synchronized(self)
    {
        [self.reuseQueue addObject:object];
    }
}
-(id) lastObject
{
    @synchronized(self)
    {
        return [self.reuseQueue lastObject];
    }
}
-(NSInteger) size
{
    @synchronized(self)
    {
        return self.reuseQueue.count;
    }
}
-(bool) isEmpty
{
    @synchronized(self)
    {
        return self.reuseQueue.count ==0;
    }
}


@end
