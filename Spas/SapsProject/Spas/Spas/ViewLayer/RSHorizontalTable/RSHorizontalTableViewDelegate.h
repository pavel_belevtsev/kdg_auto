//
//  RSHorizontalTableViewDelegate.h
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RSHorizontalTableView;

@protocol RSHorizontalTableViewDelegate <UIScrollViewDelegate>

@required
- (CGFloat)horizontalTableView:(RSHorizontalTableView *)tableView widthForColumnAtIndex:(NSInteger)indexPath;
- (CGFloat)horizontalTableView:(RSHorizontalTableView *)tableView xOriginForColumnAtIndex:(NSInteger)indexPath;
- (CGFloat)contentWidthForHorizontalTableView:(RSHorizontalTableView *)tableView;


@optional
- (void)horizontalTableView:(RSHorizontalTableView *)tableView didSelectColumnAtIndex:(NSInteger )indexPath;
- (void)scrollViewDidScrollProgrammatically:(UIScrollView *)scrollView;
-(NSArray *) horizontalTableViewCellsProperties:(RSHorizontalTableView *)tableView;

@end
