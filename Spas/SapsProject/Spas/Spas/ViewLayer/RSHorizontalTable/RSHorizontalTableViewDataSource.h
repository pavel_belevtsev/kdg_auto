//
//  RSHorizontalTableViewDataSource.h
//  HorisontalTableProject
//
//  Created by Rivka S. Peleg on 5/8/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RSHorizontalTableView;
@class RSHorizontalTableViewCell;

@protocol RSHorizontalTableViewDataSource <NSObject>

@required

- (NSInteger)horizontalTableViewNumberOfColumns:(RSHorizontalTableView *)tableView ;
- (RSHorizontalTableViewCell *)HorizontalTableView:(RSHorizontalTableView *)tableView cellForColumnAtIndex: (NSInteger)index;

@end
