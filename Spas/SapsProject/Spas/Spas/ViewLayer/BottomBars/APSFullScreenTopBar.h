//
//  APSFullScreenBottomBar.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSFullScreenTopBar;

@protocol APSFullScreenTopBarDelegate <NSObject>

-(UIView *) fullScreenTopBar:(APSFullScreenTopBar *) fullScreenTopBar underlyingViewForBlur:(APSBlurView *) blurView;

@end

@interface APSFullScreenTopBar : UIView


@property (assign, nonatomic) id<APSFullScreenTopBarDelegate>  delegate;

@property (weak, nonatomic) IBOutlet UIButton * buttonHide;
@property (weak, nonatomic) IBOutlet UIButton * buttonAllChannels;
@property (weak, nonatomic) IBOutlet UILabel * labelTitle;
@property (weak, nonatomic) IBOutlet APSBlurView * blurView;

-(void)updateAllChannelsButtonSlected:(BOOL)selected;




@end
