//
//  APSFullScreenBottomBar.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSFullScreenTopBar.h"

@implementation APSFullScreenTopBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    [self.buttonHide setTitle:[NSLocalizedString(@"HIDE", nil) uppercaseString]forState:UIControlStateNormal];
    if (is_iPad())
    {
        self.buttonHide.titleLabel.attributedText = [APSTypography attributedString:self.buttonHide.titleLabel.text withLetterSpacing:1.5];
    }
    else
    {
        self.buttonHide.titleLabel.attributedText = [APSTypography attributedString:self.buttonHide.titleLabel.text withLetterSpacing:1.0];
    }

    [self.buttonHide setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] forState:UIControlStateNormal];
    self.buttonHide.titleLabel.font = [APSTypography fontRegularWithSize:17];

    
    [self.buttonAllChannels setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1]];
    [self.buttonAllChannels setTitleColor:[APSTypography colorMediumGray] forState:UIControlStateNormal];
    [self.buttonAllChannels setTitle:[NSLocalizedString(@"ALL CHANNELS", nil) uppercaseString]forState:UIControlStateNormal];
    self.buttonAllChannels.titleLabel.font = [APSTypography fontRegularWithSize:12];
    self.blurView.layerColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
    self.blurView.blurRadius = 20;
}


-(void)setDelegate:(id<APSFullScreenTopBarDelegate>)delegate
{
    if (_delegate != delegate)
    {
        _delegate = delegate;
        self.blurView.underlyingView = [self.delegate fullScreenTopBar:self underlyingViewForBlur:self.blurView];
    }
}

-(void)updateAllChannelsButtonSlected:(BOOL)selected
{
    if (selected)
    {
        [self.buttonAllChannels setBackgroundColor:[APSTypography colorBrand]];
        [self.buttonAllChannels setTitleColor:[APSTypography colorBrand] forState:UIControlStateNormal];
        [self.buttonAllChannels setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        [self.buttonAllChannels setBackgroundColor:[APSTypography colorVeryLightGray]];
        [self.buttonAllChannels setTitleColor:[APSTypography colorBrand] forState:UIControlStateNormal];
        [self.buttonAllChannels setTitleColor:[APSTypography colorMediumGray] forState:UIControlStateNormal];
    }
}
@end
