//
//  APSButtonHighlighted.h
//  Spas
//
//  Created by Pavel Belevtsev on 06.05.15.
//  Copyright (c) 2015 Pavel Belevtsev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol APSButtonHighlightedDelegate <NSObject>

- (void)buttonSetHighlighted:(BOOL)highlighted;

@end

@interface APSButtonHighlighted : UIButton;

@property (nonatomic, weak) id <APSButtonHighlightedDelegate> delegate;

@end
