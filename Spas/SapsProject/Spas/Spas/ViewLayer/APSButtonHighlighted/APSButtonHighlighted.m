//
//  APSButtonHighlighted.m
//  Spas
//
//  Created by Pavel Belevtsev on 06.05.15.
//  Copyright (c) 2015 Pavel Belevtsev. All rights reserved.
//

#import "APSButtonHighlighted.h"

@implementation APSButtonHighlighted

- (void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(buttonSetHighlighted:)]) {
        [self.delegate buttonSetHighlighted:highlighted];
    }
}

@end
