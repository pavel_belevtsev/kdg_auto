//
//  APSFullScreenOutOfNetworkView.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSFullScreenOutOfNetworkView;

@protocol APSFullScreenOutOfNetworkViewDelegate <NSObject>
-(void) fullScreenOutOfNetworkView:(APSFullScreenOutOfNetworkView *) sender dismiss:(UIButton *) buttonSender;
@end

@interface APSFullScreenOutOfNetworkView : UIView

@property (assign, nonatomic) id<APSFullScreenOutOfNetworkViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel * labelTitle;
@property (weak, nonatomic) IBOutlet UILabel * labelBody;
@property (weak, nonatomic) IBOutlet UIButton * buttonDismiss;
@property (weak, nonatomic) IBOutlet APSBlurView * blurView;


-(IBAction)dismiss:(id)sender;

+(APSFullScreenOutOfNetworkView *) fullScreenOutOfNetworkView;



@end
