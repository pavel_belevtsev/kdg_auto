//
//  APSFullScreenOutOfNetworkView.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSFullScreenOutOfNetworkView.h"

@implementation APSFullScreenOutOfNetworkView


+(APSFullScreenOutOfNetworkView *) fullScreenOutOfNetworkView
{
    NSArray * views =nil;
    APSFullScreenOutOfNetworkView * outOfNetowrkView = nil;
    if (is_iPad())
    {
        views =  [[NSBundle mainBundle] loadNibNamed:NSStringFromClassWithiPadSuffix([APSFullScreenOutOfNetworkView class]) owner:nil options:nil];
    }
    else
    {
        views =  [[NSBundle mainBundle] loadNibNamed:NSStringFromClassWithiPhoneSuffix([APSFullScreenOutOfNetworkView class]) owner:nil options:nil] ;
        
    }
    
    for (UIView  * view in views)
    {
        if ([view isKindOfClass:[APSFullScreenOutOfNetworkView class]])
        {
            outOfNetowrkView = (APSFullScreenOutOfNetworkView *)view;
            break;
        }
    }
    
    return outOfNetowrkView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.blurView.layerColor =  [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65];
    self.blurView.blurRadius = 14;
    [self.blurView setNeedsDisplay];

    self.buttonDismiss.titleLabel.attributedText = [APSTypography attributedString:[NSLocalizedString(@"DISMISS", nil) uppercaseString] withLetterSpacing:2.0];
    [self.buttonDismiss setTitle:[NSLocalizedString(@"DISMISS", nil) uppercaseString]forState:UIControlStateNormal];
    self.buttonDismiss.titleLabel.font = [APSTypography fontRegularWithSize:is_iPad()?17:16];

    self.labelTitle.text = NSLocalizedString(@"There's No Place Like Home", nil);
    if (is_iPad())
    {
        self.labelBody.text = NSLocalizedString(@"To Watch KDG GO you must be at your home or on a KDG enabled household. \nSee you soon", nil);
    }
    else
    {
        self.labelBody.text = NSLocalizedString(@"To Watch KDG GO you must be at your home or on a KDG enabled household. See you soon", nil);
    }
    
   
    self.labelTitle.textColor = [APSTypography colorDrakGray];
    
    
    NSInteger strLength = [self.labelBody.text length];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.labelBody.text];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    
    if (is_iPad())
    {
        [style setMinimumLineHeight:21];
        [style setMaximumLineHeight:21];

        self.labelTitle.font = [APSTypography fontMediumWithSize:19];
        self.labelBody.font = [APSTypography fontRegularWithSize:16];
    }
    else
    {
        [style setMinimumLineHeight:16];
        [style setMaximumLineHeight:16];
        
         self.labelTitle.font = [APSTypography fontMediumWithSize:16];
        self.labelBody.font = [APSTypography fontRegularWithSize:14];
    }
    
    [attString addAttribute:NSParagraphStyleAttributeName
                      value:style
                      range:NSMakeRange(0, strLength)];
    
    self.labelBody.attributedText = attString;
    self.labelBody.textColor = [APSTypography colorMediumGray];
    
    
    
    
}

-(IBAction)dismiss:(id)sender
{
    [self.delegate fullScreenOutOfNetworkView:self dismiss:sender];
}




@end
