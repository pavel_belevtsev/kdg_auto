//
//  APSTopRootBar.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTopRootBar.h"

@implementation APSTopRootBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.labelTitle.textColor = [APSTypography colorBrand];
    self.labelTitle.font = [APSTypography fontRegularWithSize:17];
    self.searchField.textfieldSearch.returnKeyType = UIReturnKeySearch;

    self.menuButton.isAccessibilityElement = YES;
    self.menuButton.accessibilityLabel = @"Button Menu";

    self.searchButton.isAccessibilityElement = YES;
    self.searchButton.accessibilityLabel = @"Button Search";
    
}


+(APSTopRootBar *) topRootBar
{
    NSString * className = NSStringFromClass([APSTopRootBar class]);
    APSTopRootBar * topBar = [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:nil] lastObject];
    return topBar;
}


-(void) showSearchWithCompletion:(void(^)(void)) completion
{
    self.buttonCancelSearch.hidden = NO;
    self.searchField.hidden = NO;
    self.buttonCancelSearch.alpha = 0;
    self.searchField.alpha = 0;

    [UIView animateWithDuration:0.4 animations:^{
        
        self.searchButton.alpha = 0;
        self.menuButton.alpha = 0;
        self.labelTitle.alpha = 0;
        
        self.buttonCancelSearch.alpha = 1;
        self.searchField.alpha = 1;
        
    } completion:^(BOOL finished)
    {
        self.searchButton.hidden= YES;
        self.menuButton.hidden = YES;
        self.labelTitle.hidden = YES;
        self.searchField.active = YES;

        if (completion)
        {
            completion();
        }
    }];
}

-(void) hideSearchWithCompletion:(void(^)(void)) completion
{
    self.searchButton.hidden= NO;
    self.menuButton.hidden = NO;
    self.labelTitle.hidden = NO;
    self.searchField.active = NO;

    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.searchButton.alpha = 1;
        self.menuButton.alpha = 1;
        self.labelTitle.alpha = 1;
        
        self.buttonCancelSearch.alpha = 0;
        self.searchField.alpha = 0;
        
    } completion:^(BOOL finished) {
        self.buttonCancelSearch.hidden = YES;
        self.searchField.hidden = YES;
        
        if (completion)
        {
            completion();
        }
    }];
}

@end
