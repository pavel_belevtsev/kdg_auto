//
//  APSTopRootBar.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSGaryButton.h"
#import "APSTopBar.h"

@interface APSTopRootBar : APSTopBar

@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet APSGaryButton *buttonCancelSearch;
@property (weak, nonatomic) IBOutlet UIButton *buttonAppInfo;

+(APSTopRootBar *) topRootBar;

-(void) showSearchWithCompletion:(void(^)(void)) completion;
-(void) hideSearchWithCompletion:(void(^)(void)) completion;
@end
