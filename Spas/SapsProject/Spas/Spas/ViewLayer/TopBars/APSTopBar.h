//
//  APSTopBar.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/21/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>


@class APSBlurView;

@interface APSTopBar : UIView

@property (weak, nonatomic) IBOutlet APSSearchField *searchField;
@property (strong, nonatomic) IBOutlet APSBlurView * blurViewTopBar;

@end
