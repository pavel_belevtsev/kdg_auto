 //
//  APSTopBar.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/21/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTopBar.h"

@implementation APSTopBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.blurViewTopBar.layerColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
    self.blurViewTopBar.blurRadius = 3;
    
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.25;
    self.layer.shadowRadius = 1;
    self.searchField.textfieldSearch.enablesReturnKeyAutomatically = YES;
    
    self.searchField.textfieldSearch.isAccessibilityElement = YES;
    self.searchField.textfieldSearch.accessibilityLabel = @"Text Field Search";

}

@end
