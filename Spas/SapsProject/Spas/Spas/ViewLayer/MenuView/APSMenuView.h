//
//  APSTopBar.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSMainViewController.h"
#import "APSBrandedButton.h"
#import "APSSearchField.h"
#import "APSTopBar.h"

@class APSMenuView;
@class FXBlurView;


@protocol APSMenuViewDelegate <NSObject>
-(void) menuView:(APSMenuView *) menu didSelectDisplayPosition:(DisplayPosition) displayPostion;
-(NSInteger) menuViewNumberOfMenuItems:(APSMenuView *) menu;
-(NSString *) menuView:(APSMenuView *) menu titleForIndex:(NSInteger) index;
-(void) menuView:(APSMenuView *)menu didSelectClose:(UIButton *) button;
-(void) menuView:(APSMenuView *)menu didSelectSearch:(id) sender;
-(void) menuView:(APSMenuView *)menu didSelectSettings:(UIButton *) button;
-(void) menuView:(APSMenuView *)menu didSelectLogin:(UIButton *) button;


@end

@interface APSMenuView: APSTopBar

@property (weak, nonatomic) IBOutlet APSBrandedButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UIButton *buttonLoggedIn;

@property (assign, nonatomic) IBOutlet id<APSMenuViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTabSelector;
@property (weak, nonatomic) IBOutlet APSBlurView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancelSearch;
@property (weak, nonatomic) IBOutlet UIButton *buttonSettings;
@property (weak, nonatomic) IBOutlet UIButton *buttonAppInfo;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (strong, nonatomic) IBOutlet UILabel *fakeLabel;
-(void) setDisplayPosition:(DisplayPosition ) displayPosition;

+(APSMenuView *) menuView;
+(NSString *) nibName;
-(void)updateLoginButtonAfterLoginOrLogout;

// only for ipad
-(void) expandSearchWithCompletion:(void(^)(void))completion;
-(void) shrinkSearchWithCompletion:(void(^)(void))completion;

@end
