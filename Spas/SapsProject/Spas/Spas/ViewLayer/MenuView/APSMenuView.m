//
//  APSTopBar.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMenuView.h"
#import "APSTabCollectionViewCell.h"
#import <TvinciSDK/TVSessionManager.h>
#import "GCSessionAdapter.h"

@interface APSMenuView ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>
//@property (nonatomic, strong) UILabel* fakeLabel;
@end

@implementation APSMenuView

static UINib * sharedMenuNib = nil;


+(UINib * ) menuNib
{
    if (sharedMenuNib == nil)
    {
        sharedMenuNib = [UINib nibWithNibName:[APSMenuView nibName] bundle:nil];
    }
    
    return sharedMenuNib;
}

+(APSMenuView *) menuView
{
    NSArray * views = [[APSMenuView menuNib] instantiateWithOwner:nil options:nil];
    for (UIView * view in views )
    {
        if([view isKindOfClass:[APSMenuView class]])
            return (APSMenuView *)view;
    };
    return nil;
}

-(void)dealloc
{

    [self unregisterToSessionAdapterNotifcations];
}

+(NSString *) nibName
{
    if (is_iPad())
    {
        return @"APSMenuView_iPad";
    }
    else
    {

        return @"APSMenuView_iPhone";
    }
}


-(void)awakeFromNib
{
    [super awakeFromNib];

    [self registerToSessionAdapterNotifcations];
    NSString * reuseIdentifier = NSStringFromClass([APSTabCollectionViewCell class]);
    NSString * nibName = [APSTabCollectionViewCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewTabSelector registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
    
    self.labelTitle.textColor = [APSTypography colorLightGray];
    self.labelTitle.font = [APSTypography fontLightWithSize:20];
    self.fakeLabel.font = [APSTypography fontLightWithSize:14];
    
    self.btnBack.titleLabel.font = [APSTypography fontRegularWithSize:14];
    self.btnBack.titleLabel.textColor = [APSTypography colorLightGray];
    [self.btnBack setTitle:NSLocalizedString(@"BACK", nil) forState:UIControlStateNormal];
    
    self.searchField.textfieldSearch.returnKeyType = UIReturnKeySearch;
    self.searchField.textfieldSearch.enablesReturnKeyAutomatically = YES;

    [self.buttonLoggedIn setTitle:NSLocalizedString(@"Logged in", nil) forState:UIControlStateNormal];
    self.buttonLoggedIn.titleLabel.font =[APSTypography fontRegularWithSize:14];
    [self.buttonLoggedIn setBackgroundColor:[UIColor clearColor]];
    [self.buttonLoggedIn setTitleColor:[APSTypography colorLightGray] forState:UIControlStateNormal];

    if (is_iPad()) {
        [self setupiPadLoginButton];
    }
    [self updateLoginButtonAfterLoginOrLogout];
    
    self.buttonLogin.isAccessibilityElement = YES;
    self.buttonLogin.accessibilityLabel = @"Button Login";
    
    self.buttonSettings.isAccessibilityElement = YES;
    self.buttonSettings.accessibilityLabel = @"Button Settings";
    
}

- (void) setupiPadLoginButton
{
    // font color and background color are set by the APSBrandedButton class
    [self.buttonLogin setTitle:[NSLocalizedString(@"LOGIN", nil) uppercaseString]forState:UIControlStateNormal];
    self.buttonLogin.titleLabel.font = [APSTypography fontRegularWithSize:12];
    self.buttonLogin.titleLabel.attributedText = [APSTypography attributedString:self.buttonLogin.titleLabel.text withLetterSpacing:1.5];
}

//-(void)addBottomShadow
//{
//    CAGradientLayer *shadow = [CAGradientLayer layer];
//    shadow.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 2);
//    shadow.startPoint = CGPointMake(1.0, 0.0);
//    shadow.endPoint = CGPointMake(1.0, 1.0);
//    shadow.colors = [NSArray arrayWithObjects:(id)[[[UIColor blackColor] colorWithAlphaComponent:0.2] CGColor], (id)[[UIColor clearColor] CGColor], nil];
//    [self.layer addSublayer:shadow];
//    self.layer.masksToBounds = NO;
//}




#pragma mark - Session Adapter Notifcations -

-(void)registerToSessionAdapterNotifcations
{
    [self unregisterToSessionAdapterNotifcations];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedInChange) name:GCSessionAdapterLoginSuccessfullyCompleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedInChange) name:GCSessionAdapterLoginOutSuccessfullyCompleted object:nil];
}

-(void)unregisterToSessionAdapterNotifcations
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GCSessionAdapterLoginSuccessfullyCompleted object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GCSessionAdapterLoginOutSuccessfullyCompleted object:nil];
}

-(void)userLogedInChange
{
     [self updateLoginButtonAfterLoginOrLogout];
}

-(void)updateLoginButtonAfterLoginOrLogout
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        self.buttonLogin.hidden = YES;
        self.buttonLoggedIn.hidden = NO;
        self.buttonLoggedIn.alpha = 1.0;
    }
    else
    {
        self.buttonLogin.hidden = NO;
        self.buttonLogin.alpha  = 1.0;
        self.buttonLoggedIn.hidden = YES;
    }
    
    [_collectionViewTabSelector reloadData];
 
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) setDisplayPosition:(DisplayPosition ) displayPosition
{
     [self.collectionViewTabSelector selectItemAtIndexPath:[NSIndexPath indexPathForItem:displayPosition inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
}

#pragma mark - options

-(IBAction)settingsButtonSelected:(id)sender
{
    [self.delegate menuView:self didSelectSettings:sender];
    if(!is_iPad()){
        // On iPhone we want the tab not to have a selection after Settings view controller is displayed
        [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu = DisplayPosition_None;
    }
}

- (IBAction)closeButtonSelected:(id)sender
{
    [self.delegate menuView:self didSelectClose:sender];
}

- (IBAction)searchButtonSelected:(id)sender
{
    [self.delegate menuView:self didSelectSearch:sender];
}

- (IBAction)LoginButtonSelected:(id)sender
{
    [self.delegate menuView:self didSelectLogin:sender];
    self.buttonLogin.titleLabel.textColor =[APSTypography colorLightGray];
}

- (IBAction)LoggedButtonSelected:(id)sender {

}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.delegate menuViewNumberOfMenuItems:self];;
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * className = NSStringFromClass([APSTabCollectionViewCell class]);
    APSTabCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
    
    CGFloat letterSpacing = is_iPad()? 2.0:1.0;
    
    cell.labelTitle.attributedText = [APSTypography attributedString:[self.delegate menuView:self titleForIndex:indexPath.item] withLetterSpacing:letterSpacing];
    if (is_iPad()) {
        CGRect contentViewFrame = cell.contentView.frame;
        contentViewFrame.size = cell.frame.size;
        cell.contentView.frame = contentViewFrame;

        [cell.labelTitle sizeToFit];
        cell.labelTitle.centerX = cell.width/2;
        cell.labelTitle.y  = cell.height/2 - cell.labelTitle.height/2;
        cell.viewSelectedIndicator.width = cell.width;
    }
    
    //cell.selected = indexPath.row == [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu ;
    if (indexPath.row == [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu)
    {
        cell.labelTitle.textColor = [APSTypography colorBrand];
        cell.viewSelectedIndicator.hidden = NO;
    }
    else
    {
        cell.viewSelectedIndicator.hidden = YES;
        if (is_iPad())
        {
            cell.labelTitle.textColor = [UIColor lightGrayColor];
        }
        else
        {
            cell.labelTitle.textColor = [UIColor whiteColor];
        }
    }

    
    return cell;
}

#pragma mark - UICollectionViewDelegate

// APSMenuView is scrapped and a new instance is created after a cell is selected
// for this reason we need to save the last selected row.

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            [self.delegate menuView:self didSelectDisplayPosition:DisplayPosition_Home];
        }
            break;
        case 1:
        {
            [self.delegate menuView:self didSelectDisplayPosition:DisplayPosition_EPG];
        }
            break;
        case 2:
        {
            [self.delegate menuView:self didSelectDisplayPosition:DisplayPosition_Favorites];
        }
            break;
        default:
            break;
    }
    [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu = indexPath.row;
}

#pragma mark – UICollectionViewDelegateFlowLayout

-(CGSize ) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (is_iPad()) {
        // calculate the width required by the attributed string using a temp label.
        self.fakeLabel.attributedText = [APSTypography attributedString:[self.delegate menuView:self titleForIndex:indexPath.item] withLetterSpacing:2.0];
        
        CGRect alternativeSize = [self boundingRectForTitle:self.fakeLabel];
        return CGSizeMake(alternativeSize.size.width + 30, // Padding of each Tab item is 15px on each side
                          [APSTabCollectionViewCell size].height);
    }
    return [APSTabCollectionViewCell size];
}

-(CGRect) boundingRectForTitle:(UILabel*) label
{
    [label sizeToFit];
    return  [label frame ];
    
//    CGSize cellSize = CGSizeMake(9999, 9999);
//    return [label.text boundingRectWithSize:cellSize
//                                    options:NSStringDrawingUsesLineFragmentOrigin
//                                 attributes:@{NSFontAttributeName:label.font}
//                                    context:nil];
}

#define padding 20
-(void) expandSearchWithCompletion:(void(^)(void))completion
{
    // hide menu tabs
    // hide login button
    // hide Settings
    
    // expand search
    // show cancel Button
    self.buttonCancelSearch.hidden = NO;
    self.buttonCancelSearch.alpha=1;
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.collectionViewTabSelector.alpha =0;
        self.buttonLogin.alpha =0;
        self.buttonSettings.alpha = 0;
        self.buttonLoggedIn.alpha = 0;
        self.searchField.origin = CGPointMake(padding, self.searchField.y);

        self.buttonCancelSearch.alpha=1;

    }
    completion:^(BOOL finished)
    {
        self.collectionViewTabSelector.hidden=YES;
        self.buttonLogin.hidden= YES;
        self.buttonSettings.hidden = YES;
        self.searchField.active = YES;
        self.buttonLoggedIn.hidden = YES;
        
        [UIView animateWithDuration:0.4 animations:^{
            self.searchField.width = self.width-padding*3-self.buttonCancelSearch.width;
        } completion:^(BOOL finished) {
            if (completion)
            {
                completion();
            }
        }];
    }];
}


#define originalWidth 155
-(void) shrinkSearchWithCompletion:(void(^)(void))completion
{
    // show menu tabs
    // show login button
    // show Settings
    
    // shrink search
    // hide cancel Button
    
    
    self.collectionViewTabSelector.hidden=NO;
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        self.buttonLoggedIn.hidden = NO;
    }
    else
    {
        self.buttonLogin.hidden = NO;
    }
    self.buttonSettings.hidden = NO;
    self.searchField.active = NO;
    
    
    [UIView animateWithDuration:0.4 animations:^{
        
       

        
      
        
        self.searchField.width = originalWidth;
        
    }
                     completion:^(BOOL finished)
     {
        
         
         [UIView animateWithDuration:0.4 animations:^{
             
             self.collectionViewTabSelector.alpha =1;
             if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
             {
                 self.buttonLoggedIn.alpha =1;
             }
             else
             {
                 self.buttonLogin.alpha =1;
             }
             self.buttonSettings.alpha = 1;
             self.buttonCancelSearch.alpha=0;
             
             self.searchField.origin = CGPointMake(self.buttonLogin.origin.x-padding-originalWidth, self.searchField.y);
             
         } completion:^(BOOL finished)
         {
              self.buttonCancelSearch.hidden = YES;
             if (completion)
             {
                 completion();
             }

         }];
         
     }];

}


@end
