//
//  APSGeneralPlayerErrorView.h
//  Spas
//
//  Created by Israel Berezin on 10/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSPopupView.h"
#import "APSNetworkManager.h"
#import "CDNHandler.h"

@class APSGeneralPlayerErrorView;

@protocol APSGeneralPlayerErrorViewDelegate <NSObject>

-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *) generalPlayerErrorView didSelectTryPlayAgain:(UIButton *) sender;
-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *) generalPlayerErrorView didDismiss:(UIButton *) sender;

@end


@interface APSGeneralPlayerErrorView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic,assign) id<APSGeneralPlayerErrorViewDelegate> delegate;

@property (assign, nonatomic) id<BaseViewControllerDelegate>  baseViewControllerDelegate;
@property (weak, nonatomic) IBOutlet APSPopupView *popupView;
@property (weak, nonatomic) IBOutlet UIButton *btnTouche;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *ButtonClose;
@property (weak, nonatomic) IBOutlet UILabel * labelTitle;
@property (weak, nonatomic) IBOutlet UILabel * labelBody;
@property (weak, nonatomic) IBOutlet UIButton * buttonTryPlayAgain;
@property (weak, nonatomic) IBOutlet APSBlurView * blurView;
@property BOOL isShow;

@property (assign,nonatomic) UIView * presentOnView;

-(IBAction)tryPlayAgain:(id)sender;
-(IBAction)dismiss:(id)sender;
+(APSGeneralPlayerErrorView *) generalPlayerErrorView;
-(void) registerForNotification;
- (IBAction)toucheEvent:(id)sender;

-(void) purePlayerHaveError:(NSNotification *) notification;
@end
