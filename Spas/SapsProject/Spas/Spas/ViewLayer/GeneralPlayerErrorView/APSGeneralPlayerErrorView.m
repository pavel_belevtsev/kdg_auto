//
//  APSGeneralPlayerErrorView.m
//  Spas
//
//  Created by Israel Berezin on 10/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSGeneralPlayerErrorView.h"
#import "APSPurePlayerViewController.h"
#import "APSMainViewController_iPhone.h"
#import "APSMainViewController_iPad.h"
#import "APSSSLPinningManager.h"

@implementation APSGeneralPlayerErrorView


+(APSGeneralPlayerErrorView *) generalPlayerErrorView
{
    
    APSGeneralPlayerErrorView *view = nil;
    NSString *nibName = @"APSGeneralPlayerErrorView_iPhone";
    if (is_iPad())
    {
         nibName = @"APSGeneralPlayerErrorView_iPad";
    }
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self registerForNotification];
    
    self.blurView.layerColor =  [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65];
    self.blurView.blurRadius = 14;
    [self.blurView setNeedsDisplay];
    
    NSString *buttonText = [APSSSLPinningManager manager].SSLFailed ? NSLocalizedString(@"OK", nil) : [NSLocalizedString(@"Try again", nil) uppercaseString];
    
    self.buttonTryPlayAgain.titleLabel.attributedText = [APSTypography attributedString:buttonText withLetterSpacing:2.0];
    [self.buttonTryPlayAgain setTitle:buttonText forState:UIControlStateNormal];
    self.buttonTryPlayAgain.titleLabel.font = [APSTypography fontRegularWithSize:is_iPad()?17:16];
    self.labelTitle.textColor = [APSTypography colorDrakGray];
    NSInteger strLength = [self.labelBody.text length];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.labelBody.text];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentCenter];
    
    if (is_iPad())
    {
        [style setMinimumLineHeight:21];
        [style setMaximumLineHeight:21];
        
        self.labelTitle.font = [APSTypography fontMediumWithSize:19];
        self.labelBody.font = [APSTypography fontRegularWithSize:16];
    }
    else
    {
        [style setMinimumLineHeight:16];
        [style setMaximumLineHeight:16];
        
        self.labelTitle.font = [APSTypography fontMediumWithSize:16];
        self.labelBody.font = [APSTypography fontRegularWithSize:14];
    }
    
    [attString addAttribute:NSParagraphStyleAttributeName
                      value:style
                      range:NSMakeRange(0, strLength)];
    
    self.labelBody.attributedText = attString;
    self.labelBody.textColor = [APSTypography colorMediumGray];
    
    UITapGestureRecognizer *tapContentRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonCategoriesSortAction)];
    [self addGestureRecognizer:tapContentRecognizer];
    
    self.scrollView.contentSize=CGSizeMake(self.frame.size.width+100, 1000);
    
    self.labelTitle.text = NSLocalizedString(@"Houston, we have a problem", nil);
    self.labelBody.text = [APSSSLPinningManager manager].SSLFailed ? NSLocalizedString(@"Problem in communication with servers. Please check for the latest version (16)", nil) : NSLocalizedString(@"Apparently something went wrong and we couldn't show content.", nil);
    
}


-(IBAction)tryPlayAgain:(id)sender
{
    
    if ([APSSSLPinningManager manager].SSLFailed) {
        
        [[APSSSLPinningManager manager] goToAppStoreAndExit];
        return;
        
    }
    
   [self removeFromScreen];
    if ([self.delegate respondsToSelector:@selector(generalPlayerErrorView:didSelectTryPlayAgain:)])
    {
        [self.delegate generalPlayerErrorView:self didSelectTryPlayAgain:sender];
    }
}

-(IBAction)dismiss:(id)sender
{
    
    if ([APSSSLPinningManager manager].SSLFailed) {
        
        [[APSSSLPinningManager manager] goToAppStoreAndExit];
        return;
        
    }
    
    [self removeFromScreen];
    if ([self.delegate respondsToSelector:@selector(generalPlayerErrorView:didDismiss:)])
    {
        [self.delegate generalPlayerErrorView:self didDismiss:sender];
    }
}

-(void)removeFromScreen
{
    self.alpha = 1.0;
    [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleDefault];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.isShow = NO;
    }];

}
-(void)show
{
    [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setBackgroundColor:[UIColor clearColor]];
    self.alpha = 0.0;
    self.isShow = YES;
    [self.presentOnView addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) purePlayerHaveError:(NSNotification *) notification
{
    [self upadteViewFrame];
   
    NSDictionary * userInfo = notification.userInfo;
    
    if (userInfo)
    {
        
        FinalConclusion finalConclusion = [[userInfo objectForKey:PurePlayerfinalConclusionKey] integerValue];
        NSString * textMessage = [userInfo objectForKey:PurePlayerErrorMessageKey];

        if ([textMessage isEqualToString:PurePlayerNoInternetValue])
        {
            if (self.isShow == YES)
            {
                self.alpha = 0.0;
                [self dismiss:nil];
            }
        }
        else
        {
            [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",textMessage]];
            
            if (finalConclusion != FinalConclusion_InSideClientNetwork_But_SecurityIssue) // genera error || content error
            {
                if (self.isShow == NO)
                {
                    [self show];
                }
            }
        }
    }
}

-(void)upadteViewFrame
{
    UIViewController *showVC = [self getShowVC];
    self.transform = CGAffineTransformMakeRotation(0);

    if ([showVC isKindOfClass:[APSMainViewController_iPad class]] || [showVC isKindOfClass:[APSMainViewController_iPhone class]])
    {
        UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:nil];
        if (is_iPad())
        {
            self.presentOnView = mainView;
        }
        else
        {
            self.frame = mainView.frame;
            self.presentOnView= showVC.view.window;
        }
    }
    else // it on player
    {
        CGRect frame= showVC.view.frame;
        if (frame.size.width < frame.size.height)
        {
            frame.size.height = showVC.view.frame.size.width;
            frame.size.width = showVC.view.frame.size.height;
        }
        self.frame = frame;

        if (!is_iPad())
        {
            self.transform = CGAffineTransformMakeRotation(0);
            
            if (CGAffineTransformIsIdentity(self.transform)) {
                
            }
            else
            {
                UIDeviceOrientation deviceOrintation = [[UIDevice currentDevice] orientation];
                if(deviceOrintation == UIDeviceOrientationLandscapeRight)
                {
                    self.transform = CGAffineTransformMakeRotation(-M_PI_2);
                }
                else if(deviceOrintation == UIDeviceOrientationLandscapeLeft)
                {
                    self.transform = CGAffineTransformMakeRotation(M_PI_2);
                }
            }
        }
        
        if (SYSTEM_VERSION_LESS_THAN(@"8.0"))
        {
            self.presentOnView = showVC.view;
        }
        else
        {
            self.presentOnView= showVC.view.window;
        };
    }
    self.origin = CGPointMake(0, 0);
    self.blurView.underlyingView = showVC.view;
    self.blurView.dynamic = NO;
    [self.blurView setNeedsDisplay];
    //self.popupView.center = showVC.view.center;
    self.popupView.center = self.center;

    ASLogInfo(@"self.presentOnView = %@",self.presentOnView);
}

-(UIViewController * )getShowVC
{
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (vc.presentedViewController)
    {
        vc = vc.presentedViewController;
    }
    return vc;
}

-(void)showIfNeeded
{
    APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
    if (status != TVNetworkStatus_NoConnection)
    {
        if (self.isShow == NO)
        {
            [self show];
        }
    }
}

-(void) registerForNotification
{
    [self unregisterForNotifcation];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(purePlayerHaveError:) name:PurePlayerHaveErrorNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:NotificationName_NetworkStatusChanged object:nil];
}

-(void) unregisterForNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerHaveErrorNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationName_NetworkStatusChanged object:nil];
}

- (void)reachabilityChanged:(NSNotification *)note
{
    APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
    if (status == TVNetworkStatus_NoConnection)
    {
        self.alpha = 0.0;
        [self dismiss:nil];
    }
}

- (IBAction)toucheEvent:(id)sender
{
    ASLogInfo(@"toucheEvent");
}

-(void)buttonCategoriesSortAction
{
    ASLogInfo(@"buttonCategoriesSortAction");
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *result = [super hitTest:point withEvent:event];
    CGPoint buttonPoint = [self.buttonTryPlayAgain convertPoint:point fromView:self];
    if ([self.buttonTryPlayAgain pointInside:buttonPoint withEvent:event])
    {
        return self.buttonTryPlayAgain;
    }
    
    buttonPoint = [self.ButtonClose convertPoint:point fromView:self];
    if ([self.ButtonClose pointInside:buttonPoint withEvent:event])
    {
        return self.ButtonClose;
    }
    
    buttonPoint = [self.scrollView convertPoint:point fromView:self];
    if ([self.scrollView pointInside:buttonPoint withEvent:event])
    {
        return self.scrollView;
    }
    
    return result;
}

- (void) dealloc
{
    [self unregisterForNotifcation]; 
}

@end
