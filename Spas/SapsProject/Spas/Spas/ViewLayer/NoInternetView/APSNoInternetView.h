//
//  APSNoInternetView.h
//  Spas
//
//  Created by Israel Berezin on 10/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSNoInternetView;

@protocol APSNoInternetViewDelegate <NSObject>

-(void) noInternetView:(APSNoInternetView *) noInternetView didSelectClose:(UIButton *) sender;
-(UIView *) noInternetViewUnderlayingView:(APSNoInternetView *) noInternetView;

@end

@interface APSNoInternetView : UIView

@property (weak, nonatomic) IBOutlet APSBlurView *blurView;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstLine;

@property (weak, nonatomic) IBOutlet UILabel *lblSecLine;
@property (nonatomic,assign) id<APSNoInternetViewDelegate> delegate;
@property BOOL isVisibel;

+(APSNoInternetView *) noInternetView;

-(void)updateView;

- (IBAction)close:(id)sender;
@end
