//
//  APSNoInternetView.m
//  Spas
//
//  Created by Israel Berezin on 10/26/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSNoInternetView.h"
#import "APSSSLPinningManager.h"

@implementation APSNoInternetView

+(APSNoInternetView *) noInternetView
{
    if (is_iPad())
    {
        return  [self noInternetView_iPad];
    }
    APSNoInternetView *view = nil;
    NSString *nibName = @"APSNoInternetView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    return view;

}

+(APSNoInternetView *) noInternetView_iPad
{
    APSNoInternetView *view = nil;
    NSString *nibName = @"APSNoInternetView_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    return view;

}
-(void)updateView
{
    self.blurView.layerColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.8];
    self.blurView.blurRadius = 3;
    if ([self.delegate respondsToSelector:@selector(playerBarBlursUnderlayingView:)])
    {
        self.blurView.underlyingView = [self.delegate noInternetViewUnderlayingView:self];
    }
    [self.blurView setNeedsDisplay];
    self.layer.shadowOffset = CGSizeMake(0, -1);
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.25;
    self.layer.shadowRadius = 1;
    
    if (is_iPad())
    {
        self.lblFirstLine.font = [APSTypography fontRegularWithSize:17];
    }
    else
    {
        self.lblFirstLine.font = [APSTypography fontRegularWithSize:14];
        self.lblSecLine.font = [APSTypography fontRegularWithSize:14];
        self.lblSecLine.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    }
     self.lblFirstLine.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    if (is_iPad())
    {
        self.lblFirstLine.text = NSLocalizedString(@"No Internet Connection. Please check your setting", nil);
    }
    else
    {
        self.lblFirstLine.text = NSLocalizedString(@"No Internet Connection", nil);
        self.lblSecLine.text = NSLocalizedString(@"Please check your setting", nil);
    }
    [self layoutSubviews];
    
    
}

- (IBAction)close:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(noInternetView:didSelectClose:)])
    {
        [self.delegate noInternetView:self didSelectClose:sender];
    }

}

@end
