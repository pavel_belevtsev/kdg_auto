//
//  APSProgramFullView.h
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSBlurView.h"
#import "APSScrollView.h"
#import "APSButtonHighlighted.h"

@class APSProgramFullView;

@protocol APSProgramFullViewDelegate <NSObject>

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel;

@end

@interface APSProgramFullView : UIView <APSButtonHighlightedDelegate>

@property (assign, nonatomic) id<APSProgramFullViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (nonatomic, strong) APSTVProgram * program;
@property (nonatomic, strong) TVMediaItem * channel;

@property (strong, nonatomic) IBOutlet APSBlurView *blurView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *animateView;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (strong, nonatomic) UIView * dataView;
@property (strong, nonatomic)  UIView * descriptionView;
@property (strong, nonatomic ) UIView * metaDataView;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *topScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramName;
@property (weak, nonatomic) IBOutlet UILabel *lblProgramDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeIndicator;

@property (weak, nonatomic) IBOutlet APSScrollView *scrollViewContiner;
@property (weak, nonatomic) IBOutlet APSBrandedButton *btnPlay;
@property (weak, nonatomic) IBOutlet UIImageView *imageProgram;

@property (weak, nonatomic) IBOutlet APSButtonHighlighted *buttonImageProgram;

@property (weak, nonatomic) IBOutlet UIImageView *gracLogo;
@property (weak, nonatomic) IBOutlet UILabel *labelImageChannelName;
@property (weak, nonatomic) IBOutlet UIImageView *imageChannel;
@property (assign, nonatomic) id<BaseViewControllerDelegate>  baseViewControllerDelegate;

@property (weak, nonatomic) IBOutlet UIView *firstSpaseView;

@property (weak, nonatomic) IBOutlet UIView *secSpaceView;

@property (weak, nonatomic) IBOutlet UIView *thirdSpaceView;

+(APSProgramFullView *) programFullView;

-(void) showProgramFullViewWithChannel:(TVMediaItem*)channel andProgran:(APSTVProgram*) program OnView:(UIView *) view;
-(void) showProgramFullViewWithChannel:(TVMediaItem*)channel andProgran:(APSTVProgram*) program OnView:(UIView *) view showImage:(BOOL)showImage;

-(IBAction)close:(id)sender;

- (IBAction)playChannel:(id)sender;
-(void) animateOutWithCompletion:(void(^)(void)) completion;


@end
