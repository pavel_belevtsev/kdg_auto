//
//  APSProgramFullView.m
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSProgramFullView.h"
#import "APSEpgMetaDataParser.h"
#import "APSEpgMetaDataView.h"
#import <TvinciSDK/TVLinearLayout.h>
#import "UIView+Frame.h"
#import <TvinciSDK/UIImageView+AFNetworking.h>
#import "APSGoogleAnalyticsManager.h"

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

@implementation APSProgramFullView

+(APSProgramFullView *) programFullView
{
    if(is_iPad())
    {
        return [APSProgramFullView programFullView_ipad];
    }
    else
    {
        return [APSProgramFullView programFullView_iPhone];
    }
}

+(APSProgramFullView *) programFullView_ipad
{
    APSProgramFullView *view = nil;
    NSString *nibName = @"APSProgramFullView_iPad";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

+(APSProgramFullView *) programFullView_iPhone
{
    APSProgramFullView *view = nil;
    NSString *nibName = @"APSProgramFullView_iPhone";
    NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id object in contents)
    {
        if ([object isKindOfClass:self])
        {
            view = object;
            break;
        }
    }
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.lblTitle setTextColor:[APSTypography colorMediumGray]];
    [self.lblTimeIndicator setTextColor:[APSTypography colorMediumGray]];
    [self.lblProgramName setTextColor:[APSTypography colorDrakGray]];
    [self.lblProgramDetails setTextColor:[APSTypography colorDrakGray]];
    [self.lblTitle setFont:[APSTypography fontLightWithSize:16]];
    [self.lblTimeIndicator setFont:[APSTypography fontLightWithSize:16]];
    [self.lblProgramDetails setFont:[APSTypography fontRegularWithSize:14]];

    if (is_iPad())
    {
        [self.lblProgramName setFont:[APSTypography fontRegularWithSize:20]];
    }
    else
    {
        [self.lblProgramName setFont:[APSTypography fontRegularWithSize:18]];
    }
    self.btnPlay.titleLabel.attributedText = [APSTypography attributedString:NSLocalizedString(@"PLAY", nil) withLetterSpacing:1.0];
    self.btnPlay.titleLabel.font = [APSTypography fontRegularWithSize:12];
    
    self.buttonImageProgram.delegate = self;
    
}


#pragma  mark -

-(void) showProgramFullViewWithChannel:(TVMediaItem*)channel andProgran:(APSTVProgram*) program OnView:(UIView *) view
{
    
    [self showProgramFullViewWithChannel:channel andProgran:program OnView:view showImage:YES];
    
}

-(void) showProgramFullViewWithChannel:(TVMediaItem*)channel andProgran:(APSTVProgram*) program OnView:(UIView *) view showImage:(BOOL)showImage {
    
    [self sendGAITrackerScreenView:@"Program Info"];

    self.blurView.layerColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65];
    self.blurView.blurRadius = 14;
    self.blurView.underlyingView = view;//[self.baseViewControllerDelegate baseViewControllerMainView:self];
    self.blurView.dynamic  = NO;
    [self.blurView setNeedsDisplay];
    
    self.channel = channel;
    self.program = program;
    
    if (is_iPad()) showImage = YES;
    
    [self buildUI:showImage];
    self.size = view.bounds.size;
    [self animateIn:view];
}

-(void)sendGAITrackerScreenView:(NSString*)name
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
    //                            @"appview", kGAIHitType, @"Home Screen", kGAIScreenName, nil];
    //[tracker send:params];
}

-(IBAction)close:(id)sender
{
    [self animateOutWithCompletion:^{
        
    }];
}

- (IBAction)playChannel:(id)sender
{
     [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"play from program description" eventLabel:[NSString stringWithFormat:@"channel = %@",self.channel.name]];
    
    if ([self.delegate respondsToSelector:@selector(programFullView:didSelectChannel:)])
    {
        [self.delegate programFullView:self didSelectChannel:self.channel];
    }
}
#pragma mark - APSButtonHighlightedDelegate

- (void)buttonSetHighlighted:(BOOL)highlighted {
    
    float alpha = (highlighted ? 0.5 : 1.0);
    
    self.imageProgram.alpha = alpha;
    self.imageChannel.alpha = alpha;
    
}

#pragma mark - UI -

-(void)buildUI:(BOOL)showImage
{
    [self cleenView:self.scrollView];
   
    if (showImage)
    {
        [self loadImage];
    
        if (!is_iPad()) {
            self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y + self.imageProgram.frame.size.height, self.scrollView.frame.size.width, self.scrollView.frame.size.height - self.imageProgram.frame.size.height);
        }
    
    } else {
        
        self.imageProgram.hidden = YES;
        self.imageChannel.hidden = YES;
        self.buttonImageProgram.hidden = YES;
    }
    
    [self buildTopView];
  
    [self buildDescriptionView];
   
    [self buildMetaDataView];
    
    [self buildMainDataView];
  
    self.scrollView.contentSize = self.dataView.size;
    [self.scrollView addSubview:self.dataView];
}

-(void)cleenView:(UIView*)mainView
{
    for (UIView * view  in [mainView subviews])
    {
        [view removeFromSuperview];
    }
}


-(void)buildTopView
{
    
    NSInteger fontSize = 20;
    if (!is_iPad())
    {
        fontSize = 18;
    }
    
    NSString * channelNumberString = [self.channel.metaData objectForKey:@"Channel number"];
    NSString* channelTitle = self.channel.name;
    
    if (!channelTitle)
    {
        channelTitle = @" ";
    }
    if (!channelNumberString)
    {
        channelNumberString = @" ";
    }

    NSString * streamingTitleString = [NSString stringWithFormat:@"%@ %@",channelNumberString,channelTitle];
    NSMutableAttributedString *st = [[NSMutableAttributedString alloc] initWithString:streamingTitleString];
    
    NSRange channelName = [[st string] rangeOfString:channelTitle];
    NSRange channelNumber = [[st string] rangeOfString:channelNumberString options:NSCaseInsensitiveSearch];

    [st addAttribute:NSFontAttributeName value:[APSTypography fontRegularWithSize:fontSize] range:channelName];
    [st addAttribute:NSFontAttributeName value:[APSTypography fontLightWithSize:fontSize] range:channelNumber];
    
    self.lblTitle.attributedText = st;
    
    self.lblProgramName.text = self.program.name;

    self.lblProgramName.numberOfLines = 0;

    NSInteger programNameLineHeight = 25.f;
//    if (is_iPad()==NO)
//    {
//        
//    }
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = programNameLineHeight;
    style.maximumLineHeight = programNameLineHeight;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    if (self.program.name)
    {
        self.lblProgramName.attributedText = [[NSAttributedString alloc] initWithString:self.program.name
                                                                  attributes:attributtes];
    }
    [self.lblProgramName sizeToFit];

    self.lblProgramDetails.text = [APSEpgMetaDataParser findFiledText:@"subtitle" inProgram:self.program];
    
    self.lblTimeIndicator.text = [APSEpgMetaDataView timeLblStingFormatWithStartTime:self.program.startDateTime EndTime:self.program.endDateTime];
    
    // time lbl color
    if ([APSEpgMetaDataView isProgramInCurrent:self.program])
    {
        self.lblTimeIndicator.textColor = [APSTypography colorBrand];
    }
    else
    {
        self.lblTimeIndicator.textColor = [APSTypography colorMediumGray];
        self.scrollViewContiner.height = self.btnPlay.y + self.btnPlay.height - self.scrollViewContiner.y;
        [self.scrollViewContiner updateGradient];
        self.btnPlay.hidden = YES;
        self.buttonImageProgram.enabled = NO;
    }
//    if (self.lblProgramName.frame.size.height >programNameLineHeight)
//    {
    if (is_iPad())
    {
        NSInteger sizeH = self.lblProgramName.frame.size.height + self.lblProgramDetails.frame.size.height +  self.secSpaceView.frame.size.height;
        self.topScrollView.height = sizeH;
        self.lblProgramName.y=0;
    }
    else
    {
        NSInteger sizeH = /*self.lblTimeIndicator.frame.size.height + */self.lblProgramName.frame.size.height + self.lblProgramDetails.frame.size.height + self.secSpaceView.frame.size.height + self.thirdSpaceView.frame.size.height;
        self.topScrollView.height = sizeH;

    }
//    }
    self.topScrollView.y = 0;
}

-(void)buildDescriptionView
{
    // get description data.
    self.descriptionView = nil;
    UIFont * descriptionTextFont =[APSTypography fontRegularWithSize:14]; //
    
    if (self.program && [self.program isKindOfClass:[APSTVProgram class]] && [self.program.programDescription length] > 0 )
    {
        self.descriptionView = [APSEpgMetaDataView buildDescriptionViewWithText:self.program.programDescription andViewWidth:self.scrollView.width-20 andFont:descriptionTextFont andLineHeight:17.f andTextColor:[APSTypography colorMediumGray]];
    }
    else if (self.program && [self.program isKindOfClass:[TVEPGProgram class]] && [self.program.description length] > 0 )
    {
        self.descriptionView = [APSEpgMetaDataView buildDescriptionViewWithText:self.program.description andViewWidth:self.scrollView.width-20 andFont:descriptionTextFont andLineHeight:17.f andTextColor:[APSTypography colorMediumGray]];
    }
    else
    {
        self.descriptionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.width-20, 1)];
    }
    self.descriptionView.y = self.topScrollView.height;

}

-(void)buildMetaDataView
{
    // get other meta data.
    NSArray * orderKeys = [NSArray arrayWithObjects:@"genre",@"country",@"year",@"season", nil];
//    if (is_iPad())
//    {
//        orderKeys = [NSArray arrayWithObjects:@"genre",@"country",@"year",@"season",@"director",@"actors", nil];
//    }
    NSArray *oneItemKayes = [NSArray arrayWithObjects:@"genre",nil];
    
    APSEpgMetaDataParser * mdp = [[APSEpgMetaDataParser alloc] initWithMetaData:self.program andFieldToShow:orderKeys andFieldToShowOnlyOneItem:oneItemKayes];
    NSDictionary * valuesToShow = [mdp getAFieldToShowInApp];
    
    NSInteger margin = 5;
    NSInteger buttonSpace = 19;
    if (!is_iPad())
    {
        buttonSpace = 22;
        margin = 0;
    }
    
    NSDictionary * UIDic = [self buildUiDictionary];
    
    self.metaDataView = [APSEpgMetaDataView buildUIWithMetaData:valuesToShow keysOrder:orderKeys andLayoutMargin:margin andUIDic:UIDic];
//    if (!is_iPad())
//    {
        NSArray * listOrderKeys = [NSArray arrayWithObjects:@"director",@"actors", nil];
        APSEpgMetaDataParser * listMdp = [[APSEpgMetaDataParser alloc] initWithMetaData:self.program andFieldToShow:listOrderKeys andFieldToShowOnlyOneItem:oneItemKayes];
        NSDictionary * listValuesToShow = [listMdp getAFieldToShowInApp];
        if ([[listValuesToShow allKeys] count] > 0)
        {
            [APSEpgMetaDataView buildUIListOnView:(TVLinearLayout *)self.metaDataView WithMetaData:listValuesToShow keysOrder:listOrderKeys andLayoutMargin:margin andUIDic:UIDic andListSize:4];
        }
//    }
    self.metaDataView.y = self.descriptionView.y + self.descriptionView.height + buttonSpace;
}

-(void)buildMainDataView
{
    // data view init & setup
    self.dataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.width, 1000)];
    [self.dataView setBackgroundColor:[UIColor clearColor]];
    
    // clean data view
    [self cleenView:self.dataView];
    
    // add subviews to dataview
    [self.dataView addSubview:self.topScrollView];
    [self.dataView addSubview:self.descriptionView];
    [self.dataView addSubview:self.metaDataView];
    
    NSInteger sapce = 19;
    if (is_iPad())
    {
        sapce = 25;
    }
    
    self.gracLogo.y = self.metaDataView.y + self.metaDataView.height + sapce;
    [self.dataView addSubview:self.gracLogo];
    
    UIView * view  = [[self.dataView subviews] lastObject];
    self.dataView.height = view.y + view.height +sapce*2;
}

-(void)loadImage
{
    UIImage * defualtImage = [UIImage imageNamed:@""];
    
    CGSize imageSize = CGSizeMake(402, 226);

    NSMutableURLRequest *request = nil;
    if (self.program && [self.program isKindOfClass:[APSTVProgram class]])
    {
        NSURL * imageUrl = [self.program pictureURLForSizeWithRetinaSupport:imageSize];
        request = [NSMutableURLRequest requestWithURL:imageUrl];
    }

    if (self.program && [self.program isKindOfClass:[TVEPGProgram class]])
    {
        NSURL * imageUrl = [((TVEPGProgram *)self.program) pictureURLForSizeWithRetinaSupport:imageSize];
        request = [NSMutableURLRequest requestWithURL:imageUrl];
    }

    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.imageProgram setImageWithURLRequest:request placeholderImage:defualtImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
        self.imageProgram.image = image;
    
     }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
        if (self.channel)
        {
            [self updateUIWithChannel:self.channel];
        }
        else
        {
            self.imageProgram.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        }
        
    }];
}

-(void) updateUIWithChannel:(TVMediaItem *) mediaItem
{
    self.imageProgram.backgroundColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
    
    // no program image - we will position the channel image in the center:
    self.imageChannel.size = CGSizeMake(200, 100);
    self.imageChannel.center = self.imageProgram.center;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self.channel pictureURLForSizeWithRetinaSupport:self.imageChannel.size]];
   // NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"AAA"]];

    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.imageChannel setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
    {
        
        self.imageChannel.image = image;
        
    }
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
        [self showLabelImageChannelName];
        
    }];
}

-(void)showLabelImageChannelName
{
    NSString * channelNumber = [self.channel.metaData objectForKey:@"Channel number"];
    NSString* channelTitle = [NSString stringWithFormat:@"%@ %@",channelNumber,self.channel.name];
    self.labelImageChannelName.text = channelTitle;
    
    self.labelImageChannelName.font = [APSTypography fontLightWithSize:26];
    self.labelImageChannelName.textColor = [APSTypography colorLightGray];
    self.labelImageChannelName.width = self.imageChannel.width;
    self.labelImageChannelName.textAlignment = NSTextAlignmentCenter;
    self.labelImageChannelName.center = self.imageProgram.center;
    
    self.labelImageChannelName.hidden = NO;
    self.imageChannel.hidden = YES;
}

-(NSDictionary*)buildUiDictionary
{
    NSMutableDictionary * UIDic = [NSMutableDictionary dictionary];
    
    NSInteger viewWidth = self.scrollView.width-20;
    NSInteger lblTitleWidth = 110;
    UIFont * titleFont = [APSTypography fontRegularWithSize:13];
    UIFont * lblTextFont = [APSTypography fontRegularWithSize:13];
    BOOL lblTitleSizeToFit = YES;
    UIColor * lblTitleColor = [APSTypography colorMediumGray];
    UIColor * lblTextColor = [APSTypography colorMediumGray];
    float letterSpacing = 0.0;
    
    NSInteger multiLineHigh = 18;
    if (is_iPad())
    {
        multiLineHigh = 20;
        lblTitleWidth = 105;
        lblTitleSizeToFit = NO;
        titleFont = [APSTypography fontRegularWithSize:10];
        lblTextFont = [APSTypography fontRegularWithSize:12];
        letterSpacing = 1.5;
    }
    
    [UIDic setObject:[NSNumber numberWithInt:viewWidth] forKey:MDVUIItemViewWidth];
    [UIDic setObject:[NSNumber numberWithInt:lblTitleWidth] forKey:MDVUIItemLblTitleWidth];
    [UIDic setObject:titleFont forKey:MDVUIItemLblTitleFont];
    [UIDic setObject:lblTextFont forKey:MDVUIItemLblTextFont];
    [UIDic setObject:[NSNumber numberWithBool:lblTitleSizeToFit] forKey:MDVUIItemLblTitleSizeToFit];
    [UIDic setObject:lblTitleColor forKey:MDVUIItemLblTitleColor];
    [UIDic setObject:lblTextColor forKey:MDVUIItemLblTextColor];
    [UIDic setObject:[NSNumber numberWithInteger:letterSpacing] forKey:MDVUIItemLblTitleLetterSpacing];
    [UIDic setObject:[NSNumber numberWithInteger:multiLineHigh] forKey:MDVUIItemViewMultiLineHigh];
    return [NSDictionary dictionaryWithDictionary:UIDic];
}

#pragma mark - Animate In & Out -

-(void) animateIn:(UIView *) superView
{
    CGPoint oldorigin = self.animateView.origin;
    self.animateView.origin = CGPointMake(self.animateView.origin.x,superView.frame.size.height+100);
    self.alpha=0;
    [superView addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.alpha=1;
        [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleLightContent];
        if (!is_iPad() && !IS_IPHONE5)
        {
            self.animateView.center = CGPointMake(self.animateView.center.x, self.center.y);
        }
        else
        {
            self.animateView.origin = oldorigin;
        }

    } completion:^(BOOL finished) {

    }];
}

-(void) animateOutWithCompletion:(void(^)(void)) completion
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.animateView.origin = CGPointMake(self.animateView.origin.x,self.frame.size.height+100);
        [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleDefault];
    } completion:^(BOOL finished) {
        self.alpha=1.0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.alpha=0.0;
            
        } completion:^(BOOL finished) {
            
            [self removeFromSuperview];
            if (completion)
            {
                completion();
            }
            
        }];
    }];
}

@end
