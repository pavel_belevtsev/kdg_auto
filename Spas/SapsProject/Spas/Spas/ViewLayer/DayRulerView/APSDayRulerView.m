//
//  APSDayRulerView.m
//  Spas
//
//  Created by Rivka Peleg on 9/9/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSDayRulerView.h"
#import "APSDayRulerCell.h"
#import "TouchBlocker.h"

@interface APSDayRulerView ()<TouchBlockerDelegate>

@end

@implementation APSDayRulerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    NSString * identifier = NSStringFromClass([APSDayRulerCell class]);
    NSString * nibName = [APSDayRulerCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
   
    self.presentedDayView.backgroundColor = [UIColor colorWithRed:50.0/255 green:50.0/255 blue:50.0/255 alpha:1];
    self.collectionView.backgroundColor = [UIColor colorWithRed:50.0/255 green:50.0/255 blue:50.0/255 alpha:1];
    
    self.labelPresentedDay.font = [APSTypography fontMediumWithSize:12];
    self.labelPresentedDay.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    
    [self updateUIForPresentedDateIndex];
    
}

-(void) openRuller:(UIView *) superView completion:(void(^)(void)) completion
{
    [UIView animateWithDuration:0.3 animations:^{
        self.presentedDayView.alpha = 0;
        
        self.collectionView.frame = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height);
    } completion:^(BOOL finished) {
        

        [TouchBlocker blockExceptView:self withDelegate:self];
        if (completion)
        {
            completion();
        }
    }];
    // hide presented day view
    // show and expand table
}


-(void) closeRuller:(UIView *) superView completion:(void(^)(void)) completion
{
    [UIView animateWithDuration:0.3 animations:^{
        self.presentedDayView.alpha = 1;
        self.collectionView.frame = self.presentedDayView.frame;
    } completion:^(BOOL finished) {
        [TouchBlocker stopBlock];
         if (completion)
        {
            completion();
        }
    }];
    // shrink table & hide
    // than show presented day view
}

-(IBAction)openRuler:(id)sender
{
    [self openRuller:nil completion:nil];
}


#pragma mark - UICollectionViewDataSource -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
    //self.arrayPrograms.count;
}

// The cell that is returned must be retrieved from a call to - dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * className = NSStringFromClass([APSDayRulerCell class]);
    APSDayRulerCell *cell = (APSDayRulerCell*)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
    cell.labelDay.text = [[self dateStringForIndex:indexPath.row] uppercaseString];
    
    
    cell.labelDay.font = [APSTypography fontMediumWithSize:12];
    if (indexPath.row ==self.presentedDateIndex)
    {
        cell.labelDay.textColor = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
        
    }
    else
    {
        cell.labelDay.textColor = [UIColor colorWithRed:133.0/255.0 green:133.0/255.0 blue:133.0/255.0 alpha:1];
    }
    
    return cell;

   
}


+(NSDateFormatter *) dateFormatter
{
    static NSDateFormatter * dateDateFormatter = nil;
    
    if (dateDateFormatter ==nil)
    {
        dateDateFormatter = [[NSDateFormatter alloc] init];
        [dateDateFormatter setShortWeekdaySymbols:@[
                                             NSLocalizedString(@"Sun",nil),
                                             NSLocalizedString(@"Mon",nil),
                                             NSLocalizedString(@"Tue",nil),
                                             NSLocalizedString(@"Wed",nil),
                                             NSLocalizedString(@"Thu",nil),
                                             NSLocalizedString(@"Fri",nil),
                                             NSLocalizedString(@"Sat",nil)
                                             ]];
        
        [dateDateFormatter setShortMonthSymbols:@[
                                           NSLocalizedString(@"Jan",nil),
                                           NSLocalizedString(@"Feb",nil),
                                           NSLocalizedString(@"Mar",nil),
                                           NSLocalizedString(@"Apr",nil),
                                           NSLocalizedString(@"May",nil),
                                           NSLocalizedString(@"Jun",nil),
                                           NSLocalizedString(@"Jul",nil),
                                           NSLocalizedString(@"Aug",nil),
                                           NSLocalizedString(@"Sep",nil),
                                           NSLocalizedString(@"Oct",nil),
                                           NSLocalizedString(@"Nov",nil),
                                           NSLocalizedString(@"Dec",nil)
                                           ]];
    }
    
    return dateDateFormatter;
}

+(NSDateFormatter *) dateDayFormatter
{
    static NSDateFormatter * dateDateDayFormatter = nil;
    
    if (dateDateDayFormatter ==nil)
    {
        dateDateDayFormatter = [[NSDateFormatter alloc] init];
        dateDateDayFormatter.dateStyle = kCFDateFormatterShortStyle;
        dateDateDayFormatter.timeStyle = kCFDateFormatterNoStyle;
        dateDateDayFormatter.doesRelativeDateFormatting = YES;
        NSLocale *germanLocale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"Application Locale", nil)];
        dateDateDayFormatter.locale = germanLocale;
    }
    
    return dateDateDayFormatter;
}

-(NSString *) dateStringForIndex:(NSInteger ) index
{
    NSInteger secondToAddFromToday = index*24*60*60;
    NSDate* wantedDate = [NSDate dateWithTimeIntervalSinceNow:secondToAddFromToday];
    
    if (index == 0 || index == 1)
    {
        NSDateFormatter *dateFormat = [APSDayRulerView dateDayFormatter];
        NSString *dateString = [dateFormat stringFromDate:wantedDate];
        return dateString;
    }
    else
    {
        NSDateFormatter *dateFormat = [APSDayRulerView dateFormatter];
     

        [dateFormat setDateFormat:@"E, MMM d"];
        NSString *dateString = [dateFormat stringFromDate:wantedDate];
        return dateString;
    }

}





#pragma mark - UICollectionViewDelegateFlowLayout -

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [APSDayRulerCell cellSize];
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return [APSDayRulerCell cellEdgeInsets];
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSDayRulerCell cellEdgeInsets].top;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSDayRulerCell cellEdgeInsets].left;
}

#pragma mark - UICollectionViewDelegate -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSDate * currentDate = [self.delegate dayRulerCurrentDate:self];
//    NSInteger secondToAddFromCurrentDate = (indexPath.row-self.presentedDateIndex)*24*60*60;
//    NSDate* wantedDate = [NSDate dateWithTimeInterval:secondToAddFromCurrentDate sinceDate:currentDate];
    
    NSInteger offset = indexPath.item-self.presentedDateIndex;
    self.presentedDateIndex = indexPath.item;

    [self closeRuller:nil completion:^{
        [self.delegate dayRuler:self didSelectDateIndex:indexPath.item offset:offset];

    }];

    
}

-(void)setPresentedDateIndex:(NSInteger)presentedDateIndex
{
    if (_presentedDateIndex != presentedDateIndex)
    {
        _presentedDateIndex = presentedDateIndex;
        
        [self updateUIForPresentedDateIndex];
    }
}

-(void) updateUIForPresentedDateIndex
{
   
    self.labelPresentedDay.attributedText = [APSTypography attributedString:[[self  dateStringForIndex:self.presentedDateIndex] uppercaseString] withLetterSpacing:2];
    ;
    [self.collectionView reloadData];
//    [self.collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:self.presentedDateIndex inSection:0]]];
    
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL isInside = NO;
    for (UIView * view in self.subviews)
    {
        if ([view pointInside:[self convertPoint:point toView:view] withEvent:event])
        {
            isInside = YES;
            break;
        }
    }
    
    return isInside;
}


#pragma mark -  block delegate 
-(void) outerTouchWasReceived
{
    [self closeRuller:nil completion:nil];
}

@end
