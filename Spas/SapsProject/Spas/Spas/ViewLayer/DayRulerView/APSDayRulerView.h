//
//  APSDayRulerView.h
//  Spas
//
//  Created by Rivka Peleg on 9/9/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@class APSDayRulerView;

@protocol APSDayRulerViewDelegate <NSObject>

-(void) dayRuler:(APSDayRulerView *) dayRuler didSelectDateIndex:(NSInteger) selectedIndex offset:(NSInteger) offset;
-(NSDate *) dayRulerCurrentDate:(APSDayRulerView *) dayRuler;
-(NSDate *) dayRulerTodayOffsetDate:(APSDayRulerView *) dayRuler;

@end

@interface APSDayRulerView : UIView




@property (weak , nonatomic) IBOutlet id<APSDayRulerViewDelegate> delegate;

@property (retain,nonatomic) IBOutlet UICollectionView * collectionView;
@property (retain, nonatomic)IBOutlet UIView * presentedDayView;
@property (retain, nonatomic) IBOutlet UILabel * labelPresentedDay;
@property (assign, nonatomic) NSInteger presentedDateIndex;


-(void) openRuller:(UIView *) superView completion:(void(^)(void)) completion;
-(void) closeRuller:(UIView *) superView completion:(void(^)(void)) completion;




@end
