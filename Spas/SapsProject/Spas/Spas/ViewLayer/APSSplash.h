//
//  APSSplash.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSSplash : UIView


@property (strong, nonatomic) UIImageView * logoImageView;
+(APSSplash *) splashViewWithFrame:(CGRect) frame;

@end
