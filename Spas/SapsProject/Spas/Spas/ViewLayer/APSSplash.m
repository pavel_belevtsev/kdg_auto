//
//  APSSplash.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSplash.h"
#import "APSActivityIndicator.h"

@implementation APSSplash

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+(APSSplash *) splashViewWithFrame:(CGRect) frame
{
    //UIImage * splashImage = nil;
    UIImage * vodafoneImage = nil;

    if (is_iPad())
    {
        //splashImage = [UIImage imageNamed:@"logo_splash_screen_ipad"];
        vodafoneImage = [UIImage imageNamed:@"vodafone_logo_ipad"];
    }
    else
    {
        frame.size.height+=20;
        //splashImage = [UIImage imageNamed:@"logo_splash_screen_iphone"];
        vodafoneImage = [UIImage imageNamed:@"vodafone_logo_iphone"];
    }
    
    APSActivityIndicator * activityIndicator = [[APSActivityIndicator alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    UIImageView * vodafoneImageView = [[UIImageView alloc] initWithImage:vodafoneImage];
    //UIImageView * image = [[UIImageView alloc] initWithImage:splashImage];
    //image.clipsToBounds = NO;
    APSSplash * splashView = [[APSSplash alloc] initWithFrame:frame];

    //splashView.logoImageView =[[UIImageView alloc] initWithImage:splashImage];
    
    //splashView.backgroundColor = [UIColor whiteColor];
    
    //splashView.logoImageView.center = splashView.center;
    activityIndicator.center =splashView.center;
    
    /*
    CGRect imageFrame = splashView.logoImageView.frame;
    CGRect activityIndicatorFrame = activityIndicator.frame;
    if (is_iPad())
    {
        imageFrame.origin.y = 191;
        activityIndicatorFrame.origin.y = 542;
    }
    else
    {
        if ([[UIScreen mainScreen] bounds].size.height<568) // iPhone 4
        {
            activityIndicatorFrame.origin.y = 367;

        }
        else // iPhone 5 + 6
        {
            activityIndicatorFrame.origin.y = 467;
        }
        imageFrame.origin.y = 145;
    }
    splashView.logoImageView.frame = imageFrame;

    activityIndicator.frame = activityIndicatorFrame;

    [splashView addSubview:splashView.logoImageView];
     */
    
    vodafoneImageView.x = 0;
    vodafoneImageView.y = 0;
    [splashView addSubview:vodafoneImageView];
    
    CGRect activityIndicatorFrame = activityIndicator.frame;
    if (is_iPad())
    {
        activityIndicatorFrame.origin.y = 542;
    }
    else
    {
        if ([[UIScreen mainScreen] bounds].size.height<568) // iPhone 4
        {
            activityIndicatorFrame.origin.y = 367;
            
        }
        else // iPhone 5 + 6
        {
            activityIndicatorFrame.origin.y = 467;
        }
    }
    
    activityIndicator.frame = activityIndicatorFrame;
    
    [splashView addSubview:activityIndicator];
    
    [activityIndicator startAnimating];

    return splashView;
}

@end
