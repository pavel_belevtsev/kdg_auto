//
//  IQEndpointKit.h
//  IQEndpointKit
//
//  Created by Terry Sznober on 2014-12-12.
//  Copyright (c) 2015 IneoQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IQEndpointKit.
FOUNDATION_EXPORT double IQEndpointKitVersionNumber;

//! Project version string for IQEndpointKit.
FOUNDATION_EXPORT const unsigned char IQEndpointKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IQEndpointKit/PublicHeader.h>

#import <IQEndpointKit/IQPegasus.h>
