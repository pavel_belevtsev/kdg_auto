//
//  IQPegasus.h
//  IQPegasus
//
//  Copyright (c) 2015 IneoQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  A reason code returned by the AMP-ASM system as to why the configuration
 *  failed. You can use this code to change the behaviour of your application
 *  as desired based on the code.
 */
typedef NS_ENUM(NSInteger, IQConfigurationFailureReasonCode) {
    /**
     *  Unknown error
     */
    IQConfigurationFailureReasonCodeUnknown = 0,
    /**
     *  Access restricted due to ACL rule
     */
    IQConfigurationFailureReasonCodeACLRestriction = -1,
    /**
     *  Invalid API Key
     */
    IQConfigurationFailureReasonCodeInvalidKey = -2,
    /**
     *  Access restricted due to ACL rule
     */
    IQConfigurationFailureReasonCodeIncompatible = -3,
    /**
     *  Blacklisted
     */
    IQConfigurationFailureReasonCodeBlackListed = -4,
    /**
     *  Restricted due to your geographic location
     */
    IQConfigurationFailureReasonCodeGeoRestricted = -5,
    /**
     *  User reached a quota limit
     */
    IQConfigurationFailureReasonCodeLimit = -6,
    /**
     *  User reached a playable time limit
     */
    IQConfigurationFailureReasonCodeTimeLimit = -7,
    /**
     *  Accessed outside the user's allowable time of day
     */
    IQConfigurationFailureReasonCodeTimeOfDay = -8,
    /**
     *  User reached a bandwidth limit
     */
    IQConfigurationFailureReasonCodeBandwidthLimit = -9
};

/**
 *  Logging level that the Endpoint SDK will perform its logging at.
 */
typedef NS_ENUM(NSUInteger, IQLoggingLevel) {
    /**
     *  No log messages will be emitted.
     */
    IQLoggingLevelOff = 0,
    /**
     *  Only error log messages will be emitted.
     */
    IQLoggingLevelError = 1,
    /**
     *  Warning and error log messages will be emitted.
     */
    IQLoggingLevelWarning = 2,
    /**
     *  Info, warning, and error log messages will be emitted.
     */
    IQLoggingLevelInfo = 3,
    /**
     *  Debug, info, warning, and error log messages will be emitted.
     */
    IQLoggingLevelDebug = 4,
    /**
     *  Verbose, debug, info, warning, and error messages will bee emitted.
     */
    IQLoggingLevelVerbose = 5,
    /**
     *  All log messages will be emitted.
     */
    IQLoggingLevelAll = 6
};

/**
 This class represents the public interface to the IQPegasus SDK. It creates
 an HTTP proxy server that your media player of choice can stream media
 through. While the media is streamed through the proxy server, various
 metrics are collected and then submitted to your AMP-ASM server via
 encrypted UDP packets.
 
 When initializing this class it is important to perform the following steps in 
 the order shown below:
 
 1. Call the setDelegate: method (required)
 2. Call the setUserIDWithUint64: or setUserIDWithUUID: method (optional)
 3. Call the useHTTPProxyWithHost:proxyPort:username:password: method (optional)
 4. Call the configureWithHostPortAPIKeyURL::useGeolocation:disableSSLCertificateChecking:proxySSLHLSKeys: method (required)
 5. Call the start method (required)

 */
@interface IQPegasus : NSObject

#pragma mark - Instantiation and configuration

/**
 *  Returns the shared instance of IQPegasus.
 *
 *  @return An IQPegasus object. Note that it might not yet be configured.
 *  you should call the setup method
 */
+ (IQPegasus *)sharedInstance;

/**
 *  Set the delegate that is to receive and respond to messages from the IQPegasus SDK.
 *
 *  @param newDelegate The delegate that will receive messages from the IQPegasus SDK.
 */
- (void)setDelegate:(id)newDelegate;

/**
 *
 *  Initiates the configuration of the IQPegasus SDK. It will make an
 *  asynchronous HTTPS request to the AMP-ASM endpoint to fetch it's
 *  configuration.
 *
 *  The url parameter is essentially encoding the AMP-ASM host address or IP,
 *  the AMP-ASM port number, and the API key together into the form of a URL
 *  such as "amp://host:port/?apikey=GUID". All of the URL components including
 *  the "amp://" scheme, host, port number, and apikey query string parameter
 *  are required.
 *
 *  The apikey query string parameter in the URL must be specified in order to 
 *  successfully fetch the configuration from the AMP-ASM server. It is a string
 *  in the form of a UUID (also known as a GUID). This apiKey is managed on the 
 *  AMP-ASM administrative UI. Please see the documentation for AMP-ASM for more
 *  information.
 *
 *  This SDK reports the geolocation of the device to AMP-ASM. To use this
 *  feature, set the useGeo parameter to YES. Set it to NO to disable it.
 *  By setting useGeo to NO, AMP-ASM will still be able to show the device
 *  on the visual map interface, but it will use less accruate GeoLookup of the
 *  client IP address of the device.
 *  For this reason, it is encouraged to set set useGeo to YES.
 *
 *  **IMPORTANT NOTE:**
 *  This SDK uses Location Services to get the geolocation details, and as a
 *  consequence, Location Services prompts users the first time they attempt to
 *  use location-related information in an app but does not prompt for
 *  subsequent attempts.
 *
 *  The disableSSLCertificateChecking parameter should only be set to YES if
 *  you want the SSL certificate check at SDK configuration time to be ignored.
 *  Normally this parameter should be set to NO.
 *
 *  The proxySSLHLSKeys parameter allows the HLS keys that are served over SSL
 *  via https URLs to go through the HTTP proxy system and thus allow it to be
 *  monitored for errors, etc. When set to YES, the keys will go through the
 *  HTTP proxy. If set to NO, the requests for the HLS keys will bypass the
 *  HTTP proxy and thus will not be monitored. Only traffic going through the
 *  proxy will be monitored.
 *
 *  **IMPORTANT_NOTE:**
 *  If you are using an HTTP Proxy server in your environment, you must call
 *  useHTTPProxyWithHost:proxyPort: before calling this method.
 *
 *  @param url             An NSURL that encodes the AMP-ASM host address, port,
 *                         and API key into the form of a URL such as
 *                         amp://host:port/?apikey=GUID
 *  @param useGeo          Set to YES to use geolocation, or NO to disable it.
 *  @param disableSSLCheck Set to YES to disable SSL checking, or NO for normal operation.
 *  @param proxySSLHLSKeys Set to YES to proxy HLS keys served over SSL, or NO to bypass the proxy.
 */
- (void)configureWithHostPortAPIKeyURL:(NSURL *)url
                        useGeolocation:(BOOL)useGeo
         disableSSLCertificateChecking:(BOOL)disableSSLCheck
                       proxySSLHLSKeys:(BOOL)proxySSLHLSKeys;

/**
 *  Let the SDK know that you are using an HTTP proxy server. All HTTP/HTTPS
 *  requests going through this SDK's internal proxy will connect to the
 *  specified HTTP proxy host and port.
 *  If your proxy server does not require authentication, then set the username
 *  and password parameters to nil.
 *  If this method is not called, the SDK will behave as if no HTTP Web proxy
 *  is being used.
 *  This method must be called BEFORE calling configureWithHostPortAPIKeyURL:...
 *
 *  @param proxyHost The external HTTP proxy host address.
 *  @param proxyPort The external HTTP proxy port number.
 *  @param username The authentication username, or nil for no authentication.
 *  @param password The authentication password, or nil for no authentication.
 */
- (void)useHTTPProxyWithHost:(NSString *)proxyHost
                   proxyPort:(NSUInteger)proxyPort
                    username:(NSString *)username
                    password:(NSString *)password;

#pragma mark - API Versioning

/**
 *  Gets the API version as a string in the format major.minor.patch.
 *  An example would be "1.0.0".
 *
 *  @return The API version as a string in the format major.minor.patch.
 */
- (NSString *)apiVersionString;

/**
 *  The major API version number. This number is incremented when incompatible
 *  API changes are made such as when significant functionality is introduced.
 *
 *  Example: If the API version number was 1.4.2, then this method would return
 *  1.
 *
 *  @return The major version number of the API.
 */
- (NSUInteger)apiVersionMajor;

/**
 *  The minor API version number. This number is incremented when
 *  backwards-compatible changes are made such as additional new methods or
 *  slight changes in functionality are introduced that is still compatible with
 *  the previous API version.
 *
 *  Example: If the API version number was 1.4.2, then this method would return
 *  4.
 *
 *  @return The minor version number of the API.
 */
- (NSUInteger)apiVersionMinor;

/**
 *  The patch API version number. This number is incremented when
 *  backwards-compatible bug-fixes are introduced.
 *
 *  Example: If the API version number was 1.4.2, then this method would return
 *  2.
 *
 *  @return The patch version number of the API.
 */
- (NSUInteger)apiVersionPatch;

#pragma mark - Logging

/**
 *  Sets the logging level of the Endpoint SDK.
 *  If not called, the default logging level is IQLoggingLevelWarning.
 *
 *  @param level The logging level to use.
 */
- (void)setLoggingLevel:(IQLoggingLevel)level;

#pragma mark - Start and stop

/**
 *  Starts monitoring media playback.
 */
- (void)start;

/**
 *  Shuts down IQPegasus so that it can free its resources.
 *  Call this method in your app's shutdown methods.
 */
- (void)shutdown;

/**
 *  Returns YES if IQPegasus is running, NO otherwise.
 *
 *  @return YES if IQPegasus is running, NO otherwise.
 */
- (BOOL)isRunning;

/**
 *  Enables or disables the SDK. If the flag parameter is set to YES, any
 *  calls to proxyURLForActualURL: will return a proxy URL. If the flag
 *  parameter is set to NO, any calls to proxyURLForActualURL: will return
 *  the original URL unchanged. If your player is currently playing media from
 *  a proxy URL, it will continue to do so until it is replaced with the
 *  unchanged original media URL.
 *
 *  By default, the SDK is enabled.
 *
 *  @param flag When set to YES, the SDK will return proxy URLs from the
 *  proxyURLForActualURL: method. When set to NO, the SDK wikll return unchanged
 *  URLs from the proxyURLForActualURL: method.
 */
- (void)setEnabled:(BOOL)flag;

/**
 *  Fetches the proxy URL version of an actual URL if the SDK is enabled (see
 *  the setEnabled: method for more information). Otherwise it returns the
 *  value of actualURL parameter unchanged. In order to collect and report
 *  metrics of the media being played, this method must be called.
 *
 *  @param actualURL The actual URL to be played by the media player.
 *
 *  @return The proxy version of the actualURL if the SDK is enabled, or
 *  actualURL if it is not enabled.
 */
- (NSURL *)proxyURLForActualURL:(NSURL *)actualURL;

/**
 *  Gets the current listening port. If the port is not known, or not yet
 *  initialized, it will return 0.
 *
 *  @return The current listening port, or 0 if it is not known or initialized.
 */
- (NSUInteger)listeningPort;

#pragma mark - User ID

/**
 *  Sets the User ID in the form of a uint64_t that will be passed down to the
 *  SDK to allow AMP-ASM to track devices belonging to the same user.
 *  Before being passed through the SDK, it is packed into the least
 *  significant bits of a NSUUID object, and the higher order bits are zero.
 *
 *  @param userID The user ID as a uint64_t value.
 */
- (void)setUserIDWithUint64:(uint64_t)userID;

/**
 *  Sets the user ID in the form of a UUID that will be passed down to the SDK
 *  to allow AMP-ASM to track devices belonging to the same user.
 *
 *  @param userID The user ID as a UUID value.
 */
- (void)setUserIDWithUUID:(NSUUID *)userID;

/**
 *  Sets the user ID by passing it as an NSString that will be passed down to
 *  the SDK to allow AMP-ASM to track devices belonging to the same user.
 *  The userID parameter should contain the a UUID (aka GUID). For example:
 *  9aac35b2-1c32-4da1-999a-abe069836293
 *
 *  @param userID The User ID that should contain a UUID (aka GUID).
 *  @param error  A pointer to an NSError object. If an error occurs, it will be
 *                filled with an NSError object containing a description of the
 *                error.
 *
 *  @return YES if the user ID was set successfully, or NO otherwise.
 */
- (BOOL)setUserIDWithString:(NSString *)userID error:(NSError **)error;

/**
 *  Retrieve the user ID as an NSUUID object.
 *
 *  @return An NSUUID object representing the user ID. If the original user ID
 *          parameter was set using the setUserID: method, the user ID is packed
 *          into the least significant 64 bits, and the remainder bits are set
 *          to zero.
 */
- (NSUUID *)userID;

#pragma mark - Stats

/**
 *  Formats monitoring stats as a muti-line string that can be displayed in a UI
 *  or logged for diagnostic information.
 *  The content returned changes in real-time so this is just a snapshot of what
 *  was being monitored at the time this method was called.
 *  If the string is to be displayed in a UI, use a fixed-pitch font for best
 *  results.
 *
 *  @return Snapshot of the monitoring stats formatted as a multi-line string.
 */
- (NSString *)statsAsMultiLineString;

#pragma mark - Application event submission

/**
 *  Call this method when the user has indicated a quality rating within your
 *  user interface. This could be from a star rating, or other such UI.
 *  @param rating The rating in the range [0,10].
 */
- (void)userDidSubmitQualityRating:(NSUInteger)rating;

/**
 *  Call this method when the user has indicated that they are experiencing
 *  poor video quality. This could be a button in your UI or some other
 *  mechanism of your choice.
 *
 *  @param alarmMessage The message you wish to send to the SDK, and AMP-ASM.
 */
- (void)userDidSubmitQualityAlarm:(NSString *)alarmMessage;

/**
 *  Call this method when the user has indicated that they are no longer
 *  experiencing poor video quality. This could be button or a toggle in your
 *  UI or some other mechanism of your choice.
 */
- (void)userDidClearQualityAlarm;

/**
 *  Call this method when your media player starts playing.
 */
- (void)playerDidStart;

/**
 *  Call this method when your media player stops playing.
 */
- (void)playerDidStop;

/**
 *  Call this method when your media player is paused.
 */
- (void)playerDidPause;

/**
 *  Call this method when your media player invoked a rewind.
 */
- (void)playerDidRewind;

/**
 *  Call this method when your media player invoked a fast forward.
 */
- (void)playerDidFastForward;

/**
 *  Call this method when your media player is muted.
 */
- (void)playerDidMute;

/**
 *  Call this method when your media player is un-muted.
 */
- (void)playerDidUnmute;

/**
 *  Call this method when your media player's volume has been set or changed.
 *
 *  @param volume The volume your media player was set to in the range
 *  [0.0f, 1.0f] (normalized).
 */
- (void)playerDidSetVolume:(double)volume;

/**
 *  Call this method when your media player finished playing the media normally
 *  (i.e. the user did not press a stop button, but the media finished on it's
 *  own).
 */
- (void)playerDidFinishPlayback;

/**
 *  Call this method when your media player has encountered an error.
 *
 *  @param error An allocated and initialized NSError object indicating the
 *  error that occurred.
 */
- (void)playerDidEncounterError:(NSError *)error;

/**
 *  Call this method when your media player's buffering status changes.
 *  If the buffering status is not known, return 0.
 *  If the buffer is empty and player will stall, return 1.
 *  If the buffer is partially full and player can play, return 2.
 *  If the buffer is full and player can play without stalling, return 3.
 *
 *  @param bufferingStatus The current buffering status of your player.
 */
- (void)bufferingStatusChanged:(NSUInteger)bufferingStatus;

@end

/**
 The IQPegasusDelegate defines callback methods that allow the SDK to
 inform your application of various SDK events including such as whether the
 configuration was successful, whether the SDK has started or shut down, etc.
 
 There are also various required delegate methods that the SDK will call to 
 allow your application to report metrics back to the SDK, and of course, back 
 into AMP-ASM.
 
 The Apple frameworks such as the Media Player Framework and the AVFoundation
 Framework offer a number of metrics that can be reported into AMP ASM. Your
 application must implement all of the methods of the IQPegasusDelegate class 
 marked as @required, but if the framework or media player subsystem that
 you are using in your application does not support a given metric, you must
 report the designated default value as stated in the inline documentation for 
 that method. Most default (unknown) values will be -1, 0, or nil, as 
 appropriate.
 
 Please note that only the following methods are currently being used to send
 data back to AMP-ASM:
 
 - (NSDate *)reportPlaybackStartDate;
 - (NSString *)reportURI;
 - (int64_t)reportNumberOfBytesTransferred;
 - (NSInteger)reportNumberOfDroppedVideoFrames;
 - (NSInteger)reportNumberOfStalls;
 
 The remaining methods will be reported to AMP-ASM in future release, though
 may be used internally for calculating other metrics. You should try to provide
 correct return values wherever possible to ensure accurate metrics.
 
 */
@protocol IQPegasusDelegate <NSObject>

@optional

/**
 *  Called when IQPegasus has started.
 *
 *  This delegate method is optional.
 */
- (void)IQPegasusDidStart;

/**
 *  Called when IQPegasus has been shutdown.
 *
 *  This delegate method is optional.
 */
- (void)IQPegasusDidShutdown;

/**
 *  Called when IQPegasus has detected that the current VeriStream Index value
 *  has changed. You can use this value to gauge how well the media streaming
 *  experience is currently.
 *
 *  This delegate method is optional.
 *
 *  @param veriStreamIndex The new VeriStream Index value in the range [1,5].
 */
- (void)veriStreamChanged:(NSUInteger)veriStreamIndex;

/**
 *  Called when IQPegasus configuration has succeeded.
 *  IQPegasus can only function when it's configuration has been fetched
 *  from the AMP-ASM configuration endpoint and parsed successfully.
 *
 *  This delegate method is optional.
 */
- (void)IQPegasusConfigurationDidSucceed;

/**
 *  Called when IQPegaus configuration has failed.
 *  IQPegasus can only function when it's configuration has been fetched
 *  from the AMP-ASM configuration endpoint and parsed successfully.
 *
 *  This delegate method is optional.
 *
 *  @param error And error describing why IQPegasus failed to be configured.
 */
- (void)IQPegasusConfigurationDidFailWithError:(NSError *)error;

/**
 *  Called when IQPegaus configuration has failed, and has an extra reason code
 *  and accompanying reason string. You can change the behaviour of your
 *  application based on the reasonCode.
 *  IQPegasus can only function when it's configuration has been fetched
 *  from the AMP-ASM configuration endpoint and parsed successfully.
 *
 *  This delegate method is optional.
 *
 *  @param error        And error describing why IQPegasus failed to be configured.
 *  @param reasonCode   A reason code
 *  @param reasonString A string explaining the error.
 */
- (void)IQPegasusConfigurationDidFailWithError:(NSError *)error
                                    reasonCode:(IQConfigurationFailureReasonCode)reasonCode
                                  reasonString:(NSString *)reasonString;

/**
 *  Called when IQPegasus has encountered an HTTP error (4xx or 5xx)
 *
 *  @param httpError The HTTP status code, usually in the 4xx or 5xx range.
 *  @param url       The URL for which the HTTP error had occurred.
 */
- (void)IQPegasusDidEncounterHTTPError:(NSInteger)httpError forURL:(NSURL *)url;

/**
 *  Called when IQPegasus has started or restarted its internal HTTP proxy server.
 *  This typically occurs when the application has returned from being in the
 *  background so that the application can can safely issue a play or un-pause
 *  command to the player being used.
 *
 *  Internally, when the application goes into background mode, the HTTP proxy
 *  server's listening socket is closed. When the application becomes active,
 *  the listening socket is started again on the same port number, and then
 *  calls this delegate method.
 *
 */
- (void)IQPegasusProxyServerDidStart;

@required

/**
 *  The timestamp for when playback began for the movie log access event.
 *  If the value is not known, return nil.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The timestamp for when playback began for the movie log access event.
 */
- (NSDate *)reportPlaybackStartDate;

/**
 *  The URI of the playback item.
 *  If the value is not known, return nil.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The URI of the playback item.
 */
- (NSString *)reportURI;

/**
 *  The IP address of the web server that was the source of the last delivered
 *  media segment. Can be either an IPv4 or an IPv6 address.
 *  If the value is not known, return nil.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The IP address of the web server that was the source of the last
 *  delivered media segment. Can be either an IPv4 or an IPv6 address.
 */
- (NSString *)reportServerAddress;

/**
 *  A count of changes to the serverAddress property over the last uninterrupted
 *  period of playback.
 *  If the value is not known, return 0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return A count of changes to the serverAddress property over the last 
 *  uninterrupted period of playback.
 */
- (NSUInteger)reportNumberOfServerAddressChanges;

/**
 *  An offset into the playlist where the last uninterrupted period of playback
 *  began, in seconds.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return An offset into the playlist where the last uninterrupted period of
 *  playback began, in seconds.
 */
- (NSTimeInterval)reportPlaybackStartOffset;

/**
 *  The accumulated duration of the media downloaded, in seconds.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The accumulated duration of the media downloaded, in seconds.
 */
- (NSTimeInterval)reportSegmentsDownloadedDuration;

/**
 *  The accumulated duration of the media played, in seconds.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The accumulated duration of the media played, in seconds.
 */
- (NSTimeInterval)reportDurationWatched;

/**
 *  The total number of playback stalls encountered.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The total number of playback stalls encountered.
 */
- (NSInteger)reportNumberOfStalls;

/**
 *  The accumulated number of bytes transferred.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The accumulated number of bytes transferred.
 */
- (int64_t)reportNumberOfBytesTransferred;

/**
 *  The empirical throughput across all media downloaded for the movie player,
 *  in bits per second.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The empirical throughput across all media downloaded for the movie
 *  player, in bits per second.
 */
- (double)reportObservedBitrate;

/**
 *  The throughput required to play the stream, as advertised by the web server,
 *  in bits per second.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The throughput required to play the stream, as advertised by the web
 *  server, in bits per second.
 */
- (double)reportIndicatedBitrate;

/**
 *  Number of network read requests over WWAN.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Number of network read requests over WWAN.
 */
- (NSInteger)reportMediaRequestsWWAN;

/**
 *  The accumulated duration, in seconds, of active network transfer of bytes.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The accumulated duration, in seconds, of active network transfer of
 *  bytes.
 */
- (NSTimeInterval)reportTransferDuration;

/**
 *  A count of media read requests from the server to this client.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return A count of media read requests from the server to this client.
 */
- (NSInteger)reportNumberOfMediaRequests;

/**
 *  The playback type: live, VOD, or from a file.
 *  If the value is not known, return nil.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The playback type: live, VOD, or from a file.
 */
- (NSString *)reportPlaybackType;

/**
 *  The accumulated duration, in seconds, until player item is ready to play.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The accumulated duration, in seconds, until player item is ready to
 *  play.
 */
- (NSTimeInterval)reportStartupTime;

/**
 *  The total number of dropped video frames.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The total number of dropped video frames.
 */
- (NSInteger)reportNumberOfDroppedVideoFrames;

/**
 *  The total number of times the download of the segments took too long.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return The total number of times the download of the segments took too
 *  long.
 */
- (NSInteger)reportDownloadOverdue;

/**
 *  Standard deviation of observed segment download bit rates.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Standard deviation of observed segment download bit rates.
 */
- (double)reportObservedBitrateStandardDeviation;

/**
 *  Maximum observed segment download bit rate.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Maximum observed segment download bit rate.
 */
- (double)reportObservedMaxBitrate;

/**
 *  Minimum observed segment download bit rate.
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Minimum observed segment download bit rate.
 */
- (double)reportObservedMinBitrate;

/**
 *  Bandwidth that caused a switch (up or down).
 *  If the value is not known, return -1.0.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Bandwidth that caused a switch (up or down).
 */
- (double)reportSwitchBitrate;

/**
 *  The buffering status of the player.
 *  Your application must call this method when the buffering state of your
 *  player changes.
 *  If the buffering status is not known, return 0.
 *  If the buffer is empty and player will stall, return 1.
 *  If the buffer is partially full and player can play, return 2.
 *  If the buffer is full and player can play without stalling, return 3.
 *
 *  @return The buffering status of the player.
 */
- (NSUInteger)reportBufferingStatus;

/**
 *  Buffer size of the player.
 *  If the value is not known, return -1.
 *  Called by the SDK when this value is required.
 *  This delegate method is required.
 *
 *  @return Buffer size of the player.
 */
- (NSInteger)reportBufferSize;

@end
