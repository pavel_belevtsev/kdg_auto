//
//  TVNetworkStatusManager.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TvinciSDK/AFNetworkReachabilityManager.h>


#define NotificationName_NetworkStatusChanged @"Network Status changed"

typedef enum APSNetworkStatus
{
    TVNetworkStatus_NoConnection,
    TVNetworkStatus_WiFi,
    TVNetworkStatus_3G
    
}APSNetworkStatus;

@interface APSNetworkTracker : NSObject



+ (APSNetworkTracker *) sharedNetworkStatusManager;
-(void) startTracking;
-(void) stopTracking;
-(void) updateStatus;
@property (assign, nonatomic) APSNetworkStatus status;

@end
