//
//  TVNetworkStatusManager.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "APSNetworkTracker.h"
//#import "Reachability.h"






@implementation APSNetworkTracker



static APSNetworkTracker * sharedInstance = nil;

+ (APSNetworkTracker *) sharedNetworkStatusManager
{
    if ( sharedInstance == nil)
    {
        @synchronized(self)
        {
            sharedInstance = [[APSNetworkTracker alloc] init];
        }
    }
    
    return sharedInstance;
   
}


-(id) init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    
    return self;
}

-(void) setup
{    // for set initial status
    [self updateStatus];
}

-(void)dealloc
{
    [self unsubscribeFromNetworkReachabilityNotifications];
}


- (void)registerForNetworkReachabilityNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AFNetworkingReachabilityDidChangeNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
	[[AFNetworkReachabilityManager sharedManager] startMonitoring];
}


- (void)unsubscribeFromNetworkReachabilityNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:AFNetworkingReachabilityDidChangeNotification object:nil];
}

-(void) reachabilityChanged:(NSNotification *) notification
{
    [self updateStatus];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_NetworkStatusChanged
                                                        object:nil userInfo:nil];
}

-(void) updateStatus
{
    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    switch (status)
    {
        case  AFNetworkReachabilityStatusNotReachable:
        {
            self.status = TVNetworkStatus_NoConnection;
        }
            break;
        case AFNetworkReachabilityStatusReachableViaWWAN:
        {
            self.status = TVNetworkStatus_3G;
        }
            break;
        case AFNetworkReachabilityStatusReachableViaWiFi:
        {
            self.status = TVNetworkStatus_WiFi;
        }
            break;
        default:
            break;
    }

}


-(void) startTracking
{
    [self registerForNetworkReachabilityNotifications];
}

-(void) stopTracking
{
    [self unsubscribeFromNetworkReachabilityNotifications];
}




@end
