//
//  APSEPGViewController_iPhone.m
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGViewController_iPhone.h"
#import "APSChannelAndProgramCell_iPhone.h"
#import "APSDatePicker.h"
#import "APSProgramFullView.h"
#import "APSChannelPage.h"
#import "APSChannelGridCell.h"
#import "APSSimpleTitledCell.h"


@interface APSEPGViewController_iPhone ()<APSDatePickerDelegate,APSProgramFullViewDelegate,APSChannelAndProgramCellDelegate>

@property (strong, nonatomic)  NSMutableDictionary  * dictionaryCurrentProgramByChannelID;
@property (weak, nonatomic) IBOutlet UILabel *labelDisplayedCategoryName;
@property (weak, nonatomic) IBOutlet APSComplexButton *categoryButton;
@property (weak, nonatomic) IBOutlet UILabel *labelFilter;


@property (strong, nonatomic) NSMutableDictionary * dictionaryRequest;
@property (strong, nonatomic) IBOutlet APSDatePicker *datePicker;
@property (strong, nonatomic) APSProgramFullView * programFullView;

@property (retain, nonatomic) NSDate * referenceDate;
@property (weak, nonatomic) IBOutlet UILabel *labelDisplayTime;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UIImageView *filterArrow;
@property (weak, nonatomic) IBOutlet UIView *filterBottomLine;


@property (retain, nonatomic) NSManagedObjectContext * privateContext;



// UI Elemnets




@end

@implementation APSEPGViewController_iPhone

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setupFavorites];
    
    self.dictionaryCurrentProgramByChannelID = [NSMutableDictionary dictionary];
    self.dictionaryRequest = [NSMutableDictionary dictionary];
    [self setupCollectionViews];
    
    self.referenceDate = [NSDate date];
    self.labelDisplayTime.text = [self stringfromDate:self.referenceDate];
    
    //    [self addNoFavoritesView];
    self.collectionViewCategories.backgroundColor = [UIColor colorWithWhite:237.0/255.0 alpha:1];
    [self setupUI];
    
}
-(void) setupUI
{
    self.labelFilter.font = [APSTypography fontRegularWithSize:12];
    self.labelFilter.textColor = [APSTypography colorLightGray];
    self.labelTime.font = [APSTypography fontRegularWithSize:12];
    self.labelTime.textColor = [APSTypography colorLightGray];
    NSString * time = [NSLocalizedString(@"time", nil) uppercaseString];
    NSString * filter = [NSLocalizedString(@"filter", nil) uppercaseString];
    self.labelTime.attributedText = [APSTypography attributedString:time withLetterSpacing:0.5];
    self.labelFilter.attributedText = [APSTypography attributedString:filter withLetterSpacing:0.5];
    
    self.labelDisplayedCategoryName.font = [APSTypography fontRegularWithSize:15];
    self.labelDisplayTime.font = [APSTypography fontRegularWithSize:15];
    self.labelDisplayedCategoryName.textColor = [UIColor colorWithRed:71.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1];
    self.labelDisplayTime.font = [APSTypography fontRegularWithSize:15];
    self.labelDisplayTime.textColor = [UIColor colorWithRed:71.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1];
    self.labelDisplayedCategoryName.text = [self.arrayCategories[0] name];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self setupFavorites];
    [self.collectionViewChannelsLivePrograms reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void) setupCollectionViews
{
    NSString * identifier = NSStringFromClass([APSChannelAndProgramCell_iPhone class]);
    NSString * nibName = [APSChannelAndProgramCell_iPhone nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewChannelsLivePrograms registerNib:nib forCellWithReuseIdentifier:identifier];
    
    identifier = NSStringFromClass([APSSimpleTitledCell class]);
    nibName = [APSSimpleTitledCell nibName];
    nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewCategories registerNib:nib forCellWithReuseIdentifier:identifier];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        
        if ([self.arrayDisplayChannels count] > 0) {
            self.labelAutomation.accessibilityValue = [NSString stringWithFormat:@"%lu channels in EPG", (unsigned long)[self.arrayDisplayChannels count]];
        }
        
        return self.arrayDisplayChannels.count;
    }
    else if (collectionView == self.collectionViewCategories)
    {
        return self.arrayCategories.count;
    }
    
    return 0;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        NSString * className = NSStringFromClass([APSChannelAndProgramCell_iPhone class]);
        APSChannelAndProgramCell_iPhone *cell = (APSChannelAndProgramCell_iPhone *)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
        TVMediaItem * channel = [self.arrayDisplayChannels objectAtIndex:indexPath.item];
        cell.channel = channel;
        cell.backgroundColor = [UIColor clearColor];
        
        APSTVProgram * program = [self.dictionaryCurrentProgramByChannelID objectForKey:channel.epgChannelID];
        
        if (program == nil && [self.dictionaryRequest objectForKey:channel.epgChannelID] == nil)
        {
            [self loadProgramForChannel:channel];
        }
        
        if ((NSNull*)program != [NSNull null] && program != nil)
        {
            
            cell.program = program;
        }
        else
        {
            cell.program = nil;
            if ([program isKindOfClass:[NSNull class]])
            {
                cell.loadIndicatorView.hidden = YES;
            }
            else
            {
                cell.loadIndicatorView.hidden = NO;
                
            }
        }
        
        cell.delegate= self;
        
        cell.buttonFavorite.selected = [self.favoritesAdapter isMediaInFavorites:channel.mediaID];
        cell.buttonHideFavorite.selected = cell.buttonFavorite.selected;
        [cell updateButtonHideFavoriteBackgroundColor];
        
        return cell;
        
    }
    else if (collectionView == self.collectionViewCategories)
    {
        NSString * className = NSStringFromClass([APSSimpleTitledCell class]);
        APSSimpleTitledCell *cell = (APSSimpleTitledCell*)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
        
        TVMenuItem * menuItem = self.arrayCategories[indexPath.item];
        cell.labelTitle.text = menuItem.name;
        cell.labelTitle.textColor = [APSTypography colorMediumGray];
        
        if (indexPath.item == self.selectedCategoryIndex)
        {
            cell.labelTitle.font= [APSTypography fontMediumWithSize:16];
            cell.checkMarkImage.hidden = NO;
        }
        else
        {
            cell.labelTitle.font= [APSTypography fontLightWithSize:16];
            cell.checkMarkImage.hidden = YES;
        }
        return cell;
        
    }
    
    return nil;
}




#pragma mark - UICollectionViewDelegateFlowLayout -

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        return [APSChannelAndProgramCell_iPhone cellSize];
    }
    else if (collectionView == self.collectionViewCategories)
    {
        return [APSSimpleTitledCell cellSize];
    }
    
    return CGSizeZero;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        return [APSChannelAndProgramCell_iPhone cellEdgeInsets];
    }
    else if (collectionView == self.collectionViewCategories)
    {
        return [APSSimpleTitledCell cellEdgeInsets];
    }
    
    return UIEdgeInsetsZero;
    
}

#pragma mark - UICollectionViewDelegate -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.collectionViewCategories)
    {
        [self animateOutCategorySelectionCompeltion:^{
            
            [self reloadVisableChannelsForCategoryIndex:indexPath.item];
            
        }];
        //[self reloadVisableChannelsForCategoryIndex:indexPath.item];
    }
}

-(void) reloadDisplayForNewChannelsCategoryIndex:(NSInteger ) index
{
    self.labelDisplayedCategoryName.text = [self.arrayCategories[index] name];
    self.collectionViewChannelsLivePrograms.alpha = 0;
    
    if (self.arrayDisplayChannels.count >0)
    {
       [self.collectionViewChannelsLivePrograms setContentOffset:CGPointZero animated:NO];
    }
    
    [self.collectionViewChannelsLivePrograms reloadData];
    
    [self.collectionViewChannelsLivePrograms performBatchUpdates:^{
        [self.collectionViewChannelsLivePrograms reloadSections:[NSIndexSet indexSetWithIndex:0]];
        self.collectionViewChannelsLivePrograms.alpha = 1;
        
    } completion:^(BOOL finished) {
        
    
    }];
}




- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        return [APSChannelAndProgramCell_iPhone cellEdgeInsets].top;
    }
    else if (collectionView == self.collectionViewCategories)
    {
        return  [APSSimpleTitledCell cellEdgeInsets].top;
    }
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewChannelsLivePrograms)
    {
        return [APSChannelAndProgramCell_iPhone cellEdgeInsets].left;
    }
    else if (collectionView == self.collectionViewCategories)
    {
        return [APSSimpleTitledCell cellEdgeInsets].left;
    }
    
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (![collectionView.indexPathsForVisibleItems containsObject:indexPath]) {
        
        if (indexPath.item<self.arrayDisplayChannels.count)
        {
            TVMediaItem * channel = self.arrayDisplayChannels[indexPath.item];
            APSBlockOperation * request = [self.dictionaryRequest objectForKey:channel.epgChannelID];
            [request cancel];
            [self.dictionaryRequest removeObjectForKey:channel.epgChannelID];
        }
    }
}


#pragma mark - network
-(void) loadProgramForChannel:(TVMediaItem *) channel
{
    
    __weak  APSEPGViewController_iPhone * weakSelf = self;
    __weak TVMediaItem * weakChannel = channel;
    
    
    APSBlockOperation * request  = [[APSEPGManager sharedEPGManager]
                                    programForChannelId:channel.epgChannelID date:self.referenceDate
                                    privateContext:self.privateContext
                                    starterBlock:^{
                                        
                                    }
                                    failedBlock:^{
                                        
                                    }
                                    completionBlock:^(APSTVProgram *program) {
                                        
                                        if (program != nil)
                                        {
                                            [weakSelf.dictionaryCurrentProgramByChannelID setObject:program forKey:weakChannel.epgChannelID];
                                        }
                                        else
                                        {
                                            [weakSelf.dictionaryCurrentProgramByChannelID setObject:[NSNull null] forKey:weakChannel.epgChannelID];
                                        }
                                        
                                        [weakSelf.collectionViewChannelsLivePrograms reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:[weakSelf.arrayDisplayChannels indexOfObject:weakChannel] inSection:0]]];
                                        
                                    } ];
    
    
    if (request != nil)
    {
        [self.dictionaryRequest setObject:request forKey:channel.epgChannelID];
    }
    
}


#pragma mark - date picker

- (IBAction)changeTimeButtonPressed:(id)sender
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    self.datePicker.delegate = self;
    [self.datePicker animateIn:mainView withCompletion:^{
        NSDate * date = (self.referenceDate != nil)?self.referenceDate :[NSDate date];
        [self.datePicker updateSelectTime:date];
        
    }];
    
}


-(void) datePicker:(APSDatePicker *) datePicker didSelectDate:(NSDate *) date
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    [datePicker animateOut:mainView withCompletion:^{
        
        self.referenceDate = date;
        self.labelDisplayTime.text = [self stringfromDate:self.referenceDate];
        self.dictionaryCurrentProgramByChannelID = [NSMutableDictionary dictionary];
        self.dictionaryRequest = [NSMutableDictionary dictionary];
        [self.collectionViewChannelsLivePrograms reloadData];
    }];
}

-(void) datePicker:(APSDatePicker *) datePicker cancel:(id) sender
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    [datePicker animateOut:mainView withCompletion:^{
    }];
}


#pragma mark - APSProgramFullViewDelegate

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel
{
    [programFullView animateOutWithCompletion:^{
        [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
    }];
    
}


#pragma mark - cells delegate -

-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *) cell didselectChannel:(TVMediaItem *) mediaItem program:(APSTVProgram *)program
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"channel page" eventLabel:[NSString stringWithFormat:@"channel = %@ ",mediaItem.name]];
    
    APSChannelPage * page = (APSChannelPage*)[[APSViewControllersFactory defaultFactory] channelPageWithChannel:mediaItem];
    [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:page isRoot:NO animated:YES];
}


-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *) cell didselectProgram:(APSTVProgram *) program channel:(TVMediaItem *)channel
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    if (program != nil && channel!= nil && (NSNull*)program!= [NSNull null])
    {
        self.programFullView = [APSProgramFullView programFullView];
        self.programFullView.delegate = self;
        self.programFullView.baseViewControllerDelegate = self.baseViewControllerDelegate;
        [self.programFullView showProgramFullViewWithChannel:channel andProgran:program OnView:mainView];
    }
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",channel.name, program.name]];
}

#pragma mark - dates

+(NSDateFormatter *) dateDayFormatter
{
    static NSDateFormatter * dateDateDayFormatter = nil;
    
    if (dateDateDayFormatter ==nil)
    {
        dateDateDayFormatter = [[NSDateFormatter alloc] init];
        dateDateDayFormatter.dateStyle = kCFDateFormatterShortStyle;
        dateDateDayFormatter.timeStyle = kCFDateFormatterNoStyle;
        dateDateDayFormatter.doesRelativeDateFormatting = YES;
        NSLocale *germanLocale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"Application Locale", nil)];
        dateDateDayFormatter.locale = germanLocale;
    }
    
    return dateDateDayFormatter;
}

+(NSDateFormatter *) dateHourFormatter
{
    static NSDateFormatter * dateHourDayFormatter = nil;
    
    if (dateHourDayFormatter ==nil)
    {
        dateHourDayFormatter = [[NSDateFormatter alloc] init];
        dateHourDayFormatter.dateFormat = @"HH:mm";
    }
    
    return dateHourDayFormatter ;
}

+(NSDateFormatter *) dateFormatter
{
    static NSDateFormatter * dateDateFormatter = nil;
    
    if (dateDateFormatter ==nil)
    {
        dateDateFormatter = [[NSDateFormatter alloc] init];
        [dateDateFormatter setDateFormat:@"EEEE"];
        NSLocale *germanLocale = [[NSLocale alloc] initWithLocaleIdentifier:NSLocalizedString(@"Application Locale", nil)];
        dateDateFormatter.locale = germanLocale;

        // [dateDateFormatter setDateFormat:@"E.MMM d"];
        
    }
    
    return dateDateFormatter;
}

-(NSString *) stringfromDate:(NSDate *) date
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsdate = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
    NSDateComponents *componentsNow = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSDateComponents *componentsTommorow = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:60*60*24]];
    
    NSString *dayeString = nil;
    if ((componentsdate.year == componentsNow.year && componentsdate.month == componentsNow.month && componentsdate.day == componentsNow.day) || (componentsdate.year == componentsTommorow.year && componentsdate.month == componentsTommorow.month && componentsdate.day == componentsTommorow.day))
    {
        NSDateFormatter *dateDayFormat = [APSEPGViewController_iPhone dateDayFormatter];
        dayeString = [dateDayFormat stringFromDate:date];
        
    }
    else
    {
        NSDateFormatter *dateDayFormat = [APSEPGViewController_iPhone dateFormatter];
        
        dayeString = [dateDayFormat stringFromDate:date];
    }
    
    NSDateFormatter *dateHourFormat = [APSEPGViewController_iPhone dateHourFormatter];
    NSString * timeString = [dateHourFormat stringFromDate:date];
    return [NSString stringWithFormat:@"%@ %@",dayeString,timeString];
    
}


#pragma mark - categories picker
- (IBAction)userDidSelectCategoryPicker:(UIControl *)sender
{
    if (self.isNoFavoritesScreenShow)
    {
        [self closeNoFavoritesScreenShowWithCompletion:^{
            [self toggleAnimateCategorySelection:sender];
        }];
    }
    
    [self toggleAnimateCategorySelection:sender];
}

-(void)toggleAnimateCategorySelection:(UIControl *)sender
{
    if (!sender.selected)
    {
        [self animateInCategorySelectionCompeltion:nil];
    }
    else
    {
        [self animateOutCategorySelectionCompeltion:nil];
    }
    
}

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

-(void) animateInCategorySelectionCompeltion:(void(^)(void)) completion
{
    
    self.filterArrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180));
    self.collectionViewCategories.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.categoryButton.backgroundColor = [UIColor colorWithWhite:237.0/255.0 alpha:1];
        self.collectionViewCategories.height = self.view.height-self.collectionViewCategories.y;
        [self.collectionViewCategories reloadData];
        self.filterBottomLine.alpha = 0;
    } completion:^(BOOL finished) {
        self.categoryButton.selected = YES;
        if (completion)
        {
            completion();
        }
        
    }];
}


-(void) animateOutCategorySelectionCompeltion:(void(^)(void)) completion
{
    self.filterArrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0));
    [UIView animateWithDuration:0.3 animations:^{
        self.filterBottomLine.alpha = 1;
        self.categoryButton.backgroundColor = [UIColor colorWithWhite:217.0/255.0 alpha:1];
        self.collectionViewCategories.height = 2;
    } completion:^(BOOL finished) {
        self.collectionViewCategories.hidden = YES;
        //self.collectionViewChannelsLivePrograms.alpha = 0;
        self.categoryButton.selected = NO;
        if (completion)
        {
            completion();
        }
    }];
}

#pragma mark - Favorites - Login Favorites

- (void)userDidLogIn:(NSNotification *)notification {
    
    [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
        [self.collectionViewChannelsLivePrograms  reloadData];
    }];
    
}


#pragma- mark - favorites

-(void)setupFavorites
{
    self.favoritesAdapter = [[APSFavoritesAdapter alloc] init];
    self.favoritesAdapter.baseViewControllerDelegate = self.baseViewControllerDelegate;
    
    [self.collectionViewChannelsLivePrograms  reloadData];

    [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
        [self.collectionViewChannelsLivePrograms  reloadData];
    }];
}

-(void) channelAndProgramCell:(APSChannelAndProgramCell_iPhone *)cell didSelectFavorite:(UIButton *) favoriteButton toMediaItem:(TVMediaItem*)mediaItem
{
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
        [self.favoritesAdapter setupMediaItem:mediaItem];
        [self.favoritesAdapter likeButtonPressed:favoriteButton];
        [cell updateUIThenSuccessFavoriteChange];
    }];
}

-(void)showNoFavoritesView
{
    if (self.noFavoritesScreenShow== nil)
    {
        self.noFavoritesScreenShow = [[UIView alloc] initWithFrame:self.collectionViewChannelsLivePrograms.frame];
    }
    
    self.noFavoritesScreenShow.backgroundColor= [UIColor clearColor];
    
    UIView * starBg = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 50, 200)];
    [starBg setBackgroundColor:[APSTypography colorBrand]];
    [self.noFavoritesScreenShow addSubview:starBg];

    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 117, self.noFavoritesScreenShow.height)];
    UIImage * image = [UIImage imageNamed:@"artwork_my_channels_empty_grey_background_iphone"];
    imageView.image = image;
    [self.noFavoritesScreenShow addSubview:imageView];
    
    UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 48)];
    UILabel * subTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 24)];
    
    [title setText:NSLocalizedString(@"No Favorite channels", nil)];
    [title setFont: [APSTypography fontRegularWithSize:18]];
    [title setTextColor:[APSTypography colorGrayWithValue:153]];
    title.textAlignment = NSTextAlignmentCenter;
    title.clipsToBounds = NO;
    title.numberOfLines = 2;
    
    [subTitle setText:NSLocalizedString(@"Drag in order to add a favorite channel", nil)];
    [subTitle setFont: [APSTypography fontRegularWithSize:16]];
    [subTitle setTextColor:[APSTypography colorGrayWithValue:153]];
    subTitle.clipsToBounds = NO;
    subTitle.textAlignment = NSTextAlignmentCenter;
    subTitle.numberOfLines = 0;
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 21;
    style.maximumLineHeight = 21;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    subTitle.attributedText = [[NSAttributedString alloc] initWithString:subTitle.text attributes:attributtes];
    [subTitle sizeToFit];
    subTitle.textAlignment = NSTextAlignmentCenter;
    
    NSInteger centerX = (self.view.width/2) + (imageView.width-24)/2;
    title.center = CGPointMake(centerX, 62);
    subTitle.center = CGPointMake(centerX, 160);
    
    [self.noFavoritesScreenShow addSubview:title];
    [self.noFavoritesScreenShow addSubview:subTitle];
    
    self.noFavoritesScreenShow.alpha = 0;
    [self.view addSubview:self.noFavoritesScreenShow];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.noFavoritesScreenShow.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
}

-(void)closeNoFavoritesScreenShowWithCompletion:(void(^)(void)) completion{
    self.isNoFavoritesScreenShow = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.noFavoritesScreenShow.alpha = 0;
    } completion:^(BOOL finished) {
        [self.noFavoritesScreenShow removeFromSuperview];
        if (completion)
        {
            completion();
        }
    }];
}

@end
