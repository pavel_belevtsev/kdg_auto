//
//  APSEPGViewController_iPhone.h
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGViewController.h"

@interface APSEPGViewController_iPhone : APSEPGViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewChannelsLivePrograms;
@property (weak, nonatomic) IBOutlet UICollectionView * collectionViewCategories;

-(void)showNoFavoritesView;
@end
