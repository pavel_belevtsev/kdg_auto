//
//  APSEPGViewController_iPad.m
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGViewController_iPad.h"
#import "APSChannelGridCell.h"
#import "RSHorizontalTableViewCellProperties.h"
#import "RSHorizontalTableView.h"
#import "EGOCache.h"
#import "APSTimeIndicatorCell.h"
#import "APSNowRulerView.h"
#import "APSDayRulerView.h"
#import "APSProgramFullView.h"
#import "APSSimpleTitledCell.h"




#define    SMALL_HEIGHT 109
#define    WEEK_WIDTH self.widthPerMinute*24*60*7
#define    CONTENT_WIDTH self.widthPerMinute*24*60*7
#define    visableMinutes 2*60.0

#define INDEX_PATH_WITH_INDEX(i) [NSIndexPath indexPathForRow:i inSection:0]
#define kChannelPrograms @"programsForChannelID-%@"

#define ExteraWightToCategoryCell 32

@interface APSEPGViewController_iPad ()<YESChannelCellDelegate,APSDayRulerViewDelegate,APSProgramFullViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionviewCategories;

@property (strong, nonatomic) NSMutableDictionary * dictionaryActiveRequestByChannelID;

@property (retain, nonatomic) APSChannelGridCell * currentScrolledCell;
@property (retain, nonatomic) NSMutableDictionary * dictOfProgramsCellPropertiesByChannelID;
@property (retain, nonatomic) APSNowRulerView * nowRuler;

@property (retain, nonatomic) NSDate * dateReference ;
@property (retain, nonatomic) dispatch_queue_t myCustomEPGQueue;
@property (retain, nonatomic) APSProgramFullView * programFullView;

@property (retain ,nonatomic) UILabel * tempLabel;
@property (strong ,nonatomic) id notificationObserver;


// need to initiate quebe width & dictionary
@property (assign, nonatomic) CGFloat quebeWidth;
@property (retain, nonatomic) NSMutableDictionary * dictionaryOfAvailableQbs;

@end

@implementation APSEPGViewController_iPad

#pragma mark - getter -

-(UILabel *)tempLabel
{
    if (_tempLabel == nil)
    {
        _tempLabel = [[UILabel alloc] init];
    }
    
    return _tempLabel;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tblEPG reloadData];
    [self toggleShowHideButtonArrowLeftAndRightCategories];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     [self setupFavorites];
    
    [self setup];
    self.widthPerMinute = ((float)self.collectionViewTimeRuler.frame.size.width)/(1.5*60.0);
    self.quebeWidth = 6*60*self.widthPerMinute;
    self.currentTime = [self clculateTodayesReferenceTimeFromMidnight]-(self.collectionViewTimeRuler.frame.size.width*(1.0/4.0));
    [self setupTimeRuler];
    [self setupDictionaryOfAvailableQbs];
    [self setupChannelsAndReloadGrid];
    
    [self setupCollectionViews];
    [self registerNotifications];
    self.dayRulerView.presentedDateIndex = 0;
}


-(void) setupDictionaryOfAvailableQbs
{
    self.dictionaryOfAvailableQbs = [NSMutableDictionary dictionary];
    NSInteger numberOfQuebes = WEEK_WIDTH/self.quebeWidth;
    
    self.dictOfProgramsCellPropertiesByChannelID = [NSMutableDictionary dictionary];
    self.dictOfProgramsByChannelID= [NSMutableDictionary dictionary];

    for (TVMediaItem * channel in self.arrayDisplayChannels)
    {
        NSMutableArray * tempArray = [NSMutableArray array];
        for (int i=0 ; i<numberOfQuebes; i++)
        {
            [tempArray addObject:[NSNumber numberWithBool:NO]];
        }
        
        [self.dictionaryOfAvailableQbs setObject:tempArray forKey:channel.epgChannelID];
        [self.dictOfProgramsCellPropertiesByChannelID setObject:[NSMutableArray array] forKey:channel.epgChannelID];
        [self.dictOfProgramsByChannelID setObject:[NSMutableArray array] forKey:channel.epgChannelID];
    }

}

-(void) registerNotifications
{
    
    __unsafe_unretained  APSEPGViewController_iPad * weakSelf = self;

    if (self.notificationObserver!=nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self.notificationObserver];
    }
    
    self.notificationObserver =  [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                    object:nil
                                                    queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
                                                        [weakSelf updateNowRuler] ;
                                                        [weakSelf.tblEPG reloadData];
                                                       } ];

}

-(void) unRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self.notificationObserver];
}



-(void) setupUI
{
    self.tempLabel.font = [APSTypography fontRegularWithSize:16];
}

-(void) setupTimeRuler
{
    self.collectionViewTimeRuler.contentOffset = CGPointMake(self.currentTime, 0);
    self.collectionViewTimeRuler.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:219.0/255.0 blue:219.0/255.0 alpha:1];

    [self updateNowRuler];
}

-(void) updateNowRuler
{
    if (self.nowRuler == nil)
    {
        self.nowRuler = [APSNowRulerView nowRuler];
    }
    
    CGFloat nowOffset =  [self clculateTodayesReferenceTimeFromMidnight];
    self.nowRuler.center = CGPointMake(nowOffset, 0);
    self.nowRuler.origin = CGPointMake(self.nowRuler.origin.x, 0);
    [self.collectionViewTimeRuler addSubview:self.nowRuler];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void) setup
{
    self.myCustomEPGQueue = dispatch_queue_create("com.YES.EPGQueue", NULL);
   
    self.dictionaryActiveRequestByChannelID = [NSMutableDictionary dictionary];
    
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit);
    self.dateReference = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:date]];
}

#pragma mark - calculation -

-(CGFloat) clculateTodayesReferenceTimeFromMidnight
{
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit);
    NSDate * todaysMidNight = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:date]];
    NSDate * now = [NSDate date];
    
    return ([now timeIntervalSinceDate:todaysMidNight]/(60))*self.widthPerMinute;
}

-(void) calculateCellAttributesForChannelIndex:(NSInteger) channelIndex withChannelsArray:(NSArray *) array
{
    TVMediaItem * channel = array[channelIndex];
    NSString * channelID = channel.epgChannelID;
    NSArray * programs =  self.dictOfProgramsByChannelID[channelID];
    CGFloat xOrigin;
    CGFloat width;
    NSMutableArray * programsPropertiesArray = [NSMutableArray array];
    
    for ( int programIndex =0 ; programIndex<programs.count ; programIndex++)
    {
        APSTVProgram *prog = programs[programIndex];
        NSInteger distanceSinceReferenceDateToStartOfProg = [prog.startDateTime timeIntervalSinceDate:self.dateReference];
        xOrigin = distanceSinceReferenceDateToStartOfProg/60*self.widthPerMinute;
        
        NSInteger progLength = ([prog.endDateTime timeIntervalSinceDate:prog.startDateTime] / 60);
        width = progLength * self.widthPerMinute;
        
        
        RSHorizontalTableViewCellProperties * cellProperties = [[RSHorizontalTableViewCellProperties alloc] init];
        cellProperties.frame =  CGRectMake(xOrigin, 0, width, SMALL_HEIGHT);
        cellProperties.index = programIndex;
        [programsPropertiesArray addObject:cellProperties];        
        
    }
    
    self.dictOfProgramsCellPropertiesByChannelID[channelID] =  programsPropertiesArray;
}



#pragma mark - handle Data -

-(void) setupChannelsAndReloadGrid
{
    [self.tblEPG reloadData];
}


-(void)getChannelsProgramsForChannelIndex:(NSIndexPath *) indexPath {
    
    if (indexPath.item>=self.arrayDisplayChannels.count)
    {
        return;
    }
    
    if (![self.tblEPG.indexPathsForVisibleRows containsObject:indexPath])
    {
        return;
    }
    
    __weak TVMediaItem * channel = self.arrayDisplayChannels[indexPath.row];
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:0]];
    
    NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:7*24*60*60]];
    
    componentsStart.hour = 0;
    componentsStart.minute = 0;
    componentsStart.second = 0;

    componentsStart.hour = 0;
    componentsEnd.minute = 0;
    componentsEnd.second = 0;

    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];

    [self.activityIndicator nestedStartAnimating];
    
    __unsafe_unretained  APSEPGViewController_iPad * weakSelf = self;

    
     APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:channel.epgChannelID
                                                           fromDate:startDate
                                                             toDate:endDate
                                                     actualFromDate:startDate
                                                       actualToDate:endDate
                                      includeProgramsDuringfromDate:YES
                                        includeProgramsDuringToDate:YES
                                    privateContext:self.privateContext
                                                       starterBlock:^{
                                                          
                                                       }
                                                        failedBlock:^{
                                                            [weakSelf.activityIndicator nestedStopAnimating];
                                                        }
                                                    completionBlock:^(NSArray * programs) {
                                                        
                                                        if (programs == nil)
                                                        {
                                                            programs = [NSArray array];
                                                        }
                                                        
                                                        [weakSelf.activityIndicator nestedStopAnimating];
                                                        [weakSelf updateChannelsPrograms:programs AtChannelIndex:indexPath.row];
                                                        
                                                    }];
    if (request)
    {
        [self.dictionaryActiveRequestByChannelID setObject:request forKey:channel.epgChannelID];
    }

}



-(void) updateChannelsPrograms:(NSArray *) programs AtChannelIndex:(NSInteger) index
{
    if(index < self.arrayDisplayChannels.count)
    {
        TVMediaItem * channel = self.arrayDisplayChannels[index];
        self.dictOfProgramsByChannelID[channel.epgChannelID] = programs;
        [self calculateCellAttributesForChannelIndex:index withChannelsArray:self.arrayDisplayChannels];
        [self presentProgramsForChannel:channel.epgChannelID];

    }
    
    
}

-(void) presentProgramsForChannel:(NSString*) channelId {
    

    
    TVMediaItem* channel;
    for (int i = 0; i < self.arrayDisplayChannels.count; i++) {
        channel = self.arrayDisplayChannels[i];
        if ([channel.epgChannelID isEqualToString:channelId])
        {
//            [self.tblEPG reloadData];
            //[self.tblEPG reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            NSInteger channelIndex = [self.arrayDisplayChannels indexOfObject:channel];
            APSChannelGridCell * cell = (APSChannelGridCell *)[self.tblEPG cellForRowAtIndexPath:[NSIndexPath indexPathForRow:channelIndex inSection:0]];
            cell.cellsProperties = self.dictOfProgramsCellPropertiesByChannelID[channel.epgChannelID];
            cell.arrayPrograms = self.dictOfProgramsByChannelID[channel.epgChannelID];
            [cell.horizontalTableView reloadData];
            cell.integerTimeOffset = self.currentTime;
            
        }
    }
}

#pragma mark - Table View Methods -

- (NSInteger)tableView:(UITableView *)tableView	numberOfRowsInSection:(NSInteger)section
{
    
    if ([self.arrayDisplayChannels count] > 0) {
        self.labelAutomation.accessibilityValue = [NSString stringWithFormat:@"%lu channels in EPG", (unsigned long)[self.arrayDisplayChannels count]];
    }
    
    return self.arrayDisplayChannels.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *cellIdentifier = NSStringFromClass([APSChannelGridCell class]);
    
    APSChannelGridCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil] lastObject];
    }
    
    cell.tag = indexPath.row ;
    cell.delegate = self;

    TVMediaItem * channel = self.arrayDisplayChannels[indexPath.row];
    [cell setChannel:channel];    
    
    
    cell.widthOfOneMinute = self.widthPerMinute;
    cell.significantChangeWidth = self.widthPerMinute*60;
    
    if(self.dictOfProgramsByChannelID[channel.epgChannelID])
    {
        cell.cellsProperties = self.dictOfProgramsCellPropertiesByChannelID[channel.epgChannelID];
        cell.arrayPrograms = self.dictOfProgramsByChannelID[channel.epgChannelID];
        
    }
    
    cell.horizontalTableView.scrollEnabled = YES;
    CGRect frame = cell.horizontalTableView.frame;
    frame.size.height = SMALL_HEIGHT;
    cell.horizontalTableView.frame = frame;
    [cell.horizontalTableView reloadData];
    cell.integerTimeOffset = self.currentTime;
    
    cell.buttonFavorite.selected = [self.favoritesAdapter isMediaInFavorites:channel.mediaID];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (![tableView.indexPathsForVisibleRows containsObject:indexPath]) {
        
        if (indexPath.item<self.arrayDisplayChannels.count)
        {
            
        TVMediaItem * channel = self.arrayDisplayChannels[indexPath.item];
        APSBlockOperation * operation = [self.dictionaryActiveRequestByChannelID objectForKey:channel.epgChannelID];
        [operation cancel];
        [self.dictionaryActiveRequestByChannelID removeObjectForKey:channel.epgChannelID];
        }
    }
    
    [((APSChannelGridCell *)cell).horizontalTableView  clearData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SMALL_HEIGHT;
}

#pragma - YESChannelCellDelegate methods
-(void) channelCell:(APSChannelGridCell *) sender didSelectShareForProgramAtIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex
{
    
}

-(void) channelCell:(APSChannelGridCell *)cell didSelectBookMark:(UIButton *) bookMarkButton
{
    
}

-(void) channelCell:(APSChannelGridCell *) cell userDidSelectProgramIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    TVMediaItem * channel = self.arrayDisplayChannels[channelIndex];
    APSTVProgram * program = self.dictOfProgramsByChannelID[channel.epgChannelID][programIndex];
    
    if (program != nil && channel!= nil && (NSNull*)program!=[NSNull null])
    {
        self.programFullView = [APSProgramFullView programFullView];
        self.programFullView.delegate = self;
        self.programFullView.baseViewControllerDelegate = self.baseViewControllerDelegate;
        [self.programFullView showProgramFullViewWithChannel:channel andProgran:program OnView:mainView];
    }
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",channel.name, program.name]];
}

-(void) channelCell:(APSChannelGridCell *) cell userDidUnselectProgramIndex:(NSInteger) programIndex channelIndex:(NSInteger) channelIndex
{
    
}

-(void) channelCell:(APSChannelGridCell *)cell userDidScrollToContentOffset:(NSInteger) timeOffset
{
    if(self.currentScrolledCell == nil || cell == self.currentScrolledCell)
    {
        self.currentTime = timeOffset;
        for (APSChannelGridCell* iteratorCell in self.tblEPG.visibleCells)
        {
            if(!(iteratorCell.horizontalTableView.decelerating || iteratorCell.horizontalTableView.dragging) && cell != iteratorCell)
                iteratorCell.horizontalTableView.contentOffset = CGPointMake(timeOffset, 0);
        }
        self.collectionViewTimeRuler.contentOffset = CGPointMake( self.currentTime, 0);
    }

}

-(void) channelCellDidEndScroll:(APSChannelGridCell *)cell
{
    for (APSChannelGridCell* iteratorCell in self.tblEPG.visibleCells)
    {
        if( cell != iteratorCell)
            iteratorCell.horizontalTableView.scrollEnabled =YES;
        
    }
    //   self.hztblTimeRuler.scrollEnabled = YES;
    
    self.currentScrolledCell = nil;
}

-(void) channelCellDidstartScroll:(APSChannelGridCell *)cell
{
    self.currentScrolledCell = cell;
    
    for (APSChannelGridCell* iteratorCell in self.tblEPG.visibleCells)
    {
        if( iteratorCell != cell)
            iteratorCell.horizontalTableView.scrollEnabled =NO;
        
    }
    
    //   self.hztblTimeRuler.scrollEnabled = NO;
}

-(void) channelCellDidSelect:(APSChannelGridCell *)cell channelIndex:(NSInteger) channelIndex
{
    TVMediaItem * channel = self.arrayDisplayChannels[channelIndex];
    [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
}

-(void) channelCellDidSelectPlayChannel:(APSChannelGridCell *)cell
{
    
}

-(void) channelCell:(APSChannelGridCell *)cell didSelectSetAReminder:(UIButton *)reminderButton channelIndex:(NSInteger ) channelIndex programIndex:(NSInteger) programIndex
{
    
}


-(void) channelCell:(APSChannelGridCell *)cell didSelectRecoredButton:(UIButton *) sender channelIndex:(NSInteger ) channelIndex programIndex:(NSInteger) programIndex
{
    
}

-(void) channelCell:(APSChannelGridCell *)cell didScrollSignificantChangeOfChannel:(TVMediaItem *) channel
{
    [self performSelector:@selector(channelViewDidChangeOfChannel:) withObject:channel];

}

#pragma mark - collection view delegate methods -

-(void) setupCollectionViews
{
    NSString * identifier = NSStringFromClass([APSTimeIndicatorCell class]);
    NSString * nibName = [APSTimeIndicatorCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewTimeRuler registerNib:nib forCellWithReuseIdentifier:identifier];
    
    identifier = NSStringFromClass([APSSimpleTitledCell class]);
    nibName = [APSSimpleTitledCell nibName];
    nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionviewCategories registerNib:nib forCellWithReuseIdentifier:identifier];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        return 2*24*7;
    }
    else
    {
        return self.arrayCategories.count;
    }
}

// The cell that is returned must be retrieved from a call to - dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        NSString * className = NSStringFromClass([APSTimeIndicatorCell class]);
        APSTimeIndicatorCell *cell = (APSTimeIndicatorCell*)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];

        NSInteger hour = (indexPath.row/2)%24;
        NSString* min = indexPath.row % 2 == 0 ? @"00" : @"30";
        NSString* time = [NSString stringWithFormat:@"%d:%@",hour,min];
        cell.labelTime.text =time;

        return cell;
    }
    else
    {
       
        NSString * className = NSStringFromClass([APSSimpleTitledCell class]);
        APSSimpleTitledCell *cell = (APSSimpleTitledCell*)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
        
        TVMenuItem * menuItem = self.arrayCategories[indexPath.item];
        cell.labelTitle.text = menuItem.name;
        cell.labelTitle.font= [APSTypography fontRegularWithSize:16];
        if (indexPath.item == self.selectedCategoryIndex)
        {
            cell.labelTitle.textColor = [UIColor whiteColor];
            //[UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:228.0/255.0 alpha:1];
        }
        else
        {
            cell.labelTitle.textColor =[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1];
        }
        return cell;
    }
}


#pragma mark - UICollectionViewDelegateFlowLayout -

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        return CGSizeMake(self.widthPerMinute*30,collectionView.size.height);
    }
    else
    {
        TVMenuItem * menuItem = self.arrayCategories[indexPath.item];
        self.tempLabel.text = menuItem.name;
        [self.tempLabel sizeToFit];
        return CGSizeMake(self.tempLabel.frame.size.width+ExteraWightToCategoryCell, 60);
    }
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewTimeRuler)
    {
    return [APSTimeIndicatorCell cellEdgeInsets];
    }
    else
    {
        return [APSSimpleTitledCell cellEdgeInsets];
    }
}

#pragma mark - UICollectionViewDelegate -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        
    }
    else
    {
        [self reloadVisableChannelsForCategoryIndex:indexPath.item];
    }
}


-(void) reloadDisplayForNewChannelsCategoryIndex:(NSInteger ) index
{
    self.selectedCategoryIndex = index;
    [self.collectionviewCategories reloadData];
    [self.tblEPG reloadData];
    if (self.arrayDisplayChannels.count > 0)
    {
        [self.tblEPG scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
    }
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        return [APSTimeIndicatorCell cellEdgeInsets].top;
    }
    else
    {
        return [APSSimpleTitledCell cellEdgeInsets].top;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (collectionView == self.collectionViewTimeRuler)
    {
        return [APSTimeIndicatorCell cellEdgeInsets].left;
    }
    else
    {
        return [APSSimpleTitledCell cellEdgeInsets].left;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.collectionViewTimeRuler)
    {
        if (self.nowRuler.hidden != (self.nowRuler.center.x-3)<(self.collectionViewTimeRuler.contentOffset.x) )
        {
             self.nowRuler.hidden = (self.nowRuler.center.x-3)<(self.collectionViewTimeRuler.contentOffset.x);
            if (self.nowRuler.hidden == NO)
            {
                [self updateNowRuler];
            }
        }
        
            CGFloat dayWidth = self.widthPerMinute*60*24;
            CGFloat presentedDay = floor(scrollView.contentOffset.x/dayWidth);
            presentedDay = MAX(presentedDay, 0);
            presentedDay = MIN(presentedDay, 6);
            self.dayRulerView.presentedDateIndex = presentedDay;
        
            self.currentTime = scrollView.contentOffset.x;
            for (APSChannelGridCell* cell in self.tblEPG.visibleCells) {
                if(cell != self.currentScrolledCell && !(cell.horizontalTableView.decelerating || cell.horizontalTableView.dragging))
                    cell.integerTimeOffset = self.currentTime;
            }
        }

    
}

#pragma mark - Day Ruler day picker view delegate methods -

-(void) dayRuler:(APSDayRulerView *) dayRuler didSelectDateIndex:(NSInteger)selectedIndex offset:(NSInteger)offset
{
    __unsafe_unretained  APSEPGViewController_iPad * weakSelf = self;

    [UIView animateWithDuration:0.4 animations:^{
        if (selectedIndex!=0)
        {
            weakSelf.collectionViewTimeRuler.contentOffset = CGPointMake(weakSelf.collectionViewTimeRuler.contentOffset.x+(offset*24*60*weakSelf.widthPerMinute), 0) ;
        }
        else
        {
            weakSelf.collectionViewTimeRuler.contentOffset = CGPointMake([weakSelf clculateTodayesReferenceTimeFromMidnight]-(weakSelf.collectionViewTimeRuler.frame.size.width*(1.0/4.0)),0);
        }
    }];

}

-(NSDate *) dayRulerCurrentDate:(APSDayRulerView *) dayRuler;
{
    NSTimeInterval timeInterval = (self.collectionViewTimeRuler.contentOffset.x/self.widthPerMinute)*60;
    return [NSDate dateWithTimeInterval:timeInterval sinceDate:self.dateReference];
}

-(NSDate *) dayRulerTodayOffsetDate:(APSDayRulerView *) dayRuler
{
     ;
    NSTimeInterval timeInterval = ([self clculateTodayesReferenceTimeFromMidnight]-(self.collectionViewTimeRuler.frame.size.width*(1.0/4.0))/self.widthPerMinute)*60;
    return [NSDate dateWithTimeInterval:timeInterval sinceDate:self.dateReference];
}

#pragma mark - APSProgramFullViewDelegate -

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel
{
    [programFullView animateOutWithCompletion:^{
        [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
    }];
}

#pragma mark - Favorites - Login Favorites

- (void)userDidLogIn:(NSNotification *)notification {
    
    __weak  APSEPGViewController_iPad * weakSelf = self;
    
    [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
        
        if (weakSelf.tblEPG)
        {
            [weakSelf.tblEPG reloadData];
        }
        
    }];
    
}

#pragma mark - Favorites -

-(void)setupFavorites
{
    self.favoritesAdapter = [[APSFavoritesAdapter alloc] init];
    self.favoritesAdapter.baseViewControllerDelegate = self.baseViewControllerDelegate;

    //__weak  APSEPGViewController_iPad * weakSelf = self;

    [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
        
        [self.tblEPG reloadData];
        
        /*
        if (weakSelf.tblEPG)
        {
            [weakSelf.tblEPG reloadData];
        }
         */
    }];
}

-(void) channelCell:(APSChannelGridCell *)cell didSelectFavorite:(UIButton *) favoriteButton toMediaItem:(TVMediaItem *)mediaItem
{
    [self.favoritesAdapter setupMediaItem:mediaItem];
    [self.favoritesAdapter likeButtonPressed:favoriteButton];
}

-(void)showNoFavoritesView
{
    if (self.noFavoritesScreenShow == nil  || ![self.noFavoritesScreenShow isDescendantOfView:[self view]])
    {
        [self buildNoFavoritesView];
    }
  
    self.noFavoritesScreenShow.alpha = 0;
    [self.view addSubview:self.noFavoritesScreenShow];
    
    __unsafe_unretained  APSEPGViewController_iPad * weakSelf = self;

    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.nowRuler.alpha = 0;
        weakSelf.noFavoritesScreenShow.alpha=1;
        weakSelf.tblEPG.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)closeNoFavoritesScreenShowWithCompletion:(void(^)(void)) completion
{
    self.noFavoritesScreenShow.alpha = 0;
    [self.noFavoritesScreenShow removeFromSuperview];

    [UIView animateWithDuration:0.3 animations:^{
        self.nowRuler.alpha = 1;
        self.tblEPG.alpha = 1.0;

    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}

-(void)buildNoFavoritesView
{
    self.noFavoritesScreenShow = [[UIView alloc] initWithFrame:self.tblEPG.frame];
    self.noFavoritesScreenShow.backgroundColor= [UIColor clearColor];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.dayRulerView.presentedDayView.width, self.tblEPG.height)];
    UIImage * image = [UIImage imageNamed:@"artwork_my_channels_empty_grey_background_ipad"];
    imageView.image = image;
    [self.noFavoritesScreenShow addSubview:imageView];
    
    UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 34)];
    UILabel * subTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 24)];
    
    [title setText:NSLocalizedString(@"No Favorite channels", nil)];
    [title setFont: [APSTypography fontRegularWithSize:28]];
    [title setTextColor:[APSTypography colorGrayWithValue:153]];
    title.clipsToBounds = NO;
    title.textAlignment = NSTextAlignmentCenter;
    
    [subTitle setText:NSLocalizedString(@"Tap on the star in order to add a favorite channel", nil)];
    [subTitle setFont: [APSTypography fontRegularWithSize:21]];
    [subTitle setTextColor:[APSTypography colorGrayWithValue:153]];
    subTitle.textAlignment = NSTextAlignmentCenter;
    subTitle.numberOfLines = 0;
    NSMutableParagraphStyle *style  = [[NSMutableParagraphStyle alloc] init];
    style.minimumLineHeight = 28;
    style.maximumLineHeight = 28;
    NSDictionary *attributtes = @{NSParagraphStyleAttributeName : style,};
    subTitle.attributedText = [[NSAttributedString alloc] initWithString:subTitle.text attributes:attributtes];
    [subTitle sizeToFit];
    subTitle.textAlignment = NSTextAlignmentCenter;
    
    NSInteger centerX = (self.tblEPG.width/2) + (imageView.width)/2;
    title.center = CGPointMake(centerX, 212);
    subTitle.center = CGPointMake(centerX, 280);
    
    [self.noFavoritesScreenShow addSubview:title];
    
    [self.noFavoritesScreenShow addSubview:subTitle];
}

#pragma mark - Memory -

-(void) dealloc
{
    [self unRegisterNotifications];
    self.tblEPG.delegate = nil;
    self.tblEPG.dataSource = nil;
    
    NSLog(@"dealloc EPGViewController iPAd");
}

#pragma mark - Collectionview Categories control -

-(void) toggleShowHideButtonArrowLeftAndRightCategories
{
    NSInteger allCellsWidth =  [self getCollectionviewCategoriesContentWidth];
    if (allCellsWidth <= self.collectionviewCategories.width)
    {
        self.btnLeftArrowCategories.hidden = YES;
        self.btnRightArrowCategories.hidden = YES;
        [self.collectionviewCategories setScrollEnabled:NO];
        self.collectionviewCategories.x = 25;
    }
    else
    {
        self.btnLeftArrowCategories.hidden = NO;
        self.btnRightArrowCategories.hidden = NO;
        [self.collectionviewCategories setScrollEnabled:YES];
        self.collectionviewCategories.centerX = self.view.centerX;
    }
    [self.collectionviewCategories reloadData];
}

-(NSInteger)getCollectionviewCategoriesContentWidth
{
    
    NSInteger width = 0;
    for (int i=0; i < self.arrayCategories.count; i++)
    {
        TVMenuItem * menuItem = self.arrayCategories[i];
        self.tempLabel.text = menuItem.name;
        [self.tempLabel sizeToFit];
        width += self.tempLabel.frame.size.width+ExteraWightToCategoryCell;
    }
    return width;
}
    
  //  CGSize size =[APSSimpleTitledCell cellSize];
//    NSInteger width = 0;
//    for (UICollectionViewCell * cell in [self.collectionviewCategories visibleCells])
//    {
//        width += cell.width;
//    }
//   // NSInteger allCellsWidht = (size.width)*[self.arrayCategories count];
//    return width;
//}

-(IBAction)scrollFiltersToRight
{
    CGFloat scrollWidth = self.collectionviewCategories.contentOffset.x + self.collectionviewCategories.width;
    
    CGFloat scrollerPageWidth = [self getCollectionviewCategoriesContentWidth];
    
    if (scrollWidth + self.collectionviewCategories.contentOffset.x + self.collectionviewCategories.width > scrollerPageWidth)
    {
        scrollWidth = scrollerPageWidth - self.collectionviewCategories.width;
    }
    [self.collectionviewCategories setContentOffset:CGPointMake(scrollWidth, 0) animated:YES];
}

-(IBAction)scrollFiltersToLeft
{
    CGFloat scrollWidth = self.collectionviewCategories.contentOffset.x - self.collectionviewCategories.width;
    if (scrollWidth < 0)
    {
        scrollWidth = 0;
    }
    [self.collectionviewCategories setContentOffset:CGPointMake(scrollWidth, 0) animated:YES];
}


-(void ) channelViewDidChangeOfChannel:(TVMediaItem *) channel
{
    NSInteger startTimeIndex = floorf((self.currentTime)/self.quebeWidth);
    startTimeIndex = MAX(0, startTimeIndex);
    NSInteger endTimeIndex = ceilf((self.currentTime+self.collectionViewTimeRuler.frame.size.width)/self.quebeWidth);
    endTimeIndex = MIN(WEEK_WIDTH/self.quebeWidth,endTimeIndex);
    
    NSArray * existingQuebesMap = [self.dictionaryOfAvailableQbs objectForKey:channel.epgChannelID];
    BOOL isMissingData = NO;
    for (int index = startTimeIndex;index<endTimeIndex ; index++)
    {
        if ([((NSNumber *)existingQuebesMap[index]) boolValue] == NO)
        {
            isMissingData =YES;
        }
    }
    
    if (isMissingData == NO)
    {
        // do nothing
    }
    else
    {
        NSString * requestQuebesNoteContext = [NSString stringWithFormat:@"[%d,%d]",startTimeIndex,endTimeIndex];
        // check if request with the same quebes is already dispatched
        APSBlockOperation * prevRequest = [self.dictionaryActiveRequestByChannelID objectForKey:channel.epgChannelID];
        NSString * prevRequestContext =  (NSString *)prevRequest.context;
        if ([prevRequestContext isEqualToString:requestQuebesNoteContext])
        {
            // the prev request is already asking for the same quebes;
            // do nothing!
        }
        else
        {
            [prevRequest cancel];
            [self.dictionaryActiveRequestByChannelID removeObjectForKey:channel.epgChannelID];
            NSCalendar * calendar = [NSCalendar currentCalendar];
            NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[self.dateReference dateByAddingTimeInterval:startTimeIndex*(self.quebeWidth/self.widthPerMinute)*60]];
            
            NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[self.dateReference dateByAddingTimeInterval:endTimeIndex*(self.quebeWidth/self.widthPerMinute)*60]];
            
            NSDate * startDate = [calendar dateFromComponents:componentsStart];
            NSDate * endDate = [calendar dateFromComponents:componentsEnd];
            
            [self.activityIndicator nestedStartAnimating];
            
            __unsafe_unretained APSEPGViewController_iPad * weakSelf = self;
            
            __weak APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:channel.epgChannelID
                                                                                                    fromDate:startDate
                                                                                                      toDate:endDate
                                                                                              actualFromDate:startDate
                                                                                                actualToDate:endDate
                                                                               includeProgramsDuringfromDate:YES
                                                                                 includeProgramsDuringToDate:YES
                                                                                              privateContext:self.privateContext
                                                                                                starterBlock:^{
                                                                                                    
                                                                                                }
                                                                                                 failedBlock:^{
                                                                                                     [weakSelf.activityIndicator nestedStopAnimating];
                                                                                                 }
                                                                                             completionBlock:^(NSArray * programs) {
                                                                                                 
                                                                                                 if (programs == nil)
                                                                                                 {
                                                                                                     programs = [NSArray array];
                                                                                                 }
                                                                                                 
                                                                                                 [self.activityIndicator nestedStopAnimating];
                                                                                                 
                                                                                                 
                                                                                                 // removing the request from the running requests
                                                                                                 [weakSelf.dictionaryActiveRequestByChannelID removeObjectForKey:channel.epgChannelID];
                                                                                                 
                                                                                                 [weakSelf updateChannelsPrograms:programs forChannel:channel startIndex:startTimeIndex endIndex:endTimeIndex];
                                                                                             }];
            if (request)
            {
                request.context = requestQuebesNoteContext;
                [self.dictionaryActiveRequestByChannelID setObject:request forKey:channel.epgChannelID];
            }

            
        }
        
    }
 
}

-(void) updateChannelsPrograms:(NSArray *) programs forChannel:(TVMediaItem *) channel startIndex:(NSInteger ) startIndex endIndex:(NSInteger) endIndex
{
    //dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),^{
        // mark the quebes as downloaded
        NSMutableArray * existingQuebesMap = self.dictionaryOfAvailableQbs[channel.epgChannelID];
        for (int index = startIndex;index<endIndex ; index++)
        {
            existingQuebesMap[index] = [NSNumber numberWithBool:YES];
        }
        
        NSMutableArray * existingPrograms = self.dictOfProgramsByChannelID[channel.epgChannelID];
        
        for (APSTVProgram * program in programs)
        {
            if (![existingPrograms containsObject:program])
            {
                [existingPrograms addObject:program];
            }
        }
        
        if ([self.arrayDisplayChannels containsObject:channel])
        {
//            dispatch_sync(dispatch_get_main_queue(),^{
                NSInteger channelIndex = [self.arrayDisplayChannels indexOfObject:channel];
                [self calculateCellAttributesForChannelIndex:channelIndex withChannelsArray:self.arrayDisplayChannels];
                [self presentProgramsForChannel:channel.epgChannelID];
//            });
            
        }
  //  });
   
    
  
}


//-(void) updateChannelsPrograms:(NSArray *) programs AtChannelIndex:(NSInteger) index
//{
//    if(index < self.arrayDisplayChannels.count)
//    {
//        TVMediaItem * channel = self.arrayDisplayChannels[index];
//        self.dictOfProgramsByChannelID[channel.epgChannelID] = programs;
//        [self calculateCellAttributesForChannelIndex:index withChannelsArray:self.arrayDisplayChannels];
//        [self presentProgramsForChannel:channel.epgChannelID];
//        
//    }
//    
//    
//}



@end
