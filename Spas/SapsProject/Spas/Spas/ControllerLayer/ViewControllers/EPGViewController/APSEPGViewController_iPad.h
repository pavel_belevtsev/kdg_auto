//
//  APSEPGViewController_iPad.h
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGViewController.h"
#import "APSDayRulerView.h"

@interface APSEPGViewController_iPad : APSEPGViewController


#pragma mark - channels & programs data

@property (retain, nonatomic) NSMutableDictionary * dictOfProgramsByChannelID;



#pragma mark - EPG UI
@property (retain, nonatomic) IBOutlet UITableView *tblEPG;
@property (assign, nonatomic) CGFloat currentTime;
@property (assign,nonatomic) CGFloat widthPerMinute;

@property (weak, nonatomic) IBOutlet UIButton *btnLeftArrowCategories;

@property (weak, nonatomic) IBOutlet UIButton *btnRightArrowCategories;
#pragma mark - time ruller

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewTimeRuler;

@property (retain, nonatomic) NSManagedObjectContext * privateContext;


#pragma mark - 

@property (weak, nonatomic) IBOutlet APSDayRulerView *dayRulerView;

-(void)showNoFavoritesView;
-(void)closeNoFavoritesScreenShowWithCompletion:(void(^)(void)) completion;

@end
