//
//  APSEPGViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGViewController.h"

#import "APSRefreshProcess.h"

@interface APSEPGViewController ()

@end

@implementation APSEPGViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.activityIndicator.hidden = YES;

    [self.viewMenu setDisplayPosition:DisplayPosition_EPG];
    self.labelTitle.text = [NSLocalizedString(@"TV GUIDE", nil) uppercaseString];
     self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
    TVMenuItem * menuItem  = [[APSLayoutManager sharedLayoutManager] menuItemForPageLayout:@"EPG" menuItems:[[APSLayoutManager sharedLayoutManager] arrayMenuItems]];
    self.arrayCategories = menuItem.children;
    
    self.dictionaryChannelsArrayByCategory = [NSMutableDictionary dictionary];
    
    menuItem = [[APSLayoutManager sharedLayoutManager] menuItemForType:@"All-EPG" menuItems:menuItem.children];
    NSString * tvChannelId = [menuItem.layout objectForKey:@"ChannelID"];
    NSArray * allChannels = [[APSEPGManager sharedEPGManager] allChannels];
    [self.dictionaryChannelsArrayByCategory setObject:allChannels forKey:tvChannelId];
    self.arrayDisplayChannels = allChannels;
    self.selectedCategoryIndex = 0;
    
    [self registerToRefreshDataNotification];
    [self registerToLoginNotifcation];
    
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"TV Program Guid"];
}

#pragma mark - Application Refresh Data Notification -

-(void)registerToRefreshDataNotification
{
    [self unRegisterToRefreshDataNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appRefreshData:)
                                                 name:APSRefreshManagerRefreshCompletedNotification object:nil];
}

-(void)unRegisterToRefreshDataNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:APSRefreshManagerRefreshCompletedNotification
                                                  object:nil];
}

-(void) appRefreshData:(NSNotification *) notification
{
    
    [self reloadVisableChannelsForCategoryIndex:self.selectedCategoryIndex refreshProcess:YES];

}

#pragma mark - Favorites - Login Favorites

-(void) registerToLoginNotifcation
{
    [self unRegisterToLoginNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogIn:) name:TVSessionManagerSignInCompletedNotification object:nil];
}

-(void) unRegisterToLoginNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerSignInCompletedNotification object:nil];
}

- (void)userDidLogIn:(NSNotification *)notification {
    
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(void ) reloadVisableChannelsForCategoryIndex:(NSInteger) index
{

    [self reloadVisableChannelsForCategoryIndex:index refreshProcess:NO];
}

-(void ) reloadVisableChannelsForCategoryIndex:(NSInteger) index refreshProcess:(BOOL)refreshProcess {

    [self.categoryRequest cancel];
   // [self.networkQueue cleen];
    TVMenuItem * menuItem = self.arrayCategories[index];
    NSString * categoryChannelID = [menuItem.layout objectForKey:@"ChannelID"];
    if (categoryChannelID == nil)
    {
        return;
    }

    NSArray * channels = [self.dictionaryChannelsArrayByCategory objectForKey:categoryChannelID];
    
    if (self.isNoFavoritesScreenShow && ([categoryChannelID isEqualToString:@"MyFavorites"] == NO))
    {
        self.isNoFavoritesScreenShow = NO;

        [self closeNoFavoritesScreenShowWithCompletion:^{
     
        }];
    }
    
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:menuItem.name];
    
    if ([categoryChannelID isEqualToString:@"MyFavorites"])
    {
        if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
        {
            [self showFavoritesChannelsWithFavoritesCategoryIndex:index];
        }
        else
        {
            [self showLoginWithFavoritesCategoryIndex:index];
        }
        return;
    }
    
    if ((self.selectedCategoryIndex == index) && !refreshProcess)
    { // category is already opened
        return;
    }

    if (channels)
    {
        self.arrayDisplayChannels = channels;
        self.selectedCategoryIndex = index;
        [self reloadDisplayForNewChannelsCategoryIndex:index];
    }
    else
    {
     
        __weak APSEPGViewController * weakSelf = self;

        TVPAPIRequest * strongRequest = [TVPMediaAPI requestForGetChannelMediaList:[categoryChannelID integerValue]
                                                                       pictureSize:[TVPictureSize iPadEpgChnnel100X100]
                                                                          pageSize:80
                                                                         pageIndex:0
                                                                           orderBy:TVOrderByNoOrder
                                                                          delegate:nil];
        
        __weak TVPAPIRequest * request = strongRequest;
        
        
        [self.activityIndicator nestedStartAnimating];

        [request setStartedBlock:^{
            
        }];
        
        [request setFailedBlock:^{
            [weakSelf.activityIndicator nestedStopAnimating];
        }];
        
        [request setCompletionBlock:^{
           
            [weakSelf.activityIndicator nestedStopAnimating];
            NSMutableArray * channelsMediaArray = [NSMutableArray array];
            NSArray *  channelsDictionary = [request JSONResponse];
            for (NSDictionary * dictionary in channelsDictionary)
            {
                TVMediaItem * mediaItem = [[TVMediaItem alloc] initWithDictionary:dictionary];
                [channelsMediaArray addObject:mediaItem];
            }
            
            [weakSelf.dictionaryChannelsArrayByCategory setObject:channelsMediaArray forKey:categoryChannelID];
            
            weakSelf.arrayDisplayChannels = channelsMediaArray;
            weakSelf.selectedCategoryIndex = index;
            [weakSelf reloadDisplayForNewChannelsCategoryIndex:index];
            
        }];
        self.categoryRequest = request;
        [self sendRequest:request];
    }
    
}


-(void)showFavoritesChannelsWithFavoritesCategoryIndex:(NSInteger) index
{
    
    NSArray * favoritesMediaIds = [self.favoritesAdapter  getAllFavoritesMediaArray];
    
    NSMutableArray * favoritesChannles = [NSMutableArray array];
    NSArray * allChannels = [[APSEPGManager sharedEPGManager] allChannels];
    for (NSString * mediaId in favoritesMediaIds)
    {
        NSInteger index  = [allChannels indexOfObjectPassingTest:^BOOL(TVEPGChannel * channel, NSUInteger idx, BOOL *stop) {
            
            if ([channel.mediaID isEqualToString:mediaId])
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }];
        
        if (index != NSNotFound) {
            TVEPGChannel * channel = allChannels[index];
            [favoritesChannles addObject:channel];
        }
        
        
    }
    
    // sort favorutes channels by channel id.
    id mySort = ^(TVMediaItem  * obj1, TVMediaItem * obj2)
    {
         NSString * channelNumberString1 = [obj1.metaData objectForKey:@"Channel number"];
         NSString * channelNumberString2 = [obj2.metaData objectForKey:@"Channel number"];
        return ([channelNumberString1 integerValue] > [channelNumberString2 integerValue]);
    };
    NSArray * sortedMyObjects = [favoritesChannles sortedArrayUsingComparator:mySort];
    
    self.arrayDisplayChannels = sortedMyObjects;
    self.selectedCategoryIndex = index;
    if ([self.arrayDisplayChannels count] == 0)
    {
        self.isNoFavoritesScreenShow = YES;
        [self reloadDisplayForNewChannelsCategoryIndex:index];
        [self showNoFavoritesView];
    }
    else
    {
        self.isNoFavoritesScreenShow = NO;
        [self reloadDisplayForNewChannelsCategoryIndex:index];
    }
}


-(void)closeNoFavoritesScreenShowWithCompletion:(void(^)(void)) completion{
  
}

-(void)showNoFavoritesView
{
    // impament in sub class;
}

-(void)reloadDisplayForNewChannelsCategoryIndex:(NSInteger ) index
{
    // to implement on subclasses
}


-(void)showLoginWithFavoritesCategoryIndex:(NSInteger) index
{
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
        [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
            
            [self showFavoritesChannelsWithFavoritesCategoryIndex:index];
            
        }];
        
    }];
}


-(void) dealloc
{
    NSLog(@"dealloc EPGViewController");
    
    self.favoritesAdapter = nil;
    [self unRegisterToRefreshDataNotification];
    [self unRegisterToLoginNotifcation];
    
}







@end
