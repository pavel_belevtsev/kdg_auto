//
//  APSEPGViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSFavoritesAdapter.h"

@interface APSEPGViewController : APSBaseBarsViewController



@property (retain, nonatomic) NSArray * arrayCategories;
@property (retain, nonatomic) NSMutableDictionary * dictionaryChannelsArrayByCategory;
@property (assign, nonatomic) NSInteger selectedCategoryIndex;
@property (retain, nonatomic) NSArray * arrayDisplayChannels;
@property (strong, nonatomic) APSFavoritesAdapter * favoritesAdapter;
@property (weak, nonatomic) IBOutlet APSActivityIndicator *activityIndicator;
@property (retain, nonatomic) TVPAPIRequest * categoryRequest;

@property BOOL isNoFavoritesScreenShow;
@property (strong,nonatomic ) UIView * noFavoritesScreenShow;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

-(void)reloadVisableChannelsForCategoryIndex:(NSInteger) index;
-(void)reloadDisplayForNewChannelsCategoryIndex:(NSInteger ) index;
-(void)showNoFavoritesView;
-(void)closeNoFavoritesScreenShowWithCompletion:(void(^)(void)) completion;

@end

