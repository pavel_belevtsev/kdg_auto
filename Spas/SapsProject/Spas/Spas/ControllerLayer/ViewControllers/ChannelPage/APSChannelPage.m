//
//  APSChannelPage.m
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSChannelPage.h"
#import "APSChannelPageCell.h"
#import "UIImage+APSColoredImage.h"
#import "APSProgramFullView.h"

@interface APSChannelPage () <UITableViewDataSource,UITableViewDelegate,APSProgramFullViewDelegate>

@property (strong, nonatomic) APSProgramFullView * programFullView;

@end

@implementation APSChannelPage

#pragma mark - Life -

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.favoritesAdapter = [[APSFavoritesAdapter alloc]initWithMediaItem:self.channel control:self.favoriteButton];
    self.favoritesAdapter.baseViewControllerDelegate = self.baseViewControllerDelegate;
    NSString * channelNumber = [self.channel.metaData objectForKey:@"Channel number"];

    self.labelTitle.text = [NSString stringWithFormat:@"%@ %@",channelNumber,[self.channel.name uppercaseString]];
    self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
    self.allPrograms = nil;
    [self setupUI];
    [self dataSetup];
    [self preperToLoadSelectedDay:0];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"Channel page"];
}
#pragma mark - Memory -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UI -


-(void)setupUI
{
    UIImage * image = [UIImage imageNamed:@"channel_page_star_mask_iphone"];
    UIImage * brandedImage1 = [UIImage coloredImageWithColor:[APSTypography colorBrand] bitMapImage:image];
    [self.favoriteButton setImage:brandedImage1 forState:UIControlStateSelected];
    UIImage * brandedImage2 = [UIImage coloredImageWithColor:[APSTypography colorLightGray] bitMapImage:image];
    [self.favoriteButton setImage:brandedImage2 forState:UIControlStateNormal];
    
    [self.buttonPlay setBackgroundColor:[APSTypography colorBrand]];
    [self.selectorView setBackgroundColor:[UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1]];
    
    self.dropDownLabel.text = NSLocalizedString(@"DAY", nil);
    self.dropDownLabel.attributedText = [APSTypography attributedString:NSLocalizedString(@"DAY", nil) withLetterSpacing:1.0];
    self.dropDownLabel.font = [APSTypography fontRegularWithSize:12];
    self.dropDownLabel.textColor = [UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
    
    self.dayLable.attributedText = [APSTypography attributedString:self.dayLable.text withLetterSpacing:1.0];
    self.dayLable.font =[APSTypography fontRegularWithSize:14];
    self.dayLable.textColor = [UIColor colorWithRed:71.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1];
    
    [self.buttonNow setTitle:NSLocalizedString(@"NOW", nil) forState:UIControlStateNormal];
    [self.buttonGo setTitle:NSLocalizedString(@"Go", nil) forState:UIControlStateNormal];
}

-(void) dataSetup
{
    // configure picker
 
    [self.pickerViewTime setBackgroundColor:[UIColor whiteColor]];
    self.pickerView.y = 0;
    self.pickerAndBarView.y = self.pickerView.height;
    [self.pickerView setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
    self.pickerView.alpha = 0;
  
}



#pragma mark - oadProgramForChannelToDay -

-(void) loadProgramForChannelToDay:(NSInteger)day//:(TVMediaItem *) channel //fromDate:(NSDate*)start toDate:(NSDate*)end
{
    
    NSDate * fromDate = nil;
    NSDate * toDate = nil;
    NSDate * actualFromDate = nil;
    
    NSDate * now = [NSDate date];

    if (day == 0)
    {

        NSCalendar * calendar = [NSCalendar currentCalendar];
        NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:-60*60]];
        componentsStart.minute = 00;
        componentsStart.second = 00;
        

        NSDate * startDate = [calendar dateFromComponents:componentsStart];

        NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit| NSSecondCalendarUnit);
        
        NSDateComponents * compenent =   [calendar components:preservedComponents fromDate:now];
        compenent.hour = 23;
        compenent.minute = 59;
        compenent.second = 59;
        NSDate * tommorowMidNight = [calendar dateFromComponents:compenent];
        
        
        fromDate = startDate;
        toDate = tommorowMidNight;
        actualFromDate = now;
    }
    else
    {
        NSDate * theDay = [NSDate dateWithTimeIntervalSinceNow:day*24*60*60];
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit);
        NSDateComponents * compenent =   [calendar components:preservedComponents fromDate:theDay];
        compenent.hour = 0;
        compenent.second = 0;
        NSDate * theDaySatrtTimeDate = [calendar dateFromComponents:compenent];
        compenent.hour = 23;
        compenent.minute = 59;
        compenent.second = 59;
        NSDate * theDayEndTimeDate = [calendar dateFromComponents:compenent];
        
        fromDate = theDaySatrtTimeDate;
        toDate = theDayEndTimeDate;
        actualFromDate = fromDate;
    }
    
    ASLogInfo(@"fromDate = %@ , toDate= %@",fromDate,toDate);

    __weak TVMediaItem * weakChannel = self.channel;
    if (weakChannel == nil)
    {
        return;
    }
    __weak  APSChannelPage * weakSelf = self;
   // self.blockOperationArray
   APSBlockOperation * oper = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:self.channel.epgChannelID
                                                                                            fromDate:fromDate
                                                                                              toDate:toDate
                                                                                      actualFromDate:actualFromDate
                                                                                        actualToDate:toDate
                                                                       includeProgramsDuringfromDate:YES
                                                                         includeProgramsDuringToDate:YES
     
                                                 privateContext:self.privateContext
                                                   starterBlock:^{
                                                                            [self.activityIndicator startAnimating];
                                                                            [self.activityIndicator setHidden:NO];
                                                                        }
                                      
                                                                        failedBlock:^{
                                                                            [self.activityIndicator stopAnimating];
                                                                            [self.activityIndicator setHidden:YES];
                                                                            [weakSelf.blockOperationArray removeObject:oper];
                                                                            ASLogInfo(@"Failed!");
                                                                        }
                                      
                                                                        completionBlock:^(NSArray * programs)
                                                                        {
                                          
                                                                            if (programs != nil)
                                                                            {
                                                                                NSArray * sortedArray =[programs sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram * mediaItem1, APSTVProgram * mediaItem2)
                                                                                {
                                                                                    NSDate * prog1 =mediaItem1.startDateTime;
                                                                                    NSDate * prog2 =mediaItem2.startDateTime;
                                                                                    return [prog1 compare: prog2];
                                                                                }];
                                                                                [self.activityIndicator stopAnimating];
                                                                                [self.activityIndicator setHidden:YES];
                                                                                self.allPrograms = [NSMutableArray arrayWithArray:sortedArray];
                                                                                [weakSelf.tableView reloadData];
                                                                                
                                                                            }
                                                                            [weakSelf.blockOperationArray removeObject:oper];
            
                                                                    }];
    [self.blockOperationArray addObject:oper];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.allPrograms == nil || [self.allPrograms count] ==0)
    {
        return 0;
    }

    return  self.allPrograms.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"APSChannelPageCell";
    APSChannelPageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [APSChannelPageCell channelPageCell];
    }
    
    APSTVProgram * currProgram = [self.allPrograms objectAtIndex:indexPath.row];
    if ([currProgram isKindOfClass:[APSTVProgram class]] )
    {
        [cell showMode];
        [cell updateCellWithChannelProgram:currProgram];
    }
    else
    {
        [cell clearMode];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    APSTVProgram * currProgram = [self.allPrograms objectAtIndex:indexPath.row];
    ASLogInfo(@"%@, %@",currProgram,self.channel);

    if (self.programFullView == nil || ![self.programFullView isDescendantOfView:[self view]])
    {
        self.programFullView = [APSProgramFullView programFullView];
    }
    self.programFullView.baseViewControllerDelegate = self.baseViewControllerDelegate;
    self.programFullView.delegate = self;
    [self.programFullView showProgramFullViewWithChannel:self.channel andProgran:currProgram OnView:mainView];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",self.channel.name, currProgram.name]];
}

#pragma mark - date Piker -

- (IBAction)openDropDownToSelectOtherDay:(id)sender
{
    UIButton * btn = (UIButton*)sender;
    if (btn.selected) // close
    {
        btn.selected = NO;
        [self animatePickerOut];
    }
    else // open
    {
        btn.selected = YES;
        [self animatePickerIn];
    }
}

-(void) animatePickerIn
{
    self.pickerView.alpha=0.0;
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    self.pickerView.height = mainView.height;
    [mainView addSubview:self.pickerView];
    [UIView animateWithDuration:0.3 animations:^{
        
        self.pickerView.alpha = 1.0;
        self.pickerAndBarView.y = self.pickerView.height - self.pickerAndBarView.height;
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void) animatePickerOut
{
    self.pickerView.alpha=1.0;
    [UIView animateWithDuration:0.4 animations:^{
        
        self.pickerView.alpha = 0.0;
        self.pickerAndBarView.y =self.pickerView.height;
        
    } completion:^(BOOL finished)
    {
        [self.pickerView removeFromSuperview];
    }];

}

- (IBAction)closePikerView:(id)sender
{
    self.dropDownControl.selected = NO;
    [self animatePickerOut];
}

-(NSString*) dateStringByDaysFromToday:(NSInteger) daysFromToday
{
    if (daysFromToday == 0) return NSLocalizedString(@"Today", nil);
    if (daysFromToday == 0+1) return NSLocalizedString(@"Tomorrow", nil);
    
    NSInteger daysToSeconds = daysFromToday*24*60*60;
    NSDate* wantedDate = [[NSDate date] dateByAddingTimeInterval:daysToSeconds];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit) fromDate:wantedDate];
//    NSInteger month = [components month];
    NSInteger day = [components day];
    
    NSString* dayChar =  [self getDayByNSDate:wantedDate];
    
    NSString* stringDate = [NSString stringWithFormat:@"%@ %@ %d", dayChar,[self getMonthByNSDate:wantedDate], day];
    return stringDate;
}

-(NSString *)getDayByNSDate:(NSDate*)date
{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
    NSInteger dayInWeek = [components weekday];
    
    NSString* dayChar = @"";
    switch (dayInWeek) {
        case 1: dayChar =  NSLocalizedString(@"Sun",nil); break;
        case 2: dayChar =  NSLocalizedString(@"Mon",nil); break;
        case 3: dayChar =  NSLocalizedString(@"Tue",nil); break;
        case 4: dayChar =  NSLocalizedString(@"Wed",nil); break;
        case 5: dayChar =  NSLocalizedString(@"Thu",nil); break;
        case 6: dayChar =  NSLocalizedString(@"Fri",nil); break;
        case 7: dayChar =  NSLocalizedString(@"Sat",nil); break;
    }
    
    return dayChar;
}

-(NSString *)getMonthByNSDate:(NSDate*)date
{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
    NSInteger dayInWeek = [components month];
    
    NSString* dayChar = @"";
    switch (dayInWeek)
    {
        case 1: dayChar = NSLocalizedString(@"Jan",nil); break;
        case 2: dayChar = NSLocalizedString(@"Feb",nil); break;
        case 3: dayChar = NSLocalizedString(@"Mar",nil); break;
        case 4: dayChar = NSLocalizedString(@"Apr",nil); break;
        case 5: dayChar = NSLocalizedString(@"May",nil); break;
        case 6: dayChar = NSLocalizedString(@"Jun",nil); break;
        case 7: dayChar = NSLocalizedString(@"Jul",nil); break;
        case 8: dayChar = NSLocalizedString(@"Aug",nil); break;
        case 9: dayChar = NSLocalizedString(@"Sep",nil); break;
        case 10: dayChar = NSLocalizedString(@"Oct",nil); break;
        case 11: dayChar = NSLocalizedString(@"Nov",nil); break;
        case 12: dayChar = NSLocalizedString(@"Dec",nil); break;
    }
    
    return dayChar;
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 7;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self dateStringByDaysFromToday:row];
}

- (IBAction)didSelectNow:(id)sender
{
    [self closePikerView:self.dropDownControl];
    [self preperToLoadSelectedDay:0];
}

- (IBAction)didSelectGo:(id)sender
{
    [self closePikerView:self.dropDownControl];
    NSInteger selectedDay = [self.pickerViewTime selectedRowInComponent:0];
    [self preperToLoadSelectedDay:selectedDay];
}

-(void)preperToLoadSelectedDay:(NSInteger)selectedDay
{
    NSString * str = [self dateStringByDaysFromToday:selectedDay];
    [self.dayLable setText:str];
    [self.dayLable sizeToFit];

    CGSize stringsize = [str sizeWithFont:[APSTypography fontRegularWithSize:15]];
    self.dayLable.width = stringsize.width;
    self.arrowDownImage.x = self.dayLable.width+25;
    
    [self.allPrograms removeAllObjects];
    [self.tableView reloadData];
    
    [self loadProgramForChannelToDay:selectedDay];
}

#pragma mark - Play -

- (IBAction)playChannel:(id)sender
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Play from channel page" eventLabel:[NSString stringWithFormat:@"channel = %@",self.channel.name]];
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
        
        [self playMediaItem:self.channel fileFormatKey:self.channel.mainFile.format];
        
    }];
}

#pragma mark - programFullView Delegate - 

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel
{
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
        [self.programFullView animateOutWithCompletion:^{
            
            [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
            
        }];
        
    }];
}
@end
