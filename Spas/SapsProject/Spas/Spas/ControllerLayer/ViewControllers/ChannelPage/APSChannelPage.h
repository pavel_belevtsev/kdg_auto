//
//  APSChannelPage.h
//  Spas
//
//  Created by Israel Berezin on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseBarsViewController.h"
#import "APSComplexButton.h"
#import "APSFavoritesAdapter.h"

@interface APSChannelPage : APSBaseBarsViewController

@property(strong, nonatomic) TVMediaItem * channel;
@property (strong,nonatomic) NSMutableArray * allPrograms;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *dropDownLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLable;

@property (weak, nonatomic) IBOutlet APSComplexButton *dropDownControl;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *selectorView;

@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *pickerAndBarView;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewTime;

@property (weak, nonatomic) IBOutlet UIImageView *arrowDownImage;
@property (weak, nonatomic) IBOutlet UIButton *buttonNow;
@property (weak, nonatomic) IBOutlet UIButton *buttonGo;

@property (strong, nonatomic) APSFavoritesAdapter * favoritesAdapter;
@property (weak, nonatomic) IBOutlet UIButton *btnGo;

- (IBAction)openDropDownToSelectOtherDay:(id)sender;

- (IBAction)playChannel:(id)sender;
- (IBAction)closePikerView:(id)sender;

@end
