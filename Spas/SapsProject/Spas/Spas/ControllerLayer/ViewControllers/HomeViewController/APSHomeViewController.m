//
//  APSHomeViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSHomeViewController.h"
#import "APSProgramCollectionViewCell.h"
//#import "APSTVChannel.h"
#import "APSChannelPage.h"

#import "APSTestViewController.h"
//#import "APSCoreDataStore.h"
#import "APSProgramFullView.h"

#import "APSRefreshProcess.h"
#import "GCFavoritesStorage.h"

@interface APSHomeViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate,APSProgramCollectionViewCellDelegate,APSProgramFullViewDelegate>

@property (strong , nonatomic) NSArray * arrayChannels;
@property (strong, nonatomic)  NSMutableDictionary  * dictionaryCurrentProgramByChannelID;
@property (strong, nonatomic)  NSMutableDictionary * dictionaryRequest;
@property (strong, nonatomic)  NSManagedObjectContext * privateContext;

@property (retain, nonatomic) APSProgramFullView * programFullView;

@property (strong, nonatomic) IBOutlet UIView *viewNoFavorites;
@property (weak, nonatomic) IBOutlet UILabel *labelNoFavoritesTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNoFavoritesText;

@end

@implementation APSHomeViewController

#pragma mark - Init -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma marl - life -

- (void)viewDidLoad
{
    [super viewDidLoad];
       // Do any additional setup after loading the view from its nib.
    [self setupCollectionView];
    self.dictionaryRequest = [NSMutableDictionary dictionary];

    [self.viewNoFavorites removeFromSuperview];
    
    if (_favoritesMode) {
        
        self.arrayChannels = [NSArray array];
        
        [self setupFavorites];
        
    } else {
        
        if ([[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID count] == 0) {
            
            self.favoritesAdapter = [[APSFavoritesAdapter alloc] init];
            [self.favoritesAdapter downloadFavoritesWithCompletionBlock:nil];
            
        }
        
        self.arrayChannels = [[APSEPGManager sharedEPGManager] allChannels];
        
    }
    
    self.dictionaryRequest = [NSMutableDictionary dictionary];
    self.labelTitle.text = _favoritesMode ? [NSLocalizedString(@"FAVORITES", nil) uppercaseString] : NSLocalizedString(@"HOME", nil);
    self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
	[self.viewMenu setDisplayPosition:DisplayPosition_Home];
    [self registerToAppDidBecomeActiveNotification];
    [self registerToRefreshDataNotification];
    
    if (is_iPad())
    {
        self.btnChannelPage.hidden = YES;
    }
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionViewPrograms reloadData];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:(_favoritesMode ? @"Favorites Screen" : @"Home Screen")];
}

#pragma mark - Memory -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self unRegisterToAppDidBecomeActiveNotification];
    [self unRegisterToRefreshDataNotification];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [self.dictionaryRequest removeAllObjects];
    [self unRegisterToAppDidBecomeActiveNotification];
    [self unRegisterToRefreshDataNotification];
    self.collectionViewPrograms.delegate= nil;
    self.collectionViewPrograms.dataSource = nil;
    
    self.favoritesAdapter = nil;
    
}

#pragma mark - Application Did Become Active Notification -

-(void)registerToAppDidBecomeActiveNotification
{
    [self unRegisterToAppDidBecomeActiveNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)unRegisterToAppDidBecomeActiveNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

-(void) appDidBecomeActive:(NSNotification *) notification
{
    [self.dictionaryRequest removeAllObjects];
    [self.collectionViewPrograms reloadData];
}

#pragma mark - Application Refresh Data Notification -

-(void)registerToRefreshDataNotification
{
    [self unRegisterToRefreshDataNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appRefreshData:)
                                                 name:APSRefreshManagerRefreshCompletedNotification object:nil];
}

-(void)unRegisterToRefreshDataNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:APSRefreshManagerRefreshCompletedNotification
                                                  object:nil];
}

-(void) appRefreshData:(NSNotification *) notification
{

    if (_favoritesMode) {
        
        __weak  APSHomeViewController * weakSelf = self;
        
        [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
            
            if (weakSelf.collectionViewPrograms)
            {
                [weakSelf updateFavoritesList:NO];
            }
            
        }];
        
    } else {
        
        self.arrayChannels = [[APSEPGManager sharedEPGManager] allChannels];
        
    }
    
    [self.dictionaryRequest removeAllObjects];
    [self.collectionViewPrograms reloadData];
    
}


#pragma mark - setting -

-(NSDictionary *)dictionaryCurrentProgramByChannelID
{
    if (_dictionaryCurrentProgramByChannelID == nil)
    {
        _dictionaryCurrentProgramByChannelID = [NSMutableDictionary dictionary];
    }
    
    return _dictionaryCurrentProgramByChannelID;
}

#pragma mark - loadPrograms -


-(void) loadProgramForChannel:(TVMediaItem *) channel
{
    __weak TVMediaItem * weakChannel = channel;
    __weak  APSHomeViewController * weakSelf = self;
    APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programForChannelId:channel.epgChannelID date:[NSDate date] privateContext:self.privateContext starterBlock:^{
        
    } failedBlock:^{
        
    } completionBlock:^(APSTVProgram *program) {
        
        if (program != nil)
        {
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:program forKey:weakChannel.epgChannelID];
        }
        else
        {
            [weakSelf.dictionaryCurrentProgramByChannelID setObject:[NSNull null] forKey:weakChannel.epgChannelID];
        }
        [weakSelf.collectionViewPrograms reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:[weakSelf.arrayChannels indexOfObject:weakChannel] inSection:0]]];
    }];
    
    if (request != nil)
    {
        if (channel)
        {
            [self.dictionaryRequest setObject:request forKey:channel.epgChannelID];
        }
    }
}

-(void) setupCollectionView
{
    NSString * identifier = NSStringFromClass([APSProgramCollectionViewCell class]);
    NSString * nibName = [APSProgramCollectionViewCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewPrograms registerNib:nib forCellWithReuseIdentifier:identifier];
}

#pragma mark - UICollectionViewDataSource -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrayChannels.count;
    //self.arrayPrograms.count;
}

// The cell that is returned must be retrieved from a call to - dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * className = NSStringFromClass([APSProgramCollectionViewCell class]);
    APSProgramCollectionViewCell *cell = (APSProgramCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
    
    cell.delegate = self;
    
    TVMediaItem * channelMedia = nil;
    if (indexPath.item < [self.arrayChannels count])
    {
        channelMedia = [self.arrayChannels objectAtIndex:indexPath.item];
    }
        
    APSTVProgram * program = [self.dictionaryCurrentProgramByChannelID objectForKey:channelMedia.epgChannelID];
    
    
    NSDate * now = [NSDate date];
    
    if ((program == nil && [self.dictionaryRequest objectForKey:channelMedia.epgChannelID] == nil) || ([program isKindOfClass:[APSTVProgram class]] && [program.endDateTime compare:now] == NSOrderedAscending) )
    {
       // NSLog(@"load programs for channel : %@", channelMedia.epgChannelID);
        [self loadProgramForChannel:channelMedia];
    }
    else
    {
        if ([program isKindOfClass:[APSTVProgram class]])
        {
            [cell setChannel:channelMedia andProgram:program];
        }
        else
        {
            [cell setChannel:channelMedia andProgram:nil];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    TVMediaItem * channel = nil;
    if (indexPath.item < [self.arrayChannels count])
    {
        channel  = [self.arrayChannels objectAtIndex:indexPath.item];;
    }
    APSBlockOperation * request = [self.dictionaryRequest objectForKey:channel.epgChannelID];
    if (request)
    {       [request cancel];
        //[request clearDelegatesAndCancel];
        
        if (channel)
        {
            [self.dictionaryRequest removeObjectForKey:channel.epgChannelID];
        }
    }
}



#pragma mark - UICollectionViewDelegateFlowLayout -

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [APSProgramCollectionViewCell cellSize];
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return [APSProgramCollectionViewCell cellEdgeInsets];
}

#pragma mark - UICollectionViewDelegate -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TVMediaItem * channel = nil;
    if (indexPath.item < [self.arrayChannels count])
    {
        channel = [self.arrayChannels objectAtIndex:indexPath.item];
    }
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
         
    } succededBlock:^{
        
        APSTVProgram * program = [self.dictionaryCurrentProgramByChannelID objectForKey:channel.epgChannelID];
        if (![program isKindOfClass:[NSNull class]])
        {
            [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Home Page" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",channel.name, program.name]];
        }
        [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
         
    }];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
            return [APSProgramCollectionViewCell cellEdgeInsets].top;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSProgramCollectionViewCell cellEdgeInsets].left;
}

- (IBAction)showLogin:(id)sender {
    
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
        //NOTHING
    } succededBlock:^{
        //PLAY
    }];
    
}


- (IBAction)openChannelPage:(id)sender
{
    NSInteger index = 0;
    for (int i =0; i<self.arrayChannels.count; i++)
    {
        TVMediaItem * channelMedia = [self.arrayChannels objectAtIndex:i];
        APSTVProgram * program = [self.dictionaryCurrentProgramByChannelID objectForKey:channelMedia.epgChannelID];
        if (program && [program isKindOfClass:[APSTVProgram class]])
        {
            index = i;
            break;
        }
    }
    TVMediaItem * media = [self.arrayChannels objectAtIndex:index];
    APSChannelPage * page = (APSChannelPage*)[[APSViewControllersFactory defaultFactory] channelPageWithChannel:media];
    [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:page isRoot:NO animated:YES];
}

-(IBAction)openTestViewController:(id)sender
{
    APSTestViewController * controller = [[APSTestViewController alloc] init];
    controller.channel = [self.dictionaryCurrentProgramByChannelID objectForKey:[self.dictionaryCurrentProgramByChannelID.allKeys lastObject]];
    [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:controller isRoot:NO animated:YES];

}

#pragma mark - APSProgramCollectionViewCellDelegate

- (void)programCellDidSelectProgramInfo:(APSTVProgram *)program channel:(TVMediaItem *)channel {

    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    
    if (program != nil && channel!= nil && (NSNull*)program!=[NSNull null])
    {
        self.programFullView = [APSProgramFullView programFullView];
        self.programFullView.delegate = self;
        self.programFullView.baseViewControllerDelegate = self.baseViewControllerDelegate;
        [self.programFullView showProgramFullViewWithChannel:channel andProgran:program OnView:mainView showImage:YES]; // SYN-3410
    }
    
    //[[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Home Page" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",channel.name, program.name]];
    
}

#pragma mark - APSProgramFullViewDelegate -

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel
{
    [programFullView animateOutWithCompletion:^{
        [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
    }];
}

#pragma mark - Favorites -

-(void)setupFavorites
{
    self.favoritesAdapter = [[APSFavoritesAdapter alloc] init];
    self.favoritesAdapter.baseViewControllerDelegate = self.baseViewControllerDelegate;
    
    [self updateFavoritesList:YES];
    
    __weak  APSHomeViewController * weakSelf = self;
    
    [self.favoritesAdapter downloadFavoritesWithCompletionBlock:^{
        
        if (weakSelf.collectionViewPrograms)
        {
            [weakSelf updateFavoritesList:NO];
        }
        
    }];
    
    [self.labelNoFavoritesTitle setText:NSLocalizedString(@"No Favorite channels", nil)];
    [self.labelNoFavoritesTitle setFont:[APSTypography fontRegularWithSize:(is_iPad() ? 28.0 : 19.0)]];
    
    if (is_iPad()) {
        [self.labelNoFavoritesText setText:NSLocalizedString(@"You can set favorite channels(iPad)", nil)];
    } else {
        [self.labelNoFavoritesText setText:NSLocalizedString(@"You can set favorite channels(iPhone)", nil)];
    }
    [self.labelNoFavoritesText setFont:[APSTypography fontRegularWithSize:(is_iPad() ? 21.0 : 16.0)]];
    
    self.labelNoFavoritesText.attributedText = [APSTypography attributedString:self.labelNoFavoritesText.text withLineSpacing:(is_iPad() ? 4.0 : 2.0)];
    
    self.viewNoFavorites.frame = CGRectMake(0.0, 0.0, (is_iPad() ? 380.0 : 300.0), (is_iPad() ? 320.0 : 310.0));
    
}

- (void)updateFavoritesList:(BOOL)firstRun {

    NSArray * favoritesMediaIds = [self.favoritesAdapter  getAllFavoritesMediaArray];
    
    NSMutableArray * favoritesChannles = [NSMutableArray array];
    NSArray * allChannels = [[APSEPGManager sharedEPGManager] allChannels];
    for (NSString * mediaId in favoritesMediaIds)
    {
        NSInteger index  = [allChannels indexOfObjectPassingTest:^BOOL(TVEPGChannel * channel, NSUInteger idx, BOOL *stop) {
            
            if ([channel.mediaID isEqualToString:mediaId])
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }];
        
        if (index != NSNotFound) {
            TVEPGChannel * channel = allChannels[index];
            [favoritesChannles addObject:channel];
        }
        
        
    }
    
    // sort favorutes channels by channel id.
    id mySort = ^(TVMediaItem  * obj1, TVMediaItem * obj2)
    {
        NSString * channelNumberString1 = [obj1.metaData objectForKey:@"Channel number"];
        NSString * channelNumberString2 = [obj2.metaData objectForKey:@"Channel number"];
        return ([channelNumberString1 integerValue] > [channelNumberString2 integerValue]);
    };
    NSArray * sortedMyObjects = [favoritesChannles sortedArrayUsingComparator:mySort];
    
    self.arrayChannels = sortedMyObjects;
    
    [self.viewNoFavorites removeFromSuperview];
    
    if (([self.arrayChannels count] == 0) && !firstRun)
    {
        self.viewNoFavorites.center = CGPointMake(self.collectionViewPrograms.center.x, self.collectionViewPrograms.center.y - (is_iPad() ? 73.0 : 52.0));
        
        self.viewNoFavorites.alpha = 0.0;
        [[self.collectionViewPrograms superview] addSubview:self.viewNoFavorites];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.viewNoFavorites.alpha = 1;
        } completion:^(BOOL finished) {
            
        }];
    }

    
    [_collectionViewPrograms reloadData];
    
}

@end
