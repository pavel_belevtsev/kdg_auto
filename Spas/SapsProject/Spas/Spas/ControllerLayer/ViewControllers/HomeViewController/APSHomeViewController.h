//
//  APSHomeViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSFavoritesAdapter.h"

@interface APSHomeViewController : APSBaseBarsViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewPrograms;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelPage;

@property (strong, nonatomic) APSFavoritesAdapter * favoritesAdapter;
@property BOOL favoritesMode;

@end
