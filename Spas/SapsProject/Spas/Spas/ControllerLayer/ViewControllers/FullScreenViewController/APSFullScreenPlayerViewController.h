//
//  APSPlayerViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSGeneralPlayerErrorView.h"

@class APSPurePlayerViewController;
@class APSFullScreenTopBar;

@interface APSFullScreenPlayerViewController : APSBaseViewController
@property (assign, nonatomic) APSPurePlayerViewController * purePlayerViewController;

@property (strong, nonatomic) IBOutlet APSFullScreenTopBar * topPlayerBar;
@property (strong, nonatomic) IBOutlet APSPlayerBar * buttomPlayerBar;
@property (strong, nonatomic) NSArray * arrayAllChannels;

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeUpGestureRecognizer;

@property (copy)void (^minimizeBlock)(UIViewController * destinationViewController);
@property (copy)void (^maximizeBlock)(UIViewController * sourceViewController);

@property (strong, nonatomic) APSGeneralPlayerErrorView * generalPlayerErrorView;

- (IBAction)displayResumeWatching:(id)sender;

-(BOOL)isShowGeneralError;
@end
