//
//  APSPlayerViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSFullScreenPlayerViewController.h"
#import "APSPurePlayerViewController.h"
#import <objc/message.h>
#import "APSPlayerBar.h"
#import "APSMiniEPGViewController.h"
#import "APSFullScreenOutOfNetworkView.h"
#import "APSResumeWatchingView.h"
#import "APSFullScreenTopBar.h"
#import "APSChannelPanel.h"
#import "APSNoInternetPlayerView.h"
#import "APSNetworkManager.h"
#import "APSSessionManager.h"
#import "APSGeneralPlayerErrorView.h"
#import "CDNHandler.h"
#import "APSZappingCoachMarks.h"
#import "APSTooltip.h"


#import <AVFoundation/AVFoundation.h>

#define BarsTimeDelay 6

#define StatusBarHigh 20

#define kInfoTooltipWasDisplayed @"kInfoTooltipWasDisplayed"

@interface APSFullScreenPlayerViewController ()<APSPlayerBarDelegate,APSFullScreenOutOfNetworkViewDelegate,APSFullScreenTopBarDelegate,APSMiniEPGViewControllerDelegate,APSChannelPanel,
APSResumeWatchingViewDelegate, APSNoInternetPlayerViewDelegate, APSGeneralPlayerErrorViewDelegate,APSZappingCoachMarksDelegate, APSTooltipDelegate>

@property (assign, nonatomic) BOOL statusBarHidden;
@property (assign , nonatomic) BOOL isBarsVisible;
@property (assign , nonatomic) BOOL isInBlockMode;
@property (strong, nonatomic) NSTimer * timerBars;
@property (assign , nonatomic) BOOL isMiniEPGVisible;
@property (assign , nonatomic) BOOL isChannelPanelVisible;
@property (assign , nonatomic) BOOL isVolumeVisible;
@property (assign, nonatomic) BOOL isShowNoIntenetView;
@property (assign, nonatomic) BOOL isShowGeneralPlayerErrorView;


@property (strong, nonatomic) APSMiniEPGViewController* miniEPGBar;
@property (strong, nonatomic) APSChannelPanel * channelPanel;
@property (strong, nonatomic) IBOutlet FXBlurView * testBlurView;
@property (strong, nonatomic) APSNoInternetPlayerView * noInternetPlayerView;

@property (assign , nonatomic) BOOL showingOutOfNetworkAlert; // flag is "YES" when the "out of network" alert is displayed
@property (assign , nonatomic) BOOL showingResumeWatchView; // flag is "YES" when the "out of network" alert is displayed

@property (strong, nonatomic) APSFullScreenOutOfNetworkView * outOfNetworkView;
@property (strong, nonatomic) APSResumeWatchingView * resumeWatchingView;

@property (strong, nonatomic) APSZappingCoachMarks * zappingCoachMarks;
@property (assign , nonatomic) BOOL isShowZappingCoachMarks;
@property (assign , nonatomic) BOOL isShowInfoTooltip;
@property (strong , nonatomic) APSTooltip* tooltip;

@property BOOL allowSwipe;
@property (assign, nonatomic) CGFloat lastPitchScale;
@end

@implementation APSFullScreenPlayerViewController


-(void)dealloc
{
    self.buttomPlayerBar.delegate = nil;
    self.buttomPlayerBar = nil;
    [self.timerBars invalidate];

    [self unregisterForNotifcation];
    self.miniEPGBar = nil;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.lastPitchScale = 1.0f;
    self.allowSwipe = YES;
    [self showBarsCompletion:nil];
    
    [self registerToPurePlayerHaveErrorNotificationName];

    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"Player Page"];

    if (self.maximizeBlock)
    {
        self.maximizeBlock(self);
        if (self.isShowNoIntenetView==NO)
        {
            [self reachabilityChanged:nil];
        }
        self.maximizeBlock = nil;
    }
    
      
}


-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self unregisterToPurePlayerHaveErrorNotificationName];

    if (self.minimizeBlock)
    {
        self.minimizeBlock(self);
        self.minimizeBlock = nil;
    }

}


#define chanenlIDKey @"Channel ID"

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.arrayAllChannels = [[[APSEPGManager sharedEPGManager] allChannels] sortedArrayUsingComparator:^NSComparisonResult(TVMediaItem * obj1, TVMediaItem * obj2) {
        if ([[[obj1 metaData] objectForKey:chanenlIDKey] integerValue] < [[[obj2 metaData] objectForKey:chanenlIDKey] integerValue])
        {
            return NSOrderedAscending;
        }
        else if([[[obj1 metaData] objectForKey:chanenlIDKey] integerValue] == [[[obj2 metaData] objectForKey:chanenlIDKey] integerValue])
        {
            return NSOrderedSame;
        }
        else
        {
            return NSOrderedDescending;
        }
    }];
    
//    self.swipeUpGestureRecognizer.view.height = self.view.bounds.size.height-40;
//    self.purePlayerViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
//    [self.view insertSubview:self.purePlayerViewController.view atIndex:0];
    
    [self updateUIForStreamingChanged];
    [self registerForNotification];
    [self.buttomPlayerBar streamingChanged:nil];
   
    [self setupChannelPanel];
    self.topPlayerBar.delegate = self;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
    
    
}

-(void) setupChannels
{
    
}

-(void) registerForNotification
{
    [self unregisterForNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(streamingChanged:) name:PurePlayerStreamingChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(areaChange:) name:APSSessionManagerUserLoginStausUpdateNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidFinish:) name:PurePlayerDidFinishPlayingNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogOut:) name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionChange:) name:AVAudioSessionRouteChangeNotification object:nil];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(purePlayerHaveError:) name:PurePlayerHaveErrorNotificationName object:nil];
    [self registerToNoInerenetNotifcation];
//    [self unRegisterToAppDidBecomeActiveNotification];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground:)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissResumeWatchView)
                                                 name:PurePlayerDismissResumeWatchViewNotificationName object:nil];
    
    NSError *error = nil;
    [[AVAudioSession sharedInstance]
                    setCategory:AVAudioSessionCategoryPlayback
                    error:&error];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissResumeWatchView) name:PurePlayerDismissResumeWatchViewNotificationName object:nil];

    
}

-(void) handleEnteredBackground:(NSNotification *) notification
{
    ASLogInfo(@"appWillResignActive");
    self.allowSwipe = NO;
}

-(void) appDidBecomeActive:(NSNotification *) notification
{
    ASLogInfo(@"appDidBecomeActive");
    self.allowSwipe = YES;
}

-(void) unregisterForNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerStreamingChangedNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:APSSessionManagerUserLoginStausUpdateNotification object:nil];
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerHaveErrorNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerDismissResumeWatchViewNotificationName object:nil];

    [self unregisterToNoInerenetNotifcation];

}

-(void) updateUIForStreamingChanged
{
    if ([[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey] != nil && self.purePlayerViewController.currentMediaItem.name!= nil)
    {
        NSString * streamingTitleString = [NSString stringWithFormat:@"%@ %@",[[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey],self.purePlayerViewController.currentMediaItem.name];
        NSMutableAttributedString *st = [[NSMutableAttributedString alloc] initWithString:streamingTitleString];
        
        NSRange channelNumber = [[st string] rangeOfString:[[self.purePlayerViewController.currentMediaItem metaData] objectForKey:chanenlIDKey] options:NSCaseInsensitiveSearch];
        NSRange channelName = [[st string] rangeOfString:self.purePlayerViewController.currentMediaItem.name];
        
        [st addAttribute:NSFontAttributeName value:[APSTypography fontLightWithSize:20] range:channelNumber];
        [st addAttribute:NSFontAttributeName value:[APSTypography fontRegularWithSize:20] range:channelName];
        
        self.topPlayerBar.labelTitle.attributedText = st;
        self.topPlayerBar.labelTitle.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setters & getters

-(APSFullScreenOutOfNetworkView *) outOfNetworkView
{
    if (_outOfNetworkView == nil)
    {
        _outOfNetworkView = [APSFullScreenOutOfNetworkView fullScreenOutOfNetworkView];
    }
    
    return _outOfNetworkView;
}

-(APSResumeWatchingView *) resumeWatchingView
{
    if (_resumeWatchingView == nil)
    {
        _resumeWatchingView = [APSResumeWatchingView resumeWatchingView];
    }
    
    return _resumeWatchingView;
}

-(void)setPurePlayerViewController:(APSPurePlayerViewController *)purePlayerViewController
{
    if (_purePlayerViewController != purePlayerViewController)
    {
        _purePlayerViewController = purePlayerViewController;
    }
}

-(APSMiniEPGViewController *) miniEPGBar
{
    if (_miniEPGBar == nil)
    {
        _miniEPGBar = (APSMiniEPGViewController *)[[APSViewControllersFactory  defaultFactory] miniEPGViewController];
        _miniEPGBar.delegate = self;
    }
    
    return _miniEPGBar;
}

-(APSGeneralPlayerErrorView *)generalPlayerErrorView
{
    _generalPlayerErrorView = [APSGeneralPlayerErrorView generalPlayerErrorView];
    [_generalPlayerErrorView registerForNotification];
    _generalPlayerErrorView.delegate = self;
    _generalPlayerErrorView.baseViewControllerDelegate = self.baseViewControllerDelegate;
    return  _generalPlayerErrorView;
}

-(APSNoInternetPlayerView *) noInternetPlayerView
{
    if (_noInternetPlayerView == nil)
    {
        _noInternetPlayerView =  [APSNoInternetPlayerView noInternetPlayerView];
        _noInternetPlayerView.delegate = self;
        self.isShowNoIntenetView = NO;
    }
    return _noInternetPlayerView;
}

-(APSZappingCoachMarks*)zappingCoachMarks
{
    if (_zappingCoachMarks == nil)
    {
        _zappingCoachMarks = [APSZappingCoachMarks zappingCoachMarks];
        _zappingCoachMarks.delegate =self;
    }
    return _zappingCoachMarks;
}

-(void)setupChannelPanel
{
    self.channelPanel = (APSChannelPanel*)[[APSViewControllersFactory defaultFactory] channelPanel];
    NSInteger  top = self.topPlayerBar.height;
    NSInteger bottom = self.buttomPlayerBar.height;
    NSInteger panelHeight = self.channelPanel.view.height - (top + bottom);
    CGRect offScreenFrame = self.channelPanel.view.frame;
    offScreenFrame.origin.x = self.view.origin.x - self.channelPanel.view.frame.size.width;
    offScreenFrame.origin.y =top;
    offScreenFrame.size.height = panelHeight;
    self.channelPanel.view.frame = offScreenFrame;
    self.isChannelPanelVisible = NO;
    self.channelPanel.delegate = self;
    [self.view insertSubview:self.channelPanel.view belowSubview:self.buttomPlayerBar];

    ASLogInfo(@"subviews = %@",self.view.subviews)
   // [self.view addSubview:self.channelPanel.view];
}

#pragma mark - orientation

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


-(void) toggleBarsCompletion:(void(^)(void)) completion
{
    if (self.isBarsVisible)
    {
        [self hideBarsCompletion:completion];
    }
    else
    {
        [self showBarsCompletion:completion];
    }
}

-(void) showBarsCompletion:(void(^)(void)) completion
{
    
    [self.timerBars invalidate];
    self.timerBars = nil;
    self.timerBars = [NSTimer scheduledTimerWithTimeInterval:BarsTimeDelay target:self selector:@selector(hideBarTimeInvocationMethod) userInfo:nil repeats:NO];
    
    self.topPlayerBar.y = -self.topPlayerBar.height;
    self.topPlayerBar.width = self.view.bounds.size.width;
    [self.view insertSubview:self.topPlayerBar belowSubview:self.outOfNetworkView];
    
    self.buttomPlayerBar.y = self.view.bounds.size.height;
    self.buttomPlayerBar.width = self.view.bounds.size.width;
    [self.view insertSubview:self.buttomPlayerBar belowSubview:self.outOfNetworkView];
    
    [self showStatusBar:YES];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.topPlayerBar.y = 0;
        self.buttomPlayerBar.y = self.view.bounds.size.height-self.buttomPlayerBar.height;

    } completion:^(BOOL finished) {
        
        self.isBarsVisible = YES;
        if (completion)
        {
            completion();
        }
    }];
}


-(void) hideBarsCompletion:(void(^)(void)) completion
{
    [self.timerBars invalidate];
    self.timerBars = nil;
    [self showStatusBar:NO];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.buttomPlayerBar.y = self.view.bounds.size.height;
        self.topPlayerBar.y = -self.topPlayerBar.height;;
    } completion:^(BOOL finished) {
            self.isBarsVisible = NO;
        
        if (completion)
        {
            completion();
        }
    }];

}

-(void) hideBarTimeInvocationMethod
{
    [self hideBarsCompletion:nil];
}


-(APSPlayerBar *)buttomPlayerBar
{
    if (_buttomPlayerBar == nil)
    {
        _buttomPlayerBar = [APSPlayerBar playBarOfType:PlayerBarType_FullScreen];
        _buttomPlayerBar.purePlayerViewController = self.purePlayerViewController;
        _buttomPlayerBar.delegate = self;
    }
    
    return _buttomPlayerBar;
}


-(void) playerBar:(APSPlayerBar *) playerbar didSelectClose:(UIButton *) sender
{
    // not need to implement because x is only functional on minimized player bar
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectMaximizeVideo:(UIButton *) sender
{
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectMinimizeVideo:(UIButton *) sender
{
    [self hideBarsCompletion:^{
         [self.baseViewControllerDelegate baseViewControllerMainViewMinimizePlayer:self animated:YES];
    }];
   
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectInfoButton:(UIButton *) sender
{
    [self toggleShowHideMiniEPGCompletion:nil];
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectAllChannelsButton:(UIButton *) sender
{
    [self showChannelPanelCompletion:nil];
}

-(void) toggleShowHideMiniEPGCompletion:(void(^)(void)) completion
{
    
    TVMediaItem * currChannel = self.purePlayerViewController.currentMediaItem;
  
    if (self.isMiniEPGVisible)
    {
        if(!self.showingResumeWatchView){
            
            [self resetBarsTimer];
        }
        self.buttomPlayerBar.buttonInfo.selected = NO;
    }
    else
    {
        [self showForcePlyerBar];
        [self closeAllViewsOnPlayer];
        [self showForcePlyerBar];
        [self.miniEPGBar willOpenMiniEpgWithChannel:currChannel];
        self.buttomPlayerBar.buttonInfo.selected = YES;

        [self.view insertSubview:self.miniEPGBar.view belowSubview:self.buttomPlayerBar];
        self.miniEPGBar.view.y = self.view.bounds.size.height;
        self.miniEPGBar.view.width = self.view.bounds.size.width;
    }

    self.isMiniEPGVisible = !self.isMiniEPGVisible;
    [UIView animateWithDuration:0.3 animations:^{

        if (self.isMiniEPGVisible)
        {
            //open
            self.miniEPGBar.view.y = self.view.bounds.size.height - self.miniEPGBar.view.size.height- self.buttomPlayerBar.height;
        }
        else
        {
            //close
            self.miniEPGBar.view.y = self.view.bounds.size.height;
        }
    } completion:^(BOOL finished) {
        
        if (completion)
        {
            completion();
        }
    }];
}


-(void) showChannelPanelCompletion:(void(^)(void)) completion
{
    TVMediaItem * currChannel = self.purePlayerViewController.currentMediaItem;
    
    if (self.isChannelPanelVisible) // go to close it!
    {
        [self.buttomPlayerBar updateAllChannelsButtonSlected:NO];
        [self.topPlayerBar updateAllChannelsButtonSlected:NO];
        [self resetBarsTimer];
    }
    else
    {
        [self.channelPanel updateSelectedCellToChannel:currChannel];
        [self.buttomPlayerBar updateAllChannelsButtonSlected:YES];
        [self.topPlayerBar updateAllChannelsButtonSlected:YES];
        [self showForcePlyerBar];
        [self closeAllViewsOnPlayer];
        [self showForcePlyerBar];
    }
    self.isChannelPanelVisible = !self.isChannelPanelVisible;

    [UIView animateWithDuration:0.3 animations:^{
        
        if (self.isChannelPanelVisible)
        {
            //open
            self.channelPanel.view.x = self.view.bounds.origin.x;
        }
        else
        {
            //close
            self.channelPanel.view.x = self.view.bounds.origin.x - self.channelPanel.view.width;
        }
        
    } completion:^(BOOL finished) {
        
        if (completion)
        {
            completion();
        }
    }];
}


-(UIView *) playerBarBlursUnderlayingView:(APSPlayerBar *) playerbar
{
    return  self.view;
}

-(void) playerBar:(APSPlayerBar *) playerbar toggleShowHideVolumeView:(UIButton *) sender show:(BOOL)isShow
{
    if (isShow)
    {
        [self closeAllViewsOnPlayer];
    }
    self.isVolumeVisible = isShow;
}

-(void) playerBar:(APSPlayerBar *) playerbar volumeValueChanged:(CGFloat) value
{
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    [purePlayer setVolume:value];
}

-(CGFloat) playerBarVolume:(APSPlayerBar *) playerbar
{
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    return  [purePlayer volume];

}

-(void) playerBar:(APSPlayerBar *)playerbar delayBy:(id) sender
{
    [self showForcePlyerBar];
}

-(void) playerBar:(APSPlayerBar *)playerbar stopDelay:(id) sender
{
    [self resetBarsTimer];
}

-(void)showForcePlyerBar
{
    [self.timerBars invalidate];
    self.timerBars = nil;
    self.isInBlockMode = YES;
}

-(void)resetBarsTimer
{
    [self.timerBars invalidate];
    self.timerBars = nil;

    if(self.showingResumeWatchView || self.isShowZappingCoachMarks || self.isShowInfoTooltip)
        return;

    self.timerBars = [NSTimer scheduledTimerWithTimeInterval:BarsTimeDelay target:self selector:@selector(hideBarTimeInvocationMethod) userInfo:nil repeats:NO];
    self.isInBlockMode = NO;
}

#pragma mark - other methods -

- (IBAction)back:(id)sender
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@" Minimize player from “SCHLIESSEN”" eventLabel:@""];
    self.isChannelPanelVisible = NO;
    self.isMiniEPGVisible = NO;
    self.isVolumeVisible = NO;
    self.isInBlockMode = NO;
    [self hideBarsCompletion:^{
        [self.baseViewControllerDelegate baseViewControllerMainViewMinimizePlayer:self animated:YES];
    }];
}


#pragma mark - gesture recognizer -


- (IBAction)oneTapOnScreen:(id)sender
{
    [self showIfNeededZappingCoachMarks];

    if (!self.isInBlockMode)
    {
        [self toggleBarsCompletion:nil];
    }
    [self closeAllViewsOnPlayer];
    [self showIfNeededInfoTooltip];
}

-(void)closeAllViewsOnPlayer
{
    if (self.isMiniEPGVisible)
    {
        [self toggleShowHideMiniEPGCompletion:nil];
    }
    if (self.isChannelPanelVisible)
    {
        [self showChannelPanelCompletion:nil];
    }
    if (self.isVolumeVisible)
    {
        [self.buttomPlayerBar animateOutVolumeSender:nil completion:nil];
    }
}

- (IBAction)swipeDownOnScreen:(id)sender
{
    NSInteger playedChannelIndex = [self indexOfChanel:self.purePlayerViewController.currentMediaItem];
    NSInteger prevIndex = (self.arrayAllChannels.count+playedChannelIndex-1)%self.arrayAllChannels.count;
    TVMediaItem * previuseMediaItem = self.arrayAllChannels[prevIndex];
    
    [self dismissResumeWatchView];
    [self dismissZappingCoachMarks];

    [self.purePlayerViewController playMediaItem:previuseMediaItem fileFormatKey:previuseMediaItem.mainFile.format startOffset:0 secured:YES];
    [self showBarsAndResetBarTimer];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@"Zapping" eventLabel:[NSString stringWithFormat:@"channel = %@",previuseMediaItem.name]];
}

- (IBAction)swipeUpOnScreen:(UIGestureRecognizer *)sender
{
    //ASLogInfo(@"swipeUpOnScreen");

    NSInteger playedChannelIndex = [self indexOfChanel:self.purePlayerViewController.currentMediaItem];
    NSInteger nextIndex = (playedChannelIndex+1)%self.arrayAllChannels.count;
    TVMediaItem * nextMediaItem = self.arrayAllChannels[nextIndex];

    if (self.allowSwipe)
    {
        [self dismissResumeWatchView];
        [self dismissZappingCoachMarks];

        [self.purePlayerViewController playMediaItem:nextMediaItem fileFormatKey:nextMediaItem.mainFile.format startOffset:0 secured:YES];
        [self showBarsAndResetBarTimer];
        [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@"Zapping" eventLabel:[NSString stringWithFormat:@"channel = %@",nextMediaItem.name]];
    }
    else
    {
        ASLogInfo(@"not allow to swipe");
    }
}

-(void)showBarsAndResetBarTimer
{
    if (self.isChannelPanelVisible|| self.isMiniEPGVisible) {
            // bars are visible, but I do not fire a timer
            // and close them after 6 seconds.
            return;
    }
    if (self.isBarsVisible == NO)
    {
        [self showBarsCompletion:^{
            [self resetBarsTimer];
        }];
    }
    else
    {
        [self resetBarsTimer];
    }
}

-(void)showBarsAndStopBarTimer
{
    if (self.isChannelPanelVisible|| self.isMiniEPGVisible) {
        // bars are visible, but I do not fire a timer
        // and close them after 6 seconds.
        return;
    }
    
    if (self.isBarsVisible == NO)
    {
        [self showBarsCompletion:^{
            [self showForcePlyerBar];
        }];
    }
    else
    {
        [self showForcePlyerBar];
    }
}

-(void)updateOnPlayerViesAfterSwipeIfOpenWithChannel:(TVMediaItem*)channel
{
    if (self.isMiniEPGVisible)
    {
        [self.miniEPGBar willOpenMiniEpgWithChannel:channel];
    }
    if (self.isChannelPanelVisible)
    {
        [self.channelPanel updateSelectedCellToChannel:channel];
    }
}

-(NSInteger) indexOfChanel:(TVMediaItem *) requestedMediaItem
{
    for ( int i=0 ; i<self.arrayAllChannels.count ; i++ )
    {
        TVMediaItem * mediaItem = self.arrayAllChannels[i];
        if ([requestedMediaItem.mediaID isEqualToString:mediaItem.mediaID])
        {
            return i;
        }
    }
    
    return NSNotFound;
}

- (IBAction)pinchActionMinimize:(id)sender {
    UIPinchGestureRecognizer *recognizer = (UIPinchGestureRecognizer *)sender;
    
    if (self.lastPitchScale < 1 && recognizer.state ==UIGestureRecognizerStateEnded){
        [self playerBar:[self buttomPlayerBar] didSelectMinimizeVideo:nil];
    }
    
    const CGFloat kMaximunScale = 0.99999f;
    if (recognizer.scale < kMaximunScale || recognizer.scale > 1) {
        self.lastPitchScale = recognizer.scale;
    }
    recognizer.scale = 1;
}

#pragma mark - streaming changed

-(void) streamingChanged:(NSNotification *) notification
{
    [self updateUIForStreamingChanged];
    [self updateOnPlayerViesAfterSwipeIfOpenWithChannel:self.purePlayerViewController.currentMediaItem];
}

#pragma mark - out of network

-(void) areaChange:(NSNotification *) notification
{
    if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] == APSLoginStaus_OutOfNetwork)
    {
        [self showOutOfNetworkWithCompletion:nil];
    }
    else
    {
        [self hideOutOfNetworkWithCompletion:^{
           if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] == APSLoginStaus_Login)
           {
              // [self tryPlayVideoAgain];
           }

        }];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return self.showingOutOfNetworkAlert? UIStatusBarStyleLightContent:UIStatusBarStyleDefault;
}

-(void) showOutOfNetworkWithCompletion:(void(^)(void)) completion
{
    if ( self.showingOutOfNetworkAlert == NO)
    {
        [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",@"OutOfNetwork (Astonaut)"]];
            self.showingOutOfNetworkAlert = YES;
            [self showStatusBar:YES];
        self.outOfNetworkView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        self.outOfNetworkView.alpha = 0;
        self.outOfNetworkView.delegate = self;
        self.outOfNetworkView.blurView.underlyingView = self.view;
        [self.view addSubview:self.outOfNetworkView];

        [UIView animateWithDuration:0.3 animations:^{
            
            self.outOfNetworkView.alpha = 1;
            
        } completion:^(BOOL finished) {
            
            if (completion)
            {
                completion();
            }
        }];
    }

}

-(void) hideOutOfNetworkWithCompletion:(void(^)(void)) completion
{
    self.showingOutOfNetworkAlert = NO;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.outOfNetworkView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.outOfNetworkView removeFromSuperview];
        if (completion)
        {
            completion();
        }
    }];
}

#pragma mark - out of network view delegate 
-(void) fullScreenOutOfNetworkView:(APSFullScreenOutOfNetworkView *) sender dismiss:(UIButton *) buttonSender
{
    [self hideBarsCompletion:^{
    [self hideOutOfNetworkWithCompletion:^{
            [self.baseViewControllerDelegate baseViewControllerMainViewMinimizePlayer:self animated:YES];
        }];
    }];
}

#pragma mark - lockScreen 


#pragma mark - movieDidFinish notification
-(void) movieDidFinish: (NSNotification *) notification
{
    [self hideBarsCompletion:^{
        [self hideOutOfNetworkWithCompletion:^{
            [self.baseViewControllerDelegate baseViewControllerMainViewMinimizePlayer:self animated:YES];
        }];
    }];
}


#pragma mark - all channels 
- (IBAction)allChannelsButtonDidPressed:(id)sender
{
    [self showChannelPanelCompletion:nil];
}

#pragma mark - full Screen top bar delegate methods
-(UIView *) fullScreenTopBar:(APSFullScreenTopBar *) fullScreenTopBar underlyingViewForBlur:(APSBlurView *) blurView
{
    return [[[[self.purePlayerViewController.view subviews] firstObject] subviews] lastObject];
}


#pragma mark -  statusBar
- (BOOL)prefersStatusBarHidden
{
    return self.statusBarHidden && !self.showingOutOfNetworkAlert;
}

- (void)showStatusBar:(BOOL)show {
    [UIView animateWithDuration:0.3 animations:^{
        _statusBarHidden = !show;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }];
}

#pragma mark - APSMiniEPGViewController Delegate -

-(void) miniEPGViewController:(APSMiniEPGViewController *) miniEPGViewController didSelectChannel:(TVMediaItem *) channel
{
    [self.purePlayerViewController playMediaItem:channel fileFormatKey:channel.mainFile.format startOffset:0 secured:YES];
    [self toggleShowHideMiniEPGCompletion:nil];
}

#pragma mark - APSChannelPanel Delegate -

-(void) channelPanel:(APSChannelPanel *) channelPanel didSelectChannel:(TVMediaItem *) channel
{
    [self.purePlayerViewController playMediaItem:channel fileFormatKey:channel.mainFile.format startOffset:0 secured:YES];
    [self showChannelPanelCompletion:nil];
}

#pragma mark - logout -

-(void) userDidLogOut:(NSNotification *) notification
{
    [self back:nil];
}


#pragma mark - Resume Watch -
-(void) audioSessionChange:(NSNotification *) notification
{
    // test if headphone was unplugged
    // Get array of current audio outputs (there should only be one)
 
    NSArray *currentOutputs = [[AVAudioSession sharedInstance] currentRoute].outputs;
    AVAudioSessionPortDescription *currentPortDescription = [currentOutputs firstObject];
    
    AVAudioSessionRouteDescription *previousRouteDescription = [notification.userInfo objectOrNilForKey:AVAudioSessionRouteChangePreviousRouteKey];
    AVAudioSessionPortDescription *previousPortDescription = [previousRouteDescription.outputs firstObject];
    BOOL portChanged = ![previousPortDescription.portType isEqualToString:currentPortDescription.portType];
  
    if (portChanged && ![currentPortDescription.portType isEqualToString:AVAudioSessionPortHeadphones]) {
        [self displayResumeWatching:nil];
    }
}


- (IBAction)displayResumeWatching:(id)sender
{
    // pause the player
    BOOL isPlaying = self.purePlayerViewController.isInPlayingProcess;
    if (isPlaying) {
        [self.purePlayerViewController pause];
        
        self.showingResumeWatchView = YES;
        
        [self performSelectorOnMainThread:@selector(showResumeWatchingView) withObject:nil waitUntilDone:NO];
        
        
        return;
    }else{
//        NSLog(@"pause called when no playong");
        return;
    }
    
//    TVPPlaybackState status =self.purePlayerViewController.status;
//    NSLog(@"self.purePlayerViewController.status = %d",status);
//
//
//    
    // show the bars
    [self showForcePlyerBar];
    [self closeAllViewsOnPlayer];
    [self showBarsCompletion:^{
        [self showForcePlyerBar];
        [self.view addSubview: self.resumeWatchingView];
        [self.view insertSubview:self.resumeWatchingView belowSubview:self.channelPanel.view];

        self.resumeWatchingView.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
        self.resumeWatchingView.delegate = self;
        
        // disable timer - bars will stay on top.
        [self showBarsAndResetBarTimer];
        [self showForcePlyerBar];
    }];
}

- (void)showResumeWatchingView {
    
    [self showForcePlyerBar];
    [self closeAllViewsOnPlayer];
    [self showBarsCompletion:^{
        [self showForcePlyerBar];
        [self.view addSubview: self.resumeWatchingView];
        [self.view insertSubview:self.resumeWatchingView belowSubview:self.channelPanel.view];
        
        self.resumeWatchingView.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
        self.resumeWatchingView.delegate = self;
        
        // disable timer - bars will stay on top.
        [self showBarsAndResetBarTimer];
        [self showForcePlyerBar];
    }];
    
}

-(void) resumeWatch:(id) sender
{
    [self dismissResumeWatchView];
    // need to replay...
    
//    TVMediaItem * mediaItem = self.purePlayerViewController.currentMediaItem;
    //[self.purePlayerViewController playMediaItem:mediaItem fileFormatKey:mediaItem.mainFile.format startOffset:0 secured:YES];
    [self.purePlayerViewController resume];
}

-(void) dismissResumeWatchView
{
    if(self.showingResumeWatchView){
        self.showingResumeWatchView = NO;
        [self.resumeWatchingView removeFromSuperview];
        
        [self resetBarsTimer];
    }
}


#pragma mark - No Internet -

-(void)registerToNoInerenetNotifcation
{
    [self unregisterToNoInerenetNotifcation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:NotificationName_NetworkStatusChanged object:nil];
}

-(void)unregisterToNoInerenetNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationName_NetworkStatusChanged object:nil];
}

-(void) reachabilityChanged:(NSNotification *) notification
{
    APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
    if (status == TVNetworkStatus_NoConnection)
    {
        if (self.isShowNoIntenetView == NO)
        {
            [self showNoIntenetView];
        }
    }
    else
    {
        if (self.isShowNoIntenetView == YES)
        {
            [self removeNoInternetViewWithCompletion:^{
            
                [self tryPlayVideoAgain];
            
            }];
        }
    }
}

-(void) noInternetPlayerView:(APSNoInternetPlayerView *)noInternetPlayerView didSelectReload:(UIButton *)sender
{
    APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
    if (status != TVNetworkStatus_NoConnection)
    {
        [self removeNoInternetViewWithCompletion:^{
            
            [self tryPlayVideoAgain];

        }];
    }
}

-(void)showNoIntenetView
{
    [self showForcePlyerBar];
    [self closeAllViewsOnPlayer];
    [self showBarsCompletion:^{
        [self showForcePlyerBar];
    }];
    self.noInternetPlayerView.width = self.view.bounds.size.width;
    if(is_iPad())
    {
        self.noInternetPlayerView.y = self.topPlayerBar.height;
    }
    else
    {
        self.noInternetPlayerView.y = self.view.bounds.size.height - self.miniEPGBar.view.size.height- self.buttomPlayerBar.height;
    }
    self.noInternetPlayerView.alpha = 0;
    
    [self.view addSubview:self.noInternetPlayerView];

    [UIView animateWithDuration:0.3 animations:^{
        
        self.noInternetPlayerView.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
    }];
    self.isShowNoIntenetView = YES;
}

-(void)removeNoInternetViewWithCompletion:(void(^)(void)) completion
{
    self.noInternetPlayerView.alpha = 1.0;
    [UIView animateWithDuration:0.3 animations:^{
        
        self.noInternetPlayerView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [self.noInternetPlayerView removeFromSuperview];
        if (completion)
        {
            completion();
        }
    }];
    self.isShowNoIntenetView = NO;

}

#pragma mark - Try Play Video Again -

-(void)tryPlayVideoAgain
{
    if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] != APSLoginStaus_OutOfNetwork)
    {
        NSInteger playedChannelIndex = [self indexOfChanel:self.purePlayerViewController.currentMediaItem];
        NSString * format = self.purePlayerViewController.realFileFormat;
        TVMediaItem * item = [self.arrayAllChannels objectAtIndex:playedChannelIndex];
        [self.purePlayerViewController stopPlaying];
        [self playMediaItem:item fileFormatKey:format];
    }
    else
    {
        [self showOutOfNetworkWithCompletion:nil];
    }
    
    [self addPlayerAgainToScreen];
    [self showBarsAndResetBarTimer];
}

#pragma mark - Pure Player Have Error -

-(void) purePlayerHaveError:(NSNotification *) notification
{
    NSDictionary * userInfo = notification.userInfo;
    if (userInfo)
    {
        NSString * textMessage = [userInfo objectForKey:@"ErrorMessage"];
        if ([textMessage isEqualToString:@"NoInternet"])
        {
            if (self.generalPlayerErrorView.isShow == YES)
            {
                [self.generalPlayerErrorView dismiss:nil];
                self.isShowGeneralPlayerErrorView = NO;
            }
            [self reachabilityChanged:nil];
        }
        else
        {
            FinalConclusion finalConclusion = [[userInfo objectForKey:PurePlayerfinalConclusionKey] integerValue];
            NSString * textMessage = [userInfo objectForKey:PurePlayerErrorMessageKey];
            
            if (finalConclusion != FinalConclusion_InSideClientNetwork_But_SecurityIssue) // genera error || content error
            {
                if (self.generalPlayerErrorView.isShow == NO)
                {
                    [self showBarsAndStopBarTimer];
                    [self.generalPlayerErrorView purePlayerHaveError:notification];
                    self.isShowGeneralPlayerErrorView = YES;
                }
            }
            else // SecurityIssue
            {
                self.isShowGeneralPlayerErrorView = NO;
                [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",textMessage]];
                self.generalPlayerErrorView.isShow = NO;
                
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:textMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
                    [alert show];
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:textMessage preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }
        }
    }
}

#pragma mark - GeneralPlayerErrorView delegate -

-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *) generalPlayerErrorView didSelectTryPlayAgain:(UIButton *) sender
{
    ASLogInfo(@"generalPlayerErrorView -> didSelectTryPlayAgain");
     [self showBarsAndResetBarTimer];
    [self tryPlayVideoAgain];
    self.generalPlayerErrorView.alpha = 0;
    self.isShowGeneralPlayerErrorView = NO;
}

-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *)generalPlayerErrorView didDismiss:(UIButton *)sender
{
    [self showBarsAndResetBarTimer];
    [self addPlayerAgainToScreen];
    self.isShowGeneralPlayerErrorView = NO;
    ASLogInfo(@"generalPlayerErrorView -> didDismiss");
}

-(void)registerToPurePlayerHaveErrorNotificationName
{
    [self unregisterToPurePlayerHaveErrorNotificationName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(purePlayerHaveError:) name:PurePlayerHaveErrorNotificationName object:nil];
}

-(void)unregisterToPurePlayerHaveErrorNotificationName
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerHaveErrorNotificationName object:nil];
    
}

-(BOOL)isShowGeneralError
{
    return self.isShowGeneralPlayerErrorView;
}

#pragma mark - Zapping CoachMarks Delegate -

-(void) zappingCoachMarks:(APSZappingCoachMarks *) zappingCoachMarks didDismiss:(UIButton *) sender
{
    self.isShowZappingCoachMarks = NO;
    self.isInBlockMode = NO;
    [self resetBarsTimer];
}

#pragma mark - Zapping CoachMarks Action -

-(void)dismissZappingCoachMarks
{
    if (self.isShowZappingCoachMarks)
    {
        [self.zappingCoachMarks dissmis:nil];
    }
}

-(void)showIfNeededZappingCoachMarks
{
    if ([APSZappingCoachMarks needToShowView] && self.isShowZappingCoachMarks == NO)
    {
        self.isInBlockMode = YES;
        
        [self.zappingCoachMarks showViewOnSuperView:self.view withCompletion:^{
            if (self.isBarsVisible == NO)
            {
                [self showBarsCompletion:^{
                    [self showForcePlyerBar];
                }];
            }
            self.isShowZappingCoachMarks = YES;
            [self showForcePlyerBar];
            [self.view bringSubviewToFront:self.zappingCoachMarks];
        }];
    }
}

#pragma mark - Tooltip

-(void)showIfNeededInfoTooltip
{
    // first we show the "APSZappingCoachMarks"
    // next time the player is tapped we show the info tooltip
    if(![APSZappingCoachMarks needToShowView]){
        // verify we didn't show the info tooltip b4:
        BOOL shouldDisplayTooltip = ![[[NSUserDefaults standardUserDefaults] objectForKey:kInfoTooltipWasDisplayed] boolValue];
//        shouldDisplayTooltip = YES;
        if (shouldDisplayTooltip) {
            // save the flag in NSUserDefaults:
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kInfoTooltipWasDisplayed];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // show the tooltip
            self.isShowInfoTooltip = YES;

            // show the bars
            [self showForcePlyerBar];
            [self closeAllViewsOnPlayer];
            [self showBarsCompletion:^{
                [self showForcePlyerBar];

                [self showInfoTooltip];

                // disable timer - bars will stay on top.
                [self showBarsAndResetBarTimer];
                [self showForcePlyerBar];
            }];

            
            
        }
    }
}

- (void) showInfoTooltip
{
    self.tooltip = [APSTooltip toolTip];
    self.tooltip.delegate = self;
    
    self.tooltip.contentView.width = 260;
    self.tooltip.contentView.height = 60;
    
    self.tooltip.arrow.x = self.tooltip.contentView.x + self.tooltip.contentView.width - (30.0 + self.tooltip.arrow.width);
    self.tooltip.arrow.y = self.tooltip.contentView.y + self.tooltip.contentView.height - 2;

    self.tooltip.labelTooltip.text = NSLocalizedString(@"Browse channels and find more info on your programs.", nil);
    self.tooltip.labelTooltip.numberOfLines = 2;
    self.tooltip.labelTooltip.textAlignment = NSTextAlignmentLeft;
    self.tooltip.labelTooltip.x = 12;
    
    CGPoint ttLocation = CGPointMake(868,700);
    UIButton* infoButton = self.buttomPlayerBar.buttonInfo;
    if( infoButton){
        ttLocation = infoButton.center;
        ttLocation.y = self.buttomPlayerBar.y;
        ttLocation.y -= 4;
    }
    
    [self.tooltip showToolTipOnView:self.view atOrigin:ttLocation withCompletion:nil];
}

-(void) toolTip:(APSTooltip *) tooltip didDismiss:(id) sender
{
    self.tooltip.delegate = nil;
    self.tooltip = nil;
    self.isShowInfoTooltip = NO;
    [self resetBarsTimer];
}

// 
-(void)addPlayerAgainToScreen
{
    if(![self.purePlayerViewController.view isDescendantOfView:[self view]])
    {
        self.purePlayerViewController.view.transform = CGAffineTransformMakeRotation(0);
        self.purePlayerViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        [self.view insertSubview:self.purePlayerViewController.view atIndex:0];
    }
}

@end
