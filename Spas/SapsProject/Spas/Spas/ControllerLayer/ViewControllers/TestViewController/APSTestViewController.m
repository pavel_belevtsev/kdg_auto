//
//  APSTestViewController.m
//  Spas
//
//  Created by Rivka Peleg on 9/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTestViewController.h"

@interface APSTestViewController ()

@end

@implementation APSTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:-60*60]];
    
    NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:60*60]];
    
    componentsStart.minute = 00;
    componentsStart.second = 00;
    
    componentsEnd.minute = 00;
    componentsEnd.second = 00;
    
    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];
    
    NSDate * now = [NSDate date];

    /*  __weak TVPAPIRequest * request =*/[[APSEPGManager sharedEPGManager] programsByRangeForChannel:self.channel.epgChannelID
                                                                                            fromDate:startDate
                                                                                              toDate:endDate
                                                                                      actualFromDate:now
                                                                                        actualToDate:endDate
                                                                       includeProgramsDuringfromDate:YES
                                                                         includeProgramsDuringToDate:NO
                                                                                       privateContext:self.privateContext
                                                                                        starterBlock:^{
                                                                                            
                                                                                        }
                                                                                         failedBlock:^{
                                                                                             
                                                                                             NSLog(@"Failed!");
                                                                                         }
                                                                                     completionBlock:^(NSArray * programs)
                                      {
                                          
                                          
                                          NSLog(@"wow!");
                                          
                                          [self performSelector:@selector(requestSecondRange) withObject:nil];
                                          
                                          
                                      }];
    
    
    
    

    
    

    
}

-(void) requestSecondRange
{
    
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
   
    
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:-60*60]];
    
     NSDateComponents * componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:60*60*48]];
    
    componentsStart.minute = 00;
    componentsStart.second = 00;
    
    componentsEnd.minute = 00;
    componentsEnd.second = 00;
    
    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];
    
  /*  __weak TVPAPIRequest * request =*/ [[APSEPGManager sharedEPGManager] programsByRangeForChannel:self.channel.epgChannelID
                                                                                            fromDate:startDate
                                                                                              toDate:endDate
                                                                                      actualFromDate:startDate
                                                                                        actualToDate:endDate
                                                                       includeProgramsDuringfromDate:YES
                                                                         includeProgramsDuringToDate:NO
                                                                                      privateContext:self.privateContext
                                                                                        starterBlock:^{
                                                                                            
                                                                                        }
                                                                                         failedBlock:^{
                                                                                             
                                                                                             NSLog(@"Failed!");
                                                                                         }
                                                                                     completionBlock:^(NSArray * programs)
                                      {
                                          
                                          
                                          NSLog(@"wow!");
                                          
                                          
                                      }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
