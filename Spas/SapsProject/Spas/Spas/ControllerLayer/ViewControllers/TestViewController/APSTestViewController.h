//
//  APSTestViewController.h
//  Spas
//
//  Created by Rivka Peleg on 9/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseBarsViewController.h"

@interface APSTestViewController : APSBaseBarsViewController

@property (retain , nonatomic) TVMediaItem * channel;

@end
