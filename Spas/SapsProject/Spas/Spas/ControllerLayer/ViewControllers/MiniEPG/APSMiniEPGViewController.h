//
//  APSMiniEPGViewController.h
//  Spas
//
//  Created by Aviv Alluf on 8/18/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSBlurView.h"

@class APSMiniEPGViewController;

@protocol APSMiniEPGViewControllerDelegate <NSObject>

-(void) miniEPGViewController:(APSMiniEPGViewController *) miniEPGViewController didSelectChannel:(TVMediaItem *) channel;

@end


@interface APSMiniEPGViewController : APSBaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) id<APSMiniEPGViewControllerDelegate> delegate;

@property (nonatomic ,strong) TVMediaItem * currChannel;

//@property (weak, nonatomic) IBOutlet UITableView *channelTable;
@property (weak, nonatomic) IBOutlet UITableView *programsTable;

@property (weak, nonatomic) IBOutlet APSBlurView *blurView;

@property (weak, nonatomic) IBOutlet UIButton *btnUp;
@property (weak, nonatomic) IBOutlet UIButton *btnDown;
@property (assign , nonatomic) BOOL isMiniEPGVisible;

- (IBAction)channelSwitchUp:(id)sender;

- (IBAction)moveToFollowingChannel:(id)sender;

-(void)willOpenMiniEpgWithChannel:(TVMediaItem *)channel;

@end
