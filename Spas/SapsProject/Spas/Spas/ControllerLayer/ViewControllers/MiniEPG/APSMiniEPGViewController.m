//
//  APSMiniEPGViewController.m
//  Spas
//
//  Created by Aviv Alluf on 8/18/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMiniEPGViewController.h"
#import <TvinciSDK/UIImageView+AFNetworking.h>
#import "APSMiniEPGProgramsTableViewCell.h"
//#import "APSMiniEPGChannelCell.h"

@interface APSMiniEPGViewController ()<APSMiniEPGProgramsTableViewCellDelegate>
@property (strong, nonatomic) NSArray* allChannels;
@property (strong, nonatomic)  NSMutableDictionary  * dictionaryCurrentProgramByChannelID;
@property (strong, nonatomic) NSMutableDictionary * dictionaryRequest;

@property NSInteger currShowChannel;

@end

@implementation APSMiniEPGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // self.allChannels = [NSArray arrayWithArray:[[APSEPGManager sharedEPGManager] allChannels]];

}

//-(void)setAllChannels:(NSArray *)allChannels
//{
//    if (_allChannels == nil)
//    {
//         _allChannels = [NSArray arrayWithArray:[[APSEPGManager sharedEPGManager] allChannels]];
//    }
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  // [self.view setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.75]];

    [self.programsTable setBackgroundColor:[UIColor clearColor]];
    [self.blurView setLayerColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6]];
    self.blurView.blurRadius = 20;
    [self.blurView setNeedsDisplay];
    
     //self.allChannels = [NSArray arrayWithArray:[[APSEPGManager sharedEPGManager] allChannels]];
    [self willOpenMiniEpgWithChannel:self.currChannel];
}

-(NSDictionary *)dictionaryCurrentProgramByChannelID
{
    if (_dictionaryCurrentProgramByChannelID == nil)
    {
        _dictionaryCurrentProgramByChannelID = [NSMutableDictionary dictionary];
    }
    
    return _dictionaryCurrentProgramByChannelID;
}



-(void) loadProgramForChannel:(TVMediaItem *) channel
{
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:-60*60]];
    
    NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeIntervalSinceNow:60*60*24]];
    
    componentsStart.minute = 00;
    componentsStart.second = 00;
    
    componentsEnd.minute = 00;
    componentsEnd.second = 00;
    
    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];
    
    NSDate * now = [NSDate date];
    __weak TVMediaItem * weakChannel = channel;
    if (weakChannel == nil)
    {
        return;
    }
    __weak  APSMiniEPGViewController * weakSelf = self;
    APSBlockOperation * request = [[APSEPGManager sharedEPGManager] programsByRangeForChannel:channel.epgChannelID
                                                                                     fromDate:startDate
                                                                                       toDate:endDate
                                                                               actualFromDate:now
                                                                                 actualToDate:endDate
                                                                includeProgramsDuringfromDate:YES
                                                                  includeProgramsDuringToDate:NO
                                                                               privateContext:self.privateContext
                                                                                 starterBlock:^{
                                                                                     
                                                                                 }
                                                                                  failedBlock:^{
                                                                                      
                                                                                             NSLog(@"Failed!");
                                                                                         }
                                                                                     completionBlock:^(NSArray * programs)
                                                                                        {
                                                                                            
                                                                                            if (programs != nil)
                                                                                            {
                                                                                                NSArray * sortedArray =[programs sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram * mediaItem1, APSTVProgram * mediaItem2)
                                                                                                    {
                                                                                                        NSDate * prog1 =mediaItem1.startDateTime;
                                                                                                        NSDate * prog2 =mediaItem2.startDateTime;
                                                                                                        return [prog1 compare: prog2];
                                                                                                    }];
                                                                                            
                                                                                           
                                                                                                [weakSelf.dictionaryCurrentProgramByChannelID setObject:sortedArray forKey:weakChannel.epgChannelID];
                                                                                                
                                                                                                
                                                                                                for (int i=0 ; i<weakSelf.allChannels.count ;i++)
                                                                                                {
                                                                                                    TVMediaItem * channelsWithPrograms = weakSelf.allChannels[i];

                                                                                                    if ([channelsWithPrograms.epgChannelID isEqualToString:weakChannel.epgChannelID])
                                                                                                    {
                                                                                                        NSArray * indexPathes = [NSArray arrayWithObjects:[NSIndexPath indexPathForRow:i inSection:0],[NSIndexPath indexPathForRow:i+self.allChannels.count inSection:0],nil];
                                                                                                        
                                                                                                        dispatch_async(dispatch_get_main_queue(),
                                                                                                                       ^{
                                                                                                                           
                                                                                                                           [weakSelf.programsTable reloadRowsAtIndexPaths:indexPathes withRowAnimation:UITableViewRowAnimationFade];
                                                                                                                       });

                                                                                                    }
                                                                                                        
                                                                                                }
                                                        
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                [weakSelf.dictionaryCurrentProgramByChannelID setObject:[NSNull null] forKey:weakChannel.epgChannelID];
                                                                                            }

                                          
                                                                                        }];
    
    if (request != nil)
    {
        [self.dictionaryRequest setObject:request forKey:channel.epgChannelID];
    }
    
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.allChannels.count*2;
}

// setup programms cell
- (UITableViewCell *)programsTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"APSMiniEPGProgramsTableViewCell";
    APSMiniEPGProgramsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [APSMiniEPGProgramsTableViewCell miniEPGCell];
    }
    NSInteger row = indexPath.row%self.allChannels.count;

    TVMediaItem * channelMedia = nil;
    if (row < [self.allChannels count])
    {
        channelMedia = [self.allChannels objectAtIndex:row];
        [cell updateCell:channelMedia andCellIndex:row];
        cell.delegate = self;
    }

    NSArray * programs = [self.dictionaryCurrentProgramByChannelID objectForKey:channelMedia.epgChannelID];
    cell.width = self.view.width;

    if (programs == nil && [self.dictionaryRequest objectForKey:channelMedia.epgChannelID] == nil)
    {
         NSLog(@"load programs for channel : %@", channelMedia.epgChannelID);
        [cell.activityIndicator startAnimating];
        [self loadProgramForChannel:channelMedia];
        
    }
    else
    {
        [cell.activityIndicator stopAnimating];

        if ([programs isKindOfClass:[NSArray class]] && [programs count] > 0)
        {
            [cell updateCellWithChannelProgramsArray:programs];
        }
        else
        {
           // cell.currChannelProgram = nil;
        }
    }
    return cell;
}

// setup channel cell
//- (UITableViewCell *)channelTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"APSMiniEPGChannelCell";
//    APSMiniEPGChannelCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if(cell == nil)
//    {
//        cell = [APSMiniEPGChannelCell miniEPGChannelCell];;
//    }
//    
//    TVMediaItem * channelMedia = nil;
//    if (indexPath.item < [self.allChannels count])
//    {
//        channelMedia = [self.allChannels objectAtIndex:indexPath.item];
//    }
//    
//    [cell updateCell:channelMedia andCellIndex:indexPath.row];
//    
//    return cell;

//}

// setup the channel cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView == self.programsTable)
    {
        // update teh program table.
        return [self programsTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath];
    }
//    else if (tableView == self.channelTable)
//    {
//        return [self channelTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath];
//    }
    else
    {
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(is_iPad())
    {
        return 219;
    }
    else
    {
        return 228;
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TVMediaItem * channel = nil;
    if (indexPath.item < [self.allChannels count])
    {
        channel  = [self.allChannels objectAtIndex:indexPath.item];;
    }
    APSBlockOperation * request = [self.dictionaryRequest objectForKey:channel.epgChannelID];
    if (request)
    {
        [request cancel];
        if (channel)
        {
            [self.dictionaryRequest removeObjectForKey:channel.epgChannelID];
        }
    }
}



- (void)delayAutoClose
{
    
}

#pragma  mark - action

- (IBAction)channelSwitchUp:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        CGPoint contentOffset = self.programsTable.contentOffset;
        contentOffset.y-=self.programsTable.bounds.size.height;
        self.programsTable.contentOffset = contentOffset;
    }];
    
    
}

- (IBAction)moveToFollowingChannel:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        CGPoint contentOffset = self.programsTable.contentOffset;
        contentOffset.y+=self.programsTable.bounds.size.height;
        self.programsTable.contentOffset = contentOffset;
        
    }];
}


-(void)willOpenMiniEpgWithChannel:(TVMediaItem *)channel
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"Mini EPG"];

    if (self.allChannels == nil)
    {
            self.allChannels = [NSArray arrayWithArray:[[APSEPGManager sharedEPGManager] allChannels]];
    }
    // find the index og the channel
    [self.programsTable reloadData];
    self.currChannel = channel;
    NSInteger index = -1;
    for (int i=0 ; i< self.allChannels.count; i++)
    {
        TVMediaItem * channelWithPrograms = self.allChannels[i];
        if ([channelWithPrograms.epgChannelID isEqualToString:channel.epgChannelID])
        {
            index = i;
            break;
        }
    }
    
    if (index != -1)
    {
        [self updateTableviewContentOffsetToIndex:index];
    }
    self.isMiniEPGVisible = !self.isMiniEPGVisible;

}

-(void)updateTableviewContentOffsetToIndex:(NSInteger) index
{
//    CGFloat yOffset = index*self.programsTable.bounds.size.height;
//    CGPoint offset = CGPointMake(0, yOffset);
//    self.programsTable.contentOffset = offset;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.programsTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
    if (!self.isMiniEPGVisible)
    {
        return;
    }
    CGFloat currentOffsetX = scrollView_.contentOffset.x;
    CGFloat currentOffSetY = scrollView_.contentOffset.y;
    CGFloat contentHeight = scrollView_.contentSize.height;
    
    if (currentOffSetY < (contentHeight / 8.0))
    {
        scrollView_.contentOffset = CGPointMake(currentOffsetX,(currentOffSetY + (contentHeight/2)));
    }
    if (currentOffSetY > ((contentHeight * 6)/ 8.0))
    {
        scrollView_.contentOffset = CGPointMake(currentOffsetX,(currentOffSetY - (contentHeight/2)));
    }
}


#pragma mark - APSMiniEPGProgramsTableViewCellDelegate -

-(void) miniEPGProgramsTableViewCell:(APSMiniEPGProgramsTableViewCell *) miniEPGProgramsTableViewCell didSelectChannel:(TVMediaItem *) channel
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Player" eventAction:@"Play from S&S banner" eventLabel:[NSString stringWithFormat:@"channel = %@",channel.name]];
    [self.delegate miniEPGViewController:self didSelectChannel:channel];
}

-(void)dealloc
{
    self.programsTable.delegate = nil;
    self.programsTable.dataSource  = nil;
}
@end
