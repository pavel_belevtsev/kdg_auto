//
//  WebViewController.h
//  Spas
//
//  Created by Israel Berezin on 8/4/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseBarsViewController.h"
#import "APSAppDelegate.h"
@interface APSWebViewController : APSBaseBarsViewController

@property (strong, nonatomic) TVMenuItem * item;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString * pageTitle;
@property (strong, nonatomic) NSURL * webLink;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *allowGAView;
@property (weak, nonatomic) IBOutlet UISwitch *allowGASwitch;

@property (weak, nonatomic) IBOutlet UILabel *allowGAText;
@end
