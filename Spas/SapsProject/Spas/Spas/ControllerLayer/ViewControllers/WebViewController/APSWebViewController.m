//
//  WebViewController.m
//  Spas
//
//  Created by Israel Berezin on 8/4/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSWebViewController.h"
#import "APSAppDelegate.h"

@interface APSWebViewController ()<UIWebViewDelegate>

@end

@implementation APSWebViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.labelTitle.text = self.pageTitle;
    self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
    ASLogInfo(@"self.pageTitle = %@",self.pageTitle);
    
    if ([self.item.uniqueName isEqualToString:@"Data"])
    {
        self.allowGAView.hidden = NO;
        [self.allowGASwitch setOnTintColor:[APSTypography colorBrand]];//tintColor
        [self.allowGASwitch setTintColor:[APSTypography colorGrayWithValue:200]];//tintColor
        [self.allowGASwitch setOpaque:YES];
        [self.allowGAView setBackgroundColor:[APSTypography colorBackground]];
        
        self.allowGAText.text = NSLocalizedString(@"Allow sending my anonymized data for service improvements", nil);
        [self.allowGAText setFont:[APSTypography fontRegularWithSize:17]];
        [self.allowGAText setTextColor:[APSTypography colorGrayWithValue:51]];
        if (!is_iPad())
        {
            self.allowGAText.numberOfLines=2;
            [self.allowGAText setFont:[APSTypography fontRegularWithSize:14]];
            
        }
        
        self.allowGASwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"allowTracking"];
    }
    else
    {
        self.webView.y = 0;
        self.webView.height += self.allowGAView.height;
        self.allowGAView.hidden = YES;
    }

    NSURLRequest *requestObj = [NSURLRequest requestWithURL:self.webLink];

    //Load the request in the UIWebView.
    [self.webView loadRequest:requestObj];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:self.labelTitle.text];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];

}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
}

- (IBAction)allowGAChange:(id)sender
{
    UISwitch * switch1 = (UISwitch *) sender;
    ASLogInfo(@"switch1.isOn = %d",switch1.isOn);
    [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithBool:switch1.isOn] forKey:@"allowTracking"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    APSAppDelegate * app = (APSAppDelegate *)[[UIApplication sharedApplication] delegate];
    [app setupGoogleAnalytics];

}
@end
