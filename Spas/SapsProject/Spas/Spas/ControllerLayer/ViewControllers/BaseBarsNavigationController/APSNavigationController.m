//
//  APSNavigationController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSNavigationController.h"
#import "APSBaseBarsViewController.h"
#import "APSFullScreenPlayerViewController.h"

@interface APSNavigationController ()<UINavigationControllerDelegate>

@end

@implementation APSNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{vc
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    APSBaseBarsViewController * baseBarViewController = (APSBaseBarsViewController *)viewController;
    [baseBarViewController navigationController:self willShowViewController:baseBarViewController animated:animated];
}



-(void) updateTopBar:(UIView *) topView
{
    for (UIView * subview in self.topBar.subviews)
    {
        [subview removeFromSuperview];
    }
    
    self.topBar.size = topView.size;
    topView.origin = CGPointZero;
    
    [self.topBar addSubview:topView];

}

-(void) updateBottomBar:(UIView *) buttomView
{
    for (UIView * subview in self.buttomBar.subviews)
    {
        [subview removeFromSuperview];
    }
    
    self.buttomBar.size = buttomView.size;
    buttomView.origin = CGPointZero;
    [self.buttomBar addSubview:buttomView];
}




-(void) initiateViewWithButtomBar:(BOOL) isButtomBarWillbeShown topBar:(BOOL)isTopBarWillbeShown
{
    UIView * superView = self.topBar.superview;
//top view
    self.topBar.origin = CGPointMake(isTopBarWillbeShown?0:-self.topBar.height, 0);
//middle view
    self.middleView.origin = CGPointMake(0,isTopBarWillbeShown?self.topBar.height:0);
    self.middleView.height = superView.bounds.size.height-(isTopBarWillbeShown?self.topBar.height:0)-(isButtomBarWillbeShown?self.buttomBar.height:0);
//bottom view
    self.buttomBar.origin = CGPointMake(0,superView.bounds.size.height-(isButtomBarWillbeShown?self.buttomBar.height:0));
}



-(void) hideBottomBarAnimation:(BOOL) animated completion:(void(^)(void)) completion;
{
    [UIView animateWithDuration:0.3 animations:^{
        self.middleView.height+= self.buttomBar.height;
        self.buttomBar.y+=self.buttomBar.height;
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}

-(void) showBottomBarAnimation:(BOOL) animated completion:(void(^)(void)) completion
{
    
    if (self.buttomBar.y<self.buttomBar.superview.bounds.size.height && self.buttomBar.y!=0)
    {
        if (completion)
        {
            completion();
        }
        
        return;
    }
    
    self.buttomBar.y = self.buttomBar.superview.bounds.size.height;
    [UIView animateWithDuration:0.3 animations:^{
        self.middleView.height-= self.buttomBar.height;
        self.buttomBar.y-=self.buttomBar.height;
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];

}

-(void) closeTopBarAnimation:(BOOL) animated completion:(void(^)(void)) completion
{
    CGRect newMiddleFrame = self.middleView.frame;
    newMiddleFrame.origin.y-=self.topBar.height;
    newMiddleFrame.size.height+=self.topBar.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.middleView.frame = newMiddleFrame;
        self.topBar.y-=self.topBar.height;
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}



#pragma mark - orientation

-(BOOL)shouldAutorotate
{
    //    BOOL shouldAutoRotate = [[self.viewControllers lastObject] shouldAutorotate];
    BOOL shouldAutoRotate = [self.topViewController shouldAutorotate];
    return shouldAutoRotate;
}

-(NSUInteger)supportedInterfaceOrientations
{
    //    NSUInteger supOrientation = [[self.viewControllers lastObject] supportedInterfaceOrientations];
    NSUInteger supOrientation = [self.topViewController supportedInterfaceOrientations];
    if (supOrientation == 0)
    {
        if(is_iPad())
        {
            return UIInterfaceOrientationMaskLandscape;
        }
        else
        {
            return UIInterfaceOrientationMaskPortrait;
        }
    }
    
    return supOrientation;

}


//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    //    UIInterfaceOrientation prefOrientation = [[self.viewControllers lastObject]  preferredInterfaceOrientationForPresentation];
//    UIInterfaceOrientation prefOrientation = [self.topViewController preferredInterfaceOrientationForPresentation];
//    if(prefOrientation != UIInterfaceOrientationLandscapeLeft && prefOrientation != UIInterfaceOrientationLandscapeRight &&
//       prefOrientation != UIInterfaceOrientationPortrait && prefOrientation != UIInterfaceOrientationPortraitUpsideDown)
//    {
//        if (!is_iPad())
//        {
//           prefOrientation = UIInterfaceOrientationPortrait;
//        }
//        else
//        {
//            return nil;
//        }
//    }
//    return prefOrientation;
//    
//}



@end
