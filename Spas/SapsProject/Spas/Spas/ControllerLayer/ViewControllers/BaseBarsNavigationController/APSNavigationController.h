//
//  APSNavigationController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSNavigationController : UINavigationController



@property (strong, nonatomic) UIView * topBar;
@property (strong, nonatomic) UIView * buttomBar;
@property (strong, nonatomic) UIView * middleView;


@property (assign, nonatomic) BOOL topBarIsShown;
@property (assign, nonatomic) BOOL buttomBarIsShown;



-(void) updateTopBar:(UIView *) topView;
-(void) updateBottomBar:(UIView *) buttomView;



-(void) initiateViewWithButtomBar:(BOOL) isButtomBarWillbeShown topBar:(BOOL)isTopBarWillbeShown;
-(void) hideBottomBarAnimation:(BOOL) animated completion:(void(^)(void)) completion;
-(void) showBottomBarAnimation:(BOOL) animated completion:(void(^)(void)) completion;




@end
