//
//  APSSearchViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSearchViewController.h"
#import "APSProgramCollectionViewCell.h"
#import "APSProgramFullView.h"

@interface APSSearchViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,APSProgramFullViewDelegate>
@property (retain , nonatomic) NSArray * arraySearchResultPrograms;
@property (strong, nonatomic) APSProgramFullView * programFullView;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

@end

@implementation APSSearchViewController

#pragma mark - Setter -

-(void)setSearchText:(NSString *)searchText
{
    _searchText = searchText;
    
    if (searchText!= nil)
    {
        [self loadSearchResult];
    }
}

#pragma mark - Memory -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    NSLog(@"search dealloced");
}

#pragma mark - Init -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - life -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupCollectionView];
    
    self.topBar.searchField.textfieldSearch.text = self.searchText;

    self.topBar.searchField.textfieldSearch.delegate = self;
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if ([self.textFieldSearch.text length]>0)
//    {
//        self.searchField.buttonDeleteAll.hidden = NO;
//    }
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:@"Search page"];

    [self setupUI];
}

-(void)makeNewSearchWithText:(NSString*)text
{
    self.arraySearchResultPrograms = nil;
    [self.labelResultDescription setText:@""];
    self.labelResultDescription.hidden = NO;
    self.labelNoResult.hidden = YES;
    self.imageMagnifier.hidden = YES;
    
    [self.collectionViewSerachResult reloadData];
    
    self.topBar.searchField.textfieldSearch.delegate = self;
    
    self.searchText = text;
    self.topBar.searchField.textfieldSearch.text = self.searchText;
}

#pragma mark - UI -

-(void) setupUI
{

    if (is_iPad())
    {
        self.labelResultDescription.font = [APSTypography fontLightWithSize:28];
        self.labelResultDescription.textColor = [APSTypography colorDrakGray];
        self.labelNoResult.font = [APSTypography fontRegularWithSize:28];
        self.labelNoResult.textColor = [APSTypography colorLightGray];
    }
    else
    {
        self.labelResultDescription.font = [APSTypography fontLightWithSize:17];
        self.labelResultDescription.textColor = [APSTypography colorDrakGray];
        self.labelNoResult.font = [APSTypography fontRegularWithSize:19];
        self.labelNoResult.textColor = [APSTypography colorLightGray];

    }
    
    self.labelNoResult.hidden = YES;
    self.labelResultDescription.hidden = YES;
    self.imageMagnifier.hidden = YES;
}

-(void) setupCollectionView
{
    NSString * nibName = [APSProgramCollectionViewCell nibName];
    NSString * identifer = NSStringFromClass([APSProgramCollectionViewCell class]);
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewSerachResult registerNib:nib forCellWithReuseIdentifier:identifer];
}

-(void) loadSearchResult
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Search" eventAction:@" Performing a search" eventLabel:self.searchText];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForSearchEPGProgramsWithText:self.searchText pageSize:300 pageIndex:0 delegate:nil];
    
    __weak TVPAPIRequest * request = strongRequest;
    
    [request setStartedBlock:^{
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    }];
    
    [request setFailedBlock:^{
        
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];

    }];
    
    [request setCompletionBlock:^{
        
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
  
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:self.managedObjectContext];
        NSArray * programsDictionaries = [[request JSONResponse] arrayByRemovingNSNulls];
        NSMutableArray * tmpArray = [NSMutableArray array];
        NSDate * now = [NSDate date];

        for (NSDictionary * programDictionary  in programsDictionaries)
        {
            APSTVProgram * program = (APSTVProgram *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
            [program setdictionary:programDictionary];
            
            // get only programs from now.
            if([now compare:program.endDateTime] == NSOrderedAscending)
            {
                [tmpArray addObject:program];
            }
        }
        
        NSArray * sortedArray =[tmpArray sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram * mediaItem1, APSTVProgram * mediaItem2)
                                {
                                    NSDate * prog1 =mediaItem1.startDateTime;
                                    NSDate * prog2 =mediaItem2.startDateTime;
                                    return [prog1 compare: prog2];
                                }];

        self.arraySearchResultPrograms = [NSArray arrayWithArray:sortedArray];
        if (self.arraySearchResultPrograms.count > 0)
        {
            self.labelResultDescription.hidden = NO;
            self.labelNoResult.hidden = YES;
            self.imageMagnifier.hidden = YES;
            NSString * resultFor = NSLocalizedString(@"result for", nil);
            NSString * description = [NSString stringWithFormat:@"%d %@ %@%@%@",self.arraySearchResultPrograms.count,resultFor,@"\"",self.searchText,@"\""];
            self.labelResultDescription.text = description;
            
            self.labelAutomation.accessibilityValue = [NSString stringWithFormat:@"%ld Items Found", (long)self.arraySearchResultPrograms.count];

        }
        else
        {
            [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Search" eventAction:@"No results found" eventLabel:self.searchText];
            self.labelNoResult.hidden = NO;
            self.imageMagnifier.hidden = NO;
            self.labelResultDescription.hidden = YES;
            NSString * text = NSLocalizedString(@"We couldn't find any results for", nil);

            NSString * forbedan = NSLocalizedString(@"forbedan", nil);
            NSString * description = [NSString stringWithFormat:@"%@ %@%@%@ %@",text,@"\"",self.searchText,@"\"",forbedan];
            self.labelNoResult.text = description;
//            [self.labelNoResult sizeToFit];
        }
        
        [self.collectionViewSerachResult reloadData];
    }];
    
    [self sendRequest:request];
}



#pragma mark - UICollectionView delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arraySearchResultPrograms.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * className =NSStringFromClass([APSProgramCollectionViewCell class]);
    APSProgramCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:className forIndexPath:indexPath];
    
    APSTVProgram * program = [self.arraySearchResultPrograms objectAtIndex:indexPath.item];
    
    TVMediaItem * channel = [[APSEPGManager sharedEPGManager] channelFromChannelID:program.epgChannelID];
    
    [cell setChannel:channel andProgram:program];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [APSProgramCollectionViewCell cellSize];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    UIEdgeInsets edgInset = [APSProgramCollectionViewCell cellEdgeInsets];
    edgInset.top = 0;
    return edgInset;
}

#pragma mark - UICollectionViewDelegate -


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    APSProgramCollectionViewCell *theCell = (APSProgramCollectionViewCell *) [self.collectionViewSerachResult cellForItemAtIndexPath:indexPath];

    APSTVProgram * program =theCell.program; // [self.arraySearchResultPrograms objectAtIndex:indexPath.item];
    TVMediaItem * channel = theCell.channel; //[[APSEPGManager sharedEPGManager] getEPGChannelFromChannelID:program.epgChannelID];
    ASLogInfo(@"%@, %@",program,channel);
    
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    if (self.programFullView == nil || ![self.programFullView isDescendantOfView:[self view]])
    {
        self.programFullView = [APSProgramFullView programFullView];
    }
    self.programFullView.delegate = self;
    self.programFullView.baseViewControllerDelegate = self.baseViewControllerDelegate;

    [self.programFullView showProgramFullViewWithChannel:channel andProgran:program OnView:mainView];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Search" eventAction:@"Clicking on program" eventLabel:[NSString stringWithFormat:@"channel = %@ , program = %@",channel.name, program.name]];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSProgramCollectionViewCell cellEdgeInsets].top;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSProgramCollectionViewCell cellEdgeInsets].left;
}

- (IBAction)closeSearch:(id)sender
{
    //if im the owner of the top area
    if (self.topBar.searchField.textfieldSearch.delegate == self)
    {
        [self.baseViewControllerDelegate baseViewController:self backToPrevDisplayPosition:sender];
    }
}

#pragma mark - ProgramFullView Delegate -

-(void) programFullView:(APSProgramFullView *) programFullView didSelectChannel:(TVMediaItem *) channel
{

    
        [self.programFullView animateOutWithCompletion:^{
            
            [self playMediaItem:channel fileFormatKey:channel.mainFile.format];
            
        }];


}

@end
