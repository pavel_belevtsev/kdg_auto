//
//  APSSearchViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/7/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"

@interface APSSearchViewController : APSBaseBarsViewController

@property (retain, nonatomic) NSString * searchText;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewSerachResult;
@property (weak, nonatomic) IBOutlet UILabel * labelResultDescription;
@property (weak, nonatomic) IBOutlet UILabel * labelNoResult;
@property (weak, nonatomic) IBOutlet UIImageView * imageMagnifier;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView * activityIndicator;

-(void)makeNewSearchWithText:(NSString*)text;


@end
