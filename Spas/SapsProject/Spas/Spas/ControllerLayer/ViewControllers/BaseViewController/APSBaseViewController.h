//
//  APSBaseViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"APSGoogleAnalyticsManager.h"


@class APSBaseViewController;
@class APSMenuView;
@class APSBlurView;
@class APSPurePlayerViewController;

@protocol BaseViewControllerDelegate <NSObject>

-(APSPurePlayerViewController *) baseViewControllerPlayer:(APSBaseViewController *) viewcontrollers;
-(UIView *) baseViewControllerTopBar:(APSBaseViewController *) viewcontrollers;
-(UIView *) baseViewControllerButtomBar:(APSBaseViewController *) viewcontrollers;
-(void) baseViewController:(APSBaseViewController *) baseViewController backToPrevDisplayPosition:(id) sender;
-(UIView *) baseViewControllerMainView:(APSBaseViewController *) baseViewController;

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController playMediaItem:(TVMediaItem *) mediaItem fileFormatKey:(NSString *) formatKey;
-(void)  baseViewControllerMainViewMinimizePlayer:(APSBaseViewController *) baseViewController animated:(BOOL) animated;
-(void)  baseViewControllerMainViewMaximizePlayer:(APSBaseViewController *) baseViewController animated:(BOOL) animated;

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController  changeDisplayPosition:(APSBaseViewController *) viewController isRoot:(BOOL)isRoot animated:(BOOL)animated;

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController setStatusBarStyle:(UIStatusBarStyle) style;
-(UIViewController *) baseViewControllerLastViewController;
-(UIViewController *) baseViewControllerMainViewController:(APSBaseViewController *) baseViewController;

@end

@interface APSBaseViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;
@property (assign, nonatomic) id<BaseViewControllerDelegate>  baseViewControllerDelegate;
@property (strong, nonatomic) NSManagedObjectContext * privateContext;
@property UIStatusBarStyle statusBarTextColorStyle;
@property (strong, nonatomic) NSMutableArray * blockOperationArray;


-(void) sendRequest:(TVPAPIRequest *)request;


-(void) showLoginViewControllerIfNeededWithFailedBlock:(void(^)(void)) failedBlock
                                         succededBlock:(void(^)(void )) succededBlock;

-(void) playMediaItem:(TVMediaItem *) mediaItem
        fileFormatKey:(NSString *) formatKey;
-(void) stopPlaying;





@end
