//
//  APSBaseViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSBlurView.h"
#import "APSLoginViewController.h"
#import "APSPurePlayerViewController.h"
#import <TvinciSDK/APSCoreDataStore.h>
#import "GCSessionAdapter.h"
#import "TokenizationHandler.h"

@interface APSBaseViewController ()

@end

@implementation APSBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.blockOperationArray = [[NSMutableArray alloc]init];
    self.privateContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    self.privateContext.persistentStoreCoordinator = [[APSCoreDataStore defaultStore] persistentStoreCoordinator];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reauthenticationIsNeededAfterApplicationLaunched:) name:TokenizationHandlerExpiredNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TokenizationHandlerExpiredNotification object:nil];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 

- (void) reauthenticationIsNeededAfterApplicationLaunched:(NSNotification *) notification
{
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
    }];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - network
-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}


-(void) showLoginViewControllerIfNeededWithFailedBlock:(void(^)(void)) failedBlock
                                         succededBlock:(void(^)(void )) succededBlock

{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if (succededBlock)
        {
            succededBlock();
        }
    }
    else
    {
        UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
        APSLoginViewController * loginViewController = ( APSLoginViewController * )[[APSViewControllersFactory defaultFactory] loginViewController];
        loginViewController.baseViewControllerDelegate = self.baseViewControllerDelegate;
        [loginViewController
                                                        showLoginViewControllerWithCancelBlock:^{
                                                            
                                                            APSLoginViewController * loginView = loginViewController;
                                                            NSLog(@"%@",loginView);
                                                            if (failedBlock)
                                                            {
                                                                failedBlock();
                                                            }
                                                            
                                                        } failedBlock:^{
                                                            
                                                            APSLoginViewController * loginView = loginViewController;
                                                            NSLog(@"%@",loginView);

                                                            
                                                            if (failedBlock)
                                                            {
                                                                failedBlock();
                                                            }
                                                            
                                                        } succededBlock:^{
                                                            APSLoginViewController * loginView = loginViewController;

                                                            NSLog(@"%@",loginView);

                                                            if (succededBlock)
                                                            {
                                                                succededBlock();
                                                            }
                                                        } onView:mainView];
    }
}






-(void)dealloc
{
    [self.blockOperationArray removeAllObjects];
    [self.networkQueue cleen];
    self.networkQueue = nil;
}

#pragma mark - player

-(void) playMediaItem:(TVMediaItem *) mediaItem
        fileFormatKey:(NSString *) formatKey
{
    [self.baseViewControllerDelegate baseViewControllerMainView:self playMediaItem:mediaItem fileFormatKey:formatKey];
    // show bar and in the completion:
    [self.baseViewControllerDelegate baseViewControllerMainViewMaximizePlayer:self animated:YES];

}

-(void) stopPlaying
{
    //remove banner and stop playing
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    [purePlayer stopPlaying];
}




@end
