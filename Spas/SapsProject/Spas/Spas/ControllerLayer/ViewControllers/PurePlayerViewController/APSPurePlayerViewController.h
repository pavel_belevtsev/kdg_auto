//
//  APSPurePlayerViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"

#if !TARGET_IPHONE_SIMULATOR
#import <TVCPlayerWV/TVCPlayerFactory.h>
#import <TVCPlayerWV/TVMediaToPlayInfo.h>
#import <TVCPlayerWV/TVCplayer.h>
#endif



//This Notification is sent because also because of program changed
#define PurePlayerProgramOfCurrentStreamingAvailableNotificationName @"PurePlayerProgramOfCurrentStreamingAvailableNotificationName"
#define PurePlayerStreamingChangedNotificationName @"PurePlayerStreamingChangedNotificationName"
#define PurePlayerVolumeChangedNotificationName @"PurePlayerVolumeChangedNotificationName"
#define PurePlayerAreaChangedNotificationName @"PurePlayerAreaChangedNotificationName"
#define PurePlayerDidFinishPlayingNotificationName @"PurePlayerDidFinishPlayingNotificationName"
#define PurePlayerHaveErrorNotificationName @"PurePlayerHaveErrorNotificationName"
//@"ErrorMessage" @"NoInternet" finalConclusion
#define PurePlayerErrorMessageKey @"ErrorMessage"
#define PurePlayerNoInternetValue @"NoInternet"
#define PurePlayerfinalConclusionKey @"finalConclusion"
#define PurePlayerDismissResumeWatchViewNotificationName @"PurePlayerDismissResumeWatchViewNotificationName"

#if !TARGET_IPHONE_SIMULATOR
@interface APSPurePlayerViewController : APSBaseViewController<TVCplayerStatusProtocol>
#else
@interface APSPurePlayerViewController : APSBaseViewController
#endif


@property (assign, nonatomic) BOOL isInPlayingProcess;
@property (assign, nonatomic) BOOL isInInsertionProcess;
@property (assign, nonatomic) BOOL isInFullScreen;
@property (assign, nonatomic) BOOL isOutOfNetwork;

#if !TARGET_IPHONE_SIMULATOR
@property (retain, nonatomic) TVMediaToPlayInfo * currentMediaToPlayInfo;
#endif

@property (retain, nonatomic) TVMediaItem * currentMediaItem;
@property (retain, nonatomic) APSTVProgram * currentProgram;

@property (strong, nonatomic) NSString * realFileFormat; // keep orginal url type (in play fllow it change to "test")

@property (strong, nonatomic) NSURL *CDNProcessURL;

@property (nonatomic, weak) IBOutlet UILabel *labelAutomation;

-(void) playMediaItem:(TVMediaItem *) mediaItem
        fileFormatKey:(NSString *) formatKey
          startOffset:(float) startOffset
              secured:(BOOL) secured;


-(void)playMediaAgain;
-(void)stopPlayingAfterOutOfNetwork;
-(void)playMediaAfterIntsetToNetwork;

-(void)resume;
//-(void) play;
-(void) stopPlaying;
-(void) pause;
-(void) setVolume:(CGFloat) volumeValue;
-(CGFloat) volume;
#if !TARGET_IPHONE_SIMULATOR
-(TVPPlaybackState) status;
#endif

//return value from 0-1
-(CGFloat) currentPlayBackTimeRealtiveToDuration;
-(NSString *) startTimeText;
-(NSString *) endTimeText;



@end

//#endif
