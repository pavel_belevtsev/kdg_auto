//
//  APSPurePlayerViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

//#if TARGET_IPHONE_SIMULATOR



#import "APSPurePlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CDNHandler.h"
#import "APSEpgMetaDataView.h"
#import "APSNetworkManager.h"
#import "APSIneoQuestHandler.h"
#import "APSConfigurationManager.h"

#define DefualtTimeToCheckNextProgarm 5*60

#define AVSystemController_SystemVolumeDidChangeNotification @"AVSystemController_SystemVolumeDidChangeNotification"

@interface APSPurePlayerViewController ()
@property (retain, nonatomic) NSTimer * timerOfEndProgram;
#if !TARGET_IPHONE_SIMULATOR
@property (retain, nonatomic) TVCplayer * player;
#endif

@property (retain, nonatomic) CDNHandler * cdnHandler;
@property (retain, nonatomic) NSString * currentOriginalFormat;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView * activityIndicator;

@property (assign, nonatomic) BOOL handelingError;
@property (strong, nonatomic) APSIneoQuestHandler * ineoQuestHandler;
@property BOOL appHaveineoQuest;

@property MPMoviePlaybackState lastPlayerState;
@end

@implementation APSPurePlayerViewController

#pragma mark - Publick Mthods

-(NSString *) startTimeText
{
    APSTVProgram * program = [self currentProgram];
    
    if (program == nil)
    {
         return @"00:00";
    }
    else
    {
        NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:program.startDateTime];
        NSInteger hourStart = [componentsStart hour];
        NSInteger minuteStart = [componentsStart minute];
        
        return [NSString stringWithFormat:@"%02d:%02d",hourStart,minuteStart];
    }
}

-(NSString *) endTimeText
{
    APSTVProgram * program = [self currentProgram];

    if (program == nil)
    {
        return @"00:00";
    }
    else
    {
    
    NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:program.endDateTime];
    NSInteger hourStart = [componentsStart hour];
    NSInteger minuteStart = [componentsStart minute];
    
    return [NSString stringWithFormat:@"%02d:%02d",hourStart,minuteStart];
    }
}


-(CGFloat) currentPlayBackTimeRealtiveToDuration
{
//    if ([self.currentMediaToPlayInfo.mediaItem isLive])
//    {
        NSDate * now = [NSDate date];
       CGFloat timeFruction = [now timeIntervalSinceDate:self.currentProgram.startDateTime]/[self.currentProgram.endDateTime timeIntervalSinceDate:self.currentProgram.startDateTime];
        return timeFruction;
//    }
//    return  [self.player getCurrentPlaybackTime];
}

-(void) stopPlaying
{
#if !TARGET_IPHONE_SIMULATOR
    [self.player Stop];
    [self.player cleanPlayer];
    self.currentMediaToPlayInfo = nil;

    
#endif
    self.currentMediaItem = nil;
    self.currentProgram = nil;
    self.isInPlayingProcess = NO;
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler stopIneoQuestPressed];
    }
}

-(void) pause
{
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler stopIneoQuestPressed];
    }
#if !TARGET_IPHONE_SIMULATOR
    [self.player Pause];
#endif
    //    self.currentMediaItem = nil;
    //    self.currentProgram = nil;
    //    self.isInPlayingProcess = NO;
}

#if !TARGET_IPHONE_SIMULATOR
-(TVPPlaybackState) status
{
    return [self.player playerStatus];
}
#endif

#pragma mark - End Publick Mthods

-(void)dealloc
{
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler stopIneoQuestPressed];
    }
    NSString * properyName = NSStringFromProperty(frame);
    [self.view removeObserver:self forKeyPath:properyName];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.appHaveineoQuest = [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] appHaveineoQuest];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
#if !TARGET_IPHONE_SIMULATOR
    [self playerInitialization];
    //self.appHaveineoQuest = NO;
    if (self.appHaveineoQuest)
    {
        self.ineoQuestHandler = [[APSIneoQuestHandler alloc] initWithPlayer:self.player];
    }

#endif
    [self absorvingFrameChange];
    self.view.clipsToBounds = YES;
    self.view.backgroundColor = [UIColor blackColor];
    [self registerForNotification];
    
    self.labelAutomation.isAccessibilityElement = YES;
    self.labelAutomation.accessibilityLabel = @"Label Automation";
    self.labelAutomation.accessibilityValue = @"";
    
}

-(void) registerForNotification
{
    [self unregisterForNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector( volumeChange:) name:AVSystemController_SystemVolumeDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector( didBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground:)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
}

-(void) unregisterForNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVSystemController_SystemVolumeDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];

}

-(void) absorvingFrameChange
{
    NSString * properyName = NSStringFromProperty(frame);
    [self.view addObserver:self forKeyPath:properyName options:NSKeyValueObservingOptionNew context:nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    #if !TARGET_IPHONE_SIMULATOR
    [self.player setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.player.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    ASLogInfo(@"absorvingFrameChange = %@",self.player.view);

#endif
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resume
{
#if !TARGET_IPHONE_SIMULATOR
    [self.player Resume];
#endif
}

#if !TARGET_IPHONE_SIMULATOR
-(void) playerInitialization
{
    self.view.backgroundColor = [UIColor blackColor];
    self.player = [[TVCPlayerFactory playerFactory] GetPlayerForMediaItem:nil];
    [self.player setDelegate:self];
    [self.player setFrame:CGRectMake(0, 0, self.view.bounds.size.width ,  self.view.bounds.size.height)];
    self.player.view.backgroundColor =  [UIColor blackColor];
    self.player.view.userInteractionEnabled = NO;
    self.player.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.player.timeOutSeconds = 4;
    
    [self.view insertSubview:self.player.view atIndex:0];
    
}
#endif

-(void) playMediaItem:(TVMediaItem *) mediaItem
        fileFormatKey:(NSString *) formatKey
          startOffset:(float) startOffset
              secured:(BOOL) secured
{
 
    
#if !TARGET_IPHONE_SIMULATOR
    [self.player Stop];
    //[self.player StopAndDestroy];
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler stopIneoQuestPressed];
    }

#endif
    self.currentProgram = nil;
    [self.activityIndicator startAnimating];
    self.isOutOfNetwork = NO;
    self.isInPlayingProcess = YES;
#if !TARGET_IPHONE_SIMULATOR
    TVMediaToPlayInfo * mediaToPlayInfo = [[TVMediaToPlayInfo alloc] initWithMediaItem:mediaItem];
    mediaToPlayInfo.startTime = startOffset;
    mediaToPlayInfo.fileTypeFormatKey = formatKey;
    mediaToPlayInfo.useSignedUrl = NO;
    self.realFileFormat =formatKey;
    [self updateCurrentMediaToPlay:mediaToPlayInfo];
    [self playMediaAfterCDNProcessWithMediaToPlay:mediaToPlayInfo isDuringPlaying:NO performPlayIfNeeded:YES];
#endif
    [self updateCurrentMediaItem:mediaItem];
}

-(void)playMediaAgain
{
#if !TARGET_IPHONE_SIMULATOR
     [self playMediaAfterCDNProcessWithMediaToPlay:self.currentMediaToPlayInfo isDuringPlaying:NO performPlayIfNeeded:YES];;
#endif
}

-(void)stopPlayingAfterOutOfNetwork
{
#if !TARGET_IPHONE_SIMULATOR
    [self.player Stop];
#endif
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler stopIneoQuestPressed];
    }
}

-(void)playMediaAfterIntsetToNetwork
{
    if (self.appHaveineoQuest)
    {
        [self.ineoQuestHandler startIneoQuestPressed];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerDismissResumeWatchViewNotificationName object:nil];
#if !TARGET_IPHONE_SIMULATOR
    [self.player Play];
#endif
}

#if !TARGET_IPHONE_SIMULATOR
-(void) playMediaAfterCDNProcessWithMediaToPlay:(TVMediaToPlayInfo *) mediaToPlayInfo isDuringPlaying:(BOOL) duringPlaying performPlayIfNeeded:(BOOL) performPlay
{
    
    self.isOutOfNetwork = NO;
    
    [self.cdnHandler stopProcess];
    
    NSURL * fileURL = [mediaToPlayInfo.currentFile fileURL];
    NSInteger fileID = [[mediaToPlayInfo.currentFile fileID] integerValue];
    
    TVFile  * file = [[TVFile alloc] init];
    file.fileID = [NSString stringWithFormat:@"%d",fileID];
    file.fileURL = fileURL;
    
    self.CDNProcessURL = fileURL;
    
    //NSLog(@"++++ CDNStart");
    
    void (^CDNCompletion)(NSURL *url, FinalConclusion finalConclusion) = ^(NSURL *url, FinalConclusion finalConclusion) {
        
        NSString * textMessage =  nil;
        //NSLog(@"++++ CDNCompletion");
        switch (finalConclusion)
        {
            case FinalConclusion_InSideClientNetwork_contenent_available:
            {
                 //NSLog(@"FinalConclusion_InSideClientNetwork_contenent_available");
                if (performPlay && ( !duringPlaying ||(duringPlaying && self.player.playerStatus != TVPPlaybackStateIsPlaying) || (duringPlaying && ((self.player.movieLoadStatus & TVPMovieLoadStateStalled) == TVPMovieLoadStateStalled))))
                {
                    self.currentOriginalFormat = mediaToPlayInfo.currentFile.format;
                    NSURL * playUrl = url;
                    //NSLog(@"playUrl before ineoQuest= %@",playUrl);

                    if (self.appHaveineoQuest)
                    {
                        //NSLog(@"appHaveineoQuest");

                        playUrl = [self.ineoQuestHandler ineoQuestURLToPlay:url];
                    }
                    //NSLog(@"playUrl after ineoQuest = %@",playUrl);

                    [mediaToPlayInfo addPLTVFileWithFormat:@"test" andUrlString:[playUrl absoluteString] andBaseFile:file];
                    mediaToPlayInfo.fileTypeFormatKey = @"test";
                    [self.activityIndicator stopAnimating];
                    [self.player playMedia:mediaToPlayInfo];

                }
             }
                break;
            case FinalConclusion_OutSideClientNetwork:
            {
//                self.isOutOfNetwork = YES;
//                [self.player Stop];
            }
                break;
            case  FinalConclusion_InSideClientNetwork_But_SecurityIssue:
                textMessage = NSLocalizedString(@"user is not entitled to the service, inside KDG network", nil);
                break;
            case FinalConclusion_ContentIssue:
                textMessage = NSLocalizedString(@"Credentials are valid, content is unavailable", nil);
                break;
            default:
                textMessage = NSLocalizedString(@"System error", nil);
                break;
        }
        
        [self.activityIndicator stopAnimating];

        APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
        if (status == TVNetworkStatus_NoConnection)
        {
             textMessage = PurePlayerNoInternetValue;
        }
        if (textMessage != nil)
        {
            if (self.appHaveineoQuest)
            {
                [self.ineoQuestHandler stopIneoQuestPressed];
            }
            [self.player Stop];
            [self.activityIndicator stopAnimating];
            
            if (finalConclusion != FinalConclusion_NoInternetError)
            {
                
                //NSLog(@"%@ %@", [self.CDNProcessURL absoluteString], [url absoluteString]);
                
                if (self.CDNProcessURL && url && [[self.CDNProcessURL absoluteString] isEqualToString:[url absoluteString]]) {
                    
                    NSArray * keys = [NSArray arrayWithObjects:PurePlayerErrorMessageKey,PurePlayerfinalConclusionKey, nil];
                    NSArray * values = [NSArray arrayWithObjects:textMessage,[NSNumber numberWithInteger:finalConclusion], nil];
                    NSDictionary * userInfo = [NSDictionary dictionaryWithObjects:values forKeys:keys];
                    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerHaveErrorNotificationName object:self userInfo:userInfo];

                }
                
            }
        }
        
        self.handelingError = NO;
        
    };
    
    if (duringPlaying)
    {
        [self.cdnHandler handleErrorDuringPlayingOfURL:fileURL startedBlock:nil completionBlock:CDNCompletion];
    }
    else
    {
        [self.cdnHandler startCDNProcessWithFileURL:fileURL fileID:fileID startedBlock:nil completionBlock:CDNCompletion];
    }
    
    
    
}
#endif





#if !TARGET_IPHONE_SIMULATOR
-(void) updateCurrentMediaToPlay:(TVMediaToPlayInfo *) mediaItemToPlay
{
    self.currentMediaToPlayInfo = mediaItemToPlay;
    
}
#endif

-(void) updateCurrentMediaItem:(TVMediaItem *) mediaItem
{
    self.currentMediaItem = mediaItem;
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerStreamingChangedNotificationName object:nil];
    [self findCurrnetProgramAndScheduleTimer];
}



#pragma mark - setters getters



- (CDNHandler *)cdnHandler
{
    if (_cdnHandler ==nil)
    {
        _cdnHandler = [[CDNHandler alloc] init];
    }
    
    return _cdnHandler;
}

-(void) setIsOutOfNetwork:(BOOL)isOutOfNetwork
{
    if (_isOutOfNetwork!= isOutOfNetwork)
    {
        _isOutOfNetwork = isOutOfNetwork;
        //[[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerAreaChangedNotificationName object:nil];
    }
}
#pragma mark - TVCPlayerStatus Protocol

#if !TARGET_IPHONE_SIMULATOR

/*   Mandatory for using the player, here probably you will send Play message to the Player after switchToMediaItem.   */
- (void)movieShouldStartPlayingWithMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player
{
    self.labelAutomation.accessibilityValue = @"Movie Started";
}

/*   Triggered whenever something got wrong in the pre-processing of the media.   */
- (void)movieProcessFailedWithStatusError:(TVCDrmStatus)status withMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player
{
    NSString * textMessage =  nil;
    switch (status)
    {
        case TVCDrmStatusPersonalization_Failed:
        case TVCDrmStatusDownloadContent_Failed:
        case TVCDrmStatusIsDRM_Failed:
        case TVCDrmStatusAquireRights_Failed:
        case TVCDrmStatus_Interrupted:
        case TVCDrmStatus_IncorrectMediaItemOrNull:
        case TVCDrmStatus_IncorrectLicensedURL:
        case TVCDrmStatus_UNKNOWN:
            textMessage  = NSLocalizedString(@"General player error", nil);
            break;
        case TVCDrmStatus_IncorrectFileUrl:
            textMessage = NSLocalizedString(@"URL is Not Valid",nil);
        default:
            break;
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:textMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error",nil) message:textMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

/*  Media has reached to end    */
- (void)movieFinishedPresentationWithMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player
{
    [self.player Stop];
    self.isInPlayingProcess = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerDidFinishPlayingNotificationName object:nil userInfo:nil];
    //    close player and go back!
    // go back
}

/*  Movie player is buffering stream    */
- (void)movieIsBufferingForMediaItem:(TVMediaItem*)mediaItem
{
    [self.activityIndicator startAnimating];
}

/*  Movie player has enough buffer for playback    */
- (void)movieHasFinishedBufferingForMediaItem:(TVMediaItem*)mediaItem
{
    [self.activityIndicator stopAnimating];
}

/*  Bitrate Changed    */
- (void)moviePlaybackBitrateChangedFor:(TVMediaItem*)mediaItem withNewBitrate:(int)bitrate outOfBitratesArr:(NSArray*)bitratesArr
{
    
}


/*  Monitors playback every one second */
- (void)monitorPlaybackTimeAtPlayer:(TVCplayer*)player
{
    
}

/*   Mandatory for using the player, here probably you will send Play message to the Player after switchToMediaItem.   */
- (void)playerDetectedConcurrentWithMediaItem:(TVMediaItem*)mediaItem atPlayer:(TVCplayer*)player
{
    
}


-(void) player:(TVCplayer *) player didDetectError:(NSDictionary*) info
{
    
    //Playlist File not received
    if ([[[[[info objectForKey:@"errorLog"] events] lastObject] errorDomain] isEqualToString:@"CoreMediaErrorDomain"] && [[[[info objectForKey:@"errorLog"] events] lastObject] errorComment] == nil  )
    {
        return;
    }
    
    
    NSString * errorComment = [[[[info objectForKey:@"errorLog"] events] lastObject] errorComment];
    
    if (errorComment == nil)
    {
        return;
    }
    
    NSLog(@"errorComment = %@",errorComment);
    NSError * error;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"[0-9][0-9][0-9]"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    
    NSArray *matches = [regex matchesInString:errorComment
                                      options:0
                                        range:NSMakeRange(0, [errorComment length])];
    
    NSLog(@"error numbers matches = %@",matches);
    NSMutableArray * bugNumbers = [NSMutableArray array];
    for (NSTextCheckingResult *match in matches)
    {
        NSString* substring =  [errorComment substringWithRange:match.range];
        [bugNumbers addObject:substring];
        NSLog(@"singel error is: %@",substring);
    }

    if (self.handelingError == NO)
    {
        for (NSString * errorNum in bugNumbers)
        {
            NSInteger  number = [errorNum integerValue];
            if (number >= 400 && number < 600)
            {
                self.handelingError = YES;
                [self playMediaAfterCDNProcessWithMediaToPlay:self.currentMediaToPlayInfo isDuringPlaying:YES performPlayIfNeeded:YES];
                //[self playMediaItem:self.currentMediaItem fileFormatKey:self.currentOriginalFormat startOffset:0 secured:NO];
            }
        }
    }
}

-(void) playerDetectTimeOut:(TVCplayer *) player
{
    if (self.handelingError == NO)
    {
        self.handelingError = YES;
        [self playMediaAfterCDNProcessWithMediaToPlay:self.currentMediaToPlayInfo isDuringPlaying:YES performPlayIfNeeded:YES];
    }

}


#endif

#pragma mark - volume

-(void) setVolume:(CGFloat) volumeValue
{
    [[MPMusicPlayerController applicationMusicPlayer] setVolume:volumeValue];
}

-(CGFloat) volume
{
    return [[MPMusicPlayerController applicationMusicPlayer] volume];
}

-(void) volumeChange:(NSNotification *) notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerVolumeChangedNotificationName object:nil];
}

#pragma mark - didBecomeActive -


-(void) handleEnteredBackground:(NSNotification *) notification
{
    ASLogInfo(@"appWillResignActive");
//    if (self.isInPlayingProcess)
//    {
////        [self.player Pause];
//       // [self.ineoQuestHandler stopIneoQuestPressed];
//    }
}

-(void) didBecomeActive:(NSNotification *) notification
{
    ASLogInfo(@"appWillResignActive");
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerDismissResumeWatchViewNotificationName object:nil];
    
//    if (self.isInPlayingProcess)
//    {
//        if (self.appHaveineoQuest)
//        {
////            [self.player Play];
//
//           // [self playMediaAfterCDNProcessWithMediaToPlay:self.currentMediaToPlayInfo isDuringPlaying:NO performPlayIfNeeded:YES];
//        }
//    }
}

#pragma mark - EPG -

/**
 *  find current program and schedule time to find the next program when the current program is ended
 */


-(void) findCurrnetProgramAndScheduleTimer
{
    [self.timerOfEndProgram invalidate];
    self.timerOfEndProgram = nil;
    
    [[APSEPGManager sharedEPGManager] programForChannelId:self.currentMediaItem.epgChannelID date:[NSDate date] privateContext:self.privateContext starterBlock:^{
        
    } failedBlock:^{
        
    } completionBlock:^(APSTVProgram *program) {
        self.currentProgram = program;
       [self schedualForNextProgram];
    }];
}



-(void)schedualForNextProgram
{
    NSTimeInterval timeRemainToEndOfProgram = DefualtTimeToCheckNextProgarm;
    if (self.currentProgram != nil)
    {
        NSDate * now = [NSDate date];
        timeRemainToEndOfProgram = [self.currentProgram.endDateTime timeIntervalSinceDate:now];
    }
    //NSLog(@"next time program = %f",timeRemainToEndOfProgram);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PurePlayerProgramOfCurrentStreamingAvailableNotificationName object:nil];
    
    self.timerOfEndProgram = [NSTimer scheduledTimerWithTimeInterval:timeRemainToEndOfProgram target:self selector:@selector(findCurrnetProgramAndScheduleTimer) userInfo:nil repeats:NO];
}

@end


//#endif


// test for commit maybe uncommited changes
