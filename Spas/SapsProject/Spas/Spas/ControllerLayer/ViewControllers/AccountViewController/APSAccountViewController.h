//
//  APSAccountViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseBarsViewController.h"


@interface APSAccountViewController : APSBaseBarsViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSString * pageTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelIPadAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelIPadDetails;

@end
