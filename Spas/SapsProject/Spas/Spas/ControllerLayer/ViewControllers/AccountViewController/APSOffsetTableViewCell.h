//
//  APSOffsetTableViewCell.h
//  Spas
//
//  Created by Aviv Alluf on 8/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSAccountBaseCell.h"

// APSOffsetTableViewCell only sets the offset of the labels: text & detail to 25 pix
@interface APSOffsetTableViewCell : APSAccountBaseCell

@property CGFloat labelsOffset;
@property CGFloat distanceBetweenLabels;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andOffset:(CGFloat) offset;

@end
