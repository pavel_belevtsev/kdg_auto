//
//  APSAccountViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSAccountViewController.h"
#import "APSOffsetTableViewCell.h"
#import "APSAccountBaseCell.h"
@interface APSAccountViewController ()
@property NSArray* keys;
@property NSArray* values;
@property (nonatomic, strong) UIView* iPhoneHeaderView;
@end

@implementation APSAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.pageTitle = [self.pageTitle uppercaseString];
    self.labelTitle.text = self.pageTitle;
    self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
    [self setupTableData];
    if (is_iPad())
    {
        [self setupIPadControls];
    }
    else
    {
        [self setupIPhoneHeader];
    }
    self.tableView.userInteractionEnabled = YES;
//    [self.tableView setAllowsSelection:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:self.labelTitle.text];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setups -

- (void) setupTableData
{
    self.keys = @[
                  NSLocalizedString(@"KDG ID", nil) ,
                  NSLocalizedString(@"Device Identifier", nil) ,
                  NSLocalizedString(@"App Version", nil) ,
                  NSLocalizedString(@"OS Version", nil) ,
                      ];

    TVUser * user = [[TVSessionManager sharedTVSessionManager] currentUser];
    NSString * userName =  [NSString stringWithFormat:@"%@",[user.basicData objectForKey:@"m_sUserName"]];
    if (userName == nil || [userName isEqualToString:@"(null)"])
    {
        userName = @"";
    }
    NSString * appId = [TVConfigurationManager getTVUDID];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"ExternalVersion"]; // example: 1.0.0
    NSString *OSVersion = [[UIDevice currentDevice]systemVersion];

    self.values = @[
                      userName,
                      appId,
                      appVersion,
                      OSVersion
                      ];


}

- (void) setupIPadControls
{
    // ACCOUNT label
    self.labelIPadAccount.text = NSLocalizedString(@"Account", nil);
    self.labelIPadAccount.font = [APSTypography fontLightWithSize:28];
    self.labelIPadAccount.textColor = [APSTypography colorDrakGray];
    [self.labelIPadAccount sizeToFit];
    // PERSONAL DETAILS label
    self.labelIPadDetails.text = [NSLocalizedString(@"PERSONAL DETAILS", nil) uppercaseString];
    self.labelIPadDetails.font = [APSTypography fontMediumWithSize:15];
    self.labelIPadDetails.textColor = [APSTypography colorLightGray];
    [self.labelIPadDetails sizeToFit];
    // TableView - round corners
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.layer.cornerRadius = 4.0;
    self.tableView.layer.borderWidth = 1.0f;
    self.tableView.layer.borderColor = [UIColor clearColor].CGColor;
}

-(void) setupIPhoneHeader
{
    // On iPhone we set the header to display: "PERSONAL DETAILS"
    UILabel* label = [[UILabel alloc] init];
    label.frame = CGRectMake(25, 18, 300, 20);
    label.font = [APSTypography fontLightWithSize:15];
    label.text = NSLocalizedString(@"PERSONAL DETAILS", nil);
    label.textColor = [APSTypography colorLightGray];
    
    self.iPhoneHeaderView = [[UIView alloc] init];
    [self.iPhoneHeaderView addSubview:label];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate -

// On iPhone I use the table header for "PERSONAL DETAILS", on iPad it is displayed in a label
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return is_iPad() ? 0:48;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (is_iPad()) {
        return nil;;
    }

    return self.iPhoneHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

#define cellIdentifer @"APSAccountCell"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (is_iPad())
    {
        return [self setUpIPadCell:tableView  cellForRowAtIndexPath:indexPath];
    }
    else
    {
        return [self setUpIPhoneCell:tableView  cellForRowAtIndexPath:indexPath];
    }
}

- (UITableViewCell *)setUpIPadCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    APSAccountBaseCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer];
    if (cell == nil)
    {
        cell = [[APSAccountBaseCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifer];
        
    }
    
    cell.textLabel.font = [APSTypography fontRegularWithSize:17];
    cell.textLabel.textColor = [APSTypography colorDrakGray];
    cell.textLabel.text = [self.keys objectAtIndex:indexPath.row];
    
    cell.detailTextLabel.font = [APSTypography fontRegularWithSize:17];
    cell.detailTextLabel.textColor = [APSTypography colorLightGray];
    cell.detailTextLabel.text = [self.values objectAtIndex:indexPath.row];
    
    cell.userInteractionEnabled = YES;
    return cell;
}

- (UITableViewCell *)setUpIPhoneCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    APSOffsetTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer];
    if (cell == nil)
    {
        cell = [[APSOffsetTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifer andOffset:25];
        cell.distanceBetweenLabels = 10;
        
    }
    
    cell.textLabel.font = [APSTypography fontRegularWithSize:16];
    cell.textLabel.textColor = [APSTypography colorDrakGray];
    cell.textLabel.text = [self.keys objectAtIndex:indexPath.row];
    
    BOOL shouldUseSmallerFont = (indexPath.row == 1);
    
    cell.detailTextLabel.font = [APSTypography fontRegularWithSize:shouldUseSmallerFont? 14:16];
    cell.detailTextLabel.textColor = [APSTypography colorLightGray];
    cell.detailTextLabel.text = [self.values objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return is_iPad() ? 44:67;
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}


@end
