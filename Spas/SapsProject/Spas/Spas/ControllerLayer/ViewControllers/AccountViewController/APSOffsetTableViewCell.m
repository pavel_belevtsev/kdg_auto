//
//  APSOffsetTableViewCell.m
//  Spas
//
//  Created by Aviv Alluf on 8/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSOffsetTableViewCell.h"

@implementation APSOffsetTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andOffset:(CGFloat)offset{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _labelsOffset = offset;
        _distanceBetweenLabels = 0;
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}


- (void) layoutSubviews {
    // Let the super class UITableViewCell do whatever layout it wants.
    // Just don't use the built-in textLabel or detailTextLabel properties
    [super layoutSubviews];
    
    CGFloat newYLocationForText = 0;
    CGFloat newYLocationForDetails = 0;
    
    if(self.labelsOffset){
        if (_distanceBetweenLabels!=0) {
            // calc the Y location of the labels:
            CGFloat cellHeight = self.frame.size.height;
            CGFloat textHeight = self.textLabel.frame.size.height;
            CGFloat detailsHeight = self.detailTextLabel.frame.size.height;
            
            newYLocationForText = (cellHeight - textHeight - detailsHeight - _distanceBetweenLabels)/2;
            newYLocationForDetails = newYLocationForText + textHeight + _distanceBetweenLabels;
        }
        
        // Position the labels 25px in...
        CGRect newFrame = self.textLabel.frame;
        newFrame.origin.x = _labelsOffset;
        if (_distanceBetweenLabels!=0) {
            newFrame.origin.y = newYLocationForText;
        }
        self.textLabel.frame = newFrame;

        newFrame = self.detailTextLabel.frame;
        newFrame.origin.x = _labelsOffset;
        if (_distanceBetweenLabels!=0) {
            newFrame.origin.y = newYLocationForDetails;
        }
        self.detailTextLabel.frame = newFrame;
    }
}



@end
