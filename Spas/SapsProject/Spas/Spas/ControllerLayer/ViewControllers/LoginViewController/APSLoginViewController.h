//
//  APSLoginViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSButtonLink.h"
#import "APSLoginWebView.h"
#import "APSWelcomeView.h"

@interface APSLoginViewController : APSBaseViewController

@property (weak, nonatomic) IBOutlet UILabel *labelCompanyName;

@property (weak, nonatomic) IBOutlet UILabel *labelCompanyDescription;

@property (weak, nonatomic) IBOutlet APSTextField *textFieldUsername;

@property (weak, nonatomic) IBOutlet APSTextField *textfieldPassword;

@property (weak, nonatomic) IBOutlet APSButtonLink *buttonForgotPassword;

@property (weak, nonatomic) IBOutlet APSButtonLink *buttonNeedHelp;

@property (weak, nonatomic) IBOutlet APSButtonLink *buttonSkipForNow;

@property (weak, nonatomic) IBOutlet UILabel *lblError;

@property (weak, nonatomic) IBOutlet APSBrandedButton *buttonLogin;

@property (strong, nonatomic) IBOutlet APSBlurView *blurView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet UIView *animateView;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loginActivityIndicator;

@property (strong , nonatomic) APSLoginWebView * loginWebView;

@property (strong,nonatomic) APSWelcomeView * welcomeView;

@property (weak, nonatomic) IBOutlet UILabel *lblWCtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWCfirstParagraph;


- (IBAction)closeViewOrKeyboard:(id)sender;

- (IBAction)skipLogin:(id)sender;

- (IBAction)showHelp:(id)sender;

- (IBAction)showForgotPasswordWebView:(id)sender;

-(void) showLoginViewControllerWithCancelBlock:(void(^)(void)) cancelBlock
                                   failedBlock:(void(^)(void)) failedBlock
                                 succededBlock:(void(^)(void )) succededBlock
                                        onView:(UIView *) view;


@end
