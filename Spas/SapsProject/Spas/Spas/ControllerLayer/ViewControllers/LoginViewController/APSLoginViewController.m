//
//  APSLoginViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/27/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSLoginViewController.h"
#import "APSSessionAdapter.h"
#import <TvinciSDK/TVSessionManager.h>
#import <QuartzCore/QuartzCore.h>
#import <TvinciSDK/TVinci.h>

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define UesrNaneTextFieldTag 2014
#define PasswordTextFieldTag 2015

@interface APSLoginViewController ()<UITextFieldDelegate,UIScrollViewDelegate,APSWelcomeViewDelegate>


@property (copy, nonatomic) void (^cancelBlock)(void);
@property (copy, nonatomic) void (^failedBlock)(void);
@property (copy, nonatomic) void (^succededBlock)(void);
@property (weak, nonatomic) IBOutlet UIView *viewSmallLogin;
@property (retain, nonatomic) APSSessionAdapter * sessionAdapter;

@property (assign, nonatomic) CGPoint bgViewLocate;
@property (assign, nonatomic) CGPoint buttonCloseLocate;

@property BOOL isKeyboardShow;

@end

@implementation APSLoginViewController


#pragma mark - Init -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) showLoginViewControllerWithCancelBlock:(void(^)(void)) cancelBlock
                                                   failedBlock:(void(^)(void)) failedBlock
                                                 succededBlock:(void(^)(void )) succededBlock
                                                        onView:(UIView *) view
{
    self.cancelBlock = cancelBlock;
    self.failedBlock = failedBlock;
    self.succededBlock = succededBlock;
    //animate In
    self.view.size = view.bounds.size;
    [self animateIn:view];
}

#pragma mark - Memory -

-(void)dealloc
{
    NSLog(@"Login dealloced!");
    [self unregisterToKeyboardNotification];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self unregisterToKeyboardNotification];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Life -

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.lblError.text = @"";
    
    [self.view endEditing:YES];
    
    self.isKeyboardShow = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.baseViewControllerDelegate baseViewControllerMainView:self setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerToKeyboardNotification];

    [self setupScrollView];
    
    [self updateGaneralUI];

    if(is_iPad())
    {
       [self setupIpadView];
    }
    else
    {
       [self setupIphoneView];
    }
    
    [self updateTextInUI];
    
    self.bgViewLocate = CGPointMake(self.bgView.origin.x, self.bgView.origin.y);
    self.buttonCloseLocate = self.buttonClose.frame.origin;
    self.sessionAdapter = [[APSSessionAdapter alloc] init];
    [self.sessionAdapter setAssertBlock];
    
    self.textFieldUsername.isAccessibilityElement = YES;
    self.textFieldUsername.accessibilityLabel = @"Text Field Username";
    
    self.textfieldPassword.isAccessibilityElement = YES;
    self.textfieldPassword.accessibilityLabel = @"Text Field Password";
    
    self.buttonLogin.isAccessibilityElement = YES;
    self.buttonLogin.accessibilityLabel = @"Button Login";
    
}

#pragma mark - UI -

-(void)setupScrollView
{
    self.scrollView.delegate = self;
    self.scrollView.delaysContentTouches = NO;
    self.scrollView.contentSize = self.viewSmallLogin.frame.size;
    [self.scrollView addSubview:self.viewSmallLogin];
    self.scrollView.scrollEnabled = NO;
}

-(void)updateGaneralUI
{
    self.blurView.layerColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.65];
    self.blurView.blurRadius = 14;
    self.blurView.dynamic  = NO;
    [self.blurView setNeedsDisplay];
    
    self.bgView.layer.masksToBounds = NO;
    
    [self.bgView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.bgView.layer setShadowOpacity:0.5];
    [self.bgView.layer setShadowRadius:5.0];
    [self.bgView.layer setShadowOffset:CGSizeMake(0, 0)];
}

-(void)updateTextInUI
{
    self.labelCompanyName.text = NSLocalizedString(@"This service is only avialable", nil);
    self.labelCompanyDescription.text = NSLocalizedString(@"to KDG subscribers", nil);
    self.textFieldUsername.placeholder = NSLocalizedString(@"KDG ID or Email", nil);
    self.textfieldPassword.placeholder = NSLocalizedString(@"Password", nil);
    [self.buttonForgotPassword setTitle:NSLocalizedString(@"Forgot Password", nil) forState:UIControlStateNormal];

    [self.buttonNeedHelp setTitle:NSLocalizedString(@"Need Help", nil) forState:UIControlStateNormal];
    [self.buttonNeedHelp sizeToFit];

    [self.buttonSkipForNow setTitle:NSLocalizedString(@"Skip For Now", nil) forState:UIControlStateNormal];
        self.buttonLogin.titleLabel.attributedText = [APSTypography attributedString:NSLocalizedString(@"LOGIN", nil) withLetterSpacing:2.0];
    [self.buttonLogin setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateNormal];
    self.buttonLogin.titleLabel.font = [APSTypography fontRegularWithSize:is_iPad()?17:16];

    
    self.lblWCtitle.text = NSLocalizedString(@"Welcome to KDG", nil);
    self.lblWCfirstParagraph.text = NSLocalizedString(@"Login First Paragraph", nil);
    
//    self.textFieldUsername.text = @"FUT3";
//    self.textfieldPassword.text = @"Friendly_User_1";
}

-(void)setupIpadView
{
    self.labelCompanyName.font = [APSTypography fontRegularWithSize:16];
    self.labelCompanyName.textColor = [APSTypography colorLightGray];
    
    self.labelCompanyDescription.font = [APSTypography fontRegularWithSize:16];
    self.labelCompanyDescription.textColor = [APSTypography colorLightGray];
    
    self.bgView.layer.cornerRadius = 8.0;
    self.viewSmallLogin.layer.cornerRadius = 8;
    self.view.layer.cornerRadius = 8;
    
    self.buttonForgotPassword.titleLabel.font =[APSTypography fontMediumWithSize:16];
    self.buttonForgotPassword.titleLabel.textColor = [APSTypography colorMediumGray];
    
    self.lblError.font =[APSTypography fontRegularWithSize:13];
    self.lblError.textColor = [UIColor colorWithRed:255.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1]; // red

    self.lblWCtitle.font =[APSTypography fontMediumWithSize:19];
    self.lblWCtitle.textColor = [APSTypography colorMediumGray];
    
    self.lblWCfirstParagraph.font =[APSTypography fontMediumWithSize:16];
    self.lblWCfirstParagraph.textColor = [APSTypography colorLightGray];

}

-(void)setupIphoneView
{
    self.labelCompanyName.font = [APSTypography fontRegularWithSize:12];
    self.labelCompanyName.textColor = [APSTypography colorLightGray];
    
    self.labelCompanyDescription.font = [APSTypography fontRegularWithSize:12];
    self.labelCompanyDescription.textColor = [APSTypography colorLightGray];
    
    self.viewSmallLogin.layer.cornerRadius = 8;
    self.bgView.layer.cornerRadius = 8.0;
    self.view.layer.cornerRadius = 8;
    self.scrollView.layer.cornerRadius=8.0;
    self.buttonForgotPassword.titleLabel.font =[APSTypography fontMediumWithSize:14];
    self.buttonForgotPassword.titleLabel.textColor = [APSTypography colorMediumGray];
    
    self.lblError.font =[APSTypography fontRegularWithSize:12];
    self.lblError.textColor = [UIColor colorWithRed:255.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1]; // red
    
    self.lblWCtitle.font =[APSTypography fontMediumWithSize:19];
    self.lblWCtitle.textColor = [APSTypography colorMediumGray];
    
    self.lblWCfirstParagraph.font =[APSTypography fontMediumWithSize:14];
    self.lblWCfirstParagraph.textColor = [APSTypography colorLightGray];
}

#pragma mark - Animate In & Out -

-(void) animateIn:(UIView *) superView
{
    CGPoint oldorigin = self.animateView.origin;
    self.animateView.origin = CGPointMake(self.animateView.origin.x,superView.frame.size.height+100);
    self.view.alpha=0;
    [superView addSubview:self.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.alpha=1;
        [self.baseViewControllerDelegate baseViewControllerMainView:self setStatusBarStyle:UIStatusBarStyleLightContent];

    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 animations:^{
            if (!is_iPad() && !IS_IPHONE5)
            {
                self.animateView.center = CGPointMake(self.view.center.x, self.view.center.y+15);
            }
            else
            {
                self.animateView.origin = oldorigin;
            }
            
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(void) animateOutWithCompletion:(void(^)(void)) completion
{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.animateView.origin = CGPointMake(self.animateView.origin.x,self.view.frame.size.height+100);
        
    } completion:^(BOOL finished) {
        self.view.alpha=1.0;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.view.alpha=0.0;
            
        } completion:^(BOOL finished) {
            
            [self.view removeFromSuperview];
            if (completion)
            {
                completion();
            }
            
        }];
    }];
}

#pragma mark - Login -

- (IBAction)login:(id)sender
{
    [self closeKeyboard];

    if ([self.textFieldUsername.text length] == 0 || [self.textfieldPassword.text length] == 0)
    {
        ASLogInfo(@"No user name or password")
        [self.lblError setText:NSLocalizedString(@"Wrong user name or password",nil)];
        self.lblError.hidden = NO;
        return;
    }

    [self showLodingMode];
    self.lblError.hidden = YES;
   
    //  loginWithUserName  ssoLoginWithUserName
    self.sessionAdapter.canChangeUserLoginStatus = YES;
    [self.sessionAdapter ssoLoginWithUserName:self.textFieldUsername.text password:self.textfieldPassword.text successBlock:^{
        
        self.lblError.hidden = YES;
        [self showWelcomeView];
        
    } failedBlock:^(NSString *failedError) {
        
        __block NSString * failedErrorString =failedError;
        
        [self showLoginModeWithCompletion:^{
         
            /* 
             // SYN-4486 Wrong error message when number of devices is exceeded
             
            if ([failedErrorString isEqualToString:NSLocalizedString(@"Exceeded Limit",nil)])
            {
                failedErrorString = @"Error 500";
            }
            */
            
            if (self.sessionAdapter.isLoginHaveCustomError)
            {
                self.lblError.hidden = YES;
                [self showLoginErrorAlertViewWithMassage:failedErrorString];
            }
            else
            {
                self.lblError.hidden = NO;
                [self.lblError setText:failedErrorString];
            }
            
            [self.sessionAdapter logoutWithSuccessBlock:^{
                
            } failedBlock:^(NSString *failedError) {
                
            }];

        }];
        
    } andRgisterDeviceBlock:^{
        
        [self.sessionAdapter registerDeviceToDomain];
        
    }];
}

-(void)closeKeyboard
{
    [self.view endEditing:YES];
}

-(void)closeKeyboardWithoutAnimation;
{
    [UIView setAnimationsEnabled:NO];
    [self closeKeyboard];
    [UIView setAnimationsEnabled:YES];
}

-(void)showLoginErrorAlertViewWithMassage:(NSString*)failedError
{
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Error",nil) message:failedError delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        
        [alertView show];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Error",nil) message:failedError preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - Action -

-(IBAction)close:(id)sender
{
    [self animateOutWithCompletion:^{
        if (self.cancelBlock)
        {
            self.cancelBlock();
        }
    }];
}

- (IBAction)closeViewOrKeyboard:(id)sender
{
    if (self.isKeyboardShow)
    {
        [self.view endEditing:YES];
    }
    else
    {
        [self close:nil];
    }
}

- (void)showLodingMode
{
    self.viewSmallLogin.alpha=1.0;
    
    [self.loginActivityIndicator startAnimating];

    [UIView animateWithDuration:0.3 animations:^{
        
        self.viewSmallLogin.alpha=1.0;
        self.textfieldPassword.alpha = 0.0;
        self.textFieldUsername.alpha = 0.0;
        self.buttonForgotPassword.alpha = 0.0;
        self.buttonLogin.alpha = 0.0;
        self.buttonNeedHelp.alpha=0.0;
        self.buttonSkipForNow.alpha=0.0;
        self.lblError.alpha = 0.0;
        self.labelCompanyName.alpha=0.0;
        self.labelCompanyDescription.alpha = 0.0;
        self.loginActivityIndicator.alpha = 1.0;
        self.loginActivityIndicator.hidden = NO;
    } completion:^(BOOL finished) {
      
    }];
}

- (void)showLoginModeWithCompletion:(void(^)(void)) completion
{
    [self.loginActivityIndicator stopAnimating];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewSmallLogin.alpha=1.0;
        self.textfieldPassword.alpha = 1.0;
        self.textFieldUsername.alpha = 1.0;
        self.buttonForgotPassword.alpha = 1.0;
        self.buttonLogin.alpha = 1.0;
        self.buttonNeedHelp.alpha=1.0;
        self.buttonSkipForNow.alpha=1.0;
        self.lblError.alpha = 1.0;
        self.labelCompanyName.alpha=1.0;
        self.labelCompanyDescription.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        if (completion)
        {
            completion();
        }
    }];
}


- (void)showWelcomeView
{
    self.welcomeView = [APSWelcomeView welcomeView];
    self.welcomeView.delegate = self;
    self.welcomeView.origin = CGPointMake(0, 0);
    self.welcomeView.alpha = 0;
    self.viewSmallLogin.alpha=1.0;
    [self.animateView addSubview:self.welcomeView];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.welcomeView.alpha = 1.0;
        self.viewSmallLogin.alpha=0.0;
        self.buttonNeedHelp.alpha=0.0;
        self.buttonSkipForNow.alpha=0.0;
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (IBAction)skipLogin:(id)sender
{
    [self close:nil];
}

- (IBAction)showHelp:(id)sender
{
    TVMenuItem * item = [self findMenuItemByUniqueName:@"NeedHelp"];
    NSURL * link = [self linkFromMenuItem:item];;
    NSString * title = [NSLocalizedString(@"Need Help", nil) uppercaseString];
    [self openWebViewWithLink:link andPageTitle:title];
}

- (IBAction)showForgotPasswordWebView:(id)sender
{
    TVMenuItem * item = [self findMenuItemByUniqueName:@"ForgotPassword"];
    NSURL * link = [self linkFromMenuItem:item];;
//    NSString * title = [NSLocalizedString(@"Forgot Password", nil) uppercaseString];
    NSString * title = [NSLocalizedString(@"Forgot Password Title", nil) uppercaseString];
    [self openWebViewWithLink:link andPageTitle:title];
}

#pragma mark - UITextField delegate -

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag)
    {
        case UesrNaneTextFieldTag:
            [self.textfieldPassword becomeFirstResponder];
            break;
        case PasswordTextFieldTag:
            [self login:nil];
            [textField resignFirstResponder];
            break;
        default:
            [textField resignFirstResponder];
            break;
    }
    return YES;
}

#pragma mark - KeyboardNotification -

-(void)registerToKeyboardNotification
{
    [self unregisterToKeyboardNotification];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [center addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)unregisterToKeyboardNotification
{
     NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [center removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void) keyboardWillShow:(NSNotification *)inNotification
{
    CGPoint orginalScrollViewOrigen = self.bgViewLocate;
    CGPoint orginalCloseButtonOrigen = self.buttonCloseLocate;
    
    NSInteger contentOffsetY =0;
    if (is_iPad())
    {
        orginalScrollViewOrigen.y -= 75;
        orginalCloseButtonOrigen.y  -= 75;
        contentOffsetY = 100;
    }
    else if (IS_IPHONE5)
    {
        orginalScrollViewOrigen.y -= 60;
        orginalCloseButtonOrigen.y  -= 60;
        contentOffsetY = 10;

    }
    else
    {
        orginalScrollViewOrigen.y -= 20;
        orginalCloseButtonOrigen.y  -= 20;
        contentOffsetY = 90;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.bgView.origin = orginalScrollViewOrigen;
        self.buttonClose.origin = orginalCloseButtonOrigen;
        self.scrollView.contentOffset = CGPointMake(0,contentOffsetY);
    
    } completion:^(BOOL finished) {
        self.isKeyboardShow = YES;
        self.scrollView.scrollEnabled = YES;
    }];
}

-(void) keyboardWillHide:(NSNotification *)inNotification
{
    [UIView animateWithDuration:0.3 animations:^{
        self.bgView.origin = self.bgViewLocate;
        self.buttonClose.origin = self.buttonCloseLocate;
        self.scrollView.contentOffset = CGPointMake(0,0);
    } completion:^(BOOL finished) {
        self.isKeyboardShow = NO;
        self.scrollView.scrollEnabled = NO;

    }];
}

#pragma mark - remove Device From All Domains -

- (IBAction)removeDeviceFromAllDomains:(id)sender
{
    ASLogInfo(@"removeDeviceFromAllDomains");
    TVPAPIRequest * strongRequest = [TVPDomainAPI requestForGetDeviceDomains:nil];
    __weak TVPAPIRequest * request = strongRequest;
    
    [request setFailedBlock:^{
        NSLog(@"GetDeviceDomain failed");
    }];
    
    [request setCompletionBlock:^{
        
        NSInteger deviceBrand = kDeviceBrand_iPhone;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            deviceBrand = kDeviceBrand_iPad;
        }
        else
        {
            deviceBrand = kDeviceBrand_iPhone;
        }
        
        NSArray * domains = [[request JSONResponse] arrayByRemovingNSNulls];
        for (NSDictionary * dict in domains)
        {
            NSNumber * domainID =  [dict objectForKey:@"DomainID"];
            TVPAPIRequest * strongRequest = [TVPDomainAPI requestForRemoveDeviceFromDomainWithDeviceName:[TVConfigurationManager getTVUDID]  deviceBrandID:deviceBrand domainID:domainID.intValue delegate:nil];

            __weak TVPAPIRequest * removeRequest = strongRequest;
            
            [removeRequest setCompletionBlock:^{
                NSDictionary *response = [[removeRequest JSONResponse] dictionaryByRemovingNSNulls];
                NSInteger status = [response[@"m_oDomainResponseStatus"] integerValue];
                NSString *message = status ? @"removing device completed" : @"removing device failed";
                
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:message
                                                                    delegate:nil
                                                           cancelButtonTitle:@"ok"
                                                           otherButtonTitles:nil];
                    [alert show];
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"ok" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }];
            [self sendRequest:removeRequest];
        }
    }];
    [self sendRequest:request];
}

#pragma mark - webView -

-(void)openWebViewWithLink:(NSURL *)webLink andPageTitle:(NSString *)title
{
    [self closeKeyboardWithoutAnimation];
    
    self.loginWebView = [APSLoginWebView loginWebView];
    self.loginWebView.baseViewControllerDelegate = self.baseViewControllerDelegate;
    
    [self.loginWebView setupWebViewWithLink:webLink andPageTitle:title];
    [self.loginWebView showLoginWebViewOnView:self.view];
}

#pragma mark - Menu -

-(TVMenuItem*) findMenuItemByUniqueName:(NSString*)uniqueName
{
    NSArray * items =[[APSLayoutManager sharedLayoutManager] arrayMenuItems];
    NSArray * baseItems = nil;
    for (TVMenuItem * item in items)
    {
        if ([item.uniqueName isEqualToString:@"Misc"])
        {
            baseItems = [NSArray arrayWithArray:item.children];
        }
    }
    if (baseItems)
    {
        for (TVMenuItem * item in baseItems)
        {
            NSString * type = [item.layout objectForKey:@"Enum"];
            if ([type isEqualToString:uniqueName])
            {
                return item;
            }
        }
    }
    
    return nil;
}

-(NSURL*)linkFromMenuItem:(TVMenuItem *) item
{
    NSURL * link = nil;
    if (item)
    {
        NSString * type = [item.layout objectForKey:@"Address"];
        link = [NSURL URLWithString:type];
    }
    return link;
}

#pragma mark - ScrollView -

-(BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return YES;
}

#pragma mark - APSWelcomeViewDelegate -

-(void) welcomeView:(APSWelcomeView *)welcomeView didSelectStart:(UIButton *) button
{
    [self animateOutWithCompletion:^{
        
        if (self.succededBlock)
        {
            self.succededBlock();
        }
    }];
}
@end
