//
//  APSSettingsViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSBlurView.h"
#import "APSBaseBarsViewController.h"


@interface APSSettingsViewController : APSBaseBarsViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *tableBgView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong,nonatomic) NSMutableArray * menuItems;

-(void) settingsViewControllerWithCancelBlock:(void(^)(void)) cancelBlock
                                      selectBlock:(void(^)(APSBaseBarsViewController *)) selectBlock;

@end
