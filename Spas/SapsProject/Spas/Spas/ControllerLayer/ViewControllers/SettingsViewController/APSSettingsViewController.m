//
//  APSSettingsViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSettingsViewController.h"
#import "APSSettingsCell.h"
#import <QuartzCore/QuartzCore.h>
#import "APSLayoutManager.h"
#import "APSAccountViewController.h"
#import "GCSessionAdapter.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <TvinciSDK/TVConfigurationManager.h>
#import <sys/utsname.h>
#import "APSSessionManager.h"
#import "APSAlertController.h"


#define AppIosVersion [[UIDevice currentDevice] systemVersion]
#define AppBuild [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define  AppId  [TVConfigurationManager getTVUDID]

NSString * const SettingMenuAccountType = @"Account";
NSString * const SettingMenuURLType = @"URL";
NSString * const SettingMenuDataType = @"Data";
NSString * const SettingMenuLogoutType = @"Logout";
NSString * const SettingMenuMailType = @"Mail";

NSString * const SettingMenuLayoutKeyAddress = @"Address";
NSString * const SettingMenuLayoutKeyType = @"Type";

NSString * const SettingMenuMyZone = @"MyZone";

#define MaximumRows 8


@interface APSSettingsViewController () <UIAlertViewDelegate,MFMailComposeViewControllerDelegate>

@property (retain, nonatomic) NSMutableArray * arraySettingsOptions;

@property (copy, nonatomic) void (^cancelBlock)(void);
@property (copy, nonatomic) void (^selectBlock)(APSBaseBarsViewController * viewcontroller);

@property (nonatomic ,strong) GCSessionAdapter * sessionAdapter;

@property (strong, nonatomic)  UIAlertView * alert;

@end

@implementation APSSettingsViewController


#pragma mark - Init -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) settingsViewControllerWithCancelBlock:(void(^)(void)) cancelBlock
                                      selectBlock:(void(^)(APSBaseBarsViewController *)) selectBlock
{
    self.cancelBlock = cancelBlock;
    self.selectBlock = selectBlock;
}

#pragma mark - Life - 

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    if (is_iPad())
    {
        [self.view setBackgroundColor:[UIColor clearColor]];
        self.tableView.scrollEnabled = NO;
    }
    [self registerToSessionAdapterNotifcations];
    
    self.labelTitle.text = [NSLocalizedString(@"SETTINGS", nil) uppercaseString];
    self.labelTitle.attributedText = [APSTypography attributedString:self.labelTitle.text withLetterSpacing:1.0];
    self.sessionAdapter = [[GCSessionAdapter alloc] init];
    self.sessionAdapter.viewController = self;
    
    [self buildDataAndTable];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerScreenView:self.labelTitle.text];
}

#pragma mark - Memory -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [self unregisterToSessionAdapterNotifcations];
}

#pragma mark - Setups -

-(void)buildDataAndTable
{
    [self buildItemsArrays];
    
    if (is_iPad())
    {
        [self updateTableAndTableBgViewFrame];
        
        [self setupTableViewShadow];
    }
    [self.tableView reloadData];
}

-(NSArray*) findMyZoonInMenu
{
    NSArray * items =[[APSLayoutManager sharedLayoutManager] arrayMenuItems];
    for (TVMenuItem * item in items)
    {
        if ([item.uniqueName isEqualToString:SettingMenuMyZone])
        {
            return [NSArray arrayWithArray:item.children];
        }
    }
    return [NSArray array];
}

-(void)buildItemsArrays
{
    
    self.menuItems =  [NSMutableArray arrayWithArray:[self findMyZoonInMenu]];
   
    //[NSMutableArray arrayWithArray:[[[[APSLayoutManager sharedLayoutManager] arrayMenuItems] lastObject] children]];
    self.arraySettingsOptions = [NSMutableArray array];
    for (TVMenuItem * item in self.menuItems)
    {
        NSString * itemName = item.name;
        if (itemName)
        {
            ASLogInfo(@"%@",item.layout);

            [self.arraySettingsOptions addObject:itemName];
        }
    }

    NSString * lastItem = [self.arraySettingsOptions lastObject];
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn] == NO)
    {
        lastItem = NSLocalizedString(@"Login", nil);
        
        // remove "Account" if user logout
//        NSInteger itemIndex =0;
//        for (TVMenuItem * item in self.menuItems)
//        {
//            if ([item.uniqueName isEqualToString:SettingMenuAccountType])
//            {
//                break;
//            }
//            itemIndex++;
//        }
//        [self.arraySettingsOptions removeObjectAtIndex:itemIndex];
//        [self.menuItems removeObjectAtIndex:itemIndex];
    }

    [self.arraySettingsOptions replaceObjectAtIndex:([self.arraySettingsOptions count]-1) withObject:lastItem];
}

-(void)setupTableViewShadow
{
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.layer.cornerRadius = 4.0;
    self.tableView.layer.borderWidth = 1.0f;
    self.tableView.layer.borderColor = [UIColor clearColor].CGColor;
    [self.tableBgView.layer setCornerRadius: 4.0];
    
    // border
    [self.tableBgView.layer setBorderColor:[UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0].CGColor];
    [self.tableBgView.layer setBorderWidth:1.0f];
    
    // drop shadow
    [self.tableBgView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.tableBgView.layer setShadowOpacity:0.2];
    [self.tableBgView.layer setShadowRadius:4.0];
    [self.tableBgView.layer setShadowOffset:CGSizeMake(0, 0)];
}

-(void)updateTableAndTableBgViewFrame
{
    APSSettingsCell * cell = nil;
    if (cell == nil)
    {
        if (is_iPad())
        {
            cell = [APSSettingsCell settingsCell];
        }
        else
        {
            cell = [APSSettingsCell iPhoneSettingsCell];
        }
    }

    NSInteger itemsNumber =[self.arraySettingsOptions count];
    if (itemsNumber > MaximumRows)
    {
        itemsNumber = MaximumRows;
        self.tableView.scrollEnabled = YES;
        self.tableView.showsVerticalScrollIndicator = YES;
    }
    else
    {
        self.tableView.scrollEnabled = NO;
        self.tableView.showsVerticalScrollIndicator = NO;
    }
    
    CGRect frame = self.tableBgView.frame;
    frame.size.height = itemsNumber * cell.frame.size.height;
    self.tableBgView.frame = frame;
    
    CGRect tableFrame = self.tableView.frame;
    tableFrame.size.height = itemsNumber * cell.frame.size.height;
    self.tableView.frame = tableFrame;
    
    [self.tableBgView layoutIfNeeded];
    [self.tableView layoutIfNeeded];
}

#pragma mark - Table view -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (is_iPad())
    {
        return self.arraySettingsOptions.count;
    }
    else
    {
        switch (section)
        {
            case 0:
                return self.arraySettingsOptions.count-1;
                break;
            case 1:
                return 1;
                break;
            default:
                break;
        };
    }
   return  self.arraySettingsOptions.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (is_iPad())
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

#define cellIdentifer @"APSSettingsCell"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    APSSettingsCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer];
    if (cell == nil)
    {
        if (is_iPad())
        {
            cell = [APSSettingsCell settingsCell];
        }
        else
        {
            cell = [APSSettingsCell iPhoneSettingsCell];
        }
    }
    
    
    NSString * cellText = self.arraySettingsOptions[indexPath.row];
    TVMenuItem * item = [self.menuItems objectAtIndex:indexPath.row] ;

    if (is_iPad())
    {
        if ([[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuLogoutType])
        {
            cell.title.font =[APSTypography fontMediumWithSize:16];
        }
        else
        {
            cell.title.font =[APSTypography fontRegularWithSize:16];
        }
        
        cell.customSeparator.hidden = NO;
        if (indexPath.row == [self.arraySettingsOptions count]-1)
        {
            cell.customSeparator.hidden = YES;
        }

        cell.title.textColor = [APSTypography colorDrakGray];
    }
    else
    {
        if (indexPath.section == 1)
        {
            cellText = [self.arraySettingsOptions lastObject];
            cell.disclaimerArrow.hidden = YES;
        }
        else
        {
            cell.disclaimerArrow.hidden = NO;
        }
        cell.title.font =[APSTypography fontRegularWithSize:16];
        cell.title.textColor = [APSTypography colorMediumGray];
    }
    
    cell.title.text = cellText;
    
    cell.seperatorLine.hidden = NO;
    if (indexPath.section == 0 && indexPath.row == [self.tableView numberOfRowsInSection:0] -1)
    {
        cell.seperatorLine.hidden = YES;
    }
    if (indexPath.section == 1)
    {
        cell.seperatorLine.hidden = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setHighlighted:NO];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (is_iPad())
    {
        return 59;
    }
    else
    {
        return 60;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    APSBaseBarsViewController * destinationViewController;

    TVMenuItem * item = [self.menuItems objectAtIndex:indexPath.row] ;
    
    if (([[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuLogoutType]) || (!is_iPad() && indexPath.section == 1))
    {
        [self menuItemLogoutSelect];
    }
    else if ([[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuURLType] || [[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuDataType])
    {
        NSString * link =  [item.layout objectForKey:SettingMenuLayoutKeyAddress];
        NSString * title = [self.arraySettingsOptions objectAtIndex:indexPath.row];
        destinationViewController = [[APSViewControllersFactory defaultFactory] webViewControllerWithUrl:[NSURL URLWithString:link] andPageTitle:title andMenuItem:item];
    }
    else if ([[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuAccountType])
    {
        NSString * title = [self.arraySettingsOptions objectAtIndex:indexPath.row];
        destinationViewController = [[APSViewControllersFactory defaultFactory] accountViewControllerWithPageTitle:title];
        
    }
    else if ([[item.layout objectForKey:SettingMenuLayoutKeyType] isEqualToString:SettingMenuMailType])
    {
        [self sendEmailWith:item];
        
    }
    
    if (destinationViewController)
    {        
        if (is_iPad())
        {
            if (self.selectBlock)
            {
                self.selectBlock(destinationViewController);
            }
        }
        else
        {
            [self.navigationController pushViewController:destinationViewController animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)menuItemLogoutSelect
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn]) // make logOut
    {
        [self showLogOutActionSheet];
    }
    else // make login
    {
        [self unregisterToLogOutSeccsesNotification];
        [self cancelSettingsPopup:nil];
        [self showLoginViewControllerIfNeededWithFailedBlock:^{
            
        } succededBlock:^{
            
            [self buildDataAndTable];
        }];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if (!is_iPad())
    {
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
        [header setBackgroundColor:[APSTypography colorGrayWithValue:235]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!is_iPad())
    {
        return 30;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (!is_iPad())
    {
        return 1;
    }
    return 1;
}
#pragma mark - action -

- (IBAction)cancelSettingsPopup:(id)sender
{
    if (self.cancelBlock)
    {
        self.cancelBlock();
    }
}

-(void)logout
{
    [self.sessionAdapter logoutWithSuccessBlock:^{
        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];
        [self buildDataAndTable];
        [self menuView:nil didSelectDisplayPosition:DisplayPosition_Home];
        
    } failedBlock:^(NSString *failedError) {
        
    }];
}

#pragma mark - ActionSheet -

-(void)showLogOutActionSheet
{
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Are you sure you want to log out?", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Log out", nil), nil];
        [self.alert show];
    } else {
        APSAlertController *alertController = [APSAlertController alertControllerWithTitle:NSLocalizedString(@"Are you sure you want to log out?", nil) message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancelAction];
        
        __weak typeof(self) weakSelf = self;
        UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Log out", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           __strong typeof(weakSelf) strongSelf = weakSelf;
                                           [strongSelf logout];
                                       }];
        [alertController addAction:logoutAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex])
    {
        
    }
    else
    {
        [self logout];
    }
}


#pragma mark - Session Adapter Notifcations -

-(void)registerToSessionAdapterNotifcations
{
    [self unregisterToSessionAdapterNotifcations];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedInChange) name:GCSessionAdapterLoginSuccessfullyCompleted object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogedInChange) name:GCSessionAdapterLoginOutSuccessfullyCompleted object:nil];//
}

-(void)unregisterToSessionAdapterNotifcations
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GCSessionAdapterLoginSuccessfullyCompleted object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GCSessionAdapterLoginOutSuccessfullyCompleted object:nil];
}

-(void)userLogedInChange
{
     [self buildDataAndTable];;
}

-(void)unregisterToLogOutSeccsesNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:nil];
}

-(void)sendEmailWith:(TVMenuItem * )item
{

    // From within your active view controller
    if([MFMailComposeViewController canSendMail])
    {
        ASLogInfo(@"machineName = %@",[self machineName]);
        NSString * appVersion= NSLocalizedString(@"App Version", nil); //1.1.0
        NSString * iosVersion= NSLocalizedString(@"iOS Version", nil); //8.1
        NSString * type = NSLocalizedString(@"Device type", nil);// iOS
        NSString * device = @"iOS";
        NSString * appudid = NSLocalizedString(@"App UDID", nil);
        NSString * link =  [item.layout objectForKey:SettingMenuLayoutKeyAddress];
        NSString * mailStartText = NSLocalizedString(@"Mail start text", nil);
        NSString * deviceModel = [self machineName];
        NSString * deviceModelTitle = NSLocalizedString(@"device Model Title", nil);;
        NSString * userLoginStatus = NSLocalizedString(@"user Login Status", nil);
        
        NSString *externalVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"ExternalVersion"];
        
        NSString * mailSubject = [NSString stringWithFormat:@"%@\n\n\n\n%@: %@ \n %@: %@ \n %@: %@ \n %@: %@ \n %@: %@\n %@: %@",mailStartText,type,device,iosVersion,AppIosVersion,deviceModelTitle,deviceModel,appVersion,externalVersion,appudid,AppId,userLoginStatus,[self userStatusString]];
        
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
    
        [mailCont setSubject:NSLocalizedString(@"Email subject", nil)];
        [mailCont setToRecipients:[NSArray arrayWithObject:link]];
        [mailCont setMessageBody:mailSubject isHTML:NO];
        
       
        [[self.baseViewControllerDelegate baseViewControllerMainViewController:self] presentViewController:mailCont animated:YES completion:nil];
        
    } else {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No mail account", nil) message:NSLocalizedString(@"Please set up a Mail account in order to send a mail.", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


- (NSString *)machineName
{
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

-(NSString *)userStatusString
{
    NSString * status = NSLocalizedString(@"Logout", nil);
    switch ([[APSSessionManager sharedAPSSessionManager] userLoginStaus])
    {
        case APSLoginStaus_Logout:
//            status = NSLocalizedString(@"Logout", nil);
            status = @"Logged Out";

            break;
        case APSLoginStaus_Login:
//            status = NSLocalizedString(@"Logged in", nil);
            status =@"Logged in";

            break;
        case APSLoginStaus_OutOfNetwork:
//            status = NSLocalizedString(@"Out Of Network", nil);
            status = @"Out Of Network";
            break;
        default:
            break;
    }
    
    return status;
}
@end
