//
//  APSBaseBarsViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "APSMenuView.h"
#import "APSTopRootBar.h"
#import "APSTopBar.h"
#import "APSNavigationController.h"
#import "APSPlayerBar.h"
#import "APSNoInternetView.h"


@class APSSettingsViewController;

@class APSAutocompleteteSearchViewController;

@interface APSBaseBarsViewController : APSBaseViewController


@property (strong, nonatomic) IBOutlet UIButton * buttonMenu;
//@property (retain, nonatomic) IBOutlet UITextField * textFieldSearch;
@property (weak, nonatomic) IBOutlet UIButton * buttonCancelSearch;


@property (strong, nonatomic) IBOutlet APSTopBar * topBar;
@property (strong, nonatomic) UILabel * labelTitle;
@property (strong, nonatomic) APSMenuView * viewMenu;
@property (strong, nonatomic) APSTopRootBar * topRootBar;
@property (retain, nonatomic) APSPlayerBar * playerBar;
@property (retain, nonatomic) IBOutlet UIView * buttomBar;
@property (nonatomic ,strong) APSNoInternetView * minimizedScreenNoInternet;

@property (strong,nonatomic) APSSettingsViewController * settingsVC;

-(IBAction) openMenu:(id) sender;
-(void) menuView:(APSMenuView *)menu didSelectDisplayPosition:(DisplayPosition) displayPostion;
-(void) autocompleteteSearch:(APSAutocompleteteSearchViewController*) autocompleteteSearch didSelectClose:(id) sender;
-(void) navigationController:(APSNavigationController *) navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
