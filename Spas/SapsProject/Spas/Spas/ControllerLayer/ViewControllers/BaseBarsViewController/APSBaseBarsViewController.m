//
//  APSBaseBarsViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseBarsViewController.h"
#import "APSMenuView.h"
#import "APSSearchViewController.h"
#import "APSTopRootBar.h"
#import "APSAutocompleteteSearchViewController.h"
#import "APSMenuView.h"
#import "APSSettingsViewController.h"
#import <TvinciSDK/TVConfigurationManager.h>
#import "APSBackBar.h"
#import "APSAccountViewController.h"
#import "APSPlayerBar.h"
#import "APSFullScreenPlayerViewController.h"
#import "APSNavigationController.h"
#import "APSPurePlayerViewController.h"
#import "APSNetworkManager.h"
#import "APSSessionManager.h"
#import "APSFullScreenOutOfNetworkView.h"
#import "APSGeneralPlayerErrorView.h"
#import "APSAlertController.h"

@interface APSBaseBarsViewController ()<AutocompleteteSearchViewControllerDelegate,APSMenuViewDelegate,APSSearchFieldDelegate,UITextFieldDelegate, APSPlayerBarDelegate,APSNoInternetViewDelegate,APSFullScreenOutOfNetworkViewDelegate,APSGeneralPlayerErrorViewDelegate>

@property (strong, nonatomic) APSAutocompleteteSearchViewController * autoCompleteSearchViewController;
@property (strong, nonatomic) APSFullScreenOutOfNetworkView * outOfNetworkView;
@property (strong, nonatomic) APSGeneralPlayerErrorView * generalPlayerErrorView;

@end

@implementation APSBaseBarsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //clear top bar
    self.view.backgroundColor = [APSTypography colorBackground];
    [self setupTopBar];
    [self setupButtomBar];
   
//    [self registerToNoInerenetNotifcation];
}



-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerToPurePlayerHaveErrorNotificationName];
    [self registerToNoInerenetNotifcation];


    
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unregisterToPurePlayerHaveErrorNotificationName];
    [self unregisterToNoInerenetNotifcation];

}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if (self.topBar.searchField.active) {
        
        [self.autoCompleteSearchViewController close:nil];
        
    }
    
    
}

-(void) setupTopBar
{
   
    if(self.topBar != nil)
    {
        // We have a topBar outlet
        self.topBar.searchField.textfieldSearch.delegate= self;
        self.topBar.searchField.delegate = self;

        // If the top bar is setup as the APSSearchViewController is initialized
        // we should setup the "cancel button"
        if ([self isKindOfClass:[APSSearchViewController class]]){
            APSSearchViewController* searchVC = (APSSearchViewController*)self;
            [self setupCancelButton:searchVC.buttonCancelSearch];

        }

          if (!is_iPad() ){
              [self setupCancelButton:self.buttonCancelSearch];
              
        }
      
        if ([self.topBar isKindOfClass:[APSBackBar class]])
        {
            APSBackBar * backBar = (APSBackBar *)self.topBar;
            self.labelTitle = backBar.labelTitle;
            [backBar.btnBack addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        }
        return;
    }
    
    if (self.navigationController.viewControllers.count == 1 )
    {
        // we are at the root of the nav ctrl - i.e. we are at "Home" or "EPG"
        NSString *nibName = [APSMenuView nibName];;
        NSArray *contents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        for (id object in contents)
        {
            if ([object isKindOfClass:[APSMenuView class]])
            {
                self.viewMenu = object;
                break;
            }
        }

        self.viewMenu.delegate = self;
        [self.viewMenu.collectionViewTabSelector reloadData];
        [self.viewMenu.buttonAppInfo addTarget:self action:@selector(showAppInfo:) forControlEvents:UIControlEventTouchDownRepeat];
        
        [self setupCancelButton:self.viewMenu.buttonCancelSearch];
        if(is_iPad())
        {
            self.buttonCancelSearch = self.viewMenu.buttonCancelSearch;
            self.viewMenu.searchField.delegate = self;
            self.topBar = self.viewMenu;
        }
        else
        {
            APSTopRootBar * topRootView = [APSTopRootBar topRootBar];
            self.topRootBar = topRootView;
            [topRootView.menuButton addTarget:self action:@selector(openMenu:) forControlEvents:UIControlEventTouchUpInside];
            self.topRootBar.searchField.delegate = self;
            [self.topRootBar.searchButton addTarget:self action:@selector( openAutoCompleteSearch:) forControlEvents:UIControlEventTouchUpInside];
            [self.topRootBar.buttonAppInfo addTarget:self action:@selector(showAppInfo:) forControlEvents:UIControlEventTouchDownRepeat];
            self.buttonCancelSearch = self.topRootBar.buttonCancelSearch;
            self.labelTitle = topRootView.labelTitle;
            self.topBar = topRootView;
            
        }
        self.topBar.searchField.textfieldSearch.delegate = self;

    }
    else
    {
        APSBackBar * backBar = nil;
        backBar = [ APSBackBar backBar];
        self.labelTitle = backBar.labelTitle;
        [backBar.btnBack addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        self.topBar = backBar;
    }

    self.topBar.searchField.textfieldSearch.delegate = self;
    self.topBar.blurViewTopBar.underlyingView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
}

-(void) setupCancelButton:(UIButton*)button
{
    NSString * str =[NSLocalizedString(@"CANCEL", nil) uppercaseString];
    [button setTitle:str forState:UIControlStateNormal];
    button.titleLabel.font = [APSTypography fontRegularWithSize:12];

    if(is_iPad())
    {
        button.titleLabel.attributedText =[APSTypography attributedString:str withLetterSpacing:1.5];
    }
    else
    {
        button.titleLabel.attributedText =[APSTypography attributedString:str withLetterSpacing:1.0];
    }
}

-(void) setupButtomBar
{
    [self setupPlayerBar];
}

-(void) setupPlayerBar
{
    if (self.playerBar==nil)
    {
        self.playerBar = [APSPlayerBar playBarOfType:PlayerBarType_MinimizedScreen];
    }
    self.playerBar.delegate = self;
    self.playerBar.purePlayerViewController = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    if ([self.buttomBar isKindOfClass:[APSNoInternetView class]] == NO)
    {
        self.buttomBar = self.playerBar;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(APSFullScreenOutOfNetworkView *) outOfNetworkView
{
    if (_outOfNetworkView == nil)
    {
        _outOfNetworkView = [APSFullScreenOutOfNetworkView fullScreenOutOfNetworkView];
    }
    
    return _outOfNetworkView;
}


#pragma mark - topBar actions
-(IBAction) openMenu:(id) sender
{
    if (!is_iPad())
    {
        [self showMenuView];
    }
    else
    {
        
    }
}

-(IBAction)showAppInfo:(id)sender
{
    NSString * appId = [TVConfigurationManager getTVUDID];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSString *title = @"App Information";
    NSString *message = [NSString stringWithFormat:@" App Version: %@ \n buildNumber: %@\n appId: %@\n",appVersion,buildNumber,appId];

    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // alert.delegate = self;
        [alert show];
    } else {
        APSAlertController *alertController = [APSAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}

-(IBAction) back:(id) sender
{
    if (is_iPad())
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - autocomplete -

-(void)updateOpenAutoComplete
{
    self.autoCompleteSearchViewController.delegate = self;
    self.autoCompleteSearchViewController.baseViewControllerDelegate = self.baseViewControllerDelegate;
    self.topBar.searchField.active = YES;
    self.autoCompleteSearchViewController.view.alpha = 1;
}

-(IBAction) openAutoCompleteSearch:(id) sender
{
    if (is_iPad())
    {
        [self closeSettingViewWithCompletion:nil];
    }
    if (is_iPad() && self.viewMenu)
    {
        [self.viewMenu expandSearchWithCompletion:^{
            [self openAutoCompleteSearch];
        }];
    }
    else if (!is_iPad() && self.topRootBar)
    {
        [self.topRootBar showSearchWithCompletion:^{
            [self openAutoCompleteSearch];
        }];
        
    }
    else
    {
        [self openAutoCompleteSearch];
    }
}

-(void) openAutoCompleteSearch
{
    if (self.autoCompleteSearchViewController == nil || ![self.autoCompleteSearchViewController.view isDescendantOfView:[self view]])
    {
        self.autoCompleteSearchViewController = (APSAutocompleteteSearchViewController *)[[APSViewControllersFactory defaultFactory] autocompleteSearchViewController];
    }
    
    self.autoCompleteSearchViewController.delegate = self;
    self.autoCompleteSearchViewController.view.alpha = 0;
    self.autoCompleteSearchViewController.view.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    [self.autoCompleteSearchViewController.blurView setUnderlyingView:self.view];
    self.autoCompleteSearchViewController.blurView.blurRadius = 8;
    [self.autoCompleteSearchViewController.blurView setNeedsDisplay];
    
    [self.autoCompleteSearchViewController updateSearchTextField:self.topBar.searchField.textfieldSearch closeButton:self.buttonCancelSearch];
    self.autoCompleteSearchViewController.baseViewControllerDelegate = self.baseViewControllerDelegate;
    self.topBar.searchField.active = YES;
    [self.view addSubview: self.autoCompleteSearchViewController.view];
    
    [UIView animateWithDuration:0.3 animations:^{
            self.autoCompleteSearchViewController.view.alpha = 1;
    }];
}

#pragma mark - UITextField Delegate methods -

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (self.autoCompleteSearchViewController == nil || self.autoCompleteSearchViewController.view.alpha == 0)
    {
        [self openAutoCompleteSearch:textField];
    }
    else
    {
        [self updateOpenAutoComplete];
    }
}

#pragma mark - AutocompleteteSearchViewControllerDelegate -

-(void) autocompleteteSearch:(APSAutocompleteteSearchViewController*) autocompleteteSearch didSelectAutoComplete:(NSString*)value
{
    if (is_iPad() && self.viewMenu)
    {
        [self.viewMenu shrinkSearchWithCompletion:^{
            
            [self closeAutoCompleteSearchWithCompletion:^{
                
                [self makeSearchWithText:value];

            }];

        }];
    }
    else if (!is_iPad() && self.topRootBar)
    {
        [self.topRootBar hideSearchWithCompletion:^{
            [self closeAutoCompleteSearchWithCompletion:^{
                
               [self makeSearchWithText:value];
                
            }];
        }];
    }
    else
    {
        [self closeAutoCompleteSearchWithCompletion:^{
            
            [self makeSearchWithText:value];
            
        }];
    }
}


-(void)makeSearchWithText:(NSString*)value
{
    if ([self isKindOfClass:[APSSearchViewController class]]) // App is orledy in Search page, dont open new one, used it!
    {
        ASLogInfo(@"Bingo");
        self.topBar.searchField.textfieldSearch.delegate= self;
        self.topBar.searchField.delegate = self;

        APSSearchViewController * reuse = (APSSearchViewController*)self;
        [reuse makeNewSearchWithText:value];
        
    }
    else
    {
        APSBaseBarsViewController * destination = [[APSViewControllersFactory defaultFactory] searchViewControllerWithSearchText:value];
        [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:destination isRoot:NO animated:NO];
    }

}

-(void) autocompleteteSearch:(APSAutocompleteteSearchViewController*) autocompleteteSearch didSelectClose:(id) sender
{
    if (is_iPad() && self.viewMenu)
    {
        [self.viewMenu shrinkSearchWithCompletion:^{
            
            [self closeAutoCompleteSearchWithCompletion:nil];
            
        }];
    }
    else if (!is_iPad() && self.topRootBar)
    {
        
        [self.topRootBar hideSearchWithCompletion:^{
            
            [self closeAutoCompleteSearchWithCompletion:nil];
            
        }];
    
    }
    else
    {
        [self closeAutoCompleteSearchWithCompletion:nil];
    }
}

-(void) closeAutoCompleteSearchWithCompletion:(void(^)(void)) completion
{
    [UIView animateWithDuration:0.3 animations:^{
        self.autoCompleteSearchViewController.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.autoCompleteSearchViewController.textFieldAutoCompleteSearch resignFirstResponder];
        self.topBar.searchField.active = NO;
        [self.autoCompleteSearchViewController.view removeFromSuperview];
        self.autoCompleteSearchViewController = nil;
        self.topBar.searchField.textfieldSearch.delegate = self;
        
        if (completion)
        {
            completion();
        }
    }];
}

-(void)dealloc
{
    [self unregisterToNoInerenetNotifcation];
    [self unregisterToPurePlayerHaveErrorNotificationName];
    self.viewMenu.delegate = nil;
    self.playerBar.delegate = nil;
}

#pragma mark - APSMenuViewDelegate -

-(void) menuView:(APSMenuView *)menu didSelectDisplayPosition:(DisplayPosition) displayPostion
{
    [self hideMenuView];
    
    APSBaseBarsViewController * destinationViewController = nil;
    
    NSString * pageName= @"";
    switch (displayPostion)
    {
        case DisplayPosition_Home:
        {
            [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu = DisplayPosition_Home;
            destinationViewController = [[APSViewControllersFactory defaultFactory] homeViewController];
            pageName = @"Home Page";
        }
            break;
            
        case DisplayPosition_EPG:
        {
            pageName = @"EPG Page";
            destinationViewController = [[APSViewControllersFactory defaultFactory] EPGViewController];
        }
            break;
            
        case DisplayPosition_Favorites:
        {
            destinationViewController = [[APSViewControllersFactory defaultFactory] homeViewController:YES];
            pageName = @"Favorites";
        }
            break;
            
        default:
            break;
    }
    
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Menu" eventAction:@"Open page" eventLabel:pageName];
    [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:destinationViewController isRoot:YES animated:NO];
    
}

-(NSInteger) menuViewNumberOfMenuItems:(APSMenuView *) menu
{
    return 2 + ([[TVSessionManager sharedTVSessionManager] isSignedIn] ? 1 : 0);
}

-(NSString *) menuView:(APSMenuView *) menu titleForIndex:(NSInteger) index
{
    
    NSString * title = nil;
    switch (index)
    {
        case DisplayPosition_Home:
            title = NSLocalizedString(@"HOME",@"");
            break;
            
        case DisplayPosition_EPG:
            title = NSLocalizedString(@"TV GUIDE",@"");
            break;
            
        case DisplayPosition_EPG + 1:
            title = NSLocalizedString(@"FAVORITES",@"");
            break;
            
        default:
            break;
    }
    
    return  [title uppercaseString];
}

-(void) menuView:(APSMenuView *)menu didSelectClose:(UIButton *) button
{
    [self hideMenuView];
}

-(void) menuView:(APSMenuView *)menu didSelectLogin:(UIButton *) button;
{
    [self hideMenuView];
    if (is_iPad())
    {
        [self closeSettingViewWithCompletion:nil];
    }
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
            
            //NOTHING
    } succededBlock:^{
        
    }];
}

-(void) menuView:(APSMenuView *)menu didSelectSearch:( id ) sender
{
    [self hideMenuView];
    [self openAutoCompleteSearch:sender];
}

-(void) menuView:(APSMenuView *)menu didSelectSettings:(UIButton *)button
{
    [self hideMenuView];
    
    if (self.settingsVC == nil || ![self.settingsVC.view isDescendantOfView:[self view]])
    {
        self.settingsVC = (APSSettingsViewController * )[[APSViewControllersFactory defaultFactory] settingsViewController];
    }
    if (is_iPad())
    {
        if (button.selected == NO)
        {
            [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Menu" eventAction:@"Open page" eventLabel:@"Setting Page"];
            [button setSelected:YES];
            [self.settingsVC settingsViewControllerWithCancelBlock:
                                 ^{
                                     [self.settingsVC.view removeFromSuperview];
                                 }
                                 selectBlock:^(APSBaseBarsViewController * viewController)
                                 {
                                    [button setSelected:NO];
                                    [self closeSettingViewWithCompletion:^{
                                         BOOL isRoot = NO;
                                         if ([viewController isKindOfClass:[APSAccountViewController class]])
                                         {
                                             isRoot = YES;
                                         }
                                         
                                         [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:viewController isRoot:isRoot animated:YES];
                                    }];
                                 }];
            [self.settingsVC.view setBackgroundColor:[UIColor clearColor]];
            self.settingsVC.baseViewControllerDelegate = self.baseViewControllerDelegate;
            [self.view addSubview:self.settingsVC.view];
        }
        else
        {
            [button setSelected:NO];
            [self closeSettingViewWithCompletion:nil];
        }
    }
    else
    {
        [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Menu" eventAction:@"Open page" eventLabel:@"Setting Page"];
        [self.baseViewControllerDelegate baseViewControllerMainView:self changeDisplayPosition:self.settingsVC  isRoot:YES animated:NO];
    }
}


- (void)closeSettingViewWithCompletion:(void(^)(void)) completion
{
    UIView * settingsView = self.settingsVC.view;
    settingsView.alpha = 1.0;
    [UIView animateWithDuration:0.3 animations:^{
        settingsView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [settingsView removeFromSuperview];
        if (completion)
        {
            completion();
        }
    }];
}

#pragma mark - show & hide menu iphone only -

-(void) showMenuView
{
    UIView * meinView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    
    self.viewMenu.size = meinView.bounds.size;
    self.viewMenu.origin = CGPointMake(0, 0);
    self.viewMenu.alpha = 0.0;
    [meinView addSubview:self.viewMenu];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                        // self.viewMenu.origin = CGPointZero;
                         self.viewMenu.alpha = 1.0;
                         [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleLightContent];
                     } completion:^(BOOL finished) {
                         
                     }];
    
    
    [self.viewMenu.blurView setUnderlyingView:meinView];
    
}

-(void) hideMenuView
{

    if (!is_iPad())
    {
        self.viewMenu.alpha = 1.0;

        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.viewMenu.alpha = 0.0;
                             [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleDefault];
                             //self.viewMenu.origin = CGPointMake(0, -self.viewMenu.height);
                         } completion:^(BOOL finished) {
                             [self.viewMenu removeFromSuperview];
                         }];
    }
}


-(void) navigationController:(APSNavigationController *) navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self setupTopBar];
    [self setupButtomBar];
    
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    BOOL showButtomBar = [purePlayer isInPlayingProcess];
    [(APSNavigationController *)self.navigationController updateTopBar:self.topBar];
    if (showButtomBar)
    {
        [(APSNavigationController *)self.navigationController updateBottomBar:self.buttomBar];
    }

    APSNetworkStatus status = [[APSNetworkManager  sharedNetworkManager] currentNetworkStatus];
    ASLogInfo(@"%d",status)
    if (status == TVNetworkStatus_NoConnection)
    {
        [self reachabilityChanged:nil];

    }
    else
    {
//        if (self.minimizedScreenNoInternet.isVisibel)
//        {
//             [self reachabilityChanged:nil];
//        }
//        else
//        {
            [(APSNavigationController *)self.navigationController initiateViewWithButtomBar:showButtomBar topBar:self.topBar!=nil];
//        }
    }
}

#pragma mark - orientation

-(BOOL)shouldAutorotate
{
    return  YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    if (is_iPad())
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
    }

}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    
//    if (is_iPad())
//    {
//        return UIInterfaceOrientationLandscapeLeft;
//    }
//    else
//    {
//        return UIInterfaceOrientationPortrait;
//    }
//    
//    
//}


#pragma mark - player bar delegate methods



-(void) playerBar:(APSPlayerBar *) playerbar didSelectClose:(UIButton *) sender
{
    [(APSNavigationController *)self.navigationController hideBottomBarAnimation:YES completion:nil];
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectMaximizeVideo:(UIButton *) sender
{
    [self.baseViewControllerDelegate baseViewControllerMainViewMaximizePlayer:self animated:YES];
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectMinimizeVideo:(UIButton *) sender
{
    
}

-(void) playerBar:(APSPlayerBar *) playerbar didSelectInfoButton:(UIButton *) sender
{
    
}

-(void) playerBar:(APSPlayerBar *) playerbar volumeValueChanged:(CGFloat) value
{
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    [purePlayer setVolume:value];
}

-(CGFloat) playerBarVolume:(APSPlayerBar *) playerbar
{
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    return [purePlayer volume];
}

-(UIView *) playerBarBlursUnderlayingView:(APSPlayerBar *) playerbar
{
  return  [self.baseViewControllerDelegate baseViewControllerMainView:self];
}


#pragma mark - player

-(void) playMediaItem:(TVMediaItem *) mediaItem
        fileFormatKey:(NSString *) formatKey
{
    
    [self showLoginViewControllerIfNeededWithFailedBlock:^{
        
    } succededBlock:^{
        
        if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] != APSLoginStaus_OutOfNetwork)
        {
            [self.baseViewControllerDelegate baseViewControllerMainView:self playMediaItem:mediaItem fileFormatKey:formatKey];
            [self setupButtomBar];
            [(APSNavigationController *)self.navigationController updateBottomBar:self.buttomBar];
            // show bar and in the completion:
            [(APSNavigationController *)self.navigationController showBottomBarAnimation:YES completion:^{
                [self.baseViewControllerDelegate baseViewControllerMainViewMaximizePlayer:self animated:YES];
            }];
        }
        else
        {
            [self showOutOfNetWorkView];
        }

    }];
}


#pragma mark -  APSSearchFieldDelegate -

-(void) searchField:(APSSearchField *)searchField didSelectDeleteAll:(id)sender
{
    [self.autoCompleteSearchViewController cleanSearchResults];
}

#pragma mark - No Internet - 

-(void)registerToNoInerenetNotifcation
{
    [self unregisterToNoInerenetNotifcation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:NotificationName_NetworkStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(sslPinningError:) name:AFNetworkingSSLPinningFailedNotification object:nil];
}

-(void)unregisterToNoInerenetNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationName_NetworkStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AFNetworkingSSLPinningFailedNotification object:nil];

}

-(void)registerToPurePlayerHaveErrorNotificationName
{
    [self unregisterToPurePlayerHaveErrorNotificationName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(purePlayerHaveError:) name:PurePlayerHaveErrorNotificationName object:nil];
}

-(void)unregisterToPurePlayerHaveErrorNotificationName
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PurePlayerHaveErrorNotificationName object:nil];
}

-(void) reachabilityChanged:(NSNotification *) notification
{
    APSNetworkStatus status = [[APSNetworkManager sharedNetworkManager] currentNetworkStatus];
    if (status == TVNetworkStatus_NoConnection)
    {
        if (self.minimizedScreenNoInternet == nil)
        {
            self.minimizedScreenNoInternet = [APSNoInternetView noInternetView];
            self.minimizedScreenNoInternet.delegate = self;
        }
        if (self.minimizedScreenNoInternet.isVisibel)
        {
            return;
        }
        self.buttomBar = self.minimizedScreenNoInternet;
        
        [(APSNavigationController *)self.navigationController updateBottomBar:self.buttomBar];
        
        // show bar and in the completion:
        [(APSNavigationController *)self.navigationController showBottomBarAnimation:YES completion:^{
            
            [self.minimizedScreenNoInternet updateView];
        }];
        self.minimizedScreenNoInternet.isVisibel = YES;

    }
    else
    {
        APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
        BOOL showButtomBar = [purePlayer isInPlayingProcess];
        
        [(APSNavigationController *)self.navigationController updateTopBar:self.topBar];
        if (showButtomBar)
        {
             self.buttomBar = nil;
            [self setupButtomBar];
            [(APSNavigationController *)self.navigationController hideBottomBarAnimation:YES completion:^{

                [(APSNavigationController *)self.navigationController updateBottomBar:self.buttomBar];
                 [(APSNavigationController *)self.navigationController initiateViewWithButtomBar:showButtomBar topBar:self.topBar!=nil];
                [(APSNavigationController *)self.navigationController showBottomBarAnimation:YES completion:^{
                 
                    [purePlayer playMediaAgain];
                    
                }];
            }];
            self.minimizedScreenNoInternet.isVisibel = NO;
        
        }
        else
        {
            [(APSNavigationController *)self.navigationController hideBottomBarAnimation:YES completion:^{
                self.buttomBar = nil;
            }];
            self.minimizedScreenNoInternet.isVisibel = NO;
        }
    }
}

-(void) noInternetView:(APSNoInternetView *) noInternetView didSelectClose:(UIButton *) sender
{
    self.minimizedScreenNoInternet.isVisibel = NO;
    [(APSNavigationController *)self.navigationController hideBottomBarAnimation:YES completion:^{
        self.buttomBar = nil;
    }];
}


-(UIView *) noInternetViewUnderlayingView:(APSNoInternetView *) noInternetView
{
     return  [self.baseViewControllerDelegate baseViewControllerMainView:self];
}

#pragma mark - Out Of Network -

// Dismiss !!!
-(void) fullScreenOutOfNetworkView:(APSFullScreenOutOfNetworkView *) sender dismiss:(UIButton *) buttonSender
{
    [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleDefault];
    [UIView animateWithDuration:0.3 animations:^{
        self.outOfNetworkView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.outOfNetworkView removeFromSuperview];
        
    }];
}

// Show !!!
-(void)showOutOfNetWorkView
{
    // show Astronuts
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",@"OutOfNetwork"]];
    
    UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:self];
    self.outOfNetworkView.frame = CGRectMake(0, 0, mainView.bounds.size.width, mainView.bounds.size.height);
    self.outOfNetworkView.alpha = 0;
    self.outOfNetworkView.delegate = self;
    self.outOfNetworkView.blurView.underlyingView = mainView;
    
    [mainView addSubview:self.outOfNetworkView];
    [self.baseViewControllerDelegate baseViewControllerMainView:nil setStatusBarStyle:UIStatusBarStyleLightContent];

    [UIView animateWithDuration:0.3 animations:^{
        
        self.outOfNetworkView.alpha = 1;
        
    } completion:^(BOOL finished) {
        
    }];

}

#pragma mark - Pure Player Have Error -

-(APSGeneralPlayerErrorView *)generalPlayerErrorView
{
    _generalPlayerErrorView = [APSGeneralPlayerErrorView generalPlayerErrorView];
    [_generalPlayerErrorView registerForNotification];
    _generalPlayerErrorView.delegate = self;
    _generalPlayerErrorView.baseViewControllerDelegate = self.baseViewControllerDelegate;
    return  _generalPlayerErrorView;
}

- (void)sslPinningError:(NSNotification *) notification {
    
    if (self.generalPlayerErrorView.isShow == NO)
    {
        
        NSArray * keys = [NSArray arrayWithObjects:PurePlayerErrorMessageKey,PurePlayerfinalConclusionKey, nil];
        NSArray * values = [NSArray arrayWithObjects:@"",[NSNumber numberWithInteger:FinalConclusion_OtherError], nil];
        NSDictionary * userInfo = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        
        NSNotification *sslNotification = [[NSNotification alloc] initWithName:AFNetworkingSSLPinningFailedNotification object:nil userInfo:userInfo];
        
        [self.generalPlayerErrorView purePlayerHaveError:sslNotification];
    }
    
}

-(void) purePlayerHaveError:(NSNotification *) notification
{
    NSDictionary * userInfo = notification.userInfo;
    if (userInfo)
    {
        NSString * textMessage = [userInfo objectForKey:@"ErrorMessage"];
        if ([textMessage isEqualToString:@"NoInternet"])
        {
            if (self.generalPlayerErrorView.isShow == YES)
            {
                [self.generalPlayerErrorView dismiss:nil];
            }
           // [self reachabilityChanged:nil];
        }
        else
        {
            FinalConclusion finalConclusion = [[userInfo objectForKey:PurePlayerfinalConclusionKey] integerValue];
            NSString * textMessage = [userInfo objectForKey:PurePlayerErrorMessageKey];
            
            if (finalConclusion != FinalConclusion_InSideClientNetwork_But_SecurityIssue) // genera error || content error
            {
                if (self.generalPlayerErrorView.isShow == NO)
                {
                    [self.generalPlayerErrorView purePlayerHaveError:notification];
                }
            }
            else // SecurityIssue
            {
                [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"Player erorr: %@",textMessage]];
                self.generalPlayerErrorView.isShow = NO;
                
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:textMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
                    [alert show];
                } else {
                    APSAlertController *alertController = [APSAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:textMessage preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK",nil) style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }
        }
    }
}

#pragma mark - GeneralPlayerErrorView delegate -

-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *) generalPlayerErrorView didSelectTryPlayAgain:(UIButton *) sender
{
    ASLogInfo(@"generalPlayerErrorView -> didSelectTryPlayAgain");
    APSPurePlayerViewController * purePlayer = [self.baseViewControllerDelegate baseViewControllerPlayer:self];
    BOOL showButtomBar = [purePlayer isInPlayingProcess];
    if (showButtomBar)
    {
        [(APSNavigationController *)self.navigationController showBottomBarAnimation:YES completion:^{
            
            NSString * format =purePlayer.realFileFormat;
            if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] != APSLoginStaus_OutOfNetwork)
            {
                [self.baseViewControllerDelegate baseViewControllerMainView: self playMediaItem:purePlayer.currentMediaItem fileFormatKey:format];
            }
            else
            {
                  [self showOutOfNetWorkView];
            }
        }];
    }
}

-(void) generalPlayerErrorView:(APSGeneralPlayerErrorView *)generalPlayerErrorView didDismiss:(UIButton *)sender
{
    ASLogInfo(@"generalPlayerErrorView -> didDismiss");
    [(APSNavigationController *)self.navigationController hideBottomBarAnimation:YES completion:^{
          
    }];
}

@end
