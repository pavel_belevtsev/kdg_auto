//
//  APSAutocompleteteSearchViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSAutocompleteteSearchViewController.h"
#import "NSTimer+Blocks.h"
#import "APSAutocompleteSearchCell.h"

#define  max_chrcters_no_search 2

@interface APSAutocompleteteSearchViewController ()<UIScrollViewDelegate>

@property (assign, nonatomic) CGRect  originalTableViewFrame;
@end

@implementation APSAutocompleteteSearchViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString * reuseIdentifer = NSStringFromClass([APSAutocompleteSearchCell class]);
    NSString * nibName = [APSAutocompleteSearchCell nibName];
    UINib * nib = [UINib nibWithNibName:nibName bundle:nil];
    [self.collectionViewAutocompleteResults registerNib:nib forCellWithReuseIdentifier:reuseIdentifer];
    [self.labelNoResult setText:NSLocalizedString(@"No results", nil)];
    self.labelNoResult.hidden = YES;
    
    self.labelNoResult.textColor = [APSTypography colorLightGray];
    self.labelNoResult.font = [APSTypography fontRegularWithSize:is_iPad()?31:19];
    self.blurView.dynamic  = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textFieldAutoCompleteSearch becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)cleanSearchResults
{
    self.arraySuggestions = [NSArray array];;
    [self.collectionViewAutocompleteResults reloadData];
}

#pragma mark - UITextField delegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    NSLog(@"Current Locale: %@", [[NSLocale currentLocale] localeIdentifier]);
    NSLog(@"Current language: %@", currentLanguage);
    NSLog(@"primaryLanguage: %@", [UITextInputMode currentInputMode].primaryLanguage);
    if ([[UITextInputMode currentInputMode].primaryLanguage isEqualToString:@"he-IL"])
    {

        [self.textFieldAutoCompleteSearch setTextAlignment:NSTextAlignmentRight];
    }
    else
    {

        [self.textFieldAutoCompleteSearch setTextAlignment:NSTextAlignmentLeft];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField.text == nil || [textField.text isEqualToString:@""])
    {
        [self.buttonCancel sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        NSString * selectedText = self.textFieldAutoCompleteSearch.text;
        self.textFieldAutoCompleteSearch.text = nil;
        [self.delegate autocompleteteSearch:self didSelectAutoComplete:selectedText];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    NSRegularExpression *regex = [[NSRegularExpression alloc]
                                  initWithPattern:@"[א-ת]" options:0 error:NULL];
    
    NSUInteger matches = [regex numberOfMatchesInString:proposedNewString options:0 range:NSMakeRange(0, [string length])];
    
    if (matches > 0)
    {
        [self.textFieldAutoCompleteSearch setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        if ([[UITextInputMode currentInputMode].primaryLanguage isEqualToString:@"he-IL"] == NO)
        {
            [self.textFieldAutoCompleteSearch setTextAlignment:NSTextAlignmentLeft];
        }
        else
        {
            [self.textFieldAutoCompleteSearch setTextAlignment:NSTextAlignmentRight];
        }
    }
    
    if ([proposedNewString length]>max_chrcters_no_search)
    {
        [self autoCompleteSearchListForText:proposedNewString
                           withStartedBlock:^{
                           } completionBlock:^(NSArray *autocompleteArray,NSString * searchText) {
                               
                               if ([self.textFieldAutoCompleteSearch.text length] >max_chrcters_no_search)
                               {
                                   self.arraySuggestions = autocompleteArray;
                                   [self reloadSearchResults:autocompleteArray prefix:searchText];
                               }
                               else
                               {
                                   ASLogInfo(@"text = %@",self.textFieldAutoCompleteSearch.text);
                                   [self cleanSearchResults];
                                   
                                   self.labelNoResult.hidden = YES;
                                   [self textFieldValueChanged:self.textFieldAutoCompleteSearch prefix:@""];
                               }
                           } failedBlock:^{
                               
                           }];
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            [self cleanSearchResults];
            self.labelNoResult.hidden = YES;
            [self textFieldValueChanged:self.textFieldAutoCompleteSearch prefix:@""];
        }];
    }
    
    return YES;
}

- (BOOL)textFieldValueChanged:(UITextField *)textField prefix:(NSString*)prefix
{
    if (![prefix length])
    {
        return NO;
    }
    else
    {
        self.arraySuggestions = [self substringsWithPrefix:prefix];
        
        if (self.arraySuggestions.count>0)
        {
            self.labelNoResult.hidden = YES;
            [self.collectionViewAutocompleteResults reloadData];
            return YES;
        }
        else
        {
            if([textField.text length] > max_chrcters_no_search)
            {
                self.labelNoResult.hidden = NO;
            }
            else
            {
                self.labelNoResult.hidden = YES;
            }
            [self.collectionViewAutocompleteResults reloadData];
            return NO;
        }
    }
}

#pragma mark - searchAutoComplete -

-(void)autoCompleteSearchListForText:(NSString *)text
                    withStartedBlock:(void(^)(void)) started
                     completionBlock:(void(^)(NSArray * itemsList, NSString* searchText))completionBlock
                         failedBlock:(void(^)(void)) failedBlock
{
    if ([self.keyStroke isValid])
    {
        [self.keyStroke invalidate];
    }
    
    self.keyStroke = [NSTimer scheduledTimerWithTimeInterval:0.5 block:^{
        
        if ([text length] > 1)
        {
            __weak NSString* currSearchText = text;
            
            TVPAPIRequest *strongRequest = [TVPMediaAPI requestforGetEPGAutoComplete:currSearchText
                                                                      pageSize:300
                                                                     pageIndex:0
                                                                      delegate:nil];
            
            __weak TVPAPIRequest *request = strongRequest;
            
            [request setStartedBlock:
             ^{
                if (started)
                {
                    started();
                }
            }];
            
            [request setFailedBlock:^{
                if (failedBlock)
                {
                    failedBlock();
                }
                
            }];
            
            [request setCompletionBlock:^{
                
                NSArray *autocompleteArray = [[request JSONResponse] arrayByRemovingNSNulls];
                
                
                if(completionBlock)
                {
                    completionBlock(autocompleteArray,currSearchText);
                }
            }];
           

            [self sendRequest:request];
        }
        
    }repeats:NO];
}

-(void)reloadSearchResults:(NSArray*)autocompleteArray prefix:(NSString*)prefix
{
    
    BOOL showSuggestions = [self textFieldValueChanged:self.textFieldAutoCompleteSearch prefix:prefix];
    
    if (showSuggestions)
    {
       
    }else{
       
    }
    
}

- (NSArray *) substringsWithPrefix:(NSString*)prefix
{
    NSMutableArray *tmpArray = [NSMutableArray array];
    NSRange range;
    range.location=NSNotFound;
    
    for (NSString *tmpString in self.arraySuggestions)
    {
//        if([[tmpString lowercaseString] hasPrefix:[prefix lowercaseString]]) // Search result opens the desired word
//        {
//            range = [[tmpString lowercaseString] rangeOfString:[prefix lowercaseString] options:NSCaseInsensitiveSearch];
//        }
//        else // Requested word is in mid-sentence
//        {
            range = [[tmpString lowercaseString] rangeOfString:[NSString stringWithFormat:@"%@",[prefix lowercaseString]] options:NSCaseInsensitiveSearch];
//        }
        
        if (range.location != NSNotFound)
        {
            [tmpArray addObject:tmpString];
        }
        range.location=NSNotFound;

    }
    if ([tmpArray count]>0)
    {
        return tmpArray;
    }
    
    return nil;
}



#pragma mark - CollectionView -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arraySuggestions.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * reuseIdentifer = NSStringFromClass([APSAutocompleteSearchCell class]);
    APSAutocompleteSearchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifer forIndexPath:indexPath];
    
    
    if (is_iPad())
    {
        cell.width = self.collectionViewAutocompleteResults.width-20;
        cell.labelText.width =cell.width;
    }
    
    //NSRegularExpression *regex = [[[NSRegularExpression alloc]
    //                               initWithPattern:@"[א-ת]" options:0 error:NULL] autorelease]; // !!!
    
    NSRegularExpression *regex = [[NSRegularExpression alloc]
                                   initWithPattern:@"[א-ת]" options:0 error:NULL];
                                   
    NSString * text = [self.arraySuggestions objectAtIndex:indexPath.row];
    NSUInteger matches = [regex numberOfMatchesInString:text options:0 range:NSMakeRange(0, [text length])];
    
    if (matches > 0)
    {
        [cell.labelText setTextAlignment:NSTextAlignmentLeft];
    }
    else
    {
        [cell.labelText setTextAlignment:NSTextAlignmentLeft];
    }
    
    cell.labelText.adjustsFontSizeToFitWidth = NO;
    
    
    NSMutableAttributedString *st = [[NSMutableAttributedString alloc] initWithString:text];
//    NSRange boldRange = [[st string] rangeOfString:self.textFieldAutoCompleteSearch.text options:NSCaseInsensitiveSearch];
    NSRange otherRange = [[st string] rangeOfString:[st string]];
    [st addAttribute:NSForegroundColorAttributeName value:[APSTypography colorLightGray] range:otherRange];
//    [st addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:boldRange];
//    [st addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:cell.labelText.font.pointSize] range:boldRange];

    // mark all AutoComplete text in paragraph.
    NSRange searchRange = NSMakeRange(0,text.length);
    NSRange foundRange;
    while (searchRange.location < text.length)
    {
        searchRange.length = text.length-searchRange.location;
        foundRange = [text rangeOfString:self.textFieldAutoCompleteSearch.text options:NSCaseInsensitiveSearch range:searchRange];
        if (foundRange.location != NSNotFound)
        {
            [st addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:foundRange];
            [st addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:cell.labelText.font.pointSize] range:foundRange];
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
        }
        else
        {
            // no more substring to find
            break;
        }
    }
    
    cell.labelText.attributedText = st;
    return cell;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [APSAutocompleteSearchCell cellSize];
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * selectedText = self.arraySuggestions[indexPath.row];
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"Search" eventAction:@"Autocomplete result chosen" eventLabel:selectedText];
    self.textFieldAutoCompleteSearch.text = nil;
    [self.delegate autocompleteteSearch:self didSelectAutoComplete:selectedText];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return [APSAutocompleteSearchCell tableEdgeInsets];
}

#pragma mark - UICollectionViewDelegate


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSAutocompleteSearchCell cellEdgeInsets].top;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [APSAutocompleteSearchCell cellEdgeInsets].left;
}

#pragma mark - out actions


-(void) updateSearchTextField:(UITextField *) textField closeButton:(UIButton *) closeButton
{
    self.buttonCancel = closeButton;
    self.textFieldAutoCompleteSearch = textField;
    self.arraySuggestions = nil;
    [self.collectionViewAutocompleteResults reloadData];
    
    [self.textFieldAutoCompleteSearch becomeFirstResponder];
    
}

- (IBAction)close:(id)sender
{
    [self.textFieldAutoCompleteSearch setText:nil];
    [self.textFieldAutoCompleteSearch endEditing:YES];
    [self.delegate autocompleteteSearch:self didSelectClose:sender];
}

-(void)setButtonCancel:(UIButton *)buttonCancel
{
    if (_buttonCancel != buttonCancel)
    {
        [_buttonCancel removeTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        [buttonCancel addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
        _buttonCancel = buttonCancel;
    }
}


-(void)setTextFieldAutoCompleteSearch:(UITextField *)textFieldAutoCompleteSearch
{
//    if (_textFieldAutoCompleteSearch != textFieldAutoCompleteSearch)
//    {
        _textFieldAutoCompleteSearch.delegate = nil;
        textFieldAutoCompleteSearch.delegate = self;
        _textFieldAutoCompleteSearch = textFieldAutoCompleteSearch;
//    }

}


-(void)dealloc
{
    self.buttonCancel = nil;
}



#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.textFieldAutoCompleteSearch resignFirstResponder];
}

@end
