//
//  APSAutocompleteteSearchViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/20/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"
#import "FXBlurView.h"


@class APSAutocompleteteSearchViewController;

@protocol AutocompleteteSearchViewControllerDelegate <NSObject>

-(void) autocompleteteSearch:(APSAutocompleteteSearchViewController*) autocompleteteSearch didSelectAutoComplete:(NSString*)value;
-(void) autocompleteteSearch:(APSAutocompleteteSearchViewController*) autocompleteteSearch didSelectClose:(id) sender;

@end


@interface APSAutocompleteteSearchViewController : APSBaseViewController<UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (assign, nonatomic) id<AutocompleteteSearchViewControllerDelegate>delegate;

@property (retain, nonatomic) IBOutlet UICollectionView *collectionViewAutocompleteResults;
@property (retain, nonatomic) NSArray * arraySuggestions;
@property (nonatomic, strong) NSTimer *keyStroke;
@property (retain, nonatomic) IBOutlet FXBlurView * blurView;

@property (retain, nonatomic) UIButton * buttonCancel;
@property (retain, nonatomic) IBOutlet UITextField *textFieldAutoCompleteSearch;
@property (retain, nonatomic) IBOutlet UILabel * labelNoResult;


-(void) updateSearchTextField:(UITextField *) textField closeButton:(UIButton *) closeButton;
-(void)cleanSearchResults;
- (IBAction)close:(id)sender;

 

@end
