//
//  APSMainViewController_iPad.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/14/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMainViewController_iPad.h"
#import "APSPurePlayerViewController.h"
#import "APSNavigationController.h"
#import "APSPlayerBar.h"
#import "APSFullScreenPlayerViewController.h"

@interface APSMainViewController_iPad ()


@end

@implementation APSMainViewController_iPad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) showMenuView
{
    
}

-(void) hideMenuView
{
    
}



#pragma mark - orientations

-(NSUInteger)supportedInterfaceOrientations
{
    if (self.mainNavigationViewController)
    {
        return [self.mainNavigationViewController supportedInterfaceOrientations];
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}




-(void)  baseViewControllerMainViewMinimizePlayer:(APSBaseViewController *) baseViewController animated:(BOOL)animated
{
    
    if (self.purePlayViewController.isInFullScreen == YES)
    {
        if (self.isInMinimizeAnimation)
        {
            return;
        }
        
        self.isInMinimizeAnimation = YES;
        self.purePlayViewController.isInInsertionProcess = YES;
        CGRect convertedBigRect = [self.view convertRect:self.purePlayViewController.view.frame fromView:self.purePlayViewController.view.superview];
        self.purePlayViewController.view.frame = convertedBigRect;
        
        ((APSFullScreenPlayerViewController *) baseViewController).minimizeBlock = ^(UIViewController * sourceViewController)
        {
            [self.view addSubview:self.purePlayViewController.view];
        };
        
        // animate pure view controller from and in the completion:
        [self dismissViewControllerAnimated:NO completion:^{
            
            APSBaseBarsViewController * baseBarsViewController = (APSBaseBarsViewController *)self.mainNavigationViewController.topViewController;
            CGRect convertedSmallRect = [self.view convertRect:baseBarsViewController.playerBar.viewVideoPlaceHolder.frame fromView:baseBarsViewController.playerBar.viewVideoPlaceHolder.superview];
            [UIView animateWithDuration:animated?0.3:0 animations:^{
                self.purePlayViewController.view.frame = convertedSmallRect;
            } completion:^(BOOL finished) {
                
                self.purePlayViewController.view.frame = CGRectMake(0, 0, baseBarsViewController.playerBar.viewVideoPlaceHolder.size.width, baseBarsViewController.playerBar.viewVideoPlaceHolder.size.height);
                [baseBarsViewController.playerBar.viewVideoPlaceHolder addSubview:self.purePlayViewController.view];
                self.purePlayViewController.isInInsertionProcess = NO;
                self.purePlayViewController.isInFullScreen = NO;
                self.isInMinimizeAnimation = NO;
                
                [self displayToolTip];
                
            }];
        }];
        
    }
}

-(void)  baseViewControllerMainViewMaximizePlayer:(APSBaseBarsViewController *) senderBaseBarsViewController animated:(BOOL)animated
{
    
    if (self.purePlayViewController.isInFullScreen == NO)
    {
        if (self.isInMaximizeAnimation)
        {
            return;
        }
        
        self.isInMaximizeAnimation = YES;
        CGRect convertedRect = [self.view convertRect:self.purePlayViewController.view.frame fromView:self.purePlayViewController.view.superview];
        self.purePlayViewController.view.frame = convertedRect;
        
        [self.view addSubview:self.purePlayViewController.view];
        
        [UIView animateWithDuration:animated?0.3:0 animations:^{
            self.purePlayViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
        } completion:^(BOOL finished) {
            APSFullScreenPlayerViewController  * fullScreenPlayerViewController = (APSFullScreenPlayerViewController *)[[APSViewControllersFactory defaultFactory] fullScreenPlayerViewControllerWithPurePlayer:self.purePlayViewController];
            fullScreenPlayerViewController.baseViewControllerDelegate = self;
            fullScreenPlayerViewController.maximizeBlock = ^(UIViewController * destinationViewController){
                
                self.purePlayViewController.view.frame = CGRectMake(0, 0, destinationViewController.view.bounds.size.width, destinationViewController.view.bounds.size.height);
                [destinationViewController.view insertSubview:self.purePlayViewController.view atIndex:0];
            };
            
            [self presentViewController:fullScreenPlayerViewController animated:NO completion:nil];
            
            self.purePlayViewController.isInFullScreen = YES;
            self.isInMaximizeAnimation = NO;
        }];
    }
}

@end
