//
//  APSViewController.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMainViewController.h"
#import "APSBaseViewController.h"
#import "APSLaunchProcess.h"
#import "APSSearchViewController.h"
#import "APSSplash.h"
#import "APSSettingsViewController.h"
#import "APSNavigationController.h"
#import "APSLoginViewController.h"
#import "APSHomeViewController.h"
#import "APSEPGViewController.h"
#import "APSPurePlayerViewController.h"
#import "APSSessionAdapter.h"
#import "APSForceDownload.h"
#import "APSSessionManager.h"
#import "APSNetworkManager.h"
#import "APSNoInternetFullScreen.h"
#import "APSSSLPinningManager.h"

#import "APSTooltip.h"

#define kBarTooltipWasDisplayed @"kBarTooltipWasDisplayed"
@interface APSMainViewController ()<noInternetFullScreenDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewControllersPlaceHolder;
@property (retain, nonatomic) APSLaunchProcess * launchProcess;
@property (nonatomic ,strong) APSSessionAdapter * sessionAdapter;
@property (strong, nonatomic) APSNoInternetFullScreen * noInternetFullScreen;
@property (strong, nonatomic) APSSplash * splashView;
@property BOOL  hideStatusBar;
@property BOOL isInLoginProsses;
@property (strong, nonatomic) NSString * currInternetServer;

@end

@implementation APSMainViewController

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return self.statusBarTextColorStyle;
}

- (BOOL)prefersStatusBarHidden
{
    return self.hideStatusBar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [APSSessionManager sharedAPSSessionManager]; // init to used APSSessionManager!

	// Do any additional setup after loading the view, typically from a nib.
    self.hideStatusBar = YES;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }

   // self.view.window.backgroundColor = [UIColor orangeColor];
    self.statusBarTextColorStyle = UIStatusBarStyleDefault;
    
    self.launchProcess = [APSLaunchProcess launchProcess];
    
    self.splashView = [APSSplash splashViewWithFrame:self.view.frame];
    
    [self startApp];
}

-(void)startApp
{
    
    [self.launchProcess startLaunchingAppWithStartBlock:^{
        
        [self.view addSubview:self.splashView];
        
    } failedBlock:^(APSLaunchFailedReason reason){
        
        self.hideStatusBar = NO;
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
        {
            [self setNeedsStatusBarAppearanceUpdate];
        }
        
        APSNetworkStatus status = [[APSNetworkManager  sharedNetworkManager] currentNetworkStatus];
        ASLogInfo(@"%d",status)
        if ((status == TVNetworkStatus_NoConnection) || [APSSSLPinningManager manager].SSLFailed)
        {
            self.noInternetFullScreen = [APSNoInternetFullScreen noInternetFullScreen];
            self.noInternetFullScreen.delegate = self;
            self.noInternetFullScreen.frame = self.splashView.frame;
            [self.view addSubview:self.noInternetFullScreen];
        }
        
        [self.splashView removeFromSuperview];
        
    } completionBlock:^{
        
        [[APSNetworkManager  sharedNetworkManager] trackNetworkChanges];
        APSNetworkStatus status = [[APSNetworkManager  sharedNetworkManager] currentNetworkStatus];
        ASLogInfo(@"%d",status)
        [self registerToNoInerenetNotifcation];
        self.currInternetServer = nil;
        self.hideStatusBar = NO;
        if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
        {
            [self setNeedsStatusBarAppearanceUpdate];
        }
        [self.splashView removeFromSuperview];
        
        if (![[APSForceDownload sharedForceDownload] isApplicationContinuencePermitted])
        {
            [[APSForceDownload sharedForceDownload] handleForceDownload:self];
        }
        else
        {
            [self startLoginProcess];
            
            [self registerToAppNotifactions];
            
            //Hide Splash and start building application
            [self loadAllDisplayPositionsViewControllers];
            
            [APSLayoutManager sharedLayoutManager].lastSelectedTabInMenu = DisplayPosition_Home;
            
            APSBaseBarsViewController * homeViewController = [[APSViewControllersFactory defaultFactory] homeViewController];
            [self changeDisplayPosition:homeViewController isRoot:YES animated:NO];
        }
    }];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    [[APSSSLPinningManager manager] goToAppStoreAndExit];

}


-(void)registerToAppNotifactions
{
    [self registerToAppDidBecomeActiveNotification];
    [self registerToUserLoginStausUpdateNotification];
    [self registerToChangeNetwortServerNotification];
}




-(void) startLoginProcess
{
    NSLog(@"- - - > startLoginProcess < - - -");
    NSString * userName = [GCUserDomain  getUserName];
    NSString * password =[GCUserDomain getUserPassword];
//    NSLog(@"startLoginProcess %@,%@",userName,password);
    
    self.sessionAdapter = [[APSSessionAdapter alloc] init];
    [self.sessionAdapter setAssertBlock];
    self.sessionAdapter.canChangeUserLoginStatus = NO;
    
    if ((userName.length == 0 || password.length == 0) )
    {
        [self.sessionAdapter logoutWithSuccessBlock:^{
            
            NSLog(@"log out succeded");
        } failedBlock:^(NSString *failedError) {
            NSLog(@"log out failed");
        }];
        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];;
    }
    else
    {
        [self.sessionAdapter ssoLoginWithUserName:userName password:password successBlock:^{
            
            NSLog(@"- - - > Login Process Success < - - -");
//            NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys]);
//            NSLog(@"%@", [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allValues]);

            
        } failedBlock:^(NSString *failedError) {
            
            NSLog(@"- - - > Login Process Failed < - - -");
            if (([failedError isEqualToString:NSLocalizedString(@"Wrong user name or password", nil)] == YES) || ([failedError isEqualToString: NSLocalizedString(@"CustomErorr_1", nil)]  == YES) || ([failedError isEqualToString:NSLocalizedString(@"CustomErorr_2", nil)]  == YES))
                
            {
                NSLog(@"Login Failed -> <- Wrong user name or password");
                
                [self.sessionAdapter logoutWithSuccessBlock:^{
                    
                } failedBlock:^(NSString *failedError) {
                    
                }];
                [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];
            }
            else
            {
                NSLog(@"Login Failed -> Unknown Error");
                [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Login];
            }
            
        } andRgisterDeviceBlock:^{
            
            [self.sessionAdapter registerDeviceToDomain];
            
        }];
    }

   
  
}

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController setStatusBarStyle:(UIStatusBarStyle) style;
{
    self.statusBarTextColorStyle = style;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View Controllers manament  -

-(void) loadAllDisplayPositionsViewControllers
{
    // loadAuto complete search view controller
    self.mainNavigationViewController = [[APSNavigationController alloc] init];
    self.mainNavigationViewController.topBar = self.topBar;
    self.mainNavigationViewController.buttomBar = self.buttomBar;
    self.mainNavigationViewController.middleView = self.viewControllersPlaceHolder;
    self.mainNavigationViewController.navigationBarHidden =YES;
    self.mainNavigationViewController.view.origin = CGPointZero;
    self.mainNavigationViewController.view.size = self.viewControllersPlaceHolder.size;
    self.mainNavigationViewController.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
    self.mainNavigationViewController.view.clipsToBounds = NO;
    
    for (UIView * view in self.mainNavigationViewController.view.subviews)
    {
        [view setClipsToBounds:NO];
    }
    
    [self.viewControllersPlaceHolder addSubview:self.mainNavigationViewController.view];
    self.purePlayViewController = (APSPurePlayerViewController *)[[APSViewControllersFactory defaultFactory] purePlayerViewController];
                                   
}


#pragma mark - APSPlayerBarDelegate

-(void) hidePlayerBar
{
    self.buttomBar.origin = CGPointMake(self.buttomBar.x,self.view.height);
    self.viewControllersPlaceHolder.size = CGSizeMake(self.viewControllersPlaceHolder.width,self.view.bounds.size.height-(self.topBar.origin.y+self.topBar.size.height));

}

-(void) showPlayerBar
{
    self.buttomBar.origin = CGPointMake(self.buttomBar.x,self.view.height-self.buttomBar.height);
    self.viewControllersPlaceHolder.size = CGSizeMake(self.viewControllersPlaceHolder.width,self.view.bounds.size.height-(self.topBar.origin.y+self.topBar.size.height)-self.buttomBar.size.height);

}



#pragma mark - Tabs 
-(void) changeDisplayPosition:(APSBaseBarsViewController *) viewController isRoot:(BOOL)isRoot animated:(BOOL)animated
{
    APSBaseBarsViewController * destinationViewController = viewController;
    destinationViewController.managedObjectContext = self.managedObjectContext;
    destinationViewController.baseViewControllerDelegate = self;
    
    if (animated)
    {
        [self.mainNavigationViewController pushViewController:viewController animated:animated];
    }
    else
    {
        self.mainNavigationViewController.viewControllers = isRoot?[NSArray arrayWithObject:viewController]:[self.mainNavigationViewController.viewControllers arrayByAddingObject:viewController];
    }
}



#pragma  mark - BaseViewControllerDelegate

-(UIView *) baseViewControllerTopBar:(APSBaseViewController *) viewcontrollers
{
    return self.topBar;
}

-(UIView *) baseViewControllerButtomBar:(APSBaseViewController *) viewcontrollers
{
    return self.buttomBar;
}

-(void) baseViewController:(APSBaseViewController *) baseViewController backToPrevDisplayPosition:(id) sender
{
    [self.mainNavigationViewController popViewControllerAnimated:NO];
}


-(UIView *) baseViewControllerMainView:(APSBaseViewController *) baseViewController
{
    return self.view;
}

-(UIViewController *) baseViewControllerMainViewController:(APSBaseViewController *) baseViewController
{
    return self;
}

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController  changeDisplayPosition:(APSBaseBarsViewController *) viewController isRoot:(BOOL)isRoot animated:(BOOL)animated
{
    [self changeDisplayPosition:(APSBaseBarsViewController *)viewController isRoot:isRoot animated:animated];
}

-(void)  baseViewControllerMainView:(APSBaseViewController *) baseViewController playMediaItem:(TVMediaItem *) mediaItem fileFormatKey:(NSString *) formatKey
{
    // start animation from bar to full screen
    [self.purePlayViewController playMediaItem:mediaItem fileFormatKey:formatKey startOffset:0 secured:YES];
    
}


-(APSPurePlayerViewController *) baseViewControllerPlayer:(APSBaseViewController *) viewcontrollers
{
    return self.purePlayViewController;
}


-(UIViewController *) baseViewControllerLastViewController
{
    return [[self.mainNavigationViewController viewControllers] lastObject];
}


-(void)  baseViewControllerMainViewMinimizePlayer:(APSBaseViewController *) baseViewController animated:(BOOL)animated
{
    // animate pure view controller from and in the completion:
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

-(void)  baseViewControllerMainViewMaximizePlayer:(APSBaseBarsViewController *) senderBaseBarsViewController animated:(BOOL)animated
{
   // implemented on sons
}

#pragma mark - interface orientation -

-(BOOL)shouldAutorotate
{
    if (self.mainNavigationViewController)
    {
        return  [self.mainNavigationViewController shouldAutorotate];
    }
    else
    {
        return YES;
    }

}

#pragma mark - App Did Become Active Notification -

-(void)registerToAppDidBecomeActiveNotification
{
    [self unRegisterToAppDidBecomeActiveNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)unRegisterToAppDidBecomeActiveNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}




-(void) appDidBecomeActive:(NSNotification *) notification
{
    if ([[APSNetworkManager sharedNetworkManager] currentNetworkStatus] != TVNetworkStatus_NoConnection)
    {
        [self startLoginProcess];
    }
}

#pragma mark - Memory -

-(void)dealloc
{
    [self unRegisterToAppDidBecomeActiveNotification];
    [self unRegisterToUserLoginStausUpdateNotification];
    [self unRegisterToChangeNetwortServerNotification];
    [self unregisterToNoInerenetNotifcation];
}

#pragma mark - No internet full screen delegate -

-(void) noInternetFullScreen:(APSNoInternetFullScreen *) programFullView didSelectReload:(UIButton*)sender
{
    self.hideStatusBar = YES;
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    [self.noInternetFullScreen removeFromSuperview];
    [self startApp];
}

#pragma mark - APSSessionManager UserLoginStausUpdate Notification -

-(void)registerToUserLoginStausUpdateNotification
{
    [self unRegisterToUserLoginStausUpdateNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePlayer:)
                                                 name:APSSessionManagerUserLoginStausUpdateNotification object:nil];
}

-(void)unRegisterToUserLoginStausUpdateNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:APSSessionManagerUserLoginStausUpdateNotification
                                                  object:nil];
}

-(void) updatePlayer:(NSNotification *) notification
{
    if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] != APSLoginStaus_Login)
    {
        [self.purePlayViewController stopPlayingAfterOutOfNetwork];
    }
    
    else if ([[APSSessionManager sharedAPSSessionManager] userLoginStaus] == APSLoginStaus_Login)
    {
#if !TARGET_IPHONE_SIMULATOR
        NSDictionary *dic = notification.userInfo;
        APSLoginStaus oldStatus = [[dic objectForKey:APSSessionManagerOldUserLoginStaus] integerValue];
        APSLoginStaus NewStatus = [[dic objectForKey:APSSessionManagerNewUserLoginStaus] integerValue];
        if ([self.purePlayViewController isInPlayingProcess] && [self.purePlayViewController status] == TVPPlaybackStateStopped)
        {
            [self.purePlayViewController playMediaAfterIntsetToNetwork];
        }
#endif
    }
}

#pragma mark - ChangeNetwortServer Notification -

-(void)registerToChangeNetwortServerNotification
{
    [self unRegisterToChangeNetwortServerNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appChangeNetwortServer:)
                                                 name:APSChangeNetwortServerNotification object:nil];
}

-(void)unRegisterToChangeNetwortServerNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:APSChangeNetwortServerNotification
                                                  object:nil];
}

-(void) appChangeNetwortServer:(NSNotification *) notification
{
    if ([[APSNetworkManager sharedNetworkManager] currentNetworkStatus] != TVNetworkStatus_NoConnection)
    {
        NSDictionary * userInfo = notification.userInfo;
        if (userInfo)
        {
            NSString * server = [userInfo objectForKey:@"SSID"];
            if (server)
            {
                if ([server isEqualToString:self.currInternetServer] == NO)
                {
                    self.currInternetServer = server;
                    ASLogInfo(@"server = %@",server);
                    [self startLoginProcess];
                }
            }
        }
    }
}

#pragma mark - No Internet -

-(void)registerToNoInerenetNotifcation
{
    [self unregisterToNoInerenetNotifcation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:NotificationName_NetworkStatusChanged object:nil];
}

-(void)unregisterToNoInerenetNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NotificationName_NetworkStatusChanged object:nil];
}

-(void) reachabilityChanged:(NSNotification *) notification
{
    self.currInternetServer = nil;
    [self startLoginProcess];
}

- (void) displayToolTip
{
    BOOL shouldDisplayTooltip = ![[[NSUserDefaults standardUserDefaults] objectForKey:kBarTooltipWasDisplayed] boolValue];
//    shouldDisplayTooltip = YES;
    if (shouldDisplayTooltip) {
        
        // save the flag in NSUserDefaults:
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kBarTooltipWasDisplayed];
        [[NSUserDefaults standardUserDefaults] synchronize];

        APSTooltip* tooltip = [APSTooltip toolTip];
        tooltip.labelTooltip.text = NSLocalizedString(@"Tap here to return to player mode.", nil);
        if (is_iPad()) {
            [tooltip showToolTipOnView:self.view atOrigin:CGPointMake(130,690) withCompletion:nil];

        }else{

            CGFloat y = self.buttomBar.y;
            y -=6;
            
            [tooltip showToolTipOnView:self.view atOrigin:CGPointMake(38,y)  withCompletion:nil];
        }
    }
}

@end



