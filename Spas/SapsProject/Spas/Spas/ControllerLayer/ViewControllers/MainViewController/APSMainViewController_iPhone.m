//
//  APSMainViewController_iPhone.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/14/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMainViewController_iPhone.h"
#import "APSMenuView.h"
#import "FXBlurView.h"
#import "APSPurePlayerViewController.h"
#import "APSPlayerBar.h"
#import "APSFullScreenPlayerViewController.h"
#import "APSAlertController.h"

@interface APSMainViewController_iPhone ()

@end

@implementation APSMainViewController_iPhone

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedRotaet:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)  baseViewControllerMainViewMinimizePlayer:(APSBaseViewController *) baseViewController animated:(BOOL)animated
{
    
    
    if (self.purePlayViewController.isInFullScreen == YES)
    {
        APSFullScreenPlayerViewController * VC =(APSFullScreenPlayerViewController *) baseViewController;
        
        if ([VC isKindOfClass:[APSFullScreenPlayerViewController class]] && ([VC respondsToSelector:@selector(isShowGeneralError)]) && [VC isShowGeneralError]) // if show  General Error not allow to Minimize Player.
        {
            return;
        }
        
        self.purePlayViewController.isInInsertionProcess = YES;
        if(self.isInMinimizeAnimation)
        {
            return;
        }
        
        self.isInMinimizeAnimation = YES;
    
        ((APSFullScreenPlayerViewController *) baseViewController).minimizeBlock = ^(UIViewController * viewcontroller)
        {
            self.purePlayViewController.view.transform = CGAffineTransformMakeRotation(M_PI_2);
            self.purePlayViewController.view.bounds = CGRectMake(0, 0, self.view.bounds.size.height,self.view.bounds.size.width);
            self.purePlayViewController.view.center = CGPointMake(self.view.size.width/2, self.view.size.height/2);
            self.purePlayViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width,  self.view.bounds.size.height);
            [self.view addSubview:self.purePlayViewController.view];
        
        };
        self.purePlayViewController.view.backgroundColor = [UIColor clearColor];
    
        // animate pure view controller from and in the completion:
        [self dismissViewControllerAnimated:NO completion:^{
        
            APSBaseBarsViewController * baseBarsViewController = (APSBaseBarsViewController *)[self.mainNavigationViewController topViewController];
            CGRect convertedSmallRect = [self.view convertRect:baseBarsViewController.playerBar.viewVideoPlaceHolder.frame fromView:baseBarsViewController.playerBar.viewVideoPlaceHolder.superview];
            [UIView animateWithDuration:animated?0.3:0 animations:^{
                self.purePlayViewController.view.transform = CGAffineTransformMakeRotation(0);
                self.purePlayViewController.view.frame = convertedSmallRect;
            } completion:^(BOOL finished) {
            
                self.purePlayViewController.view.frame = CGRectMake(0, 0, baseBarsViewController.playerBar.viewVideoPlaceHolder.size.width, baseBarsViewController.playerBar.viewVideoPlaceHolder.size.height);
                [baseBarsViewController.playerBar.viewVideoPlaceHolder addSubview:self.purePlayViewController.view];
                self.purePlayViewController.view.backgroundColor = [UIColor blackColor];
                self.purePlayViewController.isInInsertionProcess = NO;
                self.purePlayViewController.isInFullScreen =NO;;
                self.isInMinimizeAnimation = NO;
                
                [self displayToolTip];

            }];
    }];
    }
    
}

-(void)  baseViewControllerMainViewMaximizePlayer:(APSBaseBarsViewController *) senderBaseBarsViewController animated:(BOOL)animated
{
    
    if (self.purePlayViewController.isInFullScreen == NO && self.purePlayViewController.isInPlayingProcess)
    {

        if (self.isInMaximizeAnimation)
        {
            return;
        }
        
        if (senderBaseBarsViewController.presentedViewController && [senderBaseBarsViewController.presentedViewController isKindOfClass:[APSAlertController class]]) {
            
            [senderBaseBarsViewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            
        }
        
        self.isInMaximizeAnimation = YES;
        CGRect convertedRect = [self.view convertRect:self.purePlayViewController.view.frame fromView:self.purePlayViewController.view.superview];
        self.purePlayViewController.view.frame = convertedRect;
        self.purePlayViewController.view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.purePlayViewController.view];
    
        [UIView animateWithDuration:animated?0.3:0 animations:^{
        //pay attention: this is a very importent steps to be one after another
            
            UIDeviceOrientation deviceOrintation = [[UIDevice currentDevice] orientation];
            if(deviceOrintation == UIDeviceOrientationLandscapeRight)
            {
                    self.purePlayViewController.view.transform = CGAffineTransformMakeRotation(-M_PI_2);
            }
            else
            {
                    self.purePlayViewController.view.transform = CGAffineTransformMakeRotation(M_PI_2);
            }
                
            self.purePlayViewController.view.bounds = CGRectMake(0, 0, self.view.bounds.size.height,self.view.bounds.size.width);
            self.purePlayViewController.view.center = CGPointMake(self.view.size.width/2, self.view.size.height/2);
            self.purePlayViewController.view.frame = CGRectMake(0, 0, self.view.bounds.size.width,  self.view.bounds.size.height);

        } completion:^(BOOL finished) {
            APSFullScreenPlayerViewController * baseBarsViewControllers = (APSFullScreenPlayerViewController *)[[APSViewControllersFactory defaultFactory] fullScreenPlayerViewControllerWithPurePlayer:self.purePlayViewController];
            baseBarsViewControllers.maximizeBlock = ^(UIViewController * viewcontroller)
            {
                self.purePlayViewController.view.transform = CGAffineTransformMakeRotation(0);
                self.purePlayViewController.view.frame = CGRectMake(0, 0, viewcontroller.view.bounds.size.width, viewcontroller.view.bounds.size.height);
                [viewcontroller.view insertSubview:self.purePlayViewController.view atIndex:0];
            };
            baseBarsViewControllers.baseViewControllerDelegate = self;
            self.purePlayViewController.view.backgroundColor = [UIColor blackColor];
            [self presentViewController:baseBarsViewControllers animated:NO completion:nil];
            self.purePlayViewController.isInFullScreen =YES;
            self.isInMaximizeAnimation = NO;
        }];
    }
}

#pragma mark - orientation changed
-(void) receivedRotaet:(NSNotification *) notfication
{
    
    UIViewController * presentedViewController = self.presentedViewController!=nil?self.presentedViewController:self.mainNavigationViewController.topViewController;
    
    UIDeviceOrientation toInterfaceOrientation = [[UIDevice currentDevice] orientation];
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        
        [self baseViewControllerMainViewMaximizePlayer:(APSBaseViewController *)presentedViewController animated:YES];
        
    }
    else if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
       
            [self baseViewControllerMainViewMinimizePlayer:(APSBaseViewController *)presentedViewController animated:YES];
        
    }
}



#pragma mark - orientation

-(NSUInteger)supportedInterfaceOrientations
{
    if (self.mainNavigationViewController)
    {
        return [self.mainNavigationViewController supportedInterfaceOrientations];
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end
