//
//  APSViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSBaseViewController.h"





typedef enum DisplayPosition
{
    DisplayPosition_None = -1,
    DisplayPosition_Home = 0,
    DisplayPosition_EPG,
    DisplayPosition_Player,
    DisplayPosition_Search,
    DisplayPosition_Setting,
    DisplayPosition_Favorites
} DisplayPosition;

@class APSMenuView;
@class APSPurePlayerViewController;
@class APSNavigationController;

@interface APSMainViewController : APSBaseViewController <BaseViewControllerDelegate>

@property (weak, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (retain, nonatomic) IBOutlet UIView * topBar;
@property (retain, nonatomic) IBOutlet UIView * buttomBar;
@property (retain, nonatomic) IBOutlet APSMenuView *viewMenu;
@property (retain, nonatomic) IBOutlet APSPurePlayerViewController * purePlayViewController;
@property (retain, nonatomic) APSNavigationController * mainNavigationViewController;


@property (assign, nonatomic) BOOL isInMaximizeAnimation;
@property (assign, nonatomic) BOOL isInMinimizeAnimation;


- (void) displayToolTip;






@end
