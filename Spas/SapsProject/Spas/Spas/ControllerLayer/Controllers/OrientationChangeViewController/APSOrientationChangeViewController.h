//
//  APSOrientationChangeViewController.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/12/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseViewController.h"

@interface APSOrientationChangeViewController : APSBaseViewController

@property (assign, nonatomic) UIInterfaceOrientation preferedorientation;
@property (assign, nonatomic) NSUInteger  supportedOrientations;


@end
