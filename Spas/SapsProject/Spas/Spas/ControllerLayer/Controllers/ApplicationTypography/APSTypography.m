//
//  APSTypography.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/22/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//





#define BRAND_COLOR              [UIColor colorWithRed:247.0/255.0 green:200.0/255.0 blue:50.0/255.0 alpha:1]
//#define BRAND_COLOR              [UIColor colorWithRed:56.0/255.0 green:166.0/255.0 blue:85.0/255.0 alpha:1]
#define VERY_VERY_LIGHT_GRAY_COLOR    [UIColor colorWithRed:249.0/255.0 green:249.0/255.0 blue:249.0/255.0 alpha:1]
#define VERY_LIGHT_GRAY_COLOR    [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]
#define LIGHT_GRAY_COLOR         [UIColor colorWithRed:166.0/255.0 green:166.0/255.0 blue:166.0/255.0 alpha:1]
#define MEDIUM_GRAY_COLOR         [UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1]
#define DARK_GRAY_COLOR         [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1]
#define VERY_LIGHT_GRAY_COLOR    [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]
#define Background_COLOR    [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]
#define TouchFeedbackBackgroundColor [UIColor colorWithRed:247.0/225.0 green:247.0/225.0 blue:247.0/225.0 alpha:1.0]
#define Helvetica_Neue_Light @"HelveticaNeue-Light"
#define Helvetica_Neue_Regular @"HelveticaNeue"
#define Helvetica_Neue_Medium @"HelveticaNeue-Medium"

#import "APSTypography.h"

@implementation APSTypography


+ (instancetype) defaultTypography {
    static APSTypography *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APSTypography alloc] init];
    });
    
    return _sharedInstance;
}


+(UIColor *) colorBrand
{
    return BRAND_COLOR;
}

+(UIColor *) colorLightGray
{
    return LIGHT_GRAY_COLOR;
}

+(UIColor *) colorMediumGray
{
    return MEDIUM_GRAY_COLOR;
}

+(UIColor *) colorDrakGray
{
    return  DARK_GRAY_COLOR;
}

+(UIColor *) colorVeryLightGray
{
    return VERY_LIGHT_GRAY_COLOR;
}

+(UIColor *) colorVeryVeryLightGray
{
    return VERY_VERY_LIGHT_GRAY_COLOR;
}

+(UIColor *) colorBackground
{
    return Background_COLOR;
}

+(UIColor *) colorTouchFeedbackBackground
{
    return TouchFeedbackBackgroundColor;

}
+(UIFont *) fontLightWithSize:(CGFloat) size
{
    return [UIFont fontWithName:Helvetica_Neue_Light size:size];
}

+(UIFont *) fontRegularWithSize:(CGFloat) size
{
    return [UIFont fontWithName:Helvetica_Neue_Regular size:size];
}

+(UIFont *) fontMediumWithSize:(CGFloat) size
{
    return [UIFont fontWithName:Helvetica_Neue_Medium size:size];
}


+(NSMutableAttributedString *)attributedString:(NSString*)string withLetterSpacing:(float)letterSpacing
{
    if (string == nil)
    {
        return nil;
    }
    NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:string];
    [attrStr addAttribute:NSKernAttributeName value:@(letterSpacing) range:NSMakeRange(0, attrStr.length)];
    return attrStr;
}

+(NSMutableAttributedString *)attributedString:(NSString*)string withLineSpacing:(float)lineSpacing
{
    if (string == nil)
    {
        return nil;
    }
    NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:lineSpacing];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    [style setAlignment:NSTextAlignmentCenter];
    
    [attrStr addAttribute:NSParagraphStyleAttributeName
                      value:style
                    range:NSMakeRange(0, attrStr.length)];
    
    return attrStr;
}

+(NSString *) stringCompanyName
{
    return  NSLocalizedString(@"Kabel deutschland", nil);
}

+(NSString *) stringCompanyDescription
{

    return  NSLocalizedString(@"Ein vodafone unternehmen", nil);
}

+(NSString*)stringCompanyWebHome
{
    return NSLocalizedString(@"Company Web Home", nil); //@"www.kdg.de/selfcare"
}

+(UIImage *)  defaultMediaImage
{
    if (is_iPad())
    {
        return [UIImage imageNamed:@"default_media_image_ipad"];
    }
    else
    {
        return [UIImage imageNamed:@"default_media_image_iphone"];
    }

}


+(UIColor *) colorGrayWithValue:(NSInteger)value
{
    return [UIColor colorWithRed:value/255.0 green:value/255.0 blue:value/255.0 alpha:1];
}

@end
