//
//  APSTypography.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/22/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSTypography : NSObject


+ (instancetype) defaultTypography;

+(UIColor *) colorBrand;
+(UIColor *) colorLightGray;
+(UIColor *) colorMediumGray;
+(UIColor *) colorDrakGray;
+(UIColor *) colorVeryLightGray;
+(UIColor *) colorVeryVeryLightGray;
+(UIColor *) colorBackground;
+(UIColor *) colorTouchFeedbackBackground;
+(UIColor *) colorGrayWithValue:(NSInteger)value;


+(UIFont *) fontLightWithSize:(CGFloat) size;
+(UIFont *) fontRegularWithSize:(CGFloat) size;
+(UIFont *) fontMediumWithSize:(CGFloat) size;

+(NSString *) stringCompanyName;
+(NSString *) stringCompanyDescription;
+(NSString*)stringCompanyWebHome;


+(UIImage *)  defaultMediaImage;

+(NSMutableAttributedString *)attributedString:(NSString*)string withLetterSpacing:(float)letterSpacing;
+(NSMutableAttributedString *)attributedString:(NSString*)string withLineSpacing:(float)lineSpacing;

@end
