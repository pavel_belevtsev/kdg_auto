//
//  APSAlertController.h
//  Spas
//
//  Created by Pavel Belevtsev on 10.06.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSAlertController : UIAlertController

@end
