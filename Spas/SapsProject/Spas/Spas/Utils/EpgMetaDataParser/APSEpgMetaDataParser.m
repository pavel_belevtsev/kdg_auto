//
//  APSEpgMetaDataParser.m
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEpgMetaDataParser.h"

@implementation APSEpgMetaDataParser

-(id)initWithMetaData:(APSTVProgram *)program  andFieldToShow:(NSArray *)fieldToShow andFieldToShowOnlyOneItem:(NSArray *)fieldWithOnlyOneItem
{
    self = [super init];
    if (self)
    {
        if ([program isKindOfClass:[APSTVProgram class]])
        {
            self.program = program;
        }
        else
        {
            self.tvprogram = (TVEPGProgram *)program;
        }
        self.fieldToShow = [NSArray arrayWithArray:fieldToShow];
        self.fieldsWithOnlyOneItem = [NSArray arrayWithArray:fieldWithOnlyOneItem];
        self.metaData = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)buildMetaDataDic
{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    /*
    for (NSString * key in self.fieldToShow)
    {
      //  NSArray * arr1 = (NSArray *)self.program.metaData;
        if (self.program.metaData)
        {
            NSArray * arr = [self find:key inArray:self.program.metaData];
            if (! arr || [arr count]==0)
            {
                if (self.program.tags)
                {
                    arr = [self find:key inArray:self.program.tags];
                }
            }
            if (arr && [arr count]>0)
            {
                [dic setObject:arr forKey:key];
            }
        }
        
    }
    */
    if (self.program.metaData) {
        dic = [self.program.metaData mutableCopy];
    }
    
    self.metaData = [NSMutableDictionary dictionaryWithDictionary:dic];
}

-(NSArray*)find:(NSString *)text inArray:(NSArray*)arr
{
    NSMutableArray * tempArr = [NSMutableArray array];
    
    for (NSDictionary * dic in arr)
    {
        if ([[[dic objectForKey:@"Key"] uppercaseString] isEqualToString:[text uppercaseString]])
        {
            NSString * item = [dic objectForKey:@"Value"];
            item=[item stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];

            if (item)
            {
                [tempArr addObject:item];
                NSInteger index = [self.fieldsWithOnlyOneItem indexOfObject:text];
                if (index != NSNotFound)
                {
                    break;
                }
            }
        }
    }
    
    return tempArr;
}
-(NSDictionary *)getAFieldToShowInApp
{
    if (self.program == nil && self.tvprogram == nil)
    {
        return [NSDictionary dictionary];
    }
    
    if ( [self.program isKindOfClass:[APSTVProgram class]])
    {
        [self buildMetaDataDic];
         return [NSDictionary dictionaryWithDictionary:self.metaData];
    }
    else
    {
        [self buildTVProgramMetaDataDic];
        return [NSDictionary dictionaryWithDictionary:self.metaData];

        //return [[self.program.metaData dictionaryWithValuesForKeys:self.fieldToShow] dictionaryByRemovingNSNulls];
    }
    
   
   
}

-(void)buildTVProgramMetaDataDic
{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    for (NSString * key in self.fieldToShow)
    {
        //  NSArray * arr1 = (NSArray *)self.program.metaData;
        if (self.tvprogram.cleanMetaData)
        {
            NSArray * arr = [self find:key inArray:self.tvprogram.cleanMetaData];
            if (! arr || [arr count]==0)
            {
                if (self.tvprogram.cleanTags)
                {
                    arr = [self find:key inArray:self.tvprogram.cleanTags];
                }
            }
            if (arr && [arr count]>0)
            {
                [dic setObject:arr forKey:key];
            }
        }
        
    }
    
    self.metaData = [NSMutableDictionary dictionaryWithDictionary:dic];
}

+(NSString *)findFiledText:(NSString *)text inProgram:(APSTVProgram *)program
{
    NSArray * metaData = nil;
    if ( [program isKindOfClass:[APSTVProgram class]])
    {
        metaData =program.metaData;
    }
    else
    {
        TVEPGProgram * prog = (TVEPGProgram *)program;
        metaData = prog.cleanMetaData;
    }
    
    if ([metaData isKindOfClass:[NSArray class]])
    {
        for (NSDictionary * dic in metaData)
        {
            if ([[dic objectForKey:@"Key"] isEqualToString:text])
            {
                ASLogInfo(@"%@",[dic objectForKey:@"Value"]);
                NSString* val = [dic objectForKey:@"Value"];
                return [val stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];

            }
        }
    }
  
    return nil;
}

@end
