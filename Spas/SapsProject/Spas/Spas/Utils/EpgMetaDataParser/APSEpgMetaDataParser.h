//
//  APSEpgMetaDataParser.h
//  Spas
//
//  Created by Israel Berezin on 8/24/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSEpgMetaDataParser : NSObject
@property (strong, nonatomic) NSMutableDictionary * metaData;
@property (strong, nonatomic) APSTVProgram *program;
@property (strong, nonatomic) TVEPGProgram *tvprogram;

@property (strong, nonatomic )NSArray * fieldToShow;
@property (strong, nonatomic )NSArray * fieldsWithOnlyOneItem;

-(id)initWithMetaData:(APSTVProgram *)program  andFieldToShow:(NSArray *)fieldToShow andFieldToShowOnlyOneItem:(NSArray *)fieldWithOnlyOneItem;

+(NSString *)findFiledText:(NSString *)text inProgram:(APSTVProgram *)program;

-(NSDictionary *)getAFieldToShowInApp;
@end
