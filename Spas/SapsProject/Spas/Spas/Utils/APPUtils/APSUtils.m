//
//  APSUtils.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/17/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSUtils.h"
#import "APSLoginViewController.h"

@implementation APSUtils

+(NSString *) stringTimeFromDate:(NSDate *) fromDate toDate:(NSDate *) toDate
{
    NSDateComponents *componentsStart = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:fromDate];
    NSInteger hourStart = [componentsStart hour];
    NSInteger minuteStart = [componentsStart minute];
    
    NSDateComponents *componentsEnd = [[NSCalendar currentCalendar] components:(kCFCalendarUnitHour | kCFCalendarUnitMinute) fromDate:toDate];
    NSInteger hourEnd = [componentsEnd hour];
    NSInteger minuteEnd = [componentsEnd minute];
    
    return [NSString stringWithFormat:@"%02d:%02d - %02d:%02d",hourStart,minuteStart,hourEnd,minuteEnd];
}


+(NSDateFormatter *) dateFormatter
{
    static NSDateFormatter * dateDateFormatter = nil;
    
    if (dateDateFormatter ==nil)
    {
        dateDateFormatter = [[NSDateFormatter alloc] init];
        [dateDateFormatter setShortWeekdaySymbols:@[
                                             NSLocalizedString(@"Sun",nil),
                                             NSLocalizedString(@"Mon",nil),
                                             NSLocalizedString(@"Tue",nil),
                                             NSLocalizedString(@"Wed",nil),
                                             NSLocalizedString(@"Thu",nil),
                                             NSLocalizedString(@"Fri",nil),
                                             NSLocalizedString(@"Sat",nil)
                                             ]];

    }
    
    return dateDateFormatter;
}

+(NSString *) stringDateFromDate:(NSDate *) date
{
    if (date)
    {
        NSDateFormatter *dateFormat = [APSUtils dateFormatter];
        [dateFormat setDateFormat:@"E, d.MM."];
        NSString *dateString = [dateFormat stringFromDate:date];
        return dateString;
    }
    else
    {
        return @"";
    }
}


+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize) size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

+ (UIImage *)imageWithColor:(UIColor *)color radius:(float) radius
{
    
    CGRect rect = CGRectMake(0.0f, 0.0f, radius*2, radius*2);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillEllipseInRect(context, rect);
    
    CGContextRestoreGState(context);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}




@end
