//
//  APSUtils.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/17/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSUtils : NSObject

+(NSString *) stringTimeFromDate:(NSDate *) fromDate toDate:(NSDate *) toDate;
+(NSString *) stringDateFromDate:(NSDate *) date;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize) size;
+ (UIImage *)imageWithColor:(UIColor *)color radius:(float) radius;



@end
