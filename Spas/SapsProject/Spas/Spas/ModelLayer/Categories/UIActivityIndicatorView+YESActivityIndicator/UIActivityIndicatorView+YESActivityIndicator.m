//
//  UIActivityIndicatorView+YESActivityIndicator.m
//  Preloader
//
//  Created by Rivka S. Peleg on 12/15/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import "UIActivityIndicatorView+YESActivityIndicator.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+ProportionalFill.h"

#define IMAGE_NAME @"ActivityIndicator"


@implementation UIActivityIndicatorView (YESActivityIndicator)

-(void) startAnimating
{
    self.color = [UIColor clearColor];
    self.hidden = NO;
    NSInteger hashNumber = [IMAGE_NAME hash];
    UIImageView * imageView = (UIImageView *)[self viewWithTag:hashNumber];
    if (!imageView)
    {
        UIImage * preloaderImage = [UIImage imageNamed:IMAGE_NAME];
        preloaderImage = [preloaderImage imageToFitSize:CGSizeMake(self.frame.size.width*1.25, self.frame.size.height*1.25) method:MGImageResizeScale];
        imageView = [[UIImageView alloc] initWithImage:preloaderImage];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.center = CGPointMake(self.frame.size.width/2, self.frame.size.width/2);
        imageView.tag = [IMAGE_NAME hash];
        [self addSubview:imageView];

    }
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @0.0f;
    animation.toValue = @(2*M_PI);
    animation.duration = 0.75f;             // this might be too fast
    animation.repeatCount = HUGE_VALF;     // HUGE_VALF is defined in math.h so import it
    [imageView.layer addAnimation:animation forKey:@"rotation"];
}

-(void) stopAnimating
{
    self.color = [UIColor clearColor];
    self.hidden = YES;
    
    NSInteger hashNumber = [IMAGE_NAME hash];
    UIImageView * imageView = (UIImageView *)[self viewWithTag:hashNumber];
    [imageView.layer removeAllAnimations];
}
@end
