//
//  UIActivityIndicatorView+YESActivityIndicator.h
//  Preloader
//
//  Created by Rivka S. Peleg on 12/15/13.
//  Copyright (c) 2013 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActivityIndicatorView (YESActivityIndicator)



- (void)startAnimating;
- (void)stopAnimating;
@end
