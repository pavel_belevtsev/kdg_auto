//
//  UIAlertView+Blocks.h
//

#import <Foundation/Foundation.h>

@interface UIAlertView (Blocks)

- (void)showWithCompletion:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))completion;

@end
