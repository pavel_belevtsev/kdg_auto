//
//  UIImage+APSColoredImage.h
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (APSColoredImage)

+(UIImage *) coloredImageWithColor:(UIColor *) color bitMapImage:(UIImage *) bitMapImage;

@end
