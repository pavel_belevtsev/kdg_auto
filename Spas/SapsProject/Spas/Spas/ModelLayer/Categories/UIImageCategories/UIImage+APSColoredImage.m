//
//  UIImage+APSColoredImage.m
//  Spas
//
//  Created by Rivka Peleg on 9/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "UIImage+APSColoredImage.h"

@implementation UIImage (APSColoredImage)


+(UIImage *) coloredImageWithColor:(UIColor *) color bitMapImage:(UIImage *) bitMapImage
{
   CGSize scaledSize = CGSizeMake(bitMapImage.size.width*bitMapImage.scale, bitMapImage.size.height*bitMapImage.scale);
   UIImage * image = [self maskImage:[self imageWithColor:color size:scaledSize]
                  withMask:bitMapImage];
    UIImage * maskedImage = [UIImage imageWithCGImage:[image CGImage] scale:bitMapImage.scale orientation:UIImageOrientationUp];
    return maskedImage;
}


+ (UIImage*) maskImage:(UIImage *) image withMask:(UIImage *) mask
{
    CGImageRef imageReference = image.CGImage;
    CGImageRef maskReference = mask.CGImage;
    
    CGImageRef imageMask = CGImageMaskCreate(CGImageGetWidth(maskReference),
                                             CGImageGetHeight(maskReference),
                                             CGImageGetBitsPerComponent(maskReference),
                                             CGImageGetBitsPerPixel(maskReference),
                                             CGImageGetBytesPerRow(maskReference),
                                             CGImageGetDataProvider(maskReference),
                                             NULL, // Decode is null
                                             YES // Should interpolate
                                             );
    
    CGImageRef maskedReference = CGImageCreateWithMask(imageReference, imageMask);
    CGImageRelease(imageMask);
    
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedReference];
    CGImageRelease(maskedReference);
    
    return maskedImage;
}


+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize) size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
