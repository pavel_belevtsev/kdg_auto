//
//  NSString+Time.h
//  QuangoMobile
//
//  Created by Yedidya Reiss on 1/20/13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Time)

/**
 * @arg time - time in seconds
 * @return stringWithFormat mm:ss
 */
+ (NSString*) stringWithTimeInterval:(NSTimeInterval) time;


/**
 * @arg date - the wanted date
 * @arg format - the wanted format
 * 
 * for example - 
 * the date: sunday 3/02/2013 sunday 16:24
 * the format:
 * @"dd/MM/yyyy" = @"03/02/2013"
 * @"MMM d, yy" = @"Feb 3, 13"
 * @"dd-MMMM-yyyy" = @"3-February-2013"
 * @"HH:mm:ss" = @"16:24:32"
 * @"h:mm a" = @"4:24 PM"
 * @"eee" = @"Sun"
 * @"eeee&dd#  y!mm" = @"Sunday&03#   2013!24"
 *
 * @return a string from the date with the given format
 */
+ (NSString*) stringWithDate: (NSDate*) date AndFormat: (NSString*) format;

@end
