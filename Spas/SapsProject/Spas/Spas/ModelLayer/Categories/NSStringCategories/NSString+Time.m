//
//  NSString+Time.m
//  QuangoMobile
//
//  Created by Yedidya Reiss on 1/20/13.
//  Copyright (c) 2013 Quickode Ltd. All rights reserved.
//

#import "NSString+Time.h"

@implementation NSString (Time)

+ (NSString*) stringWithTimeInterval:(NSTimeInterval) time {
    NSInteger minutes = (NSInteger)time / 60;
    NSInteger seconds = (NSInteger)time % 60;
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}


+ (NSString*) stringWithDate: (NSDate*) date AndFormat: (NSString*) format {
    NSDateFormatter *dateFormatStr = [[NSDateFormatter alloc] init];
    [dateFormatStr setDateFormat:format];
    NSString* dateString = [dateFormatStr stringFromDate: date];
    //[dateFormatStr release];// !!!
    return dateString;
}

@end
