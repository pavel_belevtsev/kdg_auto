//
//  APSTVChannel.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/15/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTVChannel.h"
#import "APSTVProgram.h"


@implementation APSTVChannel

@dynamic channelID;
@dynamic name;
@dynamic pictureURL;
@dynamic channelDescription;
@dynamic orderNumber;
@dynamic isActive;
@dynamic groupID;
@dynamic editorRemarks;
@dynamic status;
@dynamic updaterID;
@dynamic createDate;
@dynamic publishDate;
@dynamic mediaID;
@dynamic epgChannelID;
@dynamic programs;


static NSDateFormatter *formatter = nil;

+(NSDateFormatter *) channelDateFormatter
{
    if (formatter == nil)
    {
        formatter  = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"M/d/yyyy h:mm:ss a";
    }
    
    return  formatter;
    
    
}

+ (APSTVChannel *) channelWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    APSTVChannel *channel = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    request.entity = [NSEntityDescription entityForName:@"APSTVChannel" inManagedObjectContext:context];
    NSString * propertyKey = NSStringFromProperty(epgChannelID);
    NSString * epgChannelID = [dictionary objectForKey:EPGChannelEPGChannelIDKey];
    
    request.predicate = [NSPredicate predicateWithFormat:@"%K = %@", propertyKey,epgChannelID];
    NSError *executeFetchError = nil;
    channel = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        NSLog(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [epgChannelID intValue], [executeFetchError localizedDescription]);
    } else if (!channel) {
        channel = [NSEntityDescription insertNewObjectForEntityForName:@"APSTVChannel"
                                                inManagedObjectContext:context];
    }
    
    channel.channelID = [dictionary objectForKey:EPGChannelChannelIDKey];
    channel.epgChannelID = [dictionary objectForKey:EPGChannelEPGChannelIDKey];
    channel.name = [dictionary objectForKey:EPGChannelNameKey];
    channel.pictureURL = [dictionary objectForKey:EPGChannelPicURLKey];
    channel.channelDescription = [dictionary objectForKey:EPGChannelDescriptionKey];
    channel.orderNumber = [dictionary objectForKey:EPGChannelOrderNumKey];
    channel.isActive = [dictionary objectForKey:EPGChannelIsActiveKey];
    channel.groupID = [dictionary objectForKey:EPGChannelGroupIDKey];
    channel.editorRemarks = [dictionary objectForKey:EPGChannelEditorRemarksKey];
    channel.status = [dictionary objectForKey:EPGChannelStatusKey];
    channel.updaterID = [dictionary objectForKey:EPGChannelUpdaterIDKey];
    NSString * createDateString = [dictionary objectForKey:EPGChannelCreateDateKey];
    NSString * publishDateString = [dictionary objectForKey:EPGChannelPublishDateKey];
    channel.createDate = [[APSTVChannel channelDateFormatter] dateFromString:createDateString];
    channel.publishDate = [[APSTVChannel channelDateFormatter] dateFromString:publishDateString];
    
    return channel;
}


@end
