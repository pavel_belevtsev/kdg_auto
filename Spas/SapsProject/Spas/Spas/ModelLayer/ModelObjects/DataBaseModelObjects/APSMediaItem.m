//
//  APSMediaItem.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSMediaItem.h"
#import <TvinciSDK/NSDate+Format.h>
#import "APSImage.h"




@implementation APSMediaItem

@dynamic catalogStartDate;
@dynamic creationDate;
@dynamic duration;
@dynamic externalIDs;
@dynamic fileID;
@dynamic files;
@dynamic likeCounter;
@dynamic mediaDescription;
@dynamic mediaID;
@dynamic mediaName;
@dynamic mediaTypeID;
@dynamic mediaTypeName;
@dynamic mediaWebLink;
@dynamic metaData;
@dynamic pictures;
@dynamic pictureURL;
@dynamic rating;
@dynamic startDate;
@dynamic tags;
@dynamic totalItems;
@dynamic url;
@dynamic viewCounter;
@dynamic plainJson;





+ (APSMediaItem *) mediaWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    APSMediaItem *mediaItem = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    request.entity = [NSEntityDescription entityForName:@"APSMediaItem" inManagedObjectContext:context];
    NSString * propertyKey = NSStringFromProperty(mediaID);
    NSString * mediaID = [dictionary objectForKey:MediaItemIDKey];
    
    request.predicate = [NSPredicate predicateWithFormat:@"%K = %@", propertyKey,mediaID];
    NSError *executeFetchError = nil;
    mediaItem = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        NSLog(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [mediaID intValue], [executeFetchError localizedDescription]);
    } else if (!mediaItem) {
        mediaItem = [NSEntityDescription insertNewObjectForEntityForName:@"APSMediaItem"
                                                  inManagedObjectContext:context];
    }
    
    
    mediaItem.mediaID = [dictionary objectOrNilForKey:MediaItemIDKey];
    mediaItem.mediaName = [dictionary objectOrNilForKey:MediaItemNameKey];
    mediaItem.mediaDescription = [dictionary objectOrNilForKey:MediaItemDescriptionKey];
    mediaItem.mediaTypeName = [dictionary objectOrNilForKey:MediaItemTypeNameKey];
    mediaItem.mediaTypeID = [dictionary objectOrNilForKey:MediaItemTypeIDKey];
    mediaItem.rating = [dictionary objectOrNilForKey:MediaItemRatingKey];
    mediaItem.viewCounter = [dictionary objectOrNilForKey:MediaItemViewCounterKey];
    mediaItem.totalItems = [dictionary objectOrNilForKey:MediaItemTotalItemsKey];
    mediaItem.likeCounter = [dictionary objectOrNilForKey:TVMediaItemLikeCounterKey];
    mediaItem.duration = [dictionary objectOrNilForKey:MediaItemDurationKey];
    mediaItem.url = [dictionary objectOrNilForKey:MediaItemURLKey];
    mediaItem.fileID = [dictionary objectOrNilForKey:MediaItemFileIDKey];
    
    NSString * creationDateString =  [dictionary objectOrNilForKey:MediaItemCreationDateKey];
    mediaItem.creationDate = [APSMediaItem dateTimeByString:creationDateString];
    
    NSString * startDateString = [dictionary objectOrNilForKey: MediaItemStartDateKey];
    mediaItem.startDate = [APSMediaItem dateTimeByString:startDateString];
    
    NSString * catalogStartDateString = [dictionary objectOrNilForKey:MediaItemCatalogStartDateKey];
    mediaItem.catalogStartDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:catalogStartDateString andFormat:MediaItemCatalogStartDateFormat];
    
    mediaItem.pictureURL = [dictionary objectOrNilForKey:MediaItemPictureURLKey];
    mediaItem.mediaWebLink = [dictionary objectOrNilForKey:MediaItemWebLinkKey];
    mediaItem.metaData =  [APSMediaItem parseMetas:[dictionary objectForKey:MediaItemMetaDataKey]];
    mediaItem.tags = [APSMediaItem parseTags:[dictionary objectOrNilForKey:MediaItemTagsKey]];
    mediaItem.externalIDs = [dictionary objectOrNilForKey:TVMediaItemExternalID];
    mediaItem.files = [dictionary objectOrNilForKey:TVMediaItemFilesKey];
    
    NSArray *pictureSizesAsDicts = [dictionary objectOrNilForKey:MediaItemPicturesKey];
    for(NSDictionary *dict in pictureSizesAsDicts){
        APSImage *image = [APSImage imageWithdictionary:dict inManagedObjectContext:context];
        [mediaItem addPicturesObject:image];
    }
    mediaItem.plainJson = dictionary;
    
    return mediaItem;
}


+(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    // ASLog2Debug(@"%@",metaParsed);
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}



+(NSDictionary *) parseTags:(NSArray *)tags
{
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in tags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *values = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            NSArray *valuesSeparated = [values componentsSeparatedByString:@"|"];
            if (valuesSeparated != nil)
            {
                [tagsParsed setObject:valuesSeparated forKey:title];
            }
        }
    }
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}


NSString *const expirationDateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
+ (NSDate *)dateTimeByString:(NSString *)dateString
{
    static NSDateFormatter *formatter = nil;
    if (formatter == nil)
    {
        formatter = [[NSDateFormatter alloc] init];
    }
    
    formatter.dateFormat = expirationDateFormat;
    NSDate *toReturn = [formatter dateFromString:dateString];
    //[formatter release];// !!!
    return toReturn;
}


- (TVMediaItem *) tvMediaItem
{
    return [[TVMediaItem alloc] initWithDictionary:self.plainJson];
}

@end
