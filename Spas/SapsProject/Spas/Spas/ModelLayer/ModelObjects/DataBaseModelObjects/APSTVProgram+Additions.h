//
//  APSTVProgram+Additions.h
//  Spas
//
//  Created by Rivka Peleg on 9/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

@class APSTVProgram;

@interface APSTVProgram (Additions)


+ (APSTVProgram *) programWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;
-(void) setdictionary:(NSDictionary *)dictionary;
-(NSURL *) pictureURLForSize : (CGSize ) size;
-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size;
-(NSDictionary *) theParsedMetaData;

//decodedValueForKey will replace any occurances of @"&amp;" withString:@"&"
-(NSString *) decodedValueForKey:(NSString *) key;

+(NSDate *) dateFromProgramsDateString:(NSString *) dateString;

@end
