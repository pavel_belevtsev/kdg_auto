//
//  TBCoreDataStore.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/17/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSCoreDataStore.h"
#import "APSAppDelegate.h"

@interface APSCoreDataStore ()

//@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
//
//@property (strong, nonatomic) NSManagedObjectContext *mainQueueContext;
//@property (strong, nonatomic) NSManagedObjectContext *privateQueueContext;

@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;

@property (strong, nonatomic) NSManagedObjectContext *defaultPrivateQueueContext;

@end

#pragma mark - Singleton Access


@implementation APSCoreDataStore


+ (instancetype) defaultStore {
    static APSCoreDataStore *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APSCoreDataStore alloc] init] ;
    });
    
    return _sharedInstance;
}


-(NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator == nil)
    {
        _persistentStoreCoordinator = [((APSAppDelegate *)[[UIApplication sharedApplication] delegate])  persistentStoreCoordinator];
    }
    
    return _persistentStoreCoordinator;
}

-(NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel == nil)
    {
        _managedObjectModel = [((APSAppDelegate *)[[UIApplication sharedApplication] delegate])  managedObjectModel];;
    }
    
    return _managedObjectModel;
}


//+ (NSManagedObjectContext *)mainQueueContext
//{
//    return [[self defaultStore] mainQueueContext];
//}
//
//+ (NSManagedObjectContext *)privateQueueContext
//{
//    return [[self defaultStore] privateQueueContext];
//}
//
//#pragma mark - Getters
//
//- (NSManagedObjectContext *)mainQueueContext
//{
//    if (!_mainQueueContext) {
//        _mainQueueContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
//        _mainQueueContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
//    }
//    
//    return _mainQueueContext;
//}
//
//- (NSManagedObjectContext *)privateQueueContext
//{
//    if (!_privateQueueContext) {
//        _privateQueueContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//        _privateQueueContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
//    }
//    
//    return _privateQueueContext;
//}
//
//- (id)init
//{
//    self = [super init];
//    if (self) {
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(contextDidSavePrivateQueueContext:)
//                                                     name:NSManagedObjectContextDidSaveNotification
//                                                   object:[self privateQueueContext]];
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(contextDidSaveMainQueueContext:)
//                                                     name:NSManagedObjectContextDidSaveNotification
//                                                   object:[self mainQueueContext]];
//    }
//    return self;
//}
//
//- (void)dealloc
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//}
//
//- (void)contextDidSavePrivateQueueContext:(NSNotification *)notification
//{
//    @synchronized(self) {
//        [self.mainQueueContext performBlock:^{
//            [self.mainQueueContext mergeChangesFromContextDidSaveNotification:notification];
//        }];
//    }
//}
//
//- (void)contextDidSaveMainQueueContext:(NSNotification *)notification
//{
//    @synchronized(self) {
//        [self.privateQueueContext performBlock:^{
//            [self.privateQueueContext mergeChangesFromContextDidSaveNotification:notification];
//        }];
//    }
//}

+ (NSManagedObjectContext *)newMainQueueContext
{

    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    context.parentContext = [self defaultPrivateQueueContext];
    
    return context;
}

+ (NSManagedObjectContext *)newPrivateQueueContext
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.parentContext = [self defaultPrivateQueueContext];
    
    return context;
}

+ (NSManagedObjectContext *)defaultPrivateQueueContext
{
    return [[self defaultStore] defaultPrivateQueueContext];
}

#pragma mark - Getters

- (NSManagedObjectContext *)defaultPrivateQueueContext
{
 @synchronized(self) {
    if (!_defaultPrivateQueueContext) {
        _defaultPrivateQueueContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _defaultPrivateQueueContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    }
 }
    return _defaultPrivateQueueContext;
}


@end
