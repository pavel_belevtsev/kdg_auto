//
//  APSImage.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSImage.h"
#import "APSMediaItem.h"


@implementation APSImage

@dynamic size;
@dynamic imageURL;
@dynamic direction;
@dynamic ratio;
@dynamic mediaItem;


+ (APSImage *) imageWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    APSImage *image = nil;
    
    image = [NSEntityDescription insertNewObjectForEntityForName:@"APSImage"
                                              inManagedObjectContext:context];
    NSString *picSizeStr = [dictionary objectForKey:@"PicSize"];
    NSArray *widthHeightArr = [picSizeStr componentsSeparatedByString:@"X"];
    CGSize imageSize = CGSizeMake([[widthHeightArr objectAtIndex:0] floatValue], [[widthHeightArr objectAtIndex:1] floatValue]);
    
    
    image.size = NSStringFromCGSize(imageSize);
    image.imageURL = [dictionary objectForKey:@"URL"];
    
    image.direction = [NSNumber numberWithBool:false];
    if (imageSize.width > imageSize.height) {
        image.direction = [NSNumber numberWithBool:true];
    }

    image.ratio = [NSNumber numberWithDouble:imageSize.width /imageSize.height];
    return image;
}




@end
