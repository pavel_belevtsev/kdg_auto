//
//  APSSSLPinningManager.m
//  Spas
//
//  Created by Pavel Belevtsev on 23.06.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import "APSSSLPinningManager.h"
#import <TvinciSDK/AFSecurityPolicy.h>

@implementation APSSSLPinningManager

+ (APSSSLPinningManager *)manager {
    
    static APSSSLPinningManager *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
            
        }
    }
    return instance;
}

- (id)init {
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)start {
    
    
    self.SSLFailed = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sslPinningOk) name:AFNetworkingSSLPinningOkNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sslPinningFailed) name:AFNetworkingSSLPinningFailedNotification object:nil];
    
}

- (void)sslPinningOk {
    
    self.SSLFailed = NO;
    
}

- (void)sslPinningFailed {
    
    self.SSLFailed = YES;
    
}

- (void)goToAppStoreAndExit {
    

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/de/app/kabel-deutschland-tv-app/id926736020?mt=8"]];
        
    exit(0);
    
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
