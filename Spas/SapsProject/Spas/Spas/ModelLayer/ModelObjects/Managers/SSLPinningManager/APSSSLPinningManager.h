//
//  APSSSLPinningManager.h
//  Spas
//
//  Created by Pavel Belevtsev on 23.06.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSSSLPinningManager : NSObject


+ (APSSSLPinningManager *)manager;

- (void)start;

- (void)goToAppStoreAndExit;

@property (nonatomic) BOOL SSLFailed;

@end
