//
//  APSForceDownload.m
//  YES_iPad
//
//  Created by Israel Berezin on 9/18/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import "APSForceDownload.h"

@interface APSForceDownload () <UIAlertViewDelegate>

@property (nonatomic, weak) UIViewController *parentViewController;

@end

@implementation APSForceDownload


+ (instancetype)sharedForceDownload
{
    static APSForceDownload *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APSForceDownload alloc] init];;
    });
    
    return _sharedInstance;
}

-(BOOL) isApplicationContinuencePermitted
{
   NSString * isForceUpdate = [[(TVConfigurationManager *)[TVConfigurationManager sharedTVConfigurationManager] dictionaryVersionInformation] objectForKey:TVconfigVersionInformationIsforceupdateKey];
    
    return !(isForceUpdate.integerValue == 1);
}

-(void) handleForceDownload:(UIViewController*)viewController
{
    self.parentViewController = viewController;
    [self showForceDownloadAlert:viewController];
}

-(void) showForceDownloadAlert:(UIViewController*)viewController
{
    NSString * title = NSLocalizedString(@"Title:ForceDownload", nil);
    NSString * content =NSLocalizedString(@"Content:ForceDownload", nil);;
    NSString * update =NSLocalizedString(@"Update:ForceDownload", nil);;
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title message:content delegate:self cancelButtonTitle:update otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleAlert];
        __weak typeof(self) weakSelf = self;
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:update style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       NSString * deviceKey = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?TVconfigForcedUpdateURLiPadKey:TVconfigForcedUpdateURLiPhoneKey;
                                       NSString * linkString = [[(TVConfigurationManager *)[TVConfigurationManager sharedTVConfigurationManager] dictionaryForceDownloadLinks] objectForKey:deviceKey];
                                       NSURL * url = [NSURL URLWithString:linkString];
                                       
                                       if ([[UIApplication sharedApplication] canOpenURL:url])
                                       {
                                           [[UIApplication sharedApplication] openURL:url];;
                                       }
                                       __strong typeof(weakSelf)strongSelf = weakSelf;
                                       [strongSelf showForceDownloadAlert:viewController];
                                   }];
        [alertController addAction:okAction];
        [viewController presentViewController:alertController animated:YES completion:nil];
    }
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * deviceKey = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?TVconfigForcedUpdateURLiPadKey:TVconfigForcedUpdateURLiPhoneKey;
    NSString * linkString = [[(TVConfigurationManager *)[TVConfigurationManager sharedTVConfigurationManager] dictionaryForceDownloadLinks] objectForKey:deviceKey];
    NSURL * url = [NSURL URLWithString:linkString];
    
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];;
    }
    
    [self showForceDownloadAlert:self.parentViewController];
    
}

#pragma application did become active 





@end
