//
//  YESForceDownload.h
//  YES_iPad
//
//  Created by Israel Berezin on 9/18/14.
//  Copyright (c) 2014 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSForceDownload : NSObject

+ (instancetype)sharedForceDownload;

-(BOOL) isApplicationContinuencePermitted;
-(void) handleForceDownload:(UIViewController*)viewController;

@end
