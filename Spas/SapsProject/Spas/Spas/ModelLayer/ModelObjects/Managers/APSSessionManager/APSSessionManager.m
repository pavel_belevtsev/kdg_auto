//
//  APSSessionManager.m
//  Spas
//
//  Created by Israel Berezin on 10/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSessionManager.h"

//@"newUserLoginStaus",@"oldUserLoginStaus"

#define loginStatusSsoServerDown 23

@implementation APSSessionManager

#pragma mark - Singlton -


SYNTHESIZE_SINGLETON_FOR_CLASS(APSSessionManager);



#pragma mark - device State -

-(TVDeviceState) deviceState
{
    return self.currentdeviceState;
}

#pragma mark - TVPAPIRequest Delegate -

-(void) parseSignInResponse : (TVPAPIRequest *) loginRequest
{
    NSDictionary *response = [loginRequest JSONResponse];
    NSNumber *domainID = [response objectOrNilForKey:TVConfigDomainIDKey];
    NSString *siteGUID = [response objectOrNilForKey:TVConfigSiteGUIDKey]; //TVLoginStatusCustomError
    
    NSInteger loginStatus = TVLoginStatusCustomError;
    if ([response objectOrNilForKey:@"LoginStatus"])
    {
        loginStatus = [[response objectOrNilForKey:@"LoginStatus"] integerValue];
    }
    
    self.userData = [response objectForKey:TVUserDataKey];
    
    ASLogDebug(@"Login Status: %ld",(long)loginStatus);
    ASLogDebug(@"SiteGUID: %@",siteGUID);
    ASLogDebug(@"DomainID: %@",domainID);
    
    NSString *errorMessage = nil;
    self.currentdeviceState = TVDeviceStateUnknown;
    
    if (loginStatus == TVLoginStatusOK || loginStatus == TVLoginStatusDeviceNotRegistered|| loginStatus == loginStatusSsoServerDown)
    {
        if( self.assertLoginBlock && self.assertLoginBlock(response) == NO)
        {
            NSDictionary *info = [NSDictionary dictionaryWithObject:response forKey:NSLocalizedDescriptionKey];
            
            NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusCustomError userInfo:info];
            
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            
            return;
        }
        
        [self.tokenizationManager authenticationSuccededWithResponseHeader:loginRequest.responseHeaders];
        
        [self storeSiteGUID:siteGUID];
        [self storeDomainId:domainID];
        [self storeUserData:self.userData];
        [self storeOpeningSessionDate];
        
        if ([self.sharedInitObject belongsToValidDomain])
        {
            // Report SignIn Completed
            [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
       
            // Not necessary for the login process, but still good to have.
            [self updateUserDataFromSignInResponse:loginRequest];
            
            [self getDeviceDomains];
        }
        else
        {
            // For some reason siteGUID or DomainID is missing
            errorMessage = NSLocalizedString(@"Login Error: No SiteGUID", nil);
        }
    }
    else
    {
        errorMessage = ErrorMessageForLoginStatus(loginStatus);
    }
    
    if (errorMessage!= nil)
    {
        ASLogInfo(@"login errorMessage = %@", errorMessage);
        NSDictionary *info = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:loginStatus userInfo:info];
        [self postNotification:TVSessionManagerSignInFailedNotification error:error];
    }
}


-(void) parseRegisterDeviceResponse : (TVPAPIRequest *) request
{
    ASLogDebug(@"Device registration finished ");
    NSDictionary *response = [request JSONResponse];
    ASLogDebug(@"request  = %@",request);
    
    NSNumber* DomainResponseStatus = [response objectForKey:kAPIResponseKey_DomainResponseStatusKey];

    TVPAPIRequest * requestInfo = [TVPDomainAPI requestForGetDomainInfo:self];
    __weak TVPAPIRequest * weakRequest = requestInfo;
    
    [requestInfo setFailedBlock:^{

        [self parseDomainInfoResponse:nil domainResponseStatus:DomainResponseStatus];
        
    }];
    
    [requestInfo setCompletionBlock:^{
        
        [self parseDomainInfoResponse:weakRequest domainResponseStatus:DomainResponseStatus];
        
    }];
    
    [requestInfo startAsynchronous];
    
}

- (void) parseDomainInfoResponse : (TVPAPIRequest *) request domainResponseStatus:(NSNumber *)DomainResponseStatus {
    
    if (request) {
        
        NSDictionary *response = [request JSONResponse];
        
        self.currentDomain = [[TVDomain alloc] initWithDictionary:response];
        ASLogDebug(@"self.currentDomain= %@",self.currentDomain);////////
        
        [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
        
    }
    
    if (self.currentDevice != nil)
    {
        self.currentdeviceState = TVDeviceStateActivated;
        [self postNotification:TVSessionManagerDeviceRegistrationCompletedNotification userInfo:nil];
    }
    else
    {
        // handling known error
        // checking the response status
        if([DomainResponseStatus intValue] == kDomainResponseStatus_DeviceAlreadyExists)
        {
            ASLogDebug(@"[Device][Registration] Device already exists on another domain");////////
            NSString *errorStr = NSLocalizedString(@"Dialog Message: Device registered for another user", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification error:error];
            return;
        }
        else
        {
            NSString *errorStr = NSLocalizedString(@"Login Error: Device Registration Failed", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedNotification error:error];
        }
    }
}

#pragma mark - API -

-(void)getDeviceDomains
{
    self.currentdeviceState = TVDeviceStateNotRegistered;

    TVPAPIRequest *strongRequest = [TVPDomainAPI requestForGetDeviceDomains:nil];
    
    __weak TVPAPIRequest *request = strongRequest;//[TVPDomainAPI requestForGetDeviceDomains:nil];
    
    [request setStartedBlock:^{
        
    }];

    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[request error]];

    }];
    
    [request setCompletionBlock:^{
        NSArray * domains = [[request JSONResponse] arrayByRemovingNSNulls];
        
        for (NSDictionary * dict in domains)
        {
            NSNumber * domainID =  [dict objectForKey:@"DomainID"];
            if ([domainID integerValue] == self.sharedInitObject.domainID)
            {
                ASLogInfo(@"device is in domain !!!");
                self.currentdeviceState = TVDeviceStateActivated;
                break;
            }
        }
        [self postNotification:TVSessionManagerDeviceStateAvailableNotification userInfo:nil];

    }];
    
    [request startAsynchronous];
        
}

-(void)setUserLoginStaus:(APSLoginStaus)userLoginStaus
{
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInt:userLoginStaus],[NSNumber numberWithInt:_userLoginStaus], nil] forKeys:[NSArray arrayWithObjects:APSSessionManagerNewUserLoginStaus,APSSessionManagerOldUserLoginStaus, nil]];
    
    _userLoginStaus = userLoginStaus;
    [self postNotification:APSSessionManagerUserLoginStausUpdateNotification userInfo:dic];
    
}

//-(BOOL) isSignedIn
//{
//    NSString *userName = [GCUserDomain  getUserName];
//    NSString *password =[GCUserDomain getUserPassword];
//    NSLog(@"startLoginProcess %@,%@",userName,password);
//    if ((userName.length == 0 || password.length == 0) )
//    {
//        return NO;
//    }
//    return YES;
//}

NSString * const APSSessionManagerUserLoginStausUpdateNotification = @"APSSessionManagerUserLoginStausUpdateNotification";
NSString * const APSChangeNetwortServerNotification = @"APSChangeNetwortServerNotification";
NSString * const APSSessionManagerNewUserLoginStaus = @"newUserLoginStaus";
NSString * const APSSessionManagerOldUserLoginStaus = @"oldUserLoginStaus";

@end
