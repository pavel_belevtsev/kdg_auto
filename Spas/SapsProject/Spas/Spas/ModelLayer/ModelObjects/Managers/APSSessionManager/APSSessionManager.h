//
//  APSSessionManager.h
//  Spas
//
//  Created by Israel Berezin on 10/23/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

typedef enum
{
    APSLoginStaus_Logout = 0,
    APSLoginStaus_Login,
    APSLoginStaus_OutOfNetwork,
    APSLoginStaus_Unknown,
    
}APSLoginStaus;

@interface APSSessionManager : TVSessionManager

+(APSSessionManager *) sharedAPSSessionManager;
@property (nonatomic) TVDeviceState currentdeviceState;

@property (nonatomic) APSLoginStaus userLoginStaus;

@end

extern NSString * const APSSessionManagerUserLoginStausUpdateNotification;
extern NSString * const APSChangeNetwortServerNotification;

extern NSString * const APSSessionManagerNewUserLoginStaus;
extern NSString * const APSSessionManagerOldUserLoginStaus;
