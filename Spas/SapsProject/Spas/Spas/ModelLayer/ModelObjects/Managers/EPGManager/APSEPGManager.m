
//
//  APSEPGManager.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSEPGManager.h"
#import "APSAppDelegate.h"
#import "APSRangeArray.h"
#import "APSConfigurationManager.h"
#import "APSBlockOperation.h"
#import <TvinciSDK/APSCoreDataStore.h>
#import <TvinciSDK/APSMediaItem.h>

#define kChannelsKey @"EPG channels"
#define kProgramsByChannelAndDateKey(channelID,date) [NSString stringWithFormat:@"Programs for channel id:%@ of date:%@",channelID,date]
#define kRangeArrayUserDefaultsKey @"Ranges"

@interface APSEPGManager ()



@property (retain, nonatomic) NSOperationQueue * operationQueue;

@property (strong, nonatomic) NSTimer * timer;
@property (retain, nonatomic) NSArray * arrayChannels;




@end

@implementation APSEPGManager





+ (instancetype)sharedEPGManager {
    static APSEPGManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APSEPGManager alloc] init];
        _sharedInstance.operationQueue = [[NSOperationQueue alloc] init];
    });
    
    return _sharedInstance;
}


-(void) clearInvalidateData
{
    NSDate * minimumInsertDate = [self minimumInsertDate];
    NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
    [workerContext performBlock:^{
        
        
        NSString * tableName = NSStringFromClass([APSTVProgram class]);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
        [fetchRequest setEntity:entity];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(insertionDate < %@ )",minimumInsertDate];
        
        NSError *error;
        NSArray *items = [workerContext executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];// !!!
        
        for (NSManagedObject *managedObject in items) {
            [workerContext deleteObject:managedObject];
        }
        
        if (![workerContext save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",tableName,error);
        }
        
    }];
}



-(void) loadChannelsWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock
{
    
    TVMenuItem * menuItem =  [[APSLayoutManager sharedLayoutManager] menuItemForPageLayout:@"EPG" menuItems:[[APSLayoutManager sharedLayoutManager] arrayMenuItems]];
    menuItem = [[APSLayoutManager sharedLayoutManager] menuItemForType:@"All-EPG" menuItems:menuItem.children];
    NSString * tvChannelId = [menuItem.layout objectForKey:@"ChannelID"];
    
    
    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetChannelMediaList:[tvChannelId integerValue]
                                                                    pictureSize:[TVPictureSize iPadEpgChnnel100X100]
                                                                       pageSize:1000
                                                                      pageIndex:0
                                                                        orderBy:TVOrderByNoOrder
                                                                       delegate:nil];
    
    
    [request setFailedBlock:^{
        
        if (failedBlock)
        {
            failedBlock();
        }
    }];
    
    
    [request setCompletionBlock:^{
        
        NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
        
        NSArray * channels = [request JSONResponse];
        
        [workerContext performBlock:^{
            
            
            NSMutableArray * channelsMediaArray = [NSMutableArray array];
            
            for (NSDictionary * dictionary in channels)
            {
                TVMediaItem * mediaItem = [[TVMediaItem alloc] initWithDictionary:dictionary];
                if ([mediaItem epgChannelID])
                {
                    APSMediaItem *apsChanenlMediaItem = [APSMediaItem mediaWithdictionary:dictionary inManagedObjectContext:workerContext];
                    [channelsMediaArray addObject:[apsChanenlMediaItem tvMediaItem]];
                }
            }
            //[channel.metaData objectForKey:chanenlIDKey]
            NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

            NSArray * sortedArray =[channelsMediaArray sortedArrayUsingComparator:^NSComparisonResult(APSMediaItem * mediaItem1, APSMediaItem * mediaItem2)
                                    {
                                        NSNumber * prog1 =  [numberFormatter numberFromString:[mediaItem1.metaData objectForKey:chanenlIDKey]];

                                        NSNumber * prog2 =  [numberFormatter numberFromString:[mediaItem2.metaData objectForKey:chanenlIDKey]];
                                        return [prog1 compare: prog2];
                                    }];

            self.arrayChannels = [NSArray arrayWithArray:sortedArray];
            NSError *error;
            if (![workerContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                if (completionBlock)
                {
                    completionBlock();
                }
            });
            
        }];
        
    }];
    
    
    [self sendRequest:request];
    
}


-(NSArray *) allChannels
{
    return self.arrayChannels;
}


-(TVMediaItem *) channelFromChannelID:(NSString *) channelID
{
   
    for (TVMediaItem * channel in  self.arrayChannels)
    {
        if ([channel.epgChannelID isEqualToString:channelID])
        {
            return channel;
        }
    }
    
    return nil;
}




-(APSBlockOperation * ) programForChannelId:(NSString *) channelID
                                       date:(NSDate *) date
                             privateContext:(NSManagedObjectContext *)privateContext
                               starterBlock:(void(^)(void)) starterBlock
                                failedBlock:(void(^)(void)) failedBlock
                            completionBlock:(void(^)(APSTVProgram * program)) completionBlock
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeInterval:-60*60 sinceDate:date]];
    
    
    NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeInterval:+60*60 sinceDate:date]];
    
    componentsStart.minute = 0;
    componentsStart.second = 0;
    
    componentsEnd.minute = 0;
    componentsEnd.second = 0;
    
    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];
    
    
    APSBlockOperation * operation = [self programsByRangeForChannel:channelID fromDate:startDate toDate:endDate actualFromDate:nil actualToDate:nil includeProgramsDuringfromDate:NO includeProgramsDuringToDate:NO starterBlock:starterBlock failedBlock:failedBlock completionBlock:^(NSArray * programs)
    {
        NSDate * minimumInsertDate = [self minimumInsertDate];
        
        NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
        [workerContext performBlock:^{
            
            
            //  get the programs from data base
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            
            NSString * tableName = NSStringFromClass([APSTVProgram class]);
            request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
            request.predicate = [NSPredicate predicateWithFormat:@"(startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )",date,date,channelID,minimumInsertDate];
            
            NSError *executeFetchError = nil;
            NSArray * programs = [workerContext executeFetchRequest:request error:&executeFetchError] ;
            
            
            if (executeFetchError)
            {
                NSLog(@"[%@, %@] error looking up with error: %@",
                      NSStringFromClass([self class]), NSStringFromSelector(_cmd), [executeFetchError localizedDescription]);
                
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if (failedBlock)
                    {
                        failedBlock ();
                    }
                });
            }
            else
            {
                APSTVProgram * program = [programs lastObject];
                NSManagedObjectID * objectID = program.objectID;
               
                    if (completionBlock)
                    {
                        if ([program.name isEqualToString:@"Empty Program!"])
                        {
                             dispatch_sync(dispatch_get_main_queue(), ^{
                            completionBlock(nil);
                             });
                        }
                        else
                        {
                            [privateContext performBlock:^{
                                
                                if(objectID == nil)
                                {
                                    dispatch_sync(dispatch_get_main_queue(), ^{
                                        completionBlock(nil);
                                    });
                                }
                                else
                                {
                                    APSTVProgram * finalProgram = (APSTVProgram *)[privateContext objectWithID:objectID];
                                    dispatch_sync(dispatch_get_main_queue(), ^{
                                        completionBlock(finalProgram);
                                    });
                                    
                                }
                            }];
                        }
                        
                        
                    }
            }
            
        }];

    }];
    
    return operation;
}

-(APSBlockOperation * ) programForChannelId:(NSString *) channelID
                                       date:(NSDate *) date
                               starterBlock:(void(^)(void)) starterBlock
                                failedBlock:(void(^)(void)) failedBlock
                            completionBlock:(void(^)(APSTVProgram * program)) completionBlock
{
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsStart = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeInterval:-60*60 sinceDate:date]];
    
    
    NSDateComponents *componentsEnd = [calendar components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit |NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate dateWithTimeInterval:+60*60 sinceDate:date]];
    
    componentsStart.minute = 0;
    componentsStart.second = 0;
    
    componentsEnd.minute = 0;
    componentsEnd.second = 0;
    
    NSDate * startDate = [calendar dateFromComponents:componentsStart];
    NSDate * endDate = [calendar dateFromComponents:componentsEnd];
    
    
    APSBlockOperation * operation = [self programsByRangeForChannel:channelID fromDate:startDate toDate:endDate actualFromDate:nil actualToDate:nil includeProgramsDuringfromDate:NO includeProgramsDuringToDate:NO starterBlock:starterBlock failedBlock:failedBlock completionBlock:^(NSArray * programs)
                                     {
                                         NSDate * minimumInsertDate = [self minimumInsertDate];
                                         
                                         NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
                                         [workerContext performBlock:^{
                                             
                                             
                                             //  get the programs from data base
                                             NSFetchRequest *request = [[NSFetchRequest alloc] init];
                                             
                                             NSString * tableName = NSStringFromClass([APSTVProgram class]);
                                             request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
                                             request.predicate = [NSPredicate predicateWithFormat:@"(startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )",date,date,channelID,minimumInsertDate];
                                             
                                             NSError *executeFetchError = nil;
                                             NSArray * programs = [workerContext executeFetchRequest:request error:&executeFetchError] ;
                                             
                                             
                                             if (executeFetchError)
                                             {
                                                 NSLog(@"[%@, %@] error looking up with error: %@",
                                                       NSStringFromClass([self class]), NSStringFromSelector(_cmd), [executeFetchError localizedDescription]);
                                                 
                                                 
                                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                                     if (failedBlock)
                                                     {
                                                         failedBlock ();
                                                     }
                                                 });
                                             }
                                             else
                                             {
                                                 APSTVProgram * program = [programs lastObject];
                                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                                     if (completionBlock)
                                                     {
                                                         if ([program.name isEqualToString:@"Empty Program!"])
                                                         {
                                                             completionBlock(nil);
                                                         }
                                                         else
                                                         {
                                                              completionBlock(program);
                                                         }
                                                         
                                                         
                                                     }
                                                 });
                                             }
                                             
                                         }];
                                         
                                     }];
    
    return operation;
}


-(void) programForChannelId:(NSString *) channelID
                       date:(NSDate *) date
                failedBlock:(void(^)(void)) failedBlock
            completionBlock:(void(^)(APSTVProgram *)) completionBlock
{
    
    
    NSDate * minimumInsertDate = [self minimumInsertDate];
    
    NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
    [workerContext performBlock:^{
        
        
        //  get the programs from data base
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        
        NSString * tableName = NSStringFromClass([APSTVProgram class]);
        request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
        request.predicate = [NSPredicate predicateWithFormat:@"(startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )",date,date,channelID,minimumInsertDate];
        
        NSError *executeFetchError = nil;
        NSArray * programs = [workerContext executeFetchRequest:request error:&executeFetchError] ;
        
        
        if (executeFetchError)
        {
            NSLog(@"[%@, %@] error looking up with error: %@",
                  NSStringFromClass([self class]), NSStringFromSelector(_cmd), [executeFetchError localizedDescription]);
            
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (failedBlock)
                {
                    failedBlock ();
                }
            });
        }
        else
        {
            APSTVProgram * program = [programs lastObject];
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (completionBlock)
                {
                    if ([program.name isEqualToString:@"Empty Program!"])
                    {
                        completionBlock(nil);
                    }
                    else
                    {
                        completionBlock(program);
                    }
                    
                    
                }
            });
        }
        
    }];
    
    
    
    
    
}


-(NSArray *) programsForChannelId:(NSString *) channelID
                         fromDate:(NSDate *) fromDate
                           toDate:(NSDate *) toDate
    includeProgramsDuringfromDate:(BOOL) includeStartDate
      includeProgramsDuringToDate:(BOOL) includeEndDate
             managedObjectContext:(NSManagedObjectContext *) managedContext
{
    
    
    NSDate * minimumInsertDate = [self minimumInsertDate];
    //  get the programs from data base
    NSManagedObjectContext *context = managedContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSString * tableName = NSStringFromClass([APSTVProgram class]);
    request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:context];
    
    if (includeStartDate && includeEndDate)
    {//Vd
        
        request.predicate = [NSPredicate predicateWithFormat:@"((startDateTime <= %@) AND (endDateTime >= %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )) ",toDate,fromDate,channelID,minimumInsertDate];
    }
    else if (includeStartDate)
    {//b
        request.predicate = [NSPredicate predicateWithFormat:@"((endDateTime >= %@) AND (endDateTime < %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )) ",fromDate,toDate,channelID,minimumInsertDate];
    }
    else if (includeEndDate)
    {//Vc
        request.predicate = [NSPredicate predicateWithFormat:@"((startDateTime > %@) AND (startDateTime <= %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )) ",fromDate,toDate,channelID,minimumInsertDate];
    }
    else
    {//Va
        request.predicate = [NSPredicate predicateWithFormat:@"((startDateTime > %@) AND (endDateTime < %@) AND (epgChannelID == %@ ) AND (insertionDate > %@ )) ",fromDate,toDate,channelID,minimumInsertDate];
    }
    
    
    
    NSError *executeFetchError = nil;
    NSArray * programs = [context executeFetchRequest:request error:&executeFetchError] ;
    
    if (executeFetchError)
    {
        NSLog(@"[%@, %@] error looking up with error: %@",
              NSStringFromClass([self class]), NSStringFromSelector(_cmd), [executeFetchError localizedDescription]);
        
        return nil;
    }
    else
    {
        return programs;
    }
    
}



-(APSBlockOperation  *) programsByRangeForChannel:(NSString *) channelID
                                             fromDate:(NSDate *) fromDate
                                               toDate:(NSDate *) toDate
                                       actualFromDate:(NSDate *) actualFromDate
                                         actualToDate:(NSDate *) acturalToDate
                        includeProgramsDuringfromDate:(BOOL) includeStartDate
                          includeProgramsDuringToDate:(BOOL) includeEndDate
                                         starterBlock:(void(^)(void)) starterBlock
                                          failedBlock:(void(^)(void)) failedBlock
                                      completionBlock:(void(^)(NSArray *)) completionBlock
{
    
     APSBlockOperation *eezyBlockOperation = [APSBlockOperation blockOperationWithBlock:^{
        
        NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
        
        [workerContext performBlock:^{
            
            // Get programs that not expired
            NSArray * programs = [self programsForChannelId:channelID
                                                   fromDate:fromDate
                                                     toDate:toDate
                              includeProgramsDuringfromDate:YES
                                includeProgramsDuringToDate:YES
                                       managedObjectContext:workerContext];
            
            programs = [programs sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram * obj1, APSTVProgram * obj2) {
                return [obj1.startDateTime compare:obj2.startDateTime];
            }];
            
            // find hole in the programs list, the hole can be because of 2 reasons :
            // 1. This is a new range that we didn't load programs for it before.
            // 2. This is an old range that his programs are already expired.
            
            
            APSRangeArray * programsRangeArry = [APSRangeArray rangeArray];
            
            for (APSTVProgram * program  in programs )
            {
                APSRange * range = [APSRange rangeWithStart:program.startDateTime.timeIntervalSince1970 end:program.endDateTime.timeIntervalSince1970];
                [programsRangeArry addRange:range];
            }
            
            APSRange * requestedRange = [APSRange rangeWithStart:fromDate.timeIntervalSince1970 end:toDate.timeIntervalSince1970];
            BOOL weDontHaveMissingRange =[programsRangeArry isRangeFullyContainedByAnother:requestedRange] != nil;
            
            
            if(weDontHaveMissingRange)
            {
                if (actualFromDate != nil && acturalToDate != nil)
                {
                    NSArray * programs = [self programsForChannelId:channelID
                                                           fromDate:fromDate
                                                             toDate:toDate
                                      includeProgramsDuringfromDate:includeStartDate
                                        includeProgramsDuringToDate:includeEndDate
                                               managedObjectContext:workerContext];
                    //remove all empty programs
                    NSMutableIndexSet * cleanIndexes = [NSMutableIndexSet indexSet];
                    
                    for (int i =0 ; i<programs.count ; i++)
                    {
                        APSTVProgram * program = programs[i];
                        if (![program.name isEqualToString:@"Empty Program!"])
                        {
                            [cleanIndexes addIndex:i];
                        }
                    }
                    programs = [programs objectsAtIndexes:cleanIndexes];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        if (completionBlock){completionBlock(programs);}
                        [self clearInvalidateData];
                    });
                    
                    
                }
                else
                {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        if (completionBlock){completionBlock(nil);}
                        [self clearInvalidateData];
                    });
                }
            }
            else
            {
                if (![eezyBlockOperation isCancelled])
                {
                    
                    //1. download the new range of programs
                    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelProgrammeByDates:channelID pictureSize:[TVPictureSize iPadDetailUnderPlayer105X157] fromDate:[NSString stringWithFormat:@"/Date(%ld000)/",(long)fromDate.timeIntervalSince1970] toODate:[NSString stringWithFormat:@"/Date(%ld000)/",(long)toDate.timeIntervalSince1970] utcOffset:0 delegate:nil];
                    
                    
                    [request setStartedBlock:
                     ^{
                         if (starterBlock)
                         {
                             starterBlock();
                         }
                     }];
                    
                    [request setFailedBlock:^
                     {
                         if (failedBlock)
                         {
                             failedBlock();
                         }
                     }];
                    
                    [request setCompletionBlock:^
                     {
                         TVPAPIRequest * theTequest = request;
                         
                         if (request == nil)
                         {
                             if (completionBlock)
                             {
                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                     completionBlock(nil);
                                 });
                             }
                         }
                         
                         
                         NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
                         
                         [workerContext performBlock:^{
                             
                             //1.delete intersections range programs ( Inner)
                             NSArray * programs = [self programsForChannelId:channelID
                                                                    fromDate:fromDate
                                                                      toDate:toDate
                                               includeProgramsDuringfromDate:NO
                                                 includeProgramsDuringToDate:NO
                                                        managedObjectContext:workerContext];
                             
                             for (APSTVProgram * program in programs)
                             {
                                 [workerContext deleteObject:program];
                             }
                             
                             
                             id JSONResponse = [theTequest JSONResponse];
                             
                             if (JSONResponse && [JSONResponse isKindOfClass:[NSArray class]]) {
                                 
                                 NSArray * programsDictionary = [[theTequest JSONResponse] arrayByRemovingNSNulls];
                                 programsDictionary = [self cleanProgramsDuplication:programsDictionary];
                                 
                                 NSError *executeFetchError = nil;
                                 
                                 //2. delete programs that intersects the range downloaded at the edgs:
                                 if (programsDictionary.count>0)
                                 {
                                     NSString * firstProgramStartDateString =  [[programsDictionary firstObject] objectForKey:TVEPGProgramStartDateKey];
                                     NSString * lastProgramEndDateString = [[programsDictionary lastObject] objectForKey:TVEPGProgramEndDateKey];
                                     
                                     NSDate * firstProgramStartDate = [APSTVProgram dateFromProgramsDateString:firstProgramStartDateString];
                                     NSDate * lastProgramEndDate = [APSTVProgram dateFromProgramsDateString:lastProgramEndDateString];
                                     NSDate * minDate = MIN(firstProgramStartDate, fromDate);
                                     NSDate * maxDate = MAX(lastProgramEndDate, toDate);
                                     
                                     NSFetchRequest *request = [[NSFetchRequest alloc] init];
                                     NSString * tableName = NSStringFromClass([APSTVProgram class]);
                                     request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
                                     request.predicate = [NSPredicate predicateWithFormat:@"((startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ )) OR ((startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ ))",minDate,minDate,channelID,maxDate,maxDate,channelID];
                                     
                                     NSArray * programsIntersectRange = [workerContext executeFetchRequest:request error:&executeFetchError];
                                     for (APSTVProgram * program in programsIntersectRange)
                                     {
                                         [workerContext deleteObject:program];
                                     }
                                 }
                                 
                                 
                                 
                                 NSError *error;
                                 if (![workerContext save:&error]) {
                                     NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                     
                                 }
                                 
                                 
                                 
                                 //3.1 sorting the new programs by epg id
                                 NSArray * newProgramsArray = [programsDictionary sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSDictionary * obj1, NSDictionary * obj2) {
                                     return [[obj1 objectForKey:TVEPGProgramEPGIDKey] compare:[obj2 objectForKey:TVEPGProgramEPGIDKey]];
                                 }];
                                 
                                 //3.2 find all programs in the data base with the same id like the new ones
                                 
                                 //3.2.1 create array of the new ids
                                 NSMutableArray * newProgramIds = [NSMutableArray array];
                                 [newProgramsArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
                                     [newProgramIds addObject:[obj objectForKey:TVEPGProgramEPGIDKey]];
                                 }];
                                 // feching all duplicated programs
                                 
                                 NSDate * minimumInsertDate = [self minimumInsertDate];
                                 
                                 NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                                 fetchRequest.entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:workerContext];
                                 NSString * propertyKey = NSStringFromProperty(epgIdentifier);
                                 fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K IN %@ AND (insertionDate > %@ )", propertyKey,newProgramIds,minimumInsertDate];
                                 NSArray * programsToUpdateArray = [[workerContext executeFetchRequest:fetchRequest error:&executeFetchError] sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(APSTVProgram * obj1, APSTVProgram * obj2) {
                                     return [obj1.epgId compare:obj2.epgId];
                                 }];
                                 
                                 // 3.3 enumerate new program and for each one:
                                 // if the update programs is contains this program - update
                                 // else add it as a new object
                                 
                                 APSRangeArray * newProgramsRangeArray = [APSRangeArray alloc];
                                 
                                 NSInteger programsToUpdateIndex = 0;
                                 for (NSDictionary * dictionary in newProgramsArray)
                                 {
                                     APSTVProgram * theNewProgram =  nil;
                                     //                                 NSString * epgIDOfNewProgram = [dictionary objectForKey:TVEPGProgramEPGIDKey];
                                     NSString * epgIDOfNewProgram = [NSString stringWithFormat:@"%@",[dictionary objectForKey:TVEPGProgramEPGIDKey]];
                                     
                                     APSTVProgram * theNextProgramToUpdate =  programsToUpdateIndex<programsToUpdateArray.count?programsToUpdateArray[programsToUpdateIndex]:nil;
                                     
                                     if (programsToUpdateIndex<programsToUpdateArray.count && [epgIDOfNewProgram isEqualToString:theNextProgramToUpdate.epgId])
                                     {
                                         // to update
                                         theNewProgram  = theNextProgramToUpdate;
                                         [theNewProgram setdictionary:dictionary];
                                         // updating the insetion date to mark this as a new data.
                                         theNewProgram.insertionDate = [NSDate date];
                                         programsToUpdateIndex++;
                                         
                                         
                                     }
                                     else
                                     {
                                         theNewProgram =[APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                     }
                                     
                                     APSRange * range = [APSRange rangeWithStart:theNewProgram.startDateTime.timeIntervalSince1970 end:theNewProgram.endDateTime.timeIntervalSince1970];
                                     [newProgramsRangeArray addRange:range];
                                 }
                                 
                                 
                                 NSArray * intersectionRanges = [newProgramsRangeArray intersection:requestedRange];
                                 
                                 // adding empty programs
                                 // handle the first range if it has
                                 APSRange * firstRange = [intersectionRanges firstObject];
                                 if(firstRange.start - requestedRange.start >0)
                                 {
                                     NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                     APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                     newProgram.epgChannelID = channelID;
                                     newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:requestedRange.start];
                                     newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:firstRange.start];
                                     
                                 }
                                 
                                 //handle th elast gap if it has
                                 APSRange * lastRange = [intersectionRanges lastObject];
                                 if(requestedRange.end - lastRange.end >0)
                                 {
                                     NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                     APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                     newProgram.epgChannelID = channelID;
                                     newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:lastRange.end];
                                     newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:requestedRange.end];
                                     
                                 }
                                 
                                 // handle middle area
                                 
                                 
                                 for (NSInteger i=0 ;intersectionRanges.count!=0 && i<intersectionRanges.count-1 ; i++)
                                 {
                                     APSRange * partner1 = intersectionRanges[i];
                                     APSRange * partner2 = intersectionRanges[i+1];
                                     if (partner2.start - partner1.end >0)
                                     {
                                         NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                         APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                         newProgram.epgChannelID = channelID;
                                         newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:partner1.end];
                                         newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:partner2.start];
                                         
                                     }
                                 }
                                 
                                 
                                 
                                 
                                 
                                 if (![workerContext save:&error]) {
                                     NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                     
                                 }
                                 
                                 
                                 if (actualFromDate != nil && acturalToDate!= nil)
                                 {
                                     NSArray * resultPrograms = [self programsForChannelId:channelID
                                                                                  fromDate:actualFromDate
                                                                                    toDate:acturalToDate
                                                             includeProgramsDuringfromDate:includeStartDate
                                                               includeProgramsDuringToDate:includeEndDate
                                                                      managedObjectContext:workerContext];
                                     
                                     // remove all clear programs
                                     NSMutableIndexSet * cleanIndexes = [NSMutableIndexSet indexSet];
                                     
                                     for (int i =0 ; i<resultPrograms.count ; i++)
                                     {
                                         APSTVProgram * program = resultPrograms[i];
                                         if (![program.name isEqualToString:@"Empty Program!"])
                                         {
                                             [cleanIndexes addIndex:i];
                                         }
                                     }
                                     resultPrograms = [resultPrograms objectsAtIndexes:cleanIndexes];
                                     
                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                         
                                         if (completionBlock){completionBlock(resultPrograms);}
                                         [self clearInvalidateData];
                                     });
                                     
                                 }
                                 else
                                 {
                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                         
                                         if (completionBlock){completionBlock(nil);}
                                         [self clearInvalidateData];
                                     });
                                 }

                             }
                             
                             
                         }]; // end of second worker context
                         
                         
                     }]; // end of completion block
                    
                    eezyBlockOperation.request =  request;
                    [self sendRequest:request];
                }
                else
                {
                    NSLog(@"operation is canceled");
                }
                
            }
            
        }];// end of worker context
        
    }];
    
    [self.operationQueue addOperation:eezyBlockOperation];
    return eezyBlockOperation;
}



-(NSDate *) minimumInsertDate
{
    NSInteger invalidateCycleTimeHoures =
    [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] clearCacheExpireTimeByHour];
    NSDate * minimumInsertDate =
    //[NSDate dateWithTimeIntervalSinceNow:-60];
    [NSDate dateWithTimeIntervalSinceNow:-invalidateCycleTimeHoures*60*60];
    return minimumInsertDate;
}


-(NSArray *) cleanProgramsDuplication:(NSArray *) programsDictionaries
{
    NSMutableDictionary * dictionaryClenedFromDuplication = [NSMutableDictionary dictionary];
    for (NSDictionary * dict in programsDictionaries)
    {
        [dictionaryClenedFromDuplication setObject:dict forKey:[dict objectForKey:TVEPGProgramEPGIdentifierKey]];
    }
    
    NSArray * cleanedPrograms = [dictionaryClenedFromDuplication allValues];
    cleanedPrograms = [cleanedPrograms sortedArrayUsingComparator:^NSComparisonResult(NSDictionary * obj1, NSDictionary * obj2) {
        
        NSString * program1StartDateString =  [obj1 objectForKey:TVEPGProgramStartDateKey];
        NSString * program2StartDateString = [obj2 objectForKey:TVEPGProgramStartDateKey];
        
        NSDate * program1StartDate = [APSTVProgram dateFromProgramsDateString:program1StartDateString];
        NSDate * program2StartDate = [APSTVProgram dateFromProgramsDateString:program2StartDateString];
        
        return [program1StartDate compare:program2StartDate];
        
    }];
    
    return cleanedPrograms;
    
}


-( APSBlockOperation * ) programsByRangeForChannel:(NSString *) channelID
                                          fromDate:(NSDate *) fromDate
                                            toDate:(NSDate *) toDate
                                    actualFromDate:(NSDate *) actualFromDate
                                      actualToDate:(NSDate *) acturalToDate
                     includeProgramsDuringfromDate:(BOOL) includeStartDate
                       includeProgramsDuringToDate:(BOOL) includeEndDate
                                    privateContext:(NSManagedObjectContext *) privateContext
                                      starterBlock:(void(^)(void)) starterBlock
                                       failedBlock:(void(^)(void)) failedBlock
                                   completionBlock:(void(^)(NSArray *)) completionBlock
{
    
    
    APSBlockOperation * theOperation = [[APSBlockOperation alloc] init];
    __weak APSBlockOperation *eezyBlockOperation = theOperation;
    __weak APSEPGManager * weakSelf = self;
        [theOperation addExecutionBlock:^{
        NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
        
        [workerContext performBlock:^{
            
            
            // Get programs that not expired
            NSArray * programs = [weakSelf programsForChannelId:channelID
                                                   fromDate:fromDate
                                                     toDate:toDate
                              includeProgramsDuringfromDate:YES
                                includeProgramsDuringToDate:YES
                                       managedObjectContext:workerContext];
            
            programs = [programs sortedArrayUsingComparator:^NSComparisonResult(APSTVProgram * obj1, APSTVProgram * obj2) {
                return [obj1.startDateTime compare:obj2.startDateTime];
            }];
            
            // find hole in the programs list, the hole can be because of 2 reasons :
            // 1. This is a new range that we didn't load programs for it before.
            // 2. This is an old range that his programs are already expired.
            
            
            APSRangeArray * programsRangeArry = [APSRangeArray rangeArray];
            
            for (APSTVProgram * program  in programs )
            {
                APSRange * range = [APSRange rangeWithStart:program.startDateTime.timeIntervalSince1970 end:program.endDateTime.timeIntervalSince1970];
                [programsRangeArry addRange:range];
            }
            
            APSRange * requestedRange = [APSRange rangeWithStart:fromDate.timeIntervalSince1970 end:toDate.timeIntervalSince1970];
            BOOL weDontHaveMissingRange =[programsRangeArry isRangeFullyContainedByAnother:requestedRange] != nil;
            
            
            if(weDontHaveMissingRange)
            {
                if (actualFromDate != nil && acturalToDate != nil)
                {
                    NSArray * programs = [weakSelf programsForChannelId:channelID
                                                           fromDate:actualFromDate
                                                             toDate:acturalToDate
                                      includeProgramsDuringfromDate:includeStartDate
                                        includeProgramsDuringToDate:includeEndDate
                                               managedObjectContext:workerContext];
                    //remove all empty programs
                    NSMutableIndexSet * cleanIndexes = [NSMutableIndexSet indexSet];
                    
                    for (int i =0 ; i<programs.count ; i++)
                    {
                        APSTVProgram * program = programs[i];
                        if (![program.name isEqualToString:@"Empty Program!"])
                        {
                            [cleanIndexes addIndex:i];
                        }
                    }
                    programs = [programs objectsAtIndexes:cleanIndexes];
                    
                    
                    NSMutableArray  * finalProgramsIDs = [NSMutableArray array];
                    for (APSTVProgram * program in programs)
                    {
                        [finalProgramsIDs addObject:program.objectID];
                    }
                    
                    [privateContext performBlock:^{
                        NSMutableArray * finalPrograms = [NSMutableArray array];
                        for (NSManagedObjectID * objectID in finalProgramsIDs)
                        {
                            APSTVProgram * program = (APSTVProgram *)[privateContext objectWithID:objectID];
                            [finalPrograms addObject:program];
                        }
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            
                            if (completionBlock){completionBlock(finalPrograms);}
                            [weakSelf clearInvalidateData];
                        });

                    }];
                }
                else
                {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        if (completionBlock){completionBlock(nil);}
                        [weakSelf clearInvalidateData];
                    });
                }
            }
            else
            {
                if (![eezyBlockOperation isCancelled])
                {
                    
                    //1. download the new range of programs
                    __weak TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGChannelProgrammeByDates:channelID pictureSize:[TVPictureSize iPadDetailUnderPlayer105X157] fromDate:[NSString stringWithFormat:@"/Date(%ld000)/",(long)fromDate.timeIntervalSince1970] toODate:[NSString stringWithFormat:@"/Date(%ld000)/",(long)toDate.timeIntervalSince1970] utcOffset:0 delegate:nil];
                    
                    
                    [request setStartedBlock:
                     ^{
                         if (starterBlock)
                         {
                             starterBlock();
                         }
                     }];
                    
                    [request setFailedBlock:^
                     {
                         if (failedBlock)
                         {
                             failedBlock();
                         }
                     }];
                    
                    [request setCompletionBlock:^
                     {
                         TVPAPIRequest * theTequest = request;
                         
                         if (request == nil)
                         {
                             if (completionBlock)
                             {
                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                     completionBlock(nil);
                                 });
                             }
                         }
                         
                         
                         NSManagedObjectContext *workerContext = [APSCoreDataStore defaultPrivateQueueContext];
                         
                         [workerContext performBlock:^{
                             
                             //1.delete intersections range programs ( Inner)
                             NSArray * programs = [weakSelf programsForChannelId:channelID
                                                                    fromDate:fromDate
                                                                      toDate:toDate
                                               includeProgramsDuringfromDate:NO
                                                 includeProgramsDuringToDate:NO
                                                        managedObjectContext:workerContext];
                             
                             for (APSTVProgram * program in programs)
                             {
                                 [workerContext deleteObject:program];
                             }
                             
                             
                             
                             NSArray * programsDictionary = [[theTequest JSONResponse] arrayByRemovingNSNulls];
                             programsDictionary = [weakSelf cleanProgramsDuplication:programsDictionary];
                             
                             NSError *executeFetchError = nil;
                             
                             //2. delete programs that intersects the range downloaded at the edgs:
                             if (programsDictionary.count>0)
                             {
                                 NSString * firstProgramStartDateString =  [[programsDictionary firstObject] objectForKey:TVEPGProgramStartDateKey];
                                 NSString * lastProgramEndDateString = [[programsDictionary lastObject] objectForKey:TVEPGProgramEndDateKey];
                                 
                                 NSDate * firstProgramStartDate = [APSTVProgram dateFromProgramsDateString:firstProgramStartDateString];
                                 NSDate * lastProgramEndDate = [APSTVProgram dateFromProgramsDateString:lastProgramEndDateString];
                                 NSDate * minDate = MIN(firstProgramStartDate, fromDate);
                                 NSDate * maxDate = MAX(lastProgramEndDate, toDate);
                                 
                                 NSFetchRequest *request = [[NSFetchRequest alloc] init];
                                 NSString * tableName = NSStringFromClass([APSTVProgram class]);
                                 request.entity = [NSEntityDescription entityForName:tableName inManagedObjectContext:workerContext];
                                 request.predicate = [NSPredicate predicateWithFormat:@"((startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ )) OR ((startDateTime <= %@) AND (endDateTime > %@) AND (epgChannelID == %@ ))",minDate,minDate,channelID,maxDate,maxDate,channelID];
                                 
                                 NSArray * programsIntersectRange = [workerContext executeFetchRequest:request error:&executeFetchError];
                                 for (APSTVProgram * program in programsIntersectRange)
                                 {
                                     [workerContext deleteObject:program];
                                 }
                             }
                             
                             
                             
                             NSError *error;
                             if (![workerContext save:&error]) {
                                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                 
                             }
                             
                             
                             
                             //3.1 sorting the new programs by epg id
                             NSArray * newProgramsArray = [programsDictionary sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSDictionary * obj1, NSDictionary * obj2) {
                                 return [[obj1 objectForKey:TVEPGProgramEPGIDKey] compare:[obj2 objectForKey:TVEPGProgramEPGIDKey]];
                             }];
                             
                             //3.2 find all programs in the data base with the same id like the new ones
                             
                             //3.2.1 create array of the new ids
                             NSMutableArray * newProgramIds = [NSMutableArray array];
                             [newProgramsArray enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
                                 [newProgramIds addObject:[obj objectForKey:TVEPGProgramEPGIDKey]];
                             }];
                             // feching all duplicated programs
                             
                             NSDate * minimumInsertDate = [weakSelf minimumInsertDate];
                             
                             NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                             fetchRequest.entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:workerContext];
                             NSString * propertyKey = NSStringFromProperty(epgIdentifier);
                             fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K IN %@ AND (insertionDate > %@ )", propertyKey,newProgramIds,minimumInsertDate];
                             NSArray * programsToUpdateArray = [[workerContext executeFetchRequest:fetchRequest error:&executeFetchError] sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(APSTVProgram * obj1, APSTVProgram * obj2) {
                                 return [obj1.epgId compare:obj2.epgId];
                             }];
                             
                             // 3.3 enumerate new program and for each one:
                             // if the update programs is contains this program - update
                             // else add it as a new object
                             
                             APSRangeArray * newProgramsRangeArray = [APSRangeArray alloc];
                             
                             NSInteger programsToUpdateIndex = 0;
                             for (NSDictionary * dictionary in newProgramsArray)
                             {
                                 APSTVProgram * theNewProgram =  nil;
//                                 NSString * epgIDOfNewProgram = [dictionary objectForKey:TVEPGProgramEPGIDKey];
                                 NSString * epgIDOfNewProgram = [NSString stringWithFormat:@"%@",[dictionary objectForKey:TVEPGProgramEPGIDKey]];
                                 APSTVProgram * theNextProgramToUpdate =  programsToUpdateIndex<programsToUpdateArray.count?programsToUpdateArray[programsToUpdateIndex]:nil;
                                 
                                 if (programsToUpdateIndex<programsToUpdateArray.count && [epgIDOfNewProgram isEqualToString:theNextProgramToUpdate.epgId])
                                 {
                                     // to update
                                     theNewProgram  = theNextProgramToUpdate;
                                     [theNewProgram setdictionary:dictionary];
                                     // updating the insetion date to mark this as a new data.
                                     theNewProgram.insertionDate = [NSDate date];
                                     programsToUpdateIndex++;
                                     
                                     
                                 }
                                 else
                                 {
                                     theNewProgram =[APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                 }
                                 
                                 APSRange * range = [APSRange rangeWithStart:theNewProgram.startDateTime.timeIntervalSince1970 end:theNewProgram.endDateTime.timeIntervalSince1970];
                                 [newProgramsRangeArray addRange:range];
                             }
                             
                             
                             NSArray * intersectionRanges = [newProgramsRangeArray intersection:requestedRange];
                             
                             // adding empty programs
                             // handle the first range if it has
                             APSRange * firstRange = [intersectionRanges firstObject];
                             if(firstRange.start - requestedRange.start >0)
                             {
                                 NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                 APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                 newProgram.epgChannelID = channelID;
                                 newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:requestedRange.start];
                                 newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:firstRange.start];
                                 
                             }
                             
                             //handle th elast gap if it has
                             APSRange * lastRange = [intersectionRanges lastObject];
                             if(requestedRange.end - lastRange.end >0)
                             {
                                 NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                 APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                 newProgram.epgChannelID = channelID;
                                 newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:lastRange.end];
                                 newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:requestedRange.end];
                                 
                             }
                             
                             // handle middle area
                             
                             
                             for (NSInteger i=0 ;intersectionRanges.count!=0 && i<intersectionRanges.count-1 ; i++)
                             {
                                 APSRange * partner1 = intersectionRanges[i];
                                 APSRange * partner2 = intersectionRanges[i+1];
                                 if (partner2.start - partner1.end >0)
                                 {
                                     NSDictionary * dictionary = [NSDictionary dictionaryWithObject:@"Empty Program!" forKey:TVEPGProgramNameKey];
                                     APSTVProgram * newProgram = [APSTVProgram programWithdictionary:dictionary inManagedObjectContext:workerContext];
                                     newProgram.epgChannelID = channelID;
                                     newProgram.startDateTime = [NSDate dateWithTimeIntervalSince1970:partner1.end];
                                     newProgram.endDateTime = [NSDate dateWithTimeIntervalSince1970:partner2.start];
                                     
                                 }
                             }
                             
                             
                             
                             
                             
                             if (![workerContext save:&error]) {
                                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                                 
                             }
                             
                             
                             if (actualFromDate != nil && acturalToDate!= nil)
                             {
                                 NSArray * resultPrograms = [weakSelf programsForChannelId:channelID
                                                                              fromDate:actualFromDate
                                                                                toDate:acturalToDate
                                                         includeProgramsDuringfromDate:includeStartDate
                                                           includeProgramsDuringToDate:includeEndDate
                                                                  managedObjectContext:workerContext];
                                 
                                 // remove all clear programs
                                 NSMutableIndexSet * cleanIndexes = [NSMutableIndexSet indexSet];
                                 
                                 for (int i =0 ; i<resultPrograms.count ; i++)
                                 {
                                     APSTVProgram * program = resultPrograms[i];
                                     if (![program.name isEqualToString:@"Empty Program!"])
                                     {
                                         [cleanIndexes addIndex:i];
                                     }
                                 }
                                 resultPrograms = [resultPrograms objectsAtIndexes:cleanIndexes];
                                 
                                 NSMutableArray  * finalProgramsIDs = [NSMutableArray array];
                                 for (APSTVProgram * program in resultPrograms)
                                 {
                                     [finalProgramsIDs addObject:program.objectID];
                                 }
                                 
                                 
                                 [privateContext performBlock:^{
                                     __block NSMutableArray * finalPrograms = [NSMutableArray array];
                                     for (NSManagedObjectID * objectID in finalProgramsIDs)
                                     {
                                         APSTVProgram * program = (APSTVProgram *)[privateContext objectWithID:objectID];
                                         [finalPrograms addObject:program];
                                     }
                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                         
                                         if (completionBlock){completionBlock(finalPrograms);}
                                         [weakSelf clearInvalidateData];
                                     });
                                     
                                 }];
                                
                                 
                             }
                             else
                             {
                                 dispatch_sync(dispatch_get_main_queue(), ^{
                                     
                                     if (completionBlock){completionBlock(nil);}
                                     [weakSelf clearInvalidateData];
                                 });
                             }
                             
                             
                             
                         }]; // end of second worker context
                         
                         
                     }]; // end of completion block
                    
                    
                        eezyBlockOperation.request =  request;
                        [weakSelf sendRequest:request];

                    
                }
                else
                {
                    NSLog(@"operation is canceled");
                }
                
            }
            
        }];// end of worker context
        
    }];
    
    [self.operationQueue addOperation:eezyBlockOperation];
    return eezyBlockOperation;
}

@end
