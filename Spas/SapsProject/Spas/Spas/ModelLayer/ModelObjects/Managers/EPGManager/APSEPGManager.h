//
//  APSEPGManager.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSBaseNetworkObject.h"

#define APSEPGManagerCurrentProgramDidChangedNotificationName @"CurrentProgramDidChanged"


@class APSTVChannel;
@class APSBlockOperation;

@interface APSEPGManager : APSBaseNetworkObject


+ (instancetype)sharedEPGManager;



/**
 *	this function is nececcery for downlading channels , without calling this function channels will not be downloaded
 */
-(void) loadChannelsWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;


/**
 *
 *  @return all epg channels as TVMediaItem 
 *  Channels are alwase in the application and the loaded on the start app
 */
-(NSArray *) allChannels;


/**
 *	getEPGChannelFromChannelID
 *
 *	@param	channelID
 *
 *	@return	EPG Channel belongs to the epg id
 */
-(TVMediaItem *) channelFromChannelID:(NSString *) channelID;


/**
 *
 *
 *	@param	channelID - channel's program
 *
 *	@return	the program currntly running on this channel
 */
//-(APSBlockOperation * ) programForChannelId:(NSString *) channelID
//                                       date:(NSDate *) date
//                               starterBlock:(void(^)(void)) starterBlock
//                                failedBlock:(void(^)(void)) failedBlock
//                            completionBlock:(void(^)(APSTVProgram * program)) completionBlock;

-(APSBlockOperation * ) programForChannelId:(NSString *) channelID
                                       date:(NSDate *) date
                             privateContext:(NSManagedObjectContext *) privateContext
                               starterBlock:(void(^)(void)) starterBlock
                                failedBlock:(void(^)(void)) failedBlock
                            completionBlock:(void(^)(APSTVProgram * program)) completionBlock;




//-( APSBlockOperation * ) programsByRangeForChannel:(NSString *) channelID
//                                         fromDate:(NSDate *) fromDate
//                                           toDate:(NSDate *) toDate
//                                   actualFromDate:(NSDate *) actualFromDate
//                                     actualToDate:(NSDate *) acturalToDate
//                    includeProgramsDuringfromDate:(BOOL) includeStartDate
//                      includeProgramsDuringToDate:(BOOL) includeEndDate
//                                     starterBlock:(void(^)(void)) starterBlock
//                                      failedBlock:(void(^)(void)) failedBlock
//                                  completionBlock:(void(^)(NSArray *)) completionBlock;

-( APSBlockOperation * ) programsByRangeForChannel:(NSString *) channelID
                                          fromDate:(NSDate *) fromDate
                                            toDate:(NSDate *) toDate
                                    actualFromDate:(NSDate *) actualFromDate
                                      actualToDate:(NSDate *) acturalToDate
                     includeProgramsDuringfromDate:(BOOL) includeStartDate
                       includeProgramsDuringToDate:(BOOL) includeEndDate
                                    privateContext:(NSManagedObjectContext *) privateContext
                                      starterBlock:(void(^)(void)) starterBlock
                                       failedBlock:(void(^)(void)) failedBlock
                                   completionBlock:(void(^)(NSArray *)) completionBlock;


@end
