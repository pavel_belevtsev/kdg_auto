//
//  APSRange.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSRange : NSObject

+(APSRange *) rangeWithStart:(NSInteger)start end:(NSInteger)end;

@property (retain, nonatomic) NSDate * insertDate;
@property (assign, nonatomic) NSInteger start;
@property (assign, nonatomic) NSInteger end;


-(NSComparisonResult) compare:(APSRange *) range;



@end
