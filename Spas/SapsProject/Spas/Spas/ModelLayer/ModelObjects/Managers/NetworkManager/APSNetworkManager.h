//
//  YESNetworkManager.h
//  YES_iPhone
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSNetworkTracker.h"



@interface APSNetworkManager : NSObject

@property BOOL isAlertShow;

+ (APSNetworkManager *) sharedNetworkManager;
-(void) trackNetworkChanges;
-(void) checkCurrentNetworkStatus;
-(APSNetworkStatus)currentNetworkStatus;
@end
