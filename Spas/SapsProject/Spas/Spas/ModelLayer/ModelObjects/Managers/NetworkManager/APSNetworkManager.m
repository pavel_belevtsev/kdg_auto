//
//  YESNetworkManager.m
//  YES_iPhone
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "APSNetworkManager.h"
#import "APSNetworkTracker.h"

#define tagTo3G 333

@interface APSNetworkManager ()<UIAlertViewDelegate>

@end

@implementation APSNetworkManager


-(APSNetworkStatus)currentNetworkStatus
{
    [[APSNetworkTracker sharedNetworkStatusManager] updateStatus];
    APSNetworkStatus status = [[APSNetworkTracker sharedNetworkStatusManager] status];
    return status;
}

static APSNetworkManager * sharedInstance = nil;

+ (APSNetworkManager *) sharedNetworkManager
{
    if (sharedInstance ==nil)
    {
        @synchronized(self)
        {
            sharedInstance = [[APSNetworkManager alloc] init];
            sharedInstance.isAlertShow = NO;
        }
    }
    return  sharedInstance;
}


-(void) trackNetworkChanges
{
    [[APSNetworkTracker sharedNetworkStatusManager] startTracking];
}

-(void) dealloc
{
    [[APSNetworkTracker sharedNetworkStatusManager] stopTracking];
}



@end
