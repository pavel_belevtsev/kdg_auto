//
//  APSConfigurationManager.m
//  Spas
//
//  Created by Rivka Peleg on 9/21/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSConfigurationManager.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation APSConfigurationManager



-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    NSLog(@"conf dictionary = %@",dictionary);
    @synchronized(self)
    {
        NSDictionary *params = [dictionary objectOrNilForKey:@"params"];
        self.clearCacheExpireTimeByHour = [[params objectOrNilForKey:@"EPGTtl"] integerValue];
        
        NSArray *ineoQuestInfoArray = [params objectOrNilForKey:@"Ineoquest"];
        
        if ([ineoQuestInfoArray count]>0)
        {
            self.appHaveineoQuest = YES;
            NSDictionary * ineoQuestInfoDic = [self parsArrayOfPairsToOneDictionary:ineoQuestInfoArray];
           
            self.ineoQuestHost = [ineoQuestInfoDic objectOrNilForKey:@"Host"];
            self.ineoQuestPort = [[ineoQuestInfoDic objectOrNilForKey:@"Port"] integerValue];
            self.ineoApi_key =   [ineoQuestInfoDic objectOrNilForKey:@"Api_key"];

            NSString * dis = [ineoQuestInfoDic objectOrNilForKey:@"DisableSSL"];
            self.disableSSL = [dis isEqualToString:@"Yes"] ? YES : NO;
            NSLog(@"---- \n ineoQuestHost = %@ \n ineoQuestPort = %ld \n ineoApi_key = %@ \n disableSSL = %d \n ----",self.ineoQuestHost,(long)self.ineoQuestPort,self.ineoApi_key,self.disableSSL);
            
            if (self.ineoQuestHost == nil || [self.ineoQuestHost isEqualToString:@""] || self.ineoApi_key == nil || [self.ineoApi_key isEqualToString:@""] ||  self.ineoQuestPort == 0 )
            {
                self.appHaveineoQuest = NO;
            }
        }
        else
        {
            self.appHaveineoQuest = NO;
        }
        
        if ([params objectOrNilForKey:@"app_refresh_frequency"]) {
            self.refreshFrequency = [[params objectOrNilForKey:@"app_refresh_frequency"] integerValue];
        }  else {
            self.refreshFrequency = 12; // 12 Hours default value
        }
        
        self.refreshFrequency *= 60; // In Minutes
        
        NSString *crashlytics = [params objectOrNilForKey:@"Crashlytics"];
     
        if (crashlytics && [crashlytics isKindOfClass:[NSString class]] && [crashlytics isEqualToString:@"true"]) {
            
            [Fabric with:@[[Crashlytics class]]];
            

        }
    }
}

-(NSDictionary *) parsArrayOfPairsToOneDictionary:(NSArray *) array
{
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
    
    for (NSDictionary * pair in array)
    {
        NSString * theKey = [pair.allKeys lastObject];
        if([pair objectForKey:theKey])
        {
            [dictionary setObject:[pair objectForKey:theKey] forKey:theKey];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (void)resetRefreshAppDataDate {
    
    self.refreshDate = [NSDate date];
    
}

@end
