//
//  APSConfigurationManager.h
//  Spas
//
//  Created by Rivka Peleg on 9/21/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface APSConfigurationManager : TVConfigurationManager

- (void)resetRefreshAppDataDate;

@property (assign, nonatomic) NSInteger clearCacheExpireTimeByHour;

@property (nonatomic, retain) NSString * ineoQuestHost;
@property (nonatomic, retain) NSString * ineoApi_key;
@property (nonatomic, assign) NSInteger  ineoQuestPort;
@property (nonatomic, assign) BOOL disableSSL;

@property BOOL appHaveineoQuest;

@property (nonatomic, retain) NSDate * refreshDate;
@property (nonatomic, assign) NSInteger refreshFrequency;

@end
