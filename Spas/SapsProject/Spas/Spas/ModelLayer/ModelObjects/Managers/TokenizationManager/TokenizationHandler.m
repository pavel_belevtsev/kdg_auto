//
//  TokenizationHandler.m
//  Spas
//
//  Created by Rivka Schwartz on 7/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TokenizationHandler.h"
#import <TvinciSDK/TVTokenizationManager.h>
#import <TvinciSDK/TVSessionManager.h>
//#import "UIAlertView+Blocks.h"
#import "APSSessionManager.h"

NSString * const TokenizationHandlerExpiredNotification = @"TokenizationHandlerExpiredNotification";;

@interface TokenizationHandler ()

@end

@implementation TokenizationHandler

+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id)init {
    
    self = [super init];
    if (self)
    {
        self.sessionAdapter = [[APSSessionAdapter alloc] init];
        [self.sessionAdapter setAssertBlock];
        self.sessionAdapter.canChangeUserLoginStatus = YES;
        
    }
    
    return self;
    
}

-(void) registerForNotification
{

    [self unregisterForNotification];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reauthenticationIsNeededAfterApplicationLaunched:) name:TVSessionManagerAuthenticationExpiredNotification object:nil];

}

-(void) unregisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) appDidEnterForeground:(NSNotification *) notification
{
    // check authntication status on coming from background event
    [self refreshTokenIfNeededAndCalledFromAppLaunchedPoint:NO withStartBlock:nil completionBlock:nil];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) refreshTokenIfNeededAndCalledFromAppLaunchedPoint:(BOOL) isAppLaunchedPoint withStartBlock:(void(^)()) startBlock completionBlock:(void(^)(BOOL showLoginScreen)) completionBlock;
{
    
    [[[TVSessionManager sharedTVSessionManager] tokenizationManager] refreshTokenIfNeededConsiderSafetyMargin:YES owner:nil startBlock:startBlock failedBlock:^{

        if (isAppLaunchedPoint)
        {
            
            [self.sessionAdapter logoutWithSuccessBlock:^{
                
                [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];;
                
                if (completionBlock)
                {
                    completionBlock(YES);
                }
                
                [self registerForNotification];
                
            } failedBlock:^(NSString *failedError) {
                
                if (completionBlock)
                {
                    completionBlock(YES);
                }
                
                [self registerForNotification];
                
            }];
            
        }
    } completionBlock:^{

        if (completionBlock)
        {
            completionBlock(NO);
        }

        if (isAppLaunchedPoint)
        {
            [self registerForNotification];
        }
    }];
}


- (void) reauthenticationIsNeededAfterApplicationLaunched:(NSNotification *) notification
{
    
    
    NSLog(@"reauthenticationIsNeededAfterApplicationLaunched");
 
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TokenExpired_title",nil)
                                                        message:nil delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
    
    alertView.delegate = self;
    [alertView show];
    
/*
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:LS(@"TokenExpired_title")
                                                        message:LS(@"TokenExpired_content") delegate:nil cancelButtonTitle:LS(@"ok") otherButtonTitles:nil];

    [alertView showWithCompletion:^(UIAlertView *alertView, NSInteger buttonIndex) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KLogoutNotification object:nil];
    }];
*/
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    [self.sessionAdapter logoutWithSuccessBlock:^{
        
        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TokenizationHandlerExpiredNotification object:nil];
        
        NSLog(@"log out succeded");
    } failedBlock:^(NSString *failedError) {
        NSLog(@"log out failed");
    }];
    
}


@end
