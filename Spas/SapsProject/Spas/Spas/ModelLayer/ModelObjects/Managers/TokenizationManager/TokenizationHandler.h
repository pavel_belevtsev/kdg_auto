//
//  TokenizationHandler.h
//  Spas
//
//  Created by Rivka Schwartz on 7/9/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSSessionAdapter.h"

@interface TokenizationHandler : NSObject

extern NSString * const TokenizationHandlerExpiredNotification;

+ (id) sharedInstance;
-(void) refreshTokenIfNeededAndCalledFromAppLaunchedPoint:(BOOL) isAppLaunchedPoint withStartBlock:(void(^)()) startBlock completionBlock:(void(^)(BOOL showLoginScreen)) completionBlock ;

@property (retain, nonatomic) APSSessionAdapter * sessionAdapter;

@end
