//
//  APSBaseManager.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APSBaseNetworkObject : NSObject

@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;
-(void) sendRequest:(TVPAPIRequest *)request;

@end
