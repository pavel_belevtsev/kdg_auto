//
//  APSLaunchManager.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSBaseNetworkObject.h"
#import "APSSessionAdapter.h"

typedef NS_ENUM(NSInteger, APSLaunchFailedReason) {
    APSLaunchFailedReasonUndefined              = 0,
    APSLaunchFailedReasonNoInternetConnection   = 1
};

@interface APSLaunchProcess : APSBaseNetworkObject

-(void) startLaunchingAppWithStartBlock:(void(^)(void)) startBlock
                            failedBlock:(void(^)(APSLaunchFailedReason reason)) failedBlock
                        completionBlock:(void(^)(void)) completionBlock;

+(APSLaunchProcess *) launchProcess;

@end
