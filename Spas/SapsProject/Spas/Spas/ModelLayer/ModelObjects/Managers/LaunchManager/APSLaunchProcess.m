//
//  APSLaunchManager.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSLaunchProcess.h"
#import "APSLayoutManager.h"
#import "APSConfigurationManager.h"
#import "TokenizationHandler.h"

@interface APSLaunchProcess ()

@property (retain, nonatomic) UIImageView * imageViewSplash;

@property (copy, nonatomic) void (^startBlock)(void);
@property (copy, nonatomic) void (^failedBlock)(APSLaunchFailedReason reason);
@property (copy, nonatomic) void (^completionBlock)(void);

@property (nonatomic) BOOL showLoginScreen;

@end

@implementation APSLaunchProcess

+(APSLaunchProcess *) launchProcess
{
    return [[APSLaunchProcess alloc] init];
}

-(void) startLaunchingAppWithStartBlock:(void(^)(void)) startBlock
                            failedBlock:(void(^)(APSLaunchFailedReason reason)) failedBlock
                        completionBlock:(void(^)(void)) completionBlock;
                                  {
    self.startBlock = startBlock;
    self.failedBlock = failedBlock;
    self.completionBlock = completionBlock;

    [self startLaunchProcess];

}

-(void) startLaunchProcess {
    
    [self launchStarted];
    [TvinciSDK takeOff];
    [self registerForConfigurationNotification];
    
}

-(void) resetValues
{
    self.startBlock = nil;
    self.failedBlock = nil;
    self.completionBlock = nil;
}


-(void) registerForConfigurationNotification
{
    [self unregisterForConfigurationNotification];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(configurationFailed:)
                   name:TVConfigurationManagerDidFailToLoadConfigurationNotification
                 object:nil];
    [center addObserver:self
               selector:@selector(configurationSucceded:)
                   name:TVConfigurationManagerDidLoadConfigurationNotification
                 object:nil];
    
}

-(void) unregisterForConfigurationNotification
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self
                      name:TVConfigurationManagerDidFailToLoadConfigurationNotification
                    object:nil];
    
    [center removeObserver:self
                      name:TVConfigurationManagerDidLoadConfigurationNotification
                    object:nil];
}


-(void) configurationFailed:(NSNotification *) notification
{
    [self launchFailed];
}

-(void) configurationSucceded:(NSNotification *) notification
{
    self.showLoginScreen = NO;
    
    [[TokenizationHandler sharedInstance] refreshTokenIfNeededAndCalledFromAppLaunchedPoint:YES withStartBlock:nil completionBlock:^(BOOL showLoginScreen) {
        
        self.showLoginScreen = showLoginScreen;
        [self configurationLodedAndTokenRefreshed];
        
    }];

}

- (void)configurationLodedAndTokenRefreshed {
    
    NSInteger menuID = [[TVConfigurationManager sharedTVConfigurationManager] mainMenuID];
    [[APSLayoutManager sharedLayoutManager] downloadMenuWithMenuID:menuID failedBlock:^{
        [self launchFailed];
    } completionBlock:^{
        
        // ??? Mavrick  [[APSEPGManager sharedEPGManager] loadChannelsWithStartBlock:^{
        
        TVMenuItem * menuItem =  [[APSLayoutManager sharedLayoutManager] menuItemForPageLayout:@"EPG" menuItems:[[APSLayoutManager sharedLayoutManager] arrayMenuItems]];
        menuItem = [[APSLayoutManager sharedLayoutManager] menuItemForType:@"All-EPG" menuItems:menuItem.children];
        NSString * tvChannelId = [menuItem.layout objectForKey:@"ChannelID"];
        
        [[APSEPGManager sharedEPGManager] loadChannelsWithID:tvChannelId StartBlock:^{
            
        } failedBlock:^{
            [self launchFailed];
        } completionBlock:^{
            [self launchCompleted];
        }];
        
    }];
   
}

-(void) launchStarted
{
    
    if (self.startBlock)
    {
        self.startBlock();
    }
}

-(void) launchFailed
{
    [self unregisterForConfigurationNotification];
    if (self.failedBlock)
    {
        self.failedBlock(APSLaunchFailedReasonUndefined);
    }
    
}

-(void) launchCompleted
{
    [self unregisterForConfigurationNotification];
    
    [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] resetRefreshAppDataDate];
    
    if (self.completionBlock)
    {
        self.completionBlock();
    }
    
    if (self.showLoginScreen) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TVSessionManagerAuthenticationExpiredNotification object:nil];
        
    }
    
}

-(void)dealloc
{
    [self unregisterForConfigurationNotification];
}

@end
