//
//  APSLayoutManager.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSLayoutManager.h"

@implementation APSLayoutManager


+ (instancetype)sharedLayoutManager {
    static APSLayoutManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[APSLayoutManager alloc] init];
    });
    
    return _sharedInstance;
}


-(void) downloadMenuWithMenuID:(NSInteger ) menuid failedBlock:(void(^)(void)) failedBlock completionBlock:(void(^)(void)) completionBlock
{
    
    TVPAPIRequest * strongRequest = [TVPSiteAPI requestForGetMenu:menuid delegate:nil];
    
    __weak TVPAPIRequest * request = strongRequest;//[TVPSiteAPI requestForGetMenu:menuid delegate:nil];
    
    [request setFailedBlock:^{
        if (failedBlock)
        {
            failedBlock();
        }
    }];
    
    
    [request setCompletionBlock:^{
        
        self.menuID = menuid;
        NSArray * menuArray = [[request JSONResponse] objectForKey:@"MenuItems"];
        ASLogInfo(@"Menu = %@",[request JSONResponse]);
        NSMutableArray * menuTempArry = [NSMutableArray array];
        
        for (NSDictionary * menuItemDict in menuArray)
        {
            TVMenuItem * menuItem = [[TVMenuItem alloc] initWithDictionary:menuItemDict];
            [menuTempArry addObject:menuItem];
        }

        self.arrayMenuItems = [NSArray arrayWithArray:menuTempArry];
        
        if (completionBlock)
        {
            completionBlock();
        }
    }];
    
    [self sendRequest:request];
}

-(TVMenuItem *) menuItemForName:(NSString *) name menuItems:(NSArray *) menuItems
{
    for (TVMenuItem * menuItem in menuItems)
    {
        if ([menuItem.name isEqualToString:name])
        {
            return menuItem;
        }
    }
    
    return nil;
}

-(TVMenuItem *) menuItemForPageLayout:(NSString *) name menuItems:(NSArray *) menuItems
{
    for (TVMenuItem * menuItem in menuItems)
    {
        if ([menuItem.pageLayout isEqualToString:name])
        {
            return menuItem;
        }
    }
    
    return nil;
}

-(TVMenuItem *) menuItemForType:(NSString *) name menuItems:(NSArray *) menuItems
{
    for (TVMenuItem * menuItem in menuItems)
    {
        if ([menuItem.uniqueName isEqualToString:name])
        {
            return menuItem;
        }
    }
    
    return nil;
}
@end
