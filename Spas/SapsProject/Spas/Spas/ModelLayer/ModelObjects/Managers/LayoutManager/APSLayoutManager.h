//
//  APSLayoutManager.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSBaseNetworkObject.h"

@interface APSLayoutManager : APSBaseNetworkObject



+ (instancetype)sharedLayoutManager;


@property (assign, nonatomic) NSInteger menuID;
@property (retain, nonatomic) NSArray * arrayMenuItems;
@property (assign, nonatomic) NSInteger lastSelectedTabInMenu;

-(void) downloadMenuWithMenuID:(NSInteger ) menuid failedBlock:(void(^)(void)) failedBlock completionBlock:(void(^)(void)) completionBlock;

-(TVMenuItem *) menuItemForName:(NSString *) name menuItems:(NSArray *) menuItems;
-(TVMenuItem *) menuItemForPageLayout:(NSString *) name menuItems:(NSArray *) menuItems;
-(TVMenuItem *) menuItemForType:(NSString *) name menuItems:(NSArray *) menuItems;



@end
