//
//  APS GoogleAnalyticsManager.m
//  Spas
//
//  Created by Israel Berezin on 10/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSGoogleAnalyticsManager.h"

@implementation APSGoogleAnalyticsManager

static APSGoogleAnalyticsManager * sharedInstance = nil;

+ (APSGoogleAnalyticsManager *) sharedGoogleAnalyticsManager
{
    if (sharedInstance ==nil)
    {
        @synchronized(self)
        {
            sharedInstance = [[APSGoogleAnalyticsManager alloc] init];
        }
    }
    return  sharedInstance;
}

-(void)sendGAITrackerScreenView:(NSString*)name
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)sendGAITrackerEventWithCategory:(NSString*)eventCategory eventAction:(NSString*)eventAction eventLabel:(NSString*)eventLabel
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:eventCategory     // Event category (required)
                                                          action:eventAction  // Event action (required)
                                                           label:eventLabel          // Event label
                                                           value:nil] build]];    // Event value
}

-(void)sendGAExceptionWithDescription:(NSString*)description
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder
                    createExceptionWithDescription:description withFatal:@NO] build]];
}
@end
