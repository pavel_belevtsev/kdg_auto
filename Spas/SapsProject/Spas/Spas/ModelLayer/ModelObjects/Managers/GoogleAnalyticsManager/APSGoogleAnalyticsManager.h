//
//  APS GoogleAnalyticsManager.h
//  Spas
//
//  Created by Israel Berezin on 10/30/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface APSGoogleAnalyticsManager : NSObject

+ (APSGoogleAnalyticsManager *) sharedGoogleAnalyticsManager;

-(void)sendGAITrackerScreenView:(NSString*)name;

-(void)sendGAITrackerEventWithCategory:(NSString*)eventCategory eventAction:(NSString*)eventAction eventLabel:(NSString*)eventLabel;

-(void)sendGAExceptionWithDescription:(NSString*)description;

@end
