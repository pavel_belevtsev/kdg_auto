//
//  APSRefreshProcess.h
//  Spas
//
//  Created by Pavel Belevtsev on 27.05.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseNetworkObject.h"

@interface APSRefreshProcess : APSBaseNetworkObject

+ (APSRefreshProcess *)refreshProcess;

- (void)refreshAppDataWithStartBlock:(void(^)(void)) startBlock
                         failedBlock:(void(^)(void)) failedBlock
                     completionBlock:(void(^)(void)) completionBlock;


extern NSString * const APSRefreshManagerRefreshCompletedNotification;

@end
