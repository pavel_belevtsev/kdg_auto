//
//  APSRefreshProcess.m
//  Spas
//
//  Created by Pavel Belevtsev on 27.05.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import "APSRefreshProcess.h"
#import "APSConfigurationManager.h"

@interface APSRefreshProcess ()

@property (copy, nonatomic) void (^startBlock)(void);
@property (copy, nonatomic) void (^failedBlock)(void);
@property (copy, nonatomic) void (^completionBlock)(void);

@end

@implementation APSRefreshProcess

+ (APSRefreshProcess *)refreshProcess
{
    
    static APSRefreshProcess *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
            
        }
    }
    return instance;

}

- (void)refreshAppDataWithStartBlock:(void(^)(void)) startBlock
                         failedBlock:(void(^)(void)) failedBlock
                     completionBlock:(void(^)(void)) completionBlock {
    
    APSConfigurationManager *manager = (APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager];
    
    if ([manager isKindOfClass:[APSConfigurationManager class]]) {
        
        BOOL needToRefresh = NO;
        
        if (manager.refreshDate) {
            
            NSTimeInterval interval = -[manager.refreshDate timeIntervalSinceNow];
            
            if (interval > manager.refreshFrequency * 60) needToRefresh = YES;
            
        }
        
        if (needToRefresh) {
            
            self.startBlock = startBlock;
            self.failedBlock = failedBlock;
            self.completionBlock = completionBlock;
            
            [self refreshStarted];
            [TvinciSDK takeOff];
            [self registerForConfigurationNotification];
            
        }
        
    }
    
}

- (void)resetValues {
    
    self.startBlock = nil;
    self.failedBlock = nil;
    self.completionBlock = nil;
    
}

- (void)registerForConfigurationNotification {
    
    [self unregisterForConfigurationNotification];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self
               selector:@selector(configurationFailed:)
                   name:TVConfigurationManagerDidFailToLoadConfigurationNotification
                 object:nil];
    [center addObserver:self
               selector:@selector(configurationSucceded:)
                   name:TVConfigurationManagerDidLoadConfigurationNotification
                 object:nil];
    
}

- (void)unregisterForConfigurationNotification {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center removeObserver:self
                      name:TVConfigurationManagerDidFailToLoadConfigurationNotification
                    object:nil];
    
    [center removeObserver:self
                      name:TVConfigurationManagerDidLoadConfigurationNotification
                    object:nil];
    
}


- (void)configurationFailed:(NSNotification *) notification {
    
    [self refreshFailed];

}

- (void)configurationSucceded:(NSNotification *) notification {
    
    NSInteger menuID = [[TVConfigurationManager sharedTVConfigurationManager] mainMenuID];
    [[APSLayoutManager sharedLayoutManager] downloadMenuWithMenuID:menuID failedBlock:^{
        [self refreshFailed];
    } completionBlock:^{
        
        // ??? Mavrick  [[APSEPGManager sharedEPGManager] loadChannelsWithStartBlock:^{
        
        TVMenuItem * menuItem =  [[APSLayoutManager sharedLayoutManager] menuItemForPageLayout:@"EPG" menuItems:[[APSLayoutManager sharedLayoutManager] arrayMenuItems]];
        menuItem = [[APSLayoutManager sharedLayoutManager] menuItemForType:@"All-EPG" menuItems:menuItem.children];
        NSString * tvChannelId = [menuItem.layout objectForKey:@"ChannelID"];
        
        [[APSEPGManager sharedEPGManager] loadChannelsWithID:tvChannelId StartBlock:^{
    
        } failedBlock:^{
            [self refreshFailed];
        } completionBlock:^{
            [self refreshCompleted];
        }];
        
    }];
    
}

- (void)refreshStarted {
    
    if (self.startBlock)
    {
        self.startBlock();
    }
}


- (void)refreshFailed {
    
    [self unregisterForConfigurationNotification];
    if (self.failedBlock)
    {
        self.failedBlock();
    }
    
}

- (void)refreshCompleted {
    
    [self unregisterForConfigurationNotification];
    
    [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] resetRefreshAppDataDate];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:APSRefreshManagerRefreshCompletedNotification object:nil];
    
    if (self.completionBlock)
    {
        self.completionBlock();
    }
    
}

- (void)dealloc {
    
    [self unregisterForConfigurationNotification];
}

NSString * const APSRefreshManagerRefreshCompletedNotification = @"APSRefreshManagerRefreshCompletedNotification";

@end
