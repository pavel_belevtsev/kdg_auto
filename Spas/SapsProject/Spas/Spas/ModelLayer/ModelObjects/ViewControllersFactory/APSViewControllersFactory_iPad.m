//
//  APSViewControllersFactory_iPad.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/13/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSViewControllersFactory_iPad.h"
#import "APSMainViewController_iPad.h"
#import "APSAutocompleteteSearchViewController.h"
#import "APSSettingsViewController.h"
#import "APSSearchViewController.h"
#import "APSAccountViewController.h"
#import "APSLoginViewController.h"
#import "APSFullScreenPlayerViewController.h"
#import "APSMainViewController_iPad.h"
#import "APSMiniEPGViewController.h"
#import "APSChannelPanel.h"
#import "APSChannelPage.h"
#import "APSEPGViewController_iPad.h"

@implementation APSViewControllersFactory_iPad


-(APSBaseViewController *) mainViewController
{
    
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSMainViewController class]);
    APSBaseViewController * mainViewController = [[APSMainViewController_iPad alloc] initWithNibName:nibName bundle:nil];
    return mainViewController;
}


-(APSBaseViewController *) autocompleteSearchViewController
{

    NSString * nibName = NSStringFromClassWithiPadSuffix([APSAutocompleteteSearchViewController class]);
    APSAutocompleteteSearchViewController * autocompleteSearchViewController = [[APSAutocompleteteSearchViewController alloc] initWithNibName:nibName bundle:nil];
    return autocompleteSearchViewController;
}


-(APSBaseBarsViewController *) settingsViewController
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSSettingsViewController class]);
    APSSettingsViewController * settingsVC = [[APSSettingsViewController alloc] initWithNibName:nibName bundle:nil];
    return settingsVC;
}


-(APSBaseBarsViewController *) searchViewControllerWithSearchText:(NSString *) text
{
    NSString * nibName = @"APSSearchViewController_iPad";
    APSSearchViewController * searchViewController = [[APSSearchViewController alloc] initWithNibName:nibName bundle:nil];
    searchViewController.searchText = text;
    return searchViewController;
}

-(APSBaseBarsViewController *) accountViewControllerWithPageTitle:(NSString*)pageTitle
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSAccountViewController class]);
    APSAccountViewController * accountViewController = [[APSAccountViewController alloc] initWithNibName:nibName bundle:nil];
    accountViewController.pageTitle = pageTitle;
    return accountViewController;
}

-(APSBaseViewController *) loginViewController
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSLoginViewController class]);
    APSLoginViewController * loginViewController = [[APSLoginViewController alloc] initWithNibName:nibName bundle:nil];
    return loginViewController;
    
}

-(APSBaseViewController *) fullScreenPlayerViewControllerWithPurePlayer:(APSPurePlayerViewController *) purePlayer
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSFullScreenPlayerViewController class]);
    APSFullScreenPlayerViewController * viewController = [[APSFullScreenPlayerViewController alloc] initWithNibName:nibName bundle:nil];
    viewController.purePlayerViewController = purePlayer;
    return  viewController ;
}

-(APSBaseViewController *) miniEPGViewController
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSMiniEPGViewController class]);
    APSMiniEPGViewController * viewController = [[APSMiniEPGViewController alloc] initWithNibName:nibName bundle:nil];
    return  viewController ;
}

-(APSBaseViewController *) channelPanel
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSChannelPanel class]);
    APSChannelPanel * viewController = [[APSChannelPanel alloc] initWithNibName:nibName bundle:nil];
    return  viewController ;
}

-(APSBaseBarsViewController *) channelPageWithChannel:(TVMediaItem*)channel
{
    NSString * nibName = @"APSChannelPage";
    APSChannelPage * channelPage = [[APSChannelPage alloc] initWithNibName:nibName bundle:nil];
    channelPage.channel = channel;
    return channelPage;
}

-(APSBaseBarsViewController *) EPGViewController
{
    NSString * nibName = NSStringFromClassWithiPadSuffix([APSEPGViewController class]);
    APSBaseBarsViewController * viewController = [[APSEPGViewController_iPad  alloc] initWithNibName:nibName bundle:nil];
    return  viewController;
    
}
@end
