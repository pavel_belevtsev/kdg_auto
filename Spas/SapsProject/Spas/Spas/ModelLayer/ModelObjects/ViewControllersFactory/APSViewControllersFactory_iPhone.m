//
//  APSViewControllersFactory_iPhone.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/13/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSViewControllersFactory_iPhone.h"
#import "APSMainViewController_iPhone.h"
#import "APSAutocompleteteSearchViewController.h"
#import "APSSettingsViewController.h"
#import "APSSearchViewController.h"
#import "APSAccountViewController.h"
#import "APSLoginViewController.h"
#import "APSMainViewController_iPhone.h"
#import "APSFullScreenPlayerViewController_iPhone.h"
#import "APSChannelPanel.h"
#import "APSChannelPage.h"
#import "APSEPGViewController_iPhone.h"

@implementation APSViewControllersFactory_iPhone


-(APSBaseViewController *) mainViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSMainViewController class]);
    APSBaseViewController * mainViewController = [[APSMainViewController_iPhone alloc] initWithNibName:nibName bundle:nil];
    return mainViewController;
}

-(APSBaseViewController *) autocompleteSearchViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSAutocompleteteSearchViewController class]);
    APSAutocompleteteSearchViewController * autocompleteSearchViewController = [[APSAutocompleteteSearchViewController alloc] initWithNibName:nibName bundle:nil];
    return autocompleteSearchViewController;
}

-(APSBaseBarsViewController *)settingsViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSSettingsViewController class]);
    APSSettingsViewController * settingsVC = [[APSSettingsViewController alloc] initWithNibName:nibName bundle:nil];
    return settingsVC;
}

-(APSBaseBarsViewController *) searchViewControllerWithSearchText:(NSString *) text
{
    NSString * nibName = @"APSSearchViewController_iPhone";
    APSSearchViewController * searchViewController = [[APSSearchViewController alloc] initWithNibName:nibName bundle:nil];
    searchViewController.searchText = text;
    return searchViewController;
}


-(APSBaseViewController *) fullScreenPlayerViewControllerWithPurePlayer:(APSPurePlayerViewController *) purePlayer
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSFullScreenPlayerViewController class]);
    APSFullScreenPlayerViewController * viewController = [[APSFullScreenPlayerViewController_iPhone alloc] initWithNibName:nibName bundle:nil];
    viewController.purePlayerViewController = purePlayer;
    return  viewController ;
}

-(APSBaseBarsViewController *) accountViewControllerWithPageTitle:(NSString*)pageTitle
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSAccountViewController class]);
    APSAccountViewController * accountViewController = [[APSAccountViewController alloc] initWithNibName:nibName bundle:nil];
    accountViewController.pageTitle = pageTitle;
    return accountViewController;
}


-(APSBaseViewController *) loginViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSLoginViewController class]);
    APSLoginViewController * loginViewController = [[APSLoginViewController alloc] initWithNibName:nibName bundle:nil];
    return loginViewController;
}

-(APSBaseViewController *) channelPanel
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSChannelPanel class]);
    APSChannelPanel * viewController = [[APSChannelPanel alloc] initWithNibName:nibName bundle:nil];
    return  viewController ;
}

//APSChannelPage

-(APSBaseBarsViewController *) channelPageWithChannel:(TVMediaItem*)channel
{
    NSString * nibName = @"APSChannelPage";
    APSChannelPage * channelPage = [[APSChannelPage alloc] initWithNibName:nibName bundle:nil];
    channelPage.channel = channel;
    return channelPage;
}


-(APSBaseBarsViewController *) EPGViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSEPGViewController class]);
    APSBaseBarsViewController * viewController = [[APSEPGViewController_iPhone alloc] initWithNibName:nibName bundle:nil];
    return  viewController;
    
}

@end
