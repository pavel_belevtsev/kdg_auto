//
//  APSViewControllersFactory.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSViewControllersFactory.h"
#import "APSHomeViewController.h"
#import "APSEPGViewController.h"
#import "APSFullScreenPlayerViewController.h"
#import "APSSearchViewController.h"
#import "APSMainViewController.h"
#import "APSSettingsViewController.h"
#import "APSAccountViewController.h"
#import "APSLoginViewController.h"
#import "APSWebViewController.h"
#import "APSPurePlayerViewController.h"
#import "APSMiniEPGViewController.h"


@implementation APSViewControllersFactory




static APSViewControllersFactory *_sharedInstance = nil;

+ (instancetype) defaultFactory {
    
    @synchronized(self)
    {
        if (_sharedInstance == nil)
        {
            _sharedInstance = [[APSViewControllersFactory alloc] init];
        }
    }
    
    return _sharedInstance;
}

-(void) becomeDefaultFactory
{
    @synchronized (self)
    {
        if (_sharedInstance != self)
        {
            _sharedInstance = self;
        }
    }
}

-(APSBaseViewController *) mainViewController
{
    NSString * nibName = NSStringFromClass([APSMainViewController class]);
    APSBaseViewController * mainViewController = [[APSMainViewController alloc] initWithNibName:nibName bundle:nil];
    return mainViewController;
}

-(APSBaseBarsViewController *) homeViewController
{
    return [self homeViewController:NO];
}

-(APSBaseBarsViewController *) homeViewController:(BOOL)favoritesMode {
    
    NSString * nibName = NSStringFromClass([APSHomeViewController class]);
    APSBaseBarsViewController * viewController = [[APSHomeViewController alloc] initWithNibName:nibName bundle:nil];
    ((APSHomeViewController *)viewController).favoritesMode = favoritesMode;

    return  viewController ;
    
}

-(APSBaseBarsViewController *) EPGViewController
{
    return nil;
}


-(APSBaseBarsViewController *) fullScreenPlayerViewControllerWithPurePlayer:(APSPurePlayerViewController *) purePlayer
{
    return nil;
}


-(APSBaseViewController *) autocompleteSearchViewController
{
    return nil;
}

-(APSBaseBarsViewController *) searchViewControllerWithSearchText:(NSString *) text
{
    return nil;
    
}

-(APSBaseBarsViewController *) settingsViewController
{
    
    return nil;
}


-(APSBaseBarsViewController *) accountViewControllerWithPageTitle:(NSString*)pageTitle
{
    return nil;
}

-(APSBaseViewController *) loginViewController
{
    return nil;
}

-(APSBaseBarsViewController * )webViewControllerWithUrl:(NSURL*)webLink andPageTitle:(NSString*)pageTitle andMenuItem:(TVMenuItem *) item
{
    NSString * nibName = NSStringFromClass([APSWebViewController class]);
    APSWebViewController * webViewController = [[APSWebViewController alloc] initWithNibName:nibName bundle:nil];
    webViewController.webLink = webLink;
    pageTitle = [pageTitle uppercaseString];
    webViewController.pageTitle = pageTitle;
    webViewController.item = item;
    return webViewController;
}

-(APSBaseViewController *) purePlayerViewController
{
    NSString * nibName = NSStringFromClass([APSPurePlayerViewController class]);
    APSPurePlayerViewController * purePlayerViewController = [[APSPurePlayerViewController alloc] initWithNibName:nibName bundle:nil];
    return purePlayerViewController;
}

-(APSBaseViewController *) miniEPGViewController
{
    NSString * nibName = NSStringFromClassWithiPhoneSuffix([APSMiniEPGViewController class]);
    APSMiniEPGViewController * viewController = [[APSMiniEPGViewController alloc] initWithNibName:nibName bundle:nil];
    return  viewController ;
    
    return nil;
}

-(APSBaseViewController *) channelPanel
{
    return nil;
}

-(APSBaseBarsViewController *) channelPageWithChannel:(TVMediaItem*)channel
{
   return nil;
}
@end
