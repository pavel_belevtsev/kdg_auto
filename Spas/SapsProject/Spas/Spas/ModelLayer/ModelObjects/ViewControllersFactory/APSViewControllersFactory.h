//
//  APSViewControllersFactory.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSBaseBarsViewController.h"
#import "APSBaseViewController.h"

@class APSPurePlayerViewController;

@interface APSViewControllersFactory : NSObject


+ (instancetype) defaultFactory;
-(void) becomeDefaultFactory;


-(APSBaseViewController *) mainViewController;
-(APSBaseViewController *) autocompleteSearchViewController;
-(APSBaseViewController *) loginViewController;
-(APSBaseBarsViewController *) settingsViewController;
-(APSBaseBarsViewController *) homeViewController;
-(APSBaseBarsViewController *) homeViewController:(BOOL)favoritesMode;
-(APSBaseBarsViewController *) EPGViewController;
-(APSBaseBarsViewController *) fullScreenPlayerViewControllerWithPurePlayer:(APSPurePlayerViewController *) purePlayer;
-(APSBaseViewController *) purePlayerViewController;
-(APSBaseBarsViewController *) searchViewControllerWithSearchText:(NSString *) text;
-(APSBaseBarsViewController *) accountViewControllerWithPageTitle:(NSString*)pageTitle;
-(APSBaseBarsViewController * )webViewControllerWithUrl:(NSURL*)webLink andPageTitle:(NSString*)pageTitle andMenuItem:(TVMenuItem *) item;
-(APSBaseViewController *) miniEPGViewController;
-(APSBaseViewController *) channelPanel;
-(APSBaseBarsViewController *) channelPageWithChannel:(TVMediaItem*)channel;

@end
