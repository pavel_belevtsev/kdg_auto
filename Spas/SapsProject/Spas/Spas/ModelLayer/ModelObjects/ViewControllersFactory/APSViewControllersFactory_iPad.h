//
//  APSViewControllersFactory_iPad.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/13/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSViewControllersFactory.h"

@interface APSViewControllersFactory_iPad : APSViewControllersFactory

@end
