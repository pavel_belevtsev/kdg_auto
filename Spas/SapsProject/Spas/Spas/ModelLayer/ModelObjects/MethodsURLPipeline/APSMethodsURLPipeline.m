//
//  APSMethodsURLPipeline.m
//  Spas
//
//  Created by Rivka Peleg on 10/2/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//
#import "APSMethodsURLPipeline.h"

@implementation APSMethodsURLPipeline


-(NSURL *)transferURL:(NSURL *)url methodName:(NSString *)methodName
{
    NSURL * resultURL = url;
    
    if ([url.absoluteString rangeOfString:@"-lab"].location != NSNotFound) { // Don't use https for Lab
        
        return resultURL;
        
    }
    
    if ([methodName isEqualToString:methodNameSSOSignIn] || [methodName isEqualToString:MethodNameGetLicensedLinks])
    {
        NSString * urlString = [url absoluteString];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
        resultURL = [NSURL URLWithString:urlString];
    }
    
    return resultURL;
}

@end
