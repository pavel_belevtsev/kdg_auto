//
//  CDNHandler.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/18/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDNHandler : APSBaseNetworkObject


typedef enum FinalConclusion
{
    
    FinalConclusion_OutSideClientNetwork,
    FinalConclusion_ContentIssue, // credentials are valid, content is umavailable.
    FinalConclusion_InSideClientNetwork_But_SecurityIssue, // user is not entitled to the service, inside KDG network
    FinalConclusion_InSideClientNetwork_contenent_available,
    FinalConclusion_OtherError, //system error
    FinalConclusion_NoInternetError //no internet error
    
}FinalConclusion;

-(void) startCDNProcessWithFileURL:(NSURL *) fileURL fileID:(NSInteger ) fileID startedBlock:(void(^)(void)) startedBlock completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock;

-(void) handleErrorDuringPlayingOfURL:(NSURL *) url startedBlock:(void(^)(void)) startedBlock completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock;


-(void) stopProcess;
@end
