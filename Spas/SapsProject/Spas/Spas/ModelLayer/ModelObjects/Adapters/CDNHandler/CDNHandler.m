//
//  CDNHandler.m
//  Spas
//
//  Created by Rivka S. Peleg on 8/18/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "CDNHandler.h"
#import "APSGoogleAnalyticsManager.h"
#import "APSSessionManager.h"
#import "APSSessionAdapter.h"
#import <TvinciSDK/ASIHTTPRequest.h>

#define MAIN_URL_KEY @"mainUrl"
#define ALTERNATIVE_URL_KEY @"altUrl"

#define NumberOfAttempts 5
#define TimeToTest 3*60

@interface CDNHandler ()
@property  (retain, nonatomic) NSURL * mainURL;
@property  (retain, nonatomic) NSURL * alternativeURL;
@property (strong , nonatomic) NSMutableArray * errorTimes;
@property (nonatomic ,strong) APSSessionAdapter * sessionAdapter;
@end

@implementation CDNHandler

-(void) startCDNProcessWithFileURL:(NSURL *) fileURL fileID:(NSInteger ) fileID startedBlock:(void(^)(void)) startedBlock completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    if (fileURL == nil && fileID ==0)
    {
        if (completionBlock)
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }
    }
    else if (fileURL == nil && fileID <0)
    {
        if (completionBlock)
        {
            completionBlock(nil,(fileID+1)*(-1));
        }
    }
    else
    {
        
        TVPAPIRequest * strongRequest = [TVPMediaAPI requestForGetLicensedLinksWithFileID:fileID baseURL:fileURL delegate:nil];
        
        __weak TVPAPIRequest * request = strongRequest;
        
        [request setStartedBlock:^{
            
            if (startedBlock)
            {
                startedBlock();
            }
        }];
        [request setFailedBlock:^{
            
            [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"request  Failed %@: %@", @"GetLicensedLinks", request.error.description]];
            if ([[request error] code] == ASIConnectionFailureErrorType)
            {
                completionBlock(nil,FinalConclusion_NoInternetError);
            }
            else
            {
                completionBlock(fileURL,FinalConclusion_OtherError);
            }
        }];
        
        [request setCompletionBlock:^{
            //NSLog (@"requestForGetLicensedLinks = %@",[request JSONResponse]);
            NSDictionary *dictionary = [request JSONResponse];
            NSString * mainURL = [dictionary objectForKey:MAIN_URL_KEY];
            NSString * alterURL = [dictionary objectForKey:ALTERNATIVE_URL_KEY];
            
            self.errorTimes = [NSMutableArray array];
            self.mainURL = [NSURL URLWithString:mainURL];
            self.alternativeURL = [NSURL URLWithString:alterURL];
            
            [self firstTryDownloadManifestForURL:self.mainURL withcompletionBlock:completionBlock];
            
        }];
        
        [self sendRequest:request];
    }
    
}

-(void) firstTryDownloadManifestForURL:(NSURL *) url withcompletionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    
    // see ASIHTTPRequest fails in production builds: http://stackoverflow.com/questions/9123975/asihttprequest-fails-in-production-builds
    
    //NSLog(@"firstTryDownloadManifestForURL = %@",url);
    TVPAPIRequest * strongRequest = [TVPAPIRequest requestWithURL:url];
    
    __weak TVPAPIRequest * request = strongRequest;
    
    request.timeOutSeconds = 3;
    request.excludeInitObj = YES;
    request.requestMethod = RequestMethod_GET;
    
    [request setFailedBlock:^{
//        NSLog(@"firstTryDownloadManifest = %@",request);
        //NSLog(@"request.error.code = %ld   request.responseStatusCode = %d",(long)request.error.code,request.responseStatusCode);
        [self  handleManifestResponseFirstTry:request completionBlock:completionBlock];
    }];
    
    [request setCompletionBlock:^{
//        NSLog(@"firstTryDownloadManifest = %@",request);
        //NSLog(@"request.error.code = %ld   request.responseStatusCode = %d",(long)request.error.code,request.responseStatusCode);

        [self  handleManifestResponseFirstTry:request completionBlock:completionBlock];
    }];
    
    //NSLog(@"firstTryDownloadManifestForRequest = %@",request);
    
    if (request == nil)
    {
        //NSLog(@"firstTryDownloadManifestForRequest on nil!!!");

    }
    [self sendRequest:request];
}



-(void) handleManifestResponseFirstTry:(TVPAPIRequest *) request completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    NSURL * targetURL  = [self.mainURL isEqual:request.url]?self.alternativeURL:self.mainURL;
    
    if (([request.error.domain isEqualToString:NSURLErrorDomain] && request.error.code
         == kCFURLErrorTimedOut) || (request.responseStatusCode >= 400 && request.responseStatusCode < 600))
    {
        [self SSOCheckWithSecondURL:targetURL withstartedBlock:nil completionBlock:completionBlock];
    }
    else if (request.responseStatusCode == 200)
    {
        if (completionBlock)
        {
            completionBlock(request.url,FinalConclusion_InSideClientNetwork_contenent_available);
        }
    }
    else
    {
        if (completionBlock)
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }
    }
}

-(void) SSOCheckWithSecondURL:(NSURL *) url withstartedBlock:(void(^)(void)) startedBlock completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    NSString * userName = [GCUserDomain getUserName];
    NSString * password = [GCUserDomain getUserPassword];
    
//    NSLog(@"SSOCheckWithSecondURL %@,%@",userName,password);
    
    if ((userName.length == 0 || password.length == 0) )
    {
        NSLog(@"make Log out in SSOCheckWithSecondURL");
        self.sessionAdapter = [[APSSessionAdapter alloc] init];

        [self.sessionAdapter logoutWithSuccessBlock:^
        {
            NSLog(@"log out succeded");
            completionBlock(nil,FinalConclusion_OtherError);
        }
        failedBlock:^(NSString *failedError)
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }];
        
        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];;
        return;
    }
    
    TVPAPIRequest * strongRequest = [TVPSiteAPI requestForSSOSigninWithUserName:userName password:password providerID:0 delegate:nil]; // ??? Mavrick
    
    __weak TVPAPIRequest * request = strongRequest;
    
    [request setStartedBlock:^{
        NSLog(@"Started requestForSSOSignin");
    }];
    
    [request setFailedBlock:^{
        NSLog(@"request = %@",request);
        [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAExceptionWithDescription:[NSString stringWithFormat: @"request  Failed %@: %@", @"Player SSOSignin", request.error.description]];

        if (completionBlock)
        {
            if ([[request error] code] == ASIConnectionFailureErrorType)
            {
                completionBlock(nil,FinalConclusion_NoInternetError);
            }
            else
            {
                completionBlock(nil,FinalConclusion_OtherError);
            }
        }
    }];
    
    [request setCompletionBlock:^{
      //  NSLog (@"requestSSOSign = %@",request);
        NSDictionary * response = [request JSONResponse];
        NSDictionary * userData = [response objectOrNilForKey:@"UserData"];
        NSDictionary * userDynamicData = [userData objectOrNilForKey:@"m_oDynamicData"];
        NSArray * innerDataArray = [userDynamicData objectOrNilForKey:@"m_sUserData"];
        NSInteger externalParamIndex = [innerDataArray indexOfObjectPassingTest:^BOOL(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
            NSString * key = [obj objectForKey:@"m_sDataType"];
            if ([key isEqualToString:@"ext_status"])
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }];
        
        if (externalParamIndex == NSNotFound)
        {
            NSLog (@"externalParamIndex == NSNotFound");
            if (completionBlock)
            {
                completionBlock(nil,FinalConclusion_OtherError);
            }
        }
        else
        {
            NSDictionary * externalStatus = [innerDataArray objectAtIndex:externalParamIndex];
            NSInteger externalStatusNumber = [[externalStatus objectForKey:@"m_sValue"] integerValue];
             NSLog (@"externalStatusNumber = %ld",(long)externalStatusNumber);
            switch (externalStatusNumber)
            {
                case 0:
                {
                    [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Login];
                    [self secondTryDownloadManifestForURL:url withcompletionBlock:completionBlock];
                }
                    break;
                case 2:
                case 4:
                    if (completionBlock)
                    {
                        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];
                        completionBlock(nil,FinalConclusion_InSideClientNetwork_But_SecurityIssue);
                    }
                    break;
                default:
                    if (completionBlock)
                    {
                        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_OutOfNetwork];
                        completionBlock(nil,FinalConclusion_OutSideClientNetwork);
                    }
                    break;
            }
        }
    }];
    
    [self sendRequest:request];
}

-(void) secondTryDownloadManifestForURL:(NSURL *) url withcompletionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    TVPAPIRequest * strongRequest = [TVPAPIRequest requestWithURL:url];
    
    __weak TVPAPIRequest * request = strongRequest;
    
    request.timeOutSeconds = 3;
    request.excludeInitObj = NO;
    request.requestMethod = RequestMethod_GET;
    
    [request setFailedBlock:^{
         NSLog(@"Failed handleManifest = %@",request);
        
        [self  handleManifestResponseSecondTry:request completionBlock:completionBlock];
    }];
    
    [request setCompletionBlock:^{
        NSLog(@"handleManifest = %@",request);

        [self  handleManifestResponseSecondTry:request completionBlock:completionBlock];
    }];
    
    
    [self sendRequest:request];
}

-(void) handleManifestResponseSecondTry:(TVPAPIRequest *) request completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock
{
    if (completionBlock)
    {
        
        if ([request.error.domain isEqualToString:NSURLErrorDomain] && request.error.code
            == kCFURLErrorTimedOut)
        {
            completionBlock(nil,FinalConclusion_ContentIssue);
        }
        else if (request.responseStatusCode == 403 )
        {
            completionBlock(nil,FinalConclusion_InSideClientNetwork_But_SecurityIssue);
        }
        else if (request.responseStatusCode == 200)
        {
            completionBlock(request.url,FinalConclusion_InSideClientNetwork_contenent_available);
        }
        else
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }
    }
}


-(void) handleErrorDuringPlayingOfURL:(NSURL *) url startedBlock:(void(^)(void)) startedBlock completionBlock:(void(^)(NSURL * url,FinalConclusion finalConclusion)) completionBlock

{
    NSMutableArray * temp = [NSMutableArray array];
    NSDate * nowDate = [NSDate date];
    NSTimeInterval nowTime = [nowDate timeIntervalSince1970];
    for (NSDate * cDate in self.errorTimes)
    {
        NSTimeInterval selectTime = [cDate timeIntervalSince1970];
        NSInteger dif = nowTime - selectTime;
        if (dif <TimeToTest)
        {
            [temp addObject:cDate];
        }
    }
    self.errorTimes = [NSMutableArray arrayWithArray:temp];
    [self.errorTimes addObject:nowDate];
    NSLog(@"self.errorTime = %@",self.errorTimes);
    if ([self.errorTimes count] == NumberOfAttempts)
    {
        [self.errorTimes removeAllObjects];
        if (completionBlock)
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }
        return;
    }

    NSURL * targetURL  = [self.mainURL isEqual:url]?self.alternativeURL:self.mainURL;
    if (targetURL)
    {
        [self SSOCheckWithSecondURL:targetURL withstartedBlock:startedBlock completionBlock:completionBlock];
    }
    else
    {
        if (completionBlock)
        {
            completionBlock(nil,FinalConclusion_OtherError);
        }
    }
}


-(void) stopProcess
{
    [self.networkQueue cleen];
}


@end
