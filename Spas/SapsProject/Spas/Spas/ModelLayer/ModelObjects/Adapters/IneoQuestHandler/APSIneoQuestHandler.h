//
//  APSIneoQuestHandler.h
//  Spas
//
//  Created by Kaltura Support on 12/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "IQPegasus.h"

#if !TARGET_IPHONE_SIMULATOR
#import <TVCPlayerWV/TVCPlayerFactory.h>
#import <TVCPlayerWV/TVMediaToPlayInfo.h>
#import <TVCPlayerWV/TVCplayer.h>
#endif

@interface APSIneoQuestHandler : APSBaseNetworkObject 
#if !TARGET_IPHONE_SIMULATOR
@property (assign, nonatomic) TVCplayer * player;
#endif
@property BOOL useProxy;
#if !TARGET_IPHONE_SIMULATOR
- (id)initWithPlayer:(TVCplayer *) player;
#endif

- (NSURL* )ineoQuestURLToPlay:(NSURL *)url;

@property BOOL pegasusConfigSuccessful;

-(void)  stopIneoQuestPressed;
-(void) startIneoQuestPressed;
-(void) pauseIneoQuestPressed;

@end
