//
//  APSIneoQuestHandler.m
//  Spas
//
//  Created by Kaltura Support on 12/10/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSIneoQuestHandler.h"
#import <IQEndpointKit/IQEndpointKit.h>
#import "APSConfigurationManager.h"

#import <MediaPlayer/MediaPlayer.h>
#define IQ_DEFAULT_CONFIG_URL   @"amp://ampasm-demo.ineoquest.ca:9997/?apikey=abcdefff-aaaa-bbaa-ffff-329bf39fa1e4"


#define AMP_ASM_HOST @"ampasm-demo.ineoquest.ca"
#define AMP_ASM_PORT 9997
#define AMP_ASM_API_KEY @"abcdefff-aaaa-bbaa-FFFF-329BF39FA1E4"


// Set to YES to disable SSL certificate checking at SDK startup time.
// Set to NO to check SSL certificate at SDK startup time.
// Normally this should be set to NO.
#define DISABLE_SSL_CERT_CHECK NO

// Set to YES to allow HLS SSL keys through HTTP proxy (allows monitoring).
// Set to NO to bypass proxy (no monitoring).
#define PROXY_SSL_HLS_KEYS YES

// Set to 1 to use an HTTP Proxy server
// Set to 0 to disable it
// Update the host and port below to match your network configuration
// Update the username and password if using authentication, or leave as nil if not.
#define USING_HTTP_PROXY 1

#ifdef USING_HTTP_PROXY
#define HTTP_PROXY_HOST @"88.134.236.1" // IQ office
//#define HTTP_PROXY_HOST @"192.168.2.23" // Terry's place
#define HTTP_PROXY_PORT 3128 // IQ office
#define HTTP_PROXY_USERNAME @"dtvweb" // Do not use authentication
#define HTTP_PROXY_PASSWORD @"digitv" // Do not use authentication
#endif

@interface APSIneoQuestHandler () <IQPegasusDelegate>

@property (nonatomic, retain) NSString * ineoQuestHost;
@property (nonatomic, retain) NSString * ineoApi_key;
@property (nonatomic, assign) NSInteger  ineoQuestPort;
@property (nonatomic, assign) BOOL isDisableSSLCertificateChecking;
@property BOOL appHaveineoQuest;

//@property (nonatomic, strong) IQMPFAdapter *iqEndpoint;
@property (nonatomic, assign) MPMoviePlaybackState playbackState;
@property (nonatomic, assign) BOOL proxyServerStarted;

@end

@implementation APSIneoQuestHandler 

@synthesize playbackState;
@synthesize proxyServerStarted;

#if !TARGET_IPHONE_SIMULATOR
- (id)initWithPlayer:(TVCplayer *) player
{
    self = [super init];
    if (self)
    {
        self.player = player;
        [self addNotificationObservers];
        self.useProxy = NO;
        proxyServerStarted = NO;

        self.ineoQuestHost =  [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] ineoQuestHost];
        self.ineoApi_key =  [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] ineoApi_key];
        self.ineoQuestPort =  [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] ineoQuestPort];
         self.isDisableSSLCertificateChecking = [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] disableSSL];
        self.appHaveineoQuest = [(APSConfigurationManager*)[APSConfigurationManager sharedTVConfigurationManager] appHaveineoQuest];
        
        // use proxy only if app not have ineoQuest
        self.useProxy = !self.appHaveineoQuest;

        [self initPegasus];
    }
    return self;
}
#endif

//------------------------------------------------------------------------------
# pragma mark - Playback methods -
//------------------------------------------------------------------------------

- (void)initPegasus
{
   
    [[IQPegasus sharedInstance] setLoggingLevel:IQLoggingLevelDebug];

    // IMPORTANT! Must set delegate before calling other IQPegasus methods
    [[IQPegasus sharedInstance] setDelegate:self];
    
    // If you are going to set the user ID in IQPegasus, you must do so
    // BEFORE calling the configure method.
    [self initUserID];

    if (self.useProxy)
    {
        // If using an HTTP Proxy server, you must call this method BEFORE
        // calling the configure method.
        [[IQPegasus sharedInstance] useHTTPProxyWithHost:HTTP_PROXY_HOST
                                               proxyPort:HTTP_PROXY_PORT
                                                username:HTTP_PROXY_USERNAME
                                                password:HTTP_PROXY_PASSWORD];
    }

    NSURL * configURL = [self builUrl];
    //NSLog(@"IQPegasus HostPortAPIKeyURL = %@",configURL);
   
    [[IQPegasus sharedInstance] configureWithHostPortAPIKeyURL:configURL
                                                useGeolocation:NO
                                 disableSSLCertificateChecking:self.isDisableSSLCertificateChecking
                                               proxySSLHLSKeys:PROXY_SSL_HLS_KEYS];
    [[IQPegasus sharedInstance] start];
}

-(NSURL*)builUrl
{
    NSURL * url = nil;
    NSString * str = [NSString stringWithFormat:@"%@:%ld/?apikey=%@",self.ineoQuestHost,(long)self.ineoQuestPort,self.ineoApi_key];
    url = [NSURL URLWithString:str];
    return url;
}

- (NSURL* )ineoQuestURLToPlay:(NSURL *)url
{
    // Stop the player
    //[mpPlayer stop];
    
    // Set content URL
    NSURL * contentURL = url;
    
    if (self.pegasusConfigSuccessful)
    {
        contentURL = [[IQPegasus sharedInstance] proxyURLForActualURL:url];
        [[IQPegasus sharedInstance] playerDidStart];
        //NSLog(@"playerDidStart with IQPegasus");
    }

    //NSLog(@"Player is now playing.");
    
    return  contentURL;
}


-(void)stopIneoQuestPressed
{
    //NSLog(@"Stop pressed.");
    [[IQPegasus sharedInstance] playerDidStop];
}

-(void)startIneoQuestPressed
{
    //NSLog(@"start pressed.");
    [[IQPegasus sharedInstance] playerDidStart];
}

-(void)pauseIneoQuestPressed
{
    //NSLog(@"pause pressed.");
    [[IQPegasus sharedInstance] playerDidPause];
}

//------------------------------------------------------------------------------
#pragma mark - UIApplication Notifications -
//------------------------------------------------------------------------------

- (void)addNotificationObservers
{
    [self removeNotificationObservers];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationWillResignActive:)
                                                 name:@"com.ineoquest.appwillresignactive"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationWillEnterForeground:)
                                                 name:@"com.ineoquest.appwillenterforeground"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(onUserDefaultsDidChange:)
//                                                 name:NSUserDefaultsDidChangeNotification
//                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMoviePlayerLoadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
    
}

- (void)removeNotificationObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)onApplicationWillResignActive:(NSNotification *)notification
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
#if !TARGET_IPHONE_SIMULATOR
    // Save the current playback state
    
    /* Swipe/Background fix

    if ([self.player playerStatus]) {
        playbackState = [self.player playerStatus];
        
        NSArray *playbackStates = @[@"Stopped", @"Playing", @"Paused", @"Interrupted", @"Seeking Backward", @"Seeking Forward"];
        NSLog(@"%s Playback state before application resigns active: %@", __PRETTY_FUNCTION__, playbackStates[playbackState]);
        
        // Intentionally pause player... it will be restored appropriately when
        // application enters the foreground.
        
        [self.player Pause];
     
    }
     
     */
#endif
    // Shutdown the SDK
    NSLog(@"%s Shutting down Endpoint SDK...", __PRETTY_FUNCTION__);
   // [self stopStats];
    [[IQPegasus sharedInstance] shutdown];
    
    NSLog(@"%s Shutting down Endpoint SDK DONE", __PRETTY_FUNCTION__);
}

- (void)onApplicationWillEnterForeground:(NSNotification *)notification
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [[IQPegasus sharedInstance] start];
}

- (void)restartPlayer
{
#if !TARGET_IPHONE_SIMULATOR
    /* Swipe/Background fix

    NSArray *playbackStates = @[@"Stopped", @"Playing", @"Paused", @"Interrupted", @"Seeking Backward", @"Seeking Forward"];
    NSLog(@"%s Restoring playback state to: %@", __PRETTY_FUNCTION__, playbackStates[playbackState]);
    switch (playbackState) {
        case MPMoviePlaybackStatePlaying:
            [self.player Play];
            break;
            
        case MPMoviePlaybackStateStopped:
            [self.player Stop];
            break;
            
        default:
            [self.player Pause];
            break;
    }
     */
#endif
}

- (void)onApplicationWillTerminate:(NSNotification *)notification
{
#if !TARGET_IPHONE_SIMULATOR
    NSLog(@"%s", __PRETTY_FUNCTION__);
//    if (mpPlayer) {
//        [mpPlayer stop];
//    }
    /* Swipe/Background fix
    [self.player Stop];
     */
    [[IQPegasus sharedInstance] shutdown];
#endif
}


//------------------------------------------------------------------------------
# pragma mark - MPMoviePlayer delegate methods -
//------------------------------------------------------------------------------

- (void)onMoviePlayerLoadStateDidChange:(NSNotification *)notification
{
    MPMoviePlayerController *mpc = (MPMoviePlayerController *)notification.object;
    
    NSUInteger bufferingStatus = 0; // Unknown
    
    MPMovieLoadState loadState = [mpc loadState];
    if (loadState & MPMovieLoadStatePlaythroughOK)
    {
        bufferingStatus = 3;
    }
    else if (loadState & MPMovieLoadStatePlayable)
    {
        bufferingStatus = 2;
    }
    else if (loadState & MPMovieLoadStateStalled)
    {
        bufferingStatus = 1;
    }
    
    //NSLog(@"%s Load state changed to %lu", __PRETTY_FUNCTION__, (unsigned long)bufferingStatus);
    
    [[IQPegasus sharedInstance] bufferingStatusChanged:bufferingStatus];
}

- (void)onMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    MPMoviePlayerController *mpc = (MPMoviePlayerController *)notification.object;
    
    MPMoviePlaybackState playbackState = [mpc playbackState];
    NSString *playbackStateStr = @"Unknown";
    switch (playbackState)
    {
        case MPMoviePlaybackStateInterrupted:
            playbackStateStr = @"Interrupted";
            break;
            
        case MPMoviePlaybackStatePaused:
            playbackStateStr = @"Paused";
            break;
            
        case MPMoviePlaybackStatePlaying:
            playbackStateStr = @"Playing";
            break;
            
        case MPMoviePlaybackStateSeekingBackward:
            playbackStateStr = @"Seeking Backward";
            break;
            
        case MPMoviePlaybackStateSeekingForward:
            playbackStateStr = @"Seeking Forward";
            break;
            
        case MPMoviePlaybackStateStopped:
            playbackStateStr = @"Stopped";
            break;
            
        default:
            break;
    }
    //NSLog(@"Playback state changed to %@", playbackStateStr);
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    MPMoviePlayerController *mpPlayer = [aNotification object];
    
    if (aNotification.userInfo)
    {
        NSNumber *reasonNum = [aNotification.userInfo objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
        MPMovieFinishReason finishReason = (MPMovieFinishReason)[reasonNum integerValue];
        
        if (finishReason == MPMovieFinishReasonPlaybackEnded)
        {
            //NSLog(@"The end of the movie was reached.");
            [[IQPegasus sharedInstance] playerDidFinishPlayback];
        }
        else if (finishReason == MPMovieFinishReasonUserExited)
        {
            //NSLog(@"The user stopped playback.");
            [[IQPegasus sharedInstance] playerDidStop];
        }
        else if (finishReason == MPMovieFinishReasonPlaybackError)
        {
            //NSLog(@"There was an error during playback.");
            NSError *error = nil;
            if (mpPlayer)
            {
                if ([[mpPlayer errorLog] events])
                {
                    if ([[[mpPlayer errorLog] events] count] > 0)
                    {
                        MPMovieErrorLogEvent *errorEvent = [[[mpPlayer errorLog] events] lastObject];
                        NSLog(@"Date: %@", errorEvent.date);
                        NSLog(@"URI: %@", errorEvent.URI);
                        NSLog(@"serverAddress: %@", errorEvent.serverAddress);
                        NSLog(@"playbackSessionID: %@", errorEvent.playbackSessionID);
                        NSLog(@"errorStatusCode: %li", (long)errorEvent.errorStatusCode);
                        NSLog(@"errorDomain: %@", errorEvent.errorDomain);
                        NSLog(@"errorComment: %@", errorEvent.errorComment);
                        error = [[NSError alloc] initWithDomain:errorEvent.errorDomain
                                                           code:errorEvent.errorStatusCode
                                                       userInfo:nil];
                    }
                }
            }
            if (error == nil)
            {
                // The error log of the media player might not have been updated
                // yet, so we'll need to generate our own error.
                error = [[NSError alloc] initWithDomain:@"MediaPlayerFramework"
                                                   code:0
                                               userInfo:nil];
                //NSLog(@"error = %@",error);
            }
            [[IQPegasus sharedInstance] playerDidEncounterError:error];
          
        }
    }
}

- (void)dealloc
{
    [[IQPegasus sharedInstance] shutdown];
    [self removeNotificationObservers];
}


//------------------------------------------------------------------------------
#pragma mark - IQPegasusDelegate methods -
//------------------------------------------------------------------------------

- (void)IQPegasusDidStart
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)IQPegasusDidShutdown
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

//------------------------------------------------------------------------------
#pragma mark - IQMPFAdapterDelegate methods -
//------------------------------------------------------------------------------

- (void)IQPegasusConfigurationDidSucceed
{
    self.pegasusConfigSuccessful = YES;
    NSLog(@"configure IQPegasus !!!");
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)IQPegasusConfigurationDidFailWithError:(NSError *)error
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"Could not configure IQPegasus\n%@", error);
    self.pegasusConfigSuccessful = NO;
}

- (void)IQPegasusConfigurationDidFailWithError:(NSError *)error
                                    reasonCode:(IQConfigurationFailureReasonCode)reasonCode
                                  reasonString:(NSString *)reasonString
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"Could not configure IQPegasus. Error code: %li, Reason:%@", (long)reasonCode, reasonString);
    
    NSString *msg = nil;
    switch (reasonCode)
    {
        case IQConfigurationFailureReasonCodeInvalidKey:
        case IQConfigurationFailureReasonCodeBlackListed:
        case IQConfigurationFailureReasonCodeACLRestriction:
            msg = [NSString stringWithFormat:@"Could not complete configuration.\nError: %li\nReason: %@", (long)reasonCode, reasonString];
            break;
        case IQConfigurationFailureReasonCodeIncompatible:
            msg = @"Could not complete configuration because this application version is incompatible. Please obtain a new version.";
            break;
        default:
            msg = @"Could not complete configuration. Please try again later.";
            break;
    }
    
    NSLog(@"An Error Has Occurred: %@",msg);
}

- (void)IQEndpointProxyServerDidStart
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    proxyServerStarted = YES;
//    if (mpPlayer != nil) {
//        if (mpPlayer.contentURL != nil) {
    /* Swipe/Background fix
            [self restartPlayer];
     */
//        }
//    }
}

- (int32_t)currentBitrate
{
    int32_t currentBitrate = -1;
  
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        // NOTE: It turns out that this gives the approximate WiFi throughput,
        // and not the bitrate that the player is playing at, so it reports
        // an incorrect value.
        currentBitrate = (int32_t)(event.observedBitrate + 0.5f);
    }
#endif
    
    return currentBitrate;
}

- (void)veriStreamChanged:(NSUInteger)veriStreamIndex
{
//    [vsOverlay addNewVeriStreamIndex:veriStreamIndex];
}

- (void)IQPegasusDidEncounterHTTPError:(NSInteger)httpError forURL:(NSURL *)url
{
    NSLog(@"%s httpError=%li, url=%@", __PRETTY_FUNCTION__, (long)httpError, url);
    NSString *msg = [NSString stringWithFormat:@"Encountered an HTTP %li error for URL:\n%@", (long)httpError, url];
     NSLog(@"An HTTPError Has Occurred: %@",msg);
}




- (NSDate *)reportPlaybackStartDate
{
    NSDate *playbackStartDate = nil;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        playbackStartDate = [event playbackStartDate];
    }
#endif
    
    return playbackStartDate;
}

- (NSString *)reportURI
{
    NSString *uri = nil;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        uri = [event URI];
    }
#endif
    
    return uri;
}

- (NSString *)reportServerAddress
{
    NSString *serverAddress = nil;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        serverAddress = [event serverAddress];
    }
#endif
    
    return serverAddress;
}

- (NSUInteger)reportNumberOfServerAddressChanges
{
    NSUInteger numberOfServerAddressChanges = 0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        numberOfServerAddressChanges = [event numberOfServerAddressChanges];
    }
#endif
    
    return numberOfServerAddressChanges;
}

- (NSTimeInterval)reportPlaybackStartOffset
{
    NSTimeInterval playbackStartOffset = -1.0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event)
    {
        playbackStartOffset = [event playbackStartOffset];
    }
#endif
    
    return playbackStartOffset;
}

- (NSTimeInterval)reportSegmentsDownloadedDuration
{
    NSTimeInterval segmentsDownloadedDuration = -1.0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        segmentsDownloadedDuration = [event segmentsDownloadedDuration];
    }
#endif
    
    return segmentsDownloadedDuration;
}

- (NSTimeInterval)reportDurationWatched
{
    NSTimeInterval durationWatched = -1.0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        durationWatched = [event durationWatched];
    }
#endif
    
    return durationWatched;
}

- (NSInteger)reportNumberOfStalls
{
    NSInteger numberOfStalls = -1;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        numberOfStalls = [event numberOfStalls];
    }
#endif
    
    return numberOfStalls;
}

- (int64_t)reportNumberOfBytesTransferred
{
    int64_t numberOfBytesTransferred = -1;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        numberOfBytesTransferred = [event numberOfBytesTransferred];
    }
#endif
    
    return numberOfBytesTransferred;
}

- (double)reportObservedBitrate
{
    double observedBitrate = -1.0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        observedBitrate = [event observedBitrate];
    }
#endif
    
    return observedBitrate;
}

- (double)reportIndicatedBitrate
{
    double indicatedBitrate = -1.0;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        indicatedBitrate = [event indicatedBitrate];
    }
#endif
    
    return indicatedBitrate;
}

- (NSInteger)reportMediaRequestsWWAN
{
    NSInteger mediaRequestsWWAN = -1;
    
    // NOTE: This API is only available in AVFoundation
    
    return mediaRequestsWWAN;
}

- (NSTimeInterval)reportTransferDuration
{
    NSTimeInterval transferDuration = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return transferDuration;
}

- (NSInteger)reportNumberOfMediaRequests
{
    NSInteger numberOfMediaRequests = -1;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        // NOTE: numberOfSegmentsDownloaded returns the same value as
        // numberOfMediaRequests does in AVFoundation
        numberOfMediaRequests = [event numberOfSegmentsDownloaded];
    }
#endif
    
    return numberOfMediaRequests;
}

- (NSString *)reportPlaybackType
{
    NSString *playbackType = nil;
    
    // NOTE: This API is only available in AVFoundation
    
    return playbackType;
}

- (NSTimeInterval)reportStartupTime
{
    NSTimeInterval startupTime = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return startupTime;
}

- (NSInteger)reportNumberOfDroppedVideoFrames
{
    NSInteger numberOfDroppedVideoFrames = -1;
    
#if !TARGET_IPHONE_SIMULATOR
    MPMovieAccessLogEvent *event = [self.player lastAccessLogEvent];
    if (event) {
        numberOfDroppedVideoFrames = [event numberOfDroppedVideoFrames];
    }
#endif
    
    return numberOfDroppedVideoFrames;
}

- (NSInteger)reportDownloadOverdue
{
    NSInteger downloadOverdue = -1;
    
    // NOTE: This API is only available in AVFoundation
    
    return downloadOverdue;
}

- (double)reportObservedBitrateStandardDeviation
{
    double observedBitrateStandardDeviation = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return observedBitrateStandardDeviation;
}

- (double)reportObservedMaxBitrate
{
    double observedMaxBitrate = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return observedMaxBitrate;
}

- (double)reportObservedMinBitrate
{
    double observedMinBitrate = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return observedMinBitrate;
}

- (double)reportSwitchBitrate
{
    double switchBitrate = -1.0;
    
    // NOTE: This API is only available in AVFoundation
    
    return switchBitrate;
}

/*
 TVPMovieLoadStateUnknown              = 0,
 TVPMovieLoadStatePlayable             = 1 << 0,
 TVPMovieLoadStatePlaythroughOK        = 1 << 1,
 TVPMovieLoadStateStalled              = 1 << 2,
 } TVPMovieLoadState;
 */
- (NSUInteger)reportBufferingStatus
{
    NSUInteger bufferingStatus = 0; // Unknown
    
#if !TARGET_IPHONE_SIMULATOR
    TVPMovieLoadState loadState = [self.player movieLoadStatus];
    if (loadState & TVPMovieLoadStatePlaythroughOK)
    {
        bufferingStatus = 3;
    }
    else if (loadState & TVPMovieLoadStatePlayable)
    {
        bufferingStatus = 2;
    }
    else if (loadState & TVPMovieLoadStateStalled)
    {
        bufferingStatus = 1;
    }
#endif
    
    return bufferingStatus;
}

- (NSInteger)reportBufferSize
{
    NSInteger bufferSize = -1; // Unknown
    
    // NOTE: Neither Media Player, nor AVFoundation provide a buffer size metric.
    
    return bufferSize;
}

- (void)IQEndpointDidEncounterHTTPError:(NSInteger)httpError
                                 forURL:(NSURL *)url
{
    NSLog(@"%s httpError=%li, url=%@", __PRETTY_FUNCTION__, (long)httpError, url);
    NSString *msg = [NSString stringWithFormat:@"Encountered an HTTP %li error for URL:\n%@", (long)httpError, url];
    NSLog(@"IQEndpointDidEncounterHTTPError: %@",msg);

    
}

- (void)IQPegasusProxyServerDidStart
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self IQEndpointProxyServerDidStart];
    
}
//------------------------------------------------------------------------------
#pragma mark - User ID -
//------------------------------------------------------------------------------

- (void)initUserID
{
    NSString * userID = [TVConfigurationManager getTVUDID];
    NSError *error = nil;
    [[IQPegasus sharedInstance] setUserIDWithString:userID error:&error];
    if (error != nil)
    {
        // Handle the error..., like logging it, for example
        NSLog(@"Could not set user ID. Reason: %@", [error localizedDescription]);
        NSLog(@"userID is: %@", userID);

        
    }}



@end
