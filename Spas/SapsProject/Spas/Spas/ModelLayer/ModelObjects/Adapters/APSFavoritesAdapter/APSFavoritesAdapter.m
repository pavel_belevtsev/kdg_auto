//
//  APSFavoritesAdapter.m
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSFavoritesAdapter.h"
#import "APSLoginViewController.h"

@implementation APSFavoritesAdapter

-(IBAction)likeButtonPressed:(UIButton *)sender
{
    if (self.currMediaItem == nil)
    {
        ASLogInfo(@" !-! You msut setup MediaItem before call this method !-!" );
        return;
    }
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        [self toggleAddRemoveFavorites:sender];
    }
    else
    {
        [self showLoginViewControllerIfNeededWithFailedBlock:^{
            
        } succededBlock:^{
        
            [self downloadFavoritesWithCompletionBlock:^{
                sender.selected = YES;
                if (![self isMediaInFavorites:self.currMediaItem.mediaID])
                {
                    [self addMediaToFavorites:self.currMediaItem.mediaID withCompletionBlock:nil];
                }
            }];
        }];

    }
}


-(void) showLoginViewControllerIfNeededWithFailedBlock:(void(^)(void)) failedBlock
                                         succededBlock:(void(^)(void )) succededBlock
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if (succededBlock)
        {
            succededBlock();
        }
    }
    else
    {
        UIView * mainView = [self.baseViewControllerDelegate baseViewControllerMainView:nil];
        APSLoginViewController * loginViewController = ( APSLoginViewController * )[[APSViewControllersFactory defaultFactory] loginViewController];
        loginViewController.baseViewControllerDelegate = self.baseViewControllerDelegate;
        [loginViewController
         showLoginViewControllerWithCancelBlock:^{
             
             APSLoginViewController * loginView = loginViewController;
             ASLogInfo(@"%@",loginView);
             if (failedBlock)
             {
                 failedBlock();
             }
             
         } failedBlock:^{
             
             APSLoginViewController * loginView = loginViewController;
             ASLogInfo(@"%@",loginView);
             
             
             if (failedBlock)
             {
                 failedBlock();
             }
             
         } succededBlock:^{
             APSLoginViewController * loginView = loginViewController;
             
             ASLogInfo(@"%@",loginView);
             
             if (succededBlock)
             {
                 succededBlock();
             }
         } onView:mainView];
    }
}

#pragma mark - Tracking -

-(void)trackAddToFavorites
{
    [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Add channel to favorites" eventLabel:[NSString stringWithFormat:@"channel = %@",self.currMediaItem.name]];
}

-(void)trackRemoveFromFavorites
{
     [[APSGoogleAnalyticsManager sharedGoogleAnalyticsManager] sendGAITrackerEventWithCategory:@"EPG" eventAction:@"Remove channel From favorites" eventLabel:[NSString stringWithFormat:@"channel = %@",self.currMediaItem.name]];
}
@end
