//
//  APSFavoritesAdapter.h
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "GCFavoritesAdapter.h"

@interface APSFavoritesAdapter : GCFavoritesAdapter

@property (assign, nonatomic) id<BaseViewControllerDelegate>  baseViewControllerDelegate;


-(IBAction)likeButtonPressed:(UIButton *)sender;


@end
