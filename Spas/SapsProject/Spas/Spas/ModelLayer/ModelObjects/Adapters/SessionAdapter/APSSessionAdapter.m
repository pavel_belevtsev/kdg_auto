//
//  APSSessionAdapter.m
//  Spas
//
//  Created by Israel Berezin on 8/11/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSSessionAdapter.h"
#import "APSSessionManager.h"

NSString * const KDGLoginOk  = @"0";
NSString * const KDGLoginOutOfNetwork = @"5";

@implementation APSSessionAdapter


-(id)init
{
    self = [super init];
    if (self) {
        self.canChangeUserLoginStatus = YES;
        self.isLoginHaveCustomError = NO;
    }
    return self;
}


-(void) ssoLoginWithUserName:(NSString *) username password:(NSString *) password
{
    [[APSSessionManager sharedTVSessionManager] ssoSignInWithUsername:username password:password providerID:0]; // ??? Mavrick
}

#pragma mark - handle customer custom login errores


-(void) setAssertBlock
{
    assertLogin assertLogin = ^(NSDictionary * response)
    {
        NSArray * dynamicData = [[[response objectForKey:@"UserData"] objectForKey:@"m_oDynamicData"] objectForKey:@"m_sUserData"];
        
        for (NSDictionary * dynamicDataDic in dynamicData)
        {
            // it ok also if user Out Of Network!
            if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:KDGLoginOk])
            {
                
                [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Login];
                
                 return YES;
            }
            else if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:KDGLoginOutOfNetwork])
            {
                
                [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_OutOfNetwork];
                
                 return YES;
            }
        }
        return NO;
    };
    
    [[TVSessionManager sharedTVSessionManager] setAssertLoginBlock:assertLogin];
}


-(void)loginFailed:(NSNotification*)notification
{
    if (self.canChangeUserLoginStatus)
    {
        [[APSSessionManager sharedAPSSessionManager] setUserLoginStaus:APSLoginStaus_Logout];
    }
    self.isLoginHaveCustomError = NO;
    [self unregisterForLoginProcessNotification];
    
    NSError *error = [notification.userInfo objectForKey:TVErrorKey];
    
    if (error.code == TVLoginStatusCustomError || error.code == 23)
    {
        self.isLoginHaveCustomError = YES;

        NSArray * dynamicData = nil;
        NSDictionary * response = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        if ([response isKindOfClass:[NSDictionary class]])
        {
            dynamicData = [[[response objectOrNilForKey:@"UserData"] objectOrNilForKey:@"m_oDynamicData"] objectOrNilForKey:@"m_sUserData"];
        }
        
        if (dynamicData == nil)
        {
            [self loginFailedCompleted:NSLocalizedString(@"CustomErorr_4", nil)];
            return;
        }
        
        NSString * message = @"";

        
        for (NSDictionary * dynamicDataDic in dynamicData)
        {
            if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:@"3"])
            {
                message = NSLocalizedString(@"CustomErorr_1", nil);
            }
            else if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:@"4"])
            {
                message = NSLocalizedString(@"CustomErorr_2", nil);
            }
            else if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:@"6"])
            {
                message = NSLocalizedString(@"CustomErorr_6", nil);
            }
            
            // SYN-4489
            else if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:@"23"])
            {
                message = NSLocalizedString(@"CustomErorr_4", nil);
            }
            //
            
            else if ([[dynamicDataDic objectForKey:@"m_sValue"] isEqualToString:@"99"])
            {
                message = NSLocalizedString(@"CustomErorr_4", nil);
            }
        }
        
        [self loginFailedCompleted:message];
        
    }
    else
    {
        TVLoginStatus status = error.code;
        NSString * message = NSLocalizedString(@"Unknown Error", nil);
        switch (status)
        {
            case TVLoginStatusUserDoesNotExist:
                message =  NSLocalizedString(@"User does not exist",nil);
                break;
            case TVLoginStatusWrongPasswordOrUserName:
                message =  NSLocalizedString(@"Wrong user name or password",nil);
                break;
            case TVLoginStatusInsideLockTime:
                message =  NSLocalizedString(@"User is in lock period",nil);
                break;
            case TVLoginStatusUserRemovedFromDomain:
                message = NSLocalizedString(@"User Removed From Domain",nil);
                break;
            default:
                message =  NSLocalizedString(@"Unknown Error",nil);
                break;
        }
        
        [self loginFailedCompleted:message];
    }
}


-(void) unregisterForLoginProcessNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerSignInCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerSignInFailedNotification object:nil];
    
}
@end
