//
//  APSSessionAdapter.h
//  Spas
//
//  Created by Israel Berezin on 8/11/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "GCSessionAdapter.h"

@interface APSSessionAdapter : GCSessionAdapter

@property BOOL isLoginHaveCustomError;
@property BOOL canChangeUserLoginStatus;

-(void)loginFailed:(NSNotification*)notification;

@end

extern NSString * const KDGLoginOk;
extern NSString * const KDGLoginOutOfNetwork;