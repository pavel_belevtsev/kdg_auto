//
//  TVBaseNetworkObject.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVBaseNetworkObject.h"
#import "TVNetworkQueue.h"
#import "TVPAPIRequest.h"

@implementation TVBaseNetworkObject


-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

@end
