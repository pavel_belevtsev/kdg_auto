//
//  TVNPVRProvider.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVNPVRProvider.h"
#import "TVPMediaAPI.h"
#import "TVNPVRRecordItem.h"

#define seriesSourceKey @"season"
#define seasonIdKey @"seasonId"

@interface TVNPVRProvider ()



@property (assign, nonatomic) NSInteger requestIndex ;
@property (assign, nonatomic) NSInteger lastPageIndex;
@property (assign, nonatomic) NSInteger lastPageSize;

@property (assign, nonatomic) NSInteger virtualPageIndex;
@property (assign, nonatomic) NSInteger virtualPageSize;


@property (assign, nonatomic) BOOL endOfRecords;


@property (strong, nonatomic) NSMutableArray * arrayRecordsStack;
@property (strong, nonatomic) NSMutableArray * arrayMentionedSeriesID;
@property (strong, nonatomic) NSDate * startTime;


@end

@implementation TVNPVRProvider


-(void) setupWithPageSize:(NSInteger ) pageSize andOrderBy:(TVRecordOrderBy)orderBy
{
    
    self.requestIndex = 0;
    self.arrayRecordsStack = [NSMutableArray array];
    self.arrayMentionedSeriesID = [NSMutableArray array];
    self.orderByType = orderBy;
    
    self.endOfRecords = NO;
    
    self.lastPageSize = pageSize;
    self.lastPageIndex = -1;
    
    self.virtualPageIndex = 0;
    self.virtualPageSize = pageSize*2;
    
    self.startTime = [NSDate date];
    
}

-(void) setUpNextRequest
{
    self.requestIndex++;
    self.virtualPageIndex = 0;
}

-(void) recoredsForPageSize:(NSInteger ) pageSize
                  pageIndex:(NSInteger) pageIndex
                    orderBy:(TVRecordOrderBy)orderBy
             withStartBlock:(void(^)(void)) startBlock
                failedBlock:(void(^)(void)) failedBlock
            completionBlock:(void(^)(NSArray * records)) completionBlock
{
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
    
        if ([NSThread isMainThread]) {
            NSLog(@"im in the main thread");
        }
        
        if (pageIndex == 0)
        {
            [self setupWithPageSize:pageSize andOrderBy:orderBy];
        }
        
        if (self.orderByType != orderBy)
        {
            failedBlock();
            return ;
        }
        
        // There is bug here that if self.endOfRecords us true and there is still records in the stack it will return to the failed block 
        if (pageIndex-self.lastPageIndex==1 && self.lastPageSize == pageSize /*&& self.endOfRecords == NO*/)
        {
            self.lastPageIndex = pageIndex;
            self.lastPageSize = pageSize;
            
            NSInteger startIndex = pageIndex*pageSize;
            startIndex=MIN(self.arrayRecordsStack.count, startIndex);
            NSInteger lengthToRetreive = MIN(self.arrayRecordsStack.count-startIndex, pageSize);
            
            NSArray * fetchedRecordings = [self.arrayRecordsStack  subarrayWithRange:NSMakeRange(startIndex, lengthToRetreive)];
            
            while (fetchedRecordings.count<pageSize && self.endOfRecords == NO)
            {
                // get more records
                TVPAPIRequest *request = [self requestForIndex:self.requestIndex];
                
                [request startSynchronous];
                
                NSError *error = [request error];
                
                if (error)
                {
                    if (failedBlock)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            failedBlock();
                        });
                    }
                    
                }else
                {
                    NSArray *response = [request JSONResponse];
                    
                    // If we get less records then we expexted than is is the end and we dont need to continue asking the server for new items
                    
                    // filter out all completed and all not first appearence episode of new series
                    NSMutableArray * thisSycleRecordings = [NSMutableArray array];
                    for (NSDictionary * dictionary in response)
                    {
                        TVNPVRRecordItem * item = [[TVNPVRRecordItem alloc] initWithDictionary:dictionary];
                        // is item completed so ignore item
                        if (![self isItemIgnored:item])
                        {
                            if ([item.recordSource isEqualToString:seriesSourceKey]) {
                                
                                NSString * seriesId = [[item.epgTags objectForKey:seasonIdKey] lastObject];
                                [self.arrayMentionedSeriesID addObject:seriesId];
                            }
                            
                            [thisSycleRecordings addObject:item];
                        }
                        
                    }
                    
                    self.arrayRecordsStack = [NSMutableArray arrayWithArray:[self.arrayRecordsStack arrayByAddingObjectsFromArray:thisSycleRecordings]];
                    
                    lengthToRetreive = MIN(self.arrayRecordsStack.count-startIndex, pageSize);
                    fetchedRecordings = [self.arrayRecordsStack  subarrayWithRange:NSMakeRange(startIndex, lengthToRetreive)];
                    
                    self.virtualPageIndex++;
                    
                    
                    if (response.count<self.virtualPageSize)
                    {
                        
                        if ([self requestForIndex:self.requestIndex+1]!= nil)
                        {
                            [self setUpNextRequest];
                        }
                        else
                        {
                            self.endOfRecords = YES;
                        }
                        
                    }

                }
            }
            
            
            if (completionBlock)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    completionBlock(fetchedRecordings);
                });
            }
            // call completion block
            
        }
        else
        {
            if (failedBlock)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    failedBlock();
                });
            }
            
            // the calles are not sequenced we can't support this requests
        }
        
    });
    
}


-(BOOL) isEpisodeWithTheSameSeriesAlreadyMentioned:(TVNPVRRecordItem *) recordItem
{
    NSString * seriesId = [[recordItem.epgTags objectForKey:seasonIdKey] lastObject];
    if ([self.arrayMentionedSeriesID containsObject:seriesId])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



-(TVPAPIRequest *) requestForIndex:(NSInteger)index
{
    return nil;
}

-(BOOL) isItemIgnored:(TVNPVRRecordItem *) item
{
    return [self isEpisodeWithTheSameSeriesAlreadyMentioned:item];
}



@end
