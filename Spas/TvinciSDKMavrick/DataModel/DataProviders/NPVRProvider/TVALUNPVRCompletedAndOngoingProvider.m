//
//  TVALUNPVRProvider.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVALUNPVRCompletedAndOngoingProvider.h"
#import "TVPMediaAPI.h"
#import "TVNPVRRecordItem.h"



@interface TVALUNPVRCompletedAndOngoingProvider ()

@property (assign, nonatomic) NSInteger lastPageIndex;
@property (assign, nonatomic) NSInteger lastPageSize;

@property (assign, nonatomic) NSInteger virtualPageIndex;
@property (assign, nonatomic) NSInteger virtualPageSize;


@property (strong, nonatomic) NSMutableArray * arrayRecordsStack;
@property (strong, nonatomic) NSMutableArray * arrayMentionedSeriesID;
@property (strong, nonatomic) NSDate * startTime;

@end


@implementation TVALUNPVRCompletedAndOngoingProvider

-(TVPAPIRequest *) requestForIndex:(NSInteger) index
{
    switch (index)
    {
        case 0:
        {
            return [TVPMediaAPI requestForGetRecordingsWithPageSize:self.virtualPageSize
                                                          pageIndex:self.virtualPageIndex
                                                           searchBy:TVRecordingSearchByTypeRecordingStatus
                                                       epgChannelID:nil
                                                    recordingStatus:TVRecordingStatusOngoing
                                                       recordingIDs:nil
                                                        programsIds:nil
                                                          seriesIds:nil
                                                          startDate:self.startTime
                                                            orderBy:self.orderByType
                                                     orderDirection:TVRecordOrderDirectionDescending
                                                       withdelegate:nil];
        }
            break;
        case 1:
        {
            return [TVPMediaAPI requestForGetRecordingsWithPageSize:self.virtualPageSize
                                                          pageIndex:self.virtualPageIndex
                                                           searchBy:TVRecordingSearchByTypeRecordingStatus
                                                       epgChannelID:nil
                                                    recordingStatus:TVRecordingStatusCompleted
                                                       recordingIDs:nil
                                                        programsIds:nil
                                                          seriesIds:nil
                                                          startDate:self.startTime
                                                            orderBy:self.orderByType
                                                     orderDirection:TVRecordOrderDirectionDescending
                                                       withdelegate:nil];
        }
            break;
            
        default:
            return nil;
            break;
    }
    
}




@end
