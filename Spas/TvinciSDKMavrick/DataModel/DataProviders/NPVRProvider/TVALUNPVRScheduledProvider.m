//
//  TVALUNPVRCompletedProvider.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/12/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVALUNPVRScheduledProvider.h"
#import "TVPAPIRequest.h"
#import "TVNPVRRecordItem.h"

@interface TVALUNPVRScheduledProvider ()

@property (assign, nonatomic) NSInteger lastPageIndex;
@property (assign, nonatomic) NSInteger lastPageSize;

@property (assign, nonatomic) NSInteger virtualPageIndex;
@property (assign, nonatomic) NSInteger virtualPageSize;


@property (strong, nonatomic) NSMutableArray * arrayRecordsStack;
@property (strong, nonatomic) NSMutableArray * arrayMentionedSeriesID;
@property (strong, nonatomic) NSDate * startTime;


@end

@implementation TVALUNPVRScheduledProvider


-(TVPAPIRequest *) requestForIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            return [TVPMediaAPI requestForGetRecordingsWithPageSize:self.virtualPageSize
                                                          pageIndex:self.virtualPageIndex
                                                           searchBy:TVRecordingSearchByTypeRecordingStatus
                                                       epgChannelID:nil
                                                    recordingStatus:TVRecordingStatusScheduled
                                                       recordingIDs:nil
                                                        programsIds:nil
                                                          seriesIds:nil
                                                          startDate:nil
                                                            orderBy:self.orderByType
                                                     orderDirection:TVRecordOrderDirectionDescending
                                                       withdelegate:nil];
            break;
       
        default:
            return nil;
            break;
    }
    
    
}


@end
