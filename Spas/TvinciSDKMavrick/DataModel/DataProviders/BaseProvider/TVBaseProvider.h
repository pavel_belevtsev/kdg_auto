//
//  TVBaseProvider.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVBaseNetworkObject.h"

@interface TVBaseProvider : TVBaseNetworkObject

@end
