//
//  PaginationController.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 12/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "PaginationController.h"


#define activeFetchRequestsKey(PageIndex,PageSize)  [NSString stringWithFormat:@"PageIndex:%ld,PageSize:%ld",PageIndex,PageSize]

@interface PaginationController ()

@property (strong, nonatomic) NSMutableArray * arrayItems;
@property (assign, nonatomic) NSInteger pageSize;
@property (strong, nonatomic) NSMutableArray * arrayActiveFetchRequests;
@property (assign, nonatomic) NSInteger startOffsetCount;
@property (assign, nonatomic) BOOL endOfList;
@end

@implementation PaginationController

-(id)initWithPageSize:(NSInteger) pageSize
     initialPageIndex:(NSInteger) pageIndex
           fetchBlock:(FetchBlock) fetchBlock
          startOffset:(NSInteger) offset
             delegate:(id<PaginationControllerDelegate>) delegate;
{
    self = [super init];
    if (self)
    {
        self.pageSize =pageSize;
        self.fetchBlock = fetchBlock;
        self.startOffsetCount = offset;
        self.delegate = delegate;
        self.arrayActiveFetchRequests = [NSMutableArray array];
        
        [self loadFirstPage];
    }
    
    return self;
}

-(void) loadFirstPage
{
    __weak PaginationController * blockSelf = self;
    NSInteger blockIndex = 0;
    NSInteger pageIndex = 0;
    
    if(self.fetchBlock)
    {
        [self.arrayActiveFetchRequests addObject:activeFetchRequestsKey((long)pageIndex,(long)blockSelf.pageSize)];
        
        if ([self.delegate respondsToSelector:@selector(paginationController:didStartLodingNewPage:)])
        {
            [self.delegate paginationController:self didStartLodingNewPage:pageIndex];
        }
        
        self.fetchBlock(pageIndex,blockSelf.pageSize,
                        ^(NSArray *nextItems)
                        {
                            [blockSelf addItems:nextItems forPage:pageIndex];
                            [blockSelf.delegate paginationController:blockSelf refreshAndScrollToRowAtIndex:blockIndex];

                            [blockSelf.arrayActiveFetchRequests removeObject:activeFetchRequestsKey((long)pageIndex,(long)blockSelf.pageSize)];
                            
                            if ([self.delegate respondsToSelector:@selector(paginationController:didFinishLodingNewPage:)])
                            {
                                [self.delegate paginationController:self didFinishLodingNewPage:pageIndex];
                            }
                        },
                        ^{
                            // show error
                        });
    }

}

-(id) objectAtIndex:(NSInteger) index
{
    // out of bounds
    if (index>=self.arrayItems.count )
    {
        return nil;
    }
    
    // if index is not last index
    if (index != self.arrayItems.count-1 || self.endOfList == YES)
    {
        return [self.arrayItems objectAtIndex:index];
    }
    
    // if there is more items to load and we reached the last index
    id item = [self.arrayItems objectAtIndex:index];
    
    // index is the last index lets load more
    
    
    
    __weak PaginationController * blockSelf = self;
     NSInteger blockIndex = index;
     NSInteger pageIndex = index/self.pageSize+1;
    
    if ([self.arrayActiveFetchRequests containsObject:activeFetchRequestsKey((long)pageIndex,(long)blockSelf.pageSize)])
    {
        return item ;
    }
    
    if(self.fetchBlock)
    {
        [self.arrayActiveFetchRequests addObject:activeFetchRequestsKey((long)pageIndex,(long)blockSelf.pageSize)];
        
        if ([self.delegate respondsToSelector:@selector(paginationController:didStartLodingNewPage:)])
        {
            [self.delegate paginationController:self didStartLodingNewPage:pageIndex];
        }
        
        self.fetchBlock(pageIndex,blockSelf.pageSize,
                        ^(NSArray *nextItems)
                        {
                            [blockSelf addItems:nextItems forPage:0];
                            [blockSelf.delegate paginationController:blockSelf refreshAndScrollToRowAtIndex:blockIndex];

                            [blockSelf.arrayActiveFetchRequests removeObject:activeFetchRequestsKey((long)pageIndex,(long)blockSelf.pageSize)];
                            
                            if ([self.delegate respondsToSelector:@selector(paginationController:didFinishLodingNewPage:)])
                            {
                                [self.delegate paginationController:self didFinishLodingNewPage:pageIndex];
                            }
                        },
                        ^{
                            // show error
                        });
    }

    return item;
}

-(void) addItems:(NSArray *) array forPage:(NSInteger) pageIndex
{
    
    if ((pageIndex==0 && array.count<(self.pageSize-self.startOffsetCount))||(pageIndex!=0 && array.count<self.pageSize ))
    {
        self.endOfList = YES;
    }
    
    NSArray * firstArray = self.arrayItems;
    NSArray * secondArray = array;
    NSMutableArray * completedArray = [NSMutableArray arrayWithArray:firstArray];
    [completedArray addObjectsFromArray:secondArray];
    self.arrayItems = completedArray;
}


-(NSInteger) count
{
    return self.arrayItems.count;
}

-(void) dealloc
{

}

@end
