//
//  TVSDKSettings.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVSDKSettings.h"

#define kOfflineModeKey @"offlineMode"

@implementation TVSDKSettings

+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}



-(instancetype)init
{
    if (self = [super init])
    {
        [self loadSettings];
    }
    
    return self;

}
-(void) saveSettings
{
    [[NSUserDefaults standardUserDefaults] setBool:self.offlineMode forKey:kOfflineModeKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) loadSettings
{
    self.offlineMode = [[NSUserDefaults standardUserDefaults] boolForKey:kOfflineModeKey];
}

@end
