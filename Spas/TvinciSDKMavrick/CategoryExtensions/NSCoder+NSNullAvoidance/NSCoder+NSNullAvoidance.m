//
//  NSCoder+NSNullAvoidance.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/29/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "NSCoder+NSNullAvoidance.h"

@implementation NSCoder (NSNullAvoidance)


-(id) decodeObjectOrNilForKey:(id)aKey
{
    id objectForKey = [self decodeObjectForKey:aKey];
    if ([objectForKey isEqual:[NSNull null]])
    {
        objectForKey = nil;
    }
    return objectForKey;
}

-(void) encodeObjectOrNil : (id) object forKey : (id) key
{
    if (object != nil)
    {
        [self encodeObject:object forKey:key];
    }
}




@end
