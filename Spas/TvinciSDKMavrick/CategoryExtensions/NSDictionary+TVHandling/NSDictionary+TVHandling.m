//
//  NSDictionary+TVHandling.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/1/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "NSDictionary+TVHandling.h"

@implementation NSDictionary (TVHandling)


#define kKeyOfKey @"m_sKey"
#define kKeyOfValue @"m_sValue"

-(NSArray *) wrappedToArray
{
    NSMutableArray * wrappedList = [NSMutableArray array];
    for (NSString * key in self.allKeys)
    {
        NSString * value = [self objectForKey:key];
        if (value)
        {
            NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
            [dictionary setObject:key forKey:kKeyOfKey];
            [dictionary setObject:value forKey:kKeyOfValue];
            
            [wrappedList addObject:dictionary];
        }
        
    }
    
    return wrappedList;

}
@end
