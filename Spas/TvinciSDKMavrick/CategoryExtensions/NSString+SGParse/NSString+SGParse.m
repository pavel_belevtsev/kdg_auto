//
//  NSString+SGParse.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "NSString+SGParse.h"

@implementation NSString (SGParse)

-(NSString *) stringWithoutDigits{
    NSString* noDigitStr = [self stringByReplacingOccurrencesOfString:@"0" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"1" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"2" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"3" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"4" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"5" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"6" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"7" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"8" withString:@""];
    noDigitStr = [noDigitStr stringByReplacingOccurrencesOfString:@"9" withString:@""];
    
    return noDigitStr;

}

- (NSString *)stringByDecodingURLFormat
{
    NSString *result = [(NSString *)self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

/*
 * Checks given string to see if it includes ONLY numerics
 */
+ (BOOL) confirmedNumericsOnlyInString:(NSString*)string ShouldShowWarning:(BOOL)shouldShowWarning {
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    if ([string rangeOfCharacterFromSet:set].location == NSNotFound)
    {
        return YES;
    }
    
    return NO;
}
@end
