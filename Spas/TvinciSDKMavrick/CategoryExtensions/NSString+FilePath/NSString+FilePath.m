//
//  NSString+FilePath.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NSString+FilePath.h"

@implementation NSString (FilePath)

+(NSString *) stringfilePathForCashesDirectoryWithName:(NSString *) name extension:(NSString *) extension
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString  *cachaseDirectory = [paths objectAtIndex:0];
    NSString  *filePath = nil;
    if (extension)
    {
        filePath = [NSString stringWithFormat:@"%@/%@.%@", cachaseDirectory,name,extension];
    }
    else
    {
        filePath = [NSString stringWithFormat:@"%@/%@", cachaseDirectory,name];
    }
    return filePath;
}

@end
