//
//  UIButton+ImageResize.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "UIButton+ImageResize.h"
#import "UIImage+Resize.h"

@implementation UIButton (ImageResize)

-(void) stretchBackgroundImageUsing1X1{
    [self setBackgroundImage:[self.currentBackgroundImage resizableImageWith1x1StretchableArea] forState:UIControlStateNormal];
}

-(void) stretchSelectedBackgroundImageUsing1X1{
    [self setBackgroundImage:[[self backgroundImageForState:UIControlStateSelected] resizableImageWith1x1StretchableArea] forState:UIControlStateSelected];
}

-(void) stretchBackgroundImageUsing1XHeight{
    [self setBackgroundImage:[self.currentBackgroundImage resizableImageWith1xHightStretchableArea] forState:UIControlStateNormal];
}
@end
