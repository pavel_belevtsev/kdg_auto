//
//  NSURL+SpaciousIgnore.m
//  TvinciSDK
//
//  Created by Kaltura Support on 12/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "NSString+TVNSURLUtilities.h"

@implementation NSString (TVNSURLUtilities)


//NSString *URLString = [dictionary objectOrNilForKey:TVFileBaseURLKey];
//self.fileURL = (URLString.length > 0)? [NSURL URLWithString:URLString] : nil;

-(NSString *)stringByTrimmingSpaces
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString *)trimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet {
    NSUInteger length = [self length];
    unichar charBuffer[length];
    [self getCharacters:charBuffer];
    
    NSInteger location = 0;
    for (location = 0 ; location < length; location++)
    {
        if (![characterSet characterIsMember:charBuffer[location]]) {
            break;
        }
    }
    
    return [self substringWithRange:NSMakeRange(location, length - location)];
}

- (NSString *)trimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet {
    NSUInteger location = 0;
    NSUInteger length = [self length];
    unichar charBuffer[length];
    [self getCharacters:charBuffer];
    
    for (length = [self length]; length > 0; length--) {
        if (![characterSet characterIsMember:charBuffer[length - 1]]) {
            break;
        }
    }
    
    return [self substringWithRange:NSMakeRange(location, length - location)];
}



@end
