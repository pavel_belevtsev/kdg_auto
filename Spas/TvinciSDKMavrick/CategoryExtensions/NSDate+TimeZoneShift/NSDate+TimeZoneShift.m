//
//  NSDate+TimeZoneShift.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 3/5/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "NSDate+TimeZoneShift.h"

@implementation NSDate (TimeZoneShift)
-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) fromGMTtoTimeZone:(NSTimeZone*)tz{
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}
@end
