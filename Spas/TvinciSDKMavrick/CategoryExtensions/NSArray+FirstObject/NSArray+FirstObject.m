//
//  NSArray+FirstObject.m
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "NSArray+FirstObject.h"

@implementation NSArray (FirstObject)
-(id) firstObject
{
    return (self.count > 0) ? [self objectAtIndex:0] : nil;
}
@end
