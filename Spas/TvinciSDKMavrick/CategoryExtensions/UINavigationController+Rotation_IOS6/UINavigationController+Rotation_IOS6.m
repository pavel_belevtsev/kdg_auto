//
//  UINavigationController+Rotation_IOS6.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 11/22/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "UINavigationController+Rotation_IOS6.h"

@implementation UINavigationController (Rotation_IOS6)
-(BOOL)shouldAutorotate
{
//    BOOL shouldAutoRotate = [[self.viewControllers lastObject] shouldAutorotate];
    BOOL shouldAutoRotate = [self.topViewController shouldAutorotate];
    return shouldAutoRotate;
}

-(NSUInteger)supportedInterfaceOrientations
{
//    NSUInteger supOrientation = [[self.viewControllers lastObject] supportedInterfaceOrientations];
    NSUInteger supOrientation = [self.topViewController supportedInterfaceOrientations];
    return supOrientation;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
//    UIInterfaceOrientation prefOrientation = [[self.viewControllers lastObject]  preferredInterfaceOrientationForPresentation];
    UIInterfaceOrientation prefOrientation = [self.topViewController preferredInterfaceOrientationForPresentation];
    if(prefOrientation != UIInterfaceOrientationLandscapeLeft && prefOrientation != UIInterfaceOrientationLandscapeRight &&
       prefOrientation != UIInterfaceOrientationPortrait && prefOrientation != UIInterfaceOrientationPortraitUpsideDown){
        prefOrientation = UIInterfaceOrientationLandscapeLeft;
    }
    return prefOrientation;
    
}
@end
