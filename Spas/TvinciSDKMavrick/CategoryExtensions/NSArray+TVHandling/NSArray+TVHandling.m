//
//  NSArray+TVHandling.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "NSArray+TVHandling.h"

@implementation NSArray (TVHandling)


#define kKeyOfKey @"m_sKey"
#define kKeyOfValue @"m_sValue"

-(NSArray *) wrappedArray
{
    
    NSMutableArray * result = [NSMutableArray array];
    
    for (NSDictionary * dictionary in self)
    {
        id key = dictionary.allKeys.firstObject;
        id value = [dictionary objectForKey:key];
        
        if (key != nil && value != nil)
        {
            NSDictionary * wrappedDicionary = @{kKeyOfKey:key,kKeyOfValue:value};
            [result addObject:wrappedDicionary];
        }
    }
    
    return [NSArray arrayWithArray:result];
    
}

@end
