//
//  NSString+Additions.h
//  KalturaUnified
//
//  Created by Alex Zchut on 4/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

@interface NSString (JsonData)
-(NSString*) unescapeFromJsonString;
@end