//
//  NSString+Additions.m
//  KalturaUnified
//
//  Created by Alex Zchut on 4/8/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NSString+JsonData.h"

@implementation NSString (JsonData)

-(NSString*) unescapeFromJsonString  {
    return [[NSJSONSerialization JSONObjectWithData:(NSData*)[[[NSString alloc] initWithFormat:@"{\"value\": \"%@\"}", self] dataUsingEncoding:4] options:0 error:nil]  objectForKey:@"value"];
}
@end
