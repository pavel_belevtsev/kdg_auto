//
//  NSArray+Equivalent.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/15/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "NSArray+Equivalent.h"

@implementation NSArray (Equivalent)

-(BOOL) isEquivalent:(NSArray *) array
{
    NSArray * firstArray = self;
    NSArray * secondArray = array;

    BOOL firstContainsSecondArray = YES;
    BOOL secondContainsFirstArray = YES;
    for (id object in firstArray)
    {
        if (![secondArray containsObject:object])
        {
            secondContainsFirstArray = NO;
        }
    }

    for (id object in secondArray)
    {
        if (![firstArray containsObject:object])
        {
            firstContainsSecondArray = NO;
        }
    }

    return ( firstContainsSecondArray == true && secondContainsFirstArray == true );

}

@end
