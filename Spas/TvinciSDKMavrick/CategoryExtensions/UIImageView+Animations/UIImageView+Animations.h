//
//  UIImageView+Animations.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 5/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIImageView (Animations)

-(void) rotateImageForeverWithDuration:(float)dur;

@end
