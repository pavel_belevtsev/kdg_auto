//
//  UIImageView+Animations.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 5/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "UIImageView+Animations.h"

@implementation UIImageView (Animations)

-(void) rotateImageForeverWithDuration:(float)dur{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = dur;
    fullRotation.repeatCount = 3000;
    [self.layer addAnimation:fullRotation forKey:@"360"];

}



@end
