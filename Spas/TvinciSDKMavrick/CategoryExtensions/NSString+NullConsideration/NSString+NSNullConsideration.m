//
//  NSString+NSNullConsideration.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/15/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "NSString+NSNullConsideration.h"

@implementation NSString (NSNullConsideration)

-(id) nullOrString
{
    if ([self isEqualToString:@"null"])
    {
        return [NSNull null];
    }
    
    return self;
}
@end
