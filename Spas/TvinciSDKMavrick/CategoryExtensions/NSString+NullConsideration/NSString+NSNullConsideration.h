//
//  NSString+NSNullConsideration.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/15/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSNullConsideration)

-(id) nullOrString;
@end
