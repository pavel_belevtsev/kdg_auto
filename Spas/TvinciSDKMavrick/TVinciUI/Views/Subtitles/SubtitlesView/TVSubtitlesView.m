
//
//  subtitlesView.m
//  SubtitlesTest
//
//  Created by Avraham Shukron on 11/21/12.
//  Copyright (c) 2012 quickode. All rights reserved.
//

#import "TVSubtitlesView.h"
#import "SmartLogs.h"

@implementation TVSubtitlesView

@synthesize delegate = _delegate;
@synthesize subtitleFile = _subtitleFile, subtitlesManager = _subtitlesManager, subsAreOff = _subsAreOff;

- (void)dealloc
{

}

-(void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    
    if (newWindow == nil) {
        // Will be removed from window, similar to -viewDidUnload.
        // Unsubscribe from any notifications here.
        
        [self stopShowingSubtitles];
        
    }
    // 30. Show status bar
    
    
    
}


-(void)didMoveToWindow{
    [super didMoveToWindow];
    
    if (self.window) {
        // Added to a window, similar to -viewDidLoad.
        // Subscribe to notifications here.
        
    }
    
}


-(void) setDelegate:(id<TVMoviePlayerWithSubtitlesDelegate>)delegate {
	
	if(_delegate != delegate)
	{
		_delegate = delegate;
		
		if(self.subtitlesManager ==nil)
		{
			self.subtitlesManager = [[TVSubtitleFactory alloc] init] ;
			self.subtitlesManager.delegate=self;
            
		}
		
	}
	
}

-(void) updateVisibleSubtitles
{
    
    if(_subsAreOff || self.delegate == nil || _subtitleFile == nil){
        return;
    }
    
    NSTimeInterval time = 0;
    
    if (self.delegate != nil)
    {
         time =[self.delegate timeIntervalSinceMovieBegin];
    }
    
    long longtime = time*1000;
    if (time!=0) {
        CMTime timecm = CMTimeMake(longtime, 1000);
        
        SubRipItem *itm=nil;
        itm = [self.subtitlesManager indexOfSubRipItemAfterSeekWithStartTime:timecm];
        
        if (itm!=nil)
        {
            self.hidden=NO;
            self.text=[NSString stringWithFormat:@"%@",itm.text];
            
        }else {
            self.text=@"";
            self.hidden=YES;
        }
    }
    
    
}

-(void) setSubtitleFile:(NSString *)subtitleFile {
    
    if(_subtitleFile != subtitleFile)
    {

        _subtitleFile = subtitleFile ;
        
        [self.subtitlesManager populateFromJson:subtitleFile delay:0];
    }
}

-(void) startShowingSubtitles {
    
//    TVLogDebug(@" [12] >>> startShowingSubtitles %@",self);////////
	[self.subtitlesManager startVideoTimer];
}

-(void) stopShowingSubtitles {
//    TVLogDebug(@" [12] stopShowingSubtitles %@",self);////////
	[self.subtitlesManager stopVideoTimer];
}

-(void) removeDelegates{
    self.delegate = nil;
    self.subtitlesManager.delegate = nil;
}



@end
