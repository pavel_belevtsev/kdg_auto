//
//  JSONCreator.m
//  SubtitlesTest
//
//  Created by quickode on 11/20/12.
//  Copyright (c) 2012 quickode. All rights reserved.
//

#import "TVJSONCreator.h"
#import "GDataXMLNode.h"
#import "Subtitle.h"









@implementation TVJSONCreator

SYNTHESIZE_SINGLETON_FOR_CLASS(TVJSONCreator);


-(NSString *) JSONSubtitlesFromXML:(NSString *) xmlSubtitles
{
    NSError * err ;
    GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:xmlSubtitles options:0 error:&err];
    
    if(doc == nil)
    {
        TVLogDebug(@"%@",err);
        return nil;
    }
    
    
    GDataXMLElement * bodyElemnt = [[[doc rootElement] elementsForName:@"body"] lastObject];
    GDataXMLElement * divElemnt = [[bodyElemnt elementsForName:@"div"] lastObject];
    NSArray * subtitlesTimeStamps = [divElemnt elementsForName:@"p"];
    NSMutableArray * targetArray = [NSMutableArray array];
    
  
    [subtitlesTimeStamps enumerateObjectsUsingBlock:^(GDataXMLElement * subtitleElemnt , NSUInteger idx, BOOL *stop) {

        GDataXMLNode * begin = [subtitleElemnt attributeForName:@"begin"];
        GDataXMLNode * end = [subtitleElemnt attributeForName:@"end"];
        
        NSString * subtitleContentString = [subtitleElemnt stringValue];
        NSString * beginString = [begin stringValue];
        NSString * endString =[end stringValue];
        
        NSNumber * endTimeInMs = [self milisecondFromString:endString];
        NSNumber * beginTimeInMS = [self milisecondFromString:beginString];
        
        NSDictionary * subtitleItem = [NSDictionary dictionaryWithObjectsAndKeys:subtitleContentString,KEY_SUBTITLE_TEXT,beginTimeInMS,KEY_START_TIME,endTimeInMs,KEY_END_TIME,nil];
        [targetArray addObject:subtitleItem];
    
    }];


    
    NSError * e;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:targetArray
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&e];
    
    NSString *jsonString = nil;
    if (jsonData) {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    NSString * jsonResult = jsonString;
    return jsonResult;
}

// string format is : @"hh:mm:ss.ms"
// 1ms = 1ms
// 1s = 10^3 ms
// 1m = 60*10^3 ms
// 1h = 60*60*10^3 ms

-(NSNumber *) milisecondFromString:(NSString *) timeString {
    NSCharacterSet * carSet =[NSCharacterSet characterSetWithCharactersInString:@":."];
    NSArray * timeComponents = [timeString componentsSeparatedByCharactersInSet:carSet];
    
    //index 0: ms index 1:ss index 2:mm index 3:hh
    
    long timeInMs = [[timeComponents objectAtIndex:0] integerValue]*60;
    timeInMs = (timeInMs+[[timeComponents objectAtIndex:1] integerValue])*60;
    timeInMs = (timeInMs +[[timeComponents objectAtIndex:2] integerValue])*(10*10*10);
    timeInMs = timeInMs + [[timeComponents objectAtIndex:3] integerValue];
    return [NSNumber numberWithInteger:timeInMs];
}


-(NSArray *) arraySmilFromXML:(NSString *) xmlSmil
{
	NSError * err ;
    GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:xmlSmil options:0 error:&err];
    
    if(doc == nil)
    {
        TVLogDebug(@"%@",err);
        return nil;
    }
    
    
    GDataXMLElement * bodyElemnt = [[[doc rootElement] elementsForName:@"body"] lastObject];

	NSArray * subtitlesStreams = [bodyElemnt nodesForXPath:@"//smil/body/par/switch/textstream" error:&err];
	
	NSMutableArray * targetArray = [NSMutableArray array];

	[subtitlesStreams enumerateObjectsUsingBlock:^(GDataXMLElement * streamElem, NSUInteger idx, BOOL *stop) {
		
		GDataXMLNode * srcElem = [streamElem attributeForName:@"src"];
        GDataXMLNode * lngElem = [streamElem attributeForName:@"systemLanguage"];
		
        NSString * sourceURL = [srcElem stringValue];
        NSString * language = [lngElem stringValue];
		
		//NSDictionary * languageDic = [NSDictionary dictionaryWithObjectsAndKeys:sourceURL,KEY_SOURCE_LNG_URL,language, KEY_LANGUAGE_IDENTIFIER, nil];
        Subtitle * subtitle = [Subtitle subtitleWithLanguage:language fileURL:[NSURL  URLWithString:sourceURL]];
		[targetArray addObject:subtitle];
	}];
	
	return targetArray ;
}

-(NSMutableDictionary*)smilToDictionary:(NSString*)smilURL
{
    
    NSString *smil;
    
    //CommentOut To Use with REMOTE Path
    
    /*smil = [NSString stringWithContentsOfURL:[NSURL URLWithString:smilURL] encoding:NSUTF8StringEncoding  error:nil];*/
    smil = [NSString stringWithContentsOfURL:[NSURL URLWithString:smilURL] encoding:NSUTF8StringEncoding  error:nil];
    
    
    //CommentOut To Use with LOCAL Path
    /*smil = [NSString stringWithContentsOfFile:smilURL encoding:NSUTF8StringEncoding error:nil];*/
    
    NSError * err ;
    
    GDataXMLDocument * doc = [[GDataXMLDocument alloc] initWithXMLString:smil options:0 error:&err];
    NSMutableDictionary *smilDictionary =[NSMutableDictionary dictionary];
    
    GDataXMLElement * bodyElemnt = [[[doc rootElement] elementsForName:@"body"] lastObject];
   
    
    NSArray * subtitlesStreams = [bodyElemnt nodesForXPath:@"//smil/body/par/switch/textstream" error:&err];
    NSArray * videoStreams = [bodyElemnt nodesForXPath:@"//smil/body/par/switch/video" error:&err];
    
    //there will be only one !
    
    [videoStreams enumerateObjectsUsingBlock:^(GDataXMLElement * vidElement , NSUInteger idx, BOOL *stop) {
        
        GDataXMLNode *video = [vidElement attributeForName:@"src"];
        NSString *videoPath = [smilURL stringByDeletingLastPathComponent];
        videoPath = [self addMissingElemntsToString:videoPath];
        
       
        videoPath =[videoPath stringByAppendingString:@"/"];
        videoPath = [videoPath stringByAppendingString:[video stringValue]];
        
        //based on one video url per smil file, appending idx to the key is possible for multi video sources.
        [smilDictionary setObject:videoPath forKey:[NSString stringWithFormat:@"%@",VideoURL]];
        
    }];
    
    
    // there will be multiple
    
    NSMutableArray * languagesArray = [NSMutableArray array];
    
    [subtitlesStreams enumerateObjectsUsingBlock:^(GDataXMLElement * subElement , NSUInteger idx, BOOL *stop) {
    
            GDataXMLNode * srcElem = [subElement attributeForName:@"src"];
            GDataXMLNode * lngElem = [subElement attributeForName:@"systemLanguage"];
            
            NSString * sourceURL = [srcElem stringValue];
        
            NSString *subTitlePath = [smilURL stringByDeletingLastPathComponent];
            subTitlePath = [self addMissingElemntsToString:subTitlePath];
            subTitlePath =[subTitlePath stringByAppendingString:@"/"];
            subTitlePath = [subTitlePath stringByAppendingString:sourceURL];
            sourceURL = subTitlePath;
        
            NSString * language = [lngElem stringValue];
            
            Subtitle * subtitle = [Subtitle subtitleWithLanguage:language fileURL:[NSURL  URLWithString:sourceURL]];
            [languagesArray addObject:subtitle];
        
    }];
    
        
        if(languagesArray.count >0)
        {
              [smilDictionary setObject:languagesArray forKey:SUBTITLES_LANGUAGES_LIST];
        }
        
        
    return smilDictionary;
    
}

-(NSString *) addMissingElemntsToString :(NSString *) string
{
   
    
    NSRange range = [string rangeOfString:@"http://"];
    
    NSString * result = string ;
    
    if(range.location == NSNotFound)
    {
        result=  [string stringByReplacingOccurrencesOfString:@"http:/" withString:@"http://"];
    }
    
    return result;

}

@end
