//
//  FullyLoaded.m
//  FullyLoaded
//
//  Created by Anoop Ranganath on 1/1/11.
//  Copyright 2011 Anoop Ranganath. All rights reserved.
//
//  
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "AsyncImageLoader.h"
#import "SynthesizeSingleton.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"



NSString *const AsyncImageLoaderImageLoadedNotification = @"AsyncImageLoaderImageLoadedNotification";
NSString *const AsyncImageLoaderIsIdleNotification = @"AsyncImageLoaderIsIdleNotification";
NSString *const AsyncImageLoaderImageFailedNotification = @"AsyncImageLoaderImageFailedNotification";

NSString *const AsyncImageLoaderImageKey = @"Image";
NSString *const AsyncImageLoaderErrorKey = @"Error";
NSString *const AsyncImageLoaderURLStringKey = @"URLString";

@interface AsyncImageLoader()

@property (nonatomic, readwrite, strong) ASINetworkQueue *networkQueue;
@property (nonatomic, readwrite, strong) NSOperationQueue *responseQueue;
@property (nonatomic, readwrite, strong) NSTimer *queueSuspensionTimer;
@property (nonatomic, readwrite, strong) NSMutableDictionary *imageCache;
@property (nonatomic, readwrite, strong) NSString *imageCachePath;
@property (nonatomic, readwrite, strong) NSMutableSet *inProgressURLs;
@property (nonatomic, readwrite, strong) NSMutableArray *pendingURLs;
@property (nonatomic, strong) NSMutableDictionary *failedURLs;

-(void) sendImageLoadedNotificationForURL : (NSURL *) url withImage : (UIImage *) image;
-(void) sendImageFailedNotificationForURL : (NSURL *) url error : (NSError *) error;
@end

@implementation AsyncImageLoader

SYNTHESIZE_SINGLETON_FOR_CLASS(AsyncImageLoader);

@synthesize networkQueue = _networkQueue,
responseQueue = _responseQueue,
queueSuspensionTimer = _queueSuspensionTimer,
pendingURLs = _pendingURLs;
@synthesize failedURLs;

@synthesize imageCache = _imageCache,
imageCachePath = _imageCachePath,
inProgressURLs = _inProgressURLs;

#pragma mark - Memory

- (void)emptyCache 
{
//    ASLog(@"Emptying Cache. Removing %d photos", self.imageCache.count);
    [self.imageCache removeAllObjects];
}

-(void)emptyDiskCache
{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:self.imageCachePath error:nil];
    for (NSString *filename in fileArray)  {
        
        [fileMgr removeItemAtPath:[self.imageCachePath stringByAppendingPathComponent:filename] error:NULL];
    }
}

#pragma mark - Initializations
- (id)init 
{
    self = [super init];
    if (self) 
    {
        self.networkQueue = [[ASINetworkQueue alloc] init];
        self.networkQueue.delegate = self;
        self.networkQueue.requestDidFinishSelector = @selector(queuedRequestFinished:);
        self.networkQueue.requestDidFailSelector = @selector(queuedRequestFailed:);
        self.networkQueue.shouldCancelAllRequestsOnFailure = NO;
        [self.networkQueue go];
        
        self.responseQueue = [[NSOperationQueue alloc] init];
        
        self.pendingURLs = [[NSMutableArray alloc] init];
        self.inProgressURLs = [[NSMutableSet alloc] init];
        self.imageCache = [[NSMutableDictionary alloc] init];
        self.imageCachePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"Fully Loaded Cach"];
        
        self.failedURLs = [NSMutableDictionary dictionary];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resume) name:AsyncImageLoaderIsIdleNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emptyCache) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

#pragma mark - Queue Operations
- (void)resume 
{
    [self.networkQueue setSuspended:NO];
    [self.responseQueue setSuspended:NO];
    int pendingCount = (int)[self.pendingURLs count];
    for (int i=0; i< pendingCount; i++) 
    {
        [self enqueueURL:[self.pendingURLs lastObject]];
        [self.pendingURLs removeLastObject];
    }
}

- (void)suspend 
{
    [self.networkQueue setSuspended:YES];
    [self.responseQueue setSuspended:YES];
    [[NSNotificationQueue defaultQueue] enqueueNotification:[NSNotification notificationWithName:AsyncImageLoaderIsIdleNotification object:self ] 
                                               postingStyle:NSPostWhenIdle];	
}

- (void)enqueueURL:(NSURL *)url 
{
    if ([url scheme] != nil)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self directoryForImageURL:url] withIntermediateDirectories:YES attributes:nil error:nil];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        request.downloadDestinationPath = [self pathForImageURL:url];
        request.timeOutSeconds = 30;
        [self.networkQueue addOperation:request];
    }
}

#pragma mark - Public API

+(NSString *) imageLoadedNotificationNameForURL : (NSURL *) imageURL
{
    return (imageURL != nil) ? [NSString stringWithFormat:@"%@_%@",AsyncImageLoaderImageLoadedNotification,[imageURL absoluteString]] : nil;
}

+(NSString *) imageFailedNotificationNameForURL : (NSURL *) imageURL
{
    return (imageURL != nil) ? [NSString stringWithFormat:@"%@_%@",AsyncImageLoaderImageFailedNotification,[imageURL absoluteString]] : nil;
}

- (void) loadImageAtURL:(NSURL *)url 
{
    if ([url scheme] != nil || ([self.failedURLs objectForKey:[url absoluteString]] != nil)) 
    {
        NSString *keyForURL = [url absoluteString];
        
        // Check for in-RAM cache
        UIImage *image = [self.imageCache objectForKey:keyForURL];
        if (image == nil)
        {
            // Check for dist-chache
            image = [UIImage imageWithContentsOfFile:[self pathForImageURL:url]];
        }
        
        // Check for local file URL
        if ([url isFileURL])
        {
            if ([[NSFileManager defaultManager] isReadableFileAtPath:[url path]])
            {
                image = [UIImage imageWithContentsOfFile:[url path]];
            }
        }
        
        if (image != nil)
        {
            // Report Success
            [self sendImageLoadedNotificationForURL:url withImage:image];
        }
        else
        {
            if ([self.inProgressURLs containsObject:url] == NO)
            {
                [self.inProgressURLs addObject:url];
                if ([self.networkQueue isSuspended]) 
                {
                    [self.pendingURLs addObject:url];
                } 
                else 
                {
                    [self enqueueURL:url];
                }
            }
        }
    }
    else
    {
        // Report Failed
        [self sendImageFailedNotificationForURL:url error:nil];
    } 
}

#pragma mark - Utilities
- (NSString *) pathForImageURL:(NSURL *)aURL 
{
    NSString *targetPath = [self.imageCachePath stringByAppendingPathComponent:[aURL host]];
    targetPath = [targetPath stringByAppendingPathComponent:[aURL path]];
    NSString *params = [aURL query];
    if (params) 
    {
        targetPath = [targetPath stringByAppendingPathComponent:params];
    }
    return targetPath;
}

- (NSString *)directoryForImageURL:(NSURL *)aURL 
{
    return [[self pathForImageURL:aURL] stringByDeletingLastPathComponent];
}

- (void)cacheImageInDisk:(UIImage *)anImage forURL:(NSURL *)aURL
{
    @try
    {
        // If there is an older versioned cahed in RAM - remove it.
        [self.imageCache removeObjectForKey:[aURL absoluteString]];
        
        NSString *filePath = [self pathForImageURL:aURL];
        [[NSFileManager defaultManager] createDirectoryAtPath:[self directoryForImageURL:aURL] withIntermediateDirectories:YES attributes:nil error:nil];
        [UIImageJPEGRepresentation(anImage, 1) writeToFile:filePath atomically:YES];
    }
    @catch (NSException *exception) 
    {
        //ASLog@"%@",[exception debugDescription]);
    }
}

-(void) cacheInRAMImage : (UIImage *) image forURLString : (NSString *) URLString
{
    if (URLString.length > 0)
    {
        if (image.size.width * image.size.height < 40000)
        {
            [self.imageCache setObject:image forKey:URLString];
        }
    }
}

#pragma mark - NSOperationQueue Delegate
- (void)queuedRequestFinished:(ASIHTTPRequest *)request 
{
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(reportImageLoadedOnMainThread:) object:request];
    [self.responseQueue addOperation:operation];
}

- (void)queuedRequestFailed:(ASIHTTPRequest *)request 
{
    if (request.responseStatusCode == 404)
    {
        // If the image does not exist there is no point to request it over and over again.
        NSString *URLString = [[request originalURL] absoluteString];
        //ASLog@"Image at URL: %@ Failed with 404.",URLString);
        [self.failedURLs setObject:URLString forKey:URLString];
    }
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(reportFailedOnMainThread:) object:request];
    [self.responseQueue addOperation:operation];
}

#pragma mark - Notifying Succsess/Failure
-(void) reportFailedOnMainThread : (ASIHTTPRequest *) request
{
    [self performSelectorOnMainThread:@selector(reportFailed:) withObject:request waitUntilDone:YES];
}

-(void) reportFailed : (ASIHTTPRequest *) request
{
    NSURL *originalURL = request.originalURL;
    if (originalURL != nil)
    {
        [self.inProgressURLs removeObject:request.originalURL];
        [self sendImageFailedNotificationForURL:request.originalURL error:request.error];
    }
}

- (void)reportImageLoadedOnMainThread:(ASIHTTPRequest *)request 
{
    [self performSelectorOnMainThread:@selector(loadImage:) withObject:request waitUntilDone:YES];
}

-(void) sendImageLoadedNotificationForURL : (NSURL *) url withImage : (UIImage *) image
{
    if (url != nil)
    {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[url absoluteString],
                                  AsyncImageLoaderURLStringKey,
                                  image,
                                  AsyncImageLoaderImageKey,
                                  nil];
        
        NSString *notificationName = [AsyncImageLoader imageLoadedNotificationNameForURL:url];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:self userInfo:userInfo];
    }
}

-(void) sendImageFailedNotificationForURL : (NSURL *) url error : (NSError *) error
{
    NSDictionary *userInfo = (error != nil) ? [NSDictionary dictionaryWithObject:error forKey:AsyncImageLoaderErrorKey] : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:[AsyncImageLoader imageFailedNotificationNameForURL:url] 
                                                        object:self 
                                                      userInfo:userInfo];
}

- (void)loadImage:(ASIHTTPRequest *)request 
{
    NSURL *originalURL = request.originalURL;
    NSString *urlString = [originalURL absoluteString];
    if (urlString)
    {
        [self.inProgressURLs removeObject:request.originalURL];
        
        NSString *path = [self pathForImageURL:originalURL];
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        if (image) 
        {
            [self cacheInRAMImage:image forURLString:urlString];
            [self sendImageLoadedNotificationForURL:request.originalURL withImage:image];
        } 
        else
        {
            [self.failedURLs setObject:urlString forKey:urlString];
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
            [self reportFailedOnMainThread:request];
        }
    }
}


@end
