//
//  RatingControl.m
//  Sherut
//
//  Created by Gilad Novik on 12-01-30.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RatingControl.h"
#import "UIImage+Resize.h"
#import "SmartLogs.h"

#define ON_IMAGE_DEFAULT_NAME @"stars_on"
#define OFF_IMAGE_DEFAULT_NAME @"stars_off"

@interface RatingControl ()
@end

@implementation RatingControl
@synthesize rating = _rating;
@synthesize onImageView = _onImageView;
@synthesize offImageView = _offImageView;
@synthesize onImageName = _onImageViewName;
@synthesize offImageName = _offImageViewName;
@synthesize starsPositions = _starsPositions;

-(void) dealloc
{
    
    self.offImageName = nil;
    self.onImageName = nil;
}

#pragma mark - Setters

-(void) setOnImageName:(NSString *)onImageName
{
    if (_onImageViewName != onImageName)
    {
        _onImageViewName = [onImageName copy];
        if (_onImageViewName != nil)
        {
            [self loadImages];
        }
    }
}

-(void) setOffImageName:(NSString *)offImageName
{
    if (_offImageViewName != offImageName)
    {
        _offImageViewName = [offImageName copy];
        if (_offImageViewName != nil)
        {
            [self loadImages];
        }
    }
}

#pragma mark - Getters

-(UIImageView *) onImageView
{
    if (_onImageView == nil)
    {
        _onImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _onImageView.contentMode = UIViewContentModeLeft;
        _onImageView.clipsToBounds = YES;
    }
    return _onImageView;
}

-(UIImageView *) offImageView
{
    if (_offImageView == nil)
    {
        _offImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _offImageView.contentMode = UIViewContentModeLeft;
        _offImageView.clipsToBounds = YES;
    }
    return _offImageView;
}

#pragma mark - Setup

-(void) setupStarsPositions:(NSArray *)starsPoss{
    self.starsPositions = [NSArray arrayWithArray:starsPoss];
}

-(void)loadImages
{
	UIImage* on=[UIImage imageNamed:self.onImageName];
	UIImage* off=[UIImage imageNamed:self.offImageName];
	
	if (self.bounds.size.width < on.size.width || self.bounds.size.height < on.size.height)
	{
		
		on = [on resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:self.bounds.size interpolationQuality:1.0];
        off = [off resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:self.bounds.size interpolationQuality:1.0];
	}
    
    TVLogDebug(@"%@ %@",self.onImageName, self.offImageName);////////
    self.onImageView.image = on;
    self.offImageView.image = off;
}

-(void) setup
{
    self.backgroundColor = [UIColor clearColor];
    self.onImageName = ON_IMAGE_DEFAULT_NAME;
    self.offImageName = OFF_IMAGE_DEFAULT_NAME;
    
    [self addSubview:self.offImageView];
	[self.offImageView addSubview:self.onImageView];
    self.onImageView.frame = CGRectMake(0, 0, 0, self.onImageView.frame.size.height);
    self.rating = 0.2;
}

-(id)initWithFrame:(CGRect)frame
{
	if ((self=[super initWithFrame:frame])!=nil)
	{
		[self setup];
	}
	return self;
}

-(void)awakeFromNib
{
	[super awakeFromNib];
	[self setup];
}

#pragma mark - API

-(void)setRating:(CGFloat)rating
{
    
    TVLogDebug(@" self.offImageView.bounds.size.width = %f \n self.offImageView.bounds.size.width =%f \n self.onImageView.bounds.size.width = %f\n self.onImageView.frame.size.width  = %f",self.offImageView.bounds.size.width, self.offImageView.frame.size.width, self.onImageView.bounds.size.width,self.onImageView.frame.size.width );////////
    
	if (_rating != rating)
    {
        _rating = rating;
        CGRect frame = self.onImageView.frame;
        if(_starsPositions){
            int ind = rating *5.0;
            ind = (ind)*2;
            if(ind==10){
                ind=8; // handle rating = 1;
            }
            float bottom = [[_starsPositions objectAtIndex:ind] floatValue];
            float top = [[_starsPositions objectAtIndex:ind+1] floatValue];
            float ratingFactor = rating;
            float starPartRate  = (ratingFactor - ind*0.1)/0.2;
            float starPartWidth = (top-bottom);
            float startPartRatingWidth = starPartRate*starPartWidth;
            
            frame.size.width = bottom + startPartRatingWidth;
        }
        else{
            
            frame.size.width = ceilf(self.offImageView.bounds.size.width * rating);
            
        }
        self.onImageView.frame = frame;
    }
    TVLogDebug(@"self.offImageView.bounds.size.width = %f \n self.offImageView.bounds.size.width =%f \n self.onImageView.bounds.size.width = %f\n self.onImageView.frame.size.width  = %f",self.offImageView.bounds.size.width, self.offImageView.frame.size.width, self.onImageView.bounds.size.width,self.onImageView.frame.size.width );////////

}

#pragma mark - Touch Handling

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	return [self continueTrackingWithTouch:touch withEvent:event];
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGFloat rating = [touch locationInView:self].x / self.bounds.size.width;
	rating = floorf(rating * 5.0f + 1.0f) / 5.0f;
    rating = MAX(rating, 0.2);
	if (rating != _rating)
	{
        self.rating = rating;
        // Sending actions is only in response of user interaction, not programmatic changes.
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}
	return YES;
}

@end
