//
//  RatingControl.h
//  Sherut
//
//  Created by Gilad Novik on 12-01-30.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingControl : UIControl

@property (nonatomic, copy) NSString *onImageName;
@property (nonatomic, copy) NSString *offImageName;

@property (nonatomic, strong) UIImageView *onImageView;
@property (nonatomic, strong) UIImageView *offImageView;
@property(nonatomic,assign) CGFloat rating;	// In the range [0,1]
@property (nonatomic, strong) NSArray *starsPositions;

-(void) setupStarsPositions:(NSArray *)starsPoss;
@end
