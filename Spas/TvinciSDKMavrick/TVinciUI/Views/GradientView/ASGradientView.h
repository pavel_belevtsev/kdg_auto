//
//  ASGradientView.h
//  SDE Group Diamonds
//
//  Created by Avraham Shukron on 10/27/11.
//  Copyright (c) 2011 appSTUDIO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface ASGradientView : UIView
@property (nonatomic , readonly) CAGradientLayer *gradientLayer;
@end
