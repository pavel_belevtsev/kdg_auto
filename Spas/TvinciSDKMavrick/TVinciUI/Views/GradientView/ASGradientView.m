//
//  ASGradientView.m
//  SDE Group Diamonds
//
//  Created by Avraham Shukron on 10/27/11.
//  Copyright (c) 2011 appSTUDIO. All rights reserved.
//

#import "ASGradientView.h"
#import <QuartzCore/QuartzCore.h>
@implementation ASGradientView
// Make the view's layer a CAGradientLayer instance
+ (Class)layerClass 
{
    return [CAGradientLayer class];
}

-(CAGradientLayer *) gradientLayer
{
    return (CAGradientLayer *)self.layer;
}

-(void) setup
{
    self.backgroundColor = [UIColor clearColor];
}

-(void) awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}
@end
