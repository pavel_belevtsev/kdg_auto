//
//  TVLinearLayout.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 7/12/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVLinearLayout.h"
#import "SmartLogs.h"

@implementation TVLinearLayout
@synthesize margins;
@synthesize orientation;

#pragma mark - Setups
-(void) setup
{
    self.margins = 0;
    self.orientation = TVLayoutOrientationHorizontal;
}

-(id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self setup];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

-(void) layoutHorizontally
{
    CGFloat xCursor = 0;
    for (int i = 0; i < self.subviews.count; i++)
    {
        UIView *subview = [self.subviews objectAtIndex:i];
        if (subview.hidden == NO && subview.alpha > 0)
        {
            // TVLogDebug(@"[LinearLayout] %@",subview);////////
            CGRect frame = subview.frame;
            frame.origin.x = xCursor;
            frame.origin.y = (self.bounds.size.height - frame.size.height) / 2;
            subview.frame = frame;
            xCursor += (subview.bounds.size.width + self.margins);
        }
    }
}

-(void) layoutRTLHorizontally
{
    CGFloat xCursor = self.frame.size.width;
    for (int i = 0; i < self.subviews.count; i++)
    {
        UIView *subview = [self.subviews objectAtIndex:i];
        if (subview.hidden == NO && subview.alpha > 0)
        {
            xCursor -= subview.bounds.size.width ;
            CGRect frame = subview.frame;
            frame.origin.x = xCursor;
            frame.origin.y = (self.bounds.size.height - frame.size.height) / 2;
            subview.frame = frame;
            xCursor -= self.margins;
        }
    }
}
-(void) layoutVertically
{
    CGFloat yCursor = 0;
    for (int i = 0; i < self.subviews.count; i++)
    {
        UIView *subview = [self.subviews objectAtIndex:i];
        if (subview.hidden == NO && subview.alpha > 0)
        {
            CGRect frame = subview.frame;
            frame.origin.y = yCursor;
            frame.origin.x = 0;
            subview.frame = frame;
            yCursor += (subview.bounds.size.height + self.margins);
        }
    }
}

-(void) layoutSubviews
{
    switch (self.orientation)
    {
        case TVLayoutOrientationHorizontal:
            [self layoutHorizontally];
            break;
        case TVLayoutOrientationVertical:
            [self layoutVertically];
            break;
        case TVLayoutOrientationRTLHorizontally:
            [self layoutRTLHorizontally];
            break;
        default:
            [self layoutHorizontally];
            break;
    }
  //  (self.orientation == TVLayoutOrientationHorizontal) ? [self layoutHorizontally] : [self layoutVertically];
}

@end
