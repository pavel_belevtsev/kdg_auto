//
//  ModalViewControllerDelegate.h
//  BrunoGuez
//
//  Created by Avraham Shukron on 2/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModalViewControllerDelegate <NSObject>
@optional
-(void) dismissModalViewController : (UIViewController *) modalViewController animated : (BOOL) animated;
@end
