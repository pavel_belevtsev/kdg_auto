//
//  MainMenuViewControllerDelegate.h
//  Toggle
//
//  Created by Avraham Shukron on 7/24/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TVMenuItem;

@protocol MainMenuViewControllerDelegate <NSObject>
-(void) menuViewController : (id) sender didSelectMenuItem : (TVMenuItem *) item;
-(void) menuViewControllerDidSelectLogOut:(id)sender;
-(void) menuViewControllerDidSelectLogIn:(id)sender;
-(void) menuViewControllerDidSelectSettings:(id)sender;


@end
