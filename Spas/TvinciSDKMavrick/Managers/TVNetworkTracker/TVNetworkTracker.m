//
//  TVNetworkStatusManager.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 8/7/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVNetworkTracker.h"
#import "ReachabilityOriginal.h"

@implementation TVNetworkTracker



static TVNetworkTracker * sharedInstance = nil;

+ (TVNetworkTracker *) sharedNetworkStatusManager
{
    if ( sharedInstance == nil)
    {
        @synchronized(self)
        {
            sharedInstance = [[TVNetworkTracker alloc] init];
        }
    }
    
    return sharedInstance;
   
}


-(id) init
{
    if (self = [super init])
    {
        [self setup];
    }
    
    
    return self;
}

-(void) setup
{    // for set initial status
    [self updateStatus];
}

-(void)dealloc
{
    [self unsubscribeFromNetworkReachabilityNotifications];
    
}


- (void)registerForNetworkReachabilityNotifications
{
    [self unsubscribeFromNetworkReachabilityNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
	[[ReachabilityOriginal reachabilityForInternetConnection] startNotifier];
}


- (void)unsubscribeFromNetworkReachabilityNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[ReachabilityOriginal reachabilityForInternetConnection] stopNotifier];
}

-(void) reachabilityChanged:(NSNotification *) notification
{
    [self updateStatus];
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_NetworkStatusChanged
                                                        object:nil userInfo:nil];
}

-(void) updateStatus
{
    
    switch ([[ReachabilityOriginal reachabilityForInternetConnection] currentReachabilityStatus])
    {
        case  NotReachable:
        {
            self.status = TVNetworkStatus_NoConnection;
        }
            break;
        case ReachableViaWWAN:
        {
            self.status = TVNetworkStatus_3G;
        }
            break;
        case ReachableViaWiFi:
        {
            self.status = TVNetworkStatus_WiFi;
        }
            break;
        default:
            break;
    }

}


-(void) startTracking
{
    [self registerForNetworkReachabilityNotifications];
}

-(void) stopTracking
{
    [self unsubscribeFromNetworkReachabilityNotifications];
}




@end
