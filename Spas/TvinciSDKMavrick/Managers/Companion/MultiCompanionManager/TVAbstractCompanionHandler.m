//
//  TVNAbstractCompanionHandler.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVAbstractCompanionHandler.h"

@implementation TVAbstractCompanionHandler


- (int) getType
{
    return -1;
}

- (void) stopScanning
{
    
}

- (void) scan
{
    
}

- (void) sendVodMedia: (TVMediaItem*)media toDevice: (TVAbstractCompanionDevice*)device withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result
{
    
}

- (void) sendLiveMedia: (TVMediaItem*)media toDevice:(TVAbstractCompanionDevice*)device withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result
{
    
}

- (void) connect: (TVAbstractCompanionDevice*)device
{
    
}

- (void) disconnect
{
    
}

- (void) sendMessage: (NSString*)message withExtras: (NSDictionary*)extras toDevice:(TVAbstractCompanionDevice*)device withCompletion:(void(^)(bool success,NSDictionary* extras)) completion
{
    
}

@end
