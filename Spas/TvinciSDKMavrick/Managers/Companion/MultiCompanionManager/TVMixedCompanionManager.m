//
//  TVNMixedCompanionManager.m
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import "TVMixedCompanionManager.h"
#import "TVEPGProgram.h"


@interface TVMixedCompanionManager ()
@property NSMutableArray* handlers;
@property NSMutableArray* devices;
@property TVAbstractCompanionDevice* connectedDevice;
@end


@implementation TVMixedCompanionManager

NSString* const CompanionMessageGetVolume = @"getvolume";
NSString* const CompanionMessageSetVolume = @"setvolume";
NSString* const CompanionMessageShowUI = @"SHOW_UI";
NSString* const CompanionMessageHideUI = @"HIDE_UI";
NSString* const CompanionMessageVolumeUp = @"vup";
NSString* const CompanionMessageVolumeDown = @"vdown";


@synthesize handlers = _handlers;
@synthesize devices = _devices;
@synthesize connectedDevice = _connectedDevice;


+ (TVMixedCompanionManager*)sharedInstance {
    
    static dispatch_once_t p = 0;
    
    __strong static id sharedObject = nil;
    
    dispatch_once(&p, ^{
        sharedObject = [[self alloc] init];
    });
    
    return sharedObject;
}

- (id)init {
    if (self = [super init]) {
        _devices = [[NSMutableArray alloc] init];
        _handlers = [[NSMutableArray alloc] init];
        
    }
    return self;
}

-(void) scan {
    for (TVAbstractCompanionHandler *o in _handlers) {
        [o scan];
    }
}

- (void) addHandler: (TVAbstractCompanionHandler*) handler {
    [handler setDelegate:self];
    [_handlers addObject:handler];
}

- (void)onDiscovery:(NSArray *)devices {
    bool changed = NO;
    for (TVAbstractCompanionDevice *device in devices) {
        if (![_devices containsObject:device]) {
            [_devices addObject:device];
            changed = YES;
        }
    }
//    if (changed) {
        //notify callbacks
        [self notifyNotificationCenter:TVCompanionMixedNotificationDeviceDiscovery];
//    }
}

- (NSArray *)getDiscoveredDevices {
    return [NSArray arrayWithArray:_devices];
}


- (void) notifyNotificationCenter: (NSString*)what {
    [[NSNotificationCenter defaultCenter] postNotificationName:what object:nil userInfo:nil];
}


-(void)connect:(TVAbstractCompanionDevice *)device {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:device];
    if (handler) {
        [handler connect:device];
        [self setConnectedDevice:device];
        [self notifyNotificationCenter:TVCompanionMixedNotificationDeviceConnected];
    }
}

-(void)disconnect {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:_connectedDevice];
    if (handler) {
        [handler disconnect];
        [self setConnectedDevice:nil];
        [self notifyNotificationCenter:TVCompanionMixedNotificationDeviceDisconnected];
    }
}

- (TVAbstractCompanionHandler*) handlerForDevice:(TVAbstractCompanionDevice*)device {
    TVAbstractCompanionHandler* retVal = nil;
    
    if (device) {
        for (TVAbstractCompanionHandler *handler in _handlers) {
            if ([handler getType] == [device getType]) {
                retVal = handler;
                break;
            }
        }
    }
    return retVal;
}

- (void) sendToConnectedDeviceVodMedia: (TVMediaItem*)media withFile: (TVFile*)file currentProgress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:_connectedDevice];
    if (handler) {
        [handler sendVodMedia:media toDevice:_connectedDevice withFile:file currentProgress:progress withCompletionBlock:result];
    }
}

- (void) sendToConnectedDeviceLiveMedia: (TVMediaItem*)media withFile: (TVFile*)file andProgram: (TVEPGProgram*)program forPlaybackMode: (NSString*)playbackType progress: (int)progress withCompletionBlock:(void (^)(CompanionHandlerResult result))result {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:_connectedDevice];
    if (handler) {
        [handler sendLiveMedia:media toDevice:_connectedDevice withFile:file andProgram:program forPlaybackMode:playbackType progress:progress withCompletionBlock:result];
    }
}

- (void) sendEmptyMessageToCurrentDevice:(NSString*) message withCompletion:(void (^)(bool, NSDictionary *))completion {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:_connectedDevice];
    if (handler) {
        [handler sendMessage:message withExtras:nil toDevice:_connectedDevice withCompletion:completion];
    }
}

- (void) sendMessageToCurrentDevice:(NSString*) message withExtras:(NSDictionary*) extras withCompletion:(void (^)(bool, NSDictionary *))completion {
    TVAbstractCompanionHandler* handler = [self handlerForDevice:_connectedDevice];
    if (handler) {
        [handler sendMessage:message withExtras:extras toDevice:_connectedDevice withCompletion:completion];
    }
}


- (BOOL) isConnected {
    return _connectedDevice != nil;
}

- (TVAbstractCompanionDevice *)getCurrentConnectedDevice {
    return _connectedDevice;
}




@end
