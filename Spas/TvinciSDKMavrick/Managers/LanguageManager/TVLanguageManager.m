//
//  YESLanguageManager.m
//  YES_iPhone
//
//  Created by Rivka S. Peleg on 7/29/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "TVLanguageManager.h"

#define k_baseFileName @"LanguageDictionaryOfLanguage"
#define k_TextKey @"textKey"
#define k_blockKey @"block"

#import "TVConfigurationManager.h"
#import "TVPAPIRequest.h"
#import "TVPMediaAPI.h"

#define languageChangedNotificationName @"languageChangedNotificationName"


@class TVNetworkQueue;

NSString * stringForLanguageType(LanguageType languageType)
{
    static NSString *const languageTypeArray[] = {@"",@"en",@"he",@"fr",@"ru",@"de"};
    NSString * text = nil;
    NSInteger length = (sizeof(languageTypeArray)/sizeof(languageTypeArray[0]));
    if (languageType < length)
    {
        text = languageTypeArray[languageType];
    }
    return text;
}

NSString * stringOfTextKey(TextKey textKey)
{
    static NSString *const textKeysArray[] = {@"wrong user name & password",@"user not allowed",@"First"};
    NSString * text = nil;
    NSInteger length = (sizeof(textKeysArray)/sizeof(textKeysArray[0]));
    if (textKey < length)
    {
        text = textKeysArray[textKey];
    }
    return text;
}


@interface TVLanguageManager ()

@property (strong,nonatomic) TVNetworkQueue * networkQueue;

@property (assign,readwrite, nonatomic) LanguageManagerStatus status;
@property (assign, readwrite, nonatomic) LanguageType currentLanguage;
@property (strong, nonatomic) NSMutableArray * arrayBlocksWithTextKey;

-(void) downloadLanguageDictionaryForLanguage: (LanguageType) language;
-(void) loadFromCachLanguageDictionaryForLanguage:(LanguageType) language;
-(void) saveTranslationDictionaryForLanguage:(LanguageType) language;
-(void) operateAllQueuedBlockes;

-(void) registerForConfigirationNotification;
-(void) unregisterForConfigirationNotification;

@property (strong, nonatomic) NSDictionary * dictionaryTextInterpetationByKey;
@end

@implementation TVLanguageManager


static TVLanguageManager *sharedInstace = nil;

+ (TVLanguageManager *)sharedLanguageManager
{
    @synchronized(self)
    {
        if(sharedInstace == nil)
        {
            sharedInstace = [[TVLanguageManager alloc] init];
        }
    }
    
    return sharedInstace;
}

-(id)init
{
    if(self =[super init])
    {
        self.status = languageManagerStatus_Not_Initialized;
        self.arrayBlocksWithTextKey = [NSMutableArray array];
    }
    
    return self;
}

-(void) setupLanguage:(LanguageType) language
{
    self.currentLanguage = language;
    
      [[NSNotificationCenter defaultCenter] postNotificationName:languageChangedNotificationName object:nil];
    
    if ([[TVConfigurationManager sharedTVConfigurationManager] state] != TVConfigurationStateProperlyConfigured)
    {
        [self registerForConfigirationNotification];
    }
    else
    {
        [self downloadLanguageDictionaryForLanguage:self.currentLanguage];
    }
    
}

-(void) registerForConfigirationNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configurationReady:) name:TVConfigurationManagerDidLoadConfigurationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configurationFailed:) name:TVConfigurationManagerDidFailToLoadConfigurationNotification object:nil];
}

-(void) configurationReady:(NSNotification *) notification
{
    [self downloadLanguageDictionaryForLanguage:self.currentLanguage];
}

-(void) configurationFailed:(NSNotification *) notification
{
    
}

-(void) unregisterForConfigirationNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVConfigurationManagerDidLoadConfigurationNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVConfigurationManagerDidFailToLoadConfigurationNotification object:nil];
    
}



-(void) downloadLanguageDictionaryForLanguage: (LanguageType) language
{
   TVPAPIRequest * request = [TVPMediaAPI  requestForGetTranslations:nil];
    __weak TVPAPIRequest * weakRequest = request;

    [request setFailedBlock:
     ^{
        self.status = languageManagerStatus_Not_Initialized;
    }];
    
    [request setStartedBlock:^{
        
        self.status = languageManagerStatus_Initializing;
        
    }];
    
    [request setCompletionBlock:
     ^{
         self.status = languageManagerStatus_Initialized;

         NSDictionary * translationDictionary = [[weakRequest JSONResponse] lastObject];
         self.dictionaryTextInterpetationByKey = translationDictionary;
         
         [self operateAllQueuedBlockes];
        
    }];
    
 
    
    [self sendRequest:request];
}

-(void) saveTranslationDictionaryForLanguage:(LanguageType) language
{
   // not implement yet 
}
-(void) loadFromCachLanguageDictionaryForLanguage:(LanguageType) language
{
    // not implement yet
}

-(void) operateAllQueuedBlockes
{
    for ( NSDictionary * dict in self.arrayBlocksWithTextKey )
    {
        void (^ operateBlock)(NSString *) = [dict objectForKey:k_blockKey];
        TextKey textKey = (TextKey)((NSNumber *)[dict objectForKey:k_TextKey]).integerValue;
        if (operateBlock)
        {
            NSString * textForTextKey = [self getTextForKey:textKey];
            operateBlock(textForTextKey);
        }
        
    }
    
    [self.arrayBlocksWithTextKey removeAllObjects];
    
}



-(NSString *) getTextForKey:(TextKey) textKey
{
    NSString * interpetation = nil;
    
    if (self.dictionaryTextInterpetationByKey != nil)
    {
        NSString * textForKey = stringOfTextKey(textKey);
        interpetation = self.dictionaryTextInterpetationByKey[textForKey];
    }
    
    return interpetation;
    
}

-(void) operateTextForKey: (TextKey) textKey OnBlock:(void (^)(NSString *)) block
{
    if (self.status != languageManagerStatus_Initialized)
    {
        [self.arrayBlocksWithTextKey addObject:
        @{
            k_TextKey:[NSNumber numberWithInt:textKey],
            k_blockKey : block
        }];
    }
    else
    {
        NSString * text = [self getTextForKey:textKey];
        if (block)
        {
            block(text);
        }
    }
}

-(BOOL) isTextDerectionRightToLeft
{
    return  self.textDirection == TextDirection_RightToLeft;
}

-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

-(void)dealloc
{
    [_networkQueue cleen];
}







@end
