//
//  TVRefreshTask.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/16/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVDataRefreshTask.h"

@implementation TVDataRefreshTask


+(instancetype) dataRefreshTaskWithRefreshTimePeriod:(NSTimeInterval) timeInterval
                                        refreshBlock: (void(^)( void(^completionBlock)(TVDataRefreshTaskResult) )) refreshBlock
                                     nextRefreshDate:(NSDate *)nextRefreshDate
                                              target:(id) target
{
    TVDataRefreshTask * refreshTask = [[TVDataRefreshTask alloc] init];
    refreshTask.refreshTaskBlock = refreshBlock;
    refreshTask.refreshTimePeriod = timeInterval;
    refreshTask.nextRefreshDate = nextRefreshDate;
    refreshTask.target = target;
    return refreshTask;
    
}

@end
