//
//  DataRefresh.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/16/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVDataRefreshHandler.h"
#import "TVSDKSettings.h"

@interface TVDataRefreshHandler ()

@property (strong, nonatomic, readwrite) NSMutableArray * arrayRefreshTasksMutable;

@end
@implementation TVDataRefreshHandler


+ (instancetype) sharedInstance
{
    static dispatch_once_t onceToken;
    static id sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.arrayRefreshTasksMutable = [[NSMutableArray alloc] init];
    }
    return self;
}


//NSDate *fetchStart = [NSDate date];
//NSDate *fetchEnd = [NSDate date];
//NSTimeInterval timeElapsed = [fetchEnd timeIntervalSinceDate:fetchStart];
//NSLog(@"Background Fetch Duration: %f seconds", timeElapsed);


-(void) executeRefreshTasksWithStartBlock:(void(^)(void)) startBlock compeltionBlock:(void(^)(TVDataRefreshTaskResult result)) completionBlock
{
    
    if (startBlock)
    {
        startBlock();
    }


    if ([[TVSDKSettings sharedInstance] offlineMode])
    {
        if (completionBlock)
        {
            completionBlock(TVDataRefreshTaskResultNotRefreshed);
        }
        return;
    }
    
    NSDate * now = [NSDate date];
    dispatch_group_t group = dispatch_group_create();

    int numberOfTasksDispatched = 0;
    
    // delete all tasks that their target is nil ( freed already )
    NSMutableIndexSet * toDeleteIndexes = [[NSMutableIndexSet alloc] init];
    __block TVDataRefreshTaskResult finalResult = TVDataRefreshTaskResultNotRefreshed;
    
    for (TVDataRefreshTask * task in self.arrayRefreshTasksMutable)
    {
        // if the next refresh date is in the past , excute the refresh task and update the next refresh date
        if ([task.nextRefreshDate compare:now] == NSOrderedAscending)
        {
            
            if (task.target != nil)
            {
                task.nextRefreshDate = [now dateByAddingTimeInterval:task.refreshTimePeriod];
                
                dispatch_group_enter(group);
                numberOfTasksDispatched++;
                task.refreshTaskBlock(^(TVDataRefreshTaskResult result)
                                      {
                                          if (result == TVDataRefreshTaskResultRefreshed)
                                          {
                                              finalResult = TVDataRefreshTaskResultRefreshed;
                                          }
                                          else if (result == TVDataRefreshTaskResultFailed)
                                          {
                                              if (finalResult != TVDataRefreshTaskResultRefreshed)
                                              {
                                                  finalResult = TVDataRefreshTaskResultFailed;
                                              }
                                          }
                                          
                                          dispatch_group_leave(group);
                                      });
            }
            else
            {
                [toDeleteIndexes addIndex:[self.arrayRefreshTasksMutable indexOfObject:task]];
            }
        }
    }
    
    if (completionBlock)
    {
        if (numberOfTasksDispatched >0)
        {
            dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                {
                    completionBlock(finalResult);
                }
            });
        }
        else{completionBlock(finalResult);}
    }
    
    if (toDeleteIndexes != nil)
    {
        [self.arrayRefreshTasksMutable removeObjectsAtIndexes:toDeleteIndexes];
    }
    
}



-(void) addDataRefreshTaskWithRefreshPeriod:(NSTimeInterval) refreshTimePeriod offset:(NSTimeInterval) offset target:(id)target withRefreshBlock:(void(^)( void(^)(TVDataRefreshTaskResult)  )) refreshBlock
{
    NSDate * nextRefreshDate = [[NSDate date] dateByAddingTimeInterval:offset];
    TVDataRefreshTask * task = [TVDataRefreshTask dataRefreshTaskWithRefreshTimePeriod:refreshTimePeriod refreshBlock:refreshBlock nextRefreshDate:nextRefreshDate target:target];
    [self.arrayRefreshTasksMutable addObject:task];
}

-(void) removeAllTasksForTarget:(id) target
{
    
    NSMutableIndexSet * toDeleteIndexes = [[NSMutableIndexSet alloc] init];
    for ( int i =0 ; i<self.arrayRefreshTasksMutable.count ; i++)
    {
        TVDataRefreshTask * task = self.arrayRefreshTasksMutable[i];
        if (task.target == target)
        {
            [toDeleteIndexes addIndex:i];
        }
    }
    
    [self.arrayRefreshTasksMutable removeObjectsAtIndexes:toDeleteIndexes];
}




@end
