//
//  AnalyticsManager.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/25/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVAnalyticsManager.h"

@interface TVAnalyticsManager ()

@property (strong, nonatomic) NSMutableArray * arrayAnalyticsProviders;

@end

@implementation TVAnalyticsManager


-(void) addProvider:(TVAnalyticsProvider*) provider
{
    [self.arrayAnalyticsProviders addObject:provider];
}

-(void) removeProvider:(TVAnalyticsProvider*) provider
{
    [self.arrayAnalyticsProviders removeObject:provider];
}

-(void) sendEvent:(TVAnalyticsEvent *) event
{
    
    for (TVAnalyticsProvider * provider in self.arrayAnalyticsProviders )
    {
        [provider sendEvent:event];
    }
    
}

+ (TVAnalyticsManager *)sharedAnalyticsManager {
    static TVAnalyticsManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[TVAnalyticsManager alloc] init];
    });
    
    return _sharedInstance;
}


-(NSArray *)arrayAnalyticsProviders
{
    if (_arrayAnalyticsProviders == nil)
    {
        _arrayAnalyticsProviders = [NSMutableArray array];
    }
    
    return _arrayAnalyticsProviders;
}
@end
