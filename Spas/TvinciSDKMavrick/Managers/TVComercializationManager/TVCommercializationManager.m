//
//  TVComercializationManager.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVCommercializationManager.h"
#import "TVPConditionalAccessAPI.h"
#import "TVSessionManager.h"
#import "TVDomainServicesResponse.h"
#import "TVService.h"
#import "NSString+FilePath.h"
#import "NSArray+Equivalent.h"




#define domainSevicesFileName @"domainSevices"
@interface TVCommercializationManager ()

@property (strong, nonatomic) NSArray * arrayOfServices;
@end

@implementation TVCommercializationManager

+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}



#pragma mark - API
-(void) startTrakingForServicesWithCompletion:(void(^)()) completionBlock
{
    [self registerFordNotification];
    [self refreshDomainServicesWithCompletion:completionBlock];

}

-(BOOL) isServiceSupported:(TVCommercializationServiceType)  serviceType
{
    for (TVService * service in self.arrayOfServices)
    {
        if (service.serviceID == serviceType)
        {
            return YES;
        }
    }

    return NO;
}


#pragma mark - setters
- (void) setArrayOfServices:(NSArray *)arrayOfServices
{
    if (![_arrayOfServices isEquivalent:arrayOfServices])
    {
        _arrayOfServices = arrayOfServices;
        [self saveDomainSevices];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationNameCommercializationStatusChanged object:nil];
    }
}


#pragma mark - notification

-(void) registerFordNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshDomainServicesState:)
                                                 name:TVSessionManagerSignInCompletedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshDomainServicesState:) name:TVSessionManagerLogoutCompletedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDomainServicesState:) name:UIApplicationDidBecomeActiveNotification object:nil];


}

-(void) unregisterFordNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) refreshDomainServicesState:(NSNotification *) notficiation
{
    [self refreshDomainServicesWithCompletion:nil];
}


#pragma mark - refresh Status
-(void) refreshDomainServicesWithCompletion:(void(^)()) completionBlock
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if ([[TVSDKSettings sharedInstance] offlineMode])
        {
            self.arrayOfServices = [self loadDomainSevices];
            if (completionBlock){completionBlock();}
        }
        else
        {
            NSString * domainID = [NSString stringWithFormat:@"%ld",(long)[[[TVSessionManager sharedTVSessionManager] sharedInitObject] domainID]];

           TVPAPIRequest * request = [TVPConditionalAccessAPI requestForGetDomainServicesWithDomainID:domainID delegate:nil];
            __weak TVPAPIRequest * weakRequest = request;

            [request setFailedBlock:^{

                self.arrayOfServices = [self loadDomainSevices];
                if (completionBlock){completionBlock();}
            }];

            [request setCompletionBlock:^{

                NSDictionary * dictionary = [weakRequest JSONResponse];
                TVDomainServicesResponse * response = [[TVDomainServicesResponse alloc] initWithDictionary:dictionary];
                self.arrayOfServices =  response.arrayServices;
                if (completionBlock){completionBlock();}

            }];
            [self sendRequest:request];
        }
    }
    else
    {
        self.arrayOfServices = nil;
        if (completionBlock){completionBlock();}
    }
}

#pragma mark - offline options
-(void) saveDomainSevices
{
    NSString * filePath = [NSString stringfilePathForCashesDirectoryWithName:domainSevicesFileName extension:nil];
    NSMutableArray * arrayOfDictionaries = [NSMutableArray array];
    for (TVService * service in self.arrayOfServices)
    {
        NSDictionary * dictionary = [service keyValueRepresentation];
        [arrayOfDictionaries addObject:dictionary];
    }
    [arrayOfDictionaries writeToFile:filePath atomically:YES];
}

-(NSArray *) loadDomainSevices
{

    NSString * filePath = [NSString stringfilePathForCashesDirectoryWithName:domainSevicesFileName extension:nil];
    NSArray * array = [NSArray arrayWithContentsOfFile:filePath];

    NSMutableArray * arrayOfServices = [NSMutableArray array];
    for (NSDictionary * serviceDictionary in array)
    {
        TVService * service = [[TVService alloc] initWithDictionary:serviceDictionary];
        [arrayOfServices addObject:service];
    }

    return arrayOfServices;
    
    
}

#pragma mark - dealocation
-(void)dealloc
{
    [self unregisterFordNotification];
}

@end
