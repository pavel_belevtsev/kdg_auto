//
//  TVSuspensionManager.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVSuspensionManager.h"
#import "TVDataRefreshHandler.h"
#import "NSString+FilePath.h"

#define isSuspendedKey @"isSuspendedKey"
@implementation TVSuspensionManager

+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - API

-(void) startTrakingForSuspnesionWithCompletion:(void(^)()) completion
{
    [self registerFordNotification];
    [self refreshSuspnesionStateWithCompletion:completion];
}


#pragma mark - setters
-(void)setIsSuspended:(bool)isSuspended
{
    if (_isSuspended != isSuspended)
    {
        _isSuspended = isSuspended;
         [[NSNotificationCenter defaultCenter] postNotificationName:NotificationNameSuspendedStatusChanged object:nil];
        [self saveSuspensionStatus];
    }
}

#pragma mark = notification
-(void) registerFordNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logInComplete:)
                                                 name:TVSessionManagerSignInCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutCompeleted:)
                                                 name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSuspnesionState:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSuspnesionState:) name:TVConfigurationManagerDidLoadConfigurationNotification object:nil];


}

-(void) unregisterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) logInComplete:(NSNotification *) notficiation
{
    BOOL isUserSuspended = [[[[TVSessionManager sharedTVSessionManager] userData] objectForKey:@"m_eSuspendState"] boolValue];
    self.isSuspended = isUserSuspended;
}

-(void) logOutCompeleted:(NSNotification *) notification
{
    self.isSuspended = NO;
}

-(void) gotSuspensionStatusFromAPI:(NSNotification *) notification
{
    self.isSuspended = YES;
}

-(void) refreshSuspnesionState:(NSNotification *) notification
{
    [self refreshSuspnesionStateWithCompletion:nil];
}



#pragma mark - refresh Status
-(void) refreshSuspnesionStateWithCompletion:(void(^)()) completion
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if([[TVSDKSettings sharedInstance] offlineMode])
        {
            self.isSuspended = [self loadSuspensionStatus];
            if (completion){completion();}
        }
        else
        {
           TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:[[[TVSessionManager sharedTVSessionManager] sharedInitObject] siteGUID]  delegate:nil];
            __weak TVPAPIRequest * weakRequest = request;
            
            [request setFailedBlock:^{

                self.isSuspended = [self loadSuspensionStatus];
                if (completion){completion();}
            }];

            [request setCompletionBlock:^{
                NSDictionary * dictionary = [weakRequest JSONResponse];
                TVUser * user = [[TVUser alloc] initWithDictionary:[dictionary objectForKey:@"m_user"]];
                self.isSuspended = user.isSuspended;
                if (completion){completion();}
            }];
            [self sendRequest:request];
        }
    }
    else
    {
        self.isSuspended = NO;
        if (completion){completion();}
    }

}

#pragma mark - handle suspension from API statuse
-(BOOL) scanForSuspensionForAPIResponse:(id) response
{
    if (![[TVConfigurationManager sharedTVConfigurationManager] loginBlockedBySuspend])
    {
        return NO;
    }
    
    BOOL suspendedStatusReturned  = [self isResponseIncludedSuspensionStatus:response];
    if (suspendedStatusReturned)
    {
        self.isSuspended = YES;
    }

    return suspendedStatusReturned;
    
}

-(BOOL) isResponseIncludedSuspensionStatus:(id) response
{
    if ([response isKindOfClass:[NSDictionary class]])
    {
        NSDictionary * dictionaryResponse = (NSDictionary *)response;
        NSDictionary * status = [dictionaryResponse objectForKey:@"status"];
        if (status)
        {
            if([status isKindOfClass:[NSDictionary class]])
            {
                TVResponseStatus * responseStatus = [[TVResponseStatus alloc] initWithDictionary:status];
                
                if (responseStatus.code == TVResponseStatusCode_DomainSuspended)
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }
            else if ([status isKindOfClass:[NSString class]])
            {
                NSString * stringStatus = (NSString *)status;
                TVNPVRResponseStatus status = TVNPVRResponseStatusForString(stringStatus);
                if (status == TVNPVRResponseStatusSuspended)
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }
            
        }
        else
        {
            NSString * string =  [dictionaryResponse objectForKey:@"m_oStatus"];
            TVChargingResponseStatus responseStatus = chargingResponseStatusForString(string);
            if (responseStatus == TVChargingResponseStatus_UserSuspended)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        
    }
    else
    {
        return NO;
    }

    return NO;
}

#pragma mark - offline

#define suspensionStatusFileName @"Suspension Status"
-(void) saveSuspensionStatus
{
    NSString * string = [NSString stringWithFormat:@"%d",self.isSuspended];
    NSString * filePath = [NSString stringfilePathForCashesDirectoryWithName:suspensionStatusFileName extension:nil];
    [string writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

-(BOOL) loadSuspensionStatus
{
    NSString * filePath = [NSString stringfilePathForCashesDirectoryWithName:suspensionStatusFileName extension:nil];
    NSString * string = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    return string.boolValue;
}


#pragma mark - dealocation
-(void)dealloc
{
    [self unregisterForNotification];

}
@end
