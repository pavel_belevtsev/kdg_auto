//
//  QLogManager.m
//  QadabraSDK
//
//  Created by Nissim Pardo on 3/25/14.
//  Copyright (c) 2014 Marimedia LTD. All rights reserved.
//

#import "TVLogHandler.h"

static TVLogLevel TVLOG_LEVEL = TVLogLevelWarn;

@implementation TVLogHandler
+ (TVLogLevel)getLogLevel {
	return TVLOG_LEVEL;
}

+ (void)setLogLevel:(TVLogLevel)level {
    
    @synchronized(self)
    {
        TVLOG_LEVEL = level;
    }
}
@end
