//
//  QLog.m
//  QadabraSDK
//
//  Created by Nissim Pardo on 3/25/14.
//  Copyright (c) 2014 Marimedia LTD. All rights reserved.
//

#import "TVLog.h"

NSString *const LoggingNotification = @"kQLoggingNotification";
NSString *const LogMessageKey = @"kQLogMessageKey";
NSString *const LogMessageLevelKey = @"kQLogMessageLevelKey";



void _TVLog(TVLogLevel logLevel, NSString * fileName, int lineNumber,NSString *format, ...)
{
    format = [NSString stringWithFormat:@"%@ (line:%d): %@",fileName,lineNumber,format];
    va_list args;
    va_start(args, format);
    notifyListener([[[NSString alloc] initWithFormat:format arguments:args] init], logLevel);
    va_end(args);
    
    va_start(args,format) ;
    NSLogv(format, args);
    va_end(args) ;
    
    
}

void notifyListener(NSString *message, NSInteger messageLevel)
{
    [[NSNotificationCenter defaultCenter] postNotificationName:LoggingNotification
                                                        object:nil
                                                      userInfo:@{LogMessageKey: message,
                                                                 LogMessageLevelKey: [NSNumber numberWithInteger:messageLevel]}];
}

