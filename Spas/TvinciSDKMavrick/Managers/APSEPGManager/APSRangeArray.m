//
//  APSRangeArray.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSRangeArray.h"

@interface APSRangeArray ()
@property (strong, nonatomic) NSMutableArray * rangeArray;
@end

@implementation APSRangeArray

-(NSArray *) rangeMap
{
    return  [self.rangeArray copy];
}


-(NSArray *) rangesIntersectRange:(APSRange *) range
{

    NSMutableArray * ranges = [NSMutableArray array];
    for (APSRange * interatedRange in  self.rangeArray)
    {
        //start position
        if (range.start>=interatedRange.start && range.start<=interatedRange.end)
        {
            [ranges addObject:interatedRange];
            continue;
        }
        
        //middle position
        if (range.start<interatedRange.start && range.end>interatedRange.end)
        {
            [ranges addObject:interatedRange];
            continue;
        }
        
        //middle position
        if (range.end>=interatedRange.start && range.end<=interatedRange.end)
        {
            [ranges addObject:interatedRange];
            continue;
        }
        
    }
    
    return [NSArray arrayWithArray:ranges];
}



-(NSArray *) intersection:(APSRange *) range
{
    NSArray * rangesIntersectsGivenRange = [self rangesIntersectRange:range];
    NSMutableArray * IntersectionRanges = [[NSMutableArray alloc] initWithArray:rangesIntersectsGivenRange copyItems:YES];
    
    APSRange * firstRange = [IntersectionRanges firstObject];
    APSRange * lastRange = [IntersectionRanges lastObject];
    
    firstRange.start = MAX(firstRange.start, range.start);
    lastRange.end = MIN(lastRange.end, range.end);
    
    return IntersectionRanges;
    
}


-(APSRange *) unification:(APSRange *) range
{
    APSRange * unificatedRange = [range copy];
    NSArray * rangesIntersectsGivenRange = [self rangesIntersectRange:range];
    NSMutableArray * IntersectionRanges = [[NSMutableArray alloc] initWithArray:rangesIntersectsGivenRange copyItems:YES];
    
    APSRange * firstRange = [IntersectionRanges firstObject];
    APSRange * lastRange = [IntersectionRanges lastObject];
    
    unificatedRange.start = MIN(firstRange.start, range.start);
    unificatedRange.end = MAX(lastRange.end, range.end);
    
    return unificatedRange;
}



-(APSRange *) addRange:(APSRange *) range
{
    
    APSRange * unifiedRange  = nil;
    
    if (self.rangeArray == nil)
    {
        self.rangeArray = [NSMutableArray array];
    }
    
    
    NSArray * rangesIntersectsGivenRange = [self rangesIntersectRange:range];
    
    if (rangesIntersectsGivenRange.count>0)
    {
        APSRange * firstRange = [rangesIntersectsGivenRange firstObject];
        APSRange * lastRange = [rangesIntersectsGivenRange lastObject];
        NSInteger  startUnifiedRange = 0;
        NSInteger  endUnifiedRange = 0;
        
        startUnifiedRange = MIN(range.start, firstRange.start);
        endUnifiedRange = MAX(range.end, lastRange.end);
        
        for (APSRange * rangeIntersect in rangesIntersectsGivenRange)
        {
            [self.rangeArray removeObject:rangeIntersect];
        }
        
        unifiedRange  = [APSRange rangeWithStart:startUnifiedRange end:endUnifiedRange];
        
    }
    else
    {
        unifiedRange  = range;
    }
    
    
    
    [self.rangeArray addObject:unifiedRange];
    
    [self sort];
    return  unifiedRange;;
}


-(APSRange *) isRangeFullyContainedByAnother:(APSRange *) range
{
    NSArray * intersections = [self intersection:range];
    if (intersections.count == 1)
    {
        APSRange * intersecion = intersections.lastObject;
        if (intersecion.start == range.start && intersecion.end == range.end)
        {
            return intersecion;
        }
    }
    
    return  nil;
}

-(void) cleanRanges
{
    [self.rangeArray removeAllObjects];
}



+(id) rangeArray
{
    APSRangeArray * array = [[APSRangeArray alloc] init];
    return array;
}


-(void) sort
{
    [self.rangeArray sortUsingComparator:
             ^NSComparisonResult(APSRange * range1, APSRange * range2)
    {
        return [range1 compare:range2];
    }];
}


- (NSString *)description
{

    return [NSString stringWithFormat:@"%@", self.rangeArray];
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.rangeArray forKey:@"RangeArray"];
   
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init]))
    {
        //decode properties, other class vars
        self.rangeArray = [decoder decodeObjectForKey:@"RangeArray"];
    }
    
    return self;
}


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    APSRangeArray *another = [[APSRangeArray alloc] init];
    another.rangeArray = [[NSMutableArray alloc] initWithArray:self.rangeArray copyItems:YES];
    return another;
}

@end
