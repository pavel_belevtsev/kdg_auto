//
//  APSEPGManager.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSTVChannel.h"
#import "APSTVProgram.h"
#import "APSMediaItem.h"

#define APSEPGManagerCurrentProgramDidChangedNotificationName @"CurrentProgramDidChanged"


@class APSTVChannel;
@class APSBlockOperation;

@interface APSEPGManager : NSObject


+ (instancetype)sharedEPGManager;


/**
 *  This value configure how long time a program will be alive in the data base default is 60*60*4 ( 4 hours )
 */
@property (assign, nonatomic) NSTimeInterval lifeCycleTime;

/**
 *  This value configure the server offset time
 */
@property (assign, nonatomic) NSInteger utcOffset;



/**
 *  clear all epg data includ valid data
 *
 *  @return the operation that you can cancel it.
 */
-(APSBlockOperation * ) clearAllData;

/**
 *	this function is nececcery for downlading channels , without calling this function channels will not be downloaded
 */
-(void) loadChannelsWithID:(NSString *) collectionID StartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;


/**
 *
 *  @return all epg channels as TVMediaItem 
 *  Channels are alwase in the application and the loaded on the start app
 */
-(NSArray *) allChannels;



/**
 *	getEPGChannelFromChannelID
 *
 *	@param	channelID
 *
 *	@return	EPG Channel belongs to the epg id
 */
-(TVMediaItem *) channelFromChannelID:(NSString *) channelID;

/**
 *  This method provide program that playing during a spesific date
 *
 *  @param channelID
 *  @param date            The spesific date the program will be play during this time.
 *  @param privateContext  The context of the view controller this program will be shown
 *  @param starterBlock
 *  @param failedBlock
 *  @param completionBlock
 *
 *  @return the operation of the process for canceling when needed
 */
-(APSBlockOperation * ) programForChannelId:(NSString *) channelID
                                       date:(NSDate *) date
                             privateContext:(NSManagedObjectContext *) privateContext
                               starterBlock:(void(^)(void)) starterBlock
                                failedBlock:(void(^)(void)) failedBlock
                            completionBlock:(void(^)(APSTVProgram * program)) completionBlock;




/**
 *  This medhos provide programs that playing during range of 2 dates
 *
 *  @param channelID
 *  @param fromDate         The start date of range we will download the programs ( if we dont have it already in the data base)
 *  @param toDate           The end date of range we will download the programs ( if we dont have it already in the data base)
 *  @param actualFromDate   The start date the range of programs i'll get in the completion block.
 *  @param acturalToDate    The end date the range of programs i'll get in the completion block.
 *  @param includeStartDate This flag define wich programs we will get for the start range if the programs will start only ofter the start
 date or during the sraet date.
 *  @param includeEndDate   This flag define wich programs we will get for the end range if the programs will end only before the end date or
 during the end date.
 *  @param privateContext   Each view controller have to hold its private context so that the programs won't be deleted while he presenting
 them.
 *  @param starterBlock
 *  @param failedBlock
 *  @param completionBlock
 *
 *  @return the operation of the process for canceling when needed
 */
-( APSBlockOperation * ) programsByRangeForChannel:(NSString *) channelID
                                          fromDate:(NSDate *) fromDate
                                            toDate:(NSDate *) toDate
                                    actualFromDate:(NSDate *) actualFromDate
                                      actualToDate:(NSDate *) acturalToDate
                     includeProgramsDuringfromDate:(BOOL) includeStartDate
                       includeProgramsDuringToDate:(BOOL) includeEndDate
                                    privateContext:(NSManagedObjectContext *) privateContext
                                      starterBlock:(void(^)(void)) starterBlock
                                       failedBlock:(void(^)(void)) failedBlock
                                   completionBlock:(void(^)(NSArray *)) completionBlock;






@end
