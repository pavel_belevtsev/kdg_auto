//
//  APSBlockOperation.m
//  Spas
//
//  Created by Rivka Peleg on 10/1/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBlockOperation.h"

@implementation APSBlockOperation

-(void)cancel
{
    [super cancel];
    
    if (self.request != nil)
    {
        TVLogTrace(@"APSBlockOperation Canceled Request %@",self.request);
    }
    [self.request cancel];
}



-(void)dealloc
{
    [self cancel];
    TVLogDebug(@"dealoc APSBlockOperation");
}
@end
