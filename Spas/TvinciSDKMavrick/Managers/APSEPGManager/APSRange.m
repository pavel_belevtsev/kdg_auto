//
//  APSRange.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSRange.h"

@implementation APSRange
+(APSRange *) rangeWithStart:(NSInteger)start end:(NSInteger)end
{
    APSRange * range = [[APSRange alloc] init];
    range.start = start;
    range.end = end;
    return range;
}




-(NSComparisonResult) compare:(APSRange *) range
{
    if (self.start < range.start)
    {
        return NSOrderedAscending;
    }
    else if (self.start == range.start)
    {
        return NSOrderedSame;
    }
    else
    {
        return NSOrderedDescending;
    }
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"<APSRange %p> [%ld,%ld] InsersionDate:%@",self,(long)self.start,(long)self.end,self.insertDate];
}


-(id)copyWithZone:(NSZone *)zone
{
    // We'll ignore the zone for now
    APSRange *another = [[APSRange alloc] init];
    another.start = self.start;
    another.end = self.end;
    another.insertDate = self.insertDate;
    return another;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeInteger:self.start forKey:@"start"];
    [encoder encodeInteger:self.end forKey:@"end"];
    [encoder encodeInteger:self.insertDate.timeIntervalSince1970 forKey:@"insertionDate"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init]))
    {
        //decode properties, other class vars
        self.start = [decoder decodeIntegerForKey:@"start"];
        self.end = [decoder decodeIntegerForKey:@"end"];
        self.insertDate = [NSDate dateWithTimeIntervalSince1970:[decoder decodeIntegerForKey:@"insertionDate"]];
    }
    
    return self;
}

@end
