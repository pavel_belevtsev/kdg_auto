//
//  APSRangeArray.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APSRange.h"

@interface APSRangeArray : NSObject


/*
 get all ranges included in this range array.
*/
-(NSArray *) rangeMap;

/**
 *  autorelease constructor
 *
 *  @return rangeArray object
 */
+(id) rangeArray;

/**
 *
 *
 *  @param range
 *
 *  @return ranges that intersects this range
 */
-(NSArray *) rangesIntersectRange:(APSRange *) range;

/**
 *  The intersection
 *
 *  @return arry of Intersection
 */
-(NSArray *) intersection:(APSRange *) range;

/**
 *  The Unification
 *
 *  @param range
 *
 *  @return ranges of unification of the required range and his intersections ranges .
 */
-(APSRange *) unification:(APSRange *) range;


/**
 *  if all range is loaded already
 *
 *  @param range
 *
 *  @return if the ranged is fully contained by another - yes other no
 */
-(APSRange *) isRangeFullyContainedByAnother:(APSRange *) range;

/**
 *
 *  @param range Adding range and uniun intersects rangesj
 *
 *  @return the range add , if there was a union , return the unified range
 */
-(APSRange *) addRange:(APSRange *) range;


/**
 *  clean all ranges
 */
-(void) cleanRanges;



// remove range?
@end
