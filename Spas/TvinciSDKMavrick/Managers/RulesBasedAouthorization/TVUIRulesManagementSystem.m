//
//  TVRulesBasedAuthorization.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVUIRulesManagementSystem.h"
#import "TVUIRulesUnit.h"
#import "TVUIRulesCategories.h"

@interface TVUIRulesManagementSystem ()
@end

@implementation TVUIRulesManagementSystem


+ (id) sharedInstance {
    static dispatch_once_t onceToken = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}

-(void) loadUIUnitsRulesMapFile
{
    NSString * fileName = [self.delegate rulesManagementSystemInputFileName:self];
    NSString * path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    NSDictionary * mainDictionary = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSArray * values = [mainDictionary objectForKey:@"AllUnitsRules"];
    
    NSMutableDictionary * temp = [NSMutableDictionary dictionary];
    for (NSDictionary * dict in values )
    {
        TVUIRulesUnit * rulesUnit = [[TVUIRulesUnit alloc] initWithDictionary:dict];
        [temp setObject:rulesUnit forKey:rulesUnit.UIUnitID];
    }
    
    self.dictionaryOfRuleUnits = [NSMutableDictionary dictionaryWithDictionary:temp];
    
}


-(TVUIRule ) ruleForUnit:(NSString *) unitId
{
    TVUIRulesUnit * rulesUnit = [self.dictionaryOfRuleUnits objectForKey:unitId];
    
    if (rulesUnit == nil)
    {
        return TVUIRule_Exist;
    }
    
    if ([self.delegate respondsToSelector:@selector(rulesManagementSystem:isServiceDetached:)]) {
        
        BOOL denied = [self.delegate rulesManagementSystem:self isServiceDetached:rulesUnit.relatedService];
        if (denied)
        {
            return rulesUnit.detachedRule;
        }
    }
    
    
    
    if ([self.delegate respondsToSelector:@selector(rulesManagementSystemIsAnonymous:)])
    {
        BOOL isAnonymous = [self.delegate rulesManagementSystemIsAnonymous:self];
        if (isAnonymous)
        {
            return  rulesUnit.anonymousRule;
        }
    }
    
    
    if ([self.delegate respondsToSelector:@selector(rulesManagementSystem:isDeniedForService:)]) {
        
        BOOL denied = [self.delegate rulesManagementSystem:self isDeniedForService:rulesUnit.relatedService];
        if (denied)
        {
            return rulesUnit.deniedServiceRule;
        }
    }
    
    
    if ([self.delegate respondsToSelector:@selector(rulesManagementSystemIsUserSuspended:)])
    {
        BOOL isSuspended = [self.delegate rulesManagementSystemIsUserSuspended:self];
        
        if (isSuspended)
        {
            return rulesUnit.suspendedRule;
        }
    }
    
    return TVUIRule_Exist;
}



-(void)setDelegate:(id<TVUIRulesManagementSystemProvider>)delegate
{
    if (_delegate != delegate)
    {
        _delegate = delegate;
        
        [self loadUIUnitsRulesMapFile];
    }
}


@end
