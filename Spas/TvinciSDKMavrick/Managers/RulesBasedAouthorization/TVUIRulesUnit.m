//
//  TVComponentRules.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVUIRulesUnit.h"
#import "NSDictionary+NSNullAvoidance.h"


@implementation TVUIRulesUnit

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    NSString * relatedServiceString = [dictionary objectForKey:@"RelatedService"];
    self.relatedService = UIUnitRelatedServiceForString(relatedServiceString);
    self.anonymousRule = UIRuleForString([dictionary objectForKey:@"anonymousRule"]);
    self.deniedServiceRule = UIRuleForString([dictionary objectForKey:@"deniedServiceRule"]);
    self.detachedRule = UIRuleForString([dictionary objectForKey:@"detachedRule"]);
    
    self.suspendedRule = UIRuleForString([dictionary objectForKey:@"suspendedRule"]);
    self.UIUnitID = [dictionary objectOrNilForKey:@"UnitID"];
}









@end
