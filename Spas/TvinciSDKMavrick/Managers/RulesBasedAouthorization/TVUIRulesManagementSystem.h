//
//  TVRulesBasedAuthorization.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVUIRulesUnit.h"
#import "TVConstants.h"



/**
 *  The TVUIRulesManagementSystem class designed for unify all the UI logic depends on the user status and the application build status
 *  The users statuses we are considering in our logic is:
 *  1) Detahcable precompiled feautres for this version
 *  2) Anonymous mode
 *  3) Suspendion user mode
 *  4) Commercialization - The user purchased services.
 *  q. How do we know how to react for each case?
    a. each application needs to provide a file that will describe the behaviur for each set of controles.
        For example: start over buttons:
                    on anonymouse: all button should be hidden
                    when start over feature is detached pre build : all button should be hidden
                    when user is suspended : all buttons should be disabled
                    when user did not purchased this service also all buttons should be disable
        This set of rules that connected to a rules unit ID configured in the provided file and managed in this class, each button knows its UIUnitID and asking for activate the correct behaviur on himself ( see TVUIRulesCategories ).
 */



#define RULES [TVUIRulesManagementSystem sharedInstance]

@class TVUIRulesManagementSystem;


/**
 * In order to the user and the application status, and to get the content of the rules file a provider in the client side needs to conforms this protocol.
 */
@protocol TVUIRulesManagementSystemProvider <NSObject>

/**
 *  In order to get the rules files, the manager asking for the file name located in the application bundle.
 *  @return string filename
 */
-(NSString *) rulesManagementSystemInputFileName:(TVUIRulesManagementSystem * ) sender;

@optional

/**
 *  In order to know if the application settings - precompiled buid is defined to uninclude spesific service/feature wothout dependency of the user status the manager asking the provider.
 *  @return if the feature is detached
 */
-(BOOL) rulesManagementSystem:(TVUIRulesManagementSystem * ) sender
                          isServiceDetached:(TVUIUnitRelatedService ) service;


/**
 * In order to know if user is on anonymouse mode the manager asking the provider
 * @return if user is anonymouse
 */
-(BOOL) rulesManagementSystemIsAnonymous:(TVUIRulesManagementSystem * ) sender;

/**
 *  In order to get user suspendion status the manager asking the provider.
 *  @return bool value indicates if the user suspended or not.
 */
-(BOOL) rulesManagementSystemIsUserSuspended:(TVUIRulesManagementSystem * ) sender;

/**
 *  In order to know if user is denied ( not purchased ) for a service ( for example , start over , npvr )
 *  @return if user denied for the service or not.
 */
-(BOOL) rulesManagementSystem:(TVUIRulesManagementSystem * ) sender
                           isDeniedForService:(TVUIUnitRelatedService ) service;

@end


@interface TVUIRulesManagementSystem : NSObject

+ (id) sharedInstance;


@property (weak, nonatomic) id<TVUIRulesManagementSystemProvider> delegate;

/**
 *  This dictionary represent the file of the rules provided by the client side.
 */
@property (strong, nonatomic) NSDictionary * dictionaryOfRuleUnits;

/**
 *  In order to know the correct behaviur for a Unit in the current status each control/behaviur can call this method.
 *
 *  @param unitId  Each set of controles or behaviur connected to a one logic rule
 *
 *  @return The correct behaviur for this unit
 */
-(TVUIRule ) ruleForUnit:(NSString *) unitId;
@end
