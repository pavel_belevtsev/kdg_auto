//
//  NSObject+TVObjectAssociatedResourceID.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/25/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVUIRulesCategories.h"
#import "TVUIRulesManagementSystem.h"
#import <objc/runtime.h>

static char kAssociatedObjectKey;

@implementation NSObject (TVUIRules)

@dynamic UIUnitID;


- (void)setUIUnitID:(NSString *)objectID{
    objc_setAssociatedObject(self, &kAssociatedObjectKey, objectID, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *) UIUnitID {
    return objc_getAssociatedObject(self, &kAssociatedObjectKey);
}
@end

@implementation UICollectionViewCell (TVUIRules)
+(BOOL) shouldSelectCellForUIUnit:(NSString *) unitID
{
    TVUIRule rule = [RULES ruleForUnit:unitID];
    if (rule == TVUIRule_NotExist || rule == TVUIRule_Disabled)
    {
        return NO;
    }
    else
    {
        return YES;
    }

}
@end



@implementation NSString (TVUIRules)

-(BOOL) isUIUnitExist
{
    TVUIRule rule = [RULES ruleForUnit:self];
    return  (rule != TVUIRule_NotExist);
}



@end

@implementation UIView (TVUIRules)
-(void) activateAppropriateRule
{
    TVUIRule rule = [RULES ruleForUnit:self.UIUnitID];
    
    switch (rule)
    {
        case TVUIRule_NotExist:
            self.hidden = YES;
            self.userInteractionEnabled = YES;
            break;
        case TVUIRule_Disabled:
            self.hidden = NO;
            self.userInteractionEnabled = NO;
            break;
        case TVUIRule_Exist:
            self.userInteractionEnabled = YES;
            self.hidden = NO;
            break;
        default:
            break;
    }
}

-(void) resetRule
{
    self.userInteractionEnabled = YES;
    self.hidden = NO;
}

@end


@implementation UIControl (TVUIRules)
-(void) activateAppropriateRule
{
    TVUIRule rule = [RULES ruleForUnit:self.UIUnitID];
    
    switch (rule)
    {
        case TVUIRule_NotExist:
            self.hidden = YES;
            self.enabled = YES;
            break;
        case TVUIRule_Disabled:
            self.hidden = NO;
            self.enabled = NO;
            break;
        case TVUIRule_Exist:
            self.hidden = NO;
            self.enabled = YES;
            break;
        default:
            break;
    }
}

-(void) resetRule
{
    self.hidden = NO;
    self.enabled = YES;
}

@end


@implementation UISlider (TVUIRules)
-(void) activateAppropriateRuleWithThumbImage:(UIImage *) image
{
    TVUIRule rule = [RULES ruleForUnit:self.UIUnitID];
    
    switch (rule)
    {
        case TVUIRule_NotExist:
            [self setThumbImage:[[UIImage alloc] init] forState:UIControlStateNormal];
            self.userInteractionEnabled = NO;
            break;
        case TVUIRule_Disabled:
            [self setThumbImage:image forState:UIControlStateNormal];
            self.userInteractionEnabled = NO;
            break;
        case TVUIRule_Exist:
            [self setThumbImage:image forState:UIControlStateNormal];
            self.userInteractionEnabled = YES;
            break;
        default:
            break;
    }
}
-(void) resetRuleWithThumbImage:(UIImage *) image;
{
    [self setThumbImage:image forState:UIControlStateNormal];
    self.userInteractionEnabled = YES;
}

@end

