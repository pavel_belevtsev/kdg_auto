//
//  DomainServicesResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVDomainServicesResponse.h"
#import "TVService.h"

@implementation TVDomainServicesResponse


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSArray * arrayOfServiceDictionaries =  [dictionary objectOrNilForKey:@"services"];
    
    NSMutableArray * tempArray = [NSMutableArray array];
    for (NSDictionary * dict in arrayOfServiceDictionaries)
    {
        TVService * service = [[TVService alloc] initWithDictionary:dict];
        [tempArray addObject:service];
    }
    
    self.arrayServices = [NSArray arrayWithArray:tempArray];
    self.responseStatus = [[TVResponseStatus alloc] initWithDictionary:[dictionary objectOrNilForKey:@"status"]];
}

@end
