//
//  TVHomeNetworkInfo.m
//  TvinciSDK
//
//  Created by Israel Berezin on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVHomeNetworkInfo.h"


const NSString * TVHomeNetworkCreateDateKey = @"CreateDate";
const NSString * TVHomeNetworkDescriptionKey = @"Description";
const NSString * TVHomeNetworkIsActiveKey = @"IsActive";
const NSString * TVHomeNetworkNameKey = @"Name";
const NSString * TVHomeNetworkUIDKey = @"UID";


@implementation TVHomeNetworkInfo

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    NSString* date = [dictionary objectForKey:TVHomeNetworkCreateDateKey];
    self.createDate = [dateFormatter dateFromString:date];

    
    self.networkDescription = [dictionary objectForKey:TVHomeNetworkDescriptionKey];
    self.isActive= [[dictionary objectForKey:TVHomeNetworkIsActiveKey] boolValue];
    self.networkName = [dictionary objectForKey:TVHomeNetworkNameKey];
    self.uid = [dictionary objectForKey:TVHomeNetworkUIDKey];
}

-(NSString *)description
{
  return [NSString stringWithFormat:@"uid = %@, name = %@, createDate =%@ , isActive = %d, description = %@",self.uid, self.networkName, self.createDate,self.isActive, self.networkDescription];
}

-(void)dealloc
{

}

NSString * TVNameHomeNetworkStatus(HomeNetworkStatus action)
{
    static NSString *const SocialActionsNames[] = {@"OK",@"QuantityLimitation",@"FrequencyLimitation",@"NetworkExists",@"NetworkDoesNotExist",@"InvalidInput",@"Error",nil};
    NSString *name = nil;
    NSInteger length = (sizeof(SocialActionsNames)/sizeof(SocialActionsNames[0]));
    if (action < length)
    {
        name = SocialActionsNames[action];
    }
    return name;
}

@end
