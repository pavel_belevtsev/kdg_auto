//
//  TVResponseStatus.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVResponseStatus.h"

@implementation TVResponseStatus

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.code = [[dictionary objectOrNilForKey:@"code"] intValue];
    self.message = [dictionary objectOrNilForKey:@"message"];
}

@end
