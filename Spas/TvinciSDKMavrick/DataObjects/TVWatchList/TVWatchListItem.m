//
//  TVWatchListItem.m
//  TvinciSDK
//
//  Created by Tarek Issa on 12/16/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVWatchListItem.h"

NSString *const TVWatchSiteGuidKeyName  = @"siteGuid";
NSString *const TVWatchListTypeKeyName  = @"listType";
NSString *const TVWatchItemTypeKeyName  = @"itemType";
NSString *const TVWatchItemObjKeyName   = @"itemObj";
NSString *const TVWatchOrderKeyName     = @"orderNum";
NSString *const TVWatchItemKeyName      = @"item";


@interface TVWatchListItem ()
{
    
}

@property (strong, readwrite) NSString *siteGuid;
@property (strong, readwrite) NSArray *items;
@property (assign, readwrite) TVListItemType itemType;
@property (assign, readwrite) TVListType listType;

@end

@implementation TVWatchListItem

- (void)dealloc
{
    self.siteGuid = nil;
    self.items = nil;
}


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary {
    
    self.siteGuid   = [dictionary objectOrNilForKey:TVWatchSiteGuidKeyName];
    //self.items      = [dictionary objectOrNilForKey:TVWatchItemKeyName];
    self.itemType   = [[dictionary objectOrNilForKey:TVWatchItemTypeKeyName] intValue];
    self.listType   = [[dictionary objectOrNilForKey:TVWatchListTypeKeyName] intValue];

    NSArray* itemObjsArrRAW = [dictionary objectOrNilForKey:TVWatchItemObjKeyName];
    NSMutableArray* tmpArr = [NSMutableArray array];
    for (NSDictionary* itemObjDict in itemObjsArrRAW) {
        ItemObj* itemObj = [[ItemObj alloc] initWithDictionary:itemObjDict];
        [tmpArr addObject:itemObj];
    }
    self.items = [[NSArray alloc] initWithArray:tmpArr];
    tmpArr = nil;
}

@end





@implementation ItemObj

- (void)dealloc
{
    self.itemID = nil;
}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary {
    self.itemID = [dictionary objectOrNilForKey:TVWatchItemKeyName];
    self.order  = [[dictionary objectOrNilForKey:TVWatchOrderKeyName] intValue];
}

@end