//
//  TVNPVRSeriesItem.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVNPVRSeriesItem : BaseModelObject


@property (strong, nonatomic) NSString * epgChannelID;
@property (strong, nonatomic) NSString * recordID;
@property (strong, nonatomic) NSString * seriesId;
@property (strong, nonatomic) NSString * seriesName;
@end
