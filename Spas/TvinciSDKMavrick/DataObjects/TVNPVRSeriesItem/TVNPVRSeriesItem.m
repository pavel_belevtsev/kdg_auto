//
//  TVNPVRSeriesItem.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVNPVRSeriesItem.h"

@implementation TVNPVRSeriesItem


//[{
//    "epgChannelID": "52",
//    "recordingID": "18",
//    "seriesID": "238000294-016FFB37727C63975796AEA72B861B17",
//    "seriesName": "1000 Wege, ins Gras zu beiÃ?en"
//}]

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.epgChannelID  = [dictionary objectOrNilForKey:@"epgChannelID"];
    self.recordID = [dictionary objectOrNilForKey:@"recordingID"];
    self.seriesId = [dictionary objectOrNilForKey:@"seriesID"];
    self.seriesName = [dictionary objectOrNilForKey:@"seriesName"];
}

@end
