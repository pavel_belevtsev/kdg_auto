//
//  MediaAssetsStats.m
//  TvinciSDK
//
//  Created by Israel on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "MediaAssetsStats.h"

const NSString * Ratekey = @"m_dRate";
const NSString * AssetIDkey = @"m_nAssetID";
const NSString * Likeskey = @"m_nLikes";
const NSString * Viewskey = @"m_dViews";
const NSString * Voteskey = @"m_dVotes";
const NSString * AssetTypekey = @"m_dAssetType";

@implementation MediaAssetsStats


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.rate       = [[dictionary objectForKey:Ratekey] integerValue];
    self.assetId    = [[dictionary objectForKey:AssetIDkey] integerValue];
    self.likes      = [[dictionary objectForKey:Likeskey] integerValue];
    self.views      = [[dictionary objectForKey:Viewskey] integerValue];
    self.votes      = [[dictionary objectForKey:Voteskey] integerValue];
//    self.assetType  = [[dictionary objectForKey:Ratekey] intValue];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"AssetId = %ld, Rate = %ld, Likes = %ld, Views = %ld, Votes = %ld",(long)self.assetId,(long)self.rate,(long)self.likes,(long)self.views,(long)self.votes];
}
@end
