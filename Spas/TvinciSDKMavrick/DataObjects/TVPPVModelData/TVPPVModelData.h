//
//  TVPPVModelData.h
//  TvinciSDK
//
//  Created by Israel Berezin on 6/3/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

#import "JSONModel.h"

//m_sCodes
@protocol TVPPVData_codes @end
@interface TVPPVData_codes : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, strong) NSString *name;

@end

//m_sName
@protocol TVPPVData_name @end
@interface TVPPVData_name : JSONModel

@property (nonatomic, strong) NSString *languageCode3, *value;

@end

//m_oPrise > m_oCurrency
@interface TVPPVData_prise_currency : JSONModel

@property (nonatomic, strong) NSString *currencyCD3, *currencyCD2, *currencySign;
@property (nonatomic, readwrite) NSInteger currencyId;

@end

//m_oPrise
@interface TVPPVData_prise : JSONModel

@property (nonatomic, readwrite) CGFloat price;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) TVPPVData_prise_currency *currency;

@end

//m_oPPVPriceCode
@interface TVPPVData_PPVPriceCode : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) TVPPVData_prise *prise;
@property (nonatomic, readwrite) NSInteger objectId;
@property (nonatomic, strong) NSString<Optional> *description1;

@end

//m_oPPVPriceCode
@interface TVPPVData_PPVUsageModule : JSONModel

@property (nonatomic, readwrite) NSInteger waiver, couponId, deviceLimitId, extDiscountId, internalDiscountId, isRenew, maxNumberOfViews, objectId, waiverPeriod, numOfRecPeriods, pricingId, maxUsageModuleLifeCycle, viewLifeCycle, type;
@end

//m_oPPVPriceCode
@interface TVPPVData_purchasingInfo : JSONModel

@property (nonatomic, readwrite) NSInteger PPVCode, maxUses, currentUses, paymentMethod, PPVPurchaseId;
@property (nonatomic, strong) NSString *stringCurrentDate, *stringLastViewDate, *stringPurchaseDate, *stringEndDate;
@property (nonatomic, readwrite) BOOL recurringStatus, isSubRenewable;
@property (nonatomic, strong) NSString<Optional> *deviceUDID, *deviceName;

@property (nonatomic, strong, readonly) NSDate<Ignore> *lastViewDate, *currentDate, *purchaseDate, *endDate;

@end

@class TVMediaItem;

@interface TVPPVModelData : JSONModel

@property NSInteger firstDeviceLimitation;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, readwrite) BOOL isSubscriptionOnly;
@property (nonatomic, strong) NSDictionary *couponsGroup;
@property (nonatomic, strong) NSDictionary<Optional> *discountModule;
@property (nonatomic, strong) TVPPVData_PPVPriceCode *PPVPriceCode;
@property (nonatomic, strong) TVPPVData_PPVUsageModule *PPVUsageModule;
@property (nonatomic, strong) NSArray<Optional> * relatedFileTypes;
@property (nonatomic, strong) NSArray<TVPPVData_name ,ConvertOnDemand> * arrDescription;
@property (nonatomic, readwrite) NSInteger objectCode;
@property (nonatomic, strong) NSString *objectVirtualName;

@end
