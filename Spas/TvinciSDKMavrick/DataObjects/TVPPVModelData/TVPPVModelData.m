//
//  TVPPVModelData.m
//  TvinciSDK
//
//  Created by Israel Berezin on 6/3/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVPPVModelData.h"

@implementation TVPPVModelData

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_Product_Code": @"productCode",
                                                       @"m_bFirstDeviceLimitation": @"firstDeviceLimitation",
                                                       @"m_bSubscriptionOnly": @"isSubscriptionOnly",
                                                       @"m_oCouponsGroup": @"couponsGroup",
                                                       @"m_oDiscountModule": @"discountModule",
                                                       @"m_oPriceCode": @"PPVPriceCode",
                                                       @"m_oUsageModule": @"PPVUsageModule",
                                                       @"m_relatedFileTypes": @"relatedFileTypes",
                                                       @"m_sDescription": @"arrDescription",
                                                       
                                                       @"m_sObjectCode": @"objectCode",
                                                       @"m_sObjectVirtualName": @"objectVirtualName"
                                                       }];
}

@end

@implementation TVPPVData_PPVUsageModule

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_bWaiver": @"waiver",
                                                       @"m_coupon_id": @"couponId",
                                                       @"m_sVirtualName": @"virtualName",
                                                       @"m_device_limit_id": @"deviceLimitId",
                                                       @"m_ext_discount_id": @"extDiscountId",
                                                       @"m_internal_discount_id": @"internalDiscountId",
                                                       @"m_is_renew": @"isRenew",
                                                       @"m_nMaxNumberOfViews": @"maxNumberOfViews",
                                                       @"m_nObjectID": @"objectId",
                                                       @"m_nWaiverPeriod": @"waiverPeriod",
                                                       @"m_num_of_rec_periods": @"numOfRecPeriods",
                                                       @"m_pricing_id": @"pricingId",
                                                       @"m_sVirtualName": @"virtualName",
                                                       @"m_subscription_only": @"isSubscriptionOnly",
                                                       @"m_tsMaxUsageModuleLifeCycle": @"maxUsageModuleLifeCycle",
                                                       @"m_tsViewLifeCycle": @"viewLifeCycle",
                                                       @"m_type": @"type"
                                                       }];
}


@end

@implementation TVPPVData_PPVPriceCode

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_sCode": @"code",
                                                       @"m_sName": @"name",
                                                       @"m_oPrise": @"prise",
                                                       @"m_nObjectID": @"objectId",
                                                       @"m_sDescription": @"description1"
                                                       }];
}

@end

@implementation TVPPVData_prise

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_dPrice": @"price",
                                                       @"m_sName": @"name",
                                                       @"m_oCurrency": @"currency"
                                                       }];
}

@end

@implementation TVPPVData_prise_currency

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_sCurrencyCD3": @"currencyCD3",
                                                       @"m_sCurrencyCD2": @"currencyCD2",
                                                       @"m_sCurrencySign": @"currencySign",
                                                       @"m_nCurrencyID": @"currencyId"
                                                       }];
}

@end

@implementation TVPPVData_name

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_sLanguageCode3": @"languageCode3",
                                                       @"m_sValue": @"value"
                                                       }];
}
@end

@implementation TVPPVData_codes

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_sCode": @"code",
                                                       @"m_sName": @"name"
                                                       }];
}
@end


@interface TVPPVData_purchasingInfo ()
@property (nonatomic, strong) NSDate *lastViewDate, *currentDate, *purchaseDate, *endDate;
@end

@implementation TVPPVData_purchasingInfo

#define _P_dateFormat @"yyyy-MM-dd HH:mm:ss"
#define _P_dateFormat2 @"yyyy-MM-dd HH:mm:ss.SSS"

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"m_sPPVCode": @"PPVCode",
                                                       @"m_nMaxUses": @"maxUses",
                                                       @"m_nCurrentUses": @"currentUses",
                                                       @"m_dEndDate": @"stringEndDate",
                                                       @"m_dCurrentDate": @"stringCurrentDate",
                                                       @"m_dLastViewDate": @"stringLastViewDate",
                                                       @"m_dPurchaseDate": @"stringPurchaseDate",
                                                       @"m_bRecurringStatus": @"recurringStatus",
                                                       @"m_bIsSubRenewable": @"isSubRenewable",
                                                       @"m_nPPVPurchaseID": @"PPVPurchaseId",
                                                       @"m_paymentMethod": @"paymentMethod",
                                                       @"m_sDeviceUDID": @"dviceUDID",
                                                       @"m_sDeviceName": @"deviceName"
                                                       }];
}

- (NSDate*) endDate {
    if (!_endDate) {
        _endDate = [self dateFromString:_stringEndDate];
    }
    return _endDate;
}

- (NSDate*) lastViewDate {
    if (!_lastViewDate) {
        _lastViewDate = [self dateFromString:_stringLastViewDate];
    }
    return _lastViewDate;
}

- (NSDate*) purchaseDate {
    if (!_purchaseDate) {
        _purchaseDate = [self dateFromString:_stringPurchaseDate];
    }
    return _purchaseDate;
}

- (NSDate*) currentDate {
    if (!_currentDate) {
        _currentDate = [self dateFromString:_stringCurrentDate];
    }
    return _currentDate;
}


- (NSDate*) dateFromString:(NSString*) string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = _P_dateFormat;
    NSString *dateString = [NSString stringWithString:string];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSDate *date = [formatter dateFromString:dateString];
    if (!date) {
        formatter.dateFormat = _P_dateFormat2;
        date = [formatter dateFromString:dateString];
    }
    
    if (date) {
        NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
        date = [NSDate dateWithTimeInterval:[destinationTimeZone secondsFromGMT] sinceDate:date];
    }
    return date;
}

@end
