//
//  AdProvider.m
//  TvinciSDK
//
//  Created by iosdev1 on 7/1/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "AdProvider.h"
#import "SmartLogs.h"
#import "NSDictionary+NSNullAvoidance.h"

@implementation AdProvider

-(id)initWithAdType:(AdType)type
{
    if (self = [super init])
    {
        self.adType = type;
    }
    return self;
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    // TVLogDebug(@"[AdProvider][dictionary] AdProvider dictionary %@ To adType: %d",dictionary,_adType);
    self.ID = [[dictionary objectOrNilForKey:@"ID"]integerValue];
    self.name = [dictionary objectOrNilForKey:@"Name"];
}

@end
