//
//  TVItemList.m
//  TvinciSDK
//
//  Created by Israel Berezin on 1/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVTypedList.h"
#import "APSCoreDataStore.h"

NSString const * TVItemListContentType = @"ContentType";
NSString const * TVItemListContentMedia = @"Content";

@implementation TVTypedList

-(void)dealloc
{


}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.itemsListType =(TVTypedListType) [[dictionary objectForKey:TVItemListContentType] integerValue];
    NSArray *temp = [[dictionary objectForKey:TVItemListContentMedia] arrayByRemovingNSNulls];
    if (self.itemsListType == TVListMediaItem)
    {
        [self buildMediaItemsList:temp];
    }
    else
    {
         [self buildProgramItemsList:temp];
    }
}

-(void)buildMediaItemsList:(NSArray *)temp
{
    NSMutableArray * tempArray = [NSMutableArray array];
    for (NSDictionary *dic in temp)
    {
        TVMediaItem * item = [[TVMediaItem alloc]initWithDictionary:dic];
        if (item != nil)
        {
            [tempArray addObject:item];
        }

    }
    self.mediaArray = [NSArray arrayWithArray:tempArray];
}

-(void)buildProgramItemsList:(NSArray *)temp
{
    NSMutableArray * tempArray = [NSMutableArray array];

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"APSTVProgram" inManagedObjectContext:[APSCoreDataStore newPrivateQueueContext]];
    for (NSDictionary *dic in temp)
    {

        APSTVProgram * program = (APSTVProgram *)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        [program setdictionary:dic];
        if (program != nil)
        {
            [tempArray addObject:program];
        }

    }
    self.mediaArray = [NSArray arrayWithArray:tempArray];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"itemsListType = %d \n\n mediaArray:\n\n%@",self.itemsListType,self.mediaArray];
}
@end
