//
//  TVGenericRulesResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVGenericRulesResponse.h"
#import "TVGenericRule.h"

@implementation TVGenericRulesResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];

    NSArray * rulesDicaionries = [dictionary objectForKey:@"rules"];
    NSMutableArray * rulesArray = [NSMutableArray array];
    for (NSDictionary * ruleDictionary in rulesDicaionries)
    {
        TVGenericRule * rule = [[TVGenericRule alloc] initWithDictionary:ruleDictionary];
        [rulesArray addObject:rule];
    }

    self.arrayRules = rulesArray;
}
@end
