//
//  TVIPNOperator.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@class TVDisplay;

@interface TVIPNOperator : BaseModelObject

@property (strong, nonatomic) NSString * operatorUsername;
@property (strong, nonatomic) NSString * operatorPassword;
@property (strong, nonatomic) NSString * operatorMenuId;


@property (assign, nonatomic) NSInteger  operatorID;
@property (assign, nonatomic) NSInteger  operatorCoGuid;

@property (assign, nonatomic) NSInteger  operatorSubGroupID;

@property (strong, nonatomic) NSString * operatorName;
@property (assign, nonatomic) NSInteger  operatorType;
@property (strong, nonatomic) NSURL * operatorLoginURL;
@property (strong, nonatomic) NSURL * operatorLogoutURL;
@property (strong, nonatomic) NSString * operatorsScopes;

@property (strong, nonatomic) NSString * operatorAboutUs;
@property (strong, nonatomic) NSString * operatorConrtactUs;
@property (strong, nonatomic) NSString * colorCode;
@property (strong, nonatomic) NSURL * picURL;

@property (strong, nonatomic) TVDisplay * operatorsDisplay;








@end
