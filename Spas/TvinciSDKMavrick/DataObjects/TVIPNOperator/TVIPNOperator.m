//
//  TVIPNOperator.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//


#import "TVIPNOperator.h"
#import "TVDisplay.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVConfigurationManager.h"
#import "TVinciUtils.h"

@implementation TVIPNOperator

NSString *const IDKeyName = @"ID";
NSString *const SubGroupIDKeyName = @"Name";
NSString *const SubNameKeyName = @"SubGroupID";
NSString *const TypeKeyName = @"Type";
NSString *const LoginUrlKeyName = @"LoginUrl";
NSString *const ScopesKeyName = @"Scopes";
NSString *const DisplayParamsKeyName = @"DisplayParams";
NSString *const LogoutURLKeyName = @"LogoutURL";
NSString *const GroupUserNameKeyName = @"GroupUserName";
NSString *const GroupPasswordKeyName = @"GroupPassword";
NSString *const DataKeyName = @"UIData";
NSString *const PicIDKeyName = @"picURL";
NSString *const ColorCodeKeyName = @"ColorCode";
NSString *const ContactUsKeyName = @"ContactUs";
NSString *const AboutUsKeyName = @"AboutUs";
NSString *const CoGuidKeyName = @"CoGuid";
NSString *const GroupsOperatorsMenus = @"Groups_operators_menus";



-(void)dealloc
{

}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSNumber * operatorID = [dictionary objectForKey:IDKeyName];
    NSNumber * coGuid = [dictionary objectForKey:CoGuidKeyName];
    NSNumber * operatorSubGroupID = [dictionary objectForKey:SubGroupIDKeyName];
    NSString * operatorName = [dictionary objectForKey:SubNameKeyName];
    NSNumber * operatorType = [dictionary objectForKey:TypeKeyName];
    NSString * loginURL = [dictionary objectForKey:LoginUrlKeyName];
    NSString * scopes = [dictionary objectForKey:ScopesKeyName];
    NSDictionary * displayParametes = [dictionary objectForKey:DisplayParamsKeyName];
    NSString * logoutURL = [dictionary objectForKey:LogoutURLKeyName];

    NSString * userName = [dictionary objectForKey:GroupUserNameKeyName];
    NSString * password = [dictionary objectForKey:GroupPasswordKeyName];
    
    NSString * url =[[dictionary objectForKey:DataKeyName] objectForKey:PicIDKeyName];
    NSURL * pic_URL =  [NSURL URLWithString:url];
    NSString * colorCode = [[dictionary objectForKey:DataKeyName] objectForKey:ColorCodeKeyName];
    
    NSString * contactUs = [dictionary objectForKey:ContactUsKeyName];
    NSString * aboutUs = [dictionary objectForKey:AboutUsKeyName];
    
    NSArray * menusIds = [dictionary objectForKey:GroupsOperatorsMenus];
//    NSDictionary * menuIDByPlatform =  parsArrayOfPairsKeyValueToOneDictionary(menusIds);
//    
//    for (NSString * platfromID in menuIDByPlatform.allKeys)
//    {
//        if ([platfromID isEqualToString:[[TVConfigurationManager sharedTVConfigurationManager] menuPlatformID]])
//        {
//            self.operatorMenuId = [menuIDByPlatform objectForKey:platfromID];
//        }
//    }
    
    // take only first object
    self.operatorMenuId  = [[menusIds firstObject] objectForKey:@"value"];
    
    self.operatorCoGuid = [coGuid integerValue];
    self.picURL = pic_URL;
    self.colorCode = colorCode;
    self.operatorID = [operatorID integerValue];
    self.operatorSubGroupID = [operatorSubGroupID integerValue];
    self.operatorName = operatorName;
    self.operatorType = [operatorType integerValue];
    self.operatorLoginURL = [NSURL URLWithString:loginURL];
    self.operatorLogoutURL = [NSURL URLWithString:logoutURL];
    self.operatorsScopes = scopes;
    
    NSMutableDictionary * allDisplayParametes = [NSMutableDictionary dictionaryWithDictionary:displayParametes];
    [allDisplayParametes setObjectOrNil:operatorID forKey:@"operatorID"];
    [allDisplayParametes setObjectOrNil:[dictionary objectForKey:DataKeyName] forKey:DataKeyName];
    self.operatorsDisplay = [[TVDisplay alloc] initWithDictionary:allDisplayParametes];
    
    self.operatorUsername = userName;
    self.operatorPassword = password;
    
    self.operatorAboutUs = aboutUs;
    self.operatorConrtactUs =contactUs;
    
//#warning Hardcoded
//    if (self.operatorsDisplay.operatorID %2 == 0) {
//        self.operatorAboutUs = @"http://www.orange.com/en/about";
//        self.operatorConrtactUs =@"ipno1@tvinci.com";
//    }else{
//        self.operatorAboutUs = @"http://www.eutelsat.com/en/group/eutelsat-at-a-glance.html";
//        self.operatorConrtactUs =@"ipno2@tvinci.com";
//
//    }
    
}

-(NSString *) description
{
    return [NSString stringWithFormat:@"<%@ %p %ld %@ %ld %@ %@ %@",[self class],self, (long)self.operatorID,self.operatorName,(long)self.operatorType,self.operatorLoginURL,self.operatorsScopes,self.operatorsDisplay];
}


@end
