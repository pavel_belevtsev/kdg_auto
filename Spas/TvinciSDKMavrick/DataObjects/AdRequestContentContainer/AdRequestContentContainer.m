//
//  AdRequestContentContainer.m
//  TvinciSDK
//
//  Created by iosdev1 on 6/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "AdRequestContentContainer.h"
#import "AdRequestContentMetadata.h"

@implementation AdRequestContentContainer


-(id)initWithMediaItem:(TVMediaItem *)media
{
    if (self = [super init])
    {
        self.mediaItem = media;
        self.currentMidRollIndex  = 0;
        self.currentPostRoleIndex = 0;
        self.currentPreRoleIndex  = 0;
       // [self insertDemoDate];
    }
    return self;
}

/*
 [NSArray arrayWithObjects:@"test",nil]
 flags:[NSArray arrayWithObjects: nil]
 contentForm:VPContentFormShortForm
 category:@"validation"
 duration:600
 contentIdentifier:@"big_buck_bunny"];
 */

-(NSArray*)buildTag
{
    NSMutableArray * tag = [NSMutableArray array];
    
    NSString *rating = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Rating"];
    NSArray *ratingSeparated = [rating componentsSeparatedByString:@"|"];
    for (NSString * oneRating in ratingSeparated)
    {
        [tag addObject:oneRating];
    }
    
    NSString * mediaType = self.mediaItem.mediaTypeName;
    [tag addObject:mediaType];
    
    NSString * adTags = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Ad tags"];
    NSArray *adTagsSeparated = [adTags componentsSeparatedByString:@"|"];
    for (NSString * adTag in adTagsSeparated)
    {
        [tag addObject:adTag];
    }
    
    NSString * genres = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Genre"];
    NSArray *genreSeparated = [genres componentsSeparatedByString:@"|"];
    for (NSString * genre in genreSeparated)
    {
        [tag addObject:genre];
    }
    
    NSString * Adtag_2 = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Ad tag 2"];
    NSArray *Adtag_2Separated = [Adtag_2 componentsSeparatedByString:@"|"];
    for (NSString * hashtag in Adtag_2Separated)
    {
        [tag addObject:hashtag];
    }
    
    NSString * Adtag_3 = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Ad tag 3"];
    NSArray *Adtag_3Separated = [Adtag_3 componentsSeparatedByString:@"|"];
    for (NSString * hashtag in Adtag_3Separated)
    {
        [tag addObject:hashtag];
    }

    return [NSArray arrayWithArray:tag];
}

-(void)buildAdsRequestsWithFile:(TVFile *)file
{
    NSString *valuesCategory = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Ad tag 1"];
    NSArray *valuesOfCategorySeparated = [valuesCategory componentsSeparatedByString:@"|"];
    ASLog(@"valuesSeparated = %@",valuesOfCategorySeparated);
    NSString * category = [valuesOfCategorySeparated lastObject]; ;
    if (category == nil)
    {
        category = @"";
    }
    NSArray * tags = [self buildTag];
    
    NSString * provider = [self.mediaItem.advertisingParameters objectOrNilForKey:@"Provider"];
    
       // build pre roll
    if (file.preProvider != nil)
    {
        AdRequestContentMetadata * preRoll = [[AdRequestContentMetadata alloc] init];
        preRoll.tags = tags;
        preRoll.flags = [NSArray arrayWithObjects: nil];
        preRoll.adType = PreRoll;
        preRoll.contentIdentifier = self.mediaItem.name;
        preRoll.category = category;
        preRoll.duration = self.mediaItem.duration;
        preRoll.contentPartner = provider;
        preRoll.timeToShow=0.0;
        preRoll.contentForm = LongMovie;
        self.preRollsRequests = [NSArray arrayWithObjects:preRoll, nil];
        self.currentPreRoleIndex = 0;
    }
    
    // build mid roll
    if (file.breakProvider != nil && [file.breakPoints count] > 0)
    {
        NSMutableArray *temp = [NSMutableArray array];
        for (NSString * brakePoint in file.breakPoints)
        {
//            AdRequestContentMetadata * test1 = [[AdRequestContentMetadata alloc] init];
//            test1.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
//            test1.flags = [NSArray arrayWithObjects: nil];
//            test1.contentForm = 1;
//            test1.category = @"";
//            test1.contentIdentifier = @"";
//            test1.duration = self.mediaItem.duration;
//            test1.timeToShow = [brakePoint integerValue];
//            test1.adType = MidRoll;
            
            AdRequestContentMetadata * midRoll = [[AdRequestContentMetadata alloc] init];
            midRoll.adType = MidRoll;
            midRoll.contentIdentifier = self.mediaItem.name;
            midRoll.flags = [NSArray arrayWithObjects: nil];
            midRoll.contentPartner = provider;
            midRoll.category = category;
            midRoll.tags = tags;
            midRoll.duration = self.mediaItem.duration;
            midRoll.timeToShow = [brakePoint integerValue];
            midRoll.contentForm = LongMovie;
            
            [temp addObject:midRoll];
        }
        self.midRollsRequests =[NSArray arrayWithArray:temp];
    }
    
    // build post roll
    if (file.postProvider != nil)
    {
        AdRequestContentMetadata * postRoll = [[AdRequestContentMetadata alloc] init];
        postRoll.adType = PostRoll;
        postRoll.contentIdentifier = self.mediaItem.name;
        postRoll.tags = tags;
        postRoll.flags = [NSArray arrayWithObjects: nil];
        postRoll.contentPartner = provider;
        postRoll.category = category;
        postRoll.timeToShow =self.mediaItem.duration+0.1;
        postRoll.contentForm = LongMovie;
        self.postRollsRequests = [NSArray arrayWithObjects:postRoll, nil];
        self.currentPostRoleIndex = 0;
    }
}

-(void)insertDemoDate
{
    AdRequestContentMetadata * test = [[AdRequestContentMetadata alloc] init];
    
//    NSString * category = [[[self.mediaItem tags] objectForKey:TVTagCategory] firstObject];
//    NSString * genre = [[[self.mediaItem tags] objectForKey:TVTagGenre] firstObject];
//    NSString * rating = [[[self.mediaItem tags] objectForKey:TVTagAgeRating] firstObject];
//    NSString * provider =[[[self.mediaItem tags] objectForKey:TVTagProvider] firstObject];
//    test.tags = [NSArray arrayWithObjects:genre,rating,self.mediaItem.mediaTypeName,nil];
//    test.flags = [NSArray arrayWithObjects: nil];
//    test.contentForm = 2;
//    test.category = category;// @"validation";
//    test.contentIdentifier =[self.mediaItem name] ;// @"big_buck_bunny";
//    test.duration = self.mediaItem.duration;
//    test.contentPartner = provider;
//    ASLog(@"test = %@",test);
    
    test.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
    test.flags = [NSArray arrayWithObjects: nil];
    test.contentForm = 1;
    test.category = @"";
    test.contentIdentifier = @"";
    test.timeToShow = 0.0;
    test.adType = PreRoll;
    
    AdRequestContentMetadata * test1 = [[AdRequestContentMetadata alloc] init];
    test1.tags = [NSArray arrayWithObjects:@"testmultiple1",nil];
    test1.flags = [NSArray arrayWithObjects: nil];
    test1.contentForm = 1;
    test1.category = @"";
    test1.contentIdentifier = @"";
    test1.duration = self.mediaItem.duration;
    test1.timeToShow = 30.0;
    test1.adType = MidRoll;

   
    
    AdRequestContentMetadata * test2 = [[AdRequestContentMetadata alloc] init];
    test2.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
    test2.flags = [NSArray arrayWithObjects: nil];
    test2.contentForm = 1;
    test2.category = @"";
    test2.contentIdentifier = @"";
    test2.duration = self.mediaItem.duration;
    test2.timeToShow = 90.0;
    test2.adType = MidRoll;
    
    AdRequestContentMetadata * test3 = [[AdRequestContentMetadata alloc] init];
    test3.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
    test3.flags = [NSArray arrayWithObjects: nil];
    test3.contentForm = 1;
    test3.category = @"";
    test3.contentIdentifier = @"";
    test3.duration = self.mediaItem.duration;
    test3.timeToShow = 180;
    test3.adType = MidRoll;
    
    AdRequestContentMetadata * test4 = [[AdRequestContentMetadata alloc] init];
    test4.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
    test4.flags = [NSArray arrayWithObjects: nil];
    test4.contentForm = 1;
    test4.category = @"";
    test4.contentIdentifier = @"";
    test4.duration = self.mediaItem.duration;
    test4.timeToShow = 360;
    test4.adType = MidRoll;

    AdRequestContentMetadata * test5 = [[AdRequestContentMetadata alloc] init];
    test5.tags = [NSArray arrayWithObjects:@"testmultiple",nil];
    test5.flags = [NSArray arrayWithObjects: nil];
    test5.contentForm = 0;
    test5.category = @"";
    test5.contentIdentifier = @"";
    test5.duration = self.mediaItem.duration;
    test5.timeToShow = self.mediaItem.duration+0.1;
    test5.adType = PostRoll;
    
    // self.preRollsTimes = [NSArray arrayWithObject:[NSNumber numberWithDouble:test.timeToShow]];
    self.preRollsRequests = [NSArray arrayWithObjects:test, nil];
    self.currentPreRoleIndex = 0;
    
    // self.midRollsTimes = [NSArray arrayWithObjects:[NSNumber numberWithDouble:test1.timeToShow],[NSNumber numberWithDouble:test2.timeToShow], nil];
    self.midRollsRequests = [NSArray arrayWithObjects:test1,test2,test3,test4, nil];
   // self.midRollsRequests = [NSArray arrayWithObjects:test3,test4, nil];

    self.currentMidRollIndex = 0;
    
    // self.postRollsTimes = [NSArray arrayWithObject:[NSNumber numberWithDouble:test5.timeToShow]];
    self.postRollsRequests = [NSArray arrayWithObject:test5];
    self.currentPostRoleIndex = 0;
    
}


-(NSInteger)chackAndReturnAdLocateifNeedPlayMidPollwithTime:(NSInteger)time
{
    NSInteger place = 0;
    for (AdRequestContentMetadata * midRequest in self.midRollsRequests)
    {
        if   (time == midRequest.timeToShow)//((time - midRequest.timeToShow - time < 1.0) && (time - midRequest.timeToShow - time >= 0))
            //(midRequest.timeToShow >= time && midRequest.timeToShow < time+1)
        {
            return place;
        }
        place++;
    }
    return -1;
}

@end
