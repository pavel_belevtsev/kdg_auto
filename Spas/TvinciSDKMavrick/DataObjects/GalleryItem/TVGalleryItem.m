//
//  TVGalleryItem.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVGalleryItem.h"
#import "NSDictionary+NSNullAvoidance.h"

#define GalleryItemGalleryIDKey @"GalleryID"
#define GalleryItemIDKey @"ItemID"
#define GalleryItemViewTypeKey @"ViewType"
#define GalleryItemTitleKey @"Title"
#define GalleryItemTVMChannelIDKey @"TVMChannelID"
#define GalleryItemNumberOfItemsPerPageKey @"NumberOfItemsPerPage"
#define GalleryItemPictureSizeKey @"PictureSize"
#define GalleryItemIsPosterKey @"IsPoster"
#define GalleryItemNumberOfItemsKey @"NumOfItems"
#define GalleryItemMainPictureKey @"MainPic"
#define GalleryItemLinkKey @"Link"
#define GalleryItemMainDescriptionKey @"MainDescription"
#define GalleryItemCultureKey @"Culture"
#define GalleryItemMediaItemsKey @"GetGalleryItemContentResult"

@interface TVGalleryItem ()
@property (nonatomic, assign, readwrite) long galleryID;
@property (nonatomic, assign , readwrite) long itemID;
@end

@implementation TVGalleryItem
@synthesize galleryID = _galleryID;
@synthesize itemID = _itemID;
@synthesize viewType = _viewType;
@synthesize title = _title;
@synthesize TVMChannelID = _TVMChannelID;
@synthesize numberOfItemsPerPage = _numberOfItemsPerPage;
@synthesize numberOfItems = _numberOfItems;
@synthesize pictureSize = _pictureSize;
@synthesize isPoster = _isPoster;
@synthesize mainPicture = _mainPicture;
@synthesize link = _link;
@synthesize mainDescription = _mainDescription;
@synthesize culture = _culture;

-(void) dealloc
{
    self.viewType = nil;
    self.title = nil;
    self.link = nil;
    self.mainDescription = nil;
    self.culture = nil;

}

#pragma mark - Parsing
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSNumber *number = [dictionary objectOrNilForKey:GalleryItemIDKey];
    self.itemID = [number longValue];
    
    number = [dictionary objectOrNilForKey:GalleryItemGalleryIDKey];
    self.galleryID = [number longValue];
    
    number = [dictionary objectOrNilForKey:GalleryItemTVMChannelIDKey];
    self.TVMChannelID = [number longValue];
    
    number = [dictionary objectOrNilForKey:GalleryItemMainPictureKey];
    self.mainPicture = [number longValue];
    
    number = [dictionary objectOrNilForKey:GalleryItemNumberOfItemsPerPageKey];
    self.numberOfItemsPerPage = [number integerValue];
    number = [dictionary objectOrNilForKey:GalleryItemNumberOfItemsKey];
    self.numberOfItems = [number integerValue];
    
    number = [dictionary objectOrNilForKey:GalleryItemIsPosterKey];
    self.isPoster = [number boolValue];
    
    NSString *string = [dictionary objectOrNilForKey:GalleryItemPictureSizeKey];
    self.pictureSize = CGSizeFromString(string);
    string = [dictionary objectOrNilForKey:GalleryItemViewTypeKey];
    self.viewType = string;
    string = [dictionary objectOrNilForKey:GalleryItemTitleKey];
    self.title = string;
    string = [dictionary objectOrNilForKey:GalleryItemLinkKey];
    self.link = string;
    string = [dictionary objectOrNilForKey:GalleryItemMainDescriptionKey];
    self.mainDescription = string;
    string = [dictionary objectOrNilForKey:GalleryItemCultureKey];
    self.culture = string;
}

-(NSString *) description{
    return [NSString stringWithFormat:@"<[TVGallery] title: %@ itemID: %ld , galleryId: %ld , TVMChannelID: %ld , numberOfItems: %ld >",self.title,self.itemID,self.galleryID, self.TVMChannelID, (long)self.numberOfItems];
}
@end
