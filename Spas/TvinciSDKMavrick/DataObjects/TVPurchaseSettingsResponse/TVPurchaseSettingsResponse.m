//
//  TVPurchaseSettingsResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVPurchaseSettingsResponse.h"

@implementation TVPurchaseSettingsResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];

    self.purchasePermission = (TVPurchasePermission)[[dictionary objectForKey:@"type"] integerValue];
    self.level = (TVRuleDefinedLevel)[[dictionary objectForKey:@"level"] integerValue];
}
@end
