//
//  TVUserSocialAction.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/6/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//



#import "BaseModelObject.h"
#import "TVConstants.h"
#import "TVUserSocialActionDoc.h"

@interface TVUserSocialAction : BaseModelObject

@property (strong, nonatomic) NSString * userSiteGuid;
@property (assign, nonatomic) TVUserSocialActionType socialAction;
@property (assign, nonatomic) TVSocialPlatform socialPlatform ;
@property (assign, nonatomic) NSInteger  assetID;
@property (assign, nonatomic) NSInteger  rateValue;
@property (assign, nonatomic) TVAssetType assetType;
@property (strong, nonatomic) NSDate * addedDate;

-(void)convertObject:(TVUserSocialActionDoc*)convertObject;

@end
