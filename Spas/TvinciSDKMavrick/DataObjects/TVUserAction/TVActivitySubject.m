//
//  TVActivitySubject.m
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVActivitySubject.h"
#import "NSDictionary+NSNullAvoidance.h"

#define KEY_ACTOR_SITE_GUID  @"ActorSiteGuid"
#define KEY_ACTOR_PIC_URL  @"ActorPicUrl"
#define KEY_ACTOR_TVINCI_USERNAME  @"ActorTvinciUsername"
#define KEY_ACTOR_GROUP_ID  @"GroupID"
#define KEY_ACTOR_DEVICE_UDID  @"DeviceUdid"


@implementation TVActivitySubject


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{

    self.actorSiteGuid = [dictionary objectOrNilForKey:KEY_ACTOR_SITE_GUID];
    self.actorPicUrl = [dictionary objectOrNilForKey:KEY_ACTOR_PIC_URL];
    self.actorTvinciUsername = [dictionary objectOrNilForKey:KEY_ACTOR_TVINCI_USERNAME];
    self.groupID = [[dictionary objectOrNilForKey:KEY_ACTOR_GROUP_ID] integerValue];
    self.deviceUdid = [dictionary objectOrNilForKey:KEY_ACTOR_DEVICE_UDID];
}

-(NSString *)description
{
    NSMutableString * description = [NSMutableString string];
    [description appendFormat:@"actor Site Guid: %@",self.actorSiteGuid];
    [description appendFormat:@"actor PicUrl: %@",self.actorPicUrl];
    [description appendFormat:@"actor Tvinci Username: %@",self.actorTvinciUsername];
    [description appendFormat:@"group ID: %ld",(long)self.groupID];
    [description appendFormat:@"device Udid: %@",self.deviceUdid];

    return description;
}
@end


