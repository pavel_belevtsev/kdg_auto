//
//  TVActivityObject.m
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVActivityObject.h"
#import "NSDictionary+NSNullAvoidance.h"

#define KEY_ASSET_ID  @"AssetID"
#define KEY_OBJECT_ID  @"ObjectID"
#define KEY_ASSET_TYPE  @"AssetType"
#define KEY_ASSET_NAME  @"AssetName"
#define KEY_PIC_URL @"PicUrl"


@implementation TVActivityObject


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{

    self.assetID = [[dictionary objectForKey:KEY_ASSET_ID] integerValue];
    self.objectID = [[dictionary objectForKey:KEY_OBJECT_ID] integerValue];
    self.assetType = (TVAssetType)[[dictionary objectOrNilForKey:KEY_ASSET_TYPE] integerValue];
    self.assetName = [dictionary objectOrNilForKey:KEY_ASSET_NAME];
    self.picUrl = [dictionary objectOrNilForKey:KEY_PIC_URL];
}

-(NSString *)description
{
    NSMutableString * description = [NSMutableString string];
    [description appendFormat:@"Asset ID: %ld",(long)self.assetID];
    [description appendFormat:@"objectID: %ld",(long)self.objectID];
    [description appendFormat:@"assetType: %d",self.assetType];
    [description appendFormat:@"assetName: %ld",(long)self.assetName];
    [description appendFormat:@"picUrl: %@",self.picUrl];

    return description;
}
@end
