//
//  TVFullUserSocialAction.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/17/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVFullUserSocialAction.h"
#import "TVMediaItem.h"
#import "TVEPGProgram.h"
#import "TVUserSocialAction.h"

@implementation TVFullUserSocialAction


-(id) initWithUserSocialAction:(TVUserSocialAction *) userSocialAction user:(TVUser *) user asset:(id) asset
{
    self = [super init];
    if (self)
    {
        self.userSocialAction = userSocialAction;
        self.user = user;
        self.asset = asset;
        self.assetType = self.userSocialAction.assetType;
    }
    
    return self;
}


@end
