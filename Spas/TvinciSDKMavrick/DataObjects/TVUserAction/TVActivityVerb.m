//
//  TVActivityVerb.m
//  TvinciSDK
//
//  Created by Magen Yadid on 9/11/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVActivityVerb.h"
#import "NSDictionary+NSNullAvoidance.h"



#define KEY_SOCICL_ACTION_ID  @"SocialActionID"
#define KEY_ACTION_TYPE  @"ActionType"
#define KEY_ACTION_NAME  @"ActionName"
#define KEY_RATE_VALUE  @"RateValue"
#define KEY_ACTION_PROPERTIES  @"ActionProperties"


@implementation TVActivityVerb


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{


    self.socialActionID = [dictionary objectOrNilForKey:KEY_SOCICL_ACTION_ID];
    self.socialAction = (TVUserSocialActionType)((NSNumber *)[dictionary objectOrNilForKey:KEY_ACTION_TYPE]).integerValue;
    self.actionName = [dictionary objectOrNilForKey:KEY_ACTION_NAME];
    self.rateValue = [[dictionary objectOrNilForKey:KEY_RATE_VALUE] integerValue];
    self.actionProperties = [dictionary objectOrNilForKey:KEY_ACTION_PROPERTIES];

}

-(NSString *)description
{
    NSMutableString * description = [NSMutableString string];
    [description appendFormat:@"social Action ID: %@",self.socialActionID];
    [description appendFormat:@"social Action: %u",self.socialAction];
    [description appendFormat:@"action Name: %@",self.actionName];
    [description appendFormat:@"rate Value: %ld",(long)self.rateValue];
    [description appendFormat:@"action Properties: %@",self.actionProperties];

    return description;
}
@end



