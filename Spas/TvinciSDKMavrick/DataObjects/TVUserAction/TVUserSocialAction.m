//
//  TVUserSocialAction.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/6/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVUserSocialAction.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "NSDate+Format.h"

#define KEY_USER_SITE_GUID  @"m_sSiteGuid"
#define KEY_SOCIAL_ACTION  @"m_eSocialAction"
#define KEY_SOCIAL_PLATFORM  @"m_eSocialPlatform"
#define KEY_MEDIA_ID  @"nMediaID"
#define KEY_PEOGEAM_ID  @"nProgramID"
#define KEY_ASSET_TYPE @"assetType"
#define KEY_ADDED_DATE @"m_dActionDate"
#define KEY_RATE_VALUE @"nRateValue"

//2013-10-15T10:47:37Z
#define DATE_FORMAT @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";

@implementation TVUserSocialAction

-(void)dealloc
{

}


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.userSiteGuid = [dictionary objectOrNilForKey:KEY_USER_SITE_GUID];
    self.socialAction = (TVUserSocialActionType)((NSNumber *)[dictionary objectOrNilForKey:KEY_SOCIAL_ACTION]).integerValue;
    self.socialPlatform = (TVSocialPlatform)((NSNumber *)[dictionary objectOrNilForKey:KEY_SOCIAL_PLATFORM]).integerValue;
    self.assetID = [dictionary objectOrNilForKey:KEY_MEDIA_ID]?(((NSNumber *)[dictionary objectOrNilForKey:KEY_MEDIA_ID]).integerValue):(((NSNumber *)[dictionary objectOrNilForKey:KEY_PEOGEAM_ID]).integerValue);
    self.assetType = (TVAssetType)[[dictionary objectOrNilForKey:KEY_ASSET_TYPE] integerValue];
    
    NSString * dateString = [dictionary objectOrNilForKey:KEY_ADDED_DATE];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = DATE_FORMAT;
    //formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] ;
    self.addedDate = [formatter dateFromString:dateString];
    
    self.rateValue = ((NSNumber *)[dictionary objectOrNilForKey:KEY_RATE_VALUE]).integerValue;
}

-(void)convertObject:(TVUserSocialActionDoc*)convertObject{

    self.userSiteGuid = convertObject.userSiteGuid;
    self.socialAction = convertObject.activityVerb.socialAction;
    self.socialPlatform = convertObject.socialPlatform;
    self.assetID = convertObject.activityObject.assetID;
    self.assetType = convertObject.activityObject.assetType;

    NSDate *epochNSDate = convertObject.createDate;
    NSDateFormatter *dateFormatterEpoch = [[NSDateFormatter alloc] init];
    dateFormatterEpoch.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
    NSString *epochDate = [dateFormatterEpoch stringFromDate:epochNSDate];


    self.addedDate  = [dateFormatterEpoch dateFromString:epochDate];


}

-(NSString *)description
{
    NSMutableString * description = [NSMutableString string];
    [description appendFormat:@"User site Guid: %@",self.userSiteGuid];
    [description appendFormat:@"socialAction: %d",self.socialAction];
    [description appendFormat:@"socialPlatform: %d",self.socialPlatform];
    [description appendFormat:@"assetID: %ld",(long)self.assetID];
    [description appendFormat:@"assetType: %d",self.assetType];
    [description appendFormat:@"addedDate: %@",self.addedDate];
    
    return description;
}
@end
