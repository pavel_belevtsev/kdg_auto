//
//  TVUserSocialActionDoc.m
//  TvinciSDK
//
//  Created by Magen Yadid on 9/10/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVUserSocialActionDoc.h"
#import "NSDictionary+NSNullAvoidance.h"

#define KEY_MEDIA_ID @"id"
#define KEY_USER_SITE_GUID  @"DocOwnerSiteGuid"
#define KEY_SOCIAL_PLATFORM  @"SocialPlatform"
#define KEY_DOC_TYPE  @"DocType"
#define KEY_CREATE_DATE  @"CreateDate"
#define KEY_LAST_DATE  @"LastUpdate"
#define KEY_IS_ACTIVE  @"IsActive"
#define KEY_PERMITSHARING  @"PermitSharing"
#define KEY_ACTIVITT_OBJECT  @"ActivityObject"
#define KEY_ACTIVITT_SUBJECT  @"ActivitySubject"
#define KEY_ACTIVITT_VERB  @"ActivityVerb"
@implementation TVUserSocialActionDoc

-(void)dealloc
{
    
}


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{

    self.mediaId = [dictionary objectOrNilForKey:KEY_MEDIA_ID];
    self.userSiteGuid = [dictionary objectOrNilForKey:KEY_USER_SITE_GUID];
    self.socialPlatform = (TVSocialPlatform)((NSNumber *)[dictionary objectOrNilForKey:KEY_SOCIAL_PLATFORM]).integerValue;
    self.docType = [dictionary objectOrNilForKey:KEY_DOC_TYPE];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:[[dictionary objectOrNilForKey:KEY_CREATE_DATE] integerValue]];
    self.lastUpdate = [NSDate dateWithTimeIntervalSince1970:[[dictionary objectOrNilForKey:KEY_LAST_DATE] integerValue]];

    if ([[dictionary objectOrNilForKey:KEY_IS_ACTIVE] boolValue]) {
        self.isActive = YES;
    }else{
        self.isActive = NO;
    }

    if ([dictionary objectOrNilForKey:KEY_PERMITSHARING]) {
        self.permitSharing = YES;
    }else{
        self.permitSharing = NO;
    }

    self.activityObject = [[TVActivityObject alloc] initWithDictionary:[dictionary objectForKey:KEY_ACTIVITT_OBJECT]];
    self.activitySubject = [[TVActivitySubject alloc] initWithDictionary:[dictionary objectForKey:KEY_ACTIVITT_SUBJECT]];
    self.activityVerb = [[TVActivityVerb alloc] initWithDictionary:[dictionary objectForKey:KEY_ACTIVITT_VERB]];
}

-(NSString *)description
{
    NSMutableString * description = [NSMutableString string];
    [description appendFormat:@"User site Guid: %@",self.userSiteGuid];
    [description appendFormat:@"MEDIA ID: %@",self.mediaId];
    [description appendFormat:@"socialPlatform: %d",self.socialPlatform];
    [description appendFormat:@"DOC TYPE: %@",self.docType];
    [description appendFormat:@"CREATE DATE: %@",self.createDate];
    [description appendFormat:@"LAST DATE: %@",self.lastUpdate];
    [description appendFormat:@"PERMITSHARING: %d",self.permitSharing];
    return description;
}

@end