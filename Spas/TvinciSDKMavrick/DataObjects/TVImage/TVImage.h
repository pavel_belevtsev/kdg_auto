//
//  TVImage.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/1/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"


typedef enum  {
    tvImageDirection_Portrait = 0,
    tvImageDirection_Landscape,
}tvImageDirection;


@interface TVImage : BaseModelObject

@property(readwrite, assign) CGSize imageSize;
@property(nonatomic, strong) NSURL *imageUrl;
@property(nonatomic, assign) tvImageDirection imageDirection;
@property(nonatomic, assign, getter = ratio) double ratio;

@end
