//
//  TVImage.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/1/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVImage.h"

@implementation TVImage

@synthesize imageSize = _imageSize;
@synthesize imageUrl = _imageUrl;
@synthesize ratio = _ratio;
#pragma mark - life cycle

- (void)dealloc
{

}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary{
    
    NSString *picSizeStr = [dictionary objectForKey:@"PicSize"] ? [dictionary objectForKey:@"PicSize"] : ([dictionary objectForKey:@"Size"] ? [dictionary objectForKey:@"Size"] : nil);
    NSArray *widthHeightArr = [picSizeStr componentsSeparatedByString:@"X"];
    
    self.imageSize = CGSizeMake([[widthHeightArr objectAtIndex:0] floatValue], [[widthHeightArr objectAtIndex:1] floatValue]);
    
    self.imageUrl = [NSURL URLWithString:[dictionary objectForKey:@"URL"]];

    self.imageDirection = tvImageDirection_Portrait;
    if (self.imageSize.width > self.imageSize.height) {
        self.imageDirection = tvImageDirection_Landscape;
    }
}

-(NSString *) description{
    return [NSString stringWithFormat:@"<%@ %p size: (%f,%f) URL: %@>",[self class], self, _imageSize.width, _imageSize.height, _imageUrl.absoluteString];
}

- (double)ratio{
    double num = self.imageSize.width / self.imageSize.height;
    return num;
}


@end
