//
//  TVLicencedLink.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVLicencedLink.h"

@implementation TVLicencedLink


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.mainURL = [NSURL URLWithString:[dictionary objectOrNilForKey:@"main_url"]];
    self.alternativeURL = [NSURL URLWithString:[dictionary objectOrNilForKey:@"alternate_url"]];
}
@end
