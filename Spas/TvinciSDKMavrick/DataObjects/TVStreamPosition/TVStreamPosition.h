//
//  TVStreamPosition.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import "TVConstants.h"





extern NSString *const TVItemPrice_nMediaFileIDKey ;


@interface TVStreamPosition : BaseModelObject

@property (assign, nonatomic) TVUserType userType;
@property (assign, nonatomic) NSTimeInterval location;
@property (strong, nonatomic) NSString * userID;

@end
