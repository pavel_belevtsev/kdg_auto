//
//  TVStreamPosition.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVStreamPosition.h"


NSString *const TVStreamPosition_UserType  = @"m_eUserType";
NSString *const TVStreamPosition_Location  = @"m_nLocation";
NSString *const TVStreamPosition_UserID  = @"m_nUserID";




@implementation TVStreamPosition


//{
//    "m_lPositions": [
//                     {
//                         "m_eUserType": 1,
//                         "m_nLocation": 50,
//                         "m_nUserID": 425368
//                     }
//                     ],
//    "m_sDescription": null,
//    "m_sStatus": "OK"
//}


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.userType = (TVUserType)[[dictionary objectForKey:TVStreamPosition_UserType] intValue];
    self.location = [[dictionary objectForKey:TVStreamPosition_Location] intValue];
    if ([dictionary objectForKey:TVStreamPosition_UserID])
    {
        self.userID =  [NSString stringWithFormat:@"%@",[dictionary objectForKey:TVStreamPosition_UserID]];
    }
}
@end
