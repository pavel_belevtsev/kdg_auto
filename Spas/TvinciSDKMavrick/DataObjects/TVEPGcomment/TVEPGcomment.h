//
//  TVEPGcomment.h
//  TvinciSDK
//
//  Created by Israel Berezin on 12/24/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVEPGcomment : BaseModelObject

@property (strong, nonatomic) NSString * author;
@property (strong, nonatomic) NSString * header;
@property (strong, nonatomic) NSString * content;
@property (strong, nonatomic) NSDate * addedDate;
@property (strong, nonatomic) NSString * languageName;
@property (assign, nonatomic) NSInteger assetId;
@property (assign, nonatomic) NSInteger languageId;
@property (strong, nonatomic) NSString * userpicUrl;

@end
