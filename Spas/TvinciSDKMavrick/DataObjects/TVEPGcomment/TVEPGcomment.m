//
//  TVEPGcomment.m
//  TvinciSDK
//
//  Created by Israel Berezin on 12/24/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVEPGcomment.h"

#define EPG_KEY_WRITER @"Writer"
#define EPG_KEY_HEADER @"Header"
#define EPG_KEY_CONTENT @"ContentText"
#define EPG_KEY_ADDED_DATE @"CreateDate"
#define EPG_KEY_LANGUAGE @"Language"
#define EPG_KEY_LANGUAGE_NAME @"LanguageName"
#define EPG_KEY_ID @"ID"
#define KEY_USER_PICTURE @"UserPicURL"

#define EPG_DATE_FORMAT1 @"MM/dd/yyyy hh:mm:ss a"
#define EPG_DATE_FORMAT @"yyyy-MM-dd HH:mm:ss"

@implementation TVEPGcomment


-(void)dealloc
{

}

/*
 @property (strong, nonatomic) NSString * author;
 @property (strong, nonatomic) NSString * header;
 @property (strong, nonatomic) NSString * content;
 @property (strong, nonatomic) NSDate * addedDate;
 @property (strong, nonatomic) NSString * languageName;
 @property (assign, nonatomic) NSInteger assetId;
 @property (assign, nonatomic) NSInteger languageId;
 */

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.author = [dictionary objectOrNilForKey:EPG_KEY_WRITER];
    self.header = [dictionary objectOrNilForKey:EPG_KEY_HEADER];
    self.content = [dictionary objectOrNilForKey:EPG_KEY_CONTENT];
    self.languageName = [dictionary objectOrNilForKey:EPG_KEY_LANGUAGE_NAME];
    self.userpicUrl = [dictionary objectOrNilForKey:KEY_USER_PICTURE];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = EPG_DATE_FORMAT;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    
    NSString * dateString = [dictionary objectOrNilForKey:EPG_KEY_ADDED_DATE];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    self.addedDate = [formatter dateFromString:dateString];
    
    self.assetId = [[dictionary objectOrNilForKey:EPG_KEY_ID] integerValue];
    self.languageId = [[dictionary objectOrNilForKey:EPG_KEY_LANGUAGE] integerValue];
    

    
}


-(NSString *) description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"EPG Comment :  "];
    [description appendFormat:@" Author: %@, ",self.author];
    [description appendFormat:@" Header: %@, ",self.header];
    [description appendFormat:@" Content: %@, ",self.content];
    [description appendFormat:@" Added date: %@, ",self.addedDate];
    [description appendFormat:@" Asset Id: %@, ",[NSString stringWithFormat:@"%ld",(long)self.assetId]];
    [description appendFormat:@" Language Id : %@, ",[NSString stringWithFormat:@"%ld",(long)self.languageId]];
    [description appendFormat:@" Language Name: %@, ",self.languageName];
    return description;
}

@end
