//
//  TVItemPrice.m
//  TvinciSDK
//
//  Created by Tarek Issa on 11/27/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVItemsPrices.h"
#import "TVSingleItemPrice.h"


NSString *const TVItemPrice_nMediaFileIDKey  = @"m_nMediaFileID";
NSString *const TVItemPrice_oItemPricesKey   = @"m_oItemPrices";
NSString *const TVItemPrice_sProductCode  = @"m_sProductCode";


@implementation TVItemsPrices
@synthesize fileID = _fileID;
@synthesize productCode = _productCode;
@synthesize itemsPrices = _itemsPrices;

#pragma mark - Memory
-(void) dealloc
{
    _productCode = nil;
    _itemsPrices = nil;

  }

-(NSString *) description{
    NSString* str = [[NSString alloc] init];
    for (TVSingleItemPrice* singleItemPrice in _itemsPrices) {
        str = [str stringByAppendingString:[singleItemPrice description]];
    }
    return [NSString stringWithFormat:@"[FileID: %ld  Prices:\n %@]", (long)_fileID, str];
}

#pragma mark - Initializations

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    TVLogDebug(@"[TVItemsPrices][Attributes] %@",dictionary);////////
    
    _fileID         = [[dictionary objectOrNilForKey:TVItemPrice_nMediaFileIDKey] integerValue];
    _productCode    = [dictionary objectOrNilForKey:TVItemPrice_sProductCode];
    
    NSArray* itemsPricesArr;
    itemsPricesArr  = [dictionary objectOrNilForKey:TVItemPrice_oItemPricesKey];
    
    NSMutableArray* itemsPricesMutArr = [[NSMutableArray alloc] initWithCapacity:itemsPricesArr.count];
    for (NSDictionary* priceDic in itemsPricesArr) {
        TVSingleItemPrice* singleItemPrice = [[TVSingleItemPrice alloc] initWithDictionary:priceDic];
        [itemsPricesMutArr addObject:singleItemPrice];
    }
    _itemsPrices = [[NSArray alloc] initWithArray:itemsPricesMutArr];
}

@end
