//
//  TVSingleItemPrice.h
//  TvinciSDK
//
//  Created by Tarek Issa on 11/27/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface PriceObj : BaseModelObject {
    
}

@property (nonatomic, assign) float price;
@property (nonatomic, assign) NSInteger CurrencyID;
@property (nonatomic, assign) NSString* CurrencySign;
@property (nonatomic, assign) NSString* CurrencyCD2;
@property (nonatomic, assign) NSString* CurrencyCD3;

@end





@interface TVSingleItemPrice : BaseModelObject {
    
}
@property (nonatomic, assign) TVPriceType priceReason;
@property (nonatomic, assign) NSInteger couponStatus;
@property (nonatomic, assign) BOOL SubscriptionOnly;
@property (nonatomic, strong) NSString* PPVmoduleCode;
@property (nonatomic, strong) NSString* relevantSub;
@property (nonatomic, strong) NSString* relevantPP;
@property (nonatomic, strong) PriceObj* price;
@property (nonatomic, strong) PriceObj* fullPrice;
@property (nonatomic, strong) NSString* ppvLanguageCode3;
@property (nonatomic, strong) NSString* ppvValue;
@property (nonatomic, strong) NSString* firstDeviceName;

@end

extern NSString *const TVItemPrice_sPPVModuleCodeKey;
extern NSString *const TVItemPrice_bSubscriptionOnlyKey;
extern NSString *const TVItemPrice_oPriceKey;
extern NSString *const TVItemPrice_dPriceKey;
extern NSString *const TVItemPrice_oCurrencyKey;
extern NSString *const TVItemPrice_oFullPriceKey;
extern NSString *const TVItemPrice_PriceReasonKey;
extern NSString *const TVItemPrice_relevantSubKey;
extern NSString *const TVItemPrice_relevantPPKey;
extern NSString *const TVItemPrice_oPPVDescriptionKey;
extern NSString *const TVItemPrice_couponStatusKey;
extern NSString *const TVItemPrice_CurrencyKey;
extern NSString *const TVItemPrice_CurrencyCD3Key;
extern NSString *const TVItemPrice_CurrencyCD2Key;
extern NSString *const TVItemPrice_CurrencySignKey;
extern NSString *const TVItemPrice_CurrencyIDKey;
extern NSString *const TVItemPrice_ppvLanguageKey;
extern NSString *const TVItemPrice_ppvValueKey;


