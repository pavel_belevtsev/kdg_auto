//
//  APSTVProgram+Additions.m
//  Spas
//
//  Created by Rivka Peleg on 9/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTVProgram+Additions.h"
#import "APSTVProgram.h"
#import <TvinciSDK/TVDeviceScreenUtils.h>
#import "TVEPGProgram.h"
#import "NSDictionary+NSNullAvoidance.h"

@implementation APSTVProgram (Additions)


- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.insertionDate = [NSDate date];
}




static NSDateFormatter * formatter = nil;

+(void) setprogramDateFormatter:(NSDateFormatter *) newFormmater
{
    if (formatter != newFormmater)
    {
        formatter = newFormmater;
    }
}


+(NSDateFormatter *) programDateFormatter
{
    if (formatter == nil)
    {
        formatter  = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    return  formatter;
    
    
}

+ (APSTVProgram *) programWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    
    APSTVProgram *program = nil;
    program = [NSEntityDescription insertNewObjectForEntityForName:@"APSTVProgram"
                                            inManagedObjectContext:context];
    [program setdictionary:dictionary];
    return  program;
    
    
}


- ( void ) setdictionary:(NSDictionary *)dictionary
{
    self.name = [dictionary objectOrNilForKey:TVEPGProgramNameKey];
    self.epgIdentifier = [dictionary objectOrNilForKey:TVEPGProgramEPGIdentifierKey];
    self.epgChannelID = [dictionary objectOrNilForKey:TVEPGProgramEPGChannelIDKey];
    self.programDescription = [dictionary objectOrNilForKey:TVEPGProgramDescriptionKey];
    self.pictureURL = [dictionary objectOrNilForKey:TVEPGProgramPicURLKey];
    self.status = [dictionary objectOrNilForKey:TVEPGProgramStatusKey];
    self.isActive = [dictionary objectOrNilForKey:TVEPGProgramIsActiveKey];
    self.groupID = [dictionary objectOrNilForKey:TVEPGProgramGroupIDKey];
    self.updaterID = [dictionary objectOrNilForKey:TVEPGProgramUpdaterIDKey];
    self.mediaID = [dictionary objectOrNilForKey:TVEPGProgramMediaIDKey];
    self.epgTag = [dictionary objectOrNilForKey:TVEPGProgramEPGTagKey];
    self.epgId = [NSString stringWithFormat:@"%@",[dictionary objectOrNilForKey:TVEPGProgramEPGIDKey]];
    self.likeConter = [NSNumber numberWithInteger:[[dictionary objectOrNilForKey:TVEPGProgramEPGLikeCounterKey] integerValue]];
    
    NSString * startDateString = [dictionary objectOrNilForKey:TVEPGProgramStartDateKey];
    NSString * endDateString = [dictionary objectOrNilForKey:TVEPGProgramEndDateKey];
    NSString * updateDateString = [dictionary objectOrNilForKey:TVEPGProgramUpdateDateKey];
    NSString * publishDateString = [dictionary objectOrNilForKey:TVEPGProgramPublishDateKey];
    NSString * createDateStirng = [dictionary objectOrNilForKey:TVEPGProgramCreateDateKey];
    
    self.startDateTime = [[APSTVProgram programDateFormatter] dateFromString:startDateString];
    self.endDateTime = [[APSTVProgram programDateFormatter] dateFromString:endDateString];
    self.updateDate = [[APSTVProgram programDateFormatter] dateFromString:updateDateString];
    self.publishDate = [[APSTVProgram programDateFormatter] dateFromString:publishDateString];
    self.createDate = [[APSTVProgram programDateFormatter] dateFromString:createDateStirng];
    
    self.metaData = [self parseMetas:[dictionary objectOrNilForKey:EpgMediaItemMetaDataKey]];
    self.tags = [self parseTags:[dictionary objectOrNilForKey:EpgMediaItemTagsKey]];
    
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"program = %@ S.T.= %@  E.T. = %@",self.name,self.startDateTime,self.endDateTime];
}


-(NSDictionary *) parseTags:(NSArray *)theTags
{
    
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in theTags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value!= nil)
            {
                NSArray * tags =  [tagsParsed objectForKey:title];
                if (tags == nil)
                {
                    tags = [NSArray arrayWithObject:value];
                }
                else
                {
                    tags = [tags arrayByAddingObject:value];
                }
                [tagsParsed setObject:tags forKey:title];

            }
         }
    }
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}

-(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    // TVLogDebug(@"%@",metaParsed);
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}


-(NSDictionary *)theParsedMetaData
{
    return self.metaData;
}

-(NSString *) decodedValueForKey:(NSString *) key
{
    NSString * returnValue = [[self theParsedMetaData] objectForKey:key];
    if([returnValue length]<=0 )
        return nil;
    
    return [returnValue stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
}


-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size
{
    CGSize retinaSize;
    BOOL isRetina = [TVDeviceScreenUtils isRetina];
    retinaSize.width = isRetina?size.width*2.0:size.width;
    retinaSize.height = isRetina? size.height*2.0:size.height;
    return [self pictureURLForSize:retinaSize];
}

-(NSURL *) pictureURLForSize : (CGSize ) size
{
    NSURL *toReturn = nil;
    
    NSRegularExpression *pictureSizeRegex = [NSRegularExpression regularExpressionWithPattern:@"(\\d+X\\d+)|(full)" options:0 error:NULL];
    
    if (pictureSizeRegex != nil)
    {
        NSString *absoluteString = self.pictureURL;
        if (absoluteString != nil)
        {
            int width =size.width;
            int  height = size.height;
            NSString * name = [NSString stringWithFormat:@"%dX%d",width,height];
            
            NSString *replacement = [pictureSizeRegex stringByReplacingMatchesInString:absoluteString options:0 range:NSMakeRange(0, absoluteString.length) withTemplate:name];
            
            toReturn = [NSURL URLWithString:replacement];
            
        }
    }
    
    return toReturn;
}


- (NSComparisonResult)compare:(APSTVProgram *)other
{
    return [self.epgId isEqualToString:other.epgId];
}





+(NSDate *) dateFromProgramsDateString:(NSString *) dateString
{
        return [[APSTVProgram programDateFormatter] dateFromString:dateString];
}







-(TVEPGProgram *) EPGProgram
{
    TVEPGProgram * epgProgram = [[TVEPGProgram alloc] init];
    
    epgProgram.createDate = self.createDate;
    epgProgram.endDateTime = self.endDateTime;
    epgProgram.EPGChannelID = self.epgChannelID;
    epgProgram.EPGId = self.epgId;
    epgProgram.EPGIdentifier = self.epgIdentifier;
    epgProgram.EPGTag = self.epgTag;
    epgProgram.groupID = self.groupID;
    epgProgram.isActive = self.isActive;
    epgProgram.likeConter = [self.likeConter integerValue];
    epgProgram.mediaID = self.mediaID;
    epgProgram.metaData = self.metaData;
    epgProgram.name = self.name;
    epgProgram.pictureURL = [NSURL URLWithString:self.pictureURL];
    epgProgram.description = self.programDescription;
    epgProgram.publishDate  = self.publishDate;
    epgProgram.startDateTime = self.startDateTime;
    epgProgram.status = self.status;
    epgProgram.tags = self.tags;
    epgProgram.updateDate = self.updateDate;
    epgProgram.updaterID = self.updaterID;
    
    
    return epgProgram;
}
@end
