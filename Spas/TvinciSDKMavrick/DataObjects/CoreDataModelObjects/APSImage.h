//
//  APSImage.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSMediaItem;

@interface APSImage : APSBaseManagedObject

@property (nonatomic, strong) NSString * size;
@property (nonatomic, strong) NSString * imageURL;
@property (nonatomic, strong) NSNumber * direction;
@property (nonatomic, strong) NSNumber * ratio;
@property (nonatomic, strong) APSMediaItem *mediaItem;

+ (APSImage *) imageWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;

@end
