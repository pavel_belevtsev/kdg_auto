//
//  APSMediaItem.h
//  Spas
//
//  Created by Rivka S. Peleg on 8/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TVMediaItem.h"
#import "APSBaseManagedObject.h"

@class APSImage;

@interface APSMediaItem : APSBaseManagedObject

@property (nonatomic, strong) NSDate * catalogStartDate;
@property (nonatomic, strong) NSDate * creationDate;
@property (nonatomic, strong) NSNumber * duration;
@property (nonatomic, strong) id externalIDs;
@property (nonatomic, strong) NSString * fileID;
@property (nonatomic, strong) id files;
@property (nonatomic, strong) NSNumber * likeCounter;
@property (nonatomic, strong) NSString * mediaDescription;
@property (nonatomic, strong) NSString * mediaID;
@property (nonatomic, strong) NSString * mediaName;
@property (nonatomic, strong) NSString * mediaTypeID;
@property (nonatomic, strong) NSString * mediaTypeName;
@property (nonatomic, strong) NSString * mediaWebLink;
@property (nonatomic, strong) id metaData;
@property (nonatomic, strong) NSString * pictureURL;
@property (nonatomic, strong) NSNumber * rating;
@property (nonatomic, strong) NSDate * startDate;
@property (nonatomic, strong) id tags;
@property (nonatomic, strong) NSNumber * totalItems;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSNumber * viewCounter;
@property (nonatomic, strong) id plainJson;
@property (nonatomic, strong) NSSet *pictures;
@end

@interface APSMediaItem (CoreDataGeneratedAccessors)

+ (APSMediaItem *) mediaWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;
- (TVMediaItem *) tvMediaItem;

- (void)addPicturesObject:(APSImage *)value;
- (void)removePicturesObject:(APSImage *)value;
- (void)addPictures:(NSSet *)values;
- (void)removePictures:(NSSet *)values;

@end
