//
//  APSTVProgram.m
//  Spas
//
//  Created by Rivka Peleg on 9/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTVProgram.h"
#import "APSTVChannel.h"


@implementation APSTVProgram

@dynamic createDate;
@dynamic endDateTime;
@dynamic epgChannelID;
@dynamic epgId;
@dynamic epgIdentifier;
@dynamic epgTag;
@dynamic groupID;
@dynamic isActive;
@dynamic likeConter;
@dynamic mediaID;
@dynamic metaData;
@dynamic name;
@dynamic pictureURL;
@dynamic programDescription;
@dynamic publishDate;
@dynamic startDateTime;
@dynamic status;
@dynamic tags;
@dynamic updateDate;
@dynamic updaterID;
@dynamic insertionDate;
@dynamic channel;



-(void)copy:(APSTVProgram *)sender
{
    // We'll ignore the zone for now
    self.createDate = [sender.createDate copy];
    self.endDateTime = [sender.endDateTime copy];
    self.epgChannelID = [sender.epgChannelID copy];
    self.epgId = [sender.epgId copy];
    self.epgIdentifier = [sender.epgIdentifier copy];
    self.epgTag = [sender.epgTag copy];
    self.groupID = [sender.groupID copy];
    self.isActive = [sender.isActive copy];
    self.likeConter = [sender.likeConter copy];
    self.mediaID = [sender.mediaID copy];
    self.metaData = [sender.metaData copy];
    self.name = [sender.name copy];
    self.pictureURL = [sender.pictureURL copy];
    self.programDescription = [sender.programDescription copy];
    self.publishDate = [sender.publishDate copy];
    self.startDateTime = [sender.startDateTime copy];
    self.status = [sender.status copy];
    self.tags = [sender.tags copy];
    self.updateDate = [sender.updateDate copy];
    self.updaterID = [sender.updaterID copy];
    self.insertionDate = [sender.insertionDate copy];
    self.insertionDate = [sender.insertionDate copy];
    self.channel = [sender.channel copy];
}

@end
