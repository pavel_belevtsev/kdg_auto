//
//  APSTVChannel.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/15/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSTVChannel.h"
#import "APSTVProgram.h"
#import "TVEPGChannel.h"
#import "NSDictionary+NSNullAvoidance.h"


@implementation APSTVChannel

@dynamic channelID;
@dynamic name;
@dynamic pictureURL;
@dynamic channelDescription;
@dynamic orderNumber;
@dynamic isActive;
@dynamic groupID;
@dynamic editorRemarks;
@dynamic status;
@dynamic updaterID;
@dynamic createDate;
@dynamic publishDate;
@dynamic mediaID;
@dynamic epgChannelID;
@dynamic programs;


static NSDateFormatter *formatter = nil;

+(NSDateFormatter *) channelDateFormatter
{
    if (formatter == nil)
    {
        formatter  = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"M/d/yyyy h:mm:ss a";
    }
    
    return  formatter;
    
    
}

+ (APSTVChannel *) channelWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    APSTVChannel *channel = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    request.entity = [NSEntityDescription entityForName:@"APSTVChannel" inManagedObjectContext:context];
    NSString * propertyKey = NSStringFromProperty(epgChannelID);
    NSString * epgChannelID = [dictionary objectForKey:EPGChannelEPGChannelIDKey];
    
    request.predicate = [NSPredicate predicateWithFormat:@"%K = %@", propertyKey,epgChannelID];
    NSError *executeFetchError = nil;
    channel = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        TVLogDebug(@"[%@, %@] error looking up user with id: %i with error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [epgChannelID intValue], [executeFetchError localizedDescription]);
    } else if (!channel) {
        channel = [NSEntityDescription insertNewObjectForEntityForName:@"APSTVChannel"
                                                inManagedObjectContext:context];
    }
    
    channel.channelID = [dictionary objectOrNilForKey:EPGChannelChannelIDKey];
    channel.epgChannelID = [dictionary objectOrNilForKey:EPGChannelEPGChannelIDKey];
    channel.name = [dictionary objectOrNilForKey:EPGChannelNameKey];
    channel.pictureURL = [dictionary objectOrNilForKey:EPGChannelPicURLKey];
    channel.channelDescription = [dictionary objectOrNilForKey:EPGChannelDescriptionKey];
    channel.orderNumber = [dictionary objectOrNilForKey:EPGChannelOrderNumKey];
    channel.isActive = [dictionary objectOrNilForKey:EPGChannelIsActiveKey];
    channel.groupID = [dictionary objectOrNilForKey:EPGChannelGroupIDKey];
    channel.editorRemarks = [dictionary objectOrNilForKey:EPGChannelEditorRemarksKey];
    channel.status = [dictionary objectOrNilForKey:EPGChannelStatusKey];
    channel.updaterID = [dictionary objectOrNilForKey:EPGChannelUpdaterIDKey];
    NSString * createDateString = [dictionary objectOrNilForKey:EPGChannelCreateDateKey];
    NSString * publishDateString = [dictionary objectOrNilForKey:EPGChannelPublishDateKey];
    channel.createDate = [[APSTVChannel channelDateFormatter] dateFromString:createDateString];
    channel.publishDate = [[APSTVChannel channelDateFormatter] dateFromString:publishDateString];
    
    return channel;
}


@end
