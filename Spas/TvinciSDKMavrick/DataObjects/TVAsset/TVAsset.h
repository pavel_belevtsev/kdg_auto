//
//  TVAsset.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVAsset : BaseModelObject

@property (strong, nonatomic) NSString * assetID;
@property  (assign, nonatomic) NSInteger assetType;
@property (strong, nonatomic) NSString * assetName;
@property (strong, nonatomic) NSString * assetDescription;
@property (strong, nonatomic) NSDate * assetStartDate;
@property (strong, nonatomic) NSDate * assetEndDate;

@property (strong, nonatomic) NSArray * images;
@property (strong, nonatomic) NSArray * files;
@property (strong, nonatomic) NSDictionary * metas;
@property (strong, nonatomic) NSDictionary * tags;
@property (strong, nonatomic) NSDictionary * stats;
@property (strong, nonatomic) NSDictionary * extraParams;




@end
