//
//  TVAsset.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVAsset.h"
#import "TVAssetImage.h"
#import "TVAssetFile.h"

//{
//    "id": "237218",
//    "type": 340,
//    "name": "Phua Chu Kang Pte Ltd S2 -  Episode  28",
//    "description": "The year's still 1982: Chu Kang decides to establish a new contracting business his old buddy, Frankie Foo. Meanwhile, Chu Beng surprises the family by announcing that he's getting married to Margaret. Watch for David Yee's cameo as Margaret's first boyfriend, and of course, the closing scene when PCK's signature Singlish comes under fire. ",
//    "images": [
//               {
//                   "ratio": "16:9",
//                   "width": 105,
//                   "height": 80,
//                   "url": ""
//               },
//               {
//                   "ratio": "16:9",
//                   "width": 195,
//                   "height": 150,
//                   "url": ""
//               },
//               {
//                   "ratio": "16:9",
//                   "width": 105,
//                   "height": 55,
//                   "url": ""
//               },
//               {
//                   "ratio": "16:9",
//                   "width": 70,
//                   "height": 52,
//                   "url": ""
//               }
//               ],
//    "files": [
//              {
//                  "id": 349383,
//                  "type": "Main",
//                  "url": " vod/s/felucia/201409W-A/MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28_PC_SS.ism/Manifest"
//              },
//              {
//                  "id": 349384,
//                  "type": "iPhone Main",
//                  "url": " w/rori/201409W-A/MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28_IPH.wvm"
//              },
//              {
//                  "id": 349385,
//                  "type": "iPad Main",
//                  "url": " w/rori/201409W-A/MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28_IPAD.wvm"
//              },
//              {
//                  "id": 349382,
//                  "type": "STB Main",
//                  "url": " w/rori/201409W-A/MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28_STB.wvm"
//              },
//              {
//                  "id": 349381,
//                  "type": "iOS Clear",
//                  "url": " MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28.MPG"
//              }
//              ],
//    "metas": {
//        "Short summary": "The year's still 1982: Chu Kang decides to establish a new contracting business his old buddy, Frankie Foo. Meanwhile, Chu Beng surprises the family by announcing that he's getting married to Margaret.",
//        "Short title": "Phua Chu Kang S2",
//        "Episode name": "Days of being young Part 2",
//        "Billing ID": "12345",
//        "Licensing window start": "30/10/2013 16:00:00",
//        "Licensing window end": "31/12/2113 15:59:00",
//        "Short Pinyin title": "Short Pinyin title value",
//        "Long Pinyin title": "Long Pinyin title value",
//        "Eshop URL": "Eshop URL value",
//        "Release year": "1999",
//        "Episode number": "28",
//        "Season number": "1",
//        "Season premiere": "1",
//        "Season finale": "1",
//        "Interactive": "1"
//    },
//    "tags": {
//        "Genre": [
//                  "Comedy"
//                  ],
//        "Main cast": [
//                      "Pierre Png",
//                      "Gurmit Singh",
//                      "Irene Ang",
//                      "Tan Kheng Hua"
//                      ],
//        "Category": [
//                     "MC Library/English",
//                     "On Demand/Series",
//                     "On Demand/All",
//                     "On Demand/MC Library",
//                     "On Demand/MediaCorp"
//                     ],
//        "Rating": [
//                   "PG"
//                   ],
//        "Series name": [
//                        "Phua Chu Kang Pte Ltd S2"
//                        ],
//        "Audio language": [
//                           "English"
//                           ],
//        "Provider": [
//                     "Mediacorp"
//                     ],
//        "Provider ID": [
//                        "mediacorp.sg"
//                        ],
//        "Product": [
//                    "SVOD"
//                    ],
//        "Territory": [
//                      "Singapore"
//                      ]
//    },
//    "start_date": 1383141600,
//    "end_date": 4544171940,
//    "stats": {
//        "id": "237218",
//        "likes": 0,
//        "views": 0,
//        "rating_count": 0,
//        "rating": 0
//    },
//    "extra_params": {
//        "sys_start_date": "1383141600",
//        "sys_final_date": "4544171940",
//        "external_ids": "0"
//    }
//}

@implementation TVAsset



-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.assetID  = [dictionary objectOrNilForKey:@"id"];
    self.assetType = [[dictionary objectOrNilForKey:@"type"] integerValue];
    self.assetName = [dictionary objectOrNilForKey:@"name"];
    self.assetDescription = [dictionary objectOrNilForKey:@"description"];
    
    NSTimeInterval  startTimeInterval = [[dictionary objectOrNilForKey:@"start_date"] integerValue];
    self.assetStartDate = [NSDate dateWithTimeIntervalSince1970:startTimeInterval];
    
    NSTimeInterval  endTimeInterval = [[dictionary objectOrNilForKey:@"end_date"] integerValue];
    self.assetEndDate = [NSDate dateWithTimeIntervalSince1970:endTimeInterval];
    
    NSMutableArray * array = [NSMutableArray array];
    NSArray * imageDictionaries = [dictionary objectOrNilForKey:@"images"];
    for (NSDictionary * dictionary in imageDictionaries)
    {
        TVAssetImage * image = [[TVAssetImage alloc] initWithDictionary:dictionary];
        [array addObject:image];
    }
    
    self.images = [NSArray arrayWithArray:array];
    [array removeAllObjects];
    
     NSArray * fileDictionaries = [dictionary objectOrNilForKey:@"files"];
    
    for (NSDictionary * dictionary in fileDictionaries)
    {
        TVAssetFile * image = [[TVAssetFile alloc] initWithDictionary:dictionary];
        [array addObject:image];
    }
    
    self.files = [NSArray arrayWithArray:array];
    
    
    self.metas = [dictionary objectOrNilForKey:@"metas"];
    self.tags = [dictionary objectOrNilForKey:@"tags"];
    self.stats = [dictionary objectOrNilForKey:@"stats"];
    self.extraParams = [dictionary objectOrNilForKey:@"extra_params"];

}
@end
