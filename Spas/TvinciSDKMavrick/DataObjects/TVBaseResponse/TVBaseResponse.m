//
//  TVBaseResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVBaseResponse.h"

@implementation TVBaseResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.responseStatus = [dictionary objectForKey:@"status"];
}
@end
