//
//  TVPage.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPage.h"
#import "TVGallery.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "SmartLogs.h"

NSString *const TVPageIDKey = @"ID";
NSString *const TVPageNameKey = @"Name";
NSString *const TVPageMetaDataIDKey = @"PageMetaDataID";
NSString *const TVPageBreadCrumbTextKey = @"BreadCrumbText";
NSString *const TVPageRelativePathKey = @"URL";
NSString *const TVPageHasPlayerKey = @"HasPlayer";
NSString *const TVPageHasCarouselKey = @"HasCarousel";
NSString *const TVPageHasMiddleFooterKey = @"HasMiddleFooter";
NSString *const TVPagePlayerAutoPlayKey = @"PlayerAutoPlay";
NSString *const TVPageIsActiveKey = @"IsActive";
NSString *const TVPageDescriptionKey = @"Description";
NSString *const TVPageIsProtectedKey = @"IsProtected";
NSString *const TVPagePlayerChannelKey = @"PlayerChannel";
NSString *const TVPagePlayerTreeCategoryKey = @"PlayerTreeCategory";
NSString *const TVPageSideProfileIDKey = @"SideProfileID";
NSString *const TVPageBottomProfileIDKey = @"BottomProfileID";
NSString *const TVPageMenuIDKey = @"MenuID";
NSString *const TVPageFooterIDKey = @"FooterID";
NSString *const TVPageMiddleFooterIDKey = @"MiddleFooterID";
NSString *const TVPageProfileIDKey = @"ProfileID";
NSString *const TVPageCarouselChannelKey = @"CarouselChannel";
NSString *const TVPageTokenKey = @"PageToken";
NSString *const TVPageChildrenKey = @"Children";

NSString *const TVPageMainGalleriesKey = @"MainGalleries";

NSString *const TVPageTopGalleriesKey = @"TopGalleries";
NSString * const TVPageMainGalleriesIDs =@"MainGalleriesIDs";
//MainGalleriesIDs

@interface TVPage ()
@end

@implementation TVPage

@synthesize name = _name;
@synthesize pageId = _pageId;
@synthesize pageMetaDataId = _pageMetaDataId;
@synthesize pageDescription = _pageDescription;
@synthesize breadCrumbText = _breadCrumbText;
@synthesize pageRelativePath = _pageRelativePath;
@synthesize hasPlayer = _hasPlayer;
@synthesize hasCarousel = _hasCarousel;
@synthesize hasMiddleFooter = _hasMiddleFooter;
@synthesize playerAutoPlay = _playerAutoPlay;
@synthesize pageToken = _pageToken;
@synthesize isActive = _isActive;
@synthesize playerChannel = _playerChannel;
@synthesize playerTreeCategory = _playerTreeCategory;
@synthesize carouselChannel = _carouselChannel;
@synthesize sideProfileID = _sideProfileID;
@synthesize bottomProfileID = _bottomProfileID;
@synthesize menuID = _menuID;
@synthesize footerID = _footerID;
@synthesize middleFooterID = _middleFooterID;
@synthesize profileID = _profileID;
@synthesize isProtected = _isProtected;
@synthesize children = _children;
@synthesize mainGalleries = _mainGalleriesIds;
@synthesize topGalleries = _topGalleriesIDs;

#pragma mark - Memory
-(void) dealloc
{
    self.name = nil;
    self.pageDescription = nil;
    self.breadCrumbText = nil;
    self.pageRelativePath = nil;
    self.children = nil;
    self.mainGalleries = nil;
    self.topGalleries = nil;
}

-(TVHomeWorldType) homeWorldType
{
    return (TVHomeWorldType)self.profileID;
}

#pragma mark - Parsing
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    // TVLogDebug(@"[Page][Dictionary]: \n %@",dictionary);////////
    
    self.pageId = [[dictionary objectOrNilForKey:TVPageIDKey] integerValue];
    self.name = [dictionary objectOrNilForKey:TVPageNameKey];
    self.pageMetaDataId = [[dictionary objectOrNilForKey:TVPageMetaDataIDKey] integerValue];
    self.pageDescription = [dictionary objectOrNilForKey:TVPageDescriptionKey];
    self.breadCrumbText = [dictionary objectOrNilForKey:TVPageBreadCrumbTextKey];
    self.pageRelativePath = [dictionary objectOrNilForKey:TVPageRelativePathKey];
    self.hasPlayer = [[dictionary objectOrNilForKey:TVPageHasPlayerKey] boolValue];
    self.hasCarousel = [[dictionary objectOrNilForKey:TVPageHasCarouselKey] boolValue];
    self.hasMiddleFooter = [[dictionary objectOrNilForKey:TVPageHasMiddleFooterKey] boolValue];
    self.playerAutoPlay = [[dictionary objectOrNilForKey:TVPagePlayerAutoPlayKey] boolValue];
    self.pageToken = [[dictionary objectOrNilForKey:TVPageTokenKey] intValue];
    self.isActive = [[dictionary objectOrNilForKey:TVPageIsActiveKey ] boolValue];
    self.playerChannel = [[dictionary objectOrNilForKey:TVPagePlayerChannelKey] integerValue];
    self.playerTreeCategory = [[dictionary objectOrNilForKey:TVPagePlayerTreeCategoryKey] integerValue];
    self.carouselChannel = [[dictionary objectOrNilForKey:TVPageCarouselChannelKey] integerValue];
    self.sideProfileID = [[dictionary objectOrNilForKey:TVPageSideProfileIDKey] integerValue];
    self.bottomProfileID = [[dictionary objectOrNilForKey:TVPageBottomProfileIDKey] integerValue];
    self.menuID = [[dictionary objectOrNilForKey:TVPageMenuIDKey] integerValue];
    self.footerID = [[dictionary objectOrNilForKey:TVPageFooterIDKey] integerValue];
    self.middleFooterID = [[dictionary objectOrNilForKey:TVPageMiddleFooterIDKey] integerValue];
    self.profileID = [[dictionary objectOrNilForKey:TVPageProfileIDKey] integerValue];
    self.isProtected = [[dictionary objectOrNilForKey:TVPageIsProtectedKey] boolValue];
    self.children = [dictionary objectOrNilForKey:TVPageChildrenKey];
    
    NSArray *mainGalleries = [dictionary objectOrNilForKey:TVPageMainGalleriesKey];
    self.mainGalleries = [self parseGalleries:mainGalleries];
    NSArray *topGalleries = [dictionary objectOrNilForKey:TVPageTopGalleriesKey];
    self.topGalleries = [self parseGalleries:topGalleries];
    self.mainGalleriesIDs = [dictionary objectOrNilForKey:TVPageMainGalleriesIDs];
    
}

-(NSArray *) parseGalleries : (NSArray *) galleriesToParse
{
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *dict in galleriesToParse)
    {
        TVGallery *gallery = [[TVGallery alloc] initWithDictionary:dict];
        if (gallery != nil)
        {
            
            [temp addObject:gallery];
        }
    }
    return [NSArray arrayWithArray:temp];
}
@end
