//
//  TVRulesResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>


@interface TVParentalRulesResponse : BaseModelObject

@property (strong, nonatomic) NSArray * arrayParentalRules;
@property (strong, nonatomic) TVResponseStatus * responseStatus;
@end
