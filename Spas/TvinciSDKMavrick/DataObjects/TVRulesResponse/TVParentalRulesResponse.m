//
//  TVRulesResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVParentalRulesResponse.h"
#import "TVParentalRule.h"

@implementation TVParentalRulesResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSArray * rulesDicaionries = [dictionary objectForKey:@"rules"];
    NSMutableArray * rulesArray = [NSMutableArray array];
    for (NSDictionary * ruleDictionary in rulesDicaionries)
    {
        TVParentalRule * rule = [[TVParentalRule alloc] initWithDictionary:ruleDictionary];
        [rulesArray addObject:rule];
    }

    self.arrayParentalRules = rulesArray;

    self.responseStatus = [[TVResponseStatus alloc] initWithDictionary:[dictionary objectForKey:@"status"]];
}

@end
