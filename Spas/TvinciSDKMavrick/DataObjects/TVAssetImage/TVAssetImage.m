//
//  TVAssetImage.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVAssetImage.h"

//"ratio": "16:9",
//"width": 105,
//"height": 80,
//"url": ""


@implementation TVAssetImage

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
   
    NSString * ratio = [dictionary objectForKey:@"ratio"];
    NSArray * numbersSplited = [ratio componentsSeparatedByString:@":"];
    self.ratio = (float)[numbersSplited[0] integerValue]/(float)[numbersSplited[1] integerValue];
    self.width = [[dictionary objectForKey:@"width"] floatValue];
    self.height = [[dictionary objectForKey:@"height"] floatValue];
    NSString * stringURL = [dictionary objectForKey:@"url"];
    self.url = [NSURL URLWithString:stringURL];
    
}
@end

