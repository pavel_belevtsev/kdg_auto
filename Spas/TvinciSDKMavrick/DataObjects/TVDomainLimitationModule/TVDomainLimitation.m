//
//  TVDomainLimitationModule.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVDomainLimitation.h"
#import "TVDeviceFamilyLimitation.h"

@implementation TVDomainLimitation


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.concurrency = [[dictionary objectOrNilForKey:@"concurrency"] integerValue];
    self.quantity = [[dictionary objectOrNilForKey:@"quantity"] integerValue];
    self.deviceFrequency = [[dictionary objectOrNilForKey:@"device_frequency"] integerValue];
    self.domainLimitationID = [[dictionary objectOrNilForKey:@"domian_limit_id"] integerValue];
    self.domainLimitationName = [dictionary objectOrNilForKey:@"domain_limit_name"];
    self.npvrQuotaInSeconds = [[dictionary objectOrNilForKey:@"npvr_quota_in_seconds"] integerValue];
    self.userLimit = [[dictionary objectOrNilForKey:@"user_limit"] integerValue];
    self.userFrequency = [[dictionary objectOrNilForKey:@"user_frequency"] integerValue];
    self.userFrequencyDescription = [dictionary objectOrNilForKey:@"user_frequency_description"];
    self.deviceFrequencyDescription = [dictionary objectOrNilForKey:@"device_frequency_description"];
    self.frequency = [[dictionary objectOrNilForKey:@"frequency"] integerValue];

    NSArray * familiesLimitation = [dictionary objectOrNilForKey:@"device_family_limitations"];
    NSMutableArray * array = [NSMutableArray array];
    
    for (NSDictionary * limitDictionary in familiesLimitation)
    {
        TVDeviceFamilyLimitation * deviceFamilyLimitation = [[TVDeviceFamilyLimitation alloc] initWithDictionary:limitDictionary];
        [array addObject:deviceFamilyLimitation];
    }
    
    self.arrayFamilyLimitations = [NSArray arrayWithArray:array];
    

    
}
@end
