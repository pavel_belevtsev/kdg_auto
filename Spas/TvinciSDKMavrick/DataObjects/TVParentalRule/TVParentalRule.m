//
//  TVParentalRule.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVParentalRule.h"


//{
//    "id": 1,
//    "name": "Family friendly",
//    "description": "",
//    "order": 1,
//    "mediaTagTypeId": 1455,
//    "epgTagTypeId": 0,
//    "blockAnonymousAccess": true,
//    "ruleType": 1,
//    "mediaTagValues": [
//                       "PG",
//                       "PG-13"
//                       ],
//    "epgTagValues": [],
//    "isDefault": true,
//    "origin": 3
//}

// 3 = account, 2 = domain, 1 = user

@implementation TVParentalRule

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.ruleID  = [dictionary objectForKey:@"id"];
    self.ruleName = [dictionary objectForKey:@"name"];
    self.ruleDescription = [dictionary objectForKey:@"description"];
    self.order = [[dictionary objectForKey:@"order"] integerValue];
    self.mediaTagTypeId = [[dictionary objectForKey:@"mediaTagTypeId"] integerValue];
    self.epgTagTypeId = [[dictionary objectForKey:@"epgTagTypeId"] integerValue];
    self.blockAnonymousAccess = [[dictionary objectForKey:@"blockAnonymousAccess"] boolValue];
    self.parentalRuleType = (TVParentalRuleType)[[dictionary objectForKey:@"ruleType"] integerValue];
    self.mediaTagValues = [dictionary objectForKey:@"mediaTagValues"];
    self.epgTagValues = [dictionary objectForKey:@"epgTagValues"];
    self.isdefault = [[dictionary objectForKey:@"isDefault"] boolValue];
    self.ruleLevel = (TVRuleDefinedLevel)[[dictionary objectForKey:@"origin"] integerValue];
}
@end
