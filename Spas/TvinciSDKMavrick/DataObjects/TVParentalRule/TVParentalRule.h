//
//  TVParentalRule.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>




@interface TVParentalRule : BaseModelObject

@property (strong, nonatomic) NSString * ruleID;
@property (strong, nonatomic) NSString * ruleName;
@property (strong, nonatomic) NSString * ruleDescription;
@property (assign, nonatomic) NSInteger order;
@property (assign, nonatomic) NSInteger mediaTagTypeId;
@property (assign, nonatomic) NSInteger epgTagTypeId;
@property (assign, nonatomic) BOOL blockAnonymousAccess;
@property (assign, nonatomic) TVParentalRuleType parentalRuleType;
@property (strong, nonatomic) NSArray* mediaTagValues;
@property (strong, nonatomic) NSArray * epgTagValues;
@property (assign, nonatomic) BOOL isdefault;
@property (assign, nonatomic) TVRuleDefinedLevel ruleLevel;

@end
