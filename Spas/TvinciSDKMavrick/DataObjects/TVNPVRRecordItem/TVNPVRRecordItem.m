//
//  TVNPVRItem.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVNPVRRecordItem.h"
#import "NSDate+Format.h"

@implementation TVNPVRRecordItem



//{
//    "ChannelName": "52",
//    "IsAssetProtected": false,
//    "RecordSource": "user",
//    "RecordingID": "24",
//    "CREATE_DATE": null,
//    "DESCRIPTION": "Sein Wort hat bei den Amischen Gewicht, und SÃ¼Ã?holz raspeln gehÃ¶rt zu seinen StÃ¤rken",
//    "END_DATE": "20141223012000",
//    "EPG_CHANNEL_ID": "52",
//    "EPG_ID": 0,
//    "EPG_IDENTIFIER": "483654351‐B66CED9AA9AD18B93F69609E8BFD20BF",
//    "EPG_Meta": [],
//    "EPG_TAGS": [],
//    "GROUP_ID": "215",
//    "IS_ACTIVE": "true",
//    "LIKE_COUNTER": 0,
//    "NAME": "Amish Mafia",
//    "PIC_URL": null,
//    "PUBLISH_DATE": "",
//    "START_DATE": "20141223002500",
//    "STATUS": "completed",
//    "UPDATER_ID": null,
//    "UPDATE_DATE": null,
//    "media_id": ""
//}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.channelnName = [dictionary objectOrNilForKey:@"ChannelName"];
    self.isAssetProtected = [[dictionary objectOrNilForKey:@"IsAssetProtected"] boolValue];
    self.recordSource = [dictionary objectOrNilForKey:@"RecordSource"];
    self.recordingID = [dictionary objectOrNilForKey:@"RecordingID"];
    self.recordDescription = [dictionary objectOrNilForKey:@"DESCRIPTION"];
       self.epgChannelID = [dictionary objectOrNilForKey:@"EPG_CHANNEL_ID"];
    self.epgID = [[dictionary objectOrNilForKey:@"EPG_ID"] integerValue];
    self.epgIdentifer = [dictionary objectOrNilForKey:@"EPG_IDENTIFIER"];
    self.epgMeta = [self parseMetas:[dictionary objectForKey:@"EPG_Meta"]];
    self.epgTags = [self parseTags:[dictionary objectForKey:@"EPG_TAGS"]];
    self.groupID = [dictionary objectForKey:@"GROUP_ID"];
    self.isActive = [[dictionary objectForKey:@"IS_ACTIVE"] boolValue];
    self.likeCount = [[dictionary objectForKey:@"LIKE_COUNTER"] integerValue];
    self.name = [dictionary objectForKey:@"NAME"];
    NSString * urlString = [dictionary objectForKey:@"PIC_URL"];
    self.picURL = [NSURL URLWithString:urlString];
    self.recordingStatus = TVRecordingStatusForString([dictionary objectForKey:@"STATUS"]);
    NSLog(@" self.recordingStatus = %d" ,self.recordingStatus);
    self.updaterID = [dictionary objectOrNilForKey:@"UPDATER_ID"];
    self.mediaID = [dictionary objectOrNilForKey:@"media_id"];
    
    //startDate 20150209150000
    
    self.createDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:[dictionary objectOrNilForKey:@"CREATE_DATE"] andFormat:@"yyyyMMddHHmmss"];
    self.endDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:[dictionary objectOrNilForKey:@"END_DATE"] andFormat:@"yyyyMMddHHmmss"];
    self.publishDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:[dictionary objectOrNilForKey:@"PUBLISH_DATE"] andFormat:@"yyyyMMddHHmmss"];
    self.startDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:[dictionary objectOrNilForKey:@"START_DATE"] andFormat:@"yyyyMMddHHmmss"];
    self.updateDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:[dictionary objectOrNilForKey:@"PUBLISH_DATE"] andFormat:@"yyyyMMddHHmmss"];

    
//    self.createDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[[dictionary objectOrNilForKey:@"CREATE_DATE"] integerValue]];
//    self.endDate =  [NSDate dateWithTimeIntervalSinceReferenceDate:[[dictionary objectOrNilForKey:@"END_DATE"] integerValue]];
//    self.publishDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[[dictionary objectOrNilForKey:@"PUBLISH_DATE"] integerValue]];
//    self.startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[[dictionary objectOrNilForKey:@"START_DATE"] integerValue]];
//    self.updateDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[[dictionary objectOrNilForKey:@"PUBLISH_DATE"] integerValue]];
    
    

    
    
    

    
    
}


-(NSDictionary *) parseTags:(NSArray *)theTags
{
    
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in theTags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value!= nil)
            {
                NSArray * tags =  [tagsParsed objectForKey:title];
                if (tags == nil)
                {
                    tags = [NSArray arrayWithObject:value];
                }
                else
                {
                    tags = [tags arrayByAddingObject:value];
                }
                [tagsParsed setObject:tags forKey:title];
                
            }
        }
    }
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}

-(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    // TVLogDebug(@"%@",metaParsed);
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}

@end
