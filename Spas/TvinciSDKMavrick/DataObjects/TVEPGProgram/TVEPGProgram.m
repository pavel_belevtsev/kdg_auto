//
//  TVEPGProgram.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVEPGProgram.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "SmartLogs.h"
#import "TVPictureSize.h"
#import "NSCoder+NSNullAvoidance.h"
#import "TVDeviceScreenUtils.h"

// 8/29/2012 6:00:00 PM
//#define EPG_PROGRAM_DATE_FORMAT @"MM/dd/yyyy h:mm:ss a"

// "20/09/2012 00:10:00"
#define EPG_PROGRAM_DATE_FORMAT @"dd/MM/yyyy HH:mm:ss"

// "9/13/2012 12:43:36 PM"
#define EPG_PROGRAM_UPDATE_FORMAT @"MM/dd/yyyy hh:mm:ss a"


NSString *const TVEPGProgramEPGChannelIDKey = @"EPG_CHANNEL_ID";
NSString *const TVEPGProgramEPGIdentifierKey = @"EPG_IDENTIFIER";
NSString *const TVEPGProgramNameKey = @"NAME";
NSString *const TVEPGProgramDescriptionKey = @"DESCRIPTION";
NSString *const TVEPGProgramStartDateKey = @"START_DATE";
NSString *const TVEPGProgramEndDateKey = @"END_DATE";
NSString *const TVEPGProgramPicURLKey = @"PIC_URL";
NSString *const TVEPGProgramStatusKey = @"STATUS";
NSString *const TVEPGProgramIsActiveKey = @"IS_ACTIVE";
NSString *const TVEPGProgramGroupIDKey = @"GROUP_ID";
NSString *const TVEPGProgramUpdaterIDKey = @"UPDATER_ID";
NSString *const TVEPGProgramUpdateDateKey = @"UPDATE_DATE";
NSString *const TVEPGProgramPublishDateKey = @"PUBLISH_DATE";
NSString *const TVEPGProgramCreateDateKey = @"CREATE_DATE";
NSString *const TVEPGProgramEPGTagKey = @"EPG_TAG";
NSString *const TVEPGProgramMediaIDKey = @"media_id";
NSString *const TVEPGProgramEPGLikeCounterKey = @"LIKE_COUNTER";
NSString *const TVEPGProgramEPGIDKey = @"EPG_ID";

@implementation TVEPGProgram

@synthesize EPGChannelID = _EPGChannelID;
@synthesize EPGIdentifier = _EPGIdentifier;
@synthesize name = _name;
@synthesize description = _description;
@synthesize startDateTime = _startDate;
@synthesize endDateTime = _endDate;
@synthesize pictureURL = _picURL;
@synthesize status = _status;
@synthesize isActive = _isActive;
@synthesize groupID = _groupID;
@synthesize updaterID = _updaterID;
@synthesize updateDate = _updateDate;
@synthesize publishDate = _publishDate;
@synthesize createDate = _createDate;
@synthesize EPGTag = _EPGTag;
@synthesize mediaID = _mediaID;
@synthesize tags;
@synthesize metaData;


#pragma mark nscoding protocol

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObjectOrNil:self.EPGChannelID forKey:TVEPGProgramEPGChannelIDKey];
    [encoder encodeObjectOrNil:self.EPGIdentifier forKey:TVEPGProgramEPGIdentifierKey];
    [encoder encodeObjectOrNil:self.name forKey:TVEPGProgramNameKey];
    [encoder encodeObjectOrNil:self.description forKey:TVEPGProgramDescriptionKey];
    
    [encoder encodeObjectOrNil:[self.pictureURL absoluteString]  forKey:TVEPGProgramPicURLKey];
    
    [encoder encodeObjectOrNil:self.status  forKey:TVEPGProgramPicURLKey];
    [encoder encodeObjectOrNil:self.isActive  forKey:TVEPGProgramIsActiveKey];
    [encoder encodeObjectOrNil:self.groupID  forKey:TVEPGProgramUpdaterIDKey];
    [encoder encodeObjectOrNil:self.EPGTag  forKey:TVEPGProgramEPGTagKey];
    [encoder encodeObjectOrNil:self.mediaID  forKey:TVEPGProgramMediaIDKey];
    
    
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.startDateTime.timeIntervalSince1970] forKey:TVEPGProgramStartDateKey];
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.endDateTime.timeIntervalSince1970] forKey:TVEPGProgramEndDateKey];
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.updateDate.timeIntervalSince1970] forKey:TVEPGProgramUpdateDateKey];
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.publishDate.timeIntervalSince1970] forKey:TVEPGProgramPublishDateKey];
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.createDate.timeIntervalSince1970] forKey:TVEPGProgramCreateDateKey];
    
    [encoder encodeObjectOrNil:self.metaData  forKey:EpgMediaItemMetaDataKey];
    [encoder encodeObjectOrNil:self.tags  forKey:EpgMediaItemTagsKey];
    
    [encoder encodeObjectOrNil:self.cleanMetaData forKey:EpgMediaItemCleanMetaDataKey];
    [encoder encodeObjectOrNil:self.cleanTags forKey:EpgMediaItemCleanTagsKey];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        self.EPGChannelID = [aDecoder decodeObjectForKey:TVEPGProgramEPGChannelIDKey];
        self.EPGIdentifier = [aDecoder decodeObjectForKey:TVEPGProgramEPGIdentifierKey];
        self.name = [aDecoder decodeObjectForKey:TVEPGProgramNameKey];
        self.description = [aDecoder decodeObjectForKey:TVEPGProgramDescriptionKey];
        
        NSString *URLString = [aDecoder decodeObjectForKey:TVEPGProgramPicURLKey];
        self.pictureURL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
        
        self.status = [aDecoder decodeObjectForKey:TVEPGProgramStatusKey];
        self.isActive = [aDecoder decodeObjectForKey:TVEPGProgramIsActiveKey];
        self.groupID = [aDecoder decodeObjectForKey:TVEPGProgramGroupIDKey];
        self.updaterID = [aDecoder decodeObjectForKey:TVEPGProgramUpdaterIDKey];
        self.EPGTag = [aDecoder decodeObjectForKey:TVEPGProgramEPGTagKey];
        self.mediaID = [aDecoder decodeObjectForKey:TVEPGProgramMediaIDKey];
        
        
        // Dates
        
        NSNumber *startDateNumber = [aDecoder decodeObjectForKey:TVEPGProgramStartDateKey];
        self.startDateTime =[NSDate dateWithTimeIntervalSince1970:startDateNumber.integerValue];
        
        NSNumber *endDateNumber =  [aDecoder decodeObjectForKey:TVEPGProgramEndDateKey];
        self.endDateTime = [NSDate dateWithTimeIntervalSince1970:endDateNumber.integerValue];
        
        NSNumber *updateDateNumber =  [aDecoder decodeObjectForKey:TVEPGProgramUpdateDateKey];
        self.updateDate = [NSDate dateWithTimeIntervalSince1970:updateDateNumber.integerValue];
        
        NSNumber *publishDateNumber =  [aDecoder decodeObjectForKey:TVEPGProgramPublishDateKey];
        self.publishDate = [NSDate dateWithTimeIntervalSince1970:publishDateNumber.integerValue];
        
        NSNumber *createDateNumber =  [aDecoder decodeObjectForKey:TVEPGProgramCreateDateKey];
        self.createDate = [NSDate dateWithTimeIntervalSince1970:createDateNumber.integerValue];
        
        self.metaData = [aDecoder decodeObjectForKey:EpgMediaItemMetaDataKey];
        self.tags = [aDecoder decodeObjectForKey:EpgMediaItemTagsKey];
        
        self.cleanMetaData = [aDecoder decodeObjectOrNilForKey:EpgMediaItemCleanMetaDataKey];
        self.cleanTags = [aDecoder decodeObjectOrNilForKey:EpgMediaItemCleanTagsKey];
        
        
        
    }
    return self;
}

#pragma date formats
static NSDateFormatter * programDurationDateFormatter;
static NSDateFormatter * programUpdateDateFormatter;

static bool timeReferenceIsFromGMT = NO;

+(void) setTimeReferenceToGMT
{
        timeReferenceIsFromGMT = YES;
}



+(NSDateFormatter *) programDurationDateFormatter
{
    if (programDurationDateFormatter == nil)
    {
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = EPG_PROGRAM_DATE_FORMAT;
        if (timeReferenceIsFromGMT)
        {
            [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        }
            
        [TVEPGProgram setProgramDurationDateFormatter:formatter];
    }
    return programDurationDateFormatter;
}

+(NSDateFormatter *) programUpdateDateFormatter
{
    if (programUpdateDateFormatter == nil)
    {
        NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = EPG_PROGRAM_UPDATE_FORMAT;
        [TVEPGProgram setProgramUpdateDateFormatter:formatter];
    }
    
    return programUpdateDateFormatter;
}

+(void) setProgramDurationDateFormatter:(NSDateFormatter *) formatter
{
    if (programDurationDateFormatter != formatter)
    {

        programDurationDateFormatter = formatter;
    }
}
+(void) setProgramUpdateDateFormatter:(NSDateFormatter *) formatter
{
    if (programUpdateDateFormatter != formatter)
    {

        programUpdateDateFormatter = formatter;
    }

}


- (void)dealloc
{

}

#pragma mark - Initialization
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
   // TVLogDebug(@"[TVEPGProgram] %@",dictionary);////////
    
    self.EPGChannelID = [dictionary objectOrNilForKey:TVEPGProgramEPGChannelIDKey];
    self.EPGIdentifier = [dictionary objectOrNilForKey:TVEPGProgramEPGIdentifierKey];
    self.name = [dictionary objectOrNilForKey:TVEPGProgramNameKey];
    self.description = [dictionary objectOrNilForKey:TVEPGProgramDescriptionKey];
    
    NSString *URLString = [dictionary objectOrNilForKey:TVEPGProgramPicURLKey];
    self.pictureURL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
    
    self.status = [dictionary objectOrNilForKey:TVEPGProgramStatusKey];
    self.isActive = [dictionary objectOrNilForKey:TVEPGProgramIsActiveKey];
    self.groupID = [dictionary objectOrNilForKey:TVEPGProgramGroupIDKey];
    self.updaterID = [dictionary objectOrNilForKey:TVEPGProgramUpdaterIDKey];
    self.EPGTag = [dictionary objectOrNilForKey:TVEPGProgramEPGTagKey];
    self.mediaID = [dictionary objectOrNilForKey:TVEPGProgramMediaIDKey];
    self.EPGId = [dictionary objectOrNilForKey:TVEPGProgramEPGIDKey];
    
    if (![[dictionary objectOrNilForKey:TVEPGProgramEPGIDKey] isKindOfClass:[NSString class]]) {
        self.EPGId = [NSString stringWithFormat:@"%@",[dictionary objectOrNilForKey:TVEPGProgramEPGIDKey]];
    }

    self.likeConter =[[dictionary objectOrNilForKey:TVEPGProgramEPGLikeCounterKey] integerValue];
    // Date
    
    NSString *startDateString = [dictionary objectOrNilForKey:TVEPGProgramStartDateKey];
    self.startDateTime = [[TVEPGProgram programDurationDateFormatter] dateFromString:startDateString];
    
   // ASLog(@"self.startDateTime = %@",self.startDateTime);
    
    NSString *endDateString = [dictionary objectOrNilForKey:TVEPGProgramEndDateKey];
    self.endDateTime = [[TVEPGProgram programDurationDateFormatter] dateFromString:endDateString];
    
   // ASLog(@"self.endDateTime = %@",self.endDateTime);

    
    NSString *updateDateString = [dictionary objectOrNilForKey:TVEPGProgramUpdateDateKey];
    self.updateDate = [[TVEPGProgram programUpdateDateFormatter] dateFromString:updateDateString];
    
   // ASLog(@"self.updateDate = %@",self.updateDate);

    
    NSString *publishDateString = [dictionary objectOrNilForKey:TVEPGProgramPublishDateKey];
    self.publishDate = [[TVEPGProgram programUpdateDateFormatter] dateFromString:publishDateString];
    
    NSString *createDateString = [dictionary objectOrNilForKey:TVEPGProgramCreateDateKey];
    self.createDate = [[TVEPGProgram programUpdateDateFormatter] dateFromString:createDateString];
    
    NSArray *myMetaData = [dictionary objectOrNilForKey:EpgMediaItemMetaDataKey];
    self.metaData = [self parseMetas:myMetaData];
    
    NSArray *myTags = [dictionary objectOrNilForKey:EpgMediaItemTagsKey];
    self.tags = [self parseTags:myTags];
    
    self.cleanMetaData =[dictionary objectOrNilForKey:EpgMediaItemMetaDataKey];
    self.cleanTags = [dictionary objectOrNilForKey:EpgMediaItemTagsKey];
  //  self.preseredTags = [self parseCleanTags:self.cleanTags];
}


-(NSDictionary *) parseCleanTags:(NSArray *)theTags
{
    NSMutableArray * allKeys = [NSMutableArray array];
    
    for (NSDictionary *tagAsDictionary in theTags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        if ([allKeys indexOfObject:title] == NSNotFound)
        {
            [allKeys addObject:title];
        }
    }
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];

    for (NSString * key in allKeys)
    {
        if (theTags)
        {
            NSArray * arr = [self find:key inArray:theTags];
            if (arr && [arr count]>0)
            {
                [dic setObject:arr forKey:key];
            }
        }
        
    }
    TVLogDebug(@"dic = %@",dic);
    return [NSDictionary dictionaryWithDictionary:dic];
}


-(NSArray*)find:(NSString *)text inArray:(NSArray*)arr
{
    NSMutableArray * tempArr = [NSMutableArray array];
    
    for (NSDictionary * dic in arr)
    {
        if ([[[dic objectForKey:@"Key"] uppercaseString] isEqualToString:[text uppercaseString]])
        {
            NSString * item = [dic objectForKey:@"Value"];
            if (item)
            {
                [tempArr addObject:item];
            }
        }
    }
    
    return tempArr;
}

-(NSDictionary *) parseTags:(NSArray *)theTags
{
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in theTags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value != nil)
            {
                [tagsParsed setObject:value forKey:title];
            }
        }
    }
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}

-(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:EpgTVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    // TVLogDebug(@"%@",metaParsed);
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}

#pragma mark - utilities

-(NSString *) asString{
    return [NSString stringWithFormat:@"\n-----------Program-----------\n   name: %@\n      mediaID: %@\n   metaData: %@\n tags: %@\n--------------------------\n", self.name, self.mediaID, self.metaData, self.tags];
}

-(BOOL) programFinished{
    if(!self.startDateTime || !self.endDateTime){
        return NO;
    }
        
    return ([[NSDate date] timeIntervalSince1970] > [self.endDateTime timeIntervalSince1970]);
}

-(BOOL) programIsOn{
    
    if(!self.startDateTime || !self.endDateTime){
        return NO;
    }
    
    return (([[NSDate date] timeIntervalSince1970] <= [self.endDateTime timeIntervalSince1970]) &&
            ([[NSDate date] timeIntervalSince1970] >= [self.startDateTime timeIntervalSince1970]));
}

#pragma mark - pictures
-(NSURL *) pictureURLForPictureSize : (TVPictureSize *) size
{
    NSURL *toReturn = nil;
    if (size != nil)
    {
        NSRegularExpression *pictureSizeRegex = [NSRegularExpression regularExpressionWithPattern:@"(\\d+X\\d+)|(full)" options:0 error:NULL];
        if (pictureSizeRegex != nil)
        {
            NSString *absoluteString = [self.pictureURL absoluteString];
            if (absoluteString != nil)
            {
                NSString *replacement = [pictureSizeRegex stringByReplacingMatchesInString:absoluteString options:0 range:NSMakeRange(0, absoluteString.length) withTemplate:size.name];
                toReturn = [NSURL URLWithString:replacement];
            }
        }
    }
    return toReturn;
}


-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size
{
    CGSize retinaSize;
    BOOL isRetina = [TVDeviceScreenUtils isRetina];
    retinaSize.width = isRetina?size.width*2.0:size.width;
    retinaSize.height = isRetina? size.height*2.0:size.height;
    return [self pictureURLForSize:retinaSize];
}


-(NSURL *) pictureURLForSize : (CGSize ) size
{
    NSURL *toReturn = nil;
    
    NSRegularExpression *pictureSizeRegex = [NSRegularExpression regularExpressionWithPattern:@"(\\d+X\\d+)|(full)" options:0 error:NULL];
    
    if (pictureSizeRegex != nil)
    {
        NSString *absoluteString = [self.pictureURL absoluteString];
        if (absoluteString != nil)
        {
            int width =size.width;
            int  height = size.height;
            NSString * name = [NSString stringWithFormat:@"%dX%d",width,height];
            
            NSString *replacement = [pictureSizeRegex stringByReplacingMatchesInString:absoluteString options:0 range:NSMakeRange(0, absoluteString.length) withTemplate:name];
            
            toReturn = [NSURL URLWithString:replacement];
            
        }
    }
    
    return toReturn;
}

-(BOOL) isPlaying
{
    NSDate * now = [NSDate date];
    
    if([now compare:self.startDateTime] == NSOrderedDescending && [now compare:self.endDateTime] == NSOrderedAscending)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

-(BOOL)isEqual:(TVEPGProgram *)object
{
    return  [self.EPGIdentifier isEqualToString:object.EPGIdentifier];
}


NSString *const EpgMediaItemTagsKey = @"EPG_TAGS";
NSString *const EpgMediaItemMetaDataKey = @"EPG_Meta";
NSString *const EpgTVMediaItemTagTitleKey = @"Key";
NSString *const EpgTVMediaItemTagValuesKey = @"Value";

NSString *const EpgMediaItemCleanTagsKey = @"EPG_CLEAN_TAGS";
NSString *const EpgMediaItemCleanMetaDataKey = @"EPG_CLEAN_Meta";



@end
