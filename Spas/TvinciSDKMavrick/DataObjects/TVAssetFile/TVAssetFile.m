//
//  TVAssetFile.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVAssetFile.h"

@implementation TVAssetFile

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.fileID =  [dictionary objectOrNilForKey:@"id"];
    self.fileType = [dictionary objectOrNilForKey:@"type"];
    NSString * fileURLString = [dictionary objectOrNilForKey:@"url"];
    self.fileURL = [NSURL URLWithString:fileURLString];
}
@end
