//
//  TVRental.m
//  TvinciSDK
//
//  Created by iosdev1 on 11/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVRental.h"

NSString *const TVRentalDateFormat = @"yyyy-MM-dd HH:mm:ss.SSS";

#define PPVPeriodInYears 10

@implementation TVRental

@synthesize currentDate;
@synthesize endDate;
@synthesize purchaseDate;
@synthesize currentUses;
@synthesize maxUses;
@synthesize mediaFileID;
@synthesize mediaID;
@synthesize deviceName;
@synthesize deviceUDID;


-(void)dealloc{

}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = TVRentalDateFormat;
    
    NSString *dateString = [dictionary objectOrNilForKey:TVRentalCurrentDate];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    // Old code - time should change timezone from outside SDK
//    self.currentDate = [self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.currentDate = [formatter dateFromString:dateString];

    
    //ASLog(@"dateString = %@   <===> currentDate = %@",dateString,self.currentDate);
    
    dateString = [dictionary objectOrNilForKey:TVRentalEndDateKey];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    // Old code -time should change timezone from outside SDK
//    self.endDate = [self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.endDate = [formatter dateFromString:dateString];
    
    ASLog(@"dateString = %@   <===> self.endDate = %@",dateString,self.endDate);
    
    dateString = [dictionary objectOrNilForKey:TVRentalPurchaseDateKey];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    // Old code -time should change timezone from outside SDK
//    self.purchaseDate =[self takeSingapurTimeWIthDate:[formatter dateFromString:dateString]];
    self.purchaseDate =[formatter dateFromString:dateString];

    
    self.currentUses = [[dictionary objectOrNilForKey:TVRentalCurrentUsesKey] integerValue];
    
    self.maxUses = [[dictionary objectOrNilForKey:TVRentalMaxUsesKey] integerValue];
    
    self.mediaFileID = [[dictionary objectOrNilForKey:TVRentalMediaFileID] integerValue];
    
    self.mediaID = [[dictionary objectOrNilForKey:TVRentalMediaID] integerValue];
    
    self.purchaseMethod = [[dictionary objectOrNilForKey:TVRentalPurchaseMethod] integerValue];
    
    self.deviceName = [dictionary objectOrNilForKey:TVRentalDeviceNameKey];
    
    self.deviceUDID = [dictionary objectOrNilForKey:TVRentalDeviceUDIDKey];
    
    
    

}

- (NSInteger)getMaxRentalMonths {
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
    
    NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:unitFlags fromDate:self.purchaseDate  toDate:self.endDate  options:0];
    
    int months  = (int)[conversionInfo month];
    int days    = (int)[conversionInfo day];
    int hours   = (int)[conversionInfo hour];
    int minutes = (int)[conversionInfo minute];
    TVLogDebug(@"purchaseDate: %@  endDate: %@", self.purchaseDate, self.endDate);
    TVLogDebug(@"months: %d  days: %d  hours: %d  minutes : %d", months, days, hours, minutes);
    return months;
}

- (NSInteger)getMaxRentalYears {
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
    
    NSDateComponents *conversionInfo = [[NSCalendar currentCalendar] components:unitFlags fromDate:self.purchaseDate  toDate:self.endDate  options:0];
    
    int months  = (int)[conversionInfo month];
    int days    = (int)[conversionInfo day];
    int hours   = (int)[conversionInfo hour];
    int minutes = (int)[conversionInfo minute];
    int years   = (int)[conversionInfo year];
    TVLogDebug(@"purchaseDate: %@  endDate: %@", self.purchaseDate, self.endDate);
    TVLogDebug(@"years : %d  months: %d  days: %d  hours: %d  minutes : %d",years, months, days, hours, minutes);
    return years;
}

- (BOOL)isRental
{
    NSInteger maxRentalYears = [self getMaxRentalYears];
    if (maxRentalYears < PPVPeriodInYears)
    {
        return YES;
    }
    return NO;
}

//-(NSDate *)takeLocalTimeWIthDate:(NSDate *)sourceDate
//{
//    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];;
//    //ASLog(@"destinationTimeZone = %@",destinationTimeZone);
//    NSDate * date = [NSDate dateWithTimeInterval:[destinationTimeZone secondsFromGMT] sinceDate:sourceDate];
//    return date;
//}
//
//-(NSDate *)takeSingapurTimeWIthDate:(NSDate *)sourceDate
//{
//    
//    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"SGT"];
//    //NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];;
//    //ASLog(@"destinationTimeZone = %@",destinationTimeZone);
//    NSDate * date = [NSDate dateWithTimeInterval:[sourceTimeZone secondsFromGMT] sinceDate:sourceDate];
//    return date;
//}

@end

NSString *const TVRentalCurrentDate = @"m_dCurrentDate";
NSString *const TVRentalEndDateKey = @"m_dEndDate";
NSString *const TVRentalPurchaseDateKey = @"m_dPurchaseDate";
NSString *const TVRentalCurrentUsesKey = @"m_nCurrentUses";
NSString *const TVRentalMaxUsesKey = @"m_nMaxUses";
NSString *const TVRentalMediaFileID = @"m_nMediaFileID";
NSString *const TVRentalMediaID = @"m_nMediaID";
NSString *const TVRentalPurchaseMethod = @"m_purchaseMethod";
NSString *const TVRentalDeviceNameKey  = @"m_sDeviceName";
NSString *const TVRentalDeviceUDIDKey = @"m_sDeviceUDID";


// Purchase Method. Values: Unknown = 0, CreditCard = 1, SMS = 2, PayPal = 3, DebitCard = 4, Ideal = 5, Incaso = 6, Gift = 7, Visa = 20, MasterCard = 21, InApp = 200