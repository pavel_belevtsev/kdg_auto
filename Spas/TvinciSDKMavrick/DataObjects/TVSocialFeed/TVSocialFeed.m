//
//  TVSocialFeed.m
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialFeed.h"

#import "TVFacebookComment.h"
#import "TVTwitterComment.h"
#import "TVInAppComment.h"

@interface TVSocialFeed ()
{
    
}
@property (strong, readwrite) NSArray *facebookCommentsArr;
@property (strong, readwrite) NSArray *twitterCommentsArr;
@property (strong, readwrite) NSArray *inAppCommentsArr;


@end


NSString *const TVSocialFeedKey         = @"Feed";
NSString *const TVSocialFeedFacebookKey = @"Facebook";
NSString *const TVSocialFeedTwitterKey  = @"Twitter";
NSString *const TVSocialFeedInAppKey    = @"InApp";

@implementation TVSocialFeed


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    
    NSDictionary* feedDic = [dictionary objectOrNilForKey:TVSocialFeedKey];

    //  Parse Facebook
    NSArray* facebookArr = [feedDic objectOrNilForKey:TVSocialFeedFacebookKey];
    NSMutableArray* tempArr = [NSMutableArray arrayWithCapacity:facebookArr.count];
    for (int i = 0; i < facebookArr.count; i++)
    {
        NSDictionary* facebookDic = facebookArr[i];
        TVSocialFeedComment* facebookComment = [[TVSocialFeedComment alloc] initWithDictionary:facebookDic];
        [tempArr addObject:facebookComment];
    }
    self.facebookCommentsArr = [NSArray arrayWithArray:tempArr];
    [tempArr removeAllObjects];
    tempArr = nil;
    facebookArr = nil;

    
    //  Parse Twitter comments
    NSArray* twitterArr = [feedDic objectOrNilForKey:TVSocialFeedTwitterKey];
    tempArr = [NSMutableArray arrayWithCapacity:twitterArr.count];
    for (int i = 0; i < twitterArr.count; i++)
    {
        NSDictionary* twitterDic = twitterArr[i];
        TVSocialFeedComment* twitterComment = [[TVSocialFeedComment alloc] initWithDictionary:twitterDic] ;
        [tempArr addObject:twitterComment];
    }
    self.twitterCommentsArr = [NSArray arrayWithArray:tempArr];
    [tempArr removeAllObjects];
    tempArr = nil;
    twitterArr = nil;

    
    //  Parse InApp comments
    NSArray* inAppArr = [feedDic objectOrNilForKey:TVSocialFeedInAppKey];
    tempArr = [NSMutableArray arrayWithCapacity:inAppArr.count];
    for (int i = 0; i < inAppArr.count; i++)
    {
        NSDictionary* inAppDic = inAppArr[i];
        TVSocialFeedComment* inAppComment = [[TVSocialFeedComment alloc] initWithDictionary:inAppDic] ;
        [tempArr addObject:inAppComment];
    }
    self.inAppCommentsArr = [NSArray arrayWithArray:tempArr];
    [tempArr removeAllObjects];
    tempArr = nil;
    inAppArr = nil;
    
}


-(NSString *)description
{
    return [NSString stringWithFormat:@"facebookCommentsArr = %@ \n\n twitterCommentsArr = %@ \n\n inAppCommentsArr = %@",self.facebookCommentsArr,self.twitterCommentsArr,self.inAppCommentsArr];
}

@end
