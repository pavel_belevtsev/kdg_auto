//
//  TVSocialFeedComment.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVSocialFeedComment : BaseModelObject {
    
}


@property (strong, readonly) NSString *commentBody;
@property (strong, readonly) NSString *commentTitle;
@property (strong, readonly) NSString *creatorName;
@property (strong, readonly) NSDate *createDate;
@property (strong, readonly) NSURL *creatorUrl;
@property (assign, readonly) NSInteger popularityCounter;
@property (strong, readonly) NSArray *subComments;
@property (strong, readonly) NSURL *feedItemLinkUrl;


@end
