//
//  TVSocialNetComment.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialNetComment.h"
#import "TVSubSocialComment.h"


@interface TVSocialNetComment ()
{
    
}



@end


@implementation TVSocialNetComment



- (void)dealloc
{
}


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    
    
}

-(NSString*)description
{
    NSString * superD = [super description];
    return [NSString stringWithFormat:@"self.feedItemLinkUrl= %@, subComments = %@ , %@",self.feedItemLinkUrl,self.subComments,superD];
}
@end
