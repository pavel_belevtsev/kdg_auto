//
//  TVSocialFeedComment.m
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialFeedComment.h"
#import "NSDate+Format.h"
#import "NSString+JsonData.h"

NSString *const SrverDateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZ";
NSString *const TwitterDateFormat = @"dd/MM/yyyy HH:mm:ssZZZZ";
NSString *const secondOptionFroDateFormat = @"MM/dd/yyyy hh:mm:ss aa";

NSString *const TVSocialFeedCommentTitleKey  = @"Title";
NSString *const TVSocialFeedCommentBodyKey    = @"Body";
NSString *const TVSocialFeedCommentCreatorNameKey    = @"CreatorName";
NSString *const TVSocialFeedCommentCreateDateKey    = @"CreateDate";
NSString *const TVSocialFeedCommentCreatorImageUrlKey    = @"CreatorImageUrl";
NSString *const TVSocialFeedCommentPopularityCounterKey    = @"PopularityCounter";
NSString *const TVSocialFeedCommentSubCommentsKey    = @"Comments";
NSString *const TVSocialFeedCommentItemLinkKey = @"FeedItemLink";
NSString *const TVSocialCommentsKey         = @"Comments";

//yyyy-MM-dd HH:mm:ssZZZZ


#define kDateFormats @[@"yyyy-MM-dd'T'HH:mm:ssZZZZ",@"dd/MM/yyyy HH:mm:ssZZZZ",@"MM/dd/yyyy hh:mm:ss aa"];


@interface TVSocialFeedComment ()
{
    
}
@property (strong, readwrite) NSString *commentBody;
@property (strong, readwrite) NSString *commentTitle;
@property (strong, readwrite) NSString *creatorName;
@property (strong, readwrite) NSDate *createDate;
@property (strong, readwrite) NSURL *creatorUrl;
@property (assign, readwrite) NSInteger popularityCounter;
@property (strong, readwrite) NSArray *subComments;
@property (strong, readwrite) NSURL *feedItemLinkUrl;


@end



@implementation TVSocialFeedComment


- (void)dealloc
{
    self.commentBody = nil;
    self.commentTitle = nil;
    self.creatorName = nil;
    self.creatorUrl = nil;
    self.createDate = nil;
    
   
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];

    self.commentBody        = [dictionary objectOrNilForKey:TVSocialFeedCommentBodyKey];
    self.commentTitle       = [dictionary objectOrNilForKey:TVSocialFeedCommentTitleKey];
    self.creatorName        = [dictionary objectOrNilForKey:TVSocialFeedCommentCreatorNameKey];
    self.creatorUrl         = [NSURL URLWithString:[dictionary objectOrNilForKey:TVSocialFeedCommentCreatorImageUrlKey]];
    self.popularityCounter  = [[dictionary objectOrNilForKey:TVSocialFeedCommentPopularityCounterKey] integerValue];
    self.feedItemLinkUrl    = [NSURL URLWithString:[dictionary objectOrNilForKey:TVSocialFeedCommentItemLinkKey]];
    
    id creationDateObject =  [dictionary objectOrNilForKey:TVSocialFeedCommentCreateDateKey];
    
    if ([creationDateObject isKindOfClass:[NSString  class]])
    {
        NSString *creationDateString = creationDateObject;
        
        NSArray *dateFormats = kDateFormats;
        for (NSString * dateFormat in dateFormats)
        {
            self.createDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:creationDateString andFormat:dateFormat];
            if (self.createDate!= nil)
            {
                break;
            }
        }
    }
    else if ([creationDateObject isKindOfClass:[NSNumber class]])
    {
        self.createDate = [NSDate dateWithTimeIntervalSince1970:[creationDateObject integerValue]];
    }
    else
    {
        self.createDate = nil;
    }

    
    NSArray * commentsDictionaries = [dictionary objectOrNilForKey:TVSocialFeedCommentSubCommentsKey];
    NSMutableArray * subCommentsArray = [NSMutableArray array];
    
    if (commentsDictionaries)
    {
        for (NSDictionary * commentDicaionary in commentsDictionaries)
        {
            TVSocialFeedComment * subComment = [[TVSocialFeedComment alloc] initWithDictionary:commentDicaionary];
            [subCommentsArray addObject:subComment];
            
        }
    }
    
    self.subComments = [NSArray arrayWithArray:subCommentsArray];
}

/*

 */
-(NSString *)description
{
    return [NSString stringWithFormat:@"commentBody = %@ , commentTitle = %@ , creatorName = %@ , createDate = %@ , creatorUrl = %@ , popularityCounter = %ld",self.commentBody,self.commentTitle,self.creatorName,self.createDate,self.creatorUrl,(long)self.popularityCounter];
}
@end
