//
//  TVDomainRegion.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>



@interface TVDomainRegion : BaseModelObject

@property (strong, nonatomic) NSString * regionID;
@property (strong, nonatomic) NSString * regionName;
@property (strong, nonatomic) NSString * regionExternalID;
@property (assign, nonatomic) BOOL isDefault;
@property (strong, nonatomic) NSDictionary * channelsMap;


@end
