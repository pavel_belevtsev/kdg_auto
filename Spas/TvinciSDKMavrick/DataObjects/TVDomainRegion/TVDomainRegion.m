//
//  TVDomainRegion.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVDomainRegion.h"



@implementation TVDomainRegion

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.regionID = [dictionary objectOrNilForKey:@"id"];
    self.regionName = [dictionary objectOrNilForKey:@"name"];
    self.regionExternalID = [dictionary objectOrNilForKey:@"external_id"];
    self.isDefault = [[dictionary objectOrNilForKey:@"is_default"] boolValue];
    self.channelsMap = [dictionary objectOrNilForKey:@"data"];
}

@end
