//
//  UserActionResponse.m
//  TvinciSDK
//
//  Created by Israel on 4/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "UserActionResponse.h"

const NSString *ActionResponseStatusExternKey = @"m_eActionResponseStatusExtern";
const NSString *ActionResponseStatusInternKey = @"m_eActionResponseStatusIntern";

@implementation UserActionResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.externalStatus = [[dictionary objectForKey:ActionResponseStatusExternKey] intValue];
    self.internalStatus = [[dictionary objectForKey:ActionResponseStatusInternKey] intValue];
    
    self.isExternalSecssesd = (self.externalStatus == TVSocialActionResponseStatus_OK) ? YES : NO;
    self.isInternalSecssesd = (self.internalStatus == TVSocialActionResponseStatus_OK) ? YES : NO;
}

-(NSString*)takeExternalFailureReason
{
    return TVNameForSocialActionResponseStatus(self.externalStatus);
}

-(NSString*)takeInternalFailureReason
{
    return TVNameForSocialActionResponseStatus(self.internalStatus);
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"ExternalStatus = %@, InternalStatus = %@",TVNameForSocialActionResponseStatus(self.externalStatus),TVNameForSocialActionResponseStatus(self.internalStatus)];
}

@end
