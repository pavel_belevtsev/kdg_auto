//
//  TVNotification.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//


#define keyNotificationUniqID @"ID"
#define keyNotificationID @"NotificationID"
#define keyNotificationRequestID @"NotificationRequestID"
#define keyNotificationMessageID @"NotificationMessageID"
#define keyuserID @"UserID"
#define keyStatus @"Status"
#define keyType @"Type"
#define keyMessageText @"MessageText"
#define keyTitle @"Title"
#define keyPublishDate @"PublishDate"
#define keyAppName @"AppName"
#define keyDeviceID @"DeviceID"
#define keyUdid @"UdID"
#define keyActions @"Actions"
#define keyViewStatus @"ViewStatus"
#define keyTagNotificationParams @"TagNotificationParams"
#define keyTagDict @"TagDict"
#define keyMediaID @"mediaID"
#define keyPictureURL @"mediaPicURL"
#define keytemplateEmail @"templateEmail"
#define keynGroupID @"nGroupID"
#define keyTagsKey @"Key"
#define keyTagsValues @"Values"



/*
[
 {
     "ID": "68a3c8d8-fc6c-4c38-ae18-9841d948da5e",
     "NotificationID": 6,
     "NotificationRequestID": 16,
     "NotificationMessageID": 454346,
     "UserID": 435557,
     "Status": 1,
     "Type": 1,
     "MessageText": "New episode test 13.08.2013 in the Podcast Serie (host) you are following.",
     "Title": "PodCast (all)",
     "PublishDate": "2000-01-01T00:00:00",
     "AppName": "",
     "DeviceID": 89857,
     "UdID": "1d6415fba59e836749e8026c8fa50f4b979180fd",
     "Actions": null,
     "ViewStatus": 1,
     "TagNotificationParams": {
         "TagDict": [
                     {
                         "Key": "1159",
                         "Values": null
                     }
                     ],
         "mediaID": 243748,
         "mediaPicURL": null,
         "templateEmail": null
     },
     "nGroupID": 134
 }
 ]
 */





#import "TVNotification.h"

@implementation TVNotification


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
   
    self.notificationUniqID = [dictionary objectForKey:keyNotificationUniqID];
    self.notificationUniqID = [dictionary objectForKey:keyNotificationID];
    self.notificationRequestID = [dictionary objectForKey:keyNotificationRequestID];
    self.notificationMessageID = [dictionary objectForKey:keyNotificationMessageID];
    self.userID = [dictionary objectForKey:keyuserID];
//    self.notificationStatus
    self.notificationType = (TVNotificationType)(((NSNumber *)[dictionary objectForKey:keyType]).intValue);
    self.notificationMessage = [dictionary objectForKey:keyMessageText];
    self.notificationTitle = [dictionary objectForKey:keyTitle];
    
    
    
       
    NSString * dateString = [dictionary objectForKey:keyPublishDate];
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-ddTHH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateString];
   

    self.notificationPublishDate = date;
    
    self.appName = [dictionary objectForKey:keyAppName];
    self.deviceID = [dictionary objectForKey:keyDeviceID];
    self.udid = [dictionary objectForKey:keyUdid];
    self.notificationViewStatus = (TVNotificationViewStatus)(((NSNumber *)[dictionary objectForKey:keyViewStatus]).intValue);
    
    NSDictionary * notificationTagsPatams = [dictionary objectForKey:keyTagNotificationParams];
    if (notificationTagsPatams)
    {
        NSArray * tagsArray = [notificationTagsPatams objectForKey:keyTagDict];
        if (tagsArray.count>0)
        {
            NSDictionary * keyValuesDictionary = [tagsArray lastObject];
            self.notificationTags = keyValuesDictionary;
            
        }
        
        self.mediaID = [notificationTagsPatams objectForKey:keyMediaID];
        self.pictureURL = [notificationTagsPatams objectForKey:keyPictureURL];
    }
    
//    self.notificationGroupID
    
}

@end
