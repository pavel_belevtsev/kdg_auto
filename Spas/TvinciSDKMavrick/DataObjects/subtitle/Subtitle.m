//
//  Subtitle.m
//  TvinciSDK
//
//  Created by quickode on 11/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "Subtitle.h"

@implementation Subtitle
@synthesize language = _language;
@synthesize fileURL= _fileURL;

+(Subtitle *) subtitleWithLanguage:(NSString *) language fileURL: (NSURL*) fileURL {
    
    Subtitle * subtitle = [[Subtitle alloc] init];
    subtitle.language = language;
    subtitle.fileURL = fileURL;
    
    return subtitle;
}

@end
