//
//  Subtitle.h
//  TvinciSDK
//
//  Created by quickode on 11/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subtitle : NSObject
@property (strong, nonatomic) NSString * language ;
@property (strong, nonatomic) NSURL * fileURL;

+(Subtitle *) subtitleWithLanguage:(NSString *) language fileURL: (NSURL*) fileURL ;

@end
