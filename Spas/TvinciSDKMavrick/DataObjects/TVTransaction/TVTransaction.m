//
//  TVTransaction.m
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVTransaction.h"
#import "NSDictionary+NSNullAvoidance.h"

#define TRANSACTIONS_DATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS"
#define TRANSACTIONS_DATE_FORMAT_NO_MILLISECONDS @"yyyy-MM-dd'T'HH:mm:ss"

@implementation TVTransaction
@synthesize price = _price;
@synthesize currencyCode = _currencyCode;
@synthesize currencySign = _currencySign;
@synthesize currencyID = _currencyID;
@synthesize receiptCode = _receiptCode;
@synthesize purchasedItemName = _purchasedItemName;
@synthesize purchasedItemCode = _purchasedItemCode;
@synthesize itemType = _itemType;
@synthesize billingAction = _billingAction;
@synthesize actionDate = _actionDate;
@synthesize startDate = _startDate;
@synthesize endDate = _endDate;
@synthesize paymentMethod = _paymentMethod;
@synthesize paymentExtraDetails = _paymentExtraDetails;
@synthesize isRecurring = _isRecurring;
@synthesize billingProviderReference = _billingProviderReference;
@synthesize purchaseID = _purchaseID;
@synthesize remarks = _remarks;

-(void) dealloc
{
}

#pragma mark - Initialization
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    if (dictionary != nil)
    {
        
        // Price
        NSDictionary *priceInfo = [dictionary objectOrNilForKey:TVTransactionPriceKey];
        self.price = [[priceInfo objectOrNilForKey:TVTransactionActualPriceKey] floatValue];
        // Currency Info
        NSDictionary *currencyInfo = [priceInfo objectOrNilForKey:TVTransactionCurrencyKey];
        self.currencyCode = [currencyInfo objectOrNilForKey:TVTransactionCurrencyCodeKey];
        self.currencySign = [currencyInfo objectOrNilForKey:TVTransactionCurrencySignKey];
        self.currencyID = [[currencyInfo objectOrNilForKey:TVTransactionCurrencyIDKey] integerValue];
        // Other details
        self.receiptCode = [dictionary objectOrNilForKey:TVTransactionRecieptCodeKey];
        self.purchasedItemName = [dictionary objectOrNilForKey:TVTransactionPurchasedItemNameKey];
        self.purchasedItemCode = [dictionary objectOrNilForKey:TVTransactionPurchasedItemCodeKey];
        self.itemType = (TVBillType)[[dictionary objectOrNilForKey:TVTransactionItemTypeKey] integerValue];
        self.billingAction = [[dictionary objectOrNilForKey:TVTransactionBillingActionKey] integerValue];
        
        // Date
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = TRANSACTIONS_DATE_FORMAT;
        NSString *actionDateString = [dictionary objectOrNilForKey:TVTransactionActionDateKey];
        self.actionDate = [formatter dateFromString:actionDateString];
        NSString *startDateString = [dictionary objectOrNilForKey:TVTransactionStartDateKey];
        self.startDate  = [formatter dateFromString:startDateString];
        NSString *endDateString = [dictionary objectOrNilForKey:TVTransactionEndDateKey];
        self.startDate  = [formatter dateFromString:endDateString];

        
        self.paymentMethod = [[dictionary objectOrNilForKey:TVTransactionPaymentMethodKey] integerValue];
        self.paymentExtraDetails = [dictionary objectOrNilForKey:TVTransactionPaymentMethodExtraDetailKey];
        self.isRecurring = [[dictionary objectOrNilForKey:TVTransactionIsRecurringHistoryKey] boolValue];
        self.billingProviderReference = [[dictionary objectOrNilForKey:TVTransactionBillingProviderReferenceKey] integerValue];
        self.purchaseID = [[dictionary objectOrNilForKey:TVTransactionPurchaseIDKey] integerValue];
        self.remarks = [dictionary objectOrNilForKey:TVTransactionRemarksKey];
    }
}
@end

NSString *const TVTransactionPriceKey = @"m_Price";
NSString *const TVTransactionIsRecurringHistoryKey = @"m_bIsRecurring";
NSString *const TVTransactionActionDateKey = @"m_dtActionDate";
NSString *const TVTransactionEndDateKey = @"m_dtEndDate";
NSString *const TVTransactionStartDateKey = @"m_dtStartDate";
NSString *const TVTransactionBillingActionKey = @"m_eBillingAction";
NSString *const TVTransactionItemTypeKey = @"m_eItemType";
NSString *const TVTransactionPaymentMethodKey = @"m_ePaymentMethod";
NSString *const TVTransactionBillingProviderReferenceKey = @"m_nBillingProviderRef";
NSString *const TVTransactionPurchaseIDKey = @"m_nPurchaseID";
NSString *const TVTransactionPaymentMethodExtraDetailKey = @"m_sPaymentMethodExtraDetails";
NSString *const TVTransactionPurchasedItemCodeKey = @"m_sPurchasedItemCode";
NSString *const TVTransactionPurchasedItemNameKey = @"m_sPurchasedItemName";
NSString *const TVTransactionRecieptCodeKey = @"m_sRecieptCode";
NSString *const TVTransactionRemarksKey = @"m_sRemarks";
NSString *const TVTransactionActualPriceKey = @"m_dPrice";
NSString *const TVTransactionCurrencyKey = @"m_oCurrency";
NSString *const TVTransactionCurrencyCodeKey = @"m_sCurrencyCD3";
NSString *const TVTransactionCurrencySignKey = @"m_sCurrencySign";
NSString *const TVTransactionCurrencyIDKey = @"m_nCurrencyID";