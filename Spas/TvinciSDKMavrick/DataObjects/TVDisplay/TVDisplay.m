//
//  TVDisplay.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVDisplay.h"

@implementation TVDisplay


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    NSString * logoURL = [[dictionary objectForKey:@"UIData"] objectForKey:@"picURL"];
    NSString * colorCode = [[dictionary objectForKey:@"UIData"] objectForKey:@"ColorCode"];
    NSInteger operatorID = [[dictionary objectForKey:@"operatorID"] integerValue];
    self.displayColor = colorCode;

    if (logoURL.length ==0)
    {
        if (operatorID %2 == 0)
        {
//#warning Hardcoded
            self.displayTextColor = @"#652900";
        }
        else
        {
            self.displayTextColor = @"#FFFFFF";
        }
    }
    self.operatorID =operatorID;
    self.displayLogoURL = [NSURL URLWithString:logoURL];
}

-(void)dealloc{
    

}

-(NSString *) description
{
    return [NSString stringWithFormat:@"<%@ %p logo %@  color %@",[self class], self,self.displayLogoURL.absoluteString,self.displayColor];
}


@end
