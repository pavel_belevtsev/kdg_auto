//
//  TVService.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import "TVConstants.h"

@interface TVService : BaseModelObject

@property (assign, nonatomic) TVCommercializationServiceType serviceID;
@property (strong, nonatomic) NSString * serviceName;
@end
