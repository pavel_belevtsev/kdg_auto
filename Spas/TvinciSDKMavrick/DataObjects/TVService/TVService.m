//
//  TVService.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVService.h"

@implementation TVService

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.serviceID = [[dictionary objectOrNilForKey:@"id"] intValue];
    self.serviceName = [dictionary objectOrNilForKey:@"name"];
}

-(NSDictionary *) keyValueRepresentation
{
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObjectOrNil:[NSString stringWithFormat:@"%d",self.serviceID] forKey:@"id"];
    [dict setObjectOrNil:self.serviceName forKey:@"name"];
    return [NSDictionary dictionaryWithDictionary:dict];
}

-(BOOL)isEqual:(TVService *)object
{
    return (self.serviceID == object.serviceID && [self.serviceName isEqualToString:object.serviceName]);
}
@end
