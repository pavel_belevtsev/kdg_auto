//
//  TVSocialUtils.h
//  TvinciSDK
//
//  Created by Tarek Issa on 1/15/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//



@class TVMediaItem;
@class TVPAPIRequest;


@interface TVSocialUtils : NSObject

+ (TVPAPIRequest *)RequestForDoLikeForMediaItem : (TVMediaItem*)mediaItem delegate : (id) delegate;

@end
