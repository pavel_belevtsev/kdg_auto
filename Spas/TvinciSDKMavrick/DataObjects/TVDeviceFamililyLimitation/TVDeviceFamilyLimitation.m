//
//  TVDeviceFamililyLimitation.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVDeviceFamilyLimitation.h"

@implementation TVDeviceFamilyLimitation


-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.deviceFamily = [[dictionary objectOrNilForKey:@"device_family"] integerValue];
    self.deviceFamilyName = [dictionary objectOrNilForKey:@"device_family_name"] ;
    self.concurrency  = [[dictionary objectOrNilForKey:@"concurrency"] integerValue] ;
    self.quantity = [[dictionary objectOrNilForKey:@"quantity"]  integerValue];
    self.frequency = [[dictionary objectOrNilForKey:@"frequency"]  integerValue];
    
}

@end
