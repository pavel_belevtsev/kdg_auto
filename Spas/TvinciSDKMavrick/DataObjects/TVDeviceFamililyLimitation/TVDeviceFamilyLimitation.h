//
//  TVDeviceFamililyLimitation.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVDeviceFamilyLimitation : BaseModelObject
@property (assign, nonatomic) NSInteger deviceFamily;
@property (strong, nonatomic) NSString * deviceFamilyName;
@property (assign, nonatomic) NSInteger  concurrency;
@property (assign, nonatomic) NSInteger  quantity;
@property (assign, nonatomic) NSInteger  frequency;

@end
