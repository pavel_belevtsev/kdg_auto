//
//  TVAssetsResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVAssetsResponse.h"
#import "TVAsset.h"
#import "TVResponseStatus.h"
@implementation TVAssetsResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    NSArray * assetDicaionries = [dictionary objectForKey:@"assets"];
    NSMutableArray * assetsArray = [NSMutableArray array];
    for (NSDictionary * assetDictionary in assetDicaionries)
    {
        TVAsset * asset = [[TVAsset alloc] initWithDictionary:assetDictionary];
        [assetsArray addObject:asset];
    }

    self.arrayAssets = assetsArray;

    self.totalItems = [[dictionary objectForKey:@"total_items"] integerValue];
    self.responseStatus = [[TVResponseStatus alloc] initWithDictionary:[dictionary objectForKey:@"status"]];

}
@end
