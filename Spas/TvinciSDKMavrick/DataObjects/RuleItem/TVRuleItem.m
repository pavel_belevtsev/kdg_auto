//
//  RuleItem.m
//  TvinciSDK
//
//  Created by iosdev1 on 11/20/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVRuleItem.h"

NSString const * TVParentalRuleBlockType        = @"BlockType";
NSString const * TVParentalRuleDynamicDataKey   = @"DynamicDataKey";
NSString const * TVParentalRuleIsActive         = @"IsActive";
NSString const * TVParentalRuleTagValue         = @"TagValue";
NSString const * TVParentalRuleName             = @"Name";
NSString const * TVParentalRuleGroupRuleType    = @"GroupRuleType";
NSString const * TVParentalRuleAllTagValues     = @"AllTagValues";
NSString const * TVParentalRuleRuleID           = @"RuleID";
NSString const * TVParentalRuleTagTypeID        = @"TagTypeID";

@interface TVRuleItem ()
{
    
}

@property (assign, readwrite) BlockType blockType;
@property (assign, readwrite) NSInteger groupRuleType;
@property (assign, readwrite) NSInteger ruleId;
@property (assign, readwrite) NSInteger tagTypeId;
@property (assign, readwrite) BOOL isActive;

@property (strong, readwrite) NSString* dynamicDataKey;
@property (strong, readwrite) NSString* tagValue;
@property (strong, readwrite) NSString* ruleName;
@property (strong, readwrite) NSArray* tagValues;

@end

@implementation TVRuleItem

-(void)dealloc{
    self.dynamicDataKey = nil;
    self.tagValue       = nil;
    self.ruleName       = nil;
    self.tagValues      = nil;
  
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.groupRuleType  = [[dictionary objectOrNilForKey:TVParentalRuleGroupRuleType] integerValue];
    self.blockType      = (BlockType)[[dictionary objectOrNilForKey:TVParentalRuleBlockType] integerValue];
    self.tagTypeId      = [[dictionary objectOrNilForKey:TVParentalRuleTagTypeID] integerValue];
    self.ruleId         = [[dictionary objectOrNilForKey:TVParentalRuleRuleID] integerValue];
    self.isActive       = [[dictionary objectOrNilForKey:TVParentalRuleIsActive] boolValue];
    self.dynamicDataKey = [dictionary  objectOrNilForKey:TVParentalRuleDynamicDataKey];
    self.tagValues      = [dictionary  objectOrNilForKey:TVParentalRuleAllTagValues];
    self.tagValue       = [dictionary  objectOrNilForKey:TVParentalRuleTagValue];
    self.ruleName       = [dictionary  objectOrNilForKey:TVParentalRuleName];
}



@end

