//
//  RuleItem.h
//  TvinciSDK
//
//  Created by iosdev1 on 11/20/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "BaseModelObject.h"

typedef enum{
    NC16=0,
    M18,
    R21
}RoulsTypes;

typedef enum{
    BlockType_Allowed       = 0,
    BlockType_Validation    = 1,
    BlockType_AgeBlock      = 2,
    BlockType_GeoBlock      = 3,
    BlockType_DeviceBlock   = 4,
    BlockType_UserTypeBlock = 5
}BlockType;


@interface TVRuleItem : BaseModelObject {
    
}

@property (assign, readonly) BlockType blockType;
@property (assign, readonly) BOOL isActive;
@property (assign, readonly) NSInteger groupRuleType;
@property (assign, readonly) NSInteger ruleId;
@property (assign, readonly) NSInteger tagTypeId;

@property (strong, readonly) NSString* dynamicDataKey;
@property (strong, readonly) NSString* tagValue;
@property (strong, readonly) NSString* ruleName;
@property (strong, readonly) NSArray* tagValues;


@end

//extern NSString *const TVParentalRuleBlockType;
//extern NSString *const TVParentalRuleDynamicDataKey;
//extern NSString *const TVParentalRuleIsActive;
//extern NSString *const TVParentalRuleTagValue;
//extern NSString *const TVParentalRuleName;
//extern NSString *const TVParentalRuleGroupRuleType;
//extern NSString *const TVParentalRuleAllTagValues;
//extern NSString *const TVParentalRuleRuleID;
//extern NSString *const TVParentalRuleTagTypeID;
