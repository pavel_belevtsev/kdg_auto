//
//  TVSubscriptionPrice.m
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVSubscriptionPrice.h"
#import "TVSingleItemPrice.h"

@implementation TVSubscriptionPrice

#define TVSubscriptionPrice_PriceReason         @"m_PriceReason"
#define TVSubscriptionPrice_oPrice              @"m_oPrice"
#define TVSubscriptionPrice_sSubscriptionCode   @"m_sSubscriptionCode"

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    TVLogDebug(@"[TVSubscriptionPrice][Attributes] %@",dictionary);
    
    _priceReason  = [[dictionary objectOrNilForKey:TVSubscriptionPrice_PriceReason] integerValue];
    _price = [[PriceObj alloc] initWithDictionary: [dictionary objectOrNilForKey:TVSubscriptionPrice_oPrice]];
    _subscriptionCode = [dictionary objectOrNilForKey:TVSubscriptionPrice_sSubscriptionCode];
}

@end
