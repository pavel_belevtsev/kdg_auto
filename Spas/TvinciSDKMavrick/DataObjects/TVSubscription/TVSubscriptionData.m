//
//  TVSubscriptionData.m
//  TvinciSDK
//
//  Created by Alex Zchut on 2/21/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVSubscriptionData.h"

@implementation TVSubscriptionData


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sCodes": @"codes",
           @"m_dStartDate": @"stringStartDate",
           @"m_dEndDate": @"stringEndDate",
           @"m_sFileTypes": @"fileTypes",
           @"m_bIsRecurring": @"isRecurring",
           @"m_nNumberOfRecPeriods": @"numberOfRecPeriods",
           @"m_bIsRecurring": @"isRecurring",
           @"m_oSubscriptionPriceCode": @"subscriptionPriceCode",
           @"m_sName": @"arrName",
           @"m_oSubscriptionUsageModule": @"subscriptionUsageModule",
           @"m_Priority": @"priority",
           @"m_ProductCode": @"productCode",
           @"m_SubscriptionCode": @"subscriptionCode",
           @"m_oPriceCode": @"priceCode",
           @"m_oUsageModule": @"usageModule",
           @"m_oDiscountModule": @"discountModule",
           @"m_oCouponsGroup": @"couponsGroup",
           @"m_sDescription": @"arrDescription",
           @"m_sObjectCode": @"objectCode",
           @"m_sObjectVirtualName": @"objectVirtualName",
           @"m_bSubscriptionOnly": @"subscriptionOnly"
           }];
}

@end

@implementation TVSubscriptionData_subscriptionUsageModule

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
       @"m_bWaiver": @"waiver",
       @"m_coupon_id": @"couponId",
       @"m_sVirtualName": @"virtualName",
       @"m_device_limit_id": @"deviceLimitId",
       @"m_ext_discount_id": @"extDiscountId",
       @"m_internal_discount_id": @"internalDiscountId",
       @"m_is_renew": @"isRenew",
       @"m_nMaxNumberOfViews": @"maxNumberOfViews",
       @"m_nObjectID": @"objectId",
       @"m_nWaiverPeriod": @"waiverPeriod",
       @"m_num_of_rec_periods": @"numOfRecPeriods",
       @"m_pricing_id": @"pricingId",
       @"m_sVirtualName": @"virtualName",
       @"m_subscription_only": @"isSubscriptionOnly",
       @"m_tsMaxUsageModuleLifeCycle": @"maxUsageModuleLifeCycle",
       @"m_tsViewLifeCycle": @"viewLifeCycle",
       @"m_type": @"type",
       @"m_bIsOfflinePlayBack": @"isOfflinePlayback"
   }];
}

@end

@implementation TVSubscriptionData_subscriptionPriceCode

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sCode": @"code",
           @"m_sName": @"name",
           @"m_oPrise": @"prise",
           @"m_nObjectID": @"objectId",
           @"m_sDescription": @"description1"
           }];
}

@end

@implementation TVSubscriptionData_prise

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_dPrice": @"price",
           @"m_sName": @"name",
           @"m_oCurrency": @"currency"
           }];
}

@end

@implementation TVSubscriptionData_prise_currency

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sCurrencyCD3": @"currencyCD3",
           @"m_sCurrencyCD2": @"currencyCD2",
           @"m_sCurrencySign": @"currencySign",
           @"m_nCurrencyID": @"currencyId"
           }];
}

@end

@implementation TVSubscriptionData_name

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sLanguageCode3": @"languageCode3",
           @"m_sValue": @"value"
           }];
}
@end

@implementation TVSubscriptionData_codes

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sCode": @"code",
           @"m_sName": @"name"
           }];
}
@end


@interface TVSubscriptionData_purchasingInfo ()
@property (nonatomic, strong) NSDate *lastViewDate, *currentDate, *purchaseDate, *endDate;
@end

@implementation TVSubscriptionData_purchasingInfo

#define _P_dateFormat @"yyyy-MM-dd HH:mm:ss"
#define _P_dateFormat2 @"yyyy-MM-dd HH:mm:ss.SSS"

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
           @"m_sSubscriptionCode": @"subscriptionCode",
           @"m_nMaxUses": @"maxUses",
           @"m_nCurrentUses": @"currentUses",
           @"m_dEndDate": @"stringEndDate",
           @"m_dCurrentDate": @"stringCurrentDate",
           @"m_dLastViewDate": @"stringLastViewDate",
           @"m_dPurchaseDate": @"stringPurchaseDate",
           @"m_bRecurringStatus": @"recurringStatus",
           @"m_bIsSubRenewable": @"isSubRenewable",
           @"m_nSubscriptionPurchaseID": @"subscriptionPurchaseId",
           @"m_paymentMethod": @"paymentMethod",
           @"m_sDeviceUDID": @"dviceUDID",
           @"m_sDeviceName": @"deviceName"
           }];
}

- (NSDate*) endDate {
    if (!_endDate) {
        _endDate = [self dateFromString:_stringEndDate];
    }
    return _endDate;
}

- (NSDate*) lastViewDate {
    if (!_lastViewDate) {
        _lastViewDate = [self dateFromString:_stringLastViewDate];
    }
    return _lastViewDate;
}

- (NSDate*) purchaseDate {
    if (!_purchaseDate) {
        _purchaseDate = [self dateFromString:_stringPurchaseDate];
    }
    return _purchaseDate;
}

- (NSDate*) currentDate {
    if (!_currentDate) {
        _currentDate = [self dateFromString:_stringCurrentDate];
    }
    return _currentDate;
}


- (NSDate*) dateFromString:(NSString*) string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = _P_dateFormat;
    NSString *dateString = [NSString stringWithString:string];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    NSDate *date = [formatter dateFromString:dateString];
    if (!date) {
        formatter.dateFormat = _P_dateFormat2;
        date = [formatter dateFromString:dateString];
    }
    
    if (date) {
        NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
        date = [NSDate dateWithTimeInterval:[destinationTimeZone secondsFromGMT] sinceDate:date];
    }
    return date;
}
@end
