//
//  TVParentalPinResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVPinCodeResponse.h"

@implementation TVPinCodeResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];

    self.parentalPinCode = [dictionary objectForKey:@"pin"];
    self.level = (TVRuleDefinedLevel)[[dictionary objectForKey:@"level"] integerValue];
}
@end
