//
//  TVGenericRule.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVGenericRule.h"

//{
//    "id": 0,
//    "rule_type": 1,
//    "name": "israel",
//    "description": ""
//}

@implementation TVGenericRule

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.ruleId = [[dictionary objectForKey:@"id"] integerValue];
    self.ruleType = (TVRuleType)[[dictionary objectForKey:@"rule_type"] integerValue];
    self.ruleName = [dictionary objectForKey:@"name"];
    self.ruleDescription = [dictionary objectForKey:@"description"];
}

@end
