//
//  TVLicencedDataResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import "TVResponseStatus.h"

@class TVLicencedLink;
@class TVResponseStatus;

@interface TVLicencedDataResponse : BaseModelObject

@property (strong, nonatomic) TVLicencedLink * licencedLink;
@property (strong, nonatomic) TVResponseStatus * responseStatus;
@end
