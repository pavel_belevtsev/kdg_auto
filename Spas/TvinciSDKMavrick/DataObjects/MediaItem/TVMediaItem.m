//
//  TVMediaItem.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//




#import "TVMediaItem.h"
#import "NSDate+Format.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVPictureSize.h"
#import "TVinciUtils.h"
#import "TVConfigurationManager.h"
#import "SmartLogs.h"
#import "NSString+SGParse.h"
#import "TVSessionManager.h"
#import "SmartLogs.h"
#import "TVJSONCreator.h"
#import "TVConstants.h"
#import "NSArray+FirstObject.h"
#import "TVImage.h"
#import "NSDate+Format.h"
#import "TVDeviceScreenUtils.h"

NSString *const MediaItemTotalItemsKey = @"TotalItems";
NSString *const MediaItemCatalogStartDateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
NSString *const MediaItemIDKey = @"MediaID";
NSString *const MediaItemNameKey = @"MediaName";
NSString *const MediaItemCatalogStartDateKey = @"CatalogStartDate";
NSString *const MediaItemTypeIDKey = @"MediaTypeID";
NSString *const MediaItemTypeNameKey = @"MediaTypeName";
NSString *const MediaItemRatingKey = @"Rating";
NSString *const MediaItemViewCounterKey = @"ViewCounter";
NSString *const MediaItemDescriptionKey = @"Description";
NSString *const MediaItemCreationDateKey = @"CreationDate";
NSString *const MediaItemStartDateKey = @"StartDate";
NSString *const MediaItemPictureURLKey = @"PicURL";
NSString *const MediaItemURLKey = @"URL";
NSString *const MediaItemWebLinkKey = @"MediaWebLink";
NSString *const MediaItemDurationKey = @"Duration";
NSString *const MediaItemFileIDKey = @"FileID";
NSString *const MediaItemDynamicDataKey = @"MediaDynamicData";
NSString *const MediaItemIsFavoriteKey = @"IsFavorite";
NSString *const MediaItemPriceKey = @"Price";
NSString *const MediaItemMarkKey = @"MediaMark";
NSString *const MediaItemPriceTypeKey = @"PriceType";
NSString *const MediaItemNotificationKey = @"Notification";
NSString *const MediaItemExpirationDateKey = @"ExpirationDate";
NSString *const MediaItemSubDurationKey = @"SubDuration";
NSString *const MediaItemSubFileFormatKey = @"SubFileFormat";
NSString *const MediaItemSubFileIDKey = @"SubFileID";
NSString *const MediaItemSubURLKey = @"SubURL";
NSString *const MediaItemTagsKey = @"Tags";
NSString *const MediaItemMetaDataKey = @"Metas";
NSString *const TVMediaItemFilesKey = @"Files";
NSString *const TVMediaItemLikeCounterKey = @"like_counter";
NSString *const TVMediaItemAdvertisingParametersKey = @"AdvertisingParameters";
NSString *const MediaItemPicturesKey = @"Pictures";
NSString *const TVMediaItemTagTitleKey = @"Key";
NSString *const TVMediaItemTagValuesKey = @"Value";

NSString *const EPGEpg_id =@"epg_id";

NSString *const TVMediaItemExternalID = @"ExternalIDs";

NSString *const TVMediaItemBaseID = @"Base ID";

// used for startOver
NSString *const TVMediaItemAttributesDictionary = @"TVMediaItemAttributesDictionary";

@interface TVMediaItem ()
@property (nonatomic, copy , readwrite) NSString *mediaID;
@property (nonatomic, copy , readwrite) NSString *mediaTypeName;
@property (nonatomic, copy , readwrite) NSString *mediaTypeID;

@property (nonatomic ,strong, readwrite) TVFile *trailerFile;
@property (nonatomic ,strong, readwrite) TVFile *mainFile;
@property (nonatomic ,strong, readwrite) TVFile *widevineFile;
@property (nonatomic ,strong, readwrite) TVFile *subtitleFile;


-(NSDictionary *) parseTags:(NSArray *)tags;
-(NSDictionary *) parseMetas:(NSArray *)metas;
-(NSArray *) parseFileAsDictionary : (NSArray *) filesAsDictionary;
@end

@implementation TVMediaItem
@synthesize name = _name;
@synthesize mediaID = _mediaID;
@synthesize mediaDescription = _mediaDescription;
@synthesize tags = _tags;
@synthesize mediaTypeID = _mediaTypeID;
@synthesize rating = _rating;
@synthesize viewCounter = _viewCounter;
@synthesize creationDate = _creationDate;
@synthesize StartDate = _StartDate;
@synthesize catalogStartDate = _catalogStartDate;
@synthesize pictureURL = _pictureURL;
@synthesize duration = _duration;
@synthesize price = _price;
@synthesize expirationDate = _purchaseExpirationDate;
@synthesize mediaWebLink = _mediaWebLink;
@synthesize mediaMark = _mediaMark;
@synthesize mediaTypeName = _mediaTypeName;
@synthesize isMediaLike = _isMediaLike;
@synthesize files = _files;
@synthesize isFavorite = _isFavorite;
@synthesize priceType = _priceType;
@synthesize mainFile = _mainFile;
@synthesize widevineFile = _widevineFile;
@synthesize trailerFile = _trailerFile;
@synthesize subtitleFile = _subtitleFile;
@synthesize likeCounter = _likeCounter;
@synthesize URL = _URL;
@synthesize fileID = _fileID;
@synthesize subtitles =_subtitles;

// only for Epg
@synthesize EPGDuration;
@synthesize EPGEndCurrectPrograme;
@synthesize EPGStartCurrectPrograme;
@synthesize ExternalIDs;

// for recognizing data state of media item
@synthesize mediaItemRequestType = _mediaItemRequestType;


#pragma mark - Memory
- (void)dealloc
{
    self.URL = nil;
    self.fileID = nil;
    self.files = nil;
    self.name = nil;
    self.mediaID = nil;
    self.mediaDescription = nil;
    self.tags = nil;
    self.metaData = nil;
    self.mediaTypeID = nil;
    self.creationDate = nil;
    self.catalogStartDate = nil;
    self.pictureURL = nil;
    self.price = nil;
    self.mediaWebLink = nil;
    self.mediaTypeName = nil;
    self.mainFile = nil;
    self.trailerFile = nil;
    self.widevineFile = nil;
    self.subtitleFile = nil;
    self.StartDate = nil;
}



#pragma mark - pictures
-(NSURL *) pictureURLForPictureSize : (TVPictureSize *) size
{
    NSURL *toReturn = nil;
    if (size != nil)
    {
        NSRegularExpression *pictureSizeRegex = [NSRegularExpression regularExpressionWithPattern:@"(\\d+X\\d+)|(full)" options:0 error:NULL];
        if (pictureSizeRegex != nil)
        {
            NSString *absoluteString = [self.pictureURL absoluteString];
            if (absoluteString != nil)
            {
                NSString *replacement = [pictureSizeRegex stringByReplacingMatchesInString:absoluteString options:0 range:NSMakeRange(0, absoluteString.length) withTemplate:size.name];
                toReturn = [NSURL URLWithString:replacement];
            }
        }
    }
    return toReturn;
}

- (double)ratioBySize:(CGSize)size {
    double num = size.width/size.height;
    return num;
}

- (BOOL)isRatio:(double)newImageRatio isCloserOrEqualToRatio:(double)ratio thanRatio:(double)savedRatio {
    
    //  Only if found ratio smaller or equal to the saved ratio then return YES
    if (fabs(newImageRatio - ratio) <= fabs(savedRatio - ratio)) {
        return YES;
    }
    return NO;
}

- (BOOL)isSize:(CGSize)newSize isCloserTo:(CGSize)size thanSavedSize:(CGSize)savedSize {
    
    //  If both are bigger or both are smaller than size then have to determine which one is closer
    if (((newSize.width > size.width)&&(savedSize.width > size.width)) ||  ((newSize.width < size.width)&&(savedSize.width < size.width))) {
        BOOL newSizeIsCloser = fabs(newSize.width - size.width) <= fabs(savedSize.width - size.width);
        return newSizeIsCloser;
    }
    
    //  So surely one is bigger than wanted size and the other is smaller
    if (newSize.width > size.width) {
        return YES;
    }
    
    return NO;
}


-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size
{
    CGSize retinaSize;
    BOOL isRetina = [TVDeviceScreenUtils isRetina];
    retinaSize.width = isRetina?size.width*2.0:size.width;
    retinaSize.height = isRetina? size.height*2.0:size.height;
    return [self pictureURLForSize:retinaSize];
}

-(NSURL *) pictureURLForSize:(CGSize)size
{
    //  If there are no pictures so just return nil.
    if (self.pictures.count == 0) {
        return nil;
    }
    
    //  Array to store all ratios.
    NSMutableArray * ratioArray = nil;
    
    //  Store the ratio of the current image
    double currentRatio     = [self ratioBySize:size];
    /*     Initializations - End    */
    
    //  Run on all of the existing pictures and save them sorted in array by ratio value.
    for(TVImage *nextImage in self.pictures) {
        
        //  For first time just save nextImage
        if (!ratioArray) {
            ratioArray = [NSMutableArray array];
            [ratioArray addObject:nextImage];
            continue;
        }
        
        TVImage* savedImage      = [ratioArray lastObject];
        
        //   If nextImage's ratio is closer or equal to the wanted ratio than the last saved one, so emty array and save a new ratio.
        if ([self isRatio:[nextImage ratio]
   isCloserOrEqualToRatio:currentRatio
                thanRatio:[savedImage ratio]]) {
            
            //  Only if found a closer ratio (Not equal) then emty the array.
            if ([nextImage ratio] != [savedImage ratio]) {
                [ratioArray removeAllObjects];
            }
            [ratioArray addObject:nextImage];
        }
    }
    
    
    //  Should find the right size from all found ratios.
    TVImage* resultImage = [ratioArray lastObject];
    
    for (TVImage* image in ratioArray) {
        if ([self isSize:image.imageSize isCloserTo:size thanSavedSize:resultImage.imageSize]) {
            resultImage = image;
        }
    }
    //TVLogDebug(@"Looking for: %f X %f  ratio: %f", size.width, size.height, [self ratioBySize:size] );
    //for (TVImage* image in self.pictures) {
        //   TVLogDebug(@"%f X %f  ratio %f", image.imageSize.width, image.imageSize.height, [image ratio]);
    //}
    // TVLogDebug(@"And found  %f X %f  ratio %f", resultImage.imageSize.width, resultImage.imageSize.height, [resultImage ratio]);
    
    //  Means the size we are looking for is bigger than all existing sizes so just return the image with biggest ratio.
    return resultImage.imageUrl;
}


#pragma mark - Getters (Files)

-(NSString *) fileExtantionOfFilePath:(NSString *)filePath{
    
    NSArray * compennents = [filePath componentsSeparatedByString:@"."];
    return [compennents lastObject];
}

-(BOOL) fileHasExtentionOfSmil:(NSString *) fileName
{
    NSArray * compennents = [fileName componentsSeparatedByString:@"."];
    
    NSRange range = [[compennents lastObject] rangeOfString:@"smil"];
    
    if(range.location != NSNotFound)
    {
        return YES;
    }else
    {
        return NO;
    }
}


-(BOOL) fileHasExtentionOfWidevine:(NSString *) fileName
{
    NSArray * compennents = [fileName componentsSeparatedByString:@"."];
    
    NSRange range = [[compennents lastObject] rangeOfString:@"wvm"];
    NSRange range1 = [[compennents lastObject] rangeOfString:@"m3u8"];
    
    if(range.location != NSNotFound || range1.location  != NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


-(TVFile *) subtitleFile {
    
    if (_subtitleFile == nil)
    {
        if([self fileHasExtentionOfSmil:self.mainFile.fileURL.absoluteString])
        {
            self.subtitleFile = self.mainFile;
        }
        
    }
    
    return _subtitleFile;
}

-(TVFile *) trailerFile
{
    
    if (_trailerFile == nil)
    {
        NSString *trailerFilerFormatKey = TVConfigTrailerFileFormatKey;
        
        if([[TVSessionManager sharedTVSessionManager] hasAddedDataForKey:kSessionAddedData_MediaItemKey_iPhoneTrailer]){
            trailerFilerFormatKey = [[[TVSessionManager sharedTVSessionManager] takeAddedDataForKey:kSessionAddedData_MediaItemKey_iPhoneTrailer] firstObject];
        }
        
        NSString *trailerFileFormat = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectOrNilForKey:trailerFilerFormatKey];
        
        NSInteger index = [self.files indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVFile *file = obj;
            return [file.format isEqualToString:trailerFileFormat];
        }];
        
        if (index < self.files.count)
        {
            self.trailerFile = [self.files objectAtIndex:index];
            
        }
    }
    
    return _trailerFile;
}


-(TVFile *) mainFile
{
    
    // Checking if to use widevineFile
    TVFile *wvFile = self.widevineFile;
    
    
    if(wvFile != nil ){
        NSString * fileExtantion = [self fileExtantionOfFilePath:[wvFile.fileURL absoluteString]];
        
        if([fileExtantion isEqualToString:@"wvm"] || [fileExtantion isEqualToString:@"m3u8"])
        {
            return wvFile;
        }
    }
    
    
    if (_mainFile == nil)
    {
        
        NSString *mainFileFormat = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectOrNilForKey:TVConfigMainFileFormatKey];
        
        // Finding the file
        
        NSInteger index = [self validMainFileIndexOrNSNotFound];
        if (index != NSNotFound && index < self.files.count)
        {
            // Have Main File
            
            
            TVFile * file = [self.files objectAtIndex:index];
            self.mainFile = file;

            
        }
        else
        {
            // No Main File in the files array, but maybe this media item uses the seprecated "fileID" and "URL" properties
            if (self.fileID.length > 0)
            {
                _mainFile = [[TVFile alloc] init];
                _mainFile.fileID = self.fileID;
                _mainFile.fileURL = self.URL;
                _mainFile.format = mainFileFormat;
                _mainFile.duration = self.duration;
            }
        }
    }
    return _mainFile;
}

-(TVFile *) widevineFile
{
    if (_widevineFile == nil)
    {
        
        
        // Finding the file
        
        NSInteger index = [self widevineValidFileIndexOrNSNotFound];
        
        // updating if found
        if (index != NSNotFound && index < self.files.count)
        {
            // Have Main File
            self.widevineFile = [self.files objectAtIndex:index];
            
            if([self fileHasExtentionOfWidevine:[self.widevineFile.fileURL absoluteString]]  == NO &&
               [self fileHasExtentionOfSmil:[self.widevineFile.fileURL absoluteString]]  == NO){
                self.widevineFile = nil;
            }
            
            
            
        }
        else
        {
            
        }
    }
    return _widevineFile;
}

#pragma mark - media files validation

-(NSInteger) validMainFileIndexOrNSNotFound{
    
    NSString *mainFileFormat = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectOrNilForKey:TVConfigMainFileFormatKey];
    
    NSInteger index = [self.files indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        TVFile *file = obj;
        return [file.format isEqualToString:mainFileFormat];
    }];
    
    if (index != NSNotFound && index < self.files.count)
    {
        return index;
    }
    
    return NSNotFound;
}

-(NSInteger) widevineValidFileIndexOrNSNotFound{
    
    // shefyg - changing the mainFile way of taking formats for taking the string key if needed
    
    // new code - checking for different file type key for iPad and iPhone
    
    NSString *configWidevineFileFormatKeyByPlatform;
    if([TVSessionManager sharedTVSessionManager].appTypeUsed == TVAppType_iPad2){
        configWidevineFileFormatKeyByPlatform = TVConfigWidevineFileFormatKeyForIPad;
    }
    else if([TVSessionManager sharedTVSessionManager].appTypeUsed == TVAppType_iPhone){
        configWidevineFileFormatKeyByPlatform = TVConfigWidevineFileFormatKeyForIPhone;
    }
    else{
        configWidevineFileFormatKeyByPlatform = TVConfigWidevineFileFormatKeyForIPhone;
    }
    
    
    NSString *wvFileFormat = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectOrNilForKey:configWidevineFileFormatKeyByPlatform];
    if(wvFileFormat == nil){
        wvFileFormat = configWidevineFileFormatKeyByPlatform;
    }
    
    // old code
    //    NSString *wvFileFormat = [[TVConfigurationManager sharedTVConfigurationManager].fileFormatNames objectOrNilForKey:TVConfigWidevineFileFormatKey];
    //    if(wvFileFormat == nil){
    //        wvFileFormat = TVConfigWidevineFileFormatKey;
    //    }
    
    
    
    
    // checking for file with format
    NSInteger index = [self.files indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        TVFile *file = obj;
        // checking format as well as url
        BOOL isWidevineFormat = [file.format isEqualToString:wvFileFormat];
        BOOL isValidUURLPath = (file.fileURL != nil && [file.fileURL.path isEqualToString:@""] == NO);
        
        return isWidevineFormat && isValidUURLPath;
    }];
    
    // updating if found
    if (index != NSNotFound && index < self.files.count)
    {
        return index;
    }
    
    return NSNotFound;
    
    
}

-(BOOL) hasValidWidevineFileOrMainFile{
    
    BOOL hasValidMainFile= ([self validMainFileIndexOrNSNotFound] != NSNotFound) ;
    BOOL hasValidWidevineFile= ([self widevineValidFileIndexOrNSNotFound] != NSNotFound);
    
    
    return (hasValidMainFile || hasValidWidevineFile);
}



#pragma mark - Setters

#pragma mark - Parsing

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.mediaItemRequestType = kMediaItemRequestType_Default; // should be updated right after if possible
    
    // TVLogDebug(@"[MediaItem][dictionary] TVMediaItem dictionary %@",dictionary);////////
    
    self.name = [dictionary objectOrNilForKey:MediaItemNameKey];
	self.mediaID = [dictionary objectOrNilForKey:MediaItemIDKey];
	self.mediaDescription = [dictionary objectOrNilForKey:MediaItemDescriptionKey];
	self.mediaTypeName = [dictionary objectOrNilForKey:MediaItemTypeNameKey];
	self.mediaTypeID = [dictionary objectOrNilForKey:MediaItemTypeIDKey];
	self.rating = [[dictionary objectOrNilForKey:MediaItemRatingKey] floatValue];
    self.viewCounter = [[dictionary objectOrNilForKey:MediaItemViewCounterKey] integerValue];
    self.totalItems = [[dictionary objectOrNilForKey:MediaItemTotalItemsKey] integerValue];
    
    self.likeCounter = [[dictionary objectOrNilForKey:TVMediaItemLikeCounterKey] integerValue];
	self.duration = [[dictionary objectOrNilForKey:MediaItemDurationKey] integerValue];
    NSString *URLString = [dictionary objectOrNilForKey:MediaItemURLKey];
    self.URL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
    self.fileID = [dictionary objectOrNilForKey:MediaItemFileIDKey];
    
    // Creation Date
    NSString *creationDateString = [dictionary objectOrNilForKey:MediaItemCreationDateKey];
	self.creationDate =  [NSDate FriendlyDateTimeByString:creationDateString];
    NSString *startDateString = [dictionary objectOrNilForKey:MediaItemStartDateKey];
    self.StartDate = [NSDate FriendlyDateTimeByString:startDateString];
    
    NSString *catalogStartDateString = [dictionary objectOrNilForKey:MediaItemCatalogStartDateKey];
    self.catalogStartDate = [NSDate userVisibleDateTimeStringForRFC3339DateTimeString:catalogStartDateString andFormat:MediaItemCatalogStartDateFormat];
    
    self.pictureURL = nil;
    NSString *pictureURLString = [dictionary objectOrNilForKey:MediaItemPictureURLKey];
    
    NSURL *url = [NSURL URLWithString:pictureURLString];
    if ([url host] != nil)
    {
        self.pictureURL = url;
    }
    
    self.mediaWebLink = nil;
	NSString *mediaWebLinkStr = [dictionary objectOrNilForKey:MediaItemWebLinkKey];
    // Finally the server is returning URL encoded strings
    mediaWebLinkStr = [mediaWebLinkStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    url = [NSURL URLWithString:mediaWebLinkStr];
    if ([url host] != nil)
    {
        self.mediaWebLink = url;
    }
    

    
    NSArray *metaData = [dictionary objectOrNilForKey:MediaItemMetaDataKey];
    self.metaData = [self parseMetas:metaData];
    
    NSArray *tags = [dictionary objectOrNilForKey:MediaItemTagsKey];
    self.tags = [self parseTags:tags];
    
    
    NSArray *externalID = [dictionary objectOrNilForKey:TVMediaItemExternalID];
    self.ExternalIDs = [self parseTags:externalID];
    
    
    NSDictionary *dynamicInfo = [dictionary objectOrNilForKey:MediaItemDynamicDataKey];
    [self setDynamicMediaInfoFromJSONDictionary:dynamicInfo];

    
    NSArray *filesAsDictionary = [dictionary objectOrNilForKey:TVMediaItemFilesKey];
    if (filesAsDictionary.count > 0)
    {
        self.files = [self parseFileAsDictionary:filesAsDictionary];
    }
    
    self.isLoadFavorite=NO;
    
    NSArray *adParameters = [dictionary objectOrNilForKey:TVMediaItemAdvertisingParametersKey];
    self.advertisingParameters = [self parseMetas:adParameters];
    // ASLog(@"self.advertisingParameters = %@",self.advertisingParameters);
    self.adRequestContentContainer = [[AdRequestContentContainer alloc]initWithMediaItem:self];
    
    
    NSArray *pictureSizesAsDicts = [dictionary objectOrNilForKey:MediaItemPicturesKey];
    NSMutableArray *picSizes = [NSMutableArray array];
    for(NSDictionary *dict in pictureSizesAsDicts){
        TVImage *ps = [[TVImage alloc] initWithDictionary:dict];
        [picSizes addObject:ps];
    }
    self.pictures = [NSArray arrayWithArray:picSizes];
}

-(NSArray *) parseFileAsDictionary : (NSArray *) filesAsDictionary
{
    NSMutableArray *temp = [NSMutableArray array];
    for (NSDictionary *dictionary in filesAsDictionary)
    {
        TVFile *file = [[TVFile alloc] initWithDictionary:dictionary];
        if (file != nil)
        {
            [temp addObject:file];
        }
    }
    return [NSArray arrayWithArray:temp];
}

-(void) setDynamicMediaInfoFromJSONDictionary : (NSDictionary *) dynamicMediaInfo
{
    if (dynamicMediaInfo) {
        self.isFavorite = [[dynamicMediaInfo objectOrNilForKey:MediaItemIsFavoriteKey] boolValue];
        self.price = [dynamicMediaInfo objectOrNilForKey:MediaItemPriceKey];
        self.mediaMark = [[dynamicMediaInfo objectOrNilForKey:MediaItemMarkKey] integerValue];
        self.priceType = (TVPriceType)[[dynamicMediaInfo objectOrNilForKey:MediaItemPriceTypeKey] integerValue];
        self.expirationDate = [NSDate FriendlyDateTimeByString:[dynamicMediaInfo objectOrNilForKey:MediaItemExpirationDateKey]];
    }
    else {
        self.priceType = TVPriceTypeUnknown;
    }
}

-(NSDictionary *) parseTags:(NSArray *)tags
{
    NSMutableDictionary *tagsParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in tags)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *values = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            NSArray *valuesSeparated = [values componentsSeparatedByString:@"|"];
            if (valuesSeparated != nil)
            {
                [tagsParsed setObject:valuesSeparated forKey:title];
            }
        }
    }
    return [NSDictionary dictionaryWithDictionary:tagsParsed];
}

-(NSDictionary *) parseMetas:(NSArray *)metas
{
    NSMutableDictionary *metaParsed = [NSMutableDictionary dictionary];
    for (NSDictionary *tagAsDictionary in metas)
    {
        NSString *title = [tagAsDictionary objectOrNilForKey:TVMediaItemTagTitleKey];
        if (title != nil)
        {
            NSString *value = [tagAsDictionary objectOrNilForKey:TVMediaItemTagValuesKey];
            if (value != nil)
            {
                [metaParsed setObject:value forKey:title];
            }
        }
    }
    // TVLogDebug(@"%@",metaParsed);
    return [NSDictionary dictionaryWithDictionary:metaParsed];
}

-(NSString *) description
{
    
    return [NSString stringWithFormat:@"MediaID=%@ Type=%@ Name=%@ MainFile=%@ TrailerFile=%@" ,self.mediaID,self.mediaTypeName,self.name, self.mainFile.fileURL.path, self.trailerFile.fileURL.path];
}

-(NSString *) sortKey{
    
    NSString* noDigitName = [self.name stringWithoutDigits];
    
    NSNumber * EpisodeNumber = [[self metaData] objectForKey:@"Episode number"];
    NSNumber * SeasonNumber =  [[self metaData] objectForKey:@"Season number"];
    NSString *code = [NSString stringWithFormat:@"%@_%09ld_%09ld",noDigitName,(long)[SeasonNumber integerValue], (long)[EpisodeNumber integerValue]];
    return code;
    
}

-(NSNumber *) getMediaItemEpisodeNumber{
    return [[self metaData] objectOrNilForKey:@"Episode number"];
}

-(NSNumber *)getMediaItemSeasonNumber{
    return [NSNumber numberWithInt:[[self.metaData objectForKey:TVMetaDataSeasonNumber] intValue]];
}

-(BOOL) isMovie
{
    NSString *movieID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeMovie];
    return ([self.mediaTypeID integerValue] == [movieID integerValue]);
}

-(BOOL) isKaraoke{
    NSString *karaokeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeKaraoke];
    return ([self.mediaTypeID integerValue] == [karaokeID integerValue]);
}

-(BOOL) isEpisode
{
    NSString *episodeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeEpisode];
    return ([self.mediaTypeID integerValue] == [episodeID integerValue]);
}

-(BOOL) isSeries
{
    NSString *seriesID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeSeries];
    return ([self.mediaTypeID integerValue] == [seriesID integerValue]);
}

-(BOOL) isLive
{
    NSString *linearID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLinear];
    NSString *liveID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    return ([self.mediaTypeID integerValue] == [linearID integerValue] || [self.mediaTypeID integerValue] == [liveID integerValue]);
}

-(BOOL) isPackage
{
    NSString *packageID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypePackage];
    return ([self.mediaTypeID integerValue] == [packageID integerValue]);
}

-(BOOL) isTest
{
    NSString *testID = @"356"; //[[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypePackage];
    return ([self.mediaTypeID integerValue] == [testID integerValue]);
}

-(BOOL) isEqual:(id)object
{
    if ([object isKindOfClass:[TVMediaItem class]])
    {
        TVMediaItem *anItem = object;
        return [self.mediaID isEqualToString:anItem.mediaID];
    }
    return NO;
}

-(BOOL) hasMainFile{
    return (self.mainFile.fileURL.path != nil);
}

-(BOOL) hasTrailerFile{
    return (self.trailerFile.fileURL.path != nil);
}


-(NSString *) epgChannelID
{
    return [[self.ExternalIDs objectForKey:EPGEpg_id] lastObject];
}


#pragma mark - file manipulation
-(NSString *) getBaseURLOfFilePath:(NSString *) path {
    
    NSArray * components = [path componentsSeparatedByString:@"/"];
    NSString * baseURL = [path stringByReplacingOccurrencesOfString:[components lastObject] withString:@""];
    
    NSRange range = [baseURL rangeOfString:@"http://"];
    
    
    if(range.location != NSNotFound)
    {
        baseURL = [baseURL stringByReplacingOccurrencesOfString:@"http:/" withString:@"http://"];
    }
    
    return baseURL;
}


-(TVFile *) fileForFormat:(NSString *) fileFormat
{
    for (TVFile * file in self.files)
    {
        if ([file.format isEqualToString:fileFormat])
        {
            return file;
        }
    }
    return nil;
}

-(void)printMediaItem
{
    ASLog(@"\n-------------------------------------------------\n-----------------printMediaItem------------------\n-------------------------------------------------\n mediaID = %@ \n name = %@ \n mediaDescription = %@ \n tags = %@ \n metaData = %@ \n mediaTypeName = %@ \n mediaTypeID = %@ \n rating = %f \n viewCounter = %d \n likeCounter = %d \n totalItems = %d \n creationDate = %@ \n pictureURL = %@ \n mediaWebLink = %@ \n isFavorite = %c \n price = %@ \n mediaMark = %f \n priceType = %u \n expirationDate = %@ \n duration = %d \n URL = %@ \n fileID = %@ \n ExternalIDs = %@ \n files = %@ \n subtitles = %@ \n trailerFile = %@ \n subtitleFile = %@ \n mainFile = %@ \n widevineFile = %@ \n -------------------------------------------------\n\n",self.mediaID,self.name,self.mediaDescription,self.tags,self.metaData,self.mediaTypeName,self.mediaTypeID,self.rating,self.viewCounter,self.likeCounter,self.totalItems,self.creationDate,self.pictureURL,self.mediaWebLink,self.isFavorite,self.price,self.mediaMark,self.priceType,self.expirationDate,self.duration,self.URL,self.fileID,self.ExternalIDs,self.files,self.subtitles,self.trailerFile,self.subtitleFile,self.mainFile,self.widevineFile);
}

@end




NSString *const TVTagGenre = @"Genre";
NSString *const TVTagBusinessModel = @"Business model";
NSString *const TVTagSeriesName = @"Series Name";
//replace from "Cast" to "Main cast"
NSString *const TVTagMainCast = @"Main cast";
NSString *const TVTagAgeRating = @"Rating";
NSString *const TVTagDirector = @"Director";
NSString *const TVTagLanguage = @"Language";
NSString *const TVTagSubtitleLanguage = @"Subtitle language";
NSString *const TVTagAudioLanguage = @"Audio language";
NSString *const TVMetaDataRatingAdvisories = @"Rating advisories";
NSString *const TVMetaDataSeasonNumber = @"Season number";
NSString *const TVMetaDataEpisodeNumber = @"Episode number";
NSString *const TVMetaDataClosedCaptionAvailable = @"Closed captions available";
NSString *const TVMetaDataShortSummary = @"Short summary";
NSString *const TVMetaDataShortTitle = @"Short title";
NSString *const TVMetaDataNumberOfSeasons = @"Number of seasons";
NSString *const TVMetaDataReleaseYear = @"Release year";
NSString *const TVMetaDataReleaseDate = @"Release date";


NSString *const TVTagTerritory = @"Territory";
NSString *const TVTagCategory = @"Category";
NSString *const TVTagProvider = @"Provider";
NSString *const TVTagProviderID = @"Provider ID";
