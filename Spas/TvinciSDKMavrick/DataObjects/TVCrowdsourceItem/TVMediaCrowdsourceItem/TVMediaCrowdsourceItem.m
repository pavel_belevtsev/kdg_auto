//
//  TVMediaCrowdsourceItem.m
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVMediaCrowdsourceItem.h"

NSString *const TVCrowdsourceActionKey                  = @"Action";
NSString *const TVCrowdsourceActionDescriptionKey       = @"ActionDescription";
NSString *const TVCrowdsourceActionValKey               = @"ActionVal";
NSString *const TVCrowdsourcePeriodKey                  = @"Period";
NSString *const TVCrowdsourcePeriodDescriptionKey       = @"PeriodDescription";


@implementation TVMediaCrowdsourceItem

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    
    NSNumber *number = [dictionary objectOrNilForKey:TVCrowdsourceActionKey];
    self.action = [number intValue];
    
    self.actionDescription = [dictionary objectOrNilForKey:TVCrowdsourceActionDescriptionKey];
    
    number = [dictionary objectOrNilForKey:TVCrowdsourceActionValKey];
    self.actionVal = [number intValue];
    
    number = [dictionary objectOrNilForKey:TVCrowdsourcePeriodKey];
    self.period = [number intValue];
    
    self.periodDescription = [dictionary objectOrNilForKey:TVCrowdsourcePeriodDescriptionKey];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<[TVCrowdsourceItem]\naction: %ld\nactionVal: %ld\nperiod: %ld\n>", (long)self.action, (long)self.actionVal, (long)self.period];
}

@end
