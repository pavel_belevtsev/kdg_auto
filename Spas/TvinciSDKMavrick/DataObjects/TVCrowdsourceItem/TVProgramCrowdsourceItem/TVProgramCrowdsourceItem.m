//
//  TVProgramCrowdsourceItem.m
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVProgramCrowdsourceItem.h"

NSString *const TVCrowdsourceEpgStartTimeKey    = @"EpgStartTime";
NSString *const TVCrowdsourceProgramIdKey       = @"ProgramId";
NSString *const TVCrowdsourceProgramImageKey    = @"ProgramImage";
NSString *const TVCrowdsourceProgramNameKey     = @"ProgramName";
NSString *const TVCrowdsourceViewsKey           = @"Views";

@interface TVProgramCrowdsourceItem ()

@property (nonatomic, copy , readwrite) NSString *programID;

@end

@implementation TVProgramCrowdsourceItem

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    
    self.epgStartTime = [NSDate dateWithTimeIntervalSince1970:[[dictionary objectOrNilForKey:TVCrowdsourceEpgStartTimeKey] intValue]];
    self.programID = [[dictionary objectOrNilForKey:TVCrowdsourceProgramIdKey] stringValue];
    self.programImage = [dictionary objectOrNilForKey:TVCrowdsourceProgramImageKey];
    
    self.programName = [dictionary objectOrNilForKey:TVCrowdsourceProgramNameKey];
    
    NSNumber *number = [dictionary objectOrNilForKey:TVCrowdsourceViewsKey];
    self.views = [number integerValue];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<[TVCrowdsourceItem]\nprogramID: %@\nprogramName: %@ \nepgStartTime: %@>", self.programID,self.programName,self.epgStartTime];
}

@end
