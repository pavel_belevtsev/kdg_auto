//
//  TVCrowdsourceFeed.m
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/16/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVCrowdsourceItem.h"
#import "TVImage.h"
#import "TVMediaCrowdsourceItem.h"
#import "TVProgramCrowdsourceItem.h"


NSString *const TVCrowdsourceMediaIdKey     = @"MediaId";
NSString *const TVCrowdsourceMediaImageKey  = @"MediaImage";
NSString *const TVCrowdsourceMediaNameKey   = @"MediaName";
NSString *const TVCrowdsourceOrderKey       = @"Order";
NSString *const TVCrowdsourceTimeStampKey   = @"TimeStamp";
NSString *const TVCrowdsourceTypeKey        = @"Type";
NSString *const TVCrowdsourceTypeDescriptionKey        = @"TypeDescription";

@interface TVCrowdsourceItem ()

@property (nonatomic, copy , readwrite) NSNumber *mediaID;

@end

@implementation TVCrowdsourceItem

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    
    self.mediaID = [dictionary objectOrNilForKey:TVCrowdsourceMediaIdKey];
    
    NSArray *mediaImageSizesAsDicts = [dictionary objectOrNilForKey:TVCrowdsourceMediaImageKey];
    NSMutableArray *mediaSizes = [@[] mutableCopy];
    for(NSDictionary *dict in mediaImageSizesAsDicts){
        TVImage *ps = [[TVImage alloc] initWithDictionary:dict];
        [mediaSizes addObject:ps];
    }
    self.mediaImages = [NSArray arrayWithArray:mediaSizes];
    
    self.mediaName = [dictionary objectOrNilForKey:TVCrowdsourceMediaNameKey];
    
    NSNumber *number = [dictionary objectOrNilForKey:TVCrowdsourceOrderKey];
    self.order = [number integerValue];
    
    self.timestamp = [[dictionary objectOrNilForKey:TVCrowdsourceTimeStampKey] intValue];
    
    number = [dictionary objectOrNilForKey:TVCrowdsourceTypeKey];
    self.type = [number intValue];
    
    self.typeDescription = [dictionary objectOrNilForKey:TVCrowdsourceTypeDescriptionKey];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<[TVCrowdsourceItem]\nmediaID: %@\nmediaName: %@\norder: %ld\ntimestamp: %ld\ntype: %d>", self.mediaID,self.mediaName, (long)self.order, (long)self.timestamp, self.type];
}


+ (TVCrowdsourceItem *)crowdsourceItemWithDictionary:(NSDictionary *)dictionary
{
    TVCrowdsourceItem * item = nil;
    kCrowdsourceType  type  = [[dictionary objectOrNilForKey:TVCrowdsourceTypeKey] intValue];
    
    switch (type)
    {
        case kCrowdsourceType_LiveViews:
        {
            item = [[TVProgramCrowdsourceItem alloc] initWithDictionary:dictionary];
        }
        break;

        case kCrowdsourceType_Orca:
        {
            item = [[TVMediaCrowdsourceItem alloc] initWithDictionary:dictionary];
        }
    
        break;
            
        case kCrowdsourceType_SlidingWindow:
            return nil;
        break;

        default:
            break;
    }
    return item;
}

@end
