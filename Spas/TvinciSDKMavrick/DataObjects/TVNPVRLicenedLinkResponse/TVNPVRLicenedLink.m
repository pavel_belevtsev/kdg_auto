//
//  TVNPVRLicenedLinkResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 2/23/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVNPVRLicenedLink.h"

@implementation TVNPVRLicenedLink

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    
    NSString * licencedURLString = [dictionary objectForKey:@"mainUrl"];
    NSURL * url = [NSURL URLWithString:licencedURLString];
    self.link = url;
    self.status  = TVNPVRResponseStatusForString([dictionary objectForKey:@"status"]);
    self.message = [dictionary objectForKey:@"msg"];

}

@end
