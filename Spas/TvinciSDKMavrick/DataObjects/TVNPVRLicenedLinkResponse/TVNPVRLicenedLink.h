//
//  TVNPVRLicenedLinkResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 2/23/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVNPVRLicenedLink : BaseModelObject

@property (strong, nonatomic) NSURL * link;
@property (assign, nonatomic) TVNPVRResponseStatus status;
@property (strong, nonatomic) NSString * message;
@end
