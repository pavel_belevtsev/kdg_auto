//
//  TVConfigurationObject.h
//  tvinci iPad
//
//  Created by Alexander Israel on 1/3/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVInitObj.h"

@interface TVConfigurationObject : NSObject
{
    
}
@property (nonatomic, copy) NSDictionary* token;
@property (nonatomic, copy) NSString *registrationStatus;
@property (nonatomic, copy) NSString *udid;
@property (nonatomic, copy) NSDictionary *appVersion;
@property (nonatomic, retain) TVInitObj *initObj;
@property (nonatomic, copy) NSDictionary *initObjDic;
@property (nonatomic, copy) NSString *facebookURL;
@property (nonatomic, copy) NSString *survayURL;
@property (nonatomic, copy) NSString *siteGateway;
@property (nonatomic, copy) NSString *mediaGateway;
@property (nonatomic, copy) NSString *domainGateway;
@property (nonatomic, copy) NSString *pricingGateway;
@property (nonatomic, copy) NSString *getMenuGateway;
@property (copy, nonatomic) NSString *topMenuID;
@property (retain, nonatomic) NSMutableArray *topMenuItems;

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary;
@end
