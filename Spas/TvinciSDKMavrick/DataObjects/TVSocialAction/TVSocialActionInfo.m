//
//  TVSocialActionInfo.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/27/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVSocialActionInfo.h"

const NSString * TVSocialActionResponseStatusExtern = @"m_eActionResponseStatusExtern";
const NSString * TVSocialActionResponseStatusIntern = @"m_eActionResponseStatusIntern";
const NSString * TVSocialStatus                     = @"m_nStatus";
const NSString * TVSocialError                      = @"m_sError";
const NSString * TVSocialResponseID                 = @"m_sResponseID";



@implementation TVSocialActionInfo


-(void)dealloc
{
    _responseID = nil;
    _error = nil;
    

}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    _responseID = [dictionary objectOrNilForKey:TVSocialResponseID];
    _error      = [dictionary objectOrNilForKey:TVSocialError];
    _externalStatus  = [[dictionary objectOrNilForKey:TVSocialActionResponseStatusExtern] integerValue];
    _internalStatus  = [[dictionary objectOrNilForKey:TVSocialActionResponseStatusIntern] integerValue];
    _status  = [[dictionary objectOrNilForKey:TVSocialStatus] integerValue];
}


@end
