//
//  TVEPGChannel.m
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVEPGChannel.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "NSCoder+NSNullAvoidance.h"
#import "SmartLogs.h"
// 8/22/2012 2:47:11 PM
#define EPG_CHANNEL_DATE_FORMAT @"M/d/yyyy h:mm:ss a"

NSString *const EPGChannelEPGChannelIDKey = @"EPG_CHANNEL_ID";
NSString *const EPGChannelNameKey = @"NAME";
NSString *const EPGChannelDescriptionKey = @"DESCRIPTION";
NSString *const EPGChannelOrderNumKey = @"ORDER_NUM";
NSString *const EPGChannelIsActiveKey = @"IS_ACTIVE";
NSString *const EPGChannelPicURLKey = @"PIC_URL";
NSString *const EPGChannelGroupIDKey = @"GROUP_ID";
NSString *const EPGChannelEditorRemarksKey = @"EDITOR_REMARKS";
NSString *const EPGChannelStatusKey = @"STATUS";
NSString *const EPGChannelUpdaterIDKey = @"UPDATER_ID";
NSString *const EPGChannelCreateDateKey = @"CREATE_DATE";
NSString *const EPGChannelPublishDateKey = @"PUBLISH_DATE";
NSString *const EPGChannelChannelIDKey = @"CHANNEL_ID";
NSString *const EPGChannelMediaIDKey = @"MEDIA_ID";




@implementation TVEPGChannel

@synthesize EPGChannelID = _EPGChannelID;
@synthesize name = _name;
@synthesize channelDescription = _channelDescription;
@synthesize orderNumber = _orderNumber;
@synthesize isActive = _isActive;
@synthesize pictureURL = _pictureURL;
@synthesize groupID = _groupID;
@synthesize editorRemarks = _editorRemarks;
@synthesize status = _status;
@synthesize updaterID = _updaterID;
@synthesize createDate = _createDate;
@synthesize publishDate = _publishDate;
@synthesize channelID = _channelID;
@synthesize mediaID = _mediaID;

- (void)dealloc
{

}

#pragma mark nscoding protocol

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObjectOrNil:self.EPGChannelID forKey:EPGChannelEPGChannelIDKey];
    [encoder encodeObjectOrNil:self.name forKey:EPGChannelNameKey];
    [encoder encodeObjectOrNil:self.channelDescription forKey:EPGChannelDescriptionKey];
    [encoder encodeObjectOrNil:self.orderNumber forKey:EPGChannelOrderNumKey];
    [encoder encodeObjectOrNil:self.isActive  forKey:EPGChannelIsActiveKey];
    
    [encoder encodeObjectOrNil:[self.pictureURL absoluteString]  forKey:EPGChannelPicURLKey];
    
    [encoder encodeObjectOrNil:self.groupID  forKey:EPGChannelGroupIDKey];
    [encoder encodeObjectOrNil:self.editorRemarks  forKey:EPGChannelEditorRemarksKey];
    [encoder encodeObjectOrNil:self.status  forKey:EPGChannelStatusKey];
    [encoder encodeObjectOrNil:self.updaterID  forKey:EPGChannelUpdaterIDKey];
    
    [encoder encodeObjectOrNil:self.channelID  forKey:EPGChannelChannelIDKey];
    [encoder encodeObjectOrNil:self.mediaID  forKey:EPGChannelMediaIDKey];
    
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.createDate.timeIntervalSince1970] forKey:EPGChannelCreateDateKey];
    [encoder encodeObjectOrNil:[NSNumber numberWithInteger:self.publishDate.timeIntervalSince1970] forKey:EPGChannelPublishDateKey];
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        self.EPGChannelID = [aDecoder decodeObjectForKey:EPGChannelEPGChannelIDKey];
        self.name = [aDecoder decodeObjectForKey:EPGChannelNameKey];
        self.channelDescription = [aDecoder decodeObjectForKey:EPGChannelDescriptionKey];
        self.orderNumber = [aDecoder decodeObjectForKey:EPGChannelOrderNumKey];
        self.isActive = [aDecoder decodeObjectForKey:EPGChannelIsActiveKey];
        
        NSString *URLString = [aDecoder decodeObjectForKey:EPGChannelPicURLKey];
        self.pictureURL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
        self.groupID = [aDecoder decodeObjectForKey:EPGChannelGroupIDKey];
        self.editorRemarks = [aDecoder decodeObjectForKey:EPGChannelEditorRemarksKey];
        self.status = [aDecoder decodeObjectForKey:EPGChannelStatusKey];
        self.updaterID = [aDecoder decodeObjectForKey:EPGChannelUpdaterIDKey];
        
        self.channelID = [aDecoder decodeObjectForKey:EPGChannelChannelIDKey];
        self.mediaID = [aDecoder decodeObjectForKey:EPGChannelMediaIDKey];
        
        
        // Date
        NSNumber *createDateNumber =  [aDecoder decodeObjectForKey:EPGChannelCreateDateKey];
        self.createDate = [NSDate dateWithTimeIntervalSince1970:createDateNumber.integerValue];
        
        
        NSNumber *publishDateNumber =  [aDecoder decodeObjectForKey:EPGChannelPublishDateKey];
        self.publishDate = [NSDate dateWithTimeIntervalSince1970:publishDateNumber.integerValue];
        
    }
    return self;
}

#pragma mark - Initialization
-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    TVLogDebug(@"[EPGChannel] Dict: %@",dictionary);////////
    
    if (dictionary != nil)
    {
//#warning the change from : self.EPGChannelID = [dictionary objectOrNilForKey:EPGChannelChannelIDKey]; to  self.EPGChannelID = [dictionary objectOrNilForKey:EPGChannelEPGChannelIDKey];
        
        self.EPGChannelID = [dictionary objectOrNilForKey:EPGChannelEPGChannelIDKey];
        self.name = [dictionary objectOrNilForKey:EPGChannelNameKey];
        self.channelDescription = [dictionary objectOrNilForKey:EPGChannelDescriptionKey];
        self.orderNumber = [dictionary objectOrNilForKey:EPGChannelOrderNumKey];
        self.isActive = [dictionary objectOrNilForKey:EPGChannelIsActiveKey];
        
        NSString *URLString = [dictionary objectOrNilForKey:EPGChannelPicURLKey];
        self.pictureURL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
        self.groupID = [dictionary objectOrNilForKey:EPGChannelGroupIDKey];
        self.editorRemarks = [dictionary objectOrNilForKey:EPGChannelEditorRemarksKey];
        self.status = [dictionary objectOrNilForKey:EPGChannelStatusKey];
        self.updaterID = [dictionary objectOrNilForKey:EPGChannelUpdaterIDKey];
        
        // Date
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = EPG_CHANNEL_DATE_FORMAT;
        
        NSString *createDateString = [dictionary objectOrNilForKey:EPGChannelCreateDateKey];
        self.createDate = [formatter dateFromString:createDateString];
        
        NSString *publishDateString = [dictionary objectOrNilForKey:EPGChannelPublishDateKey];
        self.publishDate = [formatter dateFromString:publishDateString];
        

        self.channelID = [dictionary objectOrNilForKey:EPGChannelChannelIDKey];
        self.mediaID = [dictionary objectOrNilForKey:EPGChannelMediaIDKey];
    }
}

-(void) setAttributesFromCouchbaseDictionary:(NSDictionary *)dictionary
{
    if (dictionary != nil)
    {
        self.EPGChannelID = [dictionary objectOrNilForKey:[EPGChannelEPGChannelIDKey lowercaseString]];
        self.name = [dictionary objectOrNilForKey:[EPGChannelNameKey lowercaseString]];
        self.channelDescription = [dictionary objectOrNilForKey:[EPGChannelDescriptionKey lowercaseString]];
        self.orderNumber = [dictionary objectOrNilForKey:[EPGChannelOrderNumKey lowercaseString]];
        self.isActive = [dictionary objectOrNilForKey:[EPGChannelIsActiveKey lowercaseString]];
        
        NSString *URLString = [dictionary objectOrNilForKey:[EPGChannelPicURLKey lowercaseString]];
        self.pictureURL = (URLString.length > 0) ? [NSURL URLWithString:URLString] : nil;
        self.groupID = [dictionary objectOrNilForKey:[EPGChannelGroupIDKey lowercaseString]];
        self.editorRemarks = [dictionary objectOrNilForKey:[EPGChannelEditorRemarksKey lowercaseString]];
        self.status = [dictionary objectOrNilForKey:[EPGChannelStatusKey lowercaseString]];
        self.updaterID = [dictionary objectOrNilForKey:[EPGChannelUpdaterIDKey lowercaseString]];
        
        // Date
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = EPG_CHANNEL_DATE_FORMAT;
        
        NSString *createDateString = [dictionary objectOrNilForKey:[EPGChannelCreateDateKey lowercaseString]];
        self.createDate = [formatter dateFromString:createDateString];
        
        NSString *publishDateString = [dictionary objectOrNilForKey:[EPGChannelPublishDateKey lowercaseString]];
        self.publishDate = [formatter dateFromString:publishDateString];
        
        
        self.channelID = [dictionary objectOrNilForKey:[EPGChannelChannelIDKey lowercaseString]];
        self.mediaID = [dictionary objectOrNilForKey:[EPGChannelMediaIDKey lowercaseString]];
    }
}


@end
