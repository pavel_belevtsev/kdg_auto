//
//  TVLicencedDataResponse.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVLicencedDataResponse.h"
#import "TVLicencedLink.h"

@implementation TVLicencedDataResponse

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.licencedLink = [[TVLicencedLink alloc] initWithDictionary:[dictionary objectOrNilForKey:@"licensed_link"]];
    
    self.responseStatus = [[TVResponseStatus alloc] initWithDictionary:[dictionary objectOrNilForKey:@"status"]];
    
}

@end
