//
//  TVGallery.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
typedef enum 
{
	UIGalleryTypeUnknown = -1,
	UIGalleryTypeCarousel,
	UIGalleryTypeXPack,
	UIGalleryTypeFree,
	UIGalleryTypeTop,
	UIGalleryTypePlayer,
	UIGalleryTypeSpotlight,
	UIGalleryTypeMovingGallery,
	UIGalleryTypeRandomGallery,
	UIGalleryTypeCards,
	UIGalleryTypeMovieFinder,
	UIGalleryTypeRelPackages,
	UIGalleryTypeMyZoneMiniFeed,
	UIGalleryTypePeopleWhoWatched,
	UIGalleryTypeBannerBig,
	UIGalleryTypeBanner,
	UIGalleryTypeLinearChannel,
	UIGalleryTypeMulti,
	UIGalleryTypeMostViewed,
	UIGalleryTypeEditorialComments,
	UIGalleryTypeFreeFlash,
	UIGalleryTypeAdsGallery
} UIGalleryType;

@interface TVGallery : BaseModelObject
@property (nonatomic, assign) long galleryID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic , strong) NSArray *channels;
@property (nonatomic , strong) NSArray *galleryItems;
@property (nonatomic, assign) UIGalleryType galleryType;
@property (nonatomic ,assign , readonly) NSString *galleryTypeString;
@property (nonatomic, copy) NSString *galleryLocation;
@property (nonatomic, assign) NSInteger galleryOrder;
@property (nonatomic, copy) NSString *mainCulture;
@property (nonatomic, copy) NSString *linksHeader;
@property (nonatomic, copy) NSString *mainDescription;
@property (nonatomic, assign) long familyId;
@property (nonatomic) BOOL initialized;

-(NSArray *) parseGalleryItems : (NSArray *) itemsTpParse;
@end
