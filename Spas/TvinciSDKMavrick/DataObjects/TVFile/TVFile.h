//
//  TVFile.h
//  TVinci
//
//  Created by Avraham Shukron on 6/4/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "AdProvider.h"

typedef enum
{
    TVEncriptionTypeNone = 0,
    TVEncriptionTypeWidevine
}TVEncryptionType;

@interface TVFile : BaseModelObject <NSCopying>

@property (nonatomic , copy , readwrite) NSString *fileID;
@property (nonatomic , copy , readwrite) NSString *format;
@property (nonatomic , assign , readwrite) NSTimeInterval duration;
@property (nonatomic , strong) NSURL *fileURL;
@property (nonatomic, assign, readwrite) TVEncryptionType encriptionType;


//  Added for Tvinci SDK 2.5 Catwoman-Daredevil
@property (strong,readonly) NSString *language;
@property (strong,readonly) NSString *CoGuid;
@property (assign,readonly) BOOL IsDefaultLang;


@property(nonatomic,strong) AdProvider * breakProvider; // midRoll
@property(nonatomic,strong) AdProvider * postProvider;  // postRoll
@property(nonatomic,strong) AdProvider * preProvider;   // preRoll
@property(nonatomic, strong) NSArray   * breakPoints;   // breakPoints to mid roll

-(BOOL)isFileHeveAnyAds;



@end
