//
//  TVNPVRQuota.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVNPVRQuota.h"

@implementation TVNPVRQuota

//{
//          "totalQuota": 86400,
//          "occupiedQuota": 13920,
//          "status": "OK",
//          "msg": null
//}

-(void)setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.totalQuota = [[dictionary objectForKey:@"totalQuota"] integerValue];
    self.occupiedQuota = [[dictionary objectForKey:@"occupiedQuota"] integerValue];
    
    if ([dictionary objectForKey:@"status"])
    {
        self.responseStatus = TVNPVRResponseStatusForString([dictionary objectForKey:@"status"]);
    }
    self.message = [dictionary objectForKey:@"msg"];
}

@end
