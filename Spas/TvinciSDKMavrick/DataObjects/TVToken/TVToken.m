//
//  TVAccessTokenData.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/26/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVToken.h"

@implementation TVToken

-(void)setAttributesFromString:(NSString *) string
{
    NSArray * tokenValues = [string componentsSeparatedByString:@"|"];
    self.token = [tokenValues firstObject];
    NSTimeInterval expiration = [[tokenValues lastObject] integerValue];
    self.expiration = [NSDate dateWithTimeIntervalSince1970:expiration];
}

+ (instancetype) tokenWithToken:(NSString *) tokenString expiration:(NSTimeInterval) timeIntervalSince1970
{
    TVToken * token =  [[TVToken alloc] init];
    token.token = tokenString;
    token.expiration = [NSDate dateWithTimeIntervalSince1970:timeIntervalSince1970];
    return token;
}

-(BOOL)isEqual:(TVToken *)object
{
    return [self.token isEqualToString:object.token] && self.expiration.timeIntervalSinceReferenceDate == object.expiration.timeIntervalSinceReferenceDate;
}

@end
