//
//  TVDeviceFamily.m
//  TVinci
//
//  Created by Avraham Shukron on 5/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVDeviceFamily.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVDevice.h"

NSString *const TVDeviceFamilyIDKey = @"m_deviceFamilyID";
NSString *const TVDeviceFamilyNameKey = @"m_deviceFamilyName";
NSString *const TVDeviceFamilyMaxNumberOfDevicesKey = @"m_deviceLimit";
NSString *const TVDeviceFamilyDevicesKey = @"DeviceInstances";

NSString *const TVDeviceFamilyConcurrentLimitKey = @"m_deviceConcurrentLimit";

@implementation TVDeviceFamily
@synthesize familyID = _familyID;
@synthesize familyName = _familyName;
@synthesize devices = _devices;
@synthesize maximumNumberOfDevices = _maximumNumberOfDevices;
@synthesize concurrentLimit = _concurrentLimit;

#pragma mark - Memory
-(void) dealloc
{
    self.familyName = nil;
    self.devices = nil;
}

#pragma mark - JSON

-(NSArray *) parseDevices : (NSArray *) devicesAsDictionary
{
    NSMutableArray *temp = [NSMutableArray array];
    
    for (NSDictionary *deviceDictionary in devicesAsDictionary)
    {
        TVDevice *device = [[TVDevice alloc] initWithDictionary:deviceDictionary];
        if (device != nil)
        {
            [temp addObject:device];
        }
    }
    return [NSArray arrayWithArray:temp];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    [super setAttributesFromDictionary:dictionary];
    self.familyName = [dictionary objectOrNilForKey:TVDeviceFamilyNameKey];
    self.familyID = [[dictionary objectOrNilForKey:TVDeviceFamilyIDKey] integerValue];
    self.maximumNumberOfDevices = [[dictionary objectOrNilForKey:TVDeviceFamilyMaxNumberOfDevicesKey] integerValue];
    self.devices = [self parseDevices : [dictionary objectOrNilForKey:TVDeviceFamilyDevicesKey]];
    self.concurrentLimit = [[dictionary objectOrNilForKey:TVDeviceFamilyConcurrentLimitKey] integerValue];
}

-(NSDictionary *) keyValueRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    [dictionary setObjectOrNil:[NSNumber numberWithInteger:self.familyID] forKey:TVDeviceFamilyIDKey];
    [dictionary setObjectOrNil:self.familyName forKey:TVDeviceFamilyNameKey];
    [dictionary setObjectOrNil:[NSNumber numberWithInteger:self.maximumNumberOfDevices] forKey:TVDeviceFamilyMaxNumberOfDevicesKey];
    
    NSMutableArray *devices = [NSMutableArray array];
    for (TVDevice *device in self.devices)
    {
        NSDictionary *kvr = [device keyValueRepresentation];
        if (kvr != nil)
        {
            [devices addObject:kvr];
        }
    }
    [dictionary setObjectOrNil:devices forKey:TVDeviceFamilyDevicesKey];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end
