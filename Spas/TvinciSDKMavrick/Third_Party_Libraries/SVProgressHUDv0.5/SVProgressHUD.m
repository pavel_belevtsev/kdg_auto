//
//  SVProgressHUD.m
//
//  Created by Sam Vermette on 27.03.11.
//  Copyright 2011 Sam Vermette. All rights reserved.
//
//  https://github.com/samvermette/SVProgressHUD
//

#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+Animations.h"

#ifdef SVPROGRESSHUD_DISABLE_NETWORK_INDICATOR
#define SVProgressHUDShowNetworkIndicator 0
#else
#define SVProgressHUDShowNetworkIndicator 1
#endif


@interface SVProgressHUD ()

@property (nonatomic, readwrite) SVProgressHUDMaskType maskType;
@property (nonatomic, readwrite) BOOL showNetworkIndicator;
@property (nonatomic, retain) NSTimer *fadeOutTimer;
@property (nonatomic, readonly) UIView *hudView;
@property (nonatomic, readonly) UILabel *stringLabel;
@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, readonly) UIActivityIndicatorView *spinnerView;
@property (nonatomic, retain) UIWindow *previousKeyWindow;
@property (nonatomic, readonly) CGFloat visibleKeyboardHeight;
@property (nonatomic, assign) NSUInteger showCount;

- (void)registerNotifications;
- (void)moveToPoint:(CGPoint)newCenter rotateAngle:(CGFloat)angle;
- (void)positionHUD:(NSNotification*)notification;
@end


static NSDictionary *progressHudLoadingImageData=nil;

@implementation SVProgressHUD

@synthesize showCount = _showCount;
@synthesize hudView = _hudView;
@synthesize maskType = _maskType;
@synthesize showNetworkIndicator = _showNetworkIndicator;
@synthesize stringLabel = _stringLabel;
@synthesize imageView = _imageView;
@synthesize spinnerView = _spinnerView;
@synthesize previousKeyWindow = _previousKeyWindow;
@synthesize visibleKeyboardHeight = _visibleKeyboardHeight;
@synthesize fadeOutTimer = _fadeOutTimer;

@synthesize otherIndicator = _otherIndicator;
#pragma mark - Memory
- (void)dealloc 
{
    [_otherIndicator release];
	[self invalidateTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_hudView release];
    [_stringLabel release];
    [_imageView release];
    [_spinnerView release];
    [super dealloc];
}

+ (SVProgressHUD*)sharedView 
{
	static SVProgressHUD *sharedView = nil;
	if(sharedView == nil)
    {
		sharedView = [[SVProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	}
	return sharedView;
}

+ (void)setStatus:(NSString *)string 
{
	[[SVProgressHUD sharedView] setStatus:string];
}

#pragma mark - Singleton Show Methods - All mirrored

+ (void)show 
{
	[[SVProgressHUD sharedView] show];
}

+ (void)showWithStatus:(NSString *)status 
{
    [[SVProgressHUD sharedView] showWithStatus:status];
}

+ (void)showWithMaskType:(SVProgressHUDMaskType)maskType 
{
    [[SVProgressHUD sharedView] showWithMaskType:maskType];
}

+ (void)showWithStatus:(NSString*)status maskType:(SVProgressHUDMaskType)maskType 
{
    [[SVProgressHUD sharedView] showWithStatus:status maskType:maskType];
}

+ (void)showWithStatus:(NSString *)status networkIndicator:(BOOL)show 
{
    [[SVProgressHUD sharedView] showWithStatus:status networkIndicator:show];
}

+ (void)showWithMaskType:(SVProgressHUDMaskType)maskType networkIndicator:(BOOL)show 
{
    [[SVProgressHUD sharedView] showWithMaskType:maskType networkIndicator:show];
}

+ (void)showWithStatus:(NSString*)status maskType:(SVProgressHUDMaskType)maskType networkIndicator:(BOOL)show 
{
    [[SVProgressHUD sharedView] showWithStatus:status maskType:maskType networkIndicator:show];
}

+ (void)showSuccessWithStatus:(NSString *)string 
{
    [[SVProgressHUD sharedView] showSuccessWithStatus:string];
}

+ (void)showFailureWithStatus:(NSString *)string
{
    [[SVProgressHUD sharedView] showFailureWithStatus:string];
}

+(void)toggleShowHideHudView:(BOOL)value
{
    [[SVProgressHUD sharedView] hudView].hidden=value;
}

+(BOOL)isHudViewHidden
{
    return [[SVProgressHUD sharedView] hudView].hidden;
}
#pragma mark - Show Methods

-(void) show
{
    [self showWithStatus:nil networkIndicator:SVProgressHUDShowNetworkIndicator];
}

-(void) showWithStatus:(NSString *)status
{
    [self showWithStatus:status networkIndicator:SVProgressHUDShowNetworkIndicator];
}

-(void) showWithMaskType:(SVProgressHUDMaskType)maskType
{
    [self showWithStatus:nil maskType:maskType networkIndicator:SVProgressHUDShowNetworkIndicator];
}

-(void) showWithStatus:(NSString *)status maskType:(SVProgressHUDMaskType)maskType
{
    [self showWithStatus:status maskType:maskType networkIndicator:SVProgressHUDShowNetworkIndicator];
}

-(void) showWithStatus:(NSString *)status networkIndicator:(BOOL)show
{
    [self showWithStatus:status maskType:SVProgressHUDMaskTypeNone networkIndicator:show];
}

- (void)showWithMaskType:(SVProgressHUDMaskType)maskType networkIndicator:(BOOL)show 
{
    [self showWithStatus:nil maskType:maskType networkIndicator:show];
}

- (void)showWithStatus:(NSString*)string maskType:(SVProgressHUDMaskType)hudMaskType networkIndicator:(BOOL)show 
{
    self.showCount += 1;
	
    [self invalidateTimer];
    
    self.showNetworkIndicator = show;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = self.showNetworkIndicator;
	
    
	self.imageView.hidden = YES;
    self.maskType = hudMaskType;
	
	[self setStatus:string];
	[self.spinnerView startAnimating];
    
    if(self.maskType != SVProgressHUDMaskTypeNone)
        self.userInteractionEnabled = YES;
    else
        self.userInteractionEnabled = NO;
    
    if([self isKeyWindow] == NO) {
        
        [[UIApplication sharedApplication].windows enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            UIWindow *window = (UIWindow*)obj;
            if(window.windowLevel == UIWindowLevelNormal && ![[window class] isEqual:[SVProgressHUD class]]) {
                self.previousKeyWindow = window;
                *stop = YES;
            }
        }];
        
        [self makeKeyAndVisible];
    }
    
    [self positionHUD:nil];
    
	if(self.alpha != 1) 
    {
        float sizeFactor = 1.3;
        
        [self registerNotifications];
		self.hudView.transform = CGAffineTransformScale(self.hudView.transform, sizeFactor, sizeFactor);
		
        // adding loading indicator
        if(progressHudLoadingImageData){
            NSString *iconName = [progressHudLoadingImageData objectForKey: kProgressHudImageDataKey_imageName];
            
            UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:iconName]];
            
            CGRect frm = iv.frame;
            frm.origin.x =  [[progressHudLoadingImageData objectForKey: kProgressHudImageDataKey_imageX] floatValue];
            frm.origin.y = [[progressHudLoadingImageData objectForKey: kProgressHudImageDataKey_imageY] floatValue];
            iv.frame = frm;
            iv.tag = 345;
            [iv rotateImageForeverWithDuration:[[progressHudLoadingImageData objectForKey: kProgressHudImageDataKey_imageRotateDuration] floatValue]];
            [self.hudView addSubview:iv];
            
            [iv release];
        }
            
        // animation
		[UIView animateWithDuration:0.15
							  delay:0
							options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
						 animations:^{
							 self.hudView.transform = CGAffineTransformScale(self.hudView.transform, 1/sizeFactor, 1/sizeFactor);
                             
                          
                             
                             self.alpha = 1;
						 }
						 completion:NULL];
	}
    
    [self setNeedsDisplay];
}

- (void)showSuccessWithStatus:(NSString *)string 
{
    [self show];
    [self dismissWithSuccess:string afterDelay:1];
}

- (void) showFailureWithStatus:(NSString *)string
{
    [self show];
    [self dismissWithError:string afterDelay:1];
}

-(void)toggleShowHideHudView:(BOOL)value
{
    self.hudView.hidden=value;
}

-(BOOL)isHudViewHidden
{
    return self.hudView.hidden;
}
#pragma mark - Singleton Dismiss Methods - All mirrored

+ (void)dismiss 
{
	[[SVProgressHUD sharedView] dismiss];
}

+ (void)dismissWithSuccess:(NSString*)successString 
{
	[[SVProgressHUD sharedView] dismissWithSuccess:successString];
}

+ (void)dismissWithSuccess:(NSString *)successString afterDelay:(NSTimeInterval)seconds 
{
    [[SVProgressHUD sharedView] dismissWithSuccess:successString afterDelay:seconds];
}

+ (void)dismissWithError:(NSString*)errorString 
{
	[[SVProgressHUD sharedView] dismissWithError:errorString];
}

+ (void)dismissWithError:(NSString *)errorString afterDelay:(NSTimeInterval)seconds
{
    [[SVProgressHUD sharedView] dismissWithError:errorString afterDelay:seconds];
}

+ (void) explicitlyDismiss
{
    [[SVProgressHUD sharedView] explicitlyDismiss];
}

#pragma mark - Dismiss Methods
- (void)dismiss 
{
    if (self.showCount > 0)
    {
        self.showCount -= 1;
        
        
        for(UIView* subview in [self.hudView subviews]){
            if(subview.tag == 345){
                [subview removeFromSuperview];
            }
        }
       
    }
    else 
    {
//        ASLog(@"Unbalanced call to [SVProgress dismiss]");
    }
}

-(void) dismissWithSuccess:(NSString *)successString
{
    [self dismissWithStatus:successString error:NO];
}

- (void)dismissWithSuccess:(NSString *)successString afterDelay:(NSTimeInterval)seconds 
{
    [self dismissWithStatus:successString error:NO afterDelay:seconds];
}

- (void)dismissWithError:(NSString*)errorString
{
	[self dismissWithStatus:errorString error:YES];
}

- (void)dismissWithError:(NSString *)errorString afterDelay:(NSTimeInterval)seconds
{
    [self dismissWithStatus:errorString error:YES afterDelay:seconds];
}

- (void)dismissWithStatus:(NSString*)string error:(BOOL)error 
{
	[self dismissWithStatus:string error:error afterDelay:0.9];
}

- (void)dismissWithStatus:(NSString *)string error:(BOOL)error afterDelay:(NSTimeInterval)seconds 
{
    
    if(self.alpha != 1)
    {
//        ASLog(@"weired return");
        return;
    }
    
    if(self.showNetworkIndicator)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
	
    self.imageView.image = (error) ? [UIImage imageNamed:@"SVProgressHUD.bundle/error.png"] : [UIImage imageNamed:@"SVProgressHUD.bundle/success.png"];
	self.imageView.hidden = NO;
	[self setStatus:string];
	[self.spinnerView stopAnimating];
	[self invalidateTimer];
	self.fadeOutTimer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(dismiss) userInfo:nil repeats:NO];
}

-(void) explicitlyDismiss
{
    [self invalidateTimer];
    self.showCount = 0;
    if(self.showNetworkIndicator)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    if (self.isKeyWindow || self.alpha > 0)
    {
        [UIView animateWithDuration:0.15
                              delay:0
                            options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                         animations:^{	
                             self.hudView.transform = CGAffineTransformScale(self.hudView.transform, 0.8, 0.8);
                             self.alpha = 0;
                         }
                         completion:^(BOOL finished){
                             if(self.alpha == 0) 
                             {
                                 [[NSNotificationCenter defaultCenter] removeObserver:self];
                                 [self.previousKeyWindow makeKeyWindow];
                                 // uncomment to make sure UIWindow is gone from app.windows
                                 //ASLog(@"%@", [UIApplication sharedApplication].windows);
                             }
                         }];
    }
}

#pragma mark - Instance Methods

-(void) invalidateTimer
{
    [self.fadeOutTimer invalidate];
    self.fadeOutTimer = nil;
}

- (id)initWithFrame:(CGRect)frame 
{	
    if ((self = [super initWithFrame:frame])) 
    {
		self.userInteractionEnabled = NO;
        self.backgroundColor = [UIColor clearColor];
		self.alpha = 0;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _showCount = 0;
    }
	
    return self;
}

- (void)drawRect:(CGRect)rect 
{    
    CGContextRef context = UIGraphicsGetCurrentContext();
    switch (self.maskType) 
    {
        case SVProgressHUDMaskTypeBlack: 
        {
            [[UIColor colorWithWhite:0 alpha:0.5] set];
            CGContextFillRect(context, self.bounds);
            break;
        }
            
        case SVProgressHUDMaskTypeGradient: 
        {
            size_t locationsCount = 2;
            CGFloat locations[2] = {0.0f, 1.0f};
            CGFloat colors[8] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.75f}; 
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, locations, locationsCount);
            CGColorSpaceRelease(colorSpace);
            
            CGPoint center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
            float radius = MIN(self.bounds.size.width , self.bounds.size.height) ;
            CGContextDrawRadialGradient (context, gradient, center, 0, center, radius, kCGGradientDrawsAfterEndLocation);
            CGGradientRelease(gradient);
            
            break;
        }
    }
}

- (void)setStatus:(NSString *)string 
{
    CGFloat hudWidth = 100;
    CGFloat hudHeight = 100;
    CGFloat stringWidth = 0;
    CGFloat stringHeight = 0;
    CGRect labelRect = CGRectZero;
    
    if(string) 
    {
        CGSize stringSize = [string sizeWithFont:self.stringLabel.font constrainedToSize:CGSizeMake(200, 300)];
        stringWidth = stringSize.width;
        stringHeight = stringSize.height;
        hudHeight = 80+stringHeight;
        
        if(stringWidth > hudWidth)
            hudWidth = ceil(stringWidth/2)*2;
        
        if(hudHeight > 100) {
            labelRect = CGRectMake(12, 66, hudWidth, stringHeight);
            hudWidth+=24;
        } else {
            hudWidth+=24;  
            labelRect = CGRectMake(0, 66, hudWidth, stringHeight);   
        }
    }
	
	self.hudView.bounds = CGRectMake(0, 0, hudWidth, hudHeight);
	
	self.imageView.center = CGPointMake(CGRectGetWidth(self.hudView.bounds)/2, 36);
	
	self.stringLabel.hidden = NO;
	self.stringLabel.text = string;
	self.stringLabel.frame = labelRect;
	
	if(string)
		self.spinnerView.center = CGPointMake(ceil(CGRectGetWidth(self.hudView.bounds)/2)+0.5, 40.5);
	else
		self.spinnerView.center = CGPointMake(ceil(CGRectGetWidth(self.hudView.bounds)/2)+0.5, ceil(self.hudView.bounds.size.height/2)+0.5);
}

- (void)positionHUD:(NSNotification*)notification {
    
    CGFloat keyboardHeight;
    double animationDuration;
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(notification.name == UIKeyboardWillShowNotification || notification.name == UIKeyboardDidShowNotification) {
            if(UIInterfaceOrientationIsPortrait(orientation))
                keyboardHeight = keyboardFrame.size.height;
            else
                keyboardHeight = keyboardFrame.size.width;
        } else
            keyboardHeight = 0;
    } else {
        keyboardHeight = self.visibleKeyboardHeight;
    }
    
    CGRect orientationFrame = [UIScreen mainScreen].bounds;
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    
    if(UIInterfaceOrientationIsLandscape(orientation)) {
        float temp = orientationFrame.size.width;
        orientationFrame.size.width = orientationFrame.size.height;
        orientationFrame.size.height = temp;
        
        temp = statusBarFrame.size.width;
        statusBarFrame.size.width = statusBarFrame.size.height;
        statusBarFrame.size.height = temp;
    }
    
    CGFloat activeHeight = orientationFrame.size.height;
    
    if(keyboardHeight > 0)
        activeHeight += statusBarFrame.size.height*2;
    
    activeHeight -= keyboardHeight;
    CGFloat posY = floor(activeHeight*0.45);
    CGFloat posX = orientationFrame.size.width/2;
    
    CGPoint newCenter;
    CGFloat rotateAngle;
    
    switch (orientation) { 
        case UIInterfaceOrientationPortraitUpsideDown:
            rotateAngle = M_PI; 
            newCenter = CGPointMake(posX, orientationFrame.size.height-posY);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            rotateAngle = -M_PI/2.0f;
            newCenter = CGPointMake(posY, posX);
            break;
        case UIInterfaceOrientationLandscapeRight:
            rotateAngle = M_PI/2.0f;
            newCenter = CGPointMake(orientationFrame.size.height-posY, posX);
            break;
        default: // as UIInterfaceOrientationPortrait
            rotateAngle = 0.0;
            newCenter = CGPointMake(posX, posY);
            break;
    } 
    
    if(notification) {
        [UIView animateWithDuration:animationDuration 
                              delay:0 
                            options:UIViewAnimationOptionAllowUserInteraction 
                         animations:^{
                             [self moveToPoint:newCenter rotateAngle:rotateAngle];
                         } completion:NULL];
    } 
    
    else {
        [self moveToPoint:newCenter rotateAngle:rotateAngle];
    }
    
}

- (void)moveToPoint:(CGPoint)newCenter rotateAngle:(CGFloat)angle 
{
    self.hudView.transform = CGAffineTransformMakeRotation(angle); 
    self.hudView.center = newCenter;
}

- (void)moveToNewYPoint:(CGPoint)newPoint
{
    self.hudView.center = newPoint;
}

#pragma mark - Getters

- (UIView *)hudView 
{
    
    if(!_hudView) {
        _hudView = [[UIView alloc] initWithFrame:CGRectZero];
        _hudView.layer.cornerRadius = 10;
		_hudView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        _hudView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin |
                                     UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin);
        
        [self addSubview:_hudView];
    }
    
    return _hudView;
}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated"

- (UILabel *)stringLabel
{    
    if (_stringLabel == nil) 
    {
        _stringLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		_stringLabel.textColor = [UIColor whiteColor];
		_stringLabel.backgroundColor = [UIColor clearColor];
		_stringLabel.adjustsFontSizeToFitWidth = YES;
		_stringLabel.textAlignment = UITextAlignmentCenter;
		_stringLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
		_stringLabel.font = [UIFont boldSystemFontOfSize:16];
		_stringLabel.shadowColor = [UIColor blackColor];
		_stringLabel.shadowOffset = CGSizeMake(0, -1);
        _stringLabel.numberOfLines = 0;
		[self.hudView addSubview:_stringLabel];
    }
    
    return _stringLabel;
}

#pragma clang diagnostic pop



- (UIImageView *)imageView 
{    
    if (_imageView == nil) 
    {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
		[self.hudView addSubview:_imageView];
    }
    
    return _imageView;
}

- (UIActivityIndicatorView *)spinnerView 
{
    
    if (_spinnerView == nil) {
        _spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		_spinnerView.hidesWhenStopped = YES;
		_spinnerView.bounds = CGRectMake(0, 0, 37, 37);
        if(progressHudLoadingImageData == nil){
            [self.hudView addSubview:_spinnerView];
        }
    }
    
    return _spinnerView;
}

- (CGFloat)visibleKeyboardHeight
{    
    NSAutoreleasePool *autoreleasePool = [[NSAutoreleasePool alloc] init];
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) 
    {
        if(![[testWindow class] isEqual:[UIWindow class]] && ![[testWindow class] isEqual:[SVProgressHUD class]]) 
        {
            keyboardWindow = testWindow;
            break;
        }
    }
    
    // Locate UIKeyboard.  
    UIView *foundKeyboard = nil;
    for (UIView *possibleKeyboard in [keyboardWindow subviews]) 
    {
        // iOS 4 sticks the UIKeyboard inside a UIPeripheralHostView.
        if ([[possibleKeyboard description] hasPrefix:@"<UIPeripheralHostView"]) 
        {
            possibleKeyboard = [[possibleKeyboard subviews] objectAtIndex:0];
        }                                                                                
        
        if ([[possibleKeyboard description] hasPrefix:@"<UIKeyboard"]) 
        {
            foundKeyboard = possibleKeyboard;
            break;
        }
    }
    
    [autoreleasePool release];
    
    if(foundKeyboard && foundKeyboard.bounds.size.height > 100)
        return foundKeyboard.bounds.size.height;
    
    return 0;
}

#pragma mark - Setters
-(void) setShowCount:(NSUInteger)showCount
{
    @synchronized (self)
    {
        if (showCount != _showCount)
        {
            _showCount = MAX(0, showCount);
            //ASLog(@"Show count: %d",showCount);
            if (_showCount == 0)
            {
                static NSTimeInterval graceTime = 0.1;
                [self invalidateTimer];
                self.fadeOutTimer = [NSTimer scheduledTimerWithTimeInterval:graceTime
                                                                     target:self
                                                                   selector:@selector(explicitlyDismiss)
                                                                   userInfo:nil
                                                                    repeats:NO];
            }
        }
    }
}

#pragma mark - Notifications
- (void)registerNotifications 
{
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(positionHUD:) 
                                                 name:UIApplicationDidChangeStatusBarOrientationNotification 
                                               object:nil];  
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(positionHUD:) 
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(positionHUD:) 
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(positionHUD:) 
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(positionHUD:) 
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}


+(void) setProgressHudImageDictionary:(NSDictionary *)dict{
    progressHudLoadingImageData = [[NSDictionary dictionaryWithDictionary:dict] retain];
}
@end