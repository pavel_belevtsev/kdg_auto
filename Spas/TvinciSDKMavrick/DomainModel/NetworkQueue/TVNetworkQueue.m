//
//  TVNetworkQueu.m
//  TVinci
//
//  Created by Avraham Shukron on 6/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//
#import "ASINetworkQueue.h"
#import "SmartLogs.h"
#import "SmartLogs.h"
#import "TVConfigurationManager.h"
#import "TVSessionManager.h"
#import "TVPAPIRequest.h"
#import "TVTokenizationManager.h"
#import "TVSDKSettings.h"
#import "TVNetworkQueue.h"


#import "TVPAPIRequest.h"


@interface TVNetworkQueue ()

@property (strong, nonatomic) NSMutableArray * arrayRequests;

@end

@implementation TVNetworkQueue

-(instancetype)init
{
    if (self = [super init])
    {
        self.arrayRequests = [NSMutableArray array];
    }
    
    return self;
}


#pragma mark - Memory
-(void) dealloc
{
    [self cleen];
}


-(void)cleen
{
    for (TVPAPIRequest * request in self.arrayRequests)
    {
        [request cancel];
        request.requestDelegate = nil;
    }
    
    [self.arrayRequests removeAllObjects];
    
}


#pragma mark - Asynchronic Dangerous Stuff
-(void) sendRequest:(TVPAPIRequest *)request
{
    if ([[TVSDKSettings sharedInstance] offlineMode]) return;
    
    if (request != nil)
    {
        if ([[TVConfigurationManager sharedTVConfigurationManager] checkConfigLifeCycle])
        {

            if (![request isKindOfClass:[TVPAPIRequest class]] || ([request isKindOfClass:[TVPAPIRequest class]] &&  ((TVPAPIRequest *)(request)).ignoreToken == YES) )
            {
                [self sendRequest:request withDelegate:self];
            }
            else
            {
                // since we are going to use the token in the build post body method, we are prepating the token and if it is expired we will get a new one after this process
                [[[TVSessionManager sharedTVSessionManager] tokenizationManager] refreshTokenIfNeededConsiderSafetyMargin:NO owner:self startBlock:nil failedBlock:^{
                    [self sendRequest:request withDelegate:self];
                } completionBlock:^{
                    [self sendRequest:request withDelegate:self];
                }];

            }
        }else{
            [self performSelector:@selector(sendRequest:) withObject:request afterDelay:1.0];
        }
    }
}

-(void) sendRequest:(TVPAPIRequest *) request withDelegate:(id<TVPAPIRequestDelegate>) delegate
{
    request.requestDelegate = delegate;
    [request sendRequestSync:NO];
}


-(void) requestDidStart:(TVPAPIRequest *) request
{
    [self.arrayRequests addObject:request];
}

-(void) requestDidFinish:(TVPAPIRequest *) request
{
    [self.arrayRequests removeObject:request];
}

@end
