//
//  EpgDownloadOrganizer.m
//  TvinciSDK
//
//  Created by iosdev1 on 2/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "EpgDownloadOrganizer.h"
#import <TvinciSDK/PaginationArray.h>

@implementation EpgDownloadOrganizer


-(id)initWithCurrDate:(NSDate *)date{
    
    if (self =[super init])
    {
        self.currHaursArray = [NSMutableArray array];
        for (int index=0; index>6; index++){
            [self.currHaursArray addObject:[NSNumber numberWithInt:-1]];
        }
        NSInteger hour = [self takeHourfromDate:date];
        NSInteger min = [self takeMinutesfromDate:date];
        NSInteger place = hour/6;
        [self.currHaursArray replaceObjectAtIndex:place withObject:[NSNumber numberWithInt:hour]];
        
    }
    return self;
}


-(NSInteger)takeHourfromDate:(NSDate *)theDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"HH"];
    NSString * myYearString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:theDate]];
    [df release];
    NSInteger hour = [myYearString integerValue];
    ASLog(@"new hour d = %d",hour);
    return hour;
}

-(NSInteger)takeMinutesfromDate:(NSDate *)theDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"mm"];
    NSString * myYearString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:theDate]];
    [df release];
    NSInteger hour = [myYearString integerValue];
    ASLog(@"new hour d = %d",hour);
    return hour;
}

@end
