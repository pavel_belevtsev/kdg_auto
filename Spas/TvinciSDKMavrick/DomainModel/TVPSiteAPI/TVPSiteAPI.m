//
//  TVPSiteAPI.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPSiteAPI.h"
#import "TVPAPIRequest.h"
#import "TVinciUtils.h"
#import "SynthesizeSingleton.h"
#import "TVUser.h"
#import "NSString+SGParse.h"
#import "TVSessionManager.h"

#define UserNameKey @"userName"
#define PasswordKey @"password"
#define kDomainNameKey @"sDomainName"


#define kAddDomainDomainNameKey @"domainName"
#define kAddDomainDomainDescriptionKey @"domainDesc"
#define kAddDomainMmasterGuidKey @"masterGuid"

#define SiteGUIDKey @"sSiteGuid"
#define DeviceNameKey @"sDeviceName"
#define DeviceBrandIDKey @"iDeviceBrandID"
#define IsMasterKey @"bMaster"
#define IsActiveKey @"bActive"
#define GalleryIDKey @"galleryID"
#define PageIDKey @"PageID"
#define PictureSizeKey @"picSize"
#define PageSizeKey @"pageSize"
#define PageIndexKey @"pageIndex"
#define StartIndexKey @"start_index"
#define ItemIDKey @"ItemID"
#define OrderByKey @"orderBy"
#define MediaIDKey @"mediaID"


NSString * const MethodNameSignInWithToken = @"SignInWithToken";
NSString * const MethodNameRecordAll = @"RecordAll";
NSString * const MethodNameGetUserStartedWatchingMedias = @"GetUserStartedWatchingMedias";
NSString * const MethodNameCleanUserHistory = @"CleanUserHistory";
NSString * const MethodNameGetSiteGuid = @"GetSiteGuid";
NSString * const MethodNameChangeDeviceDomainStatus = @"ChangeDeviceDomainStatus";
NSString * const MethodNameDoSocialAction = @"DoSocialAction";
NSString * const MethodNameGetBottomProfile = @"GetBottomProfile"; // TODO
NSString * const MethodNameGetFooter = @"GetFooter"; //TODO
NSString * const MethodNameGetGalley = @"GetGallery";
NSString * const MethodNameGetGalleyContent = @"GetGalleryContent";
NSString * const MethodNameGetGalleyItemContent = @"GetGalleryItemContent";
NSString * const MethodNameGetMenu = @"GetMenu"; // TODO
NSString * const MethodNameGetPINForDevice = @"GetPINForDevice"; // TODO
NSString * const MethodNameGetPage = @"GetPage"; // TODO
NSString * const MethodNameGetPageByToken = @"GetPageByToken";// TODO
NSString * const MethodNameGetPageGalleries = @"GetPageGalleries"; // TODO
NSString * const MethodNameGetSecuredSiteGUID = @"GetSecuredSiteGuid"; //TODO
NSString * const MethodNameGetSideProfile = @"GetBottomProfile";// TODO
NSString * const MethodNameGetSiteMap = @"GetSiteMap";
NSString * const MethodNameGetUserCAStatus = @"GetUserStatus";//TODO
NSString * const MethodNameGetUserDeatils = @"GetUserData";
NSString * const MethodNameGetUserByUsername = @"GetUserByUsername";
NSString * const MethodNameGetUsersData = @"GetUsersData";
NSString * const MethodNameIsFacebookUser = @"IsFacebookUser";
NSString * const MethodNameIsUserSignedIn = @"IsUserSignedIn";
NSString * const MethodNameRegisterDeviceByPIN = @"RegisterDeviceByPIN";//TODO
NSString * const MethodNameSetUserData = @"SetUserData"; // TODO
NSString * const MethodNameSignIn = @"SignIn";
NSString * const methodNameSignInSecure = @"SignInSecure";
NSString * const methodNameSSOSignIn = @"SSOSignIn";

NSString * const MethodNameSignOut = @"SignOut";
NSString * const MethodNameSignUp = @"SignUp";// TODO
NSString * const MethodNameSendNewPassword = @"SendNewPassword";
NSString * const MethodNameGetSiteGUIDFromEncryptedSiteGUID = @"GetSiteGuidFromSecured";
NSString * const MethodNameFBConfig = @"FBConfig";
NSString * const MethodNamegetFBuserData = @"GetFBUserData";
NSString * const MethodNameFBUserMerge =@"FBUserMerge";
NSString * const MethodNameFBUserUnmerge =@"FBUserUnmerge";

NSString * const MethodNameFBUserRegister = @"FBUserRegister";
NSString * const MethodNameGetOperators = @"GetOperators";

NSString * const MethodNameGetEPGProgramRules   = @"GetEPGProgramRules";
NSString * const MethodNameSetRuleState         = @"SetRuleState";
NSString * const MethodNameGetDomainGroupRules  = @"GetDomainGroupRules";
NSString * const MethodNameSetDomainGroupRule   = @"SetDomainGroupRule";

NSString * const MethodNameGetGroupOperators   = @"GetGroupOperators";
NSString * const MethodNaemSignInWithToken = @"SignInWithToken";
NSString * const MethodNameGetGroupMediaRules = @"GetGroupMediaRules";
NSString * const MethodNameCheckParentalPIN = @"CheckParentalPIN";

NSString * const MethodNameGenerateDeviceToken = @"GenerateDeviceToken";
NSString * const methodNameExchangeDeviceToken =@"ExchangeDeviceToken";
NSString * const MethodNameRefreshAccessToken = @"RefreshAccessToken";
NSString * const MethodNameGetParentalRules = @"GetParentalRules";
NSString * const MethodNameGetUserParentalRules = @"GetUserParentalRules";
NSString * const MethodNameGetDomainParentalRules = @"GetDomainParentalRules";
NSString * const MethodNameSetUserParentalRules = @"SetUserParentalRules";
NSString * const MethodNameSetDomainParentalRules = @"SetDomainParentalRules";
NSString * const MethodNameGetParentalPIN = @"GetParentalPIN";
NSString * const MethodNameSetParentalPIN = @"SetParentalPIN";
NSString * const MethodNameGetPurchaseSettings = @"GetPurchaseSettings";
NSString * const MethodNameSetPurchaseSettings = @"SetPurchaseSettings";
NSString * const MethodNameGetPurchasePIN = @"GetPurchasePIN";
NSString * const MethodNameSetPurchasePIN = @"SetPurchasePIN";
NSString * const MethodNameValidateParentalPIN = @"ValidateParentalPIN";
NSString * const MethodNameValidatePurchasePIN = @"ValidatePurchasePIN";
NSString * const MethodNameChangeUser = @"ChangeUser";
NSString * const MethodNameGetMediaRules = @"GetMediaRules";
NSString * const MethodNameGetEpgRules = @"GetEpgRules";

@implementation TVPSiteAPI

#pragma mark - FaceBook Methods




/*!
 Session Related functions
 */


+(TVPAPIRequest *) requestforSignInWithToken:(NSString *) token
                                   delegate :(id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSignInWithToken] delegate:delegate];
    request.ignoreToken = YES;
    if (token == nil)
    {
        [NSException raise:NSInvalidArgumentException format:@"Both newUser and password cannot be empty"];
    }
    
    [request.postParameters setObject:token forKey:@"token"];
    return request;

}

+(TVPAPIRequest *) requestForSignUpWithNewUser : (TVUser *) newUser 
                                      password : (NSString *) password 
                                 affiliateCode : (NSString *) affiliateCode 
                                      delegate : (id) delegate
{
    NSDictionary *userDictionary = [newUser keyValueRepresentation];
    NSMutableDictionary *basicDataFromUser = [NSMutableDictionary dictionaryWithDictionary:[userDictionary objectForKey:TVUserBasicInfoKey]];
    NSMutableDictionary *basicData = [NSMutableDictionary dictionary];
    NSMutableDictionary *dynamicData = [NSMutableDictionary dictionary];//[userDictionary objectForKey:TVUserDynamicDataKey];
    
    [basicData setObjectOrNil:[NSString stringWithString:[basicDataFromUser objectForKey:TVUserUsernameKey]] forKey:@"m_sUserNameField"];
    [basicData setObjectOrNil:[NSString stringWithString:[basicDataFromUser objectForKey:@"m_sFirstName"]] forKey:@"m_sFirstNameField"];
    [basicData setObjectOrNil:[NSString stringWithString:[basicDataFromUser objectForKey:@"m_sLastName"]] forKey:@"m_sLastNameField"];
    [basicData setObjectOrNil:[NSString stringWithString:[basicDataFromUser objectForKey:@"m_sEmail"]] forKey:@"m_sEmailField"];
    // not insert User Type Field from app!!! ( no it only useg in tvinci!!!)
    [basicData setObjectOrNil:[NSNull null] forKey:@"m_UserTypeField"];
    if (newUser.phoneNumber != nil)
    {
        [basicData setObjectOrNil:[NSString stringWithString:newUser.phoneNumber] forKey:@"m_sPhoneField"];
    }
    else
    {
        [basicData setObjectOrNil:@"" forKey:@"m_sPhoneField"];
    }
    
    NSArray *ddUserDataFieldArray = nil;
    ddUserDataFieldArray = [NSArray arrayWithObjects:
                                [NSDictionary dictionaryWithObjectsAndKeys:@"NewsLetter",@"m_sDataTypeField",[NSNumber numberWithBool:newUser.isAllowNewsLetter],@"m_sValueField", nil],
                                [NSDictionary dictionaryWithObjectsAndKeys:@"NickName",@"m_sDataTypeField",[basicDataFromUser objectForKey:@"m_sFirstName"],@"m_sValueField", nil],
                                nil];
   
    [dynamicData setObjectOrNil:ddUserDataFieldArray forKey:@"m_sUserDataField"];
    [dynamicData setObjectOrNil:@"" forKey:@"sAffiliateCode"];
    
    if (password.length > 0 && basicData != nil)
    {
        // shefyg - adding missing empty fields
        [basicData setObjectOrNil:@"" forKey:@"m_sAddressField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sCityField"];
        
        NSMutableDictionary *stateFieldDict = [NSMutableDictionary dictionary];
        [stateFieldDict setObjectOrNil:[NSNumber numberWithInt:0] forKey:@"m_nObjecrtIDField"];
        [stateFieldDict setObjectOrNil:@"" forKey:@"m_sStateNameField"];
        [stateFieldDict setObjectOrNil:@"" forKey:@"m_sStateCodeField"];
        
        NSMutableDictionary *stateCountryFieldDict = [NSMutableDictionary dictionary];
        [stateCountryFieldDict setObjectOrNil:[NSNumber numberWithInt:0] forKey:@"m_nObjecrtIDField"];
        [stateCountryFieldDict setObjectOrNil:@"" forKey:@"m_sCountryNameField"];
        [stateCountryFieldDict setObjectOrNil:@"" forKey:@"m_sCountryCodeField"];
        [stateFieldDict setObjectOrNil:stateCountryFieldDict forKey:@"m_CountryField"];
        
        [basicData setObjectOrNil:stateFieldDict forKey:@"m_StateField"];
        
        NSMutableDictionary *countryCountryFieldDict = [NSMutableDictionary dictionary];
        [countryCountryFieldDict setObjectOrNil:[NSNumber numberWithInt:0] forKey:@"m_nObjecrtIDField"];
        [countryCountryFieldDict setObjectOrNil:@"" forKey:@"m_sCountryNameField"];
        [countryCountryFieldDict setObjectOrNil:@"" forKey:@"m_sCountryCodeField"];
        [basicData setObjectOrNil:countryCountryFieldDict forKey:@"m_CountryField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sZipField"];
        // [basicData setObjectOrNil:@"" forKey:@"m_sPhoneField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sFacebookIDField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sFacebookImageField"];
        [basicData setObjectOrNil:[NSNumber numberWithBool:NO] forKey:@"m_bIsFacebookImagePermittedField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sAffiliateCodeField"];
        [basicData setObjectOrNil:@"" forKey:@"m_CoGuidField"];
        [basicData setObjectOrNil:@"" forKey:@"m_ExternalTokenField"];
        [basicData setObjectOrNil:@"" forKey:@"m_sFacebookTokenField"];
        
        [dynamicData setObjectOrNil:ddUserDataFieldArray forKey:@"m_sUserDataField"];
        
        TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSignUp] delegate:delegate];
        [request.postParameters setObjectOrNil:basicData forKey:@"userBasicData"];
        
        [request.postParameters setObjectOrNil:password forKey:@"sPassword"];
        
        if (affiliateCode != nil)
        {
            [request.postParameters setObjectOrNil:affiliateCode forKey:@"sAffiliateCode"];
        }else{
            [request.postParameters setObjectOrNil:@"" forKey:@"sAffiliateCode"];
        }
       
        [request.postParameters setObjectOrNil:dynamicData forKey:@"userDynamicData"];
        
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"Both newUser and password cannot be empty"];
        return nil;
    }
}

+(TVPAPIRequest *) requestforSigninSecureWithUserName:(NSString *) username
                                             password: (NSString *) password
                                            delegate: (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodNameSignInSecure] delegate:delegate];
    request.ignoreToken = YES;
    if (username != nil && password != nil)
    {
        [request.postParameters setObjectOrNil:username forKey:@"sUsername"];
        [request.postParameters setObjectOrNil:password forKey:@"sEncryptedPassword"];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"Both username and password are required for sign in request"];
    }
    return request;

}

+(TVPAPIRequest *) requestForSSOSigninWithUserName:(NSString *) username
                                          password: (NSString *) password
                                        providerID:(NSInteger ) providerID
                                          delegate: (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodNameSSOSignIn] delegate:delegate];
    request.ignoreToken = YES;
    if (username != nil && password != nil)
    {
        [request.postParameters setObjectOrNil:username forKey:@"userName"];
        [request.postParameters setObjectOrNil:password forKey:@"password"];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:providerID] forKey:@"providerID"];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"Both username and password are required for sign in request"];
    }
    return request;
    
}




+(TVPAPIRequest *) requestForSignInWithUserName:(NSString *)username 
                                       password:(NSString *)password 
                                       delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSignIn] delegate:delegate];
    request.ignoreToken = YES;
    if (username != nil && password != nil)
    {
        [request.postParameters setObjectOrNil:username forKey:UserNameKey];
        [request.postParameters setObjectOrNil:password forKey:PasswordKey];
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException 
                    format:@"Both username and password are required for sign in request"];
    }
    return request;
}

+(TVPAPIRequest *) requestForSignOut :(id)delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameSignOut];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestForIsUserSignedIn:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameIsUserSignedIn] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestforGetUsersData:(NSArray *) siteGuids delegate:(id)delegate
{
    NSString * siteGuidsCommaSeperators = [siteGuids componentsJoinedByString:@";"];
    TVPAPIRequest * request  = [self requestWithURL:[self URLForMethodName:MethodNameGetUsersData] delegate:delegate];
    [request.postParameters setObjectOrNil:siteGuidsCommaSeperators forKey:@"sSiteGuid"];
    return request;
    
}

+(TVPAPIRequest *) requestForGetUserDetails : (NSString *) siteGUID delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserDeatils] delegate:delegate];
    if (siteGUID != nil)
    {
        [request.postParameters setObjectOrNil:siteGUID forKey:SiteGUIDKey];
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException 
                    format:@"SiteGUID must be specified for GetUserData request"];
    }
    return request;
}

+(TVPAPIRequest *) requestForGetUserDetailsByUsername : (NSString *) username delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserByUsername] delegate:delegate];
    if (username != nil)
    {
        [request.postParameters setObjectOrNil:username forKey:@"userName"];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"SiteGUID must be specified for GetUserData request"];
    }
    return request;
}


+(TVPAPIRequest *) requestForIsFacebookUser : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameIsFacebookUser] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestForGetPageWithID : (NSInteger) pageID 
                               includeMenu : (BOOL) includeMenu 
                             includeFooter : (BOOL) includeFooter
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetPage] delegate:nil];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageID] forKey:@"ID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:includeMenu] forKey:@"withMenu"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:includeFooter] forKey:@"withFooter"];
    return request;
}

/*!
 Domain Related functions
 */



/*!
 Content Related functions
 */
+(TVPAPIRequest *) requestForGetGalleryWithID : (long) galleryID
                                       atPage : (long) pageID
                                     delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetGalley] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:galleryID] forKey:GalleryIDKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:pageID] forKey:PageIDKey];
    return request;
}

+(TVPAPIRequest *) requestForGetGalleyContent : (long) galleryID 
                                       atPage : (long) pageID
                                  pictureSize : (CGSize) pictureSize
                                     pageSize : (NSInteger) pageSize
                               startFromIndex : (NSUInteger) startIndex
                                     delegate : (id) delegate 
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetGalleyContent] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:galleryID] forKey:@"ID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:pageID] forKey:PageIDKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:PageSizeKey];
    NSString *sizeString = TVPictureSizeStringFromCGSize(pictureSize);
    [request.postParameters setObjectOrNil:sizeString forKey:PictureSizeKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithUnsignedInteger:startIndex] forKey:StartIndexKey];
    return request;
}

+(TVPAPIRequest *) requestForGetGalleryItemContent : (long) itemID
                                         atGallery : (long) galleryID
                                            atPage : (long) pageID
                                       pictureSize : (CGSize) pictureSize
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSInteger) pageIndex
                                           orderBy : (TVOrderBy) orderBy
                                          delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetGalleyItemContent] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:itemID] forKey:ItemIDKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:galleryID] forKey:@"GalleryID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithLong:pageID] forKey:PageIDKey];
    NSString *sizeString = TVPictureSizeStringFromCGSize(pictureSize);
    [request.postParameters setObjectOrNil:sizeString forKey:PictureSizeKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:PageSizeKey];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageIndex] forKey:PageIndexKey];
    NSString *orderByString = TVNameForOrderBy(orderBy);
    [request.postParameters setObjectOrNil:orderByString forKey:OrderByKey];
    return request;
}

/*!
 Social Related functions
 */

+(TVPAPIRequest *) requestForDoSocialAction : (TVSocialAction) action
                                 withMediaId: (NSInteger) mediaID
                                 onPlatform : (TVSocialPlatform) platform
                             additionalInfo : (NSString *) extraInfo 
                                   delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameDoSocialAction] delegate:delegate];
    NSString *socialActionName = TVNameForSocialAction(action);
    [request.postParameters setObjectOrNil:socialActionName forKey:@"socialAction"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    NSString *socialPlatform = TVNameForSocialPlatform(platform);
    [request.postParameters setObjectOrNil:socialPlatform forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:extraInfo forKey:@"actionParam"];
    return request;
}

+(TVPAPIRequest *) requestForGetSiteMap : (id) delegate
{
    NSURL *url = [self URLForMethodName:MethodNameGetSiteMap];
    TVPAPIRequest *request = [self requestWithURL:url delegate:delegate];
    return request;
}

+(TVPAPIRequest *)requestForGetMenu : (NSInteger) menuID
                           delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMenu] delegate:delegate];
    NSNumber *menuIDNumber = [NSNumber numberWithInteger:menuID];
    [request.postParameters setObjectOrNil:menuIDNumber forKey:@"ID"];
    return request;
}

+(TVPAPIRequest *) requestForChangeDeviceDomainStatus : (BOOL) isActive
                                             delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameChangeDeviceDomainStatus] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isActive] forKey:@"bActive"];
    return request;
}

+(TVPAPIRequest *) requestForSendNewPassword : (NSString *) username
                                    delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSendNewPassword] delegate:delegate];
    [request.postParameters setObjectOrNil:username forKey:@"sUserName"];
    return request;
}

+(TVPAPIRequest *) requestForGetClearSiteGUIDFromEncryptedSiteGUID : (NSString *) encryptedSiteGUID
                                                          delegate : (id) delegate
{
    if (encryptedSiteGUID != nil)
    {
        NSString *urlDecoded = [encryptedSiteGUID stringByDecodingURLFormat];
        
        TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetSiteGUIDFromEncryptedSiteGUID] delegate:delegate];
        //[request.postParameters setObjectOrNil:encryptedSiteGUID forKey:@"encSiteGuid"];
        [request.postParameters setObjectOrNil:urlDecoded forKey:@"encSiteGuid"];
        
        return request;
    }
    else 
    {
        [NSException raise:NSInvalidArgumentException format:@"You must provide encrypted siteGUID"];
        return nil;
    }
}


#pragma mark - Face book function



+(TVPAPIRequest *) requestForFBConfigWithDelegate:(id)delegate
{
    BOOL testFlag = 0;
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameFBConfig] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:testFlag] forKey:@"sSTG"];
    return request;
    
}

+(TVPAPIRequest *) requestForGetFBUserDataWithToken:(NSString *) token delegate:(id)delegate

{
    BOOL testFlag = 0;
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNamegetFBuserData] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"sToken"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:testFlag] forKey:@"sSTG"];
    return request;
}

+(TVPAPIRequest *) requestForFBUserMergeWithToken:(NSString *) token facebookID:(NSString *) facebookID username:(NSString *) username password:(NSString *) password  delegate:(id)delegate
{
   
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameFBUserMerge] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"sToken"];
    [request.postParameters setObjectOrNil:facebookID forKey:@"sFBID"];
    [request.postParameters setObjectOrNil:username forKey:@"sUsername"];
    [request.postParameters setObjectOrNil:password forKey:@"sPassword"];
    return request;
}




+(TVPAPIRequest *) requestForFBUserRegisterWithToken:(NSString *) token createNewDomain:(BOOL) createNewDomain  delegate:(id)delegate

{
    
    BOOL testFlag = 0;
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameFBUserRegister] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"sToken"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:testFlag] forKey:@"sSTG"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:createNewDomain] forKey:@"bCreateNewDomain"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:NO] forKey:@"bGetNewsletter"];
    
    return request;
}

+(TVPAPIRequest *) requestForGetOperatorsWithCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData delegate:(id)delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetMenu] delegate:delegate];
    request.customInitObject = customInitObject;
    for (NSString * key in postData.allKeys)
    {
        [request.postParameters setObjectOrNil:[postData objectForKey:key] forKey:key];
    }
    return request;
}

+(TVPAPIRequest *) requestForGetOperatorsWithOperators:(NSArray *) operaors delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetOperators] delegate:delegate];
    [request.postParameters setObjectOrNil:operaors forKey:@"operators"];
    return request;
}


+(TVPAPIRequest *) requestForSetUserDataWithUser:(TVUser *) user
                                        delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserData] delegate:delegate];

    [request.postParameters addEntriesFromDictionary:[user keyValueRepresentationTemporary]];
    
    return request;
}

+(TVPAPIRequest *) requestForSetUserDynamicDataWithUser:(NSString *) key
                                       value:(NSString *) value
                                        delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserData] delegate:delegate];
    
    [request.postParameters setObjectOrNil:key forKey:@"sKey"];
    [request.postParameters setObjectOrNil:value forKey:@"sValue"];
    
    return request;
}


+(TVPAPIRequest *) requestForGetSiteGuidWithUserName:(NSString *) userName
                                            password:(NSString *) password
                                            delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetSiteGuid] delegate:delegate];
    
    [request.postParameters setObjectOrNil:userName forKey:@"userName"];
    [request.postParameters setObjectOrNil:password forKey:@"password"];
    
    return request;
}

+(TVPAPIRequest *) requestForCleanUserHistoryWithMediaIDs:(NSArray *) mediaIDArray
                                                 delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameCleanUserHistory] delegate:delegate];
    [request.postParameters setObjectOrNil:mediaIDArray forKey:@"mediaIDs"];
    return request;

}


+(TVPAPIRequest *) requestForGetUserStartedWatchingMediasWithNumOfItems:(NSInteger) numOfItems
                                                               delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserStartedWatchingMedias] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:numOfItems] forKey:@"numOfItems"];
    return request;

}



+(TVPAPIRequest *) requestForGetSecuredSiteGUIDWithDelegate:(id)delegate {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetSecuredSiteGUID] delegate:delegate];
    return request;
}

 



#pragma mark - Linear Pin
+(TVPAPIRequest *) requestForGetEPGProgramRulesForMediaID : (NSInteger) mediaId
                                             andProgramID : (NSInteger) programID
                                                    andIP : (NSString *) IP
                                                 delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGProgramRules] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaId] forKey:@"MediaId"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:programID] forKey:@"programId"];
    [request.postParameters setObjectOrNil:IP forKey:@"IP"];
    return request;
    
}

+(TVPAPIRequest *) requestForSetRuleStateWithRuleID : (NSInteger)ruleID
                                           isActive : (BOOL) isActive
                                           delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetRuleState] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:ruleID] forKey:@"ruleID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isActive] forKey:@"isActive"];
    return request;
    
}

+(TVPAPIRequest *) requestForGetDomainGroupRulesWithDelegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainGroupRules] delegate:delegate];
    return request;
    
}

+(TVPAPIRequest *) requestForSetDomainGroupRuleWithRuleID : (NSInteger)ruleID
                                                 isActive : (BOOL) isActive
                                                      pin : (NSString *)pin
                                                 delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetDomainGroupRule] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:ruleID] forKey:@"ruleID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isActive] forKey:@"isActive"];
    [request.postParameters setObjectOrNil:pin forKey:@"PIN"];
    
    return request;
    
}

+(TVPAPIRequest *) requestForGetGroupMediaRules:(NSInteger)mediaID
                                       delegate:(id)delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetGroupMediaRules];
    // ASLog(@"url = %@",URL);
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    return request;
}

+(TVPAPIRequest *) requestForCheckParentalPINWithRuleID:(NSInteger)ruleID
                                                 andPin:(NSString *)pin
                                               delegate:(id)delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameCheckParentalPIN];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:ruleID] forKey:@"ruleID"];
    [request.postParameters setObjectOrNil:pin forKey:@"PIN"];
    return request;
}


+(TVPAPIRequest *) requestForGetGroupOperators : (NSInteger)ruleID
                                         scope : (NSString *)scope
                                      delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetGroupOperators] delegate:delegate];
    [request.postParameters setObjectOrNil:scope forKey:@"scope"];
    
    return request;
}


/**
 *
 *
 *  @param accountNumber    This is the yes account number that we have from "dynamic data" retuned on signon process
 *  @param channelName      This is EPG meta data value of key :"YesChannel"
 *  @param recoredStartDate start date from epg program in this format: "YYYYMMDD"
 *  @param recoredStartTime start time from epg program in this format: "HH:mm:SS"
 *  @param EPGIdeitifier    This is EPG program Identifier "EPG_IDENTIFIER": "program-373128-****537609****"
 *  @param delegate        
 *
 *  @return TVPAPIRequest
 */
+(TVPAPIRequest *) requestforRecordAllWithAccountNumber:(NSString *) accountNumber
                                            channelName:(NSString *) channelName
                                       recoredStartDate:(NSString *) recoredStartDate
                                      reconredStartTime:(NSString *) recoredStartTime
                                          EPGIdeitifier:(NSString *) EPGIdeitifier
                                           SerialNumber:(NSString *) serialNumber
                                               delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRecordAll] delegate:delegate];
    
    [request.postParameters setObject:accountNumber forKey:@"accountNumber"];
    [request.postParameters setObject:channelName forKey:@"channelCode"];
    [request.postParameters setObject:recoredStartDate forKey:@"recordDate"];
    [request.postParameters setObject:recoredStartTime forKey:@"recordTime"];
    [request.postParameters setObject:EPGIdeitifier forKey:@"versionId"];

    if (![serialNumber isEqualToString:@""]) {
        [request.postParameters setObject:serialNumber forKey:@"serialNumber"];
    }


    return request;
}


+(TVPAPIRequest *) requestForFBUserUnmergeWithToken:(NSString *) token username:(NSString *) username password:(NSString *) password  delegate:(id)delegate
{
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameFBUserUnmerge] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"token"];
    [request.postParameters setObjectOrNil:username forKey:@"username"];
    [request.postParameters setObjectOrNil:password forKey:@"password"];
    return request;
}



+(TVPAPIRequest *) requestForRefreshAccessTokenWithRefreshToken:(NSString *) refreshToken
                                            delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRefreshAccessToken] delegate:delegate];
    [request.postParameters setObjectOrNil:refreshToken forKey:@"refreshToken"];
    request.ignoreToken = YES;
    return request;

}

+(TVPAPIRequest *) requestForGetParentalRules {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetParentalRules] delegate:nil];
    return request;
    
}


+(TVPAPIRequest *) requestForGetUserParentalRulesWithSiteGuid:(NSString *) siteGuid
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserParentalRules] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;
    
}





+(TVPAPIRequest *) requestForGetDomainParentalRules {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainParentalRules] delegate:nil];
    return request;
    
}





+(TVPAPIRequest *) requestForSetUserParentalRulesWithSiteGuid:(NSString *) siteGuid
                                                       ruleId:(NSInteger ) ruleId
                                                     isActive:(BOOL) isActive;
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserParentalRules] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:ruleId] forKey:@"ruleId"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:isActive] forKey:@"isActive"];
    return request;
    
}





+(TVPAPIRequest *) requestForSetDomainParentalRulesWithRuleId:(NSInteger ) ruleId
                                                     isActive:(BOOL) isActive
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetDomainParentalRules] delegate:nil];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:ruleId] forKey:@"ruleId"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:isActive] forKey:@"isActive"];

    return request;
    
}





+(TVPAPIRequest *) requestForGetParentalPINWithSiteGuid:(NSString *) siteGuid
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetParentalPIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;
    
}





+(TVPAPIRequest *) requestForSetParentalPINWithSiteGuid:(NSString *) siteGuid
                                                    pin:(NSString *) pin
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetParentalPIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:pin forKey:@"pin"];
    return request;
    
}




+(TVPAPIRequest *) requestForGetPurchaseSettingsWithSiteGuid:(NSString *) siteGuid
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetPurchaseSettings] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;
    
}




+(TVPAPIRequest *) requestForSetPurchaseSettingsWithSiteGuid:(NSString *) siteGuid
                                                     permission:(TVPurchasePermission) permission
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetPurchaseSettings] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInt:permission] forKey:@"setting"];
    return request;
    
}



+(TVPAPIRequest *) requestForGetPurchasePINWithSiteGuid:(NSString *) siteGuid
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetPurchasePIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;
    
}



+(TVPAPIRequest *) requestForSetPurchasePINWithSiteGuid:(NSString *) siteGuid
                                                    pin:(NSString *) pin {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetPurchasePIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:pin forKey:@"pin"]; 
    return request;
    
}



+(TVPAPIRequest *) requestForValidateParentalPINWithSiteGuid:(NSString *) siteGuid
                                                         pin:(NSString *) pin {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameValidateParentalPIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:pin forKey:@"pin"]; 
    return request;
    
}




+(TVPAPIRequest *) requestForValidatePurchasePINWithSiteGuid:(NSString *) siteGuid
                                                         pin:(NSString *) pin {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameValidatePurchasePIN] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:pin forKey:@"pin"]; 
    return request;
    
}


+(TVPAPIRequest *) requestForChangeUserWithSiteGuid:(NSString *) siteGuid {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameChangeUser] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;

}


+(TVPAPIRequest *) requestForGetMediaRulesWithSiteGuid:(NSString *) siteGuid
                                               mediaId:(NSString *) mediaId {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetMediaRules] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaId.integerValue] forKey:@"mediaId"];
    return request;

}




+(TVPAPIRequest *) requestForGetEpgRulesWithSiteGuid:(NSString *) siteGuid
                                               epgId:(NSString *) epgId
                                             mediaId:(NSString *) mediaID {
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEpgRules] delegate:nil];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:epgId.integerValue]  forKey:@"epgId"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID.integerValue] forKey:@"channelMediaId"];
    return request;

}


@end
