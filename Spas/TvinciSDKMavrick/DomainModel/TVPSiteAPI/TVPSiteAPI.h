//
//  TVPSiteAPI.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseTVPClient.h"
#import "TVConstants.h"
@class TVUser;

/*!
 @interface TVPSiteAPI
 @abstract A Client of the TVinci Site API, providing convinient methods to preform all the available calls to the API
 @discussion You should use this class as much as possible to make calls to the TVP API.
 The reason for that is to ensure that all the parameters are of the right type, the request URL is correct and so on.
 
 You build a request by using one of the API methods that returns TVPAPIRequest object.
 Once you have a request object you can configure it to fill your needs, like changing the timeout, specifiying your own
 callback blocks and selector and so on.
 
 It is not recommended to change the paramaters of the request returned by one of the API methods.
 
 @availability 1.0
 */
@interface TVPSiteAPI : BaseTVPClient


#pragma mark - API Requests




#pragma mark - FB methods








/*!
    @functiongroup Session Related functions
 */


/**
*  Sign in with encrippted token
*
*  @param token - The enctipted token for the sign in process
*
*  @return A request with the specified parameters and a URL for the requested method.
*  @throws NSInvalidArgumentException if token is nil
*/
+(TVPAPIRequest *) requestforSignInWithToken:(NSString *) token
                                   delegate :(id) delegate;


/*!
 
 @abstract return a request for signing up with new user details
 @param newUser - The new user object. must contain email, first name and last name at least.
 @param password must not be nil.
 @param affiliateCode
 @return A request with the specified parameters and a URL for the requested method.
 @throws NSInvalidArgumentException if either newUser or password are nil
 */




+(TVPAPIRequest *) requestForSignUpWithNewUser : (TVUser *) newUser 
                                      password : (NSString *) password 
                                 affiliateCode : (NSString *) affiliateCode 
                                      delegate : (id) delegate;







+(TVPAPIRequest *) requestforSigninSecureWithUserName:(NSString *) username
                                             password: (NSString *) password
                                             delegate: (id)delegate;

+(TVPAPIRequest *) requestForSSOSigninWithUserName:(NSString *) username
                                          password: (NSString *) password
                                        providerID:(NSInteger ) providerID
                                          delegate: (id)delegate;

/*!
 @abstract return a request for signing in with specific username and password.
 @param username must not be nil.
 @param password must not be nil.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 @throws NSInvalidArgumentException if either username or password are nil
 */
+(TVPAPIRequest *) requestForSignInWithUserName : (NSString *) username 
                                       password : (NSString *) password 
                                       delegate : (id) delegate;
/*!
 @abstract return a request for signing out.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForSignOut : (id) delegate;

/*!
 @abstract return a request for checking if the current user is sign in.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForIsUserSignedIn : (id) delegate;

/*!
 @abstract return a request for getting the current user details.
 @param siteGUID I have no idea what this parameter is.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetUserDetails : (NSString *) siteGUID delegate : (id) delegate;


/*!
 @abstract return a request for getting the user details by a given username.
 @param username - the username of the user.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetUserDetailsByUsername : (NSString *) username delegate : (id) delegate;

/*!
 @abstract return a request for Get Users Data
 @param siteGuids parameter that represent array of user ids on tvinci.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestforGetUsersData:(NSArray *) siteGuids delegate:(id)delegate;



/*!
 @abstract return a request for setting user details.
 @param siteGUID parameter that represent the user id on tvinci.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForSetUserDataWithUser:(TVUser *) user
                                        delegate:(id)delegate;

/*!
 @abstract return a request for checking if the current user is logged in through Facebook.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 */
+(TVPAPIRequest *) requestForGetSiteMap : (id) delegate DEPRECATED_ATTRIBUTE;
+(TVPAPIRequest *) requestForIsFacebookUser : (id) delegate;


+(TVPAPIRequest *)requestForGetMenu : (NSInteger) menuID
                           delegate : (id) delegate;

+(TVPAPIRequest *) requestForGetPageWithID : (NSInteger) pageID 
                               includeMenu : (BOOL) includeMenu 
                             includeFooter : (BOOL) includeFooter DEPRECATED_ATTRIBUTE;

/*!
 @functiongroup Domain Related functions
 */

/*!
 @functiongroup Content Related functions
 */
+(TVPAPIRequest *) requestForGetGalleryWithID : (long) galleryID
                                       atPage : (long) pageID
                                     delegate : (id) delegate;

+(TVPAPIRequest *) requestForGetGalleyContent : (long) galleryID 
                                       atPage : (long) pageID
                                  pictureSize : (CGSize) pictureSize
                                     pageSize : (NSInteger) pageSize     
                               startFromIndex : (NSUInteger) startIndex
                                     delegate : (id) delegate;

+(TVPAPIRequest *) requestForGetGalleryItemContent : (long) itemID
                                         atGallery : (long) galleryID
                                            atPage : (long) pageID
                                       pictureSize : (CGSize) pictureSize
                                          pageSize : (NSInteger) pageSize
                                         pageIndex : (NSInteger) pageIndex
                                           orderBy : (TVOrderBy) orderBy
                                          delegate : (id) delegate;

/*!
 @functiongroup Social Related functions
 */
+(TVPAPIRequest *) requestForDoSocialAction : (TVSocialAction) action
                                 withMediaId: (NSInteger) mediaID
                                 onPlatform : (TVSocialPlatform) platform
                             additionalInfo : (NSString *) extraInfo 
                                   delegate : (id) delegate;

+(TVPAPIRequest *) requestForChangeDeviceDomainStatus : (BOOL) isActive
                                             delegate : (id) delegate;

+(TVPAPIRequest *) requestForSendNewPassword : (NSString *) username
                                             delegate : (id) delegate;

+(TVPAPIRequest *) requestForGetClearSiteGUIDFromEncryptedSiteGUID : (NSString *) encryptedSiteGUID
                                    delegate : (id) delegate DEPRECATED_ATTRIBUTE;



+(TVPAPIRequest *) requestForGetSiteGuidWithUserName:(NSString *) userName
                                            password:(NSString *) password
                                            delegate:(id)delegate;



+(TVPAPIRequest *) requestForCleanUserHistoryWithMediaIDs:(NSArray *) mediaIDArray
                                                 delegate:(id)delegate;


/*!
 @abstract return a request for getting a secured Site Guid.
 @param delegate the delegate for handling the request lifecycle callbacks.
 @return A request with the specified parameters and a URL for the requested method.
 @functiongroup Player related functions

 Use the string in the response to set it in TVMediaToPlayInfo object in order
 to acquire rights for encrypted streams.
 */
+(TVPAPIRequest *) requestForGetSecuredSiteGUIDWithDelegate:(id)delegate;

#pragma mark - facebook
+(TVPAPIRequest *) requestForFBConfigWithDelegate:(id)delegate;

+(TVPAPIRequest *) requestForGetFBUserDataWithToken:(NSString *) token delegate:(id)delegate;

+(TVPAPIRequest *) requestForFBUserMergeWithToken:(NSString *) token facebookID:(NSString *) facebookID username:(NSString *) username password:(NSString *) password  delegate:(id)delegate;

+(TVPAPIRequest *) requestForFBUserRegisterWithToken:(NSString *) token
                                     createNewDomain:(BOOL) createNewDomain
                                            delegate:(id)delegate;

+(TVPAPIRequest *) requestForGetOperatorsWithCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData delegate:(id)delegate;

+(TVPAPIRequest *) requestForGetOperatorsWithOperators:(NSArray *) operaors
                                              delegate:(id)delegate;

+(TVPAPIRequest *) requestForGetUserStartedWatchingMediasWithNumOfItems:(NSInteger) numOfItems
                                                               delegate:(id)delegate;


#pragma Linear Pin

+(TVPAPIRequest *) requestForSetDomainGroupRuleWithRuleID : (NSInteger)ruleID
                                                 isActive : (BOOL) isActive
                                                      pin : (NSString *)pin
                                                 delegate : (id)delegate;


+(TVPAPIRequest *) requestForGetEPGProgramRulesForMediaID : (NSInteger) mediaId
                                             andProgramID : (NSInteger) programID
                                                    andIP : (NSString *) IP
                                                 delegate : (id)delegate;

+(TVPAPIRequest *) requestForGetGroupMediaRules:(NSInteger)mediaID
                                       delegate:(id)delegate;

+(TVPAPIRequest *) requestForCheckParentalPINWithRuleID:(NSInteger)ruleID
                                                 andPin:(NSString *)pin
                                               delegate:(id)delegate;

+(TVPAPIRequest *) requestForGetDomainGroupRulesWithDelegate:(id)delegate;

+(TVPAPIRequest *) requestForSetRuleStateWithRuleID : (NSInteger)ruleID
                                           isActive : (BOOL) isActive
                                           delegate : (id)delegate;

+(TVPAPIRequest *) requestForGetGroupOperators : (NSInteger)ruleID
                                         scope : (NSString *)scope
                                      delegate : (id)delegate;


+(TVPAPIRequest *) requestforRecordAllWithAccountNumber:(NSString *) accountNumber
                                            channelName:(NSString *) channelName
                                       recoredStartDate:(NSString *) recoredStartDate
                                      reconredStartTime:(NSString *) recoredStartTime
                                          EPGIdeitifier:(NSString *) EPGIdeitifier
                                           SerialNumber:(NSString *) serialNumber
                                               delegate:(id)delegate;

+(TVPAPIRequest *) requestForFBUserUnmergeWithToken:(NSString *) token
                                           username:(NSString *) username
                                           password:(NSString *) password
                                           delegate:(id)delegate;


+(TVPAPIRequest *) requestForRefreshAccessTokenWithRefreshToken:(NSString *) refreshToken
                                                       delegate:(id)delegate;



+(TVPAPIRequest *) requestForGetParentalRules;

+(TVPAPIRequest *) requestForGetUserParentalRulesWithSiteGuid:(NSString *) siteGuid;

+(TVPAPIRequest *) requestForGetDomainParentalRules;

+(TVPAPIRequest *) requestForSetUserParentalRulesWithSiteGuid:(NSString *) siteGuid
                                                       ruleId:(NSInteger ) ruleId
                                                     isActive:(BOOL) isActive;

+(TVPAPIRequest *) requestForSetDomainParentalRulesWithRuleId:(NSInteger ) ruleId
                                                     isActive:(BOOL) isActive;

+(TVPAPIRequest *) requestForGetParentalPINWithSiteGuid:(NSString *) siteGuid;

+(TVPAPIRequest *) requestForSetParentalPINWithSiteGuid:(NSString *) siteGuid
                                                    pin:(NSString *) pin;

+(TVPAPIRequest *) requestForGetPurchaseSettingsWithSiteGuid:(NSString *) siteGuid;

+(TVPAPIRequest *) requestForSetPurchaseSettingsWithSiteGuid:(NSString *) siteGuid
                                                  permission:(TVPurchasePermission) permission;

+(TVPAPIRequest *) requestForGetPurchasePINWithSiteGuid:(NSString *) siteGuid;


+(TVPAPIRequest *) requestForSetPurchasePINWithSiteGuid:(NSString *) siteGuid
                                                    pin:(NSString *) pin;


+(TVPAPIRequest *) requestForValidateParentalPINWithSiteGuid:(NSString *) siteGuid
                                                         pin:(NSString *) pin;

+(TVPAPIRequest *) requestForValidatePurchasePINWithSiteGuid:(NSString *) siteGuid
                                                         pin:(NSString *) pin;


+(TVPAPIRequest *) requestForChangeUserWithSiteGuid:(NSString *) siteGuid;



+(TVPAPIRequest *) requestForGetMediaRulesWithSiteGuid:(NSString *) siteGuid
                                               mediaId:(NSString *) mediaId;



+(TVPAPIRequest *) requestForGetEpgRulesWithSiteGuid:(NSString *) siteGuid
                                               epgId:(NSString *) epgId
                                             mediaId:(NSString *) mediaID;





@end


#pragma mark - Constants

extern NSString * const MethodNameRecordAll;
extern NSString * const MethodNameGetSiteGuid;
extern NSString * const MethodNameChangeDeviceDomainStatus;
extern NSString * const MethodNameDoSocialAction;
extern NSString * const MethodNameGetBottomProfile; // TODO
extern NSString * const MethodNameGetFooter; //TODO
extern NSString * const MethodNameGetGalley;
extern NSString * const MethodNameGetGalleyContent;
extern NSString * const MethodNameGetGalleyItemContent;
extern NSString * const MethodNameGetMenu;
extern NSString * const MethodNameGetPINForDevice; // TODO
extern NSString * const MethodNameGetPage; // TODO
extern NSString * const MethodNameGetPageByToken;// TODO
extern NSString * const MethodNameGetPageGalleries; // TODO
extern NSString * const MethodNameGetSecuredSiteGUID; //TODO
extern NSString * const MethodNameGetSiteGUIDFromEncryptedSiteGUID;
extern NSString * const MethodNameGetSideProfile;// TODO
extern NSString * const MethodNameGetSiteMap;
extern NSString * const MethodNameGetUserCAStatus;//TODO
extern NSString * const MethodNameGetUserDeatils;
extern NSString * const MethodNameIsFacebookUser;
extern NSString * const MethodNameIsUserSignedIn;
extern NSString * const MethodNameRegisterDeviceByPIN;//TODO
extern NSString * const MethodNameSetUserData; // TODO
extern NSString * const MethodNameSignIn;
extern NSString * const methodNameSignInSecure ;
extern NSString * const MethodNameSignOut;
extern NSString * const MethodNameSignUp;
extern NSString * const MethodNameSendNewPassword;
extern NSString * const MethodNameFBConfig ;
extern NSString * const MethodNamegetFBuserData ;
extern NSString * const MethodNameFBUserMerge;
extern NSString * const MethodNameFBUserRegister ;
extern NSString * const MethodNameGetOperators;
extern NSString * const MethodNameCleanUserHistory;
extern NSString * const MethodNameGetEPGProgramRules;
extern NSString * const MethodNameSetRuleState;
extern NSString * const MethodNameGetDomainGroupRules;
extern NSString * const MethodNameSetDomainGroupRule;
extern NSString * const MethodNameGetGroupOperators;
extern NSString * const MethodNameSignInWithToken;
extern NSString * const MethodNaemSignInWithToken;
extern NSString * const MethodNameGetGroupMediaRules;
extern NSString * const MethodNameCheckParentalPIN;
extern NSString * const methodNameSSOSignIn;
extern NSString * const MethodNameFBUserUnmerge;
extern NSString * const MethodNameGenerateDeviceToken;
extern NSString * const methodNameExchangeDeviceToken;
extern NSString * const MethodNameRefreshAccessToken;
extern NSString * const MethodNameGetParentalRules;
extern NSString * const MethodNameGetUserParentalRules;
extern NSString * const MethodNameGetDomainParentalRules;
extern NSString * const MethodNameSetUserParentalRules;
extern NSString * const MethodNameSetDomainParentalRules;
extern NSString * const MethodNameGetParentalPIN;
extern NSString * const MethodNameSetParentalPIN;
extern NSString * const MethodNameGetPurchaseSettings;
extern NSString * const MethodNameSetPurchaseSettings;
extern NSString * const MethodNameGetPurchasePIN;
extern NSString * const MethodNameSetPurchasePIN;
extern NSString * const MethodNameGetParentalMediaRules;
extern NSString * const MethodNameGetParentalEPGRules;
extern NSString * const MethodNameValidateParentalPIN;
extern NSString * const MethodNameValidatePurchasePIN;
extern NSString * const MethodNameChangeUser;
extern NSString * const MethodNameGetMediaRules;
extern NSString * const MethodNameGetEpgRules;