//
//  TVPConditionalAccessAPIRequestParams.m
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVPConditionalAccessAPIRequestParams.h"

@implementation RPGetSubscriptionsPricesWithCoupon @end
@implementation RPGetItemsPricesWithCoupons @end
@implementation RPIsPermittedSubscription @end
@implementation RPCancelSubscription @end