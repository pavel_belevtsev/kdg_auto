//
//  TVPConditionalAccessAPI.m
//  TvinciSDK
//
//  Created by Alex Zchut on 2/11/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVPConditionalAccessAPI.h"
#import "NSDate+Format.h"

@implementation TVPConditionalAccessAPI

#define methodName_GetSubscriptionsPricesWithCoupon  @"GetSubscriptionsPricesWithCoupon"
#define methodName_GetItemsPricesWithCoupons         @"GetItemsPricesWithCoupons"
#define methodName_IsPermittedSubscription           @"IsPermittedSubscription"
#define methodName_CancelSubscription                @"CancelSubscription"
#define methodName_GetDomainPermittedSubscriptions   @"GetDomainPermittedSubscriptions"




NSString * const MethodNameGetDomainServices                = @"GetDomainServices";
NSString * const MethodNameGetEPGLicensedData               = @"GetEPGLicensedData";
NSString * const MethodNameCancelServiceNow                 = @"CancelServiceNow";
NSString * const MethodNameCancelSubscriptionRenewal        = @"CancelSubscriptionRenewal";


+(TVPAPIRequest *) requestForGetSubscriptionsPricesWithCoupon: (void(^)(RPGetSubscriptionsPricesWithCoupon *requestParams))requestParams {
    
    RPGetSubscriptionsPricesWithCoupon *reqParams = [RPGetSubscriptionsPricesWithCoupon new];
    requestParams(reqParams);
    
    NSURL *URL = [self URLForMethodName:methodName_GetSubscriptionsPricesWithCoupon];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
    [request.postParameters setObjectOrNil:reqParams.arrSubscriptionIds forKey:@"sSubscriptions"];
    [request.postParameters setObjectOrNil:reqParams.userGuid      forKey:@"sUserGUID"];
    [request.postParameters setObjectOrNil:reqParams.couponCode    forKey:@"sCouponCode"];
    [request.postParameters setObjectOrNil:reqParams.countryCode   forKey:@"sCountryCd2"];
    [request.postParameters setObjectOrNil:reqParams.languageCode  forKey:@"sLanguageCode3"];
    [request.postParameters setObjectOrNil:reqParams.deviceName    forKey:@"sDeviceName"];
    return request;
}

+(TVPAPIRequest *) requestForGetItemsPricesWithCoupons: (void(^)(RPGetItemsPricesWithCoupons *requestParams))requestParams {
    
    RPGetItemsPricesWithCoupons *reqParams = [RPGetItemsPricesWithCoupons new];
    requestParams(reqParams);
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_GetItemsPricesWithCoupons] delegate:nil];
    
    [request.postParameters setObjectOrNil:reqParams.fileIDsArray  forKey:@"nMediaFiles"];
    [request.postParameters setObjectOrNil:reqParams.userGuid      forKey:@"sUserGUID"];
    [request.postParameters setObjectOrNil:reqParams.couponCode    forKey:@"sCouponCode"];
    [request.postParameters setObjectOrNil:reqParams.countryCode   forKey:@"sCountryCd2"];
    [request.postParameters setObjectOrNil:reqParams.languageCode   forKey:@"sLanguageCode3"];
    [request.postParameters setObjectOrNil:reqParams.deviceName    forKey:@"sDeviceName"];
    [request.postParameters setObjectOrNil:@(reqParams.onlyLowest) forKey:@"bOnlyLowest"];
    
    return request;
}

+(TVPAPIRequest *) requestForIsPermittedSubscription: (void(^)(RPIsPermittedSubscription *requestParams))requestParams {
    RPIsPermittedSubscription *reqParams = [RPIsPermittedSubscription new];
    requestParams(reqParams);
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_IsPermittedSubscription] delegate:nil];
    [request.postParameters setObjectOrNil:@(reqParams.subId)  forKey:@"subId"];
    
    return request;
}

+(TVPAPIRequest *) requestForCancelSubscription: (void(^)(RPCancelSubscription *requestParams))requestParams {
    RPCancelSubscription *reqParams = [RPCancelSubscription new];
    requestParams(reqParams);
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_CancelSubscription] delegate:nil];
    [request.postParameters setObjectOrNil:@(reqParams.subId)  forKey:@"sSubscriptionID"];
    [request.postParameters setObjectOrNil:@(reqParams.purchaseId)  forKey:@"sSubscriptionPurchaseID"];

    return request;
}

+(TVPAPIRequest *) requestForGetDomainPermittedSubscriptions {
    return [self requestWithURL:[self URLForMethodName:methodName_GetDomainPermittedSubscriptions] delegate:nil];
}


+(TVPAPIRequest *) requestForGetDomainServicesWithDomainID :(NSString *) domainID
                                                  delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainServices] delegate:nil];
    [request.postParameters setObjectOrNil:domainID forKey:@"domainID"];
    
    return request;
    

}




    
+(TVPAPIRequest *) requestForGetEPGLicensedDataWithFileID:(NSInteger)  fileID
                                                EPGItemID:(NSInteger)  itemID
                                                startTime:(NSDate *)   startTime
                                                basicLink:(NSString*)  basicLink
                                                   userIP:(NSString *) userIP
                                                 refferer:(NSString *) refferer
                                               countryCd2:(NSString *) countryCd2
                                            languageCode3:(NSString *) languageCode3
                                               formatType:(EPGLicensedLinkFormatType) format
                                                 delegate:(id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetEPGLicensedData] delegate:delegate];
    
    NSString *dateString = [startTime stringWithDateFormat:@"yyyy-MM-dd HH:mm:ss.sss" andGMT:-2] ;
    
    [request.postParameters setObjectOrNil:dateString forKey:@"startTime"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:fileID] forKey:@"mediaFileID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:itemID] forKey:@"EPGItemID"];
    [request.postParameters setObjectOrNil:basicLink forKey:@"basicLink"];
    [request.postParameters setObjectOrNil:userIP forKey:@"userIP"];
    [request.postParameters setObjectOrNil:refferer forKey:@"refferer"];
    [request.postParameters setObjectOrNil:countryCd2 forKey:@"countryCd2"];
    [request.postParameters setObjectOrNil:languageCode3 forKey:@"languageCode3"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:format] forKey:@"formatType"];
    
    return request;
}



+(TVPAPIRequest *) requestForCancelServiceNowWithDomainID:(NSInteger) domainID
                                                serviceID:(NSInteger) serviceID
                                              serviceType:(TVServiceType) serviceType
                                              forceCancel:(BOOL) forceCancel
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameCancelServiceNow] delegate:nil];
    [request.postParameters setObject:[NSNumber numberWithInteger:domainID] forKey:@"domainId"];
    [request.postParameters setObject:[NSNumber numberWithInteger:serviceID]  forKey:@"serviceID"];
    [request.postParameters setObject:TVNameForServiceType(serviceType) forKey:@"serviceType"];
    [request.postParameters setObject:[NSNumber numberWithBool:forceCancel] forKey:@"forceCancel"];
    
    return request;

}


+(TVPAPIRequest *) requestForCancelSubscriptionRenewalWithDomainID:(NSInteger) domainID
                                                         serviceID:(NSInteger) serviceID
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameCancelSubscriptionRenewal] delegate:nil];
    [request.postParameters setObject:[NSNumber numberWithInteger:domainID] forKey:@"domainId"];
    [request.postParameters setObject:[NSNumber numberWithInteger:serviceID]  forKey:@"serviceID"];
    
    return request;
    
}


@end
