//
//  TVPPricingAPI.m
//  TvinciSDK
//
//  Created by Alex Zchut on 2/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVPPricingAPI.h"

@implementation TVPPricingAPI

#define methodName_GetSubscriptionsContainingMediaFile           @"GetSubscriptionsContainingMediaFile"
#define methodName_GetSubscriptionIDsContainingMediaFile         @"GetSubscriptionIDsContainingMediaFile"
#define methodName_GetSubscriptionData                           @"GetSubscriptionData"
#define methodName_GetPPVModuleData                              @"GetPPVModuleData"

+(TVPAPIRequest *) requestForGetSubscriptionsContainingMediaFile: (void(^)(RPGetSubscriptionsContainingMediaFile *requestParams))requestParams {
    
    RPGetSubscriptionsContainingMediaFile *reqParams = [RPGetSubscriptionsContainingMediaFile new];
    requestParams(reqParams);
    
    NSURL *URL = [self URLForMethodName:methodName_GetSubscriptionsContainingMediaFile];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:nil];
    [request.postParameters setObjectOrNil:@(reqParams.mediaId) forKey:@"iMediaID"];
    [request.postParameters setObjectOrNil:@(reqParams.fileId)  forKey:@"iFileID"];

    return request;
}

+(TVPAPIRequest *) requestforGetSubscriptionIDsContainingMediaFile: (void(^)(RPGetSubscriptionIDsContainingMediaFile *requestParams))requestParams {

    RPGetSubscriptionIDsContainingMediaFile *reqParams = [RPGetSubscriptionIDsContainingMediaFile new];
    requestParams(reqParams);
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_GetSubscriptionIDsContainingMediaFile] delegate:nil];
    [request.postParameters setObjectOrNil:@(reqParams.mediaId) forKey:@"iMediaID"];
    [request.postParameters setObjectOrNil:@(reqParams.fileId)  forKey:@"iFileID"];
    
    return request;
}

+(TVPAPIRequest *) requestforGetSubscriptionData: (void(^)(RPGetSubscriptionData *requestParams))requestParams {
    RPGetSubscriptionData *reqParams = [RPGetSubscriptionData new];
    requestParams(reqParams);
    
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_GetSubscriptionData] delegate:nil];
    [request.postParameters setObjectOrNil:reqParams.arrSubscriptionIds forKey:@"subIDs"];
    
    return request;
}

+(TVPAPIRequest *) requestForGetPPVModuleData : (NSString *) PPVModule
                                     delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:methodName_GetPPVModuleData] delegate:delegate];
    [request.postParameters setObjectOrNil:PPVModule forKey:@"ppvCode"];
    
    return request;
    
    
}
@end
