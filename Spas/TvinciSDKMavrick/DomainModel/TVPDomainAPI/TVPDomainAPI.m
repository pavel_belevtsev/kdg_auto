//
//  TVPDomainAPI.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/29/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVPDomainAPI.h"
#import "TVSessionManager.h"
#import "TVPAPIRequest.h"


#define DeviceNameKey @"sDeviceName"
#define DeviceBrandIDKey @"iDeviceBrandID"
#define IsMasterKey @"bMaster"

#define kAddDomainDomainNameKey @"domainName"
#define kAddDomainDomainDescriptionKey @"domainDesc"
#define kAddDomainMmasterGuidKey @"masterGuid"

#define kDomainDescriptionKey @"sDomainDescription"
#define kDomainNameKey @"sDomainName"


@implementation TVPDomainAPI



+(TVPAPIRequest *) requestForAddDeviceToDomainWithDeviceName : (NSString *) deviceName
                                               deviceBrandID : (NSInteger) deviceBrandID
                                                    delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddDeviceToDomain] delegate:delegate];
    if (deviceName != nil)
    {
        [request.postParameters setObjectOrNil:deviceName forKey:DeviceNameKey];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:deviceBrandID] forKey:DeviceBrandIDKey];
    }
    else {
        [NSException raise:NSInvalidArgumentException format:@"You must provide a device name in order to add the device to domain"];
    }
    return request;
}


+(TVPAPIRequest *) requestForRemoveDeviceFromDomainWithDeviceName : (NSString *) deviceName
                                                    deviceBrandID : (NSInteger) deviceBrandID
                                                          domainID: (NSInteger ) domainID
                                                         delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRemoveDeviceFromDomain] delegate:delegate];
    
    if (deviceName != nil)
    {
        [request.postParameters setObjectOrNil:deviceName forKey:DeviceNameKey];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:deviceBrandID] forKey:DeviceBrandIDKey];
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainID] forKey:@"domainID"];
        
    }
    else {
        [NSException raise:NSInvalidArgumentException format:@"You must provide a device name in order to remove the device from domain"];
    }
    return request;
    
}

+(TVPAPIRequest *) requestForAddUserToDomainWithDomainId : (NSInteger) domainID
                                                userGuid : (NSString *) userGuid
                                           masteUserGuid : (NSString *) masterUserGuid
                                                delegate : (id) delegate
{
    //  Simulate a request from a different user (in this case, the master user).

    TVInitObject* customInitObj = [TVInitObject getInitObjFrom:[[TVSessionManager sharedTVSessionManager] sharedInitObject]];
    customInitObj.siteGUID = masterUserGuid;
    customInitObj.domainID = domainID;
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddUserToDomain] delegate:delegate];
    request.customInitObject = customInitObj;
    [request.postParameters setValue:userGuid forKey:@"AddedUserGuid"];
    
    return request;
}

+(TVPAPIRequest *) requestForAddUserToDomainIsMaster : (BOOL) isMaster
                                             domainId:(NSInteger) domainID
                                             userGuid:(NSString *) userGuid
                                            delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddUserToDomain] delegate:delegate];
    
    [[[TVSessionManager sharedTVSessionManager] sharedInitObject] setDomainID:domainID];
    [[[TVSessionManager sharedTVSessionManager] sharedInitObject] setSiteGUID:userGuid];
    
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isMaster] forKey:IsMasterKey];
    [request.postParameters setObjectOrNil:userGuid forKey:@"AddedUserGuid"];
    return request;
}

+(TVPAPIRequest *) requestForRemoveUserFromDomainIsMaster : (BOOL) isMaster
                                                  domainId:(NSInteger) domainID
                                                  userGuid:(NSString *) userGuid
                                                 delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRemoveUserFromDomain] delegate:delegate];
    
    [[[TVSessionManager sharedTVSessionManager] sharedInitObject] setDomainID:domainID];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isMaster] forKey:IsMasterKey];
    [request.postParameters setObjectOrNil:userGuid forKey:@"userGuidToRemove"];
    return request;
}

+(TVPAPIRequest *) requestForGetDeviceDomains : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDeviceDomains] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestForGetDomainInfo : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainInfo] delegate:delegate];
    return request;
}

+(TVPAPIRequest *) requestForAddDomainWithDomainName:(NSString *)domainName
                                   domainDescription:(NSString *)domainDescription
                                          masterGuid:(int)masterGuid
                                            delegate:(id)delegate __attribute__((deprecated))

{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddDomain] delegate:delegate];
    
    if (domainName != nil && domainDescription != nil)
    {
        [request.postParameters setObjectOrNil:domainName forKey:kAddDomainDomainNameKey];
        [request.postParameters setObjectOrNil:domainDescription forKey:kAddDomainDomainDescriptionKey];
        
        if(masterGuid == -1){
//            [[request postParameters] setObject:nil forKey:@"initObj"];

            masterGuid = [[[[request postParameters] objectForKey:@"initObj"] objectForKey:@"SiteGuid"] intValue];
        }
        
        [request.postParameters setObjectOrNil:[NSNumber numberWithInt:masterGuid] forKey:kAddDomainMmasterGuidKey];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"Domain Name, Domain Description and Master GUID are required for addDomain request"];
    }
    return request;
    
    
    
    
}

+(TVPAPIRequest *) requestForSetDomainInfoWithDomainName:(NSString *)domainName
                                       domainDescription:(NSString *)domainDescription
                                                delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetDomainInfo] delegate:delegate];
    if (domainName != nil && domainDescription != nil)
    {
        [request.postParameters setObjectOrNil:domainName forKey:kDomainNameKey];
        [request.postParameters setObjectOrNil:domainDescription forKey:kDomainDescriptionKey];
    }
    else
    {
        [NSException raise:NSInvalidArgumentException
                    format:@"Both Domain Name and Domain Description are required for SetDomainInfo in request"];
    }
    return request;
}

#pragma Houshold API methods
//
+(TVPAPIRequest *) requestForRemoveDomainWithDomainID:(NSInteger)domainID
                                             delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRemoveDomain] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainID] forKey:@"domainID"];

    return request;
}

//
+(TVPAPIRequest *) requestForSendPasswordMailWithUsername:(NSString *)username
                                                 delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSendPasswordMail] delegate:delegate];
    [request.postParameters setObjectOrNil:username forKey:@"userName"];
    
    return request;
}

//
+(TVPAPIRequest *) requestForGetDomainByCoGuid:(NSString *)coGuid
                                      delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainByCoGuid] delegate:delegate];
    [request.postParameters setObjectOrNil:coGuid forKey:@"coGuid"];
    
    return request;
}

+(TVPAPIRequest *) requestForAddDomainWithCoGuid:(NSString *)coGuid
                                      domainName:(NSString *)domainName
                                      domainDesc:(NSString *)domainDesc
                                      masterGuid:(NSString *)masterGuid
                                      delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddDomainWithCoGuid] delegate:delegate];
    [request.postParameters setObjectOrNil:coGuid forKey:@"coGuid"];
    [request.postParameters setObjectOrNil:domainName forKey:@"domainName"];
    [request.postParameters setObjectOrNil:domainDesc forKey:@"domainDesc"];
    [request.postParameters setObjectOrNil:masterGuid forKey:@"masterGuid"];
    
    return request;
}

//
+(TVPAPIRequest *) requestForGetDomainIDByCoGuid:(NSString *)coGuid
                                        delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainIDByCoGuid] delegate:delegate];
    [request.postParameters setObjectOrNil:coGuid forKey:@"coGuid"];
    
    return request;
}

//
+(TVPAPIRequest *) requestForSubmitAddUserToDomainWithMasterUsername:(NSString *)masterUsername
                                                            delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSubmitAddUserToDomainRequest] delegate:delegate];
    [request.postParameters setObjectOrNil:masterUsername forKey:@"masterUsername"];
    
    return request;
}

//
+(TVPAPIRequest *) requestForActivateAccountByDomainMaster:(NSString *)masterUsername
                                                  username:(NSString *)username
                                                     token:(NSString *)token
                                                  delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameActivateAccountByDomainMaster] delegate:delegate];
    [request.postParameters setObjectOrNil:masterUsername forKey:@"masterUserName"];
    [request.postParameters setObjectOrNil:username forKey:@"userName"];
    [request.postParameters setObjectOrNil:token forKey:@"token"];
    
    return request;
}

+(TVPAPIRequest *) requestForSubmitAddDeviceToDomainRequestWithDeviceName:(NSString *)deviceName
                                                            deviceBrandId:(NSInteger)deviceBrandId
                                                                 delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSubmitAddDeviceToDomainRequest] delegate:delegate];
    [request.postParameters setObjectOrNil:deviceName forKey:@"deviceName"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:deviceBrandId] forKey:@"brandId"];
    return request;
}

+(TVPAPIRequest *) requestForMethodNameGetDeviceInfoWithDeviceUDID:(NSString *)deviceUdid
                                                            isUdid:(BOOL)isUdid
                                                                 delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDeviceInfo] delegate:delegate];
    [request.postParameters setObjectOrNil:deviceUdid forKey:@"sId"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isUdid] forKey:@"bIsUDID"];
    return request;
}


+(TVPAPIRequest *) requestForAddHomeNetworkToDomainWithNetworkId:(NSString *)networkId
                                                            networkName:(NSString *)networkName
                                                            networkDescription:(NSString *)networkDesc
                                                          delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddHomeNetworkToDomain] delegate:delegate];
    [request.postParameters setObjectOrNil:networkId forKey:@"networkId"];
    [request.postParameters setObjectOrNil:networkName forKey:@"networkName"];
    [request.postParameters setObjectOrNil:networkDesc forKey:@"networkDesc"];
    return request;
}

+(TVPAPIRequest *) requestForGetDomainHomeNetworksWithdelegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainHomeNetworks] delegate:delegate];
       return request;
}

+(TVPAPIRequest *) requestForRemoveDomainHomeNetworkWithNetworkId:(NSString *)networkId
                                            delegate:(id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRemoveDomainHomeNetwork] delegate:delegate];
    [request.postParameters setObjectOrNil:networkId forKey:@"networkId"];
    return request;
}

+(TVPAPIRequest *) requestForUpdateDomainHomeNetworkWithNetworkId :(NSString *)networkId
                                         networkName : (NSString *)networkName
                                         networkDescription : (NSString *)networkDesc
                                            isActive : (BOOL) isActive
                                            delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameUpdateDomainHomeNetwork] delegate:delegate];
    [request.postParameters setObjectOrNil:networkId forKey:@"networkId"];
    [request.postParameters setObjectOrNil:networkName forKey:@"networkName"];
    [request.postParameters setObjectOrNil:networkDesc forKey:@"networkDesc"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:isActive] forKey:@"isActive"];
    return request;
}

+(TVPAPIRequest *) requestForConfirmDeviceByDomainMasterWithUdid :(NSString *)udid
                                         masterUn : (NSString *)masterUn
                                         token : (NSString *)token
                                            delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameConfirmDeviceByDomainMaster] delegate:delegate];
    [request.postParameters setObjectOrNil:udid forKey:@"udid"];
    [request.postParameters setObjectOrNil:masterUn forKey:@"masterUn"];
    [request.postParameters setObjectOrNil:token forKey:@"token"];
    return request;
}

+(TVPAPIRequest *) requestForChangeDomainMasterWithCurrentMasterID  : (NSInteger)currentMasterID
                                                        newMasterID : (NSInteger)newMasterID
                                                        delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameChangeDomainMaster] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:currentMasterID] forKey:@"currentMasterID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:newMasterID] forKey:@"newMasterID"];
    return request;
}

+(TVPAPIRequest *) requestForResetDomainFrequencyWithFrequencyType  : (FrequencyType)frequencyType
                                                           delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameResetDomainFrequency] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:frequencyType] forKey:@"frequencyType"];
    return request;
}

+(TVPAPIRequest *) requestForSubmitAddDeviceToDomainRequestWithSpecificSiteGuid : (NSString *) masterUserGuid
                                                            andSpecificDomainID : (NSInteger)domainID
                                                                 WithDeviceName : (NSString *)deviceName
                                                                  deviceBrandId : (NSInteger)deviceBrandId
                                                                       delegate : (id)delegate
{
    TVInitObject* customInitObj = [TVInitObject getInitObjFrom:[[TVSessionManager sharedTVSessionManager] sharedInitObject]];
    customInitObj.siteGUID = masterUserGuid;
    customInitObj.domainID = domainID;
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSubmitAddDeviceToDomainRequest] delegate:delegate];
    request.customInitObject = customInitObj;
    [request.postParameters setObjectOrNil:deviceName forKey:@"deviceName"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:deviceBrandId] forKey:@"brandId"];
    return request;
}


+(TVPAPIRequest *) requestForGetDomainLastPosition :(NSString *) mediaID
                                          delegate : (id) delegate
{
    NSURL *URL = [self URLForMethodName:MethodNameGetDomainLastPosition];
    TVPAPIRequest *request = [self requestWithURL:URL delegate:delegate];
    [request.postParameters setObjectOrNil:mediaID forKey:@"mediaID"];
    return request;
}

+(TVPAPIRequest *) requestForGetDomainLimitationModuleWithDomainLimitationID :(NSInteger ) limitationID
                                                                    delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDomainLimitationModule] delegate:delegate];
    [request.postParameters setObjectOrNil:@(limitationID)  forKey:@"limitationID"];
    
    return request;

}


+(TVPAPIRequest *) requestForSuspendDomain:(NSInteger) domainID
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSuspendDomain] delegate:nil];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainID]  forKey:@"domainId"];
    return request;
    
}

+(TVPAPIRequest *) requestForResumeDomain:(NSInteger) domainID
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameResumeDomain] delegate:nil];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:domainID]  forKey:@"domainId"];
    return request;
}

+(TVPAPIRequest *) requestForGetRegions:(NSArray *) regionIds
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetRegions] delegate:nil];
    [request.postParameters setObjectOrNil:regionIds forKey:@"region_ids"];
    return request;
    
}


+(TVPAPIRequest *) requestForSetDomainRegionWithDomainID:(NSInteger ) domainID
                                        externalRegionID:(NSString *) regionID
                                               lookupKey:(NSString *) lookupKey
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetDomainRegion] delegate:nil];
    [request.postParameters setObject:[NSNumber numberWithInteger:domainID] forKey:@"domain_id"];
    [request.postParameters setObject:regionID forKey:@"ext_region_id"];
    [request.postParameters setObject:lookupKey forKey:@"lookup_key"];
    return request;
    
}


//"initObj": {...},
//"domain_id": 111891,
//"ext_region_id": "region",
//"lookup_key": ""


NSString * const MethodNameSetDomainRegion           = @"GetRegions";
NSString * const MethodNameGetRegions           = @"GetRegions";
NSString * const MethodNameSuspendDomain        = @"SuspendDomain";
NSString * const MethodNameResumeDomain        = @"ResumeDomain";
NSString * const MethodNameGetDomainLimitationModule        = @"GetDomainLimitationModule";
NSString * const MethodNameGetDomainLastPosition        = @"GetDomainLastPosition";
NSString * const MethodNameSetDomainInfo                = @"SetDomainInfo"; // TODO
NSString * const MethodNameAddDomain                    = @"AddDomain";
NSString * const MethodNameRemoveDeviceFromDomain       = @"RemoveDeviceFromDomain";
NSString * const MethodNameRemoveUserFromDomain         = @"RemoveUserFromDomain";
NSString * const MethodNameAddDeviceToDomain            = @"AddDeviceToDomain";
NSString * const MethodNameAddUserToDomain              = @"AddUserToDomain";
NSString * const MethodNameGetDeviceDomains             = @"GetDeviceDomains";
NSString * const MethodNameGetDomainInfo                = @"GetDomainInfo";

NSString * const MethodNameRemoveDomain                 = @"RemoveDomain";
NSString * const MethodNameSendPasswordMail             = @"SendPasswordMail";
NSString * const MethodNameGetDomainByCoGuid            = @"GetDomainByCoGuid";
NSString * const MethodNameAddDomainWithCoGuid          = @"AddDomainWithCoGuid";
NSString * const MethodNameGetDomainIDByCoGuid          = @"GetDomainIDByCoGuid";
NSString * const MethodNameSubmitAddUserToDomainRequest     = @"SubmitAddUserToDomainRequest";
NSString * const MethodNameActivateAccountByDomainMaster    = @"ActivateAccountByDomainMaster";
NSString * const MethodNameSubmitAddDeviceToDomainRequest   = @"SubmitAddDeviceToDomainRequest";
NSString * const MethodNameResetDomainFrequency             = @"ResetDomainFrequency";
NSString * const MethodNameGetDeviceInfo                    = @"GetDeviceInfo";
NSString * const MethodNameAddHomeNetworkToDomain           = @"AddHomeNetworkToDomain";
NSString * const MethodNameGetDomainHomeNetworks            = @"GetDomainHomeNetworks";
NSString * const MethodNameRemoveDomainHomeNetwork          = @"RemoveDomainHomeNetwork";
NSString * const MethodNameUpdateDomainHomeNetwork          = @"UpdateDomainHomeNetwork";
NSString * const MethodNameConfirmDeviceByDomainMaster      = @"ConfirmDeviceByDomainMaster";
NSString * const MethodNameChangeDomainMaster               = @"ChangeDomainMaster";



@end
