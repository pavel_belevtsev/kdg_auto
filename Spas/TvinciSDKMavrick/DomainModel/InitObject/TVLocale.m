//
//  TVLocale.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/14/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVLocale.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "NSString+NSNullConsideration.h"

NSString *const TVLocaleLanguageKey = @"LocaleLanguage";
NSString *const TVLocaleCountryKey = @"LocaleCountry";
NSString *const TVLocaleDeviceKey = @"LocaleDevice";
NSString *const TVLocaleUserStateKey = @"LocaleUserState";

NSString *const TVLocaleUserStateUnknownString = @"Unknown";
NSString *const TVLocaleUserStateAnonymousString = @"Anonymous";
NSString *const TVLocaleUserStateNewString = @"New";
NSString *const TVLocaleUserStateSubString = @"Sub";
NSString *const TVLocaleUserStateExSubString = @"ExSub";
NSString *const TVLocaleUserStatePPVString = @"PPV";
NSString *const TVLocaleUserStateExPPVString = @"ExPPV";

@implementation TVLocale
@synthesize localeCountry = _localeCountry;
@synthesize localeDevice = _localeDevice;
@synthesize localeUserState = _localeUserState;
@synthesize localeLanguage = _localeLanguage;

#pragma mark - Memory
-(void) dealloc
{
    self.localeLanguage = nil;
    self.localeCountry = nil;
    self.localeDevice = nil;

}

#pragma mark - Initializations
-(id) init
{
    if (self = [super init])
    {
        self.localeDevice = @"";
        self.localeCountry = @"";
        self.localeLanguage = @"";
        self.localeUserState = TVLocaleUserStateUnknown;
    }
    return self;
}

-(id) initWithArray:(NSArray *)array
{
    if (self = [self init])
    {
        [self setAttributesFromArray:array];
    }
    return self;
}
-(void) setAttributesFromArray:(NSArray *)array
{
    for (NSDictionary *param in  array)
    {
        if ([param objectOrNilForKey:TVLocaleCountryKey]) {
            self.localeCountry = [[param objectOrNilForKey:TVLocaleCountryKey] nullOrString];
        }
        if ([param objectOrNilForKey:TVLocaleDeviceKey]) {
            self.localeDevice = [[param objectOrNilForKey:TVLocaleDeviceKey] nullOrString];
        }
        if ([param objectOrNilForKey:TVLocaleLanguageKey]) {
            self.localeLanguage = [[param objectOrNilForKey:TVLocaleLanguageKey] nullOrString];
        }
        if ([param objectOrNilForKey:TVLocaleUserStateKey]) {
            self.localeUserStateString = [[param objectOrNilForKey:TVLocaleUserStateKey] nullOrString];
        }
    }
    
}


-(NSDictionary *) keyValueRepresentation
{
    NSMutableDictionary *toReturn = [NSMutableDictionary dictionary];
    [toReturn setObjectOrNil:self.localeLanguage forKey:TVLocaleLanguageKey];
    
    [toReturn setObjectOrNil:self.localeDevice forKey:TVLocaleDeviceKey];
    
    [toReturn setObjectOrNil:self.localeCountry forKey:TVLocaleCountryKey];
    
    NSString *userState = [self localeUserStateString];
    [toReturn setObjectOrNil:userState forKey:TVLocaleUserStateKey];
    return [NSDictionary dictionaryWithDictionary:toReturn];
}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.localeCountry = [dictionary objectOrNilForKey:TVLocaleCountryKey];
    self.localeDevice = [dictionary objectOrNilForKey:TVLocaleDeviceKey];
    self.localeLanguage = [dictionary objectOrNilForKey:TVLocaleLanguageKey];
    self.localeUserStateString = [dictionary objectOrNilForKey:TVLocaleUserStateKey];
}

#pragma mark - Utility
-(NSString *)localeUserStateString
{
    NSString *toReturn = [[self class] userStateString:self.localeUserState];
    return toReturn;
}

+(NSString *) userStateString:(TVLocaleUserState)userState
{
    switch (userState) 
	{
		case TVLocaleUserStateUnknown:
			return TVLocaleUserStateUnknownString;
		case TVLocaleUserStateAnonymous:
			return TVLocaleUserStateAnonymousString;
		case TVLocaleUserStateNew:
			return TVLocaleUserStateNewString;
		case TVLocaleUserStateSub:
			return TVLocaleUserStateSubString;
		case TVLocaleUserStateExSub:
			return TVLocaleUserStateExSubString;
		case TVLocaleUserStatePPV:
			return TVLocaleUserStatePPVString;
		case TVLocaleUserStateExPPV:
			return TVLocaleUserStateExPPVString;
		default:
			return nil;
	}
}

-(void) setLocaleUserStateString:(NSString *)localeUserStateString
{
    self.localeUserState = TVLocaleUserStateUnknown;
    if ([localeUserStateString isEqualToString:TVLocaleUserStateNewString])
    {
        self.localeUserState = TVLocaleUserStateNew;
    }
    if ([localeUserStateString isEqualToString:TVLocaleUserStateAnonymousString])
    {
        self.localeUserState = TVLocaleUserStateAnonymous;
    }
    if ([localeUserStateString isEqualToString:TVLocaleUserStateExPPVString])
    {
        self.localeUserState = TVLocaleUserStateExPPV;
    }
    if ([localeUserStateString isEqualToString:TVLocaleUserStateExSubString])
    {
        self.localeUserState = TVLocaleUserStateExSub;
    }
    if ([localeUserStateString isEqualToString:TVLocaleUserStatePPVString])
    {
        self.localeUserState = TVLocaleUserStatePPV;
    }
    if ([localeUserStateString isEqualToString:TVLocaleUserStateSubString])
    {
        self.localeUserState = TVLocaleUserStateSub;
    }
}

#pragma mark - Class Methods
+(TVLocale *) localeWithDictionary : (NSDictionary *) dictionary
{
    TVLocale *locale = [[TVLocale alloc] initWithDictionary:dictionary];
    return locale ;
}

+(TVLocale *) localeWithArray :(NSArray*) array
{
    TVLocale *locale = [[TVLocale alloc] initWithArray:array];
    return locale ;
}
@end
