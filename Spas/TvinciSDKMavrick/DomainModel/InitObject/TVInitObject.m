//
//  TVInitObject.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVInitObject.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVLocale.h"
#import "OpenUDID.h"


NSString *const TVConfigLocaleKey = @"Locale";
NSString *const TVConfigSiteGUIDKey = @"SiteGuid";
NSString *const TVConfigDomainIDKey = @"DomainID";
NSString *const TVConfigAPIUsernameKey = @"ApiUser";
NSString *const TVConfigAPIPasswordKey = @"ApiPass";
NSString *const TVConfigPlatformKey = @"Platform";
NSString *const TVConfigUDIDKey = @"UDID";
NSString *const TVConfigUDIDGetKey = @"udid";
NSString *const TVInitObjTokenKey = @"Token";

NSString *const TVPlatformiPad = @"iPad";
NSString *const TVPlatformiPhone = @"iPhone";
NSString *const TVPLatformUnknown = @"Unknown";
NSString *const TVPLatformCellular = @"Cellular";


// #define TV_Open_UDID [OpenUDID value]

@interface TVInitObject ()
@end

@implementation TVInitObject
@synthesize locale = _locale;
@synthesize siteGUID = _siteGUID;
@synthesize platform = _platform;
@synthesize APIUsername = _APIUsername;
@synthesize APIPassword = _APIPassword;
@synthesize domainID = _domainID;

#pragma mark - Memory

-(void) dealloc
{
    self.udid = nil;
    self.siteGUID = nil;
    self.APIUsername = nil;
    self.APIPassword = nil;
    self.locale = nil;
    self.token = nil;

}

#pragma mark - Initializations

-(id) init
{
    if (self = [super init])
    {
        self.APIPassword = @"";
        self.APIUsername = @"";
        self.siteGUID = @"";
        self.domainID = 0;
        self.platform = TVPLatformCellular;
        self.locale = [[TVLocale alloc] init];
    }
    return self;
}

-(id) initWithArray:(NSArray *)array
{
    if (self = [self init])
    {
        [self setAttributesFromArray:array];
    }
    return self;
}


-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.APIPassword = [dictionary objectOrNilForKey:TVConfigAPIPasswordKey];
    self.APIUsername = [dictionary objectOrNilForKey:TVConfigAPIUsernameKey];
    

    self.platform = [dictionary objectOrNilForKey:TVConfigPlatformKey];
    NSDictionary *locale = [dictionary objectOrNilForKey:TVConfigLocaleKey];
    self.locale = [TVLocale localeWithDictionary:locale];
    self.udid = [dictionary objectForKey:TVConfigUDIDGetKey];
}

-(void) setAttributesFromArray:(NSArray *)array
{
    for (NSDictionary *param in  array)
    {
        if ([param objectOrNilForKey:TVConfigAPIUsernameKey]) {
            self.APIUsername = [param objectOrNilForKey:TVConfigAPIUsernameKey];
        }
        if ([param objectOrNilForKey:TVConfigAPIPasswordKey]) {
            self.APIPassword = [param objectOrNilForKey:TVConfigAPIPasswordKey];
        }
        if ([param objectOrNilForKey:TVConfigPlatformKey]) {
            self.platform = [param objectOrNilForKey:TVConfigPlatformKey];
        }
        if ([param objectOrNilForKey:TVConfigLocaleKey]) {
            self.locale = [TVLocale localeWithArray:[param objectOrNilForKey:TVConfigLocaleKey]];
        }
        
        if ([param objectOrNilForKey:TVConfigUDIDGetKey]) {
            self.udid = [param objectOrNilForKey:TVConfigUDIDGetKey];
        }
        
        if ([param objectOrNilForKey:TVInitObjTokenKey]) {
            self.token = [param objectOrNilForKey:TVInitObjTokenKey];
        }
        
    }
    
}

#pragma mark - Getters

-(BOOL) belongsToValidDomain
{
    @synchronized (self)
    {
        return (self.siteGUID.length > 0 && self.domainID != 0 && ![self.siteGUID isEqualToString:@"0"]) ;
    }
}


-(NSDictionary *) JSONObject
{
    NSMutableDictionary *toReturn = [NSMutableDictionary dictionary];
    NSDictionary *locale = [self.locale keyValueRepresentation];
    [toReturn setObjectOrNil:locale forKey:TVConfigLocaleKey];
    [toReturn setObjectOrNil:self.platform forKey:TVConfigPlatformKey];
    [toReturn setObjectOrNil:self.siteGUID forKey:TVConfigSiteGUIDKey];
    NSNumber *domainID = [NSNumber numberWithInteger:self.domainID];
    [toReturn setObjectOrNil:domainID forKey:TVConfigDomainIDKey];
    [toReturn setObjectOrNil:self.APIUsername forKey:TVConfigAPIUsernameKey];
    [toReturn setObjectOrNil:self.APIPassword forKey:TVConfigAPIPasswordKey];
    [toReturn setObjectOrNil:self.udid forKey:TVConfigUDIDKey];
    [toReturn setObjectOrNil:self.token forKey:TVInitObjTokenKey];
    return [NSDictionary dictionaryWithDictionary:toReturn];
}

+(TVInitObject *) getInitObjFrom:(TVInitObject *)originalInitObj {
    NSArray* array = [NSArray arrayWithObjects:
                      [NSDictionary dictionaryWithObject:originalInitObj.APIPassword forKey:TVConfigAPIPasswordKey],
                      [NSDictionary dictionaryWithObject:originalInitObj.APIUsername forKey:TVConfigAPIUsernameKey],
                      [NSDictionary dictionaryWithObject:originalInitObj.platform forKey:TVConfigPlatformKey],
                      [NSDictionary dictionaryWithObject:originalInitObj.udid forKey:TVConfigUDIDGetKey],
                       originalInitObj.token?[NSDictionary dictionaryWithObject:originalInitObj.token forKey:TVInitObjTokenKey]:nil,
                      nil];
    
    NSArray* localArr = [NSArray arrayWithObjects:
                         [NSDictionary dictionaryWithObject:originalInitObj.locale.localeCountry forKey:TVLocaleCountryKey],
                         [NSDictionary dictionaryWithObject:originalInitObj.locale.localeDevice forKey:TVLocaleDeviceKey],
                         [NSDictionary dictionaryWithObject:originalInitObj.locale.localeLanguage forKey:TVLocaleLanguageKey],
                         [NSDictionary dictionaryWithObject:originalInitObj.locale.localeUserStateString forKey:TVLocaleUserStateKey]
                         , nil];
    [NSDictionary dictionaryWithObject:localArr forKey:TVConfigLocaleKey];
    
    
    TVInitObject* customInitObj = [[TVInitObject alloc] initWithArray:array] ;
    return customInitObj;
}

@end
