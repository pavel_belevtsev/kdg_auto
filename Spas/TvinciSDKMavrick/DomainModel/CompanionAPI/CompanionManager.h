//
//  CompanionManager.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/17/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "TVCompanionDevice.h"

@protocol CompanionModeProtocol <NSObject>

- (void)companionSearchFinishedWithDevices:(NSArray *)devices;
- (void)companionSearchFinishedWithError:(NSError *)error;

@end

@interface CompanionManager : BaseModelObject {
    
}

+ (CompanionManager *)sharedManager;
- (void)searchForDevices;
- (void)downloadXMLDataForDevices:(NSArray*)devices WithCompletionBlock : (void (^)(NSArray* downloadedDevices)) completion;

/*  TVNotificationCompanionDevicePaired notification will be sent when Pair is done  */
- (BOOL)pairDevice:(TVCompanionDevice *)device;

/*  TVNotificationCompanionDeviceUnPaired notification will be sent when Un Pair is done  */
- (BOOL)unPairCurrentDevice;


@property (nonatomic, strong) NSArray* devicesItems;
@property (nonatomic, assign) BOOL isDevicePaired;
@property (nonatomic, strong) TVCompanionDevice* devicePaired;
@property (nonatomic, weak) id<CompanionModeProtocol> delegate;

@end
