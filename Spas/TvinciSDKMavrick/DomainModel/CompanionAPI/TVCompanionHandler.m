//
//  TVCompanionHandler.m
//  TvinciSDK
//
//  Created by Tarek Issa on 4/9/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVCompanionHandler.h"
#import "TVCompanionDevice.h"
#import "TVCompanionAPI.h"
#import <TVDeviceFamily.h>
#import <TVCompairAdapter.h>
#import "ASIHTTPRequest.h"
@implementation TVCompanionHandler


+ (NSArray *)crossDevicesArray:(NSArray *)devicesArray withUDIDsArray:(NSArray *)deviceFamilyArray {
    
    NSMutableArray *crossingResultArr   = [NSMutableArray array];
    
    for (TVCompanionDevice *device in devicesArray) {
        for (TVDeviceFamily* deviceFamily in deviceFamilyArray) {
            for (TVDevice* knownDevice in deviceFamily.devices) {
                TVLogDebug(@"knownDevice.UDID: %@    device.udid_serverFormat: %@", knownDevice.UDID, device.udid);

                if ([TVCompairAdapter UDIDManipulationFor:knownDevice.UDID and:device.udid]) {
                    [device changeFriendlyNameTo:knownDevice.deviceName];
                    [crossingResultArr addObject:device];
                    break;
                }
            }
        }
    }
    return crossingResultArr;
}

+ (void)sendMediaToSTB : (TVCompanionDevice *)stbDevice
               mediaID : (NSString *)mediaID
     withMediaMarkRate : (NSInteger)seconds
              siteGuid : (NSString *)siteGuid
         playback_type : (NSString *)playback_type
          epgProgramId : (NSString *)epgProgramId
              itemType : (NSString *)itemType
                fileId : (NSString *)fileId
              domainID : (NSInteger)domainId
          doRequestPin : (BOOL)requestPin
   withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion
{
    //currentmedia
   TVPAPIRequest * request  = [TVCompanionAPI requestForSendPlayToSTBWithBaseUrl:stbDevice.basUrlString
                                                   playback_type:playback_type
                                                    epgProgramId:epgProgramId
                                                        siteGuid:siteGuid
                                                        itemType:itemType
                                                         mediaId:mediaID
                                                          fileId:fileId
                                                        domainId:domainId
                                                       mediaMark:seconds
                                                    doRequestPin:NO
                                                        delegate:nil];
    __weak TVPAPIRequest * weakRequest = request;
    
    
    [request setFailedBlock:^{
        completion(TVCompanionSwoosh_Fail);
    }];
    
    [request setCompletionBlock:^{
        TVLogDebug(@"________________________\n%@", weakRequest);
        TVLogDebug(@"________________________\n%@", [weakRequest debugPostBodyString]);
        NSDictionary * dict = [weakRequest JSONResponse];
        NSString* statusStr = [dict objectOrNilForKey:@"status"];
        TVCompanionSwooshResult  result = TVCompanionSwoosh_Fail;
        
        
        if ([[statusStr lowercaseString] isEqualToString:@"unknown_format"]) {
            result = TVCompanionSwoosh_UknownFormat;
        }else if ([[statusStr lowercaseString] isEqualToString:@"success"]) {
            result = TVCompanionSwoosh_Success;
        }else if ([[statusStr lowercaseString] isEqualToString:@"fail"]) {
            result = TVCompanionSwoosh_Fail;
        }
        TVLogDebug(@"request For Send Play To STB = %@",weakRequest);
        completion(result);
    }];
    
    [request startAsynchronous];
}

+ (void)sendMediaToSTB : (TVCompanionDevice *)stbDevice
               mediaID : (NSString *)mediaID
     withMediaMarkRate : (NSInteger)seconds
         playback_type : (NSString *)playback_type
          epgProgramId : (NSString *)epgProgramId
              itemType : (NSString *)itemType
                fileId : (NSString *)fileId
             play_time : (NSInteger)play_time
          doRequestPin : (BOOL)requestPin
   withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion {
    
//    self
//    
//    
//    [self sendMediaToSTB:stbDevice mediaID:mediaID withMediaMarkRate:seconds siteGuid:[TVSessionManager sharedTVSessionManager].currentUser.siteGUID playback_type:playback_type epgProgramId:epgProgramId itemType:itemType fileId:fileId domainID:[TVSessionManager sharedTVSessionManager].currentDomain.domainID play_time:play_time doRequestPin:requestPin withCompletionBlock:completion];
    
}

+ (void)sendMediaToNetgemSTB : (TVCompanionDevice *)stbDevice
               mediaID : (NSString *)mediaID
                mediaUrl : (NSString *)mediaUrl
     withMediaMarkRate : (NSInteger)seconds
              siteGuid : (NSString *)siteGuid
         playback_type : (NSString *)playback_type
          epgProgramId : (NSString *)epgProgramId
              itemType : (NSString *)itemType
                fileId : (NSString *)fileId
              domainID : (NSInteger)domainId
          doRequestPin : (BOOL)requestPin
   withCompletionBlock : (void (^)(TVCompanionSwooshResult SwooshResult)) completion
{
    
    NSURL * urlToSend = nil;
    if (seconds > 0)
    {
        urlToSend = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Live/External/play?url=%@&position=%ld", stbDevice.ipAddress,mediaUrl,(long)seconds]];
    }
    else
    {
        urlToSend = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Live/External/play?url=%@", stbDevice.ipAddress,mediaUrl]];
    }
    
    __weak  ASIHTTPRequest *requestA = [ASIHTTPRequest requestWithURL:urlToSend];
    [requestA addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [requestA setRequestMethod:@"GET"];
        
    [requestA setFailedBlock:^{
        TVLogDebug(@"%@",[requestA error]);
        completion(TVCompanionSwoosh_Fail);
    }];
    
    [requestA setCompletionBlock:^{
        TVLogDebug(@"request For Send Play To STB url= %@",requestA.url);
        TVLogDebug(@"STB responseString = %@", requestA.responseString);
        TVCompanionSwooshResult  result = TVCompanionSwoosh_Success;
        completion(result);
    }];
    
    [requestA startAsynchronous];
}


@end
