//
//  TVPNotificationAPI.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/8/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVPNotificationAPI.h"

@implementation TVPNotificationAPI





+(TVPAPIRequest *) requestforGetDeviceNotificationsType: (TVNotificationType) notificationType
                                             viewStatus: (TVNotificationViewStatus) notificationViewStatus
                                     notificationsCount:(NSInteger) count
                                               delegate: (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetDeviceNotifications] delegate:nil];
    [request.postParameters setObjectOrNil: TVNameForNotificationType(notificationType) forKey:@"notificationType"];
    [request.postParameters setObjectOrNil: TVNameForNotificationViewStatus(notificationViewStatus) forKey:@"viewStatus"];
    [request.postParameters setObjectOrNil: [NSNumber numberWithInteger:count]  forKey:@"messageCount"];
    
    return request;

}


+(TVPAPIRequest *) requestForGetUserStatusSubscriptionWithDelegate: (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserStatusSubscriptions] delegate:nil];
    return request;
}

+(TVPAPIRequest *) requestforSetNotificationMessageViewStatus : (TVNotificationViewStatus) notificationViewStatus
                                        notificationRequestID : (NSString *) notificationRequestID
                                        notificationMessageID : (NSString *) notificationMessageID
                                                      delegate: (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSetNotificationMessageViewStatus] delegate:nil];
    [request.postParameters setObjectOrNil: notificationRequestID forKey:@"notificationRequestID"];
    [request.postParameters setObjectOrNil: notificationMessageID forKey:@"notificationMessageID"];
    [request.postParameters setObjectOrNil: TVNameForNotificationViewStatus(notificationViewStatus) forKey:@"viewStatus"];
     return request;
    
}

+(TVPAPIRequest *) requestforSubscribeByTag: (NSArray *) tags
                                  delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameSubscribeByTag] delegate:nil];
    [request.postParameters setObjectOrNil: tags forKey:@"tags"];
    return request;
}

+(TVPAPIRequest *) requestForUnsubscribeFollowUpByTag : (NSArray *) tags
                                             delegate : (id)delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameUnsubscribeFollowUpByTag] delegate:nil];
    [request.postParameters setObjectOrNil: tags forKey:@"tags"];
    return request;
}







NSString * const MethodNameGetDeviceNotifications  = @"GetDeviceNotifications";
NSString * const MethodNameGetUserStatusSubscriptions = @"GetUserStatusSubscriptions";
NSString * const MethodNameSetNotificationMessageViewStatus = @"SetNotificationMessageViewStatus" ;
NSString * const MethodNameSubscribeByTag = @"SubscribeByTag";
NSString * const MethodNameUnsubscribeFollowUpByTag = @"UnsubscribeFollowUpByTag";

@end
