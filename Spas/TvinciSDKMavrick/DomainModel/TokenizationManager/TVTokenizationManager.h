//
//  TVTokenizationManager.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/29/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVBaseNetworkObject.h"

@class TVTokenizationManager;
@class TVToken;


typedef enum
{
    TokenStatus_Unknown, // This is the start point of the tokenization status, after login process, if login will return tokens or not we will know if the application supported tokens or not.
    TokenStatus_NotSupported, // When login request didn't has token at the response header, we can decide that the back end didn't sent it cause is is not supported.
    TokenStatus_Valid, // When we are getting new tokens at login or after refresh process the token as marked as valid.
    TokenStatus_OnRefresh, // When Tokenization manager is deciding to refresh the token we are in refresh status . while this process is active and other process like api calles will wait untill refresh will done and will continue with the new token.
    TokenStatus_AccessExpired, // This status define a situation when access expired because of 401 response of API or refresh failed because of network issues or other resons except refresh token expired ( when 401 is returned from refresh token API ) when status access expired "refresh token if needed" methos will trigger refresh action regardless the expiration time that may be still valid.
    TokenStatus_RefreshExpired, // There is no option to refresh
}TokenStatus;

@protocol TVTokenizationManagerDelegate <NSObject>

-(void) tokenizationManagerAuthenticationExpired:(TVTokenizationManager *)sender; // When Refresh token expired and we can't use the access or refresh it , only user login will help us :) so, this method is called and causes a login process.

@end


@interface TVTokenizationManager : TVBaseNetworkObject

@property (readonly, strong, nonatomic) TVToken * accessToken;

@property (assign, nonatomic) TokenStatus tokenStatus;

@property (nonatomic, weak) id<TVTokenizationManagerDelegate> delegate;



/**
 *  Refreshes the access token by the refresh token id the access token is expired
 *  When the access token is expired, we can get a new one by the refesh method when we are still in our authorized refresh period.
 *  @param startBlock           Called in the beginig of the process
 *  @param failedBlock          Called when failed to refresh token,
                                so you will need to reauthnticate your self by the initial method sign-in.
 *  @param completionBlock      Called when the user is not signed in,
                                so there is not need to refresh token.
                                Token is still valid
                                Succeded to refresh token
                                In the time between reauthentication request sent and the authentication succeded event didn't accure.
 
 */
-(void) refreshTokenIfNeededConsiderSafetyMargin:(BOOL) considerSaftyMargin  owner:(id) owner startBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock;



/**
 *  Updates the refresh token and the expiration token after successful authntication.
 *
 *  @param header           The response header. 
                            ( pay attention to the different between the request header and the response header )
 */
-(void) authenticationSuccededWithResponseHeader:(NSDictionary *) header;

/**
 *  mark token as expired from external reason
 */
-(void) markTokenAsExpired;

/**
 *  Delete all token from the cache and the keychain
 */
-(void) clearTokens;









@end
