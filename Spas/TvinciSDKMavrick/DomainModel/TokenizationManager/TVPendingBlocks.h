//
//  TVPendingTokenizationBlocks.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 8/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVPendingBlocks : NSObject

@property (weak, nonatomic) id owner;
@property (nonatomic, copy) void (^successBlock)();
@property (nonatomic, copy) void (^failedBlock)();
@end
