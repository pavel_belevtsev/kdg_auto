//
//  TVTokenizationManager.m
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/29/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVTokenizationManager.h"
#import "TVToken.h"
#import "TvinciKeychainWrapper.h"
#import "TVConfigurationManager.h"
#import "FTHTTPCodes.h"
#import "TVPendingBlocks.h"


#define requestHeaderAccessTokenKey @"access_token"
#define requestHeaderRefreshTokenKey @"refresh_token"

#define keychainAccessTokenKey @"AccessToken"
#define keychainAccessTokenExpirationKey @"AccessTokenExpiration"

#define keychainRefreshTokenKey @"RefreshToken"
#define keychainRefreshTokenExpirationKey @"RefreshTokenExpiration"


#define keychainTokenStatusKey @"TokenStatus"

@interface TVTokenizationManager ()


@property (readwrite, strong, nonatomic) TVToken * accessToken;
@property (readwrite, strong, nonatomic) TVToken * refreshToken;



@property (strong, atomic) NSMutableArray * arrayFinishBlocks;
@end

@implementation TVTokenizationManager


-(instancetype)init
{
    if (self = [super init])
    {
        self.accessToken = [self loadTokenWithTokenKey:keychainAccessTokenKey expirationKey:keychainAccessTokenExpirationKey];
        self.refreshToken = [self loadTokenWithTokenKey:keychainRefreshTokenKey expirationKey:keychainRefreshTokenExpirationKey];
        self.tokenStatus = [self loadTokenStatus];
        self.arrayFinishBlocks = [NSMutableArray array];

    }

    return self;
}

-(void) authenticationSuccededWithResponseHeader:(NSDictionary *) header
{
    NSString * accessTokenString = [header objectForKey:requestHeaderAccessTokenKey];
    NSString * refreshTokenString = [header objectForKey:requestHeaderRefreshTokenKey];

    self.tokenStatus = accessTokenString && refreshTokenString?TokenStatus_Valid:TokenStatus_NotSupported;

    if (TokenStatus_Valid)
    {
        TVToken * accessToken = [[TVToken alloc] init];
        [accessToken setAttributesFromString:accessTokenString];
        self.accessToken = accessToken;

        TVToken * refreshToken = [[TVToken alloc] init];
        [refreshToken setAttributesFromString:refreshTokenString];
        self.refreshToken = refreshToken;

        NSDate * now = [NSDate date];
        TVLogDebug(@"authenticationSucceded : now = %@, accessExpiration = %@ refreshExpiration = %@ access returned:%@",now,self.accessToken.expiration,self.refreshToken.expiration,self.accessToken.token);
    }

}

-(void)setAccessToken:(TVToken *)accessToken
{
    if (_accessToken != accessToken)
    {
        _accessToken = accessToken;
        [self storeToken:self.accessToken withTokenKey:keychainAccessTokenKey expirationKey:keychainAccessTokenExpirationKey];
    }
}

-(void) setRefreshToken:(TVToken *)refreshToken
{
    if (_refreshToken != refreshToken)
    {
        _refreshToken = refreshToken;
        [self storeToken:self.refreshToken withTokenKey:keychainRefreshTokenKey expirationKey:keychainRefreshTokenExpirationKey];
    }
}

-(void)setTokenStatus:(TokenStatus)tokenStatus
{
    if (_tokenStatus != tokenStatus)
    {
        if (tokenStatus == TokenStatus_RefreshExpired)
        {
            [self sendAuthnticationExpiredEvent];
        }

        _tokenStatus = tokenStatus;
        [self storeTokenStatus:_tokenStatus];
    }
}


-(void) storeToken:(TVToken *) token withTokenKey:(NSString *) tokenKey expirationKey:(NSString *) expirationKey
{
    if (token.token)
    {
        [TvinciKeychainWrapper setKeychainValue:token.token forIdentifier:tokenKey sync:NO];

        if (token.expiration)
        {
            [TvinciKeychainWrapper setKeychainValue:[NSString stringWithFormat:@"%f",token.expiration.timeIntervalSince1970] forIdentifier:expirationKey sync:NO];
        }
    }
    else
    {
        [TvinciKeychainWrapper deleteKeychainValue:tokenKey sync:NO];
        [TvinciKeychainWrapper deleteKeychainValue:expirationKey sync:NO];
    }

}


-(TVToken *) loadTokenWithTokenKey:(NSString *) tokenKey expirationKey:(NSString *) expirationKey
{
    NSString * tokenString = [TvinciKeychainWrapper searchKeychainCopyMatching:tokenKey sync:NO];
    NSString * tokenExpirationString = [TvinciKeychainWrapper searchKeychainCopyMatching:expirationKey sync:NO];

    TVToken * token = nil;
    if (tokenString && tokenExpirationString)
    {
        token = [TVToken tokenWithToken:tokenString expiration:tokenExpirationString.integerValue];
    }
    return  token;
}


-(void) storeTokenStatus:(TokenStatus) tokenStatus
{
     [TvinciKeychainWrapper setKeychainValue:[NSString stringWithFormat:@"%d",tokenStatus] forIdentifier:keychainTokenStatusKey sync:NO];
}

-(TokenStatus) loadTokenStatus
{
    NSString * tokenStatusString = [TvinciKeychainWrapper searchKeychainCopyMatching:keychainTokenStatusKey sync:NO];
    TokenStatus tokenStatus = (TokenStatus)tokenStatusString.integerValue;
    tokenStatus = tokenStatus == TokenStatus_OnRefresh?TokenStatus_AccessExpired:tokenStatus;
    return tokenStatus;
}

-(void) clearTokens
{
    self.accessToken = nil;
    self.refreshToken = nil;
    self.tokenStatus = TokenStatus_Unknown;

}


-(void) refreshTokenIfNeededConsiderSafetyMargin:(BOOL) considerSaftyMargin  owner:(id) owner startBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock
{
    if (startBlock)
    {
        startBlock();
    }

    if (![[TVSessionManager sharedTVSessionManager] isSignedIn] || self.tokenStatus == TokenStatus_NotSupported || [[TVSDKSettings sharedInstance] offlineMode] || self.tokenStatus == TokenStatus_Unknown)
    {
        if (completionBlock)
        {
            completionBlock();
        }
        return;
    }


    if (self.tokenStatus == TokenStatus_RefreshExpired)
    {
        if (failedBlock)
        {
            failedBlock();
        }
        return;
    }


    BOOL refreshTokenIsValid = [self isRefreshTokenValidConsiderSafetyMargin:considerSaftyMargin];
    BOOL accessTokenIsValid = [self isAccessTokenValid];

    if (accessTokenIsValid && refreshTokenIsValid && self.tokenStatus != TokenStatus_AccessExpired)
    {
        if (completionBlock)
        {
            completionBlock();
        }
        return;
    }


    if (self.tokenStatus == TokenStatus_OnRefresh)
    {

        if (failedBlock || completionBlock)
        {
            TVPendingBlocks * pendingBlocks = [[TVPendingBlocks alloc] init];
            pendingBlocks.failedBlock = failedBlock;
            pendingBlocks.successBlock = completionBlock;
            pendingBlocks.owner = owner;
            [self.arrayFinishBlocks addObject:pendingBlocks];
        }

        return;
    }

    self.tokenStatus = TokenStatus_OnRefresh;
    TVPAPIRequest * request = [TVPSiteAPI requestForRefreshAccessTokenWithRefreshToken:self.refreshToken.token delegate:nil];
    __weak TVPAPIRequest * weakRequest = request;

    [request setStartedBlock:^{

    }];

    [request setFailedBlock:^{

#warning HTTPCode200OK added for correct work during video playing
        if ((request.responseStatusCode == HTTPCode401Unauthorised) || (request.responseStatusCode == HTTPCode200OK))
        {
            self.tokenStatus = TokenStatus_RefreshExpired;
            [self dispatchAllFailedBlocksCurrentFailedBlock:failedBlock];
        }
        else
        {
            self.tokenStatus = TokenStatus_AccessExpired;
            
#warning Completed replaced to Failed for correct work on app start
//            [self dispatchAllCompletionBlocksCurrentCompletionBlock:completionBlock];
            [self dispatchAllFailedBlocksCurrentFailedBlock:failedBlock];

        }
    }];


    [request setCompletionBlock:^{

        NSDictionary * response = [weakRequest JSONResponse];
        NSString * accessTokenString = [response objectForKey:@"access_token"];
        NSString * refreshTokenString = [response objectForKey:@"refresh_token"];
        NSNumber * accessTokenExpirationEpoch = [response objectForKey:@"expiration_time"];
        NSNumber * refreshTokenExpirationEpoch = [response objectForKey:@"refresh_expiration_time"];

        TVToken * accessToken = nil;
        if (accessTokenString && accessTokenExpirationEpoch)
        {
            accessToken = [TVToken tokenWithToken:accessTokenString expiration:accessTokenExpirationEpoch.integerValue];
        }

        TVToken * refreshToken = nil;

        if (refreshTokenString && refreshTokenExpirationEpoch)
        {
            refreshToken =  [TVToken tokenWithToken:refreshTokenString expiration:refreshTokenExpirationEpoch.integerValue];
        }

        self.accessToken = accessToken;
        self.refreshToken = refreshToken;

        TVLogDebug(@"RefreshAccessToken access expiration:%@ , refresh expiration:%@ access returned:%@ ",self.accessToken.expiration,self.refreshToken.expiration,self.accessToken.token);

        BOOL refreshTokenValid = [self isRefreshTokenValidConsiderSafetyMargin:considerSaftyMargin];
        if (refreshTokenValid == NO)
        {
            self.tokenStatus = TokenStatus_RefreshExpired;
            [self dispatchAllFailedBlocksCurrentFailedBlock:failedBlock];
        }
        else
        {
            self.tokenStatus = TokenStatus_Valid;
            [self dispatchAllCompletionBlocksCurrentCompletionBlock:completionBlock];
        }
    }];
    
    [self sendRequest:request];

}

-(void) markTokenAsExpired
{
    NSDate * now = [NSDate date];
    TVLogDebug(@"markTokenAsExpired : now = %@, access expiration = %@ ",now,self.accessToken.expiration);

    if(self.tokenStatus != TokenStatus_RefreshExpired && self.tokenStatus != TokenStatus_OnRefresh && [[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if (self.tokenStatus == TokenStatus_Unknown)
        {
            self.tokenStatus = TokenStatus_RefreshExpired;
        }
        else
        {
            self.tokenStatus = TokenStatus_AccessExpired;
        }

    }
}


 -(void)setDelegate:(id<TVTokenizationManagerDelegate>)delegate
{
    if (_delegate != delegate)
    {
        _delegate = delegate;
    }
}

-(void) sendAuthnticationExpiredEvent
{
    // sending the event only on first time
    if ([self.delegate respondsToSelector:@selector(tokenizationManagerAuthenticationExpired:)] && self.tokenStatus != TokenStatus_RefreshExpired)
    {
        [self.delegate tokenizationManagerAuthenticationExpired:self];
    }
}

-(void) dispatchAllCompletionBlocksCurrentCompletionBlock:(void(^)()) completionBlock
{
    if ( completionBlock)
    {
        completionBlock();
    }

    for (TVPendingBlocks * finishBlocks  in self.arrayFinishBlocks)
    {
        if (finishBlocks.owner && finishBlocks.successBlock)
        {
            finishBlocks.successBlock();
        }
        else
        {
            NSLog(@"Owner is not exist any more!! %@",finishBlocks.owner);
        }
    }

    [self.arrayFinishBlocks removeAllObjects];


}

-(void) dispatchAllFailedBlocksCurrentFailedBlock:(void(^)()) failedBlock
{
    if (failedBlock)
    {
        failedBlock();
    }

    for (TVPendingBlocks * finishBlocks in self.arrayFinishBlocks)
    {
        if (finishBlocks.owner && finishBlocks.failedBlock)
        {
            finishBlocks.failedBlock();
        }
        else
        {
            NSLog(@"Owner is not exist any more!! %@",finishBlocks.owner);
        }

    }

    [self.arrayFinishBlocks removeAllObjects];
}

-(BOOL) isRefreshTokenValidConsiderSafetyMargin:(BOOL) considerSafetyMargin
{
    NSTimeInterval refreshTokenSafetyMargin = [[TVConfigurationManager sharedTVConfigurationManager] refreshTokenSafetyMarginInSeconds];
    NSDate * now = [NSDate date];

    NSTimeInterval refreshExpirationTimeIntervalSinceReferenceDate = self.refreshToken.expiration.timeIntervalSinceReferenceDate - (considerSafetyMargin?refreshTokenSafetyMargin:0);

    TVLogDebug(@"isRefreshTokenValid :now = %@, refresh expiration = %@ ",now,self.refreshToken.expiration);
    return self.refreshToken && (refreshExpirationTimeIntervalSinceReferenceDate > now.timeIntervalSinceReferenceDate);
}


-(BOOL) isAccessTokenValid
{
    NSDate * now = [NSDate date];
    TVLogDebug(@"isAccessTokenValid :now = %@, access expiration expiration = %@ ",now,self.accessToken.expiration);

    return self.accessToken && (self.accessToken.expiration.timeIntervalSinceReferenceDate  > now.timeIntervalSinceReferenceDate);

}
@end
