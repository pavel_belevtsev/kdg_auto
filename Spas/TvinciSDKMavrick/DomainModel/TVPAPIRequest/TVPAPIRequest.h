//
//  TvpAPIRequest.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//
//  This Class is used for network operation it creted as proxi that included Third party request inside.
//
//  Litle History: this request was designed first as ASIHTTPRequest ( inheritance ) After ASIHTTPRequest stopped to give support for new version of IOS
//  and we found some more advanteges in other third party code we change this class to the new pattern as proxi , now it will be more natural to change
//  this class even to other network manament vendors.
//
//
//

#import "TVInitObject.h"
#import "AFHTTPRequestOperation.h"

@class TVNetworkQueue;
@class TVPAPIRequest;

/** This delegates created for observing the start and finish events of the request.
 * TVNetworkQueue is using this for saving the active requests of this qeueu*/
@protocol TVPAPIRequestDelegate <NSObject>
-(void) requestDidStart:(TVPAPIRequest *) request;
-(void) requestDidFinish:(TVPAPIRequest *) request;
@end




typedef enum RequestMethod
{
    RequestMethod_GET,
    RequestMethod_HEAD,
    RequestMethod_POST,
    RequestMethod_PUT,
    RequestMethod_PATCH,
    RequestMethod_DELETE,
}RequestMethod;

typedef void (^TVBasicBlock)(void);

@interface TVPAPIRequest : NSObject


#pragma mark - request delegate
/**  This is the observer for request did start and finish events*/
@property (weak, nonatomic) id<TVPAPIRequestDelegate> requestDelegate;

#pragma mark - request basic params
/**   request url*/
@property (strong, nonatomic) NSURL * url;
/**  The request post body parameters , this will be converted to NSData and will be applied to the postBody*/
@property (nonatomic , strong, readonly) NSMutableDictionary *postParameters;
/** Set this value to change the request method type default is POST*/
@property (assign, nonatomic) RequestMethod requestMethod;
/**  time out seconds*/
@property (assign) NSTimeInterval timeOutSeconds;
/**  The request error pay attention that this is native network error , use this file to identify errors types: NSURLError.h*/
@property (readonly, nonatomic, strong) NSError *error;
/**  HTTP Response status Code*/
@property (assign, nonatomic) NSInteger responseStatusCode;
/**  The response JSON object ( Array , Dictionary , NSString , NSNumber , NSData etec')*/
@property (nonatomic , strong, readonly) id JSONResponse;
/**  Response as string*/
@property (readonly, nonatomic, strong) NSString * responseString;
/**  Response as string*/
@property (readonly, nonatomic, strong) NSData * responseData;


// To handle:
@property (nonatomic, strong) NSDictionary *responseHeaders;
@property (nonatomic, assign) BOOL inProgress;


#pragma mark - request callbacks
/**  block to execute when request started*/
@property(copy, nonatomic) TVBasicBlock startedBlock;
/**  block to execute when request completes successfully*/
@property(copy, nonatomic) TVBasicBlock completionBlock;
/**  block to execute when request failed use error param and responseStatusCode to Identify the error*/
@property(copy, nonatomic) TVBasicBlock failedBlock;



#pragma mark - custom properties
/**  In case you want to send request to the outer world (*&#$8) and you dont need to include init object of TVinci protocol mark this as true.*/
@property (nonatomic, assign) BOOL excludeInitObj;
/**  Some Times you want to assigne custom init object to the request , this not recommended at all but is you need it , use it wisely*/
@property (nonatomic, strong) TVInitObject *customInitObject;
/**  use it to attch context as tag to identify spesific request*/
@property (nonatomic, strong) id context;
/** should request need to ignore token status */
@property (nonatomic, assign) BOOL ignoreToken;



#pragma mark - Main API:

/**  request with url
 *
 *  @param url
 *
 *  @return TVPAPIRequest autoreleased object*/
+(TVPAPIRequest *) requestWithURL:(NSURL *) url;

/**  request with url
 *
 *  @param url
 *
 *  @return TVPAPIRequest retained object*/
- (id) initWithURL:(NSURL *) url;

/**  Adding request header
 *
 *  @param header heady key
 *  @param value  header value
 *
 *  default header :@"Content-type":@"application/json;charset=utf-8"*/
- (void)addRequestHeader:(NSString *)header value:(NSString *)value;

/**  Cancel the request , this method removing the request from operation queue and canceling its progress.*/
- (void) cancel;

/**  Dispatch request*/
- (void) sendRequestSync:(BOOL) sync;

/**  start sending request without request queue */
- (void) startAsynchronous;

- (void)startSynchronous;

/**  Fixing corrupted response because of backend bug.
 *
 *  @return clean and fixed response*/
-(id) bakedJSONResponse;
-(id) JSONResponseJSONOrString;

/**  post body for debug usege
 *
 *  @return post body as string*/
-(NSString *) debugPostBudyString __attribute__((deprecated));
-(NSString *) debugPostBodyString;



+(void) addFailedStatusCodes:(NSInteger) statusCode;
+(void) removeFailedStatusCodes:(NSInteger) statusCode;



///------------
/// deprecation
///------------

/**  This prameter was used for ASIHttpRequest nad now there is no need for it*/
@property (assign, nonatomic) id delegate  __attribute__((deprecated));







@end

