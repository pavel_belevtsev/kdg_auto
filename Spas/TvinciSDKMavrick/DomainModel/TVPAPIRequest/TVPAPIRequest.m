//
//  TvpAPIRequest.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVPAPIRequest.h"
#import "TVInitObject.h"
#import "TVSessionManager.h"
#import "TVConfigurationManager.h"
#import "BaseTVPClient.h"
#import "TVTokenizationManager.h"
#import "TVResponseStatus.h"
#import "TVSuspensionManager.h"
#import "TVSDKSettings.h"
#import "FTHTTPCodes.h"
#import "TVNetworkQueue.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFNetworkActivityIndicatorManager.h"


NSString *const TVPAPIRerquestErrorMessageKey = @"Error";
NSString *const TVPServerError = @"TVPServerError";

#define defaultTimeoutInterval 30


@interface TVPAPIRequest ()

@property (nonatomic, strong) AFHTTPRequestOperation * coreRequest; // The main request witch TVPAPIRequest is proxing this request
@property (nonatomic , strong, readwrite) NSMutableDictionary *postParameters; // the request post params
@property (nonatomic , strong, readwrite) id JSONResponse; // JSON response
@property (strong, nonatomic) NSMutableDictionary * dictionaryHeader;
@property (readwrite, nonatomic, strong) NSError *error;
@property (strong, nonatomic) AFHTTPRequestOperationManager* operationManager;// operation manager to excute this request
@property (assign, nonatomic) BOOL sync;

@end

@implementation TVPAPIRequest

static NSMutableArray * additionalFailedStatusCodes;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (void)initialize {
    if (self == [TVPAPIRequest class]) {
        // do whatever
        [TVPAPIRequest addFailedStatusCodes:HTTPCode403Forbidden];
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];

    }
}


#pragma mark - Init

+(TVPAPIRequest *) requestWithURL:(NSURL *) url
{
    TVPAPIRequest * request = [[TVPAPIRequest alloc] initWithURL:url];
    return request;

}

-(id) initWithURL:(NSURL *)newURL
{
    if (self = [super init])
    {
        self.url = newURL;
        self.requestMethod = RequestMethod_POST;
        [self addRequestHeader:@"Content-type" value:@"application/json;charset=utf-8"];
        self.operationManager = [[AFHTTPRequestOperationManager alloc] init];
    }
    return self;
}


#pragma mark - Getters

-(NSDictionary *)responseHeaders
{
    if (_responseHeaders == nil)
    {
        _responseHeaders = [[self.coreRequest response] allHeaderFields];
    }

    return _responseHeaders;
}

-(NSMutableDictionary *) postParameters
{
    if (_postParameters == nil)
    {
        _postParameters = [[NSMutableDictionary alloc] init];
    }
    return _postParameters;
}

-(NSMutableDictionary *) dictionaryHeader
{
    if (_dictionaryHeader == nil)
    {
        _dictionaryHeader = [[NSMutableDictionary alloc] init];
    }
    return _dictionaryHeader;
}

-(NSError *)error
{
   return [self.coreRequest error];
}

-(NSString *)responseString
{
    return [self.coreRequest responseString];
}


-(NSData *) responseData
{
    return [self.coreRequest responseData];
}

-(id) JSONResponse
{
    if (_JSONResponse == nil)
    {
        [self parseResponseToJSONFormat];
    }
    return _JSONResponse;
}


#pragma mark- Setters


#pragma mark - Main API


-(void) startSynchronous
{
    [self startRequestSync:YES];
}

-(void) startAsynchronous
{
    [self startRequestSync:NO];
}

-(void) startRequestSync:(BOOL) sync
{
    if([[TVSDKSettings sharedInstance] offlineMode] ==YES ) return;

    if (self.ignoreToken)
    {
        [self sendRequestSync:sync];
    }
    else
    {
        // since we are going to use the token in the build post body method, we are prepating the token and if it is expired we will get a new one after this process
        [[[TVSessionManager sharedTVSessionManager] tokenizationManager] refreshTokenIfNeededConsiderSafetyMargin:NO  owner:self startBlock:nil failedBlock:^{
            [self sendRequestSync:sync];
        } completionBlock:^{
            [self sendRequestSync:sync];
        }];
    }
}



-(void) cancel
{
    [self.coreRequest cancel];
    [self.operationManager.operationQueue cancelAllOperations];
}

- (void)addRequestHeader:(NSString *)header value:(NSString *)value
{
    [self.dictionaryHeader setObject:value forKey:header];
}

-(void) parseResponseToJSONFormatOrString
{
    [self JSONResponse];
}

-(id) JSONResponseJSONOrString{

    [self parseResponseToJSONFormatOrString];
    return _JSONResponse;
}

-(id) bakedJSONResponse
{
    if (_JSONResponse == nil)
    {
        [self bakeAndParseResponseToJSONFormat];
    }
    return _JSONResponse;
}


-(NSString *) description{

    return [NSString stringWithFormat:@"\n\n---------------\nRequest url:\n %@\n\
            Request PostBody:\n %@\n \
            PostParams:\n %@\n\
            Response:\n%@\n\n---------------\n"
            ,self.url,[self debugPostBodyString],self.postParameters,[self JSONResponse]];

}

-(NSString *) debugPostBodyString
{
    NSString * body = [[NSString alloc] initWithData:[[self.coreRequest request] HTTPBody] encoding:NSUTF8StringEncoding];
    return body;
}


#pragma mark - parsing request
-(void) parseResponseToJSONFormat
{

    NSError *e = nil;
    id parsed = nil;

    NSString *responseString = [self.coreRequest responseString];
    NSData * data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    if (data != nil)
    {
        parsed = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &e];}
    
    if (!parsed) {
        NSString *responseString = [self responseString];

        if (responseString.length > 0)
        {
            //Strip extra " sign in case of response that contains only JSON string like "hello"
            parsed = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            //Check for boolean value.
            if ([parsed isEqualToString:@"true"])
            {
                parsed = [NSNumber numberWithBool:YES];
            }
            else if ([parsed isEqualToString:@"false"])
            {
                parsed = [NSNumber numberWithBool:NO];
            }
            else
            {
                // Check for number
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                NSNumber *number = [formatter numberFromString:parsed];
                parsed = (number != nil) ? number : parsed;
            }
        }
    }
    
    self.JSONResponse = parsed;
    
}

-(void) bakeAndParseResponseToJSONFormat
{
    NSString *responseString = [self.coreRequest responseString];
    // Try to parse it as a valid JSON
    NSError *e = nil;
    id parsed = nil;
    NSData * data = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    if (data)
    {
        parsed = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &e];
    }
    
    if (parsed == nil)
    {
        responseString = [responseString stringByReplacingOccurrencesOfString:@"''" withString:@"\""];
        if (responseString)
        {
            parsed = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
        }
        
        if (parsed != nil)
        {
            self.JSONResponse = parsed;
            return;
        }
        // Not a valid JSON, but not an empty string, so what is it?
        if (responseString.length > 0)
        {
            // Strip extra " sign in case of response that contains only JSON string like "hello"
            parsed = [responseString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            // Check for boolean value.
            if ([parsed isEqualToString:@"true"])
            {
                parsed = [NSNumber numberWithBool:YES];
            }
            else if ([parsed isEqualToString:@"false"])
            {
                parsed = [NSNumber numberWithBool:NO];
            }
            else
            {
                // Check for number
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                NSNumber *number = [formatter numberFromString:parsed];
                parsed = (number != nil) ? number : parsed;
            }
        }
    }
    self.JSONResponse = parsed;
}



#pragma mark - AFNetwork adaptetion

-(void) adjustOperationManager
{
//    self.operationManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
//    self.operationManager.securityPolicy.allowInvalidCertificates = YES;

    for (NSString * key in self.dictionaryHeader.allKeys)
    {
        [self.operationManager.requestSerializer setValue:[self.dictionaryHeader objectForKey:key] forHTTPHeaderField:key];
    }
    
    self.operationManager.requestSerializer = [[AFJSONRequestSerializer alloc] init]; // very important to send the body as json
    self.operationManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    self.operationManager.requestSerializer.timeoutInterval = self.timeOutSeconds>0?self.timeOutSeconds:defaultTimeoutInterval;
}

-(void) adjustInitObject
{
        if ( self.excludeInitObj == NO)
        {
            NSDictionary *initObjectDictionary = nil;
            if (self.customInitObject)
            {
                initObjectDictionary = [self.customInitObject JSONObject];
            }
            else
            {
                initObjectDictionary = [[TVSessionManager sharedTVSessionManager].sharedInitObject JSONObject];
            }
            
            [self.postParameters setObject:initObjectDictionary forKey:TVConfigInitObjectPostKey];
        }
}





-(void) prepareAndSendRequestWithStartedBlock:(TVBasicBlock) startedBlock failedBlock:(TVBasicBlock) failedBlock completionBlock:(TVBasicBlock) completionBlock
{

    [self requestDidStart];
    if (startedBlock){startedBlock();}
    [self adjustOperationManager];
    [self adjustInitObject];


    void (^requestCompletion)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        self.JSONResponse = responseObject;
        self.responseStatusCode = [operation.response statusCode];

        NSString *errorMessage = nil;
        if ([self.JSONResponse isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * responseWithoutNulls = [self.JSONResponse dictionaryByRemovingNSNulls];
            errorMessage = [responseWithoutNulls objectForKey:TVPAPIRerquestErrorMessageKey];
        }

        NSError * serverError = nil;
        if (errorMessage != nil)
        {
            NSDictionary *info = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
            serverError = [NSError errorWithDomain:TVPServerError code:0 userInfo:info];
        }
        else if ([TVPAPIRequest isStatusCodeIsMarkedAsFailed:self.responseStatusCode])
        {
            NSDictionary *info = [NSDictionary dictionaryWithObject:@"response status code error" forKey:NSLocalizedDescriptionKey];
            serverError = [NSError errorWithDomain:TVPServerError code:self.responseStatusCode userInfo:info];
        }
        else
        {
            BOOL isSuspended = [[TVSuspensionManager sharedInstance]
                                scanForSuspensionForAPIResponse:self.JSONResponse];
            if (isSuspended )
            {
                NSDictionary *info = [NSDictionary dictionaryWithObject:@"UserSuspended" forKey:NSLocalizedDescriptionKey];
                serverError = [NSError errorWithDomain:TVPServerError code:0 userInfo:info];
            }
        }

        if (serverError)
        {
            self.error = serverError;
            if (failedBlock){failedBlock();}
        }
        else if (completionBlock){completionBlock();}
        [self requestDidFinish];
    };
    
    void (^requestFailed)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
        self.error = error;
        self.responseStatusCode = [operation.response statusCode];
        if (failedBlock){failedBlock();}
        [self requestDidFinish];
    };

    


    NSArray * requestMehods = @[@"GET",@"HEAD",@"POST",@"PUT",@"PATCH",@"DELETE"];
    NSString * requestMethodName = requestMehods[self.requestMethod];


    NSMutableURLRequest *request = [self.operationManager.requestSerializer requestWithMethod:requestMethodName URLString:[self.url absoluteString] parameters:self.postParameters error:nil];
    self.coreRequest = [self.operationManager HTTPRequestOperationWithRequest:request success:requestCompletion failure:^(AFHTTPRequestOperation * operation, NSError * error)
                        {
                            if ([operation.response statusCode] == HTTPCode401Unauthorised && self.ignoreToken == NO) // in case we are getting 401 in the request , we want to resume request after successsful refresh token.
                            {
                                [[[TVSessionManager sharedTVSessionManager] tokenizationManager]  markTokenAsExpired];
                                [[[TVSessionManager sharedTVSessionManager] tokenizationManager] refreshTokenIfNeededConsiderSafetyMargin:NO owner:self startBlock:nil failedBlock:^{
                                    if (requestFailed){requestFailed(operation, error);}
                                } completionBlock:^{

                                    [self adjustInitObject];
                                    NSMutableURLRequest *request = [self.operationManager.requestSerializer requestWithMethod:operation.request.HTTPMethod URLString:[operation.request.URL absoluteString] parameters:self.postParameters error:nil];
                                    AFHTTPRequestOperation * operation = [self.operationManager HTTPRequestOperationWithRequest:request success:
                                                                          ^(AFHTTPRequestOperation * operation, id somthing) {
                                                                              requestCompletion(operation, somthing);
                                                                          } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
                                                                              requestFailed(operation, error);
                                                                          }];
                                    self.coreRequest = operation;
                                    [self dispatchRequest];


                                }];
                            }
                            else
                            {
                                requestFailed(operation, error);
                            }
                        }];


    [self dispatchRequest];
    
}

-(void) dispatchRequest
{
    if (self.sync)
    {
        [self.coreRequest start];
        [self.coreRequest waitUntilFinished];
    }
    else
    {
        [self.operationManager.operationQueue addOperation:self.coreRequest];
    }

}


-(void) networkRequestDidStart:(NSNotification *) notification
{

}


-(NSString *) debugPostBudyString
{
    return [self debugPostBodyString];
}




+(void) addFailedStatusCodes:(NSInteger) statusCode
{
    if (additionalFailedStatusCodes == nil)
    {
        additionalFailedStatusCodes = [[NSMutableArray alloc] init];
    }

    if (![additionalFailedStatusCodes containsObject:[NSNumber numberWithInteger:statusCode]])
    {
        [additionalFailedStatusCodes addObject:[NSNumber numberWithInteger:statusCode]];
    }

    
    

}

+(void) removeFailedStatusCodes:(NSInteger) statusCode
{
    [additionalFailedStatusCodes removeObject:[NSNumber numberWithInteger:statusCode]];
}

+(BOOL) isStatusCodeIsMarkedAsFailed:(NSInteger) statusCode
{
   NSInteger index =   [additionalFailedStatusCodes indexOfObjectPassingTest:^BOOL(NSNumber * obj, NSUInteger idx, BOOL *stop) {
        
        if(obj.integerValue == statusCode)
        {
            return YES;
        }
        
        return NO;
    }];

    return index != NSNotFound;
}







-(void) networkRequestDidFinish:(NSNotification *) notification
{
    }

-(void) sendRequestSync:(BOOL) sync
{
    self.sync = sync;
    [self prepareAndSendRequestWithStartedBlock:self.startedBlock failedBlock:self.failedBlock completionBlock:self.completionBlock];
}



-(void) requestDidStart
{
    self.inProgress = YES;

    if ([self.requestDelegate respondsToSelector:@selector(requestDidStart:)])
    {
        [self.requestDelegate requestDidStart:self];
    }

}

-(void) requestDidFinish
{
    self.inProgress = NO;
    if ([self.requestDelegate respondsToSelector:@selector(requestDidFinish:)])
    {
        [self.requestDelegate requestDidFinish:self];
    }
}


@end


//[[[TVSessionManager sharedTVSessionManager] tokenizationManager] refreshTokenIfNeededConsiderSafetyMargin:NO  owner:self startBlock:nil failedBlock:^{
//
//    if (requestFailed)
//    {
//        requestFailed(operation,error);
//    }
//
//} completionBlock:
// ^{
//
//
//     [self adjustInitObject];
//     [self adjustOperationManager];
//     NSMutableURLRequest *request = [self.operationManager.requestSerializer requestWithMethod:operation.request.HTTPMethod URLString:[operation.request.URL absoluteString] parameters:self.postParameters error:nil];
//     AFHTTPRequestOperation * operation = [self.operationManager HTTPRequestOperationWithRequest:request success:
//                                           ^(AFHTTPRequestOperation * operation, id somthing) {
//                                               requestCompletion(operation, somthing);
//                                           } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
//                                               requestFailed(operation, error);
//                                           }];
//
//
//     self.coreRequest = operation;
//
//
//     if (self.sync)
//     {
//         [self.coreRequest start];
//         [self.coreRequest waitUntilFinished];
//     }
//     else
//     {
//         [self.operationManager.operationQueue addOperation:self.coreRequest];
//     }
//
// }];

