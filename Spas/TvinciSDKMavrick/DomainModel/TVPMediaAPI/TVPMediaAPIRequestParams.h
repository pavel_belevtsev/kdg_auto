//
//  TVPMediaAPIRequestParams.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/12/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RPChargeUserWithInApp : NSObject

@property (nonatomic, readwrite) CGFloat price;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *receipt;
@property (nonatomic, strong) NSString *productCode;

@end

@interface RPChargeUserPPVWithInApp : NSObject

@property (nonatomic, readwrite) CGFloat price; //dPrice
@property (nonatomic, strong) NSString *currency; // sCurrencyCode3
@property (nonatomic, strong) NSString *productCode; //sPPVModuleCode
@property (nonatomic, strong) NSString *ppvModuleCode; //sPPVModuleCode
@property (nonatomic, strong) NSString *receipt; //ReceiptData

@end

@interface RPGetSubscriptionProductCode : NSObject

@property (nonatomic, readwrite) NSInteger subscriptionCode;

@end

@interface RPGetSubscriptionMedias : NSObject

@property (nonatomic, strong) NSArray *arrSubscriptionIds;
@property (nonatomic, strong) NSString *picSize;
@property (nonatomic, strong) NSString *orderBy;

@end

