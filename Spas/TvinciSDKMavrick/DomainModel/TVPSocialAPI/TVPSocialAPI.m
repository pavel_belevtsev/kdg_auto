//
//  TVPSocialAPI.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 10/3/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVPSocialAPI.h"

@implementation TVPSocialAPI


+(TVPAPIRequest *) requestForDoUserActionWithUserAction : (TVUserSocialActionType) userAction
                                              assetType : (TVAssetType) assetType
                                                assetID : (NSInteger) assetID
                                            extraParams : (NSArray *) extraParams
                                         SocialPlatform : (TVSocialPlatform) socialPlatform
                                               delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameDoUserAction] delegate:delegate];
    
    
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:assetID] forKey:@"assetID"];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    if (extraParams ==nil)
    {
        [request.postParameters setObjectOrNil:[NSNull null] forKey:@"extraParams"];
    }
    else
    {
        [request.postParameters setObjectOrNil:extraParams forKey:@"extraParams"];
    }
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatform)  forKey:@"socialPlatform"];
    return  request;
}




+(TVPAPIRequest *) requestforGetUserActionsWithUserAction : (TVUserSocialActionType) userAction
                                                assetType : (TVAssetType) assetType
                                                   assetID : (NSInteger) assetID
                                                startIndex : (NSInteger) startIndex
                                                recordsNum : (NSInteger ) recordsNum
                                            socialPlatform : (TVSocialPlatform ) socialPlatform
                                                  delegate : (id) delegate
{
    
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserActions] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:assetID] forKey:@"assetID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:startIndex] forKey:@"startIndex"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:recordsNum] forKey:@"numOfRecords"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatform) forKey:@"socialPlatform"];
    return request;
}

+(TVPAPIRequest *) requestforGetUserActivityFeedWithPageSize : (NSInteger) nPageSize
                                                   pageIndex : (NSInteger) nPageIndex
                                                picDimension : (NSString *) sPicDimension
                                                    delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserActivityFeed] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:nPageSize] forKey:@"nPageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:nPageIndex] forKey:@"nPageIndex"];
    [request.postParameters setObjectOrNil:sPicDimension forKey:@"sPicDimension"];
    return request;
}

+(TVPAPIRequest *) requestforGetUserActivityFeedWithPageSize : (NSInteger) nPageSize
                                                   pageIndex : (NSInteger) nPageIndex
                                                picDimension : (NSString *) sPicDimension
                                                    siteGuid : (NSString *) siteGuid
                                                    delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserActivityFeed] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:nPageSize] forKey:@"nPageSize"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:nPageIndex] forKey:@"nPageIndex"];
    [request.postParameters setObjectOrNil:sPicDimension forKey:@"sPicDimension"];
    [request.postParameters setObjectOrNil:siteGuid forKey:@"siteGuid"];
    return request;
}

+(TVPAPIRequest *) requestForGetUserFriendsWithDelegate : (id) delegate {

    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserFriends] delegate:delegate];
    
    return request;

}

+(TVPAPIRequest *) requestForGetUsersLikedMediaWithMediaID : (NSInteger) mediaID
                                               onlyFriends : (BOOL) onlyFriends
                                                startIndex : (NSInteger) startIndex
                                                  pageSize : (NSInteger) pageSize
                                                  delegate : (id) delegate
{
    
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUsersLikedMedia] delegate:delegate];
    
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaID] forKey:@"mediaID"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithBool:onlyFriends] forKey:@"onlyFriends"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:startIndex] forKey:@"startIndex"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:pageSize] forKey:@"pageSize"];
    
    return request;

}



+(TVPAPIRequest *) requestForGetFriendsActionsWithUserActions : (NSArray *) userActions
                                                    assetType : (TVAssetType ) assetType
                                                      assetID : ( NSInteger ) assetID
                                                   startIndex : ( NSInteger ) startIndex
                                                 numOfRecords : ( NSInteger ) numOfRecords
                                               socialPlatform : (TVSocialPlatform ) socialPlatform
                                                     delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetFriendsActions] delegate:delegate];
    [request.postParameters setObjectOrNil:userActions forKey:@"userActions"];
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    if (assetID != -1)
    {
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:assetID] forKey:@"assetID"];
    }
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:startIndex] forKey:@"startIndex"];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:numOfRecords] forKey:@"numOfRecords"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatform) forKey:@"socialPlatform"];

    return request;
}

+(TVPAPIRequest *) requestForGetCrowdsourceFeedWithPageSize : (NSString *)pageSize
                                              epochLastTime : (NSString *)epochLastTime
                                                  assetType : (TVAssetType ) assetType
                                                    assetID : ( NSInteger ) assetID
                                             socialPlatform : (TVSocialPlatform ) socialPlatform
                                                   delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetCrowdsourceFeed] delegate:delegate];
    
    [request.postParameters setObjectOrNil:pageSize forKey:@"pageSize"];
    [request.postParameters setObjectOrNil:epochLastTime forKey:@"epochLastTime"];
    [request.postParameters setObjectOrNil:TVNameForAssetType(assetType) forKey:@"assetType"];
    if (assetID != -1)
    {
        [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:assetID] forKey:@"assetID"];
    }
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatform) forKey:@"socialPlatform"];
    
    return request;
}

#pragma mark - social privacy

+(TVPAPIRequest *) requestforGetUserFBActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                  socialPlatform : (TVSocialPlatform) socialPlatfom
                                                        delegate : (id) delegate  __attribute__((deprecated))
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserFBActionPrivacy] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatfom) forKey:@"mediaID"];
    return request;

}

+(TVPAPIRequest *) requestForSetUserFBActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                  socialPlatform :( TVSocialPlatform ) socialPLatform
                                                   privacyAction :(TVUserActionPrivacy) privacyAction
                                                        delegate : (id) delegate  __attribute__((deprecated))
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserFBActionPrivacy] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:TVNameForSocialPrivacy(privacyAction) forKey:@"actionPrivacy"];
    
    return request;
}

+(TVPAPIRequest *) requestForGetUserSocialPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                socialPlatform :( TVSocialPlatform ) socialPLatform
                                                      delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserSocialPrivacy] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    
    return request;
}

+(TVPAPIRequest *) requestForSetUserSocialPrivacy:(TVUserSocialActionType) userAction 
                                                      socialPlatform :(TVSocialPlatform ) socialPLatform
                                                       actionPrivacy :(TVUserActionPrivacy)actionPrivacy
                                                            delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserSocialPrivacy] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:TVNameForSocialPrivacy(actionPrivacy) forKey:@"socialPrivacy"];

    return request;
}


+(TVPAPIRequest *) requestForSetUserExternalActionShare:(TVUserSocialActionType) userAction
                                                  socialPlatform :( TVSocialPlatform ) socialPLatform
                                                   actionPrivacy :(TVUserPrivacy)userPrivacy
                                                        delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserExternalActionShare] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:TVNameForUserPrivacy(userPrivacy) forKey:@"actionPrivacy"];
    
    return request;
}

+(TVPAPIRequest *) requestforGetUserInternalActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                        socialPlatform : (TVSocialPlatform) socialPlatfom
                                                              delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserInternalActionPrivacy] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPlatfom) forKey:@"socialPlatform"];
    return request;
}

+(TVPAPIRequest *) requestForSetUserInternalActionPrivacyWithUserAction:(TVUserSocialActionType) userAction
                                                        socialPlatform :( TVSocialPlatform ) socialPLatform
                                                         privacyAction :(TVUserPrivacy) userPrivacy
                                                              delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameSetUserInternalActionPrivacy] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    [request.postParameters setObjectOrNil:TVNameForUserPrivacy(userPrivacy) forKey:@"actionPrivacy"];
    
    return request;
}

+(TVPAPIRequest *) requestForGetUserExternalActionShare:(TVUserSocialActionType) userAction
                                                        socialPlatform :( TVSocialPlatform ) socialPLatform
                                                              delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetUserExternalActionShare] delegate:delegate];
    [request.postParameters setObjectOrNil:TVNameForUserAction(userAction) forKey:@"userAction"];
    [request.postParameters setObjectOrNil:TVNameForSocialPlatform(socialPLatform) forKey:@"socialPlatform"];
    
    return request;
}


+(TVPAPIRequest *) requestforGetSocialFeedWithMediaID : (NSInteger) mediaId
                                             delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameGetSocialFeed] delegate:delegate];
    [request.postParameters setObjectOrNil:[NSNumber numberWithInteger:mediaId] forKey:@"mediaId"];
    return request;
}

+(TVPAPIRequest *) requestforFBTokenValidation : (NSString *) token
                                             delegate : (id) delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameFBTokenValidation] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"token"];
    return request;
}


+(TVPAPIRequest *)requestforFBUserSigninWithToken:(NSString *)token delegate:(id)delegate
{
    TVPAPIRequest * request = [self requestWithURL:[self URLForMethodName:MethodNameFBUserSignin] delegate:delegate];
    [request.postParameters setObjectOrNil:token forKey:@"token"];
    return request;
    
}

NSString * const MethodNameGetFriendsActions            = @"GetFriendsActions";
NSString * const MethodNameGetCrowdsourceFeed           = @"GetCrowdsourceFeed";
NSString * const MethodNameGetUserActivityFeed          = @"GetUserActivityFeed";
NSString * const MethodNameGetUserActions               = @"GetUserActions";
NSString * const MethodNameDoUserAction                 = @"DoUserAction";
NSString * const MethodNameGetUserFriends               = @"GetUserFriends";
NSString * const MethodNameGetUsersLikedMedia           = @"GetUsersLikedMedia";

NSString * const MethodNameSetUserFBActionPrivacy       = @"SetUserFBActionPrivacy";
NSString * const MethodNameGetUserFBActionPrivacy       = @"GetUserFBActionPrivacy";

NSString * const MethodNameSetUserInternalActionPrivacy = @"SetUserInternalActionPrivacy";
NSString * const MethodNameGetUserInternalActionPrivacy = @"GetUserInternalActionPrivacy";

NSString * const MethodNameSetUserExternalActionShare   = @"SetUserExternalActionShare";
NSString * const MethodNameGetUserExternalActionShare   = @"GetUserExternalActionShare";

NSString * const MethodNameGetUserSocialPrivacy         = @"GetUserSocialPrivacy";
NSString * const MethodNameSetUserSocialPrivacy         = @"SetUserSocialPrivacy";

NSString * const MethodNameGetSocialFeed                = @"GetSocialFeed";

NSString * const MethodNameFBTokenValidation            = @"FBTokenValidation";
NSString * const MethodNameFBUserSignin                 = @"FBUserSignin";

@end
