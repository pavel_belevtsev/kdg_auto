//
//  MethodsURLPipeline.m
//  TvinciSDK
//
//  Created by Rivka Peleg on 10/2/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "MethodsURLPipeline.h"

@implementation MethodsURLPipeline

 static MethodsURLPipeline *_sharedMethodsURLPipeline = nil;

+ (instancetype)sharedMethodsURLPipeline {
    
    @synchronized(self)
    {
        if (_sharedMethodsURLPipeline == nil)
        {
            _sharedMethodsURLPipeline = [[MethodsURLPipeline alloc] init];
        }
    }
    
    return _sharedMethodsURLPipeline;
}

+ (void) becomeDefaultPipline:(MethodsURLPipeline*) defaultPipeIne
{
    @synchronized (self)
    {
        if (_sharedMethodsURLPipeline != defaultPipeIne)
        {
            _sharedMethodsURLPipeline = defaultPipeIne;
        }
    }
}


-(NSURL *) transferURL:(NSURL *)url methodName:(NSString *) methodName;
{
    return url;
}

@end
