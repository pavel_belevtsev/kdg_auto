//
//  TVCompairAdapter.m
//  TvinciSDK
//
//  Created by Tarek Issa on 5/5/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TVCompairAdapter.h"
#import "TVCompanionDevice.h"
#import "TVDeviceFamily.h"

@implementation TVCompairAdapter

+ (BOOL)UDIDManipulationFor:(NSString *)udidOne and:(NSString *)udidTwo {
    udidOne = [[[udidOne stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@":" withString:@""] uppercaseString];
    
    udidTwo = [[[udidTwo stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@":" withString:@""] uppercaseString];
    
    if ([udidTwo isEqualToString:udidOne]) {
        return  YES;
    }
    return NO;
}


@end
