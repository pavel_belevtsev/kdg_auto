//
//  TvinciUDID.m
//  PersistenceID
//
//  Created by Rivka S. Peleg on 2/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "TvinciUDID.h"
#import "TvinciKeychainWrapper.h"

@implementation TvinciUDID


#define KEYCHAIN_IDENTIFIER @"TvinciUDID"

+ (NSString*) value
{
    
    NSString * value = nil;
    value = [TvinciKeychainWrapper searchKeychainCopyMatching:KEYCHAIN_IDENTIFIER sync:NO];
    
    if (value == nil || value.length == 0)
    {
        NSString * uuid = [[NSUUID UUID] UUIDString];
        uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        [TvinciKeychainWrapper deleteKeychainValue:KEYCHAIN_IDENTIFIER sync:NO];
        [TvinciKeychainWrapper createKeychainValue:uuid forIdentifier:KEYCHAIN_IDENTIFIER sync:NO];
        value = [TvinciKeychainWrapper searchKeychainCopyMatching:KEYCHAIN_IDENTIFIER sync:NO];
    }
    
    return value;
}


+ (void) clear
{
    [TvinciKeychainWrapper deleteKeychainValue:KEYCHAIN_IDENTIFIER sync:NO];
}



@end
