//
//  TvinciKeychainWrapper.m
//  PersistenceID
//
//  Created by Rivka S. Peleg on 2/6/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "TvinciKeychainWrapper.h"
#import <Security/Security.h>


@implementation TvinciKeychainWrapper

static NSString *serviceName = @"com.tvinci";


+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier sync:(BOOL) sync
{
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:serviceName forKey:(__bridge id)kSecAttrService];
    [searchDictionary setObject:(__bridge id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(__bridge id)kSecAttrAccessible];
    
    if (sync)
    {
        [searchDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecAttrSynchronizable];
    }
    
    return searchDictionary;
}

+ (NSString *)searchKeychainCopyMatching:(NSString *)identifier sync:(BOOL) sync {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    
    // Add search attributes
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    // Add search return types
    [searchDictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    NSData *result = nil;
    /*OSStatus status = */ SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary,
                                               (void *)&result);
    

    
    //TVLogDebug(@"%d",(int)status);
    NSString * resultString = nil;
    if (result)
    {
        resultString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    }
    
    
    return resultString ;
}

+ (BOOL)createKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync {
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier sync:sync];
    
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:valueData forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);

    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (BOOL)updateKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];
    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [updateDictionary setObject:valueData forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)searchDictionary,
                                    (__bridge CFDictionaryRef)updateDictionary);
    

    
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (void)deleteKeychainValue:(NSString *)identifier sync:(BOOL) sync {
    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier sync:sync];
    SecItemDelete((__bridge CFDictionaryRef)searchDictionary);

}


+ (BOOL)setKeychainValue:(NSString *)value forIdentifier:(NSString *)identifier sync:(BOOL) sync
{
    NSString * result = [TvinciKeychainWrapper searchKeychainCopyMatching:identifier sync:sync];
    if (result == nil)
    {
        return [TvinciKeychainWrapper createKeychainValue:value forIdentifier:identifier sync:sync];
    }
    else
    {
        return [TvinciKeychainWrapper updateKeychainValue:value forIdentifier:identifier sync:sync];
    }
}

@end
