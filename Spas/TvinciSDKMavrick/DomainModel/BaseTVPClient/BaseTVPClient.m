//
//  BaseTVPClient.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseTVPClient.h"
#import "TVConfigurationManager.h"
#import "MethodsURLPipeline.h"

#define AppIosVersion [[UIDevice currentDevice] systemVersion]
#define AppBuild [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
 
@interface BaseTVPClient ()
@end

@implementation BaseTVPClient
#pragma mark - Utilities
+(NSURL *) URLForMethodName : (NSString *) methodName
{
    NSURL * methodURL = nil;
    NSURL *serverBaseURL = [TVConfigurationManager sharedTVConfigurationManager].gatewayURL;
    NSString *URLString = [serverBaseURL absoluteString];
    NSString *query = [NSString stringWithFormat:@"?m=%@&iOSv=%@&Appv=%@",methodName,AppIosVersion,AppBuild];
    URLString = [URLString stringByAppendingString:query];
    NSString* encodedStringURL = [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    methodURL = [NSURL URLWithString:encodedStringURL];
    MethodsURLPipeline * methodsURLPipeline =[MethodsURLPipeline sharedMethodsURLPipeline];
    methodURL = [methodsURLPipeline transferURL:methodURL methodName:methodName];
    return methodURL;
}



+(TVPAPIRequest *) requestWithURL : (NSURL *) URL delegate : (id) delegate
{
    TVPAPIRequest *request = [[TVPAPIRequest alloc] initWithURL:URL];
    return request;
}





@end
