//
//  TVPUsersAPI.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 10/3/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVPUsersAPI.h"
#import "TVSessionManager.h"

@implementation TVPUsersAPI



+(TVPAPIRequest *) requestForRenewUserPasswordWithUserName : (NSString *) username
                                               newPassword : (NSString *) newPassword
                                                    delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRenewUserPassword] delegate:delegate];
        [request.postParameters setObjectOrNil:username forKey:@"sUN"];
        [request.postParameters setObjectOrNil:newPassword forKey:@"sPass"];
        return request;
}


+(TVPAPIRequest *) requestForChangeUserPasswordWithUserName : (NSString *) username
                                               oldPassword : (NSString *) oldPassword
                                               newPassword : (NSString *) newPassword
                                                  delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameChangeUserPassword] delegate:delegate];
    [request.postParameters setObjectOrNil:username forKey:@"sUN"];
    [request.postParameters setObjectOrNil:oldPassword forKey:@"sOldPass"];
    [request.postParameters setObjectOrNil:newPassword forKey:@"sPass"];
    return request;
}

#pragma WatchList API methods
+(TVPAPIRequest *) requestForAddItemToListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameAddItemToList] delegate:delegate];

    [request.postParameters setObjectOrNil:TVNameForWatchListItemTypeBy(itemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:TVNameForWatchListTypeBy(listType) forKey:@"listType"];

    NSMutableArray* mutArr = [[NSMutableArray alloc] initWithCapacity:idsArr.count] ;
    for (int i = 0; i < idsArr.count; i++) {
        id item = idsArr[i];
        if ([item isKindOfClass:[NSNumber class]]) {
            id order = [NSNull null];
            if (!autoOrder) {
                order = [NSNumber numberWithInt:i];
            }

            NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:item, @"item", order, @"orderNum", nil];
            [mutArr addObject:dic];
        }
    }
    [request.postParameters setObjectOrNil:mutArr forKey:@"itemObjects"];

    
    return request;
    
}

+(TVPAPIRequest *) requestForIsItemExistsInListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameIsItemExistsInList] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForWatchListItemTypeBy(itemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:TVNameForWatchListTypeBy(listType) forKey:@"listType"];
    
    NSMutableArray* mutArr = [[NSMutableArray alloc] initWithCapacity:idsArr.count] ;
    for (int i = 0; i < idsArr.count; i++) {
        id item = idsArr[i];
        if ([item isKindOfClass:[NSNumber class]]) {
            id order = [NSNull null];
            NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:item, @"item", order, @"orderNum", nil];
            [mutArr addObject:dic];
        }
    }
    [request.postParameters setObjectOrNil:mutArr forKey:@"itemObjects"];
    
    
    return request;
    
}

+(TVPAPIRequest *) requestForGetItemsFromListWithItemType : (TVListItemType) itemType
                                                listType : (TVListType) listType
                                              arrayofIds : (NSArray *) idsArr
                                         shouldAutoOrder : (BOOL) autoOrder
                                                delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetItemFromList] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForWatchListItemTypeBy(itemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:TVNameForWatchListTypeBy(listType) forKey:@"listType"];
    
    NSMutableArray* mutArr = [[NSMutableArray alloc] initWithCapacity:idsArr.count];
    for (int i = 0; i < idsArr.count; i++) {
        id item = idsArr[i];
        if ([item isKindOfClass:[NSNumber class]]) {
            id order = [NSNull null];
            NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:item, @"item", order, @"orderNum", nil];
            [mutArr addObject:dic];
        }
    }
    [request.postParameters setObjectOrNil:mutArr forKey:@"itemObjects"];
    
    
    return request;
    
}


+(TVPAPIRequest *) requestForRemoveItemFromListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameRemoveItemFromList] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForWatchListItemTypeBy(itemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:TVNameForWatchListTypeBy(listType) forKey:@"listType"];
    
    NSMutableArray* mutArr = [[NSMutableArray alloc] initWithCapacity:idsArr.count] ;
    for (int i = 0; i < idsArr.count; i++) {
        id item = idsArr[i];
        if ([item isKindOfClass:[NSNumber class]]) {
            id order = [NSNull null];
            NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:item, @"item", order, @"orderNum", nil];
            [mutArr addObject:dic];
        }
    }
    [request.postParameters setObjectOrNil:mutArr forKey:@"itemObjects"];
    
    
    return request;
    
}

+(TVPAPIRequest *) requestForSetUserDynamicDataExWithKey : (NSString *) key
                                                   value : (NSString *) value
                                                siteGUID : (NSString *) siteGUID
                                                domainID : (NSInteger) domainID
                                                delegate : (id) delegate
{
    //  Simulate a request with siteGUID and domainID which not yet saved for this user
    
    TVInitObject* customInitObj = [TVInitObject getInitObjFrom:[[TVSessionManager sharedTVSessionManager] sharedInitObject]];
    customInitObj.siteGUID = siteGUID;
    customInitObj.domainID = domainID;
    NSDictionary * postData = @{@"key" : key, @"value" : value};
    TVPAPIRequest *request = [TVPAPIRequest requestWithURL:[self URLForMethodName:MethodNameSetUserDynamicDataEx]];
    request.customInitObject = customInitObj;
    for (NSString * key in postData.allKeys)
    {
        [request.postParameters setObjectOrNil:[postData objectForKey:key] forKey:key];
    }

    return request;
}

+(TVPAPIRequest *) requestForUpdateItemFromListWithItemType : (TVListItemType) itemType
                                                   listType : (TVListType) listType
                                                 arrayofIds : (NSArray *) idsArr
                                            shouldAutoOrder : (BOOL) autoOrder
                                                   delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameUpdateItemInList] delegate:delegate];
    
    [request.postParameters setObjectOrNil:TVNameForWatchListItemTypeBy(itemType) forKey:@"itemType"];
    [request.postParameters setObjectOrNil:TVNameForWatchListTypeBy(listType) forKey:@"listType"];
    
    NSMutableArray* mutArr = [[NSMutableArray alloc] initWithCapacity:idsArr.count];
    for (int i = 0; i < idsArr.count; i++) {
        id item = idsArr[i];
        if ([item isKindOfClass:[NSNumber class]]) {
            id order = [NSNull null];
            NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:item, @"item", order, @"orderNum", nil];
            [mutArr addObject:dic];
        }
    }
    [request.postParameters setObjectOrNil:mutArr forKey:@"itemObjects"];
    
    
    return request;
    
}

+(TVPAPIRequest *) requestForGetAccountSTBsWithAccountNumber : (NSString *) accountNumber
                                                    delegate : (id) delegate
{
    TVPAPIRequest *request = [self requestWithURL:[self URLForMethodName:MethodNameGetAccountSTBs] delegate:delegate];
    [request.postParameters setObjectOrNil:accountNumber forKey:@"accountNumber"];
    return request;
}

NSString * const MethodNameChangeUserPassword = @"ChangeUserPassword";
NSString * const MethodNameRenewUserPassword = @"RenewUserPassword";
NSString * const MethodNameGetAccountSTBs = @"GetAccountSTBs";

NSString * const MethodNameAddItemToList            = @"AddItemToList";
NSString * const MethodNameGetItemFromList          = @"GetItemFromList";
NSString * const MethodNameIsItemExistsInList       = @"IsItemExistsInList";
NSString * const MethodNameRemoveItemFromList       = @"RemoveItemFromList";
NSString * const MethodNameUpdateItemInList         = @"UpdateItemInList";
NSString * const MethodNameSetUserDynamicDataEx     = @"SetUserDynamicDataEx";


@end
