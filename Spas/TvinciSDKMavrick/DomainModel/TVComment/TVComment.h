//
//  TVComment.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/6/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVComment : BaseModelObject

@property (strong, nonatomic) NSString * author;
@property (strong, nonatomic) NSString * header;
@property (strong, nonatomic) NSString * content;
@property (strong, nonatomic) NSString * userpicUrl;
@property (strong, nonatomic) NSDate * addedDate;


@end
