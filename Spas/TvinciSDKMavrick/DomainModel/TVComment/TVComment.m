//
//  TVComment.m
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/6/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVComment.h"

#define KEY_AUTHOR @"Author"
#define KEY_HEADER @"Header"
#define KEY_CONTENT @"Content"
#define KEY_ADDED_DATE @"AddedDate"
#define KEY_USER_PICTURE @"UserPicURL"

#define DATE_FORMAT @"MM/dd/yyyy hh:mm:ss a"

@implementation TVComment


-(void) dealloc
{

}

-(void) setAttributesFromDictionary:(NSDictionary *)dictionary
{
    self.author = [dictionary objectOrNilForKey:KEY_AUTHOR];
    self.header = [dictionary objectOrNilForKey:KEY_HEADER];
    self.content = [dictionary objectOrNilForKey:KEY_CONTENT];
    self.userpicUrl = [dictionary objectOrNilForKey:KEY_USER_PICTURE];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = DATE_FORMAT;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    
    
    NSString * dateString = [dictionary objectOrNilForKey:KEY_ADDED_DATE];
    self.addedDate = [formatter dateFromString:dateString];
    
}


-(NSString *) description
{
    NSMutableString *description = [NSMutableString stringWithFormat:@"\n Cooment \n"];
    [description appendFormat:@"Author: %@",self.author];
    [description appendFormat:@"Header: %@",self.header];
    [description appendFormat:@"Content: %@",self.content];
    [description appendFormat:@"Added date: %@",self.addedDate];
    [description appendFormat:@"Picture Url: %@",self.userpicUrl];
    return description;
}



@end
