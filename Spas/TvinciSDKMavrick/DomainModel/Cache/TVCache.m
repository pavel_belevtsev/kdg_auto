//
//  TVCache.m
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVCache.h"
#import "SynthesizeSingleton.h"
@implementation TVCache
SYNTHESIZE_SINGLETON_FOR_CLASS(TVCache);

-(id) init
{
    if (self = [super init])
    {
        [self setName:@"TVinciSharedCache"];
    }
    return self;
}
@end
