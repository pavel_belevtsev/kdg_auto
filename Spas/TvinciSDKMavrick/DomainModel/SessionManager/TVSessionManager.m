//
//  TVSessionManager.m
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSessionManager.h"
#import "NSDictionary+NSNullAvoidance.h"
#import "TVConfigurationManager.h"
#import "SynthesizeSingleton.h"
#import "TVDeviceFamily.h"
#import "TVPAPIRequest.h"
#import "TVInitObject.h"
#import "TVinciUtils.h"
#import "TVPSiteAPI.h"
#import "SmartLogs.h"
#import "TVDomain.h"
#import "TVUser.h"
#import "TVPDomainAPI.h"
#import "SmartLogs.h"
#import "SVProgressHUD.h"
#import "TvinciKeychainWrapper.h"
#import "TVTokenizationManager.h"
#import "TVToken.h"



NSString *const TVUserDataKey = @"UserData";
NSString *const TVSessionManagerKnownUsersKey = @"ExistingUsers";
NSString *const InitObjectKey = @"initObj";
NSString *const CurrentDomainKey = @"TVinciDomain";
NSString *const CurrentDeviceKey = @"TVinciDevice";
NSString *const TVSessionManagerCurrentUserKey = @"TVinciCurrentUser";
NSString *const TVSessionHasAllocedOnce = @"TVSessionHasAllocedOnce";
NSString *const TVSessionManagerUsernameKey = @"TVinciUsername";
NSString *const TVSessionManagerPasswordKey = @"TVinciPassword";

@interface TVSessionManager () <TVTokenizationManagerDelegate>


@property (strong, readwrite) TVUser *currentUser;

@property (nonatomic, strong , readwrite) TVDevice *currentDevice;
@property (nonatomic , strong , readwrite) TVInitObject *sharedInitObject;


-(void) parseSignInResponse : (TVPAPIRequest *) loginRequest;
-(void) getDomainInfo;
-(void) postNotification:(NSString *)notification error : (NSError *)error;
-(void) registerForConfigurationNotifications;
-(void) unregisterFromConfigurationNotifications;
@end

@implementation TVSessionManager
@synthesize currentUser = _currentUser;
@synthesize sharedInitObject = _sharedInitObject;
@synthesize currentDomain = _currentDomain;
@synthesize currentDevice = _currentDevice;


@synthesize showedSignInFailedError = _showedSignInFailedError;

@synthesize appTypeUsed = _appTypeUsed;
@synthesize lastPreferedLoginType = _lastPreferedLoginType, lastLoginIsForNewUser = _lastLoginIsForNewUser;
@synthesize sessionFlags = _sessionFlags;
@synthesize sessionAddedData = _sessionAddedData;

#pragma mark - Memory -

-(void) dealloc
{
    [self unregisterFromConfigurationNotifications];
    self.currentDevice = nil;
    self.currentDomain = nil;
    self.currentUser = nil;
    self.sharedInitObject = nil;
    

}

#pragma mark - Getters


-(TVInitObject *)sharedInitObject
{
    _sharedInitObject.token = self.tokenizationManager.accessToken.token;
    return _sharedInitObject;
}

-(TVDevice *) getCurrentDeviceInCurrentDomainInFamiliy:(kDeviceFamilyID) deviceFamilyID
{
    TVDevice * device =nil;
    if (self.currentDomain != nil)
    {
        
        int expectedDeviceFamilyID = deviceFamilyID;
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                
                device = [found.devices objectAtIndex:deviceIndex];
                self.currentDevice = device;
                return device;
            }
        }
    }
    
    return device;
}



-(TVDevice *) findCurrentDeviceInDomain:(NSInteger ) domainID
{
    if (self.currentDomain != nil)
    {
        NSInteger expectedDeviceFamilyID = domainID;
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                return [found.devices objectAtIndex:deviceIndex];
            }
        }
    }
    
    return nil;
    
}

-(TVDevice *)  findCurrentDeviceInCurrentDomainInALlFamiliesID
{
    
    TVDevice * device =nil;
    for (TVDeviceFamily * deviceFamily  in self.currentDomain.deviceFamilies)
    {
        device = [self findCurrentDeviceInDomain:deviceFamily.familyID];
        if (device) {
            break;
        }
    }
    
    self.currentDevice = device;
    return device;
}


-(void) findCurrentDeviceInCurrentDomain __attribute__((deprecated))
{
    if (self.currentDomain != nil)
    {
        
        int expectedDeviceFamilyID = kDeviceFamilyID_Smartphone;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            expectedDeviceFamilyID = kDeviceFamilyID_Tablet;
        }
        else
        {
            expectedDeviceFamilyID = kDeviceFamilyID_Smartphone;
        }
        
        NSInteger index = [self.currentDomain.deviceFamilies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            TVDeviceFamily *family = obj;
            return (family.familyID == expectedDeviceFamilyID);
        }];
        
        if (index != NSNotFound)
        {
            TVDeviceFamily *found = [self.currentDomain.deviceFamilies objectAtIndex:index];
            NSInteger deviceIndex = [found.devices indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                TVDevice *device = obj;
                if ([device.UDID isEqualToString:self.sharedInitObject.udid])
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];
            
            if (deviceIndex != NSNotFound)
            {
                self.currentDevice = [found.devices objectAtIndex:deviceIndex];
            }
        }
    }
}

-(TVDevice *) currentDevice
{
    @synchronized (self)
    {
        if (_currentDevice == nil)
        {
            [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
        }
        return _currentDevice;
    }
}



-(void) loadSharedInitObject
{
    self.sharedInitObject = [TVConfigurationManager sharedTVConfigurationManager].defaultInitObject;
    NSString *siteGUID = [self loadSiteGUID];
    NSString *domainIDString = [self loadDomainId];
    
    if (siteGUID != nil)
    {
        TVLogDebug(@"Loaded Saved siteGUID: %@",siteGUID);
        self.sharedInitObject.siteGUID = siteGUID;
    }
    else
    {
        self.sharedInitObject.siteGUID = @"0";
    }
    
    if (domainIDString != nil)
    {
        TVLogDebug(@"Loaded Saved domainID: %@",domainIDString);
        self.sharedInitObject.domainID = [domainIDString integerValue];
    }

    // setting the delegate will also update the access token in the init object
    self.tokenizationManager.delegate = self;

}

-(void) loadCurrentUser
{
    TVLogDebug(@"loadCurrentUser");
    NSDictionary *userDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:TVSessionManagerCurrentUserKey];
    if (userDictionary != nil)
    {
        self.currentUser = [[TVUser alloc] initWithDictionary:userDictionary];
        self.currentUser.siteGUID = [self loadSiteGUID];
        
        TVLogDebug(@"Current User loaded: %@ %@",self.currentUser.firstName, self.currentUser.lastName);
    }
    else
    {
        TVLogDebug(@"No saved current user. Getting details.");
    }
    
    // ***      depicated !     ***
    // Download again anyway because the user might update his profile from time to time.
    // [self getCurrentUserDetails];
}

-(void) saveCurrentUser
{
    NSMutableDictionary *userDictionary = [[self.currentUser keyValueRepresentation] mutableCopy];
    if (userDictionary != nil)
    {
        [userDictionary removeObjectForKey:TVUserSiteGUIDKey];
        NSDictionary * imutableUserDictionary = [NSDictionary dictionaryWithDictionary:userDictionary];
        TVLogDebug(@"Saving current user: %@",userDictionary);
        [[NSUserDefaults standardUserDefaults] setObject:imutableUserDictionary forKey:TVSessionManagerCurrentUserKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        TVLogDebug(@"Cannot save current user. Key value representation is invalid (nil dictionary)");
    }
    
}



-(TVDeviceState) deviceState
{
    if (self.currentDomain != nil)
    {
        // If there is a domain info, but the current device is not in it, then the device is not registered.
        TVLogDebug(@"self.currentDevice.deviceState = %d",self.currentDevice.deviceState);
        return (self.currentDevice != nil) ? self.currentDevice.deviceState : TVDeviceStateNotRegistered;
    }
    else
    {
        // No Domain, So no devie info at all.
        return TVDeviceStateUnknown;
    }
}

-(TVAuthenticationState) authenticationState
{
    return [self.sharedInitObject belongsToValidDomain] ? TVAuthenticationStateSignedIn : TVAuthenticationStateNotSignedIn;
}

-(BOOL) isSignedIn
{
    return [self authenticationState] == TVAuthenticationStateSignedIn;
}

-(BOOL) isDeviceReady
{
    return [self deviceState] == TVDeviceStateActivated;
}


#pragma mark - Lifecycle
-(id) init
{
    if (self = [super init])
    {
        TVLogDebug(@"[showedSignInFailedError] = NO");////////

        self.tokenizationManager = [[TVTokenizationManager alloc] init] ;
        [self clearClassifiedDataAtFirstLaunch];

        self.showedSignInFailedError = NO;
        
        [self registerForConfigurationNotifications];
        [self loadSharedInitObject];

        // setdelegate after the shared init object is loaded so it will get the token by the delegate method of the tokenization.

        
        [self loadUserData];
        if ([self.sharedInitObject belongsToValidDomain])
        {
            self.appTypeUsed = TVAppType_Unknown;
            self.lastPreferedLoginType = kPreferedLoginType_Unknown;
            self.lastLoginIsForNewUser = NO;
            [self loadCurrentUser];
            
        }
    }
    return self;
}

#pragma mark - Sign In / Sign Out

-(void) signInAfterExternalVerificationWithSiteGuid:(NSString * ) siteGuid
{
    NSString *siteGUID = siteGuid;
    
   TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:siteGUID delegate:self];
    __weak TVPAPIRequest * weakRequest = request;

    [request setFailedBlock:^{
        
    }];
    
    [request setCompletionBlock:
     ^{
         NSDictionary *response = [weakRequest JSONResponse];
         NSDictionary *user = [response objectOrNilForKey:@"m_user"];
         
         self.userData = response;
         self.currentUser = [[TVUser alloc] initWithDictionary:user];
         
         NSNumber * domainID = [user objectForKey:@"m_domianID"];
         
         [self storeSiteGUID:self.currentUser.siteGUID];
         [self storeDomainId:domainID];
         
         [self storeUserData:self.userData];
         [self storeOpeningSessionDate];
         
         if ([self.sharedInitObject belongsToValidDomain])
         {
             // Report SignIn Completed
             [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
             
             // So far so good.
             [self getDomainInfo];
             // Not necessary for the login process, but still good to have.
         }
         else
         {
             // For some reason siteGUID or DomainID is missing
             NSString * errorMessage = NSLocalizedString(@"Login Error: No SiteGUID", nil);
             NSDictionary *info = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
             NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:0 userInfo:info];
             [self postNotification:TVSessionManagerSignInFailedNotification error:error];
         }
         
         
         if(_lastLoginIsForNewUser){
             self.currentUser.preferedLoginType = _lastPreferedLoginType;
         }
         
         if (self.currentUser != nil)
         {
             [self saveCurrentUser];
             [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
         }else{
             [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
             
         }
         
     }];
    
    [request startAsynchronous];
    
}

-(void) signInWithToken:(NSString *) token
{
    
   TVPAPIRequest * request = [TVPSiteAPI requestforSignInWithToken:token delegate:nil];
    __weak TVPAPIRequest * weakRequest = request;

    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[weakRequest error]];
        
    }];
    
    [request setCompletionBlock:^{
        TVLogDebug(@"%@",weakRequest.debugPostBodyString);
        TVLogDebug(@"%@",weakRequest.responseString);
        
        [self parseSignInResponse:weakRequest];
    }];
    
    [request startAsynchronous];
}



-(void) signInSecureWithUserName:(NSString *) userName securePassword:(NSString *) securePassword
{
    
    TVLogDebug(@"[Request][SiteAPI] requestForSignInWithUserName");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestforSigninSecureWithUserName:userName
                                                                           password:securePassword
                                                                           delegate:nil];
    __weak TVPAPIRequest * weakRequest = request;

    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[weakRequest error]];
        
    }];
    
    [request setCompletionBlock:^{
        TVLogDebug(@"%@",weakRequest.debugPostBodyString);
        TVLogDebug(@"%@",weakRequest.responseString);
        
        [self parseSignInResponse:weakRequest];
    }];
    
    [request startAsynchronous];
}

-(void) signInWithUsername:(NSString *)username password:(NSString *)password
{
    
    TVLogDebug(@"[Request][SiteAPI] requestForSignInWithUserName");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestForSignInWithUserName:username
                                                                     password:password
                                                                     delegate:self];
    __weak TVPAPIRequest * weakRequest = request;
    
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[weakRequest error]];
        
    }];
    
    [request setCompletionBlock:^{
        TVLogDebug(@"%@",weakRequest.debugPostBodyString);
        TVLogDebug(@"%@",weakRequest.responseString);
        
        [self parseSignInResponse:weakRequest];
    }];
    
    [request startAsynchronous];
}

-(void) ssoSignInWithUsername:(NSString *)username password:(NSString *)password providerID:(NSInteger) providerID
{
    
    TVLogDebug(@"[Request][SiteAPI] requestForSSOSigninWithUserName");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestForSSOSigninWithUserName:username
                                                                        password:password
                                                                      providerID:providerID
                                                                        delegate:self];
    __weak TVPAPIRequest * weakRequest = request;
    
    [request setStartedBlock:^{
        
    }];
    
    [request setFailedBlock:^{
        [self postNotification:TVSessionManagerSignInFailedNotification error:[weakRequest error]];
        
    }];
    
    [request setCompletionBlock:^{
        TVLogDebug(@"%@",weakRequest);
        [self parseSignInResponse:weakRequest];
    }];
    
    [request startAsynchronous];
}



-(void) resetSessionInfo
{
    self.sharedInitObject.siteGUID = nil;
    self.sharedInitObject.token = nil;
    self.sharedInitObject.domainID = 0;
    self.currentDevice = nil;
    self.currentDomain = nil;
    self.currentUser = nil;
    self.lastOpeningSessionDate = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:InitObjectKey];
    [defaults removeObjectForKey:CurrentDeviceKey];
    [defaults removeObjectForKey:CurrentDomainKey];
    [defaults removeObjectForKey:TVUserDataKey];
    [defaults removeObjectForKey:TVSessionManagerCurrentUserKey];
    [defaults synchronize];
    
    [self deleteSiteGUID];
    [self deleteDomainID];

    [self.tokenizationManager clearTokens];
    
    NSArray *cookiesArray = [NSArray arrayWithArray:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [cookiesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:obj];
    }];
    
    
    
    
}


-(void) logoutWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock
{
    @synchronized (self)
    {
        TVLogDebug(@"[Request][SiteAPI] requestForSignOut");////////

       TVPAPIRequest * request = [TVPSiteAPI requestForSignOut:self];
        [request setStartedBlock:^{

            if (startBlock)
            {
                startBlock();
            }

            [self logOutCompleted];

            if (completionBlock)
            {
                completionBlock();
            }

        }];


        [request setFailedBlock:^{
        }];

        [request setCompletionBlock:^{
        }];

        [request startAsynchronous];
    }
}


/*
 (void) logoutFromIPNO - makes a logout but keeps some consistant data:
 
 -  Made for extra facilities and features made by Tvinci Backend
 by exentding to Multi User option.

 -  Used by projects such Eutelsat, for multi user feature.
 
 
 Scenario for example:
 When logout is made, there still an option for
 login with another user from the same domain,
 so domain information should not be deleted.
 
 */
-(void) logoutFromIPNOWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock
{

    @synchronized (self)
    {
        TVLogDebug(@"[Request][SiteAPI] requestForSignOut");////////

       TVPAPIRequest * request = [TVPSiteAPI requestForSignOut:self];
        [request setStartedBlock:^{

            if (startBlock)
            {
                startBlock();
            }

            [self logOutIPNOCompleted];

            if (completionBlock)
            {
                completionBlock();
            }
        }];

        [request setFailedBlock:^{}];

        [request setCompletionBlock:^{}];

        [request startAsynchronous];
    }
    
}
/*!
 @abstract Passes the call to the TVInitObject to reset the domainID and siteGUID
 */
-(void) logout
{
    [self logoutWithStartBlock:nil failedBlock:nil completionBlock:nil];
}

/*!
 @abstract logoutFromIPNO: Logout from IPNO account. And ready to login to a new IPNO account.
 */
-(void) logoutFromIPNO
{
    [self logoutFromIPNOWithStartBlock:nil failedBlock:nil completionBlock:nil];
}

- (BOOL)isSignedInToIPNO {
    if (self.userData) {
        return YES;
    }
    return NO;
}

-(void) resetSessionIPNOInfo
{
    self.currentUser = nil;
    self.userData = nil;
    self.sharedInitObject.siteGUID = nil;
    self.sharedInitObject.token = nil;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:TVUserDataKey];
    [defaults removeObjectForKey:TVSessionManagerCurrentUserKey];
    [defaults synchronize];
    
    [self deleteSiteGUID];
    [self.tokenizationManager clearTokens];
    
    
    //  Remove coockies
    NSArray *cookiesArray = [NSArray arrayWithArray:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [cookiesArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:obj];
    }];
    
    
}

-(void)logOutIPNOCompleted
{
    [self resetSessionIPNOInfo];
    [self postNotification:TVSessionManagerLogoutCompletedNotification userInfo:nil];
}


-(void)logOutCompleted
{
    [self resetSessionInfo];
    TVLogDebug(@"Logged Out");
    [self postNotification:TVSessionManagerLogoutCompletedNotification userInfo:nil];
}

#pragma mark - Domain And Device
-(void) checkDeviceState
{
    if (self.deviceState == TVDeviceStateUnknown)
    {
        [self getDomainInfo];
    }
}

-(void) forceCheckDeviceState{
    
    [self getDomainInfo];
    
}


-(void) getDomainInfo
{
    static BOOL inProgress = NO;
    if (inProgress == NO)
    {
        inProgress = YES;
        TVLogDebug(@"[Request][SiteAPI] requestForGetDomainInfo");////////
        
       TVPAPIRequest * request = [TVPDomainAPI requestForGetDomainInfo:self];
        __weak TVPAPIRequest * weakRequest = request;

        [request setFailedBlock:^{
            //ASLog@"GetDomainInfo Failed: %@",[request error]);
            [self postNotification:TVSessionManagerSignInFailedNotification error:[weakRequest error]];
            inProgress = NO;
        }];
        
        [request setCompletionBlock:^{
            
            NSDictionary *response = [weakRequest JSONResponse];
            self.currentDomain =[[TVDomain alloc] initWithDictionary:response];
            [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
            if (self.currentDomain != nil)
            {
                TVLogDebug(@"Domain Exist. Device State Available");
                // If we have domain, we know the device state.
                [self postNotification:TVSessionManagerDeviceStateAvailableNotification userInfo:nil];
            }
            inProgress = NO;
        }];
        
        [request startAsynchronous];
    }
}

-(void) registerDevice
{
    TVLogDebug(@"[Request][SiteAPI] requestForAddDeviceToDomainWithDeviceName");////////
    NSInteger deviceBrand;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        deviceBrand = kDeviceBrand_iPad;
    }
    else
    {
        deviceBrand = kDeviceBrand_iPhone;
        
    }
    
   TVPAPIRequest * request = [TVPDomainAPI requestForAddDeviceToDomainWithDeviceName:[UIDevice currentDevice].name
                                                                               deviceBrandID:deviceBrand
                                                                                    delegate:self];
    __weak TVPAPIRequest * weakRequest = request;
    
    [request setFailedBlock:^{
        TVLogDebug(@"request  = %@",weakRequest);
        [self postNotification:TVSessionManagerDeviceRegistrationFailedNotification error:[weakRequest error]];
    }];
    
    [request setCompletionBlock:^{
        
        [self parseRegisterDeviceResponse:weakRequest];
        
    }];
    
    [request startAsynchronous];
}


-(void) parseRegisterDeviceResponse : (TVPAPIRequest *) request
{
    TVLogDebug(@"Device registration finished ");
    NSDictionary *response = [request JSONResponse];
    TVLogDebug(@"request  = %@",request);
    
    self.currentDomain = [[TVDomain alloc] initWithDictionary:[response objectForKey:TVDomainKey]];
    TVLogDebug(@"self.currentDomain= %@",self.currentDomain);////////
    
    [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
    NSNumber* DomainResponseStatus = [response objectForKey:kAPIResponseKey_DomainResponseStatusKey];
    if (self.currentDevice != nil)
    {
        [self postNotification:TVSessionManagerDeviceRegistrationCompletedNotification userInfo:nil];
    }
    else
    {
        // handling known error
        // checking the response status
        if([DomainResponseStatus intValue] == kDomainResponseStatus_DeviceAlreadyExists)
        {
            TVLogDebug(@"[Device][Registration] Device already exists on another domain");////////
            NSString *errorStr = NSLocalizedString(@"Dialog Message: Device registered for another user", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification error:error];
            return;
        }
        else
        {
            NSString *errorStr = NSLocalizedString(@"Login Error: Device Registration Failed", nil);
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorStr, NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceRegistrationFailedNotification error:error];
        }
    }
}

-(void) activateDevice
{
    
    TVLogDebug(@"[Request][SiteAPI] requestForChangeDeviceDomainStatus");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestForChangeDeviceDomainStatus:YES delegate:self];
    __weak TVPAPIRequest * weakRequest = request;

    [request setFailedBlock:^{
        //ASLog@"Device Activation Failed: %@",[request error]);
        [self postNotification:TVSessionManagerDeviceActivationFailedNotification error:[weakRequest error]];
    }];
    
    [request setCompletionBlock:^{
        NSDictionary *response = [weakRequest JSONResponse];
        NSNumber* DomainResponseStatus = [response objectForKey:kAPIResponseKey_DomainResponseStatusKey];
        self.currentDomain = [[TVDomain alloc] initWithDictionary:[response objectForKey:TVDomainKey]] ;
        [self findCurrentDeviceInCurrentDomainInALlFamiliesID];
        if ([self deviceState] == TVDeviceStateActivated)
        {
            [self postNotification:TVSessionManagerDeviceActivationCompletedNotification userInfo:nil];
        }
        else
        {
            
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      NSLocalizedString(@"Login Error: Device Activation Failed", nil), NSLocalizedDescriptionKey,
                                      DomainResponseStatus, kAPIResponseKey_DomainResponseStatusKey, nil];
            
            NSError *error = [NSError errorWithDomain:TVDeviceRegistrationErrorDomain code:0 userInfo:userInfo];
            [self postNotification:TVSessionManagerDeviceActivationFailedNotification error:error];
        }
    }];
    
    [request startAsynchronous];
}


-(void) getDomainIDFromUserDetailsWithSiteGUID : (NSString *) siteGUID
{
    TVLogDebug(@"");
    TVLogDebug(@"[Request][SiteAPI] requestForGetUserDetails");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:siteGUID delegate:self];
    __weak TVPAPIRequest * weakRequest = request;
    [request setStartedBlock:^{
        
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Logging in...", nil) maskType:SVProgressHUDMaskTypeGradient];    }];
    
    [request setFailedBlock:^{
        //ASLog@"GetUserDetails Failed: %@",request.error);
        [ SVProgressHUD dismiss];
    }];
    
    [request setCompletionBlock:^{
        [ SVProgressHUD dismiss];
        NSError *error = nil;
        NSNumber *domainID = nil;
        NSDictionary *response = [weakRequest JSONResponse];
        NSDictionary *user = [response objectOrNilForKey:@"m_user"];
        self.currentUser = [[TVUser alloc] initWithDictionary:user] ;
        
        if (self.currentUser.username != nil)
        {
            [self saveCurrentUser];
            [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
        }
        
        domainID = [user objectOrNilForKey:@"m_domianID"];
        
        [self storeSiteGUID:siteGUID];
        [self storeDomainId:domainID];
        
        if ([self.sharedInitObject belongsToValidDomain])
        {
            [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
            [self getDomainInfo];
        }
        else
        {
            NSString *errorTitle = NSLocalizedString(@"Dialog title: Facebook connect unavailable", nil);
            NSString *errorMessage = NSLocalizedString(@"Dialog body: Please log in using your Access ID", nil);
            
            // shefyg - added facebook error because want specific title and error
            NSDictionary * errDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:errorMessage,errorTitle,nil] forKeys:[NSArray arrayWithObjects:NSLocalizedDescriptionKey,@"userErrorTitle",nil]];
            
            error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusUserDoesNotExist userInfo:errDictionary];
        }
        
        if (error != nil)
        {
            // shefyg - changed because want specific error
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            
            
        }
    }];
    
    [request startAsynchronous];
}



#pragma mark - Notifications

-(void) postNotification:(NSString *)notification error:(NSError *)error
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:error forKey:TVErrorKey];
    [self postNotification:notification userInfo:userInfo];
}



- (void) storeSiteGUID:(NSString *)siteGUID
{
    self.sharedInitObject.siteGUID = siteGUID;
    
    if (siteGUID != nil)
    {
        [TvinciKeychainWrapper setKeychainValue:siteGUID forIdentifier:TVConfigSiteGUIDKey sync:NO];
    }
}


-(void) storeDomainId:(NSNumber *) domainID
{
    self.sharedInitObject.domainID = [domainID integerValue];
    
    if (domainID != nil)
    {
        [TvinciKeychainWrapper setKeychainValue:[domainID stringValue] forIdentifier:TVConfigDomainIDKey sync:NO];
    }
}


-(NSString *) loadSiteGUID
{
    NSString * siteGuid = [TvinciKeychainWrapper searchKeychainCopyMatching:TVConfigSiteGUIDKey sync:NO];
    return  siteGuid;
}

-(NSString * ) loadDomainId
{
    NSString * domainId = [TvinciKeychainWrapper searchKeychainCopyMatching:TVConfigDomainIDKey sync:NO];
    return  domainId;
    
}


-(void) deleteSiteGUID
{
    [TvinciKeychainWrapper deleteKeychainValue:TVConfigSiteGUIDKey sync:NO];
}

-(void) deleteDomainID
{
    [TvinciKeychainWrapper deleteKeychainValue:TVConfigDomainIDKey sync:NO];
    
}


-(void) loadUserData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableData *data = [userDefaults objectForKey:TVUserDataKey];
    if (data != nil)
    {
        NSDictionary *myDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (myDictionary != nil)
        {
            NSMutableDictionary * userDataWithSiteGuid = [myDictionary mutableCopy];
            if ([self loadSiteGUID])
            {
                [userDataWithSiteGuid setObject:[self loadSiteGUID] forKey:TVUserSiteGUIDKey];
                TVLogDebug(@"Loaded Saved userData: %@",myDictionary);
                
            }
            self.userData = [NSDictionary dictionaryWithDictionary:userDataWithSiteGuid];
        }
        else
        {
            self.userData = [NSDictionary dictionary];
        }
    }
}


-(void) storeUserData : (NSDictionary *)userDataDict
{
    if (userDataDict != nil)
    {
        NSMutableDictionary * mutableUserData = [userDataDict mutableCopy];
        [mutableUserData removeObjectForKey:TVUserSiteGUIDKey];
        
        /** data is ready now, and you can use it **/
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:mutableUserData];
        if (data != nil) {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:data forKey:TVUserDataKey];
            [userDefaults synchronize];
        }
    }
}

#pragma mark - Configuration

-(void) configurationReady : (NSNotification *) notification
{
    TVLogDebug(@"Cofiguration File Ready. Updating initObj...");
}

-(void) loadConsistantData : (NSNotification *) notification
{
    TVLogDebug(@"Consistant Data Loaded...");
    [self loadSharedInitObject];
    [self loadUserData];
    if ([self.sharedInitObject belongsToValidDomain])
    {
        self.appTypeUsed = TVAppType_Unknown;
        self.lastPreferedLoginType = kPreferedLoginType_Unknown;
        self.lastLoginIsForNewUser = NO;
        [self loadCurrentUser];
    }
}

-(void) registerForConfigurationNotifications
{
    [self unregisterFromConfigurationNotifications];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVConfigurationManager *mngr = [TVConfigurationManager sharedTVConfigurationManager];
    [center addObserver:self
               selector:@selector(configurationReady:)
                   name:TVConfigurationManagerDidLoadConfigurationNotification
                 object:mngr];
    [center addObserver:self
               selector:@selector(loadConsistantData:)
                   name:TVConfigurationManagerReadyToLoadConsistantData
                 object:nil];


}

-(void) unregisterFromConfigurationNotifications
{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    TVConfigurationManager *mngr = [TVConfigurationManager sharedTVConfigurationManager];
    [center removeObserver:self
                      name:TVConfigurationManagerDidLoadConfigurationNotification
                    object:mngr];
    [center removeObserver:self
                      name:TVConfigurationManagerReadyToLoadConsistantData
                    object:nil];
}

#pragma mark - TVPAPIRequest Delegate -

-(void) parseSignInResponse : (TVPAPIRequest *) loginRequest
{
    NSDictionary *response = [[loginRequest JSONResponse] dictionaryByRemovingNSNulls];
    NSNumber *domainID = [response objectOrNilForKey:TVConfigDomainIDKey];
    NSString *siteGUID = [response objectOrNilForKey:TVConfigSiteGUIDKey]; //TVLoginStatusCustomError
    
    TVLoginStatus loginStatus = TVLoginStatusCustomError;
    
    if ([response objectOrNilForKey:@"LoginStatus"])
    {
        loginStatus = (TVLoginStatus)[[response objectOrNilForKey:@"LoginStatus"] integerValue];
    }
    
    self.userData = [response objectForKey:TVUserDataKey];
    
    TVLogDebug(@"Login Status: %ld",(long)loginStatus);
    TVLogDebug(@"SiteGUID: %@",siteGUID);
    TVLogDebug(@"DomainID: %@",domainID);
    
    NSString *errorMessage = nil;
    
    if (loginStatus == TVLoginStatusOK || loginStatus == TVLoginStatusDeviceNotRegistered || (loginStatus == TVLoginStatusUserSuspended && [[TVConfigurationManager sharedTVConfigurationManager] loginBlockedBySuspend] == NO))
    {
        if ( self.assertLoginBlock && self.assertLoginBlock(response) == NO)
        {
            //If we have siteGUID and user data
            NSDictionary *info = [NSDictionary dictionaryWithObject:response forKey:NSLocalizedDescriptionKey];
            NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:TVLoginStatusCustomError userInfo:info];
            [self postNotification:TVSessionManagerSignInFailedNotification error:error];
            return;
        }

        [self.tokenizationManager authenticationSuccededWithResponseHeader:loginRequest.responseHeaders];
        
        [self storeSiteGUID:siteGUID];
        [self storeDomainId:domainID];
        
        [self storeUserData:self.userData];
        [self storeOpeningSessionDate];
        
        if ([self.sharedInitObject belongsToValidDomain])
        {
            // Report SignIn Completed
            [self postNotification:TVSessionManagerSignInCompletedNotification userInfo:nil];
            
            // So far so good - Calling deviceStateAvailable:
            [self getDomainInfo];
            // Not necessary for the login process, but still good to have.
            
            [self updateUserDataFromSignInResponse:loginRequest];
            // [self getCurrentUserDetails];
        }
        else
        {
            // For some reason siteGUID or DomainID is missing
            errorMessage = NSLocalizedString(@"Login Error: No SiteGUID", nil);
        }
    }
    else
    {
        errorMessage = ErrorMessageForLoginStatus(loginStatus);
    }
    
    if (errorMessage!= nil)
    {
        TVLogDebug(@"login errorMessage = %@", errorMessage);
        NSDictionary *info = [NSDictionary dictionaryWithObject:errorMessage forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:TVSignInErrorDomain code:loginStatus userInfo:info];
        [self postNotification:TVSessionManagerSignInFailedNotification error:error];
    }
}

#pragma mark - User Details -

-(void)updateUserDataFromSignInResponse : (TVPAPIRequest *) loginRequest
{
    
    NSDictionary *response = [loginRequest JSONResponse];
    
      
    NSDictionary *user = [response objectOrNilForKey:TVUserDataKey];
    if (user != nil)
    {
        self.currentUser =[[TVUser alloc] initWithDictionary:user] ;
    }
    
    if(_lastLoginIsForNewUser)
    {
        self.currentUser.preferedLoginType = _lastPreferedLoginType;
    }
    
    if (self.currentUser != nil)
    {
        [self saveCurrentUser];
        [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
    }
    else
    {
        [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
    }
    
}

#pragma mark - Session Flags and data

-(void) addFlag:(NSString *)flag
{
    if(!_sessionFlags)
    {
        self.sessionFlags = [NSMutableArray array];
    }
    
    if(![_sessionFlags containsObject:flag])
    {
        [_sessionFlags addObject:flag];
    }
    
}

-(BOOL) hasFlag:(NSString *)flag{
    if(!_sessionFlags){
        return NO;
    }
    return [_sessionFlags containsObject:flag];
}

-(void) addAddedDataArray:(NSArray *)data forKey:(NSString *)key{
    if(!_sessionAddedData){
        self.sessionAddedData = [NSMutableDictionary dictionary];
    }
    [_sessionAddedData setObject:data forKey:key];
}

-(BOOL) hasAddedDataForKey:(NSString *)key{
    if(_sessionAddedData && [_sessionAddedData objectOrNilForKey:key] !=nil){
        return YES;
    }
    return NO;
}


-(NSArray *) takeAddedDataForKey:(NSString *)key{
    if([self hasAddedDataForKey:key]){
        return [_sessionAddedData objectOrNilForKey:key];
    }
    return nil;
}





#pragma mark - Singlton
SYNTHESIZE_SINGLETON_FOR_CLASS(TVSessionManager);


#pragma mark - IPNO Operators

-(void) setOprator:(TVIPNOperator *)tvoperator
{
    if (tvoperator.operatorUsername != nil && tvoperator.operatorPassword != nil)
    {
        //        self.sharedInitObject.APIUsername = tvoperator.operatorUsername;
        //        self.sharedInitObject.APIPassword = tvoperator.operatorPassword;
    }
}

-(BOOL) isUserConnectedToFacebook
{
    return self.currentUser.faceBookToken.length>0;
}


#pragma mark - dates

-(void) storeOpeningSessionDate
{
    self.lastOpeningSessionDate = [NSDate date];
}

#pragma mark - User Details

-(void) getCurrentUserDetailsWithStartBlock:(void(^)()) startBlock failedBlock:(void(^)()) failedBlock completionBlock:(void(^)()) completionBlock
{
    NSString *siteGUID = self.sharedInitObject.siteGUID;
    TVLogDebug(@"[Request][SiteAPI] requestForGetUserDetails");////////
    
   TVPAPIRequest * request = [TVPSiteAPI requestForGetUserDetails:siteGUID delegate:self];
    __weak TVPAPIRequest * weakRequest = request;

    [request setStartedBlock:^{
        TVLogDebug(@"Getting user details");
        if (startBlock)
        {
            startBlock();
        }
    }];
    
    [request setFailedBlock:^{
        ASLogError(weakRequest.error);
        TVLogDebug(@"[Request][RequestFailed] - getCurrentUserDetails");////////
        
        [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
        if (failedBlock)
        {
            failedBlock();
        }
        
        
    }];
    
    [request setCompletionBlock:
     ^{
         if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
         {
             NSDictionary *response = [weakRequest JSONResponse];
             NSDictionary *user = [response objectOrNilForKey:@"m_user"];
             self.currentUser = [[TVUser alloc] initWithDictionary:user] ;
             
             if(_lastLoginIsForNewUser)
             {
                 self.currentUser.preferedLoginType = _lastPreferedLoginType;
             }
             
             if (self.currentUser != nil)
             {
                 [self saveCurrentUser];
                 [self postNotification:TVSessionManagerUserDetailsAvailableNotification userInfo:nil];
             }
             else
             {
                 [self postNotification:TVSessionManagerCurrentUserDetailsErrorNotification userInfo:nil];
             }
         }
         if (completionBlock)
         {
             completionBlock();
         }
     }];
    
    [request startAsynchronous];
}


-(void) clearClassifiedDataAtFirstLaunch
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:TVSessionHasAllocedOnce])
    {
        // app already launched and keychain is uptodate
    }
    else
    {
        // This is the first launch after installation or first generating this code
        //inserting flag to nsuser defaults to identify first launch to clear keychain data that where not deleted after unintall
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TVSessionHasAllocedOnce];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *siteGUID = [userDefaults objectForKey:TVConfigSiteGUIDKey];
        NSNumber *domainID = [userDefaults objectForKey:TVConfigDomainIDKey];
        
        if (siteGUID && domainID)
        {
            [userDefaults removeObjectForKey:TVConfigSiteGUIDKey];
            [userDefaults removeObjectForKey:TVConfigDomainIDKey];
            [userDefaults synchronize];
            
            [self storeSiteGUID:siteGUID];
            [self storeDomainId:domainID];
            
        }
        else
        {
            // key chain values are not deleted after unintall so this is to ensure previuse information from the last insall are deleted
            [self.tokenizationManager clearTokens];
            [self deleteDomainID];
            [self deleteSiteGUID];
        }



        [userDefaults removeObjectForKey:TVSessionManagerUsernameKey];
        [userDefaults removeObjectForKey:TVSessionManagerPasswordKey];
        
        
        NSMutableDictionary *userDictionary = [[userDefaults objectForKey:TVSessionManagerCurrentUserKey] mutableCopy];
        [userDictionary  removeObjectForKey:TVUserSiteGUIDKey];
        [userDefaults setObject:userDictionary forKey:TVSessionManagerCurrentUserKey];
        
        
        NSData *data = [userDefaults objectForKey:TVUserDataKey];
        if (data != nil)
        {
            NSDictionary *myDictionary = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if (myDictionary != nil)
            {
                NSMutableDictionary * userDataWithoutSiteGuid = [myDictionary mutableCopy];
                [userDataWithoutSiteGuid removeObjectForKey:TVUserSiteGUIDKey];
                
                data = [NSKeyedArchiver archivedDataWithRootObject:userDataWithoutSiteGuid];
                if (data != nil)
                {
                    
                    [userDefaults setObject:data forKey:TVUserDataKey];
                    [userDefaults synchronize];
                }
                
            }
        }
        
        [userDefaults removeObjectForKey:TVSessionManagerKnownUsersKey];
        
        [userDefaults synchronize];
    }
}


#pragma mark - tokenizaion

-(void)tokenizationManagerAuthenticationExpired:(TVTokenizationManager *)sender
{
    NSError * error = [[NSError alloc] init];
    [self postNotification:TVSessionManagerAuthenticationExpiredNotification error:error];
}


@end


// Error Domains
NSString * const TVSignInErrorDomain = @"TVSignInErrorDomain";
NSString * const TVDeviceRegistrationErrorDomain = @"TVDeviceRegistrationErrorDomain";
NSString * const TVDeviceActivationErrorDomain = @"TVDeviceActivationErrorDomain";
NSString * const TVLogoutErrorDomain = @"TVLogoutErrorDomain";

// Facebook Login
NSString * const facebookResposeStatus = @"status";

// Device Sign In Notification
NSString * const TVSessionManagerSignInCompletedNotification = @"TVSessionManagerSignInCompletedNotification";
NSString * const TVSessionManagerSignInFailedNotification = @"TVSessionManagerSignInFailedNotification";

// Logout Notification
NSString * const TVSessionManagerLogoutCompletedNotification = @"TVSessionManagerLogoutCompletedNotification";
NSString * const TVSessionManagerLogoutFailedNotification = @"TVSessionManagerLogoutFailedNotification";

// Device Registration Notification
NSString * const TVSessionManagerDeviceRegistrationCompletedNotification = @"TVSessionManagerDeviceRegistrationCompletedNotification";
NSString * const TVSessionManagerDeviceRegistrationFailedNotification = @"TVSessionManagerDeviceRegistrationFailedNotification";
NSString * const TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification = @"TVSessionManagerDeviceRegistrationFailedWithErrorNotification";


// Device Activation Notification
NSString * const TVSessionManagerDeviceActivationCompletedNotification = @"TVSessionManagerDeviceActivationCompletedNotification";
NSString * const TVSessionManagerDeviceActivationFailedNotification = @"TVSessionManagerDeviceActivationFailedNotification";

NSString * const TVSessionManagerDeviceStateAvailableNotification = @"TVSessionManagerDeviceStateAvailableNotification";
NSString * const TVSessionManagerUserDetailsAvailableNotification = @"TVSessionManagerUserDetailsAvailableNotification";

NSString * const TVSessionManagerCurrentUserDetailsErrorNotification = @"TVSessionManagergetCurrentUserDetailsErrorNotification";

NSString * const TVSessionManagerAuthenticationExpiredNotification = @"TVSessionManagerAuthenticationExpiredNotification";;



