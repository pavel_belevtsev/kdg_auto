//
//  TVSubscriptionData.h
//  TvinciSDK
//
//  Created by Alex Zchut on 2/21/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

//m_sCodes
@protocol TVSubscriptionData_codes @end
@interface TVSubscriptionData_codes : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, strong) NSString *name;

@end

//m_sName
@protocol TVSubscriptionData_name @end
@interface TVSubscriptionData_name : JSONModel

@property (nonatomic, strong) NSString *languageCode3, *value;

@end

//m_oPrise > m_oCurrency
@interface TVSubscriptionData_prise_currency : JSONModel

@property (nonatomic, strong) NSString *currencyCD3, *currencyCD2, *currencySign;
@property (nonatomic, readwrite) NSInteger currencyId;

@end

//m_oPrise
@interface TVSubscriptionData_prise : JSONModel

@property (nonatomic, readwrite) CGFloat price;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) TVSubscriptionData_prise_currency *currency;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_subscriptionPriceCode : JSONModel

@property (nonatomic, readwrite) NSInteger code;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) TVSubscriptionData_prise *prise;
@property (nonatomic, readwrite) NSInteger objectId;
@property (nonatomic, strong) NSString<Optional> *description1;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_subscriptionUsageModule : JSONModel

@property (nonatomic, readwrite) NSInteger waiver, couponId, deviceLimitId, extDiscountId, internalDiscountId, isRenew, maxNumberOfViews, objectId, waiverPeriod, numOfRecPeriods, pricingId, maxUsageModuleLifeCycle, viewLifeCycle, type;
@property (nonatomic, readwrite) BOOL isSubscriptionOnly;
@property (nonatomic, readwrite) BOOL isOfflinePlayback;
@property (nonatomic, strong) NSString *virtualName;

@end

//m_oSubscriptionPriceCode
@interface TVSubscriptionData_purchasingInfo : JSONModel

@property (nonatomic, readwrite) NSInteger subscriptionCode, maxUses, currentUses, paymentMethod, subscriptionPurchaseId;
@property (nonatomic, strong) NSString *stringCurrentDate, *stringLastViewDate, *stringPurchaseDate, *stringEndDate;
@property (nonatomic, readwrite) BOOL recurringStatus, isSubRenewable;
@property (nonatomic, strong) NSString<Optional> *deviceUDID, *deviceName;

@property (nonatomic, strong, readonly) NSDate<Ignore> *lastViewDate, *currentDate, *purchaseDate, *endDate;

@end

@class TVMediaItem;
@interface TVSubscriptionData : JSONModel

@property (nonatomic, strong) NSArray<TVSubscriptionData_codes ,ConvertOnDemand ,Optional> *codes;
@property (nonatomic, strong) NSString *stringStartDate;
@property (nonatomic, strong) NSString *stringEndDate;

@property (nonatomic, strong) NSArray<Optional> *fileTypes;
@property (nonatomic, readwrite) BOOL isRecurring;
@property (nonatomic, readwrite) NSInteger numberOfRecPeriods;
@property (nonatomic, strong) TVSubscriptionData_subscriptionPriceCode *subscriptionPriceCode;
@property (nonatomic, strong) NSArray<TVSubscriptionData_name ,ConvertOnDemand> *arrName, *arrDescription;
@property (nonatomic, strong) TVSubscriptionData_subscriptionUsageModule *subscriptionUsageModule;
@property (nonatomic, readwrite) NSInteger priority;
@property (nonatomic, strong) NSString *productCode;
@property (nonatomic, strong) NSString *subscriptionCode;
@property (nonatomic, strong) TVSubscriptionData_subscriptionPriceCode<Optional> *priceCode;
@property (nonatomic, strong) NSDictionary *usageModule;
@property (nonatomic, strong) NSDictionary *discountModule;
@property (nonatomic, strong) NSDictionary *couponsGroup;
@property (nonatomic, readwrite) NSInteger objectCode;
@property (nonatomic, strong) NSString *objectVirtualName;
@property (nonatomic, readwrite) BOOL subscriptionOnly;
@property (nonatomic, strong) TVMediaItem<Ignore> *virtualMediaItem;
@property (nonatomic, strong) TVSubscriptionData_purchasingInfo<Ignore> *purchasingInfo;

@end

