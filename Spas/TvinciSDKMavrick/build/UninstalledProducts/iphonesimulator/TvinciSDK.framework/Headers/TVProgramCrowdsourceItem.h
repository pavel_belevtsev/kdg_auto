//
//  TVProgramCrowdsourceItem.h
//  TvinciSDK
//
//  Created by Amit Bar-Shai on 12/28/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>
#import "TVCrowdsourceItem.h"

@interface TVProgramCrowdsourceItem : TVCrowdsourceItem

@property (nonatomic, strong) NSDate *epgStartTime;
@property (nonatomic, copy , readonly) NSString *programID;
@property (nonatomic, strong) NSString *programImage;
@property (nonatomic, copy) NSString *programName;
@property (nonatomic, assign) NSInteger views;

@end
