//
//  TVFacebookConfig.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/19/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVFacebookConfig : BaseModelObject

@property (strong, nonatomic) NSString * appID;
@property (strong, nonatomic) NSArray * scopeElements;
//@property (strong, nonatomic) NSString * APIUser;
//@property (strong, nonatomic) NSString * APIPassword;




@end
