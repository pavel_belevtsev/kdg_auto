//
//  TVMenuItem.h
//  Kanguroo
//
//  Created by Avraham Shukron on 4/29/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModelObject.h"

typedef enum
{
    TVMenuItemTypeUnknown=0,
    TVMenuItemTypeCategory =0,
    
    TVMenuItemTypeChannel,
    TVMenuItemTypeGallery,
    TVMenuItemTypeRoot
}TVMenuItemType;

@interface TVMenuItem : BaseModelObject
@property (nonatomic, assign, readonly) TVMenuItemType type;
@property (nonatomic , assign) NSInteger menuItemID;
@property (nonatomic , assign) NSInteger menuType;
@property (nonatomic , assign) NSInteger ruleID;
@property (nonatomic , copy) NSString *name;
@property (nonatomic , copy) NSString *uniqueName;

@property (nonatomic , copy) NSString *culture;
@property (nonatomic, strong) NSArray *children;
@property (nonatomic, strong) NSURL *URL;
// Extra info

@property (nonatomic, assign) NSInteger channelID;
@property (nonatomic , assign) NSInteger galleryID;
@property (nonatomic , assign) NSInteger topGalleryID;
@property (nonatomic , assign) NSInteger mainGalleryID;
@property (nonatomic , assign) NSInteger pageID;
@property (nonatomic , copy) NSString *pageLayout;
@property (nonatomic, strong) NSURL *iconURL;
@property (nonatomic , copy) NSString *iconName;
@property (nonatomic, copy) NSString * vodCategoryID;
@property (nonatomic, strong) NSDictionary * layout;


@end

extern NSString *const TVMenuItemNameKey;
extern NSString *const TVMenuItemMenuTypeKey;
extern NSString *const TVMenuItemRuleIDKey;
extern NSString *const TVMenuItemIDKey;
extern NSString *const TVMenuItemPageIDKey;
extern NSString *const TVMenuItemPageLayoutKey;
extern NSString *const TVMenuItemCultureKey;
extern NSString *const TVMenuItemChildrenKey;
extern NSString *const TVMenuItemURLKey;
extern NSString *const TVMenuItemGalleryIDKey;
extern NSString *const TVMenuItemTopGalleryIDKey;
extern NSString *const TVMenuItemMainGalleryIDKey;
extern NSString *const TVMenuItemIconURLKey;
extern NSString *const TVMenuItemIconKey;
extern NSString * const TVMenuIremVODCategoryIdKey;
