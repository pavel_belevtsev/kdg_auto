//
//  APSTVChannel.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/16/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSTVProgram;

@interface APSTVChannel : APSBaseManagedObject

@property (nonatomic, strong) NSString * channelID;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * pictureURL;
@property (nonatomic, strong) NSString * channelDescription;
@property (nonatomic, strong) NSString * orderNumber;
@property (nonatomic, strong) NSString * isActive;
@property (nonatomic, strong) NSString * groupID;
@property (nonatomic, strong) NSString * editorRemarks;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSString * updaterID;
@property (nonatomic, strong) NSDate * createDate;
@property (nonatomic, strong) NSDate * publishDate;
@property (nonatomic, strong) NSString * mediaID;
@property (nonatomic, strong) NSString * epgChannelID;
@property (nonatomic, strong) NSSet *programs;
@end

@interface APSTVChannel (CoreDataGeneratedAccessors)

+ (APSTVChannel *) channelWithdictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context;


- (void)addProgramsObject:(APSTVProgram *)value;
- (void)removeProgramsObject:(APSTVProgram *)value;
- (void)addPrograms:(NSSet *)values;
- (void)removePrograms:(NSSet *)values;

@end
