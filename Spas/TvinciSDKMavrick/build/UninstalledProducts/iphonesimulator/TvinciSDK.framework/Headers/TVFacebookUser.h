//
//  TVFacebookUser.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/10/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"

@interface TVFacebookUser : BaseModelObject

@property (assign, nonatomic) TVFacebookStatus status;
@property (strong, nonatomic) NSString * siteGuid;
@property (strong, nonatomic) NSString * tvinciName;
@property (strong, nonatomic) NSString * facebookName;
@property (strong, nonatomic) NSURL * pictureURL;
@property (strong, nonatomic) NSString  * data;
@property (strong, nonatomic) NSString * minFriends;
@property (strong, nonatomic) NSDictionary * facebookUser;
@property (strong, nonatomic) NSString * token;











@end
