//
//  TVConfigurationManager.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/15/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModelObject.h"
#import "TVUniqueIDPovider.h"


typedef enum {
    TVConfigurationStateNotConfigured = 0,
    TVConfigurationStateLastKnownConfiguration,
    TVConfigurationStatePreInstalledConfiguration,
    TVConfigurationStateProperlyConfigured,
    TVConfigurationStateUnregistered
}TVConfigurationState;


@class TVInitObject;
@class TVPromotion;

@interface TVConfigurationManager : BaseModelObject

+(TVConfigurationManager *) sharedTVConfigurationManager;



@property (strong, nonatomic, readwrite) NSString * appIdentifier;
@property (strong, nonatomic, readwrite) NSString * appVersion;
@property (strong,readonly, nonatomic) NSString * internalVersion;


/*!
@abstract Tells the application what type of unique identifier to use.
@discussion Used it in first row in "didFinishLaunchingWithOptions"!!!
*/
-(void)setUDIDType:(UDIDType)type; //Tells the application what type of unique identifier to use.

+(NSString *) getTVUDID; // return app unique id

@property (nonatomic, assign, readonly) TVConfigurationState state;

/*!
 @abstract Contains additional information from the configuration file.
 @discussion This property allows the server to send addtional configuration information
 in the init.js file, and future client to use that data without the need to update the TVinci Library.
 */
@property (strong ,readonly) NSDictionary *configuration;


/*!
 @abstract init Object sub configuration json.
 */
@property (nonatomic, copy) NSArray *theInitObjArray;


/*!
 @abstract The logo URL of the current client.
 */
@property (strong , readonly) NSURL *logoURL;

/*!
 @abstract The URL for the JSON REST API
 */
@property (strong , atomic) NSURL *gatewayURL;


/*!
 @abstract The Version number of the API
 */
@property (atomic) float apiVersion;

/*!
 @abstract The default initObj object
 */
@property (nonatomic ,strong) TVInitObject *defaultInitObject;

/*!
 @abstract Specify whether or not the user can browse the content while not logged in
 */
@property (assign, readonly) BOOL allowBrowseMode;

/*!
 @abstract Used to get the main menu by calling GetMenu from the server
 */
@property (assign, readonly) NSInteger mainMenuID;

/*!
 @abstract Used to get the main menu by calling GetMenu from the server
 */
@property (assign, readonly) NSInteger mainCategoryID;

/*!
 @abstract This dictionary will contain the different file formats available
 */
@property (strong, readonly) NSDictionary *fileFormatNames;

/*!
 @abstract Used for Facebook login
 */
@property (strong, readonly) NSURL *facebookLoginURL;

-(NSURL *) facebookURLWithAction : (NSString *) action siteGUID : (NSString *) siteGUID;


+(BOOL) urlStringHasFacebookLoginBaseURL:(NSString *)urlStr;

/*!
 @abstract Token validity token
 */
@property (strong, readonly) NSString *configToken;


/*!
 @abstract Contains the client-specific media type IDs
 */
@property (strong, readonly) NSDictionary *mediaTypes;


/*!
 @abstract Contains the client-specific prepaid active state
 */
@property (readwrite, assign) BOOL prepaidActive;

/*!
 @abstract Discretix Personalization URL Server
 */
@property (strong, readonly) NSURL *DxPersonlizationUrl;

@property (strong, nonatomic) NSString * dmsBaseURL;

@property (strong, nonatomic) NSString * tvUserName;
@property (strong, nonatomic) NSString * tvAppPassword;

/*!
 @abstract Operations URL, 
 */
@property (strong, readonly) NSArray *OperatorsArr;

/*!
 @abstract Server License URL for WideVine playback,
 */

@property (strong, readonly) NSURL *LicenseServerUrl;



@property (strong, nonatomic, readonly) NSString *WVPortalID;

@property (strong, nonatomic, readonly) NSURL *WVProxyUrl;


/**
    @abstract displaying version information
    appname = bundle identifer 
    clientversion = app version
    isforceupdate = is the application is not valid in this version
    platfrom = iOS / Android
 }
 
 */
@property (strong, nonatomic) NSDictionary * dictionaryVersionInformation;


/**
 *  @abstract if the version is expired and we need to link the user for download update for the application here is the links :
 */
@property (strong, nonatomic) NSDictionary * dictionaryForceDownloadLinks;
@property (strong, nonatomic) NSString * menuPlatformID;
@property (strong, nonatomic) NSString * defaultIPNOID;
@property (strong, nonatomic) NSString * groupID;
@property (assign, nonatomic) BOOL loginBlockedBySuspend;

/**
 *  When we want to preced the refresh token expiration;
 */
@property (assign, nonatomic) NSTimeInterval refreshTokenSafetyMarginInSeconds;

/**
 *  When we want to preced the access token expiration;
 */
@property (assign, nonatomic) NSTimeInterval accessTokenSafetyMarginInSeconds;


-(NSDictionary *) parsArrayOfPairsToOneDictionary:(NSArray *) array;


-(void) downloadConfigurationFile;
-(id) objectForKey : (NSString *) key;
-(BOOL) checkConfigLifeCycle;
@end




extern NSString *const TVConfigurationManagerDidLoadConfigurationNotification;
extern NSString *const TVConfigurationManagerDidFailToLoadConfigurationNotification;
extern NSString *const TVConfigurationManagerReadyToLoadConsistantData;
extern NSString *const TVConfigurationManagerNeedsForceUpdate;

extern NSString *const TVConfigLogoURLKey;
extern NSString *const TVConfigGatewayURLKey;
extern NSString *const TVConfigMainMenuIDKey;
extern NSString *const TVConfigSmallPictureSizeKey;
extern NSString *const TVConfigMediumPictureSizeKey;
extern NSString *const TVConfigLargePictureSizeKey;
extern NSString *const TVConfigHighDefinitionKey;
extern NSString *const DxPers_URLKey;
extern NSString *const LicenseServer_URLKey;

extern NSString *const TVConfigStandatdDefinitionKey;
extern NSString *const TVConfigInitObjectKey;
extern NSString *const TVConfigInitObjectPostKey;
extern NSString *const TVConfigAllowBrowseModeKey;
extern NSString *const TVConfigFacebookURLKey;

extern NSString *const TVConfigFileFormatsKey;
extern NSString *const TVConfigMainFileFormatKey;
extern NSString *const TVConfigWidevineFileFormatKey;
extern NSString *const TVConfigWidevineFileFormatKeyForIPad;
extern NSString *const TVConfigWidevineFileFormatKeyForIPhone;
extern NSString *const TVConfigTrailerFileFormatKey;
extern NSString *const TVConfigSubtitlesFileFormatKey;

extern NSString *const TVConfigPrepaidActiveKey;
  
extern NSString *const TVMediaTypeAny;
extern NSString *const TVMediaTypeMovie;
extern NSString *const TVMediaTypeEpisode;
extern NSString *const TVMediaTypeSeries;
extern NSString *const TVMediaTypePerson;
extern NSString *const TVMediaTypePackage;
extern NSString *const TVMediaTypePrepaid;
extern NSString *const TVMediaTypeLinear;
extern NSString *const TVMediaTypeLive;
extern NSString *const TVMediaTypeSports;
extern NSString *const TVMediaTypeMusic;
extern NSString *const TVMediaTypeKaraoke;
extern NSString *const TVMediaTypeTest;
extern NSString *const TVMediaTypeCast;
    
extern NSString *const TVFacebookLoginStatusKey;
extern NSString *const TVFacebookLoginStatusError;
extern NSString *const TVFacebookLoginStatusOK;
extern NSString *const TVFacebookLoginStatusMergeOK;
extern NSString *const TVFacebookLoginStatusNewUser;
extern NSString *const TVFacebookLoginStatusMerge;
extern NSString *const TVFacebookLoginStatusMinFriends;
extern NSString *const TVFacebookLoginStatusUserNotExist;

extern NSString *const TVFacebookLoginActionRegister;
extern NSString *const TVFacebookLoginActionLogout;
extern NSString *const TVFacebookLoginActionGetData;

extern NSString *const TVConfigWVPortalID;
extern NSString *const TVConfigWVProxyURL;

extern NSString *const TVconfigForcedUpdateURLKey;
extern NSString *const TVconfigForcedUpdateURLiPhoneKey;
extern NSString *const TVconfigForcedUpdateURLiPadKey;
extern NSString *const TVconfigVersionInformationKey;
extern NSString *const TVconfigVersionInformationIsforceupdateKey;
extern NSString *const TVconfigMenuPlatfromKey;

extern NSString *const TVAccessTokenSafetyMarginInSecondsKey;
extern NSString *const TVRefreshTokenSafetyMarginInSecondsKey;
