//
//  TVGenericRulesResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVBaseResponse.h"

@interface TVGenericRulesResponse : TVBaseResponse
@property (strong, nonatomic) NSArray * arrayRules;
@end
