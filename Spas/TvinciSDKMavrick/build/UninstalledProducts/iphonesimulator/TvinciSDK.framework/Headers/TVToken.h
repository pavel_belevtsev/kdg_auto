//
//  TVAccessTokenData.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/26/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVToken : BaseModelObject

@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSDate * expiration;

+ (instancetype) tokenWithToken:(NSString *) token expiration:(NSTimeInterval) timeIntervalSince1970;
-(void)setAttributesFromString:(NSString *) string;
@end
