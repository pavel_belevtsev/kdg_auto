//
//  TVDeviceFamily.h
//  TVinci
//
//  Created by Avraham Shukron on 5/31/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVDeviceFamily : BaseModelObject
@property (nonatomic , copy) NSString *familyName;
@property (nonatomic , assign) NSInteger familyID;
@property (nonatomic , assign) NSInteger concurrentLimit;
@property (nonatomic , assign) NSInteger maximumNumberOfDevices;
@property (nonatomic , strong) NSArray *devices;
@end
