//
//  TVFullUserSocialAction.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/17/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//



#import "BaseModelObject.h"
#import "TVConstants.h"
#import "TVUser.h"
#import "TVUserSocialAction.h"



@interface TVFullUserSocialAction : BaseModelObject
@property (strong, nonatomic) TVUserSocialAction * userSocialAction;
@property (strong, nonatomic) TVUser * user;


/**
 *	Can be TVMediaItem or TVEPGProgram
 */
@property (strong, nonatomic) id asset;
@property (assign, nonatomic) TVAssetType assetType;


-(id) initWithUserSocialAction:(TVUserSocialAction *) userSocialAction user:(TVUser *) user asset:(id) asset;

@end
