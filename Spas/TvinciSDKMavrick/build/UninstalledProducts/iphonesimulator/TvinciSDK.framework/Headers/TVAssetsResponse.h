//
//  TVAssetsResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>


@class TVResponseStatus;

@interface TVAssetsResponse : BaseModelObject

@property (strong, nonatomic) NSArray * arrayAssets;
@property (assign, nonatomic) NSUInteger totalItems;
@property (strong, nonatomic) TVResponseStatus * responseStatus;

@end
