//
//  APSTVProgram.h
//  Spas
//
//  Created by Rivka Peleg on 9/29/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "APSBaseManagedObject.h"

@class APSTVChannel;

@interface APSTVProgram : APSBaseManagedObject

@property (nonatomic, strong) NSDate * createDate;
@property (nonatomic, strong) NSDate * endDateTime;
@property (nonatomic, strong) NSString * epgChannelID;
@property (nonatomic, strong) NSString * epgId;
@property (nonatomic, strong) NSString * epgIdentifier;
@property (nonatomic, strong) NSString * epgTag;
@property (nonatomic, strong) NSString * groupID;
@property (nonatomic, strong) NSString * isActive;
@property (nonatomic, strong) NSNumber * likeConter;
@property (nonatomic, strong) NSString * mediaID;
@property (nonatomic, strong) NSDictionary  * metaData;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * pictureURL;
@property (nonatomic, strong) NSString * programDescription;
@property (nonatomic, strong) NSDate * publishDate;
@property (nonatomic, strong) NSDate * startDateTime;
@property (nonatomic, strong) NSString * status;
@property (nonatomic, strong) NSDictionary * tags;
@property (nonatomic, strong) NSDate * updateDate;
@property (nonatomic, strong) NSString * updaterID;
@property (nonatomic, strong) NSDate * insertionDate;
@property (nonatomic, strong) APSTVChannel *channel;

@end
