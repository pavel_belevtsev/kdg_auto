//
//  AnalyticsAbstractEvent.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 12/25/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVAnalyticsEvent : NSObject


@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * category;

@end
