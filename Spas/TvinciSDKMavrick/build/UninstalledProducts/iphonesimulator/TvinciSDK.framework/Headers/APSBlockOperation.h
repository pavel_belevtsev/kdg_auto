//
//  APSBlockOperation.h
//  Spas
//
//  Created by Rivka Peleg on 10/1/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVPAPIRequest.h"

@interface APSBlockOperation : NSBlockOperation


@property (strong, nonatomic) TVPAPIRequest * request;
@property (strong, nonatomic) id context;


@end
