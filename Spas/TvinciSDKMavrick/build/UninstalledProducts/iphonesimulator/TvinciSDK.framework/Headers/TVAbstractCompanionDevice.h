//
//  TVNAbstractCompanionDevice.h
//  Tvinci
//
//  Created by Sagi Antebi on 8/24/14.
//  Copyright (c) 2014 Sergata Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVAbstractCompanionDevice : NSObject


@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* baseAddress;
@property (nonatomic, strong) NSString* uid;

- (int) getType;

@end
