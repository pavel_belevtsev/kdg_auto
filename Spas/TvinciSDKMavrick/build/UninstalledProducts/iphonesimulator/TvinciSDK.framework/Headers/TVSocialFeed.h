//
//  TVSocialFeed.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/31/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVSocialFeed : BaseModelObject {
    
}


@property (strong,readonly) NSArray *facebookCommentsArr;
@property (strong,readonly) NSArray *twitterCommentsArr;
@property (strong,readonly) NSArray *inAppCommentsArr;


@end
