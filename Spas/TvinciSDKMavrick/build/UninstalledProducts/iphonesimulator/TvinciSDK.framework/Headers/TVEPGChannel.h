//
//  TVEPGChannel.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVEPGChannel : BaseModelObject <NSCoding>
@property (nonatomic, copy) NSString *EPGChannelID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *channelDescription;
@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, copy) NSString *isActive;
@property (nonatomic, strong) NSURL *pictureURL;
@property (nonatomic, copy) NSString *groupID;
@property (nonatomic, copy) NSString *editorRemarks;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *updaterID;
@property (nonatomic, strong) NSDate *createDate;
@property (nonatomic, strong) NSDate *publishDate;
@property (nonatomic, copy) NSString *channelID;
@property (nonatomic, copy) NSString *mediaID;
@end

extern NSString *const EPGChannelEPGChannelIDKey;
extern NSString *const EPGChannelNameKey;
extern NSString *const EPGChannelDescriptionKey;
extern NSString *const EPGChannelOrderNumKey;
extern NSString *const EPGChannelIsActiveKey;
extern NSString *const EPGChannelPicURLKey;
extern NSString *const EPGChannelGroupIDKey;
extern NSString *const EPGChannelEditorRemarksKey;
extern NSString *const EPGChannelStatusKey;
extern NSString *const EPGChannelUpdaterIDKey;
extern NSString *const EPGChannelCreateDateKey;
extern NSString *const EPGChannelPublishDateKey;
extern NSString *const EPGChannelChannelIDKey;
extern NSString *const EPGChannelMediaIDKey;


