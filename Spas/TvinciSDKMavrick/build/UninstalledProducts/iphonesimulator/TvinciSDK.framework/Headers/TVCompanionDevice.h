//
//  TVCompanionDevice.h
//  TvinciSDK
//
//  Created by Tarek Issa on 3/18/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

//#import "TvinciSDK.h"
#import "TvinciSDK.h"


@interface TVCompanionDevice : BaseModelObject {
    
}



- (instancetype)initWithString:(NSString *)string;
- (void) downloadXmlDataWithCompletionBlock : (void (^)(NSError* error)) completion;
- (void)changeFriendlyNameTo:(NSString *)newName;

@property (strong,readonly) NSString *friendlyName;
@property (strong,readonly) NSString *basUrlString;
@property (strong,readonly) NSString *CacheControl;
@property (strong,readonly) NSString *userAgent;
@property (strong,readonly) NSString *ipAddress;
@property (strong,readonly) NSString *server;
@property (strong,readonly) NSString *port;
@property (strong,readwrite) NSString *udid;
//@property (strong,readonly) NSString *udid_serverFormat;
@property (strong,readonly) NSString *USN;
@property (strong,readonly) NSString *ext;
@property (strong,readonly) NSString *OPT;
@property (strong,readonly) NSString *NLS;
@property (strong,readonly) NSString *ST;
@property (strong,readonly) NSDate *date;
@property (strong,readonly) NSURL *location;


@end
