//
//  TVBaseResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "BaseModelObject.h"

@class  TVResponseStatus;

@interface TVBaseResponse : BaseModelObject
@property (strong,nonatomic) TVResponseStatus * responseStatus;
@end
