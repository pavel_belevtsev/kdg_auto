//
//  TVDomain.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 5/10/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVDomain : BaseModelObject
@property (nonatomic , copy) NSString *domainName;
@property (nonatomic , copy) NSString *domainDescription;
@property (nonatomic , assign) NSInteger domainID;
@property (nonatomic , assign) NSInteger groupID;
@property (nonatomic , assign) NSInteger limit;
@property (nonatomic , assign) NSInteger domainStatus;
@property (nonatomic , assign) BOOL isActive;
@property (nonatomic , strong) NSArray *userIDs;
@property (nonatomic , strong) NSArray *deviceFamilies;
@property (nonatomic , strong) NSArray *masterGUIDs;
@property (nonatomic , assign) NSInteger status;

@property (nonatomic , assign) NSInteger deviceLimit;   
@property (nonatomic , assign) NSInteger userLimit;
@property (nonatomic , assign) NSInteger concurrentLimit;
@property (nonatomic , assign) NSInteger frequencyFlag;
@property (nonatomic , assign) NSInteger domainRestriction;
@property (nonatomic , assign) NSInteger ssoOperatorID;
@property (nonatomic , strong) NSDate *nextActionFreq;
@property (nonatomic , strong) NSDate *nextUserActionFreq;
@property (nonatomic , strong) NSArray *pendingUsersIDs;
@property (nonatomic , strong) NSArray *defaultUsersIDs;

@property (nonatomic, strong) NSArray * homeNetworks;
@property (nonatomic ,assign) NSInteger coGuid;

@end

extern NSString *const TVDomainKey;