//
//  LanguageOptionDelegate.h
//  TvinciSDK
//
//  Created by quickode on 11/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Subtitle.h"



@protocol LanguageOptionDelegate <NSObject>
-(void) LanguageOptionChosenSubtitle: (NSString *) subtitleLanguage Dubbing:(NSString *) dubbingLanguage ;
@end
