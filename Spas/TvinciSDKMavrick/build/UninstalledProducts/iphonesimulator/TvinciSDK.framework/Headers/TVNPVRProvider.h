//
//  TVNPVRProvider.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/5/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import "TVConstants.h"


@class TVNPVRRecordItem;
@class TVPAPIRequest;

@interface TVNPVRProvider : NSObject

@property (assign, nonatomic) TVRecordOrderBy orderByType;

-(void) recoredsForPageSize:(NSInteger ) pageSize
                  pageIndex:(NSInteger) pageIndex
                    orderBy:(TVRecordOrderBy)orderBy
             withStartBlock:(void(^)(void)) startBlock
                failedBlock:(void(^)(void)) failedBlock
            completionBlock:(void(^)(NSArray * records)) completionBlock;


-(TVPAPIRequest *) requestForIndex:(NSInteger)index;

@end
