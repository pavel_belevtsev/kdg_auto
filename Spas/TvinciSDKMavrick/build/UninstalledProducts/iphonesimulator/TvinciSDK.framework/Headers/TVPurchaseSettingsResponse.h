//
//  TVPurchaseSettingsResponse.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import "TVBaseResponse.h"
#import "TVConstants.h"

@interface TVPurchaseSettingsResponse : TVBaseResponse
@property (assign, nonatomic) TVPurchasePermission purchasePermission;
@property (assign, nonatomic) TVRuleDefinedLevel level;
@end
