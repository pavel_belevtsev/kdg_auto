//
//  TVNPVRItem.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVNPVRRecordItem : BaseModelObject

@property (strong, nonatomic) NSString * channelnName;
@property (assign, nonatomic) BOOL isAssetProtected;
@property (strong, nonatomic) NSString * recordSource;
@property (strong, nonatomic) NSString * recordingID;
@property (strong, nonatomic) NSDate * createDate;
@property (strong, nonatomic) NSString * recordDescription;
@property (strong, nonatomic) NSDate * endDate;
@property (strong, nonatomic) NSString * epgChannelID;
@property (assign, nonatomic) NSInteger epgID;
@property (strong, nonatomic) NSString * epgIdentifer;
@property (strong, nonatomic) NSDictionary * epgMeta;
@property (strong, nonatomic) NSDictionary * epgTags;
@property (strong, nonatomic) NSString * groupID;
@property (assign, nonatomic) BOOL isActive;
@property (assign, nonatomic) NSInteger likeCount;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSURL * picURL;
@property (strong, nonatomic) NSDate * publishDate;
@property (strong, nonatomic) NSDate * startDate;
@property (assign, nonatomic) TVRecordingStatus recordingStatus;
@property (strong, nonatomic) NSString * updaterID;
@property (strong, nonatomic) NSDate * updateDate;
@property (strong, nonatomic) NSString * mediaID;





@end
