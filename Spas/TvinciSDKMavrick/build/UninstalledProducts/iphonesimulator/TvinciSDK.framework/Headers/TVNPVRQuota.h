//
//  TVNPVRQuota.h
//  TvinciSDK
//
//  Created by Rivka Peleg on 1/11/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVNPVRQuota : BaseModelObject

@property (assign, nonatomic) NSTimeInterval totalQuota;
@property (assign, nonatomic) NSTimeInterval occupiedQuota;
@property (assign, nonatomic) TVNPVRResponseStatus responseStatus;
@property (strong, nonatomic) NSString * message;
@end
