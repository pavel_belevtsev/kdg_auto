//
//  TVWatchListItem.h
//  TvinciSDK
//
//  Created by Tarek Issa on 12/16/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"





@interface TVWatchListItem : BaseModelObject {
    
}


@property (strong, readonly) NSString *siteGuid;
@property (strong, readonly) NSArray *items;
@property (assign, readonly) TVListItemType itemType;
@property (assign, readonly) TVListType listType;


@end


@interface ItemObj : BaseModelObject {
    
}

@property (assign, nonatomic) int order;
@property (strong, nonatomic) NSString* itemID;

@end
