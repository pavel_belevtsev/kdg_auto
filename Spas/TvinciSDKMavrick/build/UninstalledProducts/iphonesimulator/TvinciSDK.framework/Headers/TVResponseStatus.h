//
//  TVResponseStatus.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/28/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVResponseStatus : BaseModelObject

@property (assign, nonatomic) TVResponseStatusCode code;
@property (strong, nonatomic) NSString * message;
@end
