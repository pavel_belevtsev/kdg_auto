//
//  TVPromotion.h
//  TvinciSDK
//
//  Created by Tarek Issa on 12/12/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"

@interface TVPromotion : BaseModelObject {
    
}

/*!
 @abstract Promotion URL.
 */
@property (strong, readonly) NSURL *    PromotionURL;
@property (strong, readonly) NSString * PromotionText;

@end
