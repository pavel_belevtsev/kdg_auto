//
//  DataRefresh.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 4/16/15.
//  Copyright (c) 2015 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVDataRefreshTask.h"

@interface TVDataRefreshHandler : NSObject
/**
 *  Singelton mthod
 *
 *  @return The shared class object
 */
+ (instancetype) sharedInstance;

/**
 *  This method triggering interation for all tasks and perform all tasks that overdue the data to refresh.
 *
 *  @param startBlock      called when the starting this process
 *  @param completionBlock called when the last task is completed.
 *
 *  The recommented location to call this method can be:
 *  1) -(void)applicationDidBecomeActive:(UIApplication *)application
 *  2) -(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
 *  3) Triggered by timer every x second.
 *
 */
-(void) executeRefreshTasksWithStartBlock:(void(^)(void)) startBlock compeltionBlock:(void(^)(TVDataRefreshTaskResult result)) completionBlock;

/**
 *  This method adding a data refresh task that will be handled by this class.
 *
 *  @param refreshTimePeriod Valid data time the task will be ready to execute, and the data will be refreshed. after each execution new period is start up.
 *  @param startFromNow      Is the task will run right after the next trigger , or should wait to the time period to complete.
 *  @param offset      the start offset the task will start at and then by cycle of period time.
 *  @param refreshBlock      The task block itself.

 */


-(void) addDataRefreshTaskWithRefreshPeriod:(NSTimeInterval) refreshTimePeriod offset:(NSTimeInterval) offset target:(id)target withRefreshBlock:(void(^)( void(^)(TVDataRefreshTaskResult)  )) refreshBlock;

/**
 *  This method remove all tasks of spesific target for removing tasks before target is freed.
 *
 *  @param target
 */
-(void) removeAllTasksForTarget:(id) target;
@end
