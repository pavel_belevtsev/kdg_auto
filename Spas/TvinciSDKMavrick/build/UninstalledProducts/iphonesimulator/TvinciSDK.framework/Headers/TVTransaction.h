//
//  TVTransaction.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/23/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"

#define kTransactionItemTypesArray [NSArray arrayWithObjects:NSLocalizedString(@"Unknown", nil),NSLocalizedString(@"PPV", nil),NSLocalizedString(@"Subscription", nil), NSLocalizedString(@"PrePaid", nil), NSLocalizedString(@"PrePaidExpired", nil), nil]


@interface TVTransaction : BaseModelObject
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, copy) NSString *currencyCode;
@property (nonatomic, copy) NSString *currencySign;
@property (nonatomic, assign) NSInteger currencyID;
@property (nonatomic, copy) NSString *receiptCode;
@property (nonatomic, copy) NSString *purchasedItemName;
@property (nonatomic, copy) NSString *purchasedItemCode;
@property (nonatomic, assign) TVBillType itemType;
@property (nonatomic, assign) NSInteger billingAction;
@property (nonatomic, strong) NSDate *actionDate;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, assign) NSInteger paymentMethod;
@property (nonatomic, copy) NSString *paymentExtraDetails;
@property (nonatomic, assign) BOOL isRecurring;
@property (nonatomic, assign) NSInteger billingProviderReference;
@property (nonatomic, assign) NSInteger purchaseID;
@property (nonatomic, copy) NSString *remarks;
@end

extern NSString *const TVTransactionPriceKey;
extern NSString *const TVTransactionActualPriceKey;
extern NSString *const TVTransactionCurrencyKey;
extern NSString *const TVTransactionCurrencyCodeKey;
extern NSString *const TVTransactionCurrencySignKey;
extern NSString *const TVTransactionCurrencyIDKey;
extern NSString *const TVTransactionIsRecurringHistoryKey;
extern NSString *const TVTransactionActionDateKey;
extern NSString *const TVTransactionEndDateKey;
extern NSString *const TVTransactionStartDateKey;
extern NSString *const TVTransactionBillingActionKey;
extern NSString *const TVTransactionItemTypeKey;
extern NSString *const TVTransactionPaymentMethodKey;
extern NSString *const TVTransactionBillingProviderReferenceKey;
extern NSString *const TVTransactionPurchaseIDKey;
extern NSString *const TVTransactionPaymentMethodExtraDetailKey;
extern NSString *const TVTransactionPurchasedItemCodeKey;
extern NSString *const TVTransactionPurchasedItemNameKey;
extern NSString *const TVTransactionRecieptCodeKey;
extern NSString *const TVTransactionRemarksKey;