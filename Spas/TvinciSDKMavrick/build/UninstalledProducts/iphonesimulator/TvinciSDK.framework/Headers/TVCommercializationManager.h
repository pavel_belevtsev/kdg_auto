//
//  TVComercializationManager.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 5/19/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVConstants.h"
#import "TVBaseNetworkObject.h"


#define NotificationNameCommercializationStatusChanged @"NotificationNameCommercializationStatusChanged"

@interface TVCommercializationManager : TVBaseNetworkObject

+ (id)  sharedInstance;
-(void) startTrakingForServicesWithCompletion:(void(^)()) completionBlock;
-(BOOL) isServiceSupported:(TVCommercializationServiceType) serviceType;


@end
