//
//  TVAssetImage.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/1/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>



@interface TVAssetImage : BaseModelObject


@property (assign, nonatomic) CGFloat ratio;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat height;
@property (strong, nonatomic) NSURL * url;


@end
