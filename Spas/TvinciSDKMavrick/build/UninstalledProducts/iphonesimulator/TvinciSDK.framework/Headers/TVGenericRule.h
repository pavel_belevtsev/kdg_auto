//
//  TVGenericRule.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 7/20/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

@interface TVGenericRule : BaseModelObject


@property (assign, nonatomic) NSInteger ruleId;
@property (strong, nonatomic) NSString * ruleName;
@property (strong, nonatomic) NSString * ruleDescription;
@property (assign, nonatomic) TVRuleType ruleType;
@end
