//
//  TVNotification.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVConstants.h"
#import "BaseModelObject.h"

@interface TVNotification : BaseModelObject

@property (strong, nonatomic) NSString * notificationID;
@property (strong, nonatomic) NSString * notificationUniqID;
@property (strong, nonatomic) NSString * notificationRequestID;
@property (strong, nonatomic) NSString * notificationMessageID;
@property (strong, nonatomic) NSString * userID;
@property (assign, nonatomic) TVNotificationViewStatus notificationViewStatus;
@property (assign, nonatomic) TVNotificationType notificationType;
@property (strong, nonatomic) NSString * appName;
@property (strong, nonatomic) NSString * udid;
@property (strong, nonatomic) NSString * deviceID;
@property (strong, nonatomic) NSString * notificationMessage;
@property (strong, nonatomic) NSString * notificationTitle;
@property (strong, nonatomic) NSDate * notificationPublishDate;
@property (strong, nonatomic) NSDictionary * notificationTags;
@property (strong, nonatomic) NSString * mediaID;
@property (strong, nonatomic) NSURL * pictureURL;
@property (strong, nonatomic) NSString  * templateEmail;










@end
