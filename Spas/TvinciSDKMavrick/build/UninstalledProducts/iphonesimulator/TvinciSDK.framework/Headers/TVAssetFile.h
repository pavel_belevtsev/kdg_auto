//
//  TVAssetFile.h
//  TvinciSDK
//
//  Created by Rivka Schwartz on 6/2/15.
//  Copyright (c) 2015 Kaltura. All rights reserved.
//

#import <TvinciSDK/TvinciSDK.h>

//{
//    "id": 349383,
//    "type": "Main",
//    "url": " vod/s/felucia/201409W-A/MC_SD_PHUA_CHU_KANG_PTE_LTD_SERIES2_EP28_PC_SS.ism/Manifest"
//}

@interface TVAssetFile : BaseModelObject
@property (strong, nonatomic) NSString * fileID;
@property (strong, nonatomic) NSString * fileType;
@property (strong, nonatomic) NSURL * fileURL;


@end
