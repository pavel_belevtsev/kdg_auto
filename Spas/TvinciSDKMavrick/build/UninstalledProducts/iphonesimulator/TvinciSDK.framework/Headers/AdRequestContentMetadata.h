//
//  AdRequestContentMetadata.h
//  TvinciSDK
//
//  Created by iosdev1 on 6/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "TVConstants.h"

typedef enum{
    ShortMovie = 1,
    LongMovie = 2
}ContentForm;



@interface AdRequestContentMetadata : BaseModelObject
@property (nonatomic, strong) NSString * category;
@property (nonatomic, assign) ContentForm contentForm;
@property (nonatomic, strong) NSString *contentIdentifier;
@property (nonatomic, strong) NSString *contentPartner;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, strong) NSArray *flags; //NSString[]
@property (nonatomic, strong) NSArray *tags ; //NSString[]
@property (nonatomic, assign) NSTimeInterval timeToShow;
@property (nonatomic, assign) AdType adType;

@end
