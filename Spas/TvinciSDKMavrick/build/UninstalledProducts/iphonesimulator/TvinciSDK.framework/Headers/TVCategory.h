//
//  TVCategory.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 8/26/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//


#import "BaseModelObject.h"

@interface TVCategory : BaseModelObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, strong) NSArray *subCategories;
@property (nonatomic, strong) NSString *pictureURL;
@property (nonatomic, strong) NSString *coGuid;

@end

//  
extern NSString *const TVCategoryTitleKey;
extern NSString *const TVCategoryIDKey;
extern NSString *const TVCategoryChannelsKey;
extern NSString *const TVCategoryInnerCategoriesKey;
extern NSString *const TVCategoryImageKey;
extern NSString *const TVCategoryCoGuid;