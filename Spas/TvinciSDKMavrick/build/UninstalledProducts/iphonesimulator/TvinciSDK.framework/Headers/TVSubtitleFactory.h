//
//  TVSubtitleFactory.h
//  SubtitleDemo
//
//  Created by Alexander Israel on 11/14/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CMTime.h>
#import <AVFoundation/AVTime.h>
#import <AVFoundation/AVFoundation.h>

typedef enum {
    SubRipScanPositionArrayIndex,
    SubRipScanPositionTimes,
    SubRipScanPositionText
} SubRipScanPosition;

@interface SubRipItem : NSObject < NSCoding > {
    CMTime startTime;
    CMTime endTime;
    NSString *text;
    NSString *uniqueID;
    NSInteger index;
}

@property(assign) CMTime startTime;
@property(assign) CMTime endTime;
@property(copy) NSString *text;
@property(assign) NSInteger index;

@property(readonly, getter = startTimeString) NSString *startTimeString;
@property(readonly, getter = endTimeString) NSString *endTimeString;
@property(readonly) NSString *uniqueID;

-(NSString *)startTimeString;
-(NSString *)endTimeString;

-(NSString *)_convertCMTimeToString:(CMTime)theTime;

-(NSString *)description;

-(NSInteger)startTimeInSeconds;
-(NSInteger)endTimeInSeconds;

-(BOOL)containsString:(NSString *)str;

-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)initWithCoder:(NSCoder *)decoder;

@end


@protocol TVSubtitleFactoryDelegate <NSObject>
-(void) updateVisibleSubtitles;
@end

@interface TVSubtitleFactory : NSObject
{
    
    id timeObserver;
    dispatch_source_t _timer;
    
}
@property (assign, nonatomic) NSObject<TVSubtitleFactoryDelegate>*delegate;
@property (assign) NSInteger currentSubtitleIndex;
@property (strong, nonatomic) NSMutableArray *subtitleItems;

-(void)populateFromJson:(NSString *)json delay:(NSInteger)delay;

-(SubRipItem*)indexOfSubRipItemWithStartTime:(CMTime)theTime;
-(SubRipItem*)indexOfSubRipItemAfterSeekWithStartTime:(CMTime)theTime;

/*-(void) startVideoTimer:(AVPlayer*)player;
 -(void) stopVideoTimer:(AVPlayer*)player;*/

-(void) startVideoTimer;
-(void) stopVideoTimer;
-(void) pauseVideoTimer;
-(void) resumeVideoTimer;

@end
