//
//  TVMediaItem.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/2/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "BaseModelObject.h"
#import "TVConstants.h"
#import "TVPictureSize.h"
#import "TVFile.h"
#import "AdRequestContentContainer.h"

typedef enum  {
    kMediaItemRequestType_Default = 0,
    kMediaItemRequestType_GetMediaInfoWithMediaID,
}kMediaItemRequestType;

typedef enum  {
    kMediaType_Episode = 1,
    kMediaType_Movie=2,
     kMediaType_Series=3,
     kMediaType_Epg=4,
    kMediaType_test=5
}kMediaType;

@interface TVMediaItem : BaseModelObject
@property (nonatomic, copy , readonly) NSString *mediaID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mediaDescription;
@property (nonatomic, strong) NSDictionary *tags;
@property (nonatomic, strong) NSDictionary *metaData;
@property (nonatomic, copy , readonly) NSString *mediaTypeName;
@property (nonatomic, copy , readonly) NSString *mediaTypeID;
@property (nonatomic, assign) CGFloat rating;
@property (nonatomic, assign) NSInteger viewCounter;
@property (nonatomic, assign) NSInteger likeCounter;
@property (nonatomic, assign) NSInteger totalItems;
@property (nonatomic, strong) NSDate *creationDate;
@property (nonatomic, strong) NSDate *StartDate;
@property (nonatomic, strong) NSDate *catalogStartDate;
@property (nonatomic, copy) NSURL *pictureURL;
@property (nonatomic, copy) NSURL *mediaWebLink;
@property (nonatomic, assign) BOOL isMediaLike;
@property (nonatomic , strong) NSArray *files;
@property (nonatomic, strong) NSArray * subtitles;
@property(nonatomic, strong) NSArray *pictures;

@property (nonatomic ,strong, readonly) TVFile *trailerFile;
@property (nonatomic ,strong, readonly) TVFile * subtitleFile;
@property (nonatomic ,strong, readonly) TVFile *mainFile;
@property (nonatomic, strong, readonly) TVFile *widevineFile;

// Dynamic Info
@property (nonatomic ,assign) BOOL isFavorite;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, assign) NSTimeInterval mediaMark;
@property (nonatomic ,assign) TVPriceType priceType;
@property (nonatomic, assign) NSDate *expirationDate;

// Deprecated properties
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *fileID;

// Recognizing Data available
@property (readwrite, assign) kMediaItemRequestType mediaItemRequestType;

@property BOOL isLoadFavorite;

// Contain all Request to Ads company to get Ads.
@property (nonatomic ,strong) AdRequestContentContainer * adRequestContentContainer;
@property (nonatomic ,strong)  NSDictionary * advertisingParameters;

/*
"ExternalIDs":[
{
    "Key":"epg_id",
    "Value":"123456"
}
               ]
 */

// only for EPG
@property (nonatomic, strong) NSDictionary *ExternalIDs;
@property (nonatomic, strong) NSDate * EPGStartCurrectPrograme;
@property (nonatomic, strong) NSDate * EPGEndCurrectPrograme;
@property (nonatomic ,assign )NSInteger EPGDuration;


-(NSURL *) pictureURLForSize:(CGSize)size;
-(NSURL *) pictureURLForSizeWithRetinaSupport:(CGSize)size;
-(NSURL *) pictureURLForPictureSize : (TVPictureSize *) size;
-(void) setDynamicMediaInfoFromJSONDictionary : (NSDictionary *) dynamicMediaInfo;


-(BOOL) isMovie;
-(BOOL) isKaraoke;
-(BOOL) isEpisode;
-(BOOL) isSeries;
-(BOOL) isLive;
-(BOOL) isPackage;
-(BOOL) isTest;

-(BOOL) hasMainFile;
-(BOOL) hasTrailerFile;

-(BOOL) hasValidWidevineFileOrMainFile;
-(BOOL)  fileHasExtentionOfWidevine:(NSString *) fileName;

-(NSString *) sortKey;

-(NSNumber *) getMediaItemEpisodeNumber;
-(NSNumber *)getMediaItemSeasonNumber;

-(void)printMediaItem;

-(NSString *) epgChannelID;

-(TVFile *) fileForFormat:(NSString *) fileFormat;

@end

extern NSString *const EPGEpg_id;
extern NSString *const TVTagGenre;
extern NSString *const TVTagBusinessModel;
extern NSString *const TVTagSeriesName;
extern NSString *const TVTagAgeRating;
extern NSString *const TVTagMainCast;
extern NSString *const TVTagDirector;
extern NSString *const TVTagLanguage;
extern NSString *const TVTagSubtitleLanguage;
extern NSString *const TVTagAudioLanguage;
extern NSString *const TVMetaDataRatingAdvisories;
extern NSString *const TVMetaDataSeasonNumber;
extern NSString *const TVMetaDataEpisodeNumber;
extern NSString *const TVMetaDataClosedCaptionAvailable;
extern NSString *const TVMetaDataShortSummary;
extern NSString *const TVMetaDataShortTitle;
extern NSString *const TVMetaDataNumberOfSeasons;
extern NSString *const TVMetaDataReleaseYear;
extern NSString *const TVMetaDataReleaseDate;
extern NSString *const TVMediaItemBaseID;
extern NSString * const MediaItemTotalItemsKey;
extern NSString *const TVTagTerritory;
extern NSString *const TVTagCategory;
extern NSString *const TVTagProvider;
extern NSString *const TVTagProviderID;

extern NSString *const TVMediaItemAttributesDictionary;
extern NSString *const MediaItemIDKey;
extern NSString *const MediaItemCatalogStartDateKey;

extern NSString *const MediaItemTotalItemsKey ;
extern NSString *const MediaItemCatalogStartDateFormat;
extern NSString *const MediaItemNameKey ;
extern NSString *const MediaItemTypeIDKey;
extern NSString *const MediaItemTypeNameKey ;
extern NSString *const MediaItemRatingKey;
extern NSString *const MediaItemViewCounterKey ;
extern NSString *const MediaItemDescriptionKey ;
extern NSString *const MediaItemCreationDateKey ;
extern NSString *const MediaItemStartDateKey ;
extern NSString *const MediaItemPictureURLKey;
extern NSString *const MediaItemURLKey;
extern NSString *const MediaItemWebLinkKey ;
extern NSString *const MediaItemDurationKey ;
extern NSString *const MediaItemFileIDKey ;
extern NSString *const MediaItemDynamicDataKey ;
extern NSString *const MediaItemIsFavoriteKey ;
extern NSString *const MediaItemPriceKey ;
extern NSString *const MediaItemMarkKey ;
extern NSString *const MediaItemPriceTypeKey ;
extern NSString *const MediaItemNotificationKey ;
extern NSString *const MediaItemExpirationDateKey ;
extern NSString *const MediaItemSubDurationKey ;
extern NSString *const MediaItemSubFileFormatKey ;
extern NSString *const MediaItemSubFileIDKey;
extern NSString *const MediaItemSubURLKey ;
extern NSString *const MediaItemTagsKey ;
extern NSString *const MediaItemMetaDataKey;
extern NSString *const TVMediaItemFilesKey ;
extern NSString *const TVMediaItemLikeCounterKey ;
extern NSString *const TVMediaItemAdvertisingParametersKey ;
extern NSString *const MediaItemPicturesKey ;
extern NSString *const TVMediaItemTagTitleKey ;
extern NSString *const TVMediaItemTagValuesKey ;
extern NSString *const EPGEpg_id ;
extern NSString *const TVMediaItemExternalID ;

extern NSString *const TVMediaItemBaseID ;

