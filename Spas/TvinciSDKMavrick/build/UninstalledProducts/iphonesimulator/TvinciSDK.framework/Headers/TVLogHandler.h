//
//  QLogManager.h
//  QadabraSDK
//
//  Created by Nissim Pardo on 3/25/14.
//  Copyright (c) 2014 Marimedia LTD. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
	TVLogLevelAll		= 0,
	TVLogLevelTrace		= 10,
	TVLogLevelDebug		= 20,
	TVLogLevelInfo		= 30,
	TVLogLevelWarn		= 40,
	TVLogLevelError		= 50,
	TVLogLevelOff		= 60
} TVLogLevel;

// use the `QLogManager` methods to set the desired level of log filter
@interface TVLogHandler : NSObject

// gets the current log filter level
+ (TVLogLevel)getLogLevel;

// set the log filter level
+ (void)setLogLevel:(TVLogLevel)level;
@end
