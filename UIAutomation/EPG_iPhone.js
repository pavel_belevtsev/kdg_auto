
var target = UIATarget.localTarget();
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);

target.delay(10);

target.frontMostApp().mainWindow().buttons()["Button Menu"].tap();
target.delay(2);
target.tap({x:80.00, y:160.00});

target.delay(5);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("No EPG List");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
