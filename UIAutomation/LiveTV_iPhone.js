var target = UIATarget.localTarget();
target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);

target.delay(10);

target.frontMostApp().mainWindow().collectionViews()[0].cells()[0].tap();

target.delay(10);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("Movie Not Started");
    target.captureScreenWithName("screenshot");
} else  {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
