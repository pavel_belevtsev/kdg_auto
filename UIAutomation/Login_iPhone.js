
UIATarget.onAlert = function onAlert(alert) {
    var title = alert.name();
    UIALogger.logWarning("Alert with title ’" + title + "’ encountered!");
    
    if (title == "Möchten Sie sich wirklich abmelden?") {
        alert.buttons()["Abmelden"].tap();
        return true; 
    }
    
    return false;
}

var target = UIATarget.localTarget();

target.setDeviceOrientation(UIA_DEVICE_ORIENTATION_PORTRAIT);

target.delay(10);

var buttonMenu = target.frontMostApp().mainWindow().buttons()["Button Menu"];
buttonMenu.tap();
target.delay(2);

var buttonLogin = target.frontMostApp().mainWindow().buttons()["Button Login"];

if (!buttonLogin.isVisible()) {
    
    target.frontMostApp().mainWindow().buttons()["Button Settings"].tap();
    target.delay(2);

    target.frontMostApp().mainWindow().tableViews()[0].cells()["Abmelden"].tap();

    target.delay(5);
    
    target.frontMostApp().mainWindow().buttons()["Button Menu"].tap();
}
    
buttonLogin.tap();
    
target.delay(2);
    
target.frontMostApp().mainWindow().scrollViews()[0].textFields()["Text Field Username"].tap();
target.frontMostApp().keyboard().typeString("fut6\n");
    
target.frontMostApp().keyboard().typeString("Friendly_User_1");
    
target.frontMostApp().mainWindow().scrollViews()[0].buttons()["Button Login"].tap();
    
target.delay(10);
    

var buttonStart = target.frontMostApp().mainWindow().buttons()["TV APP STARTEN"];

if (!buttonStart.checkIsValid()) {
    UIALogger.logFail("Login Failure");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass("Login Success");
    target.captureScreenWithName("screenshot");
    
}

