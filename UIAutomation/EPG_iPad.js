
var target = UIATarget.localTarget();

target.delay(10);

target.frontMostApp().mainWindow().collectionViews()[0].cells()[1].tap();

target.delay(5);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];
                                                                                 
if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("No EPG List");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
