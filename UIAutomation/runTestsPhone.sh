#! /bin/sh

XCODE_PATH=`xcode-select -print-path`
TRACETEMPLATE=$1
BASE_TEST_FOLDER=$2
APP_LOCATION=$3
DEVICE_ID=$4
WORKSPACE=$5

if [ ! $# -gt 1 ]; then
	echo "You must specify the app location and the test file."
	echo "\t (optionally supply unique device ID of physical iOS device)"
	echo "\t eg. ./build.sh suite.js <xcodeproject directory>/build/Debug-iphonesimulator/myapp.app <device-udid>"
	exit -1
fi

# If running on device, only need name of app, full path not important
if [ ! "$DEVICE_ID" = "" ]; then
  RUN_ON_SPECIFIC_DEVICE_OPTION="-w $DEVICE_ID"
  APP_LOCATION=`basename $APP_LOCATION`
fi

FOLDER_REPORT=${WORKSPACE}/test-reports
FOLDER_SCREENSHOTS=${WORKSPACE}/screenshots

# Create junit reporting directory
if [ ! -d "$FOLDER_REPORT" ]; then
  mkdir $FOLDER_REPORT
fi

if [ ! -d "$FOLDER_SCREENSHOTS" ]; then
  mkdir $FOLDER_SCREENSHOTS
fi

   rm -rf $FOLDER_SCREENSHOTS/*

SCRIPTS=( "Login_iPhone" "Search_iPhone" "LiveTV_iPhone" "EPG_iPhone" )

echo "<?xml version='1.0' encoding='UTF-8'?>" > test-results.xml
echo "<testsuite tests=\"${#SCRIPTS[@]}\">" >> test-results.xml

for SCRIPT_NAME in "${SCRIPTS[@]}"
do
   
   rm -rf $FOLDER_REPORT/*

# Kick off the instruments build
instruments \
$RUN_ON_SPECIFIC_DEVICE_OPTION \
-t $TRACETEMPLATE \
$APP_LOCATION \
-e UIASCRIPT $BASE_TEST_FOLDER/$SCRIPT_NAME.js \
-e UIARESULTSPATH $FOLDER_REPORT 1>$FOLDER_REPORT/test-results.log

# cleanup the tracefiles produced from instruments
rm -rf *.trace

echo "    <testcase classname=\"Blink\" name=\"$SCRIPT_NAME\">" >> test-results.xml

if [ `grep "Pass:" $FOLDER_REPORT/test-results.log | wc -l` -gt 0 ]; then
	echo "        <success> Success </success>" >> test-results.xml
else
	if [ -f "$FOLDER_REPORT/Run 1/screenshot.png" ]; then
		cp "$FOLDER_REPORT/Run 1/screenshot.png" $FOLDER_SCREENSHOTS/$SCRIPT_NAME.png
	fi
	echo "        <failure> Failure </failure>" >> test-results.xml
	echo "        <system-out>" >> test-results.xml
	echo "[[ATTACHMENT|${FOLDER_SCREENSHOTS}/$SCRIPT_NAME.png]]" >> test-results.xml
	echo "        </system-out>" >> test-results.xml
fi

echo "    </testcase>" >> test-results.xml
   
done

echo "</testsuite>" >> test-results.xml

echo 'Build Passed'
exit 0
