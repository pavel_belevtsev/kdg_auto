
var target = UIATarget.localTarget();

target.delay(5);

target.frontMostApp().mainWindow().textFields()["Text Field Search"].tap();
target.frontMostApp().keyboard().typeString("you\n");

target.delay(10);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("No Items Found");
    target.captureScreenWithName("screenshot");
} else {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}
