var target = UIATarget.localTarget();

target.delay(10);

target.frontMostApp().mainWindow().collectionViews()[1].tapWithOptions({tapOffset:{x:0.2, y:0.2}});

target.delay(10);

var label = target.frontMostApp().mainWindow().staticTexts()["Label Automation"];

if ((label.value() == "") || !label.checkIsValid()) {
    UIALogger.logFail("Movie Not Started");
    target.captureScreenWithName("screenshot");
} else  {
    UIALogger.logPass(label.value());
    target.captureScreenWithName("screenshot");
}