//
//  YESConditionalAccessAdapter.h
//  YES_iPad
//
//  Created by Rivka S. Peleg on 1/21/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

@class TVMediaToPlayInfo;


typedef enum
{
    conditionalAccessFailureReason_NoFileToPlay,
    conditionalAccessFailureReason_NotPurchased,
    conditionalAccessFailureReason_parental_issue,
    conditionalAccessFailureReason_customdata_issue,
    conditionalAccessFailureReason_requestFailed,
    
}conditionalAccessFailureReason;


@interface GCConditionalAccessAdapter : APSBaseNetworkObject

-(void) startConditionalAcessProcessWithMediaToPlay:(TVMediaToPlayInfo *) mediaToPlay
                                       startedBlock:(void(^)(void)) started
                                    completionBlock:(void(^)(void)) completionBlock
                                        failedBlock:(void(^)(conditionalAccessFailureReason error)) failedBlock;
@end
