//
//  YESConditionalAccessAdapter.m
//  YES_iPad
//
//  Created by Rivka S. Peleg on 1/21/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "GCConditionalAccessAdapter.h"
#if DX == 1
#import <TVCPlayerStaticLibrary/TVMediaToPlayInfo.h>
#endif
#if WV ==1
#import <TVCPlayerWV/TVMediaToPlayInfo.h>
#endif




#define parental_raitng_key = @""

@interface GCConditionalAccessAdapter ()

@end

@implementation GCConditionalAccessAdapter


-(void) startConditionalAcessProcessWithMediaToPlay:(TVMediaToPlayInfo *) mediaToPlay
                                       startedBlock:(void(^)(void)) started
                                     completionBlock:(void(^)(void)) completionBlock
                                         failedBlock:(void(^)(conditionalAccessFailureReason error)) failedBlock
{
    
    [[self networkQueue] cleen];
    
    
    if (started)
    {
        started();
    }
    
    
    NSInteger mediaParentalRating = [[[mediaToPlay.mediaItem.tags objectForKey:@"Parental"] lastObject] integerValue];
   
    if (mediaParentalRating)
    {
        NSInteger userParenralRating = [self parentalValue];
        
        if (mediaParentalRating>userParenralRating)
        {
            if (failedBlock)
            {
                failedBlock(conditionalAccessFailureReason_parental_issue);
                return;
            }
        }
        
    }
    
    NSInteger mediaId = [[[mediaToPlay mediaItem] mediaID] integerValue];
    TVPAPIRequest * request = [TVPMediaAPI requestForIsItemPurchased:mediaId  delegate:nil];
    
    [request setCompletionBlock:^{
        if (completionBlock)
        {
            completionBlock();
        }
    }];
    
    
    [request setStartedBlock:^{
        
       
    }];
    
    [request setFailedBlock:^{
        
        if (failedBlock)
        {
            failedBlock(conditionalAccessFailureReason_requestFailed);
        }
    }];
    
    [request setCompletionBlock:^{
       
        NSNumber *isItemPurchased = [request JSONResponse];
        BOOL isPurchased = [isItemPurchased boolValue];
        if (isPurchased == NO)
        {
            if (failedBlock)
            {
                failedBlock(conditionalAccessFailureReason_NotPurchased);
            }
        }
        else
        {
            //            if (completionBlock)
            //            {
            //                completionBlock();
            //            }
            
            [self getMediaLicenseDataForMediaID:mediaToPlay startedBlock:started completionBlock:completionBlock failedBlock:failedBlock];
        }
    }];
    
    
    [self sendRequest:request];
}


-(NSInteger ) parentalValue
{
    NSArray * userDynamicDataParams = [[[[TVSessionManager sharedTVSessionManager] currentUser] dynamicData] objectForKey:@"m_sUserData"];
    
    for (NSDictionary * dict in userDynamicDataParams)
    {
        if ([[dict objectForKey:@"m_sDataType"] isEqualToString:@"parentalControl"])
        {
            return [[dict objectForKey:@"m_sValue"] integerValue];
        }
    }
    
    return NSIntegerMax;
}

// get custom data
- (void)getMediaLicenseDataForMediaID:(TVMediaToPlayInfo*)mediaItemToPlayInfo
                         startedBlock:(void(^)(void)) started
                      completionBlock:(void(^)(void)) completionBlock
                          failedBlock:(void(^)(conditionalAccessFailureReason error)) failedBlock
{
    
    TVFile *mainFile = [mediaItemToPlayInfo currentFile];
    
    if (mainFile != nil)
    {
        NSInteger fileID = [mainFile.fileID integerValue];
        
        __block TVPAPIRequest *request = [TVPMediaAPI requestForMediaLicenseData:fileID  andMediaID:[mediaItemToPlayInfo.mediaItem.mediaID integerValue] delegate:nil];
        
        [request setStartedBlock:
         ^{
             
         }];
        
        [request setFailedBlock:^{
            
            if(failedBlock)
            {
                failedBlock(conditionalAccessFailureReason_requestFailed);
            }
        }];
        
        [request setCompletionBlock:
         ^{
             
             id response = [request JSONResponse];
             
             //  If response is a real custom data
             if ((response != nil) && ([response isKindOfClass:[NSString class]]) && ![self isResponseIncludeServerError:response])
             {
                 mediaItemToPlayInfo.customData = response;
                 if (completionBlock)
                 {
                     completionBlock();
                 }
                 
             }else{
                 
                 if (failedBlock)
                 {
                     failedBlock(conditionalAccessFailureReason_customdata_issue);
                 }
             }
             
         }];
        
        [self sendRequest:request];
        
    }
    else
    {
        // there is no file!
        if(failedBlock)
        {
            failedBlock(conditionalAccessFailureReason_NoFileToPlay);
        }
    }
}

- (BOOL)isResponseIncludeServerError:(id)response
{
    if (([response rangeOfString:@"Description"].location != NSNotFound) && ([response rangeOfString:@"Error"].location) != NSNotFound) {
        return YES;
    }
    return NO;
}




@end
