//
//  YESSessionAdapter.h
//  YES_iPad
//
//  Created by Rivka S. Peleg on 12/8/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//


#import "APSBaseNetworkObject.h"

@interface GCSessionAdapter : APSBaseNetworkObject

@property (nonatomic, assign) UIViewController *viewController;

// Sitlent login - This login in assumed taht the user already loged in succefully at previuse session ,
// This login in designed for make sure the user account status not changed . ( For example - the user in Legal issue )
-(void) loginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock;

-(void) silentLoginWithToken:(NSString *) token SuccessBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock;

-(void) ssoLoginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock andRgisterDeviceBlock:(void (^)(void)) registerDeviceBlock;
-(void) loginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock andRgisterDeviceBlock:(void (^)(void)) registerDeviceBlock;

-(void) logoutWithSuccessBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock;

-(void) setAssertBlock;
-(void) loginFailedCompleted:(NSString *) errorMessage;
-(void)registerDeviceToDomain;
-(void) ssoLoginWithUserName:(NSString *) username password:(NSString *) password;

@end

extern NSString *const GCSessionAdapterLoginSuccessfullyCompleted;
extern NSString *const GCSessionAdapterLoginOutSuccessfullyCompleted;