//
//  YESSessionAdapter.m
//  YES_iPad
//
//  Created by Rivka S. Peleg on 12/8/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "GCSessionAdapter.h"
#import "GCUserDomain.h"
#import <TvinciSDK/TVSessionManager.h>



typedef void (^SuccededBlock)();
typedef void (^RegisterBlock)();
typedef void (^FailedBlock)(NSString * errorMessage);

typedef enum
{
    AlertType_RegisterDevice,
    AlertType_ActivateDevice,
} AlertType ;

@interface GCSessionAdapter ()<UIAlertViewDelegate>

@property (copy, nonatomic) SuccededBlock loginSuccededBlock;
@property (copy, nonatomic) FailedBlock loginFailedBlock;
@property (copy, nonatomic) SuccededBlock logoutSuccededBlock;
@property (copy, nonatomic) FailedBlock logoutFailedBlock;
@property (copy, nonatomic) RegisterBlock registerDeviceBlock;

@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * password;


@end

@implementation GCSessionAdapter


-(void) dealloc
{
    NSLog(@"Login dealloced!");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) cleanLogOutProcess
{
    self.logoutSuccededBlock = nil;
    self.logoutFailedBlock = nil;
}

-(void) cleanLoginInProcess
{
    self.loginSuccededBlock = nil;
    self.logoutFailedBlock = nil;
    
}

-(void) loginFailedCompleted:(NSString *) errorMessage
{
    ASLogInfo(@"loginFailedCompleted");
    [self unregisterfromAllNotification];
    if (self.loginFailedBlock)
    {
        ASLogInfo(@"loginFailedBlock = %@",self.loginFailedBlock);
        self.loginFailedBlock(errorMessage);
    }
    
}

-(void) loginSucceded
{
    [self unregisterfromAllNotification];
    [[NSNotificationCenter defaultCenter] postNotificationName:GCSessionAdapterLoginSuccessfullyCompleted object:nil];
    
    [GCUserDomain setUserName:self.userName];
    [GCUserDomain setUserPassword:self.password];
    
    if (self.loginSuccededBlock)
    {
        self.loginSuccededBlock();
    }
}

-(void) logoutFailedCompleted:(NSString *) errorMessage
{
    [self unregisterfromAllNotification];
    if (self.logoutFailedBlock)
    {
        self.logoutFailedBlock(errorMessage);
    }
}

-(void) logoutSucceded
{
//    [self unregisterfromAllNotification];
      [self unregisterForLogoutNotification];
    [[NSNotificationCenter defaultCenter] postNotificationName:GCSessionAdapterLoginOutSuccessfullyCompleted object:nil];
    if (self.logoutSuccededBlock)
    {
        self.logoutSuccededBlock();
    }
}

#pragma mark - handle customer custom login errores


-(void) setAssertBlock
{
    assertLogin assertLogin = ^(NSDictionary * response)
    {
        NSArray * dynamicData = [[[response objectForKey:@"UserData"] objectForKey:@"m_oDynamicData"] objectForKey:@"m_sUserData"];
        for (NSDictionary * dynamicDataDic in dynamicData)
        {
            if ([[dynamicDataDic objectForKey:@"m_sDataType"] isEqualToString:@"USER_PERMISSIONS"])
            {
                NSString * permissionStatus = [dynamicDataDic objectForKey:@"m_sValue"];
                
                if ([permissionStatus isEqualToString:@"OK"])
                {
                    return YES;
                }
                else
                    return NO;
            }
        }
        return NO;
    };
    
    [[TVSessionManager sharedTVSessionManager] setAssertLoginBlock:assertLogin];
}

#pragma mark - public function

-(void) silentLoginWithToken:(NSString *) token SuccessBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock
{
    self.loginSuccededBlock =successBlock;
    self.loginFailedBlock =failedBlock;
    
    
    if (token)
    {
        [self registerForLoginProcessNotification];
        
        [[TVSessionManager sharedTVSessionManager] signInWithToken:token];
    }
    else
    {
        [self loginFailedCompleted:nil];
    }
}

-(void) loginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock
{
    
    self.userName = username;
    self.password = password;
    
    self.loginSuccededBlock = successBlock;
    self.loginFailedBlock = failedBlock;
    
    
    
    if (username.length>0 && password.length >0)
    {
        [self registerForLoginProcessNotification];
        
        [[TVSessionManager sharedTVSessionManager] signInWithUsername:username password:password];
    }
    else
    {
        [self loginFailedCompleted:nil];
    }
}


-(void) loginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock andRgisterDeviceBlock:(void (^)(void)) registerDeviceBlock
{
    self.userName = username;
    self.password = password;

    self.loginSuccededBlock = successBlock;
    self.loginFailedBlock = failedBlock;
    self.registerDeviceBlock = registerDeviceBlock;

    if (username.length>0 && password.length >0)
    {
        [self registerForLoginProcessNotification];
        
        [[TVSessionManager sharedTVSessionManager] signInWithUsername:username password:password];
    }
    else
    {
        [self loginFailedCompleted:nil];
    }
}

-(void) ssoLoginWithUserName:(NSString *) username password:(NSString *) password successBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock andRgisterDeviceBlock:(void (^)(void)) registerDeviceBlock
{
    self.userName = username;
    self.password = password;

    self.loginSuccededBlock = successBlock;
    self.loginFailedBlock = failedBlock;
    self.registerDeviceBlock = registerDeviceBlock;
    
    if (username.length>0 && password.length >0)
    {
        [self registerForLoginProcessNotification];
        
        [self ssoLoginWithUserName:username password:password];
    }
    else
    {
        [self loginFailedCompleted:nil];
    }
}

-(void) ssoLoginWithUserName:(NSString *) username password:(NSString *) password
{
    [[TVSessionManager sharedTVSessionManager] ssoSignInWithUsername:username password:password providerID:0]; // ??? Mavrick
}

-(void) logoutWithSuccessBlock:(void (^)(void)) successBlock failedBlock:(void (^)(NSString * failedError)) failedBlock
{
    self.logoutSuccededBlock = successBlock;
    self.logoutFailedBlock = failedBlock;
    
    [GCUserDomain sharedUserDomainManager]; // register to notifcation!

    [self registerForLogoutNotification];
    
    [[TVSessionManager sharedTVSessionManager] logout];
}

#pragma mark- notification call back function


-(void)loginSuccess:(NSNotification*)notification
{
    //[self unregisterForLoginProcessNotification];
    
    [self registerForDeviceStateNotification];
    
    // This fuction is not telling us if the whole login process is succeded, this is the first step through the login process , the next step is to checl device satet if registered or not
}

-(void)loginFailed:(NSNotification*)notification
{
    
    [self unregisterForLoginProcessNotification];
    
    NSError *error = [notification.userInfo objectForKey:TVErrorKey];
    
    if (error.code == TVLoginStatusCustomError)
    {
        NSDictionary * response = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        NSArray * dynamicData = [[[response objectOrNilForKey:@"UserData"] objectOrNilForKey:@"m_oDynamicData"] objectOrNilForKey:@"m_sUserData"];
        
        for (NSDictionary * dynamicDataDic in dynamicData)
        {
            if ([[dynamicDataDic objectForKey:@"m_sDataType"] isEqualToString:@"USER_PERMISSIONS"])
            {
                NSString * permissionStatus = [dynamicDataDic objectForKey:@"m_sValue"];
                
                if (![permissionStatus isEqualToString:@"OK"])
                {
                    NSString * message = permissionStatus;
                    
                    [self loginFailedCompleted:message];
                }
            }
        }
    }
    else
    {
        TVLoginStatus status = error.code;
        NSString * message = NSLocalizedString(@"Unknown Error", nil);
        switch (status)
        {
            case TVLoginStatusUserDoesNotExist:
                message =  NSLocalizedString(@"User does not exist",nil);
                break;
            case TVLoginStatusWrongPasswordOrUserName:
                message =  NSLocalizedString(@"Wrong user name or password",nil);
                break;
            case TVLoginStatusInsideLockTime:
                message =  NSLocalizedString(@"User is in lock period",nil);
                break;
            case TVLoginStatusUserRemovedFromDomain:
                message = NSLocalizedString(@"User Removed From Domain",nil);
                break;
            default:
                message =  NSLocalizedString(@"wrong user name or password",nil);
                break;
        }
        
        [self loginFailedCompleted:message];
    }
}



-(void) logoutSucceded:(NSNotification *) notification
{
    [self unregisterForLogoutNotification];
    
    [GCUserDomain clearUsernameAndPasswordFromKeychain];
    
    [self logoutSucceded];
}

-(void) logoutFailed:(NSNotification *) notification
{
    [self unregisterForLogoutNotification];
    
    [self logoutFailedCompleted:nil];
}


-(void) deviceStateAvailable:(NSNotification *) notification
{
    if (notification)
    {
        [self unregisterForDeviceStateNotification];
        [self unregisterForLoginProcessNotification];
    }
    
    switch ([[TVSessionManager sharedTVSessionManager]deviceState]) {
        case TVDeviceStateUnknown:
        {
            //error
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                UIAlertView *error =[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Unknown", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Continue", nil) otherButtonTitles: nil];
                [error show];
                //[error release]; // !!!
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Unknown", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Continue", nil) style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:okAction];
                [self.viewController presentViewController:alertController animated:YES completion:nil];
            }
        }
            break;
        case TVDeviceStateNotRegistered:
        {
            if (self.registerDeviceBlock != nil)
            {
                self.registerDeviceBlock();
            }
            else
            {
                NSString * messageText = NSLocalizedString(@"For Using the service please register your device", nil);
            
                if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                    UIAlertView *registerDevice =[[UIAlertView alloc]initWithTitle:nil message:messageText delegate:self cancelButtonTitle:NSLocalizedString(@"Sign out", nil) otherButtonTitles:NSLocalizedString(@"Register", nil), nil];
                    [registerDevice setTag:AlertType_RegisterDevice];
                    [registerDevice show];
                    //[registerDevice release]; // !!!
                } else {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:messageText preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *signOutAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Sign out", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                    {
                                                        [self loginFailedCompleted:nil];
                                                    }];
                    [alertController addAction:signOutAction];
                    
                    UIAlertAction *registerAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Register", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                    {
                                                        [self registerForDeviceRegisterNotifications];
                                                        [[TVSessionManager sharedTVSessionManager] registerDevice];
                                                    }];
                    [alertController addAction:registerAction];
                    [self.viewController presentViewController:alertController animated:YES completion:nil];
                }
            }
        }
            break;
        case TVDeviceStateActivated:
        {
            [self loginSucceded];
        }
            break;
        case TVDeviceStateNotActivated:
        {
            NSString * messageText = NSLocalizedString(@"For Using the service please activate your device", nil);
            
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                UIAlertView *activateDevice =[[UIAlertView alloc]initWithTitle:nil message:messageText delegate:self cancelButtonTitle:NSLocalizedString(@"ניתוק", nil)  otherButtonTitles:NSLocalizedString(@"Activate", nil) , nil];
                [activateDevice setTag:AlertType_ActivateDevice];
                [activateDevice show];
                //[activateDevice release]; // !!!
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:messageText preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ניתוק", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                               {
                                                   [self loginFailedCompleted:nil];
                                               }];
                [alertController addAction:cancelAction];
                
                UIAlertAction *activateAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Activate", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                 {
                                                     [self registerForActivateDeviceNotifications];
                                                     [[TVSessionManager sharedTVSessionManager] activateDevice];
                                                 }];
                [alertController addAction:activateAction];
                
                [self.viewController presentViewController:alertController animated:YES completion:nil];
            }
        }
        default:
            break;
    }
    
}


-(void)deviceRegistrationSucceded:(NSNotification *) notification
{
    [self unregisterForDeviceRegisterNotifications];
    
    [self deviceStateAvailable:notification];
}



//kDomainResponseStatus_LimitationPeriod=0,
//kDomainResponseStatus_UnKnown=1,
//kDomainResponseStatus_Error=2,
//kDomainResponseStatus_DomainAlreadyExists=3,
//kDomainResponseStatus_ExceededLimit=4,
//kDomainResponseStatus_DeviceTypeNotAllowed=5,
//kDomainResponseStatus_DeviceNotInDomin=6,
//kDomainResponseStatus_DeviceNotExists=7,
//kDomainResponseStatus_DeviceAlreadyExists=8,
//kDomainResponseStatus_UserNotExistsInDomain=9,
//kDomainResponseStatus_OK=10,
//kDomainResponseStatus_ActionUserNotMaster = 11,
//kDomainResponseStatus_UserNotAllowed = 12,
//kDomainResponseStatus_ExceededUserLimit = 13,
//kDomainResponseStatus_NoUsersInDomain = 14,
//kDomainResponseStatus_UserExistsInOtherDomains = 15,
//kDomainResponseStatus_DomainNotExists = 16,
//kDomainResponseStatus_HouseholdUserFailed = 17,
//kDomainResponseStatus_DeviceExistsInOtherDomains = 18,
//kDomainResponseStatus_DomainNotInitialized = 19,
//kDomainResponseStatus_RequestSent = 20,
//kDomainResponseStatus_DeviceNotConfirmed = 21,
//kDomainResponseStatus_RequestFailed = 22,
//kDomainResponseStatus_InvalidUser = 23

-(void)deviceRegistrationFailed:(NSNotification *) notification
{
    [self unregisterForDeviceRegisterNotifications];
    
    NSError * error = [notification.userInfo objectForKey:TVErrorKey];
    NSNumber * domainErrorCode = [error.userInfo objectForKey:kAPIResponseKey_DomainResponseStatusKey];
    NSString * errorMessage = @"";
    
    switch (domainErrorCode.integerValue)
    {
        case kDomainResponseStatus_ExceededLimit:
            errorMessage = NSLocalizedString(@"Exceeded Limit",nil);
            break;
        case kDomainResponseStatus_DeviceTypeNotAllowed:
            errorMessage = NSLocalizedString(@"DeviceTypeNotAllowed",nil);
            break;
        case kDomainResponseStatus_DeviceAlreadyExists:
            errorMessage = NSLocalizedString(@"DeviceAlreadyExists",nil);
        default:
            break;
    }
    
    [self loginFailedCompleted:errorMessage];
    
}

-(void)deviceActivationSucceded:(NSNotification *) notification
{
    [self unregisterForActivateDeviceNotifications];
    
    [self deviceStateAvailable:notification];
}

-(void)deviceActivationFailed:(NSNotification *) notification
{
    [self unregisterForActivateDeviceNotifications];
    
    [self loginFailedCompleted:nil];
    
}




#pragma mark - notification

-(void) registerForLoginProcessNotification
{
    [self unregisterForLoginProcessNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccess:) name:TVSessionManagerSignInCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginFailed:) name:TVSessionManagerSignInFailedNotification object:nil];
    
}

-(void) unregisterForLoginProcessNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerSignInCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerSignInFailedNotification object:nil];
    
}

-(void) registerForDeviceStateNotification
{
    [self unregisterForDeviceStateNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceStateAvailable:) name:TVSessionManagerDeviceStateAvailableNotification object:nil];
}

-(void) unregisterForDeviceStateNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceStateAvailableNotification object:nil];
}


-(void) registerForDeviceRegisterNotifications
{
    [self unregisterForDeviceRegisterNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceRegistrationSucceded:) name:TVSessionManagerDeviceRegistrationCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceRegistrationFailed:) name:TVSessionManagerDeviceRegistrationFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceRegistrationFailed:) name:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification object:nil];
}

-(void) unregisterForDeviceRegisterNotifications
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceRegistrationCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceRegistrationFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceRegistrationFailedWithKnownErrorNotification object:nil];
    
}

-(void) registerForActivateDeviceNotifications
{
    [self unregisterForActivateDeviceNotifications];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceActivationSucceded:) name:TVSessionManagerDeviceActivationCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deviceActivationFailed:) name:TVSessionManagerDeviceActivationFailedNotification object:nil];
}

-(void) unregisterForActivateDeviceNotifications
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceActivationCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerDeviceActivationFailedNotification object:nil];
}

-(void) registerForLogoutNotification
{
    [self unregisterForLogoutNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSucceded:) name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutFailed:) name:TVSessionManagerLogoutFailedNotification object:nil];
}

-(void) unregisterForLogoutNotification
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:TVSessionManagerLogoutFailedNotification object:nil];
}

-(void) unregisterfromAllNotification
{
    [self unregisterForActivateDeviceNotifications];
    [self unregisterForDeviceRegisterNotifications];
    [self unregisterForDeviceStateNotification];
    [self unregisterForLoginProcessNotification];
    [self unregisterForLogoutNotification];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        case AlertType_ActivateDevice:
        {
            if (buttonIndex ==1)
            {
                [self registerForActivateDeviceNotifications];
                [[TVSessionManager sharedTVSessionManager] activateDevice];
            }
            else
            {
                [self loginFailedCompleted:nil];
            }
        }
            break;
        case AlertType_RegisterDevice:
        {
            if (buttonIndex ==1)
            {
                [self registerForDeviceRegisterNotifications];
                [[TVSessionManager sharedTVSessionManager] registerDevice];
            }
            else
            {
                [self loginFailedCompleted:nil];
            }
            
        }
            
            break;
            
        default:
            break;
    }
}


-(void)registerDeviceToDomain
{
    [self registerForDeviceRegisterNotifications];
    [[TVSessionManager sharedTVSessionManager] registerDevice];
}

@end

NSString *const GCSessionAdapterLoginSuccessfullyCompleted = @"GCSessionAdapterLoginSuccessfullyCompleted";
NSString *const GCSessionAdapterLoginOutSuccessfullyCompleted = @"GCSessionAdapterLoginOutSuccessfullyCompleted";
