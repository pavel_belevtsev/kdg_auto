//
//  APSUserDomain.h
//  Spas
//
//  Created by Rivka S. Peleg on 7/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCUserDomain : NSObject

@property (nonatomic, readwrite, strong) TVNetworkQueue *networkQueue;
@property (strong, nonatomic) NSMutableDictionary * dictionaryFavoritsMediaByMediaID;

+ (NSString*)getUserName;
+ (NSString*)getUserPassword;
+ (void)setUserName:(NSString*)userName;
+ (void)setUserPassword:(NSString*)password;
+ (void)clearUsernameAndPasswordFromKeychain;

+(GCUserDomain *) sharedUserDomainManager;
-(BOOL) isMediaInFavorites:(NSString *) mediaID ;
-(void) addMediaToFavorites:(NSString *) mediaID ;//withCompletionblock:(void (^)(void)) completion;
-(void) removeMediaFromFavorites:(NSString *) mediaID ;//withCompletionblock:(void (^)(void)) completion;
-(NSArray *) getAllFavoritesMediaArray;
-(void) addMediaToFavorites:(NSString *) mediaID withCompletionBlock:(void (^)(void)) completion;
-(void) removeMediaFromFavorites:(NSString *) mediaID withCompletionBlock:(void (^)(void)) completion;

@end
