//
//  APSUserDomain.m
//  Spas
//
//  Created by Rivka S. Peleg on 7/28/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "GCUserDomain.h"
#import "KeychainItemWrapper.h"
#import <TvinciSDK/TvinciKeychainWrapper.h>

@implementation GCUserDomain


static GCUserDomain * sharedInstace = nil;

+(GCUserDomain *) sharedUserDomainManager
{
    @synchronized(self)
    {
        if (sharedInstace == nil)
        {
            sharedInstace = [[GCUserDomain alloc] init];
        }
    }
    
    return sharedInstace;
}

-(void) setup
{
    [self downloadFavorites];
    [self registerToLoginAndLogoutNotifcation];
}

-(void) registerToLoginAndLogoutNotifcation
{
    [self unRegisterToLoginAndLogoutNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogOut:) name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogIn:) name:TVSessionManagerSignInCompletedNotification object:nil];

}

-(void) unRegisterToLoginAndLogoutNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerSignInCompletedNotification object:nil];
}

-(void) userDidLogOut:(NSNotification *) notification
{
    [self.networkQueue cleen];
    [self clearAllUserData];
}

-(void) clearAllUserData
{
    [self.dictionaryFavoritsMediaByMediaID removeAllObjects];
}


-(void) userDidLogIn:(NSNotification *)notification
{
    [self downloadFavorites];
}


-(id) init
{
    self = [super init];
    
    if (self)
    {
        [self setup];
    }
    
    return self;
    
}

-(BOOL) isMediaInFavorites:(NSString *) mediaID
{
    TVMediaItem * favoritesMedia = [self.dictionaryFavoritsMediaByMediaID objectForKey:mediaID];
    
    if(favoritesMedia)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void) addMediaToFavorites:(NSString *) mediaID
{
    [self.dictionaryFavoritsMediaByMediaID setObject:mediaID forKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionAddToFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];
    __weak TVPAPIRequest * request = strongRequest;

    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         
         
     }];
    
    [self sendRequest:request];
    
}
-(void) removeMediaFromFavorites:(NSString *) mediaID
{
    [self.dictionaryFavoritsMediaByMediaID removeObjectForKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];
    __weak TVPAPIRequest * request = strongRequest;

    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         
         
     }];
    
    [self sendRequest:request];
    
}

-(void) addMediaToFavorites:(NSString *) mediaID  withCompletionBlock:(void (^)(void)) completion
{
    [self.dictionaryFavoritsMediaByMediaID setObject:mediaID forKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionAddToFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];
    __weak TVPAPIRequest * request = strongRequest;

    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSLog(@"%@",request.responseString);
         
         if (completion)
         {
             completion();
         }
         
     }];
    
    [self sendRequest:request];
    
}
-(void) removeMediaFromFavorites:(NSString *) mediaID withCompletionBlock:(void (^)(void)) completion
{
    [self.dictionaryFavoritsMediaByMediaID removeObjectForKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];
    __weak TVPAPIRequest * request = strongRequest;

    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSLog(@"%@",request.responseString);
         
         if (completion)
         {
             completion();
         }
         
     }];
    
    [self sendRequest:request];
    
}

#pragma mark - server requests

-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

-(void) downloadFavorites
{
    self.dictionaryFavoritsMediaByMediaID = [NSMutableDictionary dictionary];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForGetUserItems:TVUserItemTypeFavorites mediaType:liveTypeID pictureSize:[TVPictureSize pictureSizeWithSize:CGSizeMake(177, 99)] pageSize:100 pageIndex:0 delegate:nil];
    __weak TVPAPIRequest * request = strongRequest;

    [request setFailedBlock:^{
        
        NSLog(@"%@",request.error);
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSArray * mediaArrayDictionaries = [request JSONResponse];
         
         if (mediaArrayDictionaries == nil || [mediaArrayDictionaries count] ==0)
         {
             [self.dictionaryFavoritsMediaByMediaID removeAllObjects];
         }
         
         for (NSDictionary *dic in mediaArrayDictionaries)
         {
             TVMediaItem *mediaItem = [[TVMediaItem alloc] initWithDictionary:dic];
             [self.dictionaryFavoritsMediaByMediaID setObject:mediaItem.mediaID forKey:mediaItem.mediaID];
         }
     }];
    
    [self sendRequest:request];
}

-(NSArray *) getAllFavoritesMediaArray
{
    return [self.dictionaryFavoritsMediaByMediaID allValues];
}


+ (NSString*)getUserName
{
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
//    NSString *username = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
//    [keychainItem release];
    NSString * username = [TvinciKeychainWrapper searchKeychainCopyMatching:(__bridge id)(kSecAttrAccount) sync:NO];
//    if (!username)
//    {
//        username = [GCUserDomain getUserFromOldKeychain];
//    }
    return username;
}

+ (NSString*)getUserPassword
{
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
//    NSString *password = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
//    [keychainItem release];
    NSString * password = [TvinciKeychainWrapper searchKeychainCopyMatching:(__bridge id)(kSecValueData) sync:NO];
//    if (!password)
//    {
//        password = [GCUserDomain getPasswordFromOldKeychain];
//    }
    return password;
}

+(NSString*)getUserFromOldKeychain
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
        NSString *username = [keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
        //[keychainItem release];// !!!
    if (username && [username isEqualToString:@""] == NO)
    {
        [GCUserDomain setUserName:username];
    }
    return username;
}

+(NSString*)getPasswordFromOldKeychain
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
    NSString *password = [keychainItem objectForKey:(__bridge id)(kSecValueData)];
        //[keychainItem release];// !!!

    if (password && [password isEqualToString:@""] == NO)
    {
        [GCUserDomain setUserPassword:password];
    }
    
    [self clearOldUsernameAndPasswordFromKeychain];
    
    return password;
}


+ (void)setUserName:(NSString*)userName
{
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
//    [keychainItem setObject:userName forKey:(__bridge id)(kSecAttrAccount)];
//    [keychainItem release];
   // NSString * oldSavedName = [GCUserDomain getUserName];
//    if ([oldSavedName isEqualToString:userName] == NO)
//    {
        [TvinciKeychainWrapper deleteKeychainValue:(__bridge id)(kSecAttrAccount) sync:NO];
        [TvinciKeychainWrapper createKeychainValue:userName forIdentifier:(__bridge id)(kSecAttrAccount) sync:NO];
//    }
}
+ (void)setUserPassword:(NSString*)password
{
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
//    [keychainItem setObject:password forKey:(__bridge id)(kSecValueData)];
//    [keychainItem release];
    //NSString * oldPassword = [GCUserDomain getUserPassword];
//    if ([oldPassword isEqualToString:password] == NO)
//    {
        [TvinciKeychainWrapper deleteKeychainValue:(__bridge id)(kSecValueData) sync:NO];
        [TvinciKeychainWrapper createKeychainValue:password forIdentifier:(__bridge id)(kSecValueData) sync:NO];
//    }
}

+ (void)clearUsernameAndPasswordFromKeychain
{
//    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
//    [keychainItem resetKeychainItem];
//    [keychainItem release];
    [TvinciKeychainWrapper deleteKeychainValue:(__bridge id)(kSecValueData) sync:NO];
    [TvinciKeychainWrapper deleteKeychainValue:(__bridge id)(kSecAttrAccount) sync:NO];

}

+ (void)clearOldUsernameAndPasswordFromKeychain
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"kYESiPadKeychain" accessGroup:nil];
    [keychainItem resetKeychainItem];
    //[keychainItem release];// !!!
}


-(void)dealloc
{
    [self unRegisterToLoginAndLogoutNotifcation];
}
@end
