//
//  IPhoneIPadDetection.m
//  yesGo
//
//  Created by Rivka S. Peleg on 4/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "GCIPhoneIPadDetection.h"


BOOL is_iPad()
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return YES; /* Device is iPad */
    }
    else
    {
        return NO;
    }
}


@implementation GCIPhoneIPadDetection

@end
