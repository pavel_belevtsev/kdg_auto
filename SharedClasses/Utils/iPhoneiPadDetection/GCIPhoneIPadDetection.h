//
//  IPhoneIPadDetection.h
//  yesGo
//
//  Created by Rivka S. Peleg on 4/8/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

BOOL is_iPad();

@interface GCIPhoneIPadDetection : NSObject

@end
