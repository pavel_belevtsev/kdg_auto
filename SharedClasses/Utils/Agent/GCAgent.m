//
//  TimerAgent.m
//  FeedAir
//
//  Created by Rivka Schwartz on 9/13/12.
//  Copyright (c) 2012 quickode ltd. All rights reserved.
//

#import "GCAgent.h"

@implementation GCAgent
@synthesize owner = _owner;
@synthesize firedSelector = _firedSelector;


- (id)initWithOwner:(id)owner selector:(SEL)selector {
    self = [super init];
    if (!self) return nil;
    self.owner = owner;
    self.firedSelector = selector;
    return self;
}

- (void)selectorFired:(NSTimer *)t {
    

    if ([self.owner respondsToSelector:self.firedSelector]) {
        [self.owner performSelector:self.firedSelector];
    }
}


@end


