//
//  TimerAgent.h
//  FeedAir
//
//  Created by Rivka Schwartz on 9/13/12.
//  Copyright (c) 2012 quickode ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCAgent : NSObject

@property (assign, nonatomic) id owner;
@property (assign, nonatomic) SEL firedSelector;

- (id)initWithOwner:(id)owner selector:(SEL) selector;
- (void)selectorFired:(NSTimer *)t;

@end





