//
//  APSActivityIndicator.h
//  Spas
//
//  Created by Israel Berezin on 8/12/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APSActivityIndicator : UIActivityIndicatorView


-(void) nestedStartAnimating;
-(void) nestedStopAnimating;
@end
