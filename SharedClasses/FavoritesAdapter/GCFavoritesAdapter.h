//
//  GCFavoritesAdapter.h
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "APSBaseNetworkObject.h"

@interface GCFavoritesAdapter : APSBaseNetworkObject

@property (strong, nonatomic) IBOutlet UIButton * buttonLike;
//@property (strong, nonatomic) NSMutableDictionary * dictionaryFavoritsMediaByMediaID;
@property (strong, nonatomic) TVMediaItem * currMediaItem;

// this func must to implament in sub class in your project.
-(IBAction)likeButtonPressed:(id)sender;

-(void) setupMediaItem:(TVMediaItem *) mediaItem;

-(BOOL) isMediaInFavorites:(NSString *) mediaID;

-(NSArray *) getAllFavoritesMediaArray;

// call again "downloadFavorites".
-(void) refreshWithCompletionBlock:(void (^)(void)) completion;

// if used it, must before call "isMediaInFavorites" do "setupMediaItem:"
-(id)initWithControl:(UIButton *) button;

-(id)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button;

// used in sub class in "likeButtonPressed" function.
-(void) addMediaToFavorites:(NSString *) mediaID  withCompletionBlock:(void (^)(void)) completion;
-(void) removeMediaFromFavorites:(NSString *) mediaID withCompletionBlock:(void (^)(void)) completion;
-(void) downloadFavoritesWithCompletionBlock:(void (^)(void)) completion;
-(void)toggleAddRemoveFavorites:(UIButton *)sender;
@end
