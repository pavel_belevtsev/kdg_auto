//
//  GCFavoritesAdapter.m
//  Spas
//
//  Created by Israel Berezin on 9/3/14.
//  Copyright (c) 2014 Rivka S. Peleg. All rights reserved.
//

#import "GCFavoritesAdapter.h"
#import "GCFavoritesStorage.h"

@implementation GCFavoritesAdapter

#pragma mark - Init - 

-(id)init {
    self = [super init];
    if (self) {
        [self registerToLoginAndLogoutNotifcation];
    }
    return self;
}

-(id)initWithControl:(UIButton *) button
{
    self = [super init];
    if (self)
    {
        self.buttonLike = button;
        //self.dictionaryFavoritsMediaByMediaID = [NSMutableDictionary dictionary];
        [self.buttonLike addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
        {
            [self downloadFavoritesWithCompletionBlock:nil];
        }
    }
    return self;
}

-(id)initWithMediaItem:(TVMediaItem *) mediaItem control:(UIButton *) button
{
    self = [super init];
    if (self)
    {
        //self.dictionaryFavoritsMediaByMediaID = [NSMutableDictionary dictionary];
        self.currMediaItem = mediaItem;
        self.buttonLike = button;
        [self.buttonLike addTarget:self action:@selector(likeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
        {
            [self downloadFavoritesWithCompletionBlock:^{
                
                self.buttonLike.selected = [self isMediaInFavorites:self.currMediaItem.mediaID];
            
            }];
        }
        else
        {
            self.buttonLike.selected = NO;
        }
    }
    return self;
}

#pragma mark - Setters -

-(void) setupMediaItem:(TVMediaItem *) mediaItem
{
    self.currMediaItem = mediaItem;
    if (self.buttonLike)
    {
        self.buttonLike.selected = [self isMediaInFavorites:self.currMediaItem.mediaID];
    }
}

#pragma mark - Controls -

-(void) refreshWithCompletionBlock:(void (^)(void)) completion
{
    if ([[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        [self downloadFavoritesWithCompletionBlock:^{
            if (completion)
            {
                completion();
            }
        }];
    }
}

-(NSArray *) getAllFavoritesMediaArray
{
    return [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID allValues];
}

-(BOOL) isMediaInFavorites:(NSString *) mediaID
{
    if (![[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        return NO;
    }
    
    TVMediaItem * favoritesMedia = [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID objectForKey:mediaID];
    
    if(favoritesMedia)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


-(IBAction)likeButtonPressed:(id)sender
{
    // Must to implament in sub class !!!
}

-(void)toggleAddRemoveFavorites:(UIButton *)sender
{
    if (!sender.selected)
    {
        sender.selected = YES;
        [self addMediaToFavorites:self.currMediaItem.mediaID withCompletionBlock:nil];
    }
    else
    {
        sender.selected = NO;
        [self removeMediaFromFavorites:self.currMediaItem.mediaID withCompletionBlock:nil];
    }
}

#pragma mark - Network call -

-(void) downloadFavoritesWithCompletionBlock:(void (^)(void)) completion
{
   
    if (![[TVSessionManager sharedTVSessionManager] isSignedIn])
    {
        if (completion)
        {
            completion();
        }
        return;
    }
    
    //self.dictionaryFavoritsMediaByMediaID = [NSMutableDictionary dictionary];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForGetUserItems:TVUserItemTypeFavorites mediaType:liveTypeID pictureSize:[TVPictureSize pictureSizeWithSize:CGSizeMake(177, 99)] pageSize:100 pageIndex:0 delegate:nil];
    
    __weak TVPAPIRequest * request = strongRequest;
    
    [request setFailedBlock:^{
        
        NSLog(@"%@",request.error);
        NSLog(@"failed request");
        
    }];
    
    [request setCompletionBlock:^
     {
         NSArray * mediaArrayDictionaries = [request JSONResponse];
         
         //if (mediaArrayDictionaries == nil || [mediaArrayDictionaries count] ==0)
         //{
             [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID removeAllObjects];
         //}
         
         for (NSDictionary *dic in mediaArrayDictionaries)
         {
             TVMediaItem *mediaItem = [[TVMediaItem alloc] initWithDictionary:dic];
             [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID setObject:mediaItem.mediaID forKey:mediaItem.mediaID];
         }
         
         if (completion)
         {
             completion();
         }
     }];
    
    [self sendRequest:request];
}


-(void) addMediaToFavorites:(NSString *) mediaID  withCompletionBlock:(void (^)(void)) completion
{
    [self trackAddToFavorites];
    [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID setObject:mediaID forKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionAddToFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];

    __weak TVPAPIRequest * request = strongRequest;
    
    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSLog(@"%@",request.responseString);
         
         if (completion)
         {
             completion();
         }
         
     }];
    
    [self sendRequest:request];
    
}
-(void) removeMediaFromFavorites:(NSString *) mediaID withCompletionBlock:(void (^)(void)) completion
{
    [self trackRemoveFromFavorites];
    [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID removeObjectForKey:mediaID];
    
    NSString * liveTypeID = [[TVConfigurationManager sharedTVConfigurationManager].mediaTypes objectForKey:TVMediaTypeLive];
    
    TVPAPIRequest * strongRequest = [TVPMediaAPI requestForActionDone:TVMediaActionRemoveFromFavorites mediaID:mediaID.integerValue mediaType:liveTypeID extraValue:nil delegate:nil];
    
    __weak TVPAPIRequest * request = strongRequest;
    
    [request setFailedBlock:^{
        
        NSLog(@"failed request");
    }];
    
    [request setCompletionBlock:^
     {
         NSLog(@"%@",request.responseString);
         
         if (completion)
         {
             completion();
         }
         
     }];
    
    [self sendRequest:request];
    
}

#pragma mark - Login & Logut Notifcation -

-(void) registerToLoginAndLogoutNotifcation
{
    [self unRegisterToLoginAndLogoutNotifcation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogOut:) name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogIn:) name:TVSessionManagerSignInCompletedNotification object:nil];
    
}

-(void) unRegisterToLoginAndLogoutNotifcation
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerLogoutCompletedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TVSessionManagerSignInCompletedNotification object:nil];
}

-(void) userDidLogOut:(NSNotification *) notification
{
    [self.networkQueue cleen];
    [self clearAllUserData];
}

-(void) clearAllUserData
{
    [[GCFavoritesStorage instance].dictionaryFavoritsMediaByMediaID removeAllObjects];
}


-(void) userDidLogIn:(NSNotification *)notification
{
    [self downloadFavoritesWithCompletionBlock:nil];
}

#pragma mark - Memory -

-(void)dealloc
{
    [self.networkQueue cleen];
    [self unRegisterToLoginAndLogoutNotifcation];
}

#pragma mark - Tracking -

-(void)trackAddToFavorites
{
    // implament in sub class
}

-(void)trackRemoveFromFavorites
{
    // implament in sub class
}
@end
