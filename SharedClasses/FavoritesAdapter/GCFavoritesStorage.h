//
//  GCFavoritesStorage.h
//  Spas
//
//  Created by Pavel Belevtsev on 22.06.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCFavoritesStorage : NSObject

+ (GCFavoritesStorage *)instance;

@property (strong, nonatomic) NSMutableDictionary * dictionaryFavoritsMediaByMediaID;

@end
