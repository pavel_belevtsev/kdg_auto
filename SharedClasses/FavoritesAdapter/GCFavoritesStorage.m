//
//  GCFavoritesStorage.m
//  Spas
//
//  Created by Pavel Belevtsev on 22.06.15.
//  Copyright (c) 2015 Rivka S. Peleg. All rights reserved.
//

#import "GCFavoritesStorage.h"

@implementation GCFavoritesStorage

+ (GCFavoritesStorage *)instance {
    
    static GCFavoritesStorage *instance = nil;
    @synchronized (self)
    {
        if (instance == nil)
        {
            instance = [[self alloc] init];
            
        }
    }
    return instance;
}

- (id)init {
    
    self = [super init];
    if (self)
    {
        
        self.dictionaryFavoritsMediaByMediaID = [NSMutableDictionary dictionary];
        
    }
    return self;
    
}

@end
