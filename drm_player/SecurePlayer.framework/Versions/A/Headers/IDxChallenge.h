//
//  DxChallenge.h
//  Copyright 2013 Discretix Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DxDrmManagerDefines.h"

//! \brief
//! The object of this interface will be returned from generateChallengeFromInitiator or from generateChallengefromContent functions.
@interface IDxChallenge : NSObject {
}

//! \brief
//! Retrieves the generated challenge ready to be sent to the server.
//!
//! \return the challenge.
//!
-(NSString*) getChallenge;

//! \brief
//! Retrieves the generated personalization challenge ready to be sent to the personalization server.
//!
//! \return the challenge.
//!
-(NSData*) getPersonalizationChallenge;

//! \brief
//! Retrieves a URL to the server, which the generated challenge should be sent to.
//!
//! \return the URL.
//!
-(NSString*) getServerUrl;

//! \brief
//! Retrieves an action type to be set in the HTTP header SOAPAction of the requset,
//! which will contain the generated challenge
//!
//!
//! \return The action type.
//!
-(NSString*) getSoapAction;

//! \brief
//! Processes the response that the server sends in answer to the challenge.
//! If an acknowledgment challenge is required in response to a given challenge,
//! this function generates such a challenge.
//!
//! \param[in] serverResponse The server response.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - if serverResponse is nil
//! \return
//! IDxChallenge that represents the generated acknowledgment challenge, if such a challenge has to be sent to the server,
//! nil - otherwise.
//!
-(IDxChallenge*) processServerResponse: (NSString*) serverResponse result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Processes the response that the server sends in answer to the challenge.
//!
//! \param[in] serverResponse The server response.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - if serverResponse is nil
//!
-(void) processPersonalizationServerResponse: (NSData*) serverResponse result:(DxDrmManagerStatus*)checkResultOrNil;

@end