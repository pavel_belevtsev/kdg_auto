//
//  DxDrmManagerDefines.h
//  Copyright 2013 Discretix Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

//! \brief
//! list of error codes returned by Discretix SecurePlayer SDK APIs
typedef enum DxDrmManagerStatus{
    /*00*/DX_MANAGER_SUCCESS,                   //!< Operation was successful
    /*01*/DX_MANAGER_ERROR_BAD_ARGUMENTS,
    /*02*/DX_MANAGER_ERROR_CONTENT_NOT_RECOGNIZED,
    /*03*/DX_MANAGER_ERROR_FILE_ACCESS_ERROR,
    /*04*/DX_MANAGER_ERROR_FILE_IS_BEING_DOWNLOADED,
    /*05*/DX_MANAGER_ERROR_INVALID_FORMAT,
    /*06*/DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED,
    /*07*/DX_MANAGER_ERROR_NOT_INITIALIZED,
    /*08*/DX_MANAGER_ERROR_NO_RIGHTS,
    /*09*/DX_MANAGER_ERROR_OPERATION_FAILED,
    /*10*/DX_MANAGER_ERROR_SECURE_CLOCK_IS_NOT_SET,
    /*11*/DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED,
    /*12*/DX_MANAGER_ERROR_VERIFICATION_FAILURE,
    /*13*/DX_MANAGER_ERROR_ROOTED,
    /*14*/DX_MANAGER_VERSION_TOO_OLD,
    /*15*/DX_MANAGER_VERSION_NOT_SUPPORTED,
    /*16*/DX_MANAGER_ERROR_NOT_IMPLEMENTED,
    /*17*/DX_MANAGER_ERROR_COMMUNICATION_FAILURE
} DxDrmManagerStatus;

//! \brief
//! list of parameters to configure in Discretix SecurePlayer SDK
typedef enum DxConfigurationParams{
    /*00*/DX_DRM_OPERATION_NETWORK_TIMEOUT
} DxConfigurationParams;