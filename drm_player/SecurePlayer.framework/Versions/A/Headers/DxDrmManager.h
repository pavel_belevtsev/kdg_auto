//
//  DxDrmManager.h
//  DxDLC
//
//  Created by Hagai Hanan on 4/17/11.
//  Copyright 2011 Discretix Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DxDrmManagerDefines.h"

#import "IDxChallenge.h"

#define SOAP_ERROR_CUSTOM_DATA   @"soapErrorCustomData"
#define SOAP_ERROR_REDIRECT_URL  @"soapErrorRedirectUrl"
#define SOAP_ERROR_BODY			 @"soapErrorBody"
//! \brief
//! DxDrmManager supplies an interface to perform DRM (Digital Rights Management) operations. This includes
//! license acquisition, rights verification and managing licenses.
//! \details
//! DxDrmManager is a signleton object using the instantiator sharedManager method to supply it.
//! The device must be personalized in order for any DRM operation to work.
@interface DxDrmManager : NSObject {
}

//! \brief
//! Instantiator method to supply Signleton object of DxDrmManager.
//! \warning 
//! If the Device is rooted a nil will be returned.
//! \return DxDrmManager signleton object.
+ (DxDrmManager *)sharedManager;

//! \brief
//! Instantiator method to supply Signleton object of DxDrmManager.
//! \warning 
//! If the Device is rooted a nil will be returned and the drmError will be allocated.
//! \warning 
//! The drmError* will be allocated only if drmError  (NSError**) is a valid pointer and not nil.
//! The memory is allocated to the auto release pool. Calls to this function should be wrapped with 
//! an NSAutoreleasePool. 
//! \return DxDrmManager signleton object.
+ (DxDrmManager *)sharedManager:(NSError**)drmError;

//! \brief
//! Sets the DRM logging level. This method MUST be called before initializing a DxDrmManager instance.
//! \warning 
//! Calling this method after the instance method was called will have no effect.
//! \param[in]	logFile - full path to a file which the log will be printed to.
//! \param[in]	stdoutLogs - Determines if logs should be printed to the standard output.
//! \param[in]	loggingLevel - The maximum log level to print. 10 will print errors only, 30 will print errors & warnings, 40 will also include info messages, 50 will also include debug messages and 60 will also include verbose messages.
//! \param[in]	disabledModules - The modules which should not print log messages.
//!
//! \return 
//! True is setLoggingLevel is successful; else False.
//! \note
//! This method should be used for debuging purposes.
+ (BOOL)DEBUG_setLoggingLevel:(NSString *)loggingLevel 
								withLogFile:(NSString *)logFile 
								useStdOutLogs:(BOOL)stdoutLogs 
								withDisabledModules:(NSString *)disabledModules;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  filename The initiator's filename
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithFile:(NSString *)filename;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  filename The initiator's filename
//! \param[out] drmError If DRM operation fails and the server returns a SOAP error then the SOAP error will 
//!             be propagated via this output parameter. In case there is no SOAP error the output 
//!             parameter will not be changed by this function. This parameter should be inspected
//!             if the return value is a other than DX_MANAGER_SUCCESS.
//!
//! \warning 
//! The drmError* will be allocated only if drmError  (NSError**) is a valid pointer and not nil.
//! The memory is allocated to the auto release pool. Calls to this function should be wrapped with 
//! an NSAutoreleasePool.    
//!          
//! \warning
//! If the drmError output parameter is nil then the SOAP error (if exists) will obviously not be 
//! propagated to the application layer.
//!
//! \note
//! The drmError output parameter is an NSError object that contains the SOAP error data. The error
//! is specifically located in the userInfo NSDictionary container. The container maintains three keys:
//! SOAP_ERROR_CUSTOM_DATA, SOAP_ERROR_REDIRECT_URL and SOAP_ERROR_BODY.
//! 
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithFile:(NSString *)filename error:(NSError**)drmError;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  data The data containing the initiator buffer
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - If data paremeter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithData:(NSData *)data;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  data The data containing the initiator buffer
//! \param[out] drmError If DRM operation fails and the server returns a SOAP error then the SOAP error will 
//!             be propagated via this output parameter. In case there is no SOAP error the output 
//!             parameter will not be changed by this function. This parameter should be inspected
//!             if the return value is a other than DX_MANAGER_SUCCESS.
//!
//! \warning 
//! The drmError* will be allocated only if drmError  (NSError**) is a valid pointer and not nil.
//! The memory is allocated to the auto release pool. Calls to this function should be wrapped with 
//! an NSAutoreleasePool.    
//!          
//! \warning
//! If the drmError output parameter is nil then the SOAP error (if exists) will obviously not be 
//! propagated to the application layer.
//!
//! \note
//! The drmError output parameter is an NSError object that contains the SOAP error data. The error
//! is specifically located in the userInfo NSDictionary container. The container maintains three keys:
//! SOAP_ERROR_CUSTOM_DATA, SOAP_ERROR_REDIRECT_URL and SOAP_ERROR_BODY.
//! 
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - If data paremeter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithData:(NSData *)data error:(NSError**)drmError;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  url The initiator's URL
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - If url paremeter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithURL:(NSURL *)url;

//! \brief
//! Execute a PlayReady initiator file. Initiators may be used for license acquisition,
//! join/leave domain and metering. A combined initiator can contain one or more of the
//! above.
//! \param[in]  url The initiator's URL
//! \param[out] drmError If DRM operation fails and the server returns a SOAP error then the SOAP error will 
//!             be propagated via this output parameter. In case there is no SOAP error the output 
//!             parameter will not be changed by this function. This parameter should be inspected
//!             if the return value is a other than DX_MANAGER_SUCCESS.
//!
//! \warning 
//! The drmError* will be allocated only if drmError  (NSError**) is a valid pointer and not nil.
//! The memory is allocated to the auto release pool. Calls to this function should be wrapped with 
//! an NSAutoreleasePool.    
//!          
//! \warning
//! If the drmError output parameter is nil then the SOAP error (if exists) will obviously not be 
//! propagated to the application layer.
//!
//! \note
//! The drmError output parameter is an NSError object that contains the SOAP error data. The error
//! is specifically located in the userInfo NSDictionary container. The container maintains three keys:
//! SOAP_ERROR_CUSTOM_DATA, SOAP_ERROR_REDIRECT_URL and SOAP_ERROR_BODY.
//! 
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_MIME_TYPE_NOT_RECOGNIZED - The MIME type is not recognized as DRM object MIME type.
//! - DX_MANAGER_ERROR_INVALID_FORMAT - THe DRM object's format does not match the provided MIME type.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - sourceFileName does not exist or could not be opened or read or 
//!     destFileName (if provided) cannot be opened for write.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The Initator execution has failed.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - If url paremeter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)executeInitiatorWithURL:(NSURL *)url error:(NSError**)drmError;

//! \brief
//!  This funtion verifies if the file specified by filename has valid rights. 
//! \param[in]     filename - full path to either the content file, the rights of which should be verified, or the corresponding initiator. 
//!
//! \return     
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - The file does not exist or the file could not be opened or read.
//! - DX_MANAGER_ERROR_NO_RIGHTS - The file does not have license.
//! - DX_MANAGER_ERROR_SECURE_CLOCK_IS_NOT_SET - The secure clock is not set.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - filename parameter is nil.
- (DxDrmManagerStatus)verifyRightsForFile:(NSString *)filename;

//! \brief
//! Verifies that the personalization process of the assets that were stored with the specified tag 
//! was completed successfully and these assets can be successfully used by the PlayReady DRM scheme.
//!
//! \warning
//! If this function returns any value other than DX_MANAGER_SUCCESS then the DxDrmManager DRM methods
//! will not work.
//!
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//! - DX_MANAGER_ERROR_VERIFICATION_FAILURE - The provisioning was not completed successfully (i.e. assets are missing, 
//!     asset format is not valid, assets don't match each other)
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - assetTag is null or points to null.
- (DxDrmManagerStatus)personalizationVerify;

//! \brief
//! Starts the personalization process. 
//! 
//! \param[in] sessionID       currently use pass NULL
//! \param[in] serverURL       The server address
//! \param[in] appVersionOrNil The Customer Application Version Identifier
//!
//! \note 
//! This method can take up to 40 seconds to return.
//!
//! \return
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//! - DX_MANAGER_ERROR_OPERATION_FAILED - The personalization operation has failed.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - serverURL is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)performPersonalizationWithSessionID:(NSString *)sessionID withServerURL:(NSString *)serverURL withAppVersion:(NSString *)appVersionOrNil;

//! \brief
//! Deletes all the rights objects of a specific file from the Secure Storage.
//! \param[in]     filename - full path to the content file. 
//!
//! \return 
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - If filename is nil.
- (DxDrmManagerStatus)deleteRightsForFile:(NSString *)filename;

//! \brief
//! Acquire the rights for the specified DRM file.
//! \param[in]     filename - Full path to the content file. 
//! \param[in]     dataOrNil - Custom data for customer internal usage.
//! \param[in]     urlOrNil - Url to a Rights Server to be used only for this operation.
//!
//! \return 
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - in case filename parameter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)acquireRightsForFile:(NSString *)filename withCustomData:(NSString *)dataOrNil withRightsUrl:(NSString *)urlOrNil;

//! \brief
//! Acquire the rights for the specified DRM file.
//! \param[in]     filename - Full path to the content file. 
//! \param[in]     dataOrNil - Custom data for customer internal usage.
//! \param[in]     urlOrNil - Url to a Rights Server to be used only for this operation.
//! \param[out] drmError If DRM operation fails and the server returns a SOAP error then the SOAP error will 
//!             be propagated via this output parameter. In case there is no SOAP error the output 
//!             parameter will not be changed by this function. This parameter should be inspected
//!             if the return value is a other than DX_MANAGER_SUCCESS.
//!
//! \warning 
//! The drmError* will be allocated only if drmError  (NSError**) is a valid pointer and not nil.
//! The memory is allocated to the auto release pool. Calls to this function should be wrapped with 
//! an NSAutoreleasePool.    
//!          
//! \warning
//! If the drmError output parameter is nil then the SOAP error (if exists) will obviously not be 
//! propagated to the application layer.
//!
//! \note
//! The drmError output parameter is an NSError object that contains the SOAP error data. The error
//! is specifically located in the userInfo NSDictionary container. The container maintains three keys:
//! SOAP_ERROR_CUSTOM_DATA, SOAP_ERROR_REDIRECT_URL and SOAP_ERROR_BODY.
//!
//! \return 
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - The secure storage is used and found to be corrupted.
//! - DX_MANAGER_ERROR_BAD_ARGUMENTS - in case filename parameter is nil.
//! - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus)acquireRightsForFile:(NSString *)filename withCustomData:(NSString *)dataOrNil withRightsUrl:(NSString *)urlOrNil error:(NSError**)drmError;

//! \brief
//! Returns an array of rights objects describing the available rights for a file
//! \param[in]		filename - full path to either the content file, the rights info of which is required, or the corresponding initiator.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - The file does not exist or the file could not be opened or read.
//!                 - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//!                 - DX_MANAGER_ERROR_CONTENT_NOT_RECOGNIZED - if DRM Scheme could not be identified.
//!                 - DX_MANAGER_ERROR_FILE_IS_BEING_DOWNLOADED - If the file is being downloaded and there is not enough data to begin using it.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - If drmFile or activeRo are nil, or an invalid iterationMode given, or filename parameter is nil.
//! \return
//! An array that consists of DxRightstInfo read-only objects for each right info.
//! Returns nil in case of no rights or error.
//!
//! \note
//! In case the function returns nil, the caller should check the value returned in the output 
//! parameter checkResultOrNil. 
- (NSArray *)getRightObjectsForFile:(NSString *)filename result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Checks if a file is a DRM protected file
//! \param[in]		contentFilename - full path to the content file.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_FILE_ACCESS_ERROR - The file does not exist or the file could not be opened or read.
//!                 - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENT - If contentFilename is nil.
//!                 - DX_MANAGER_ERROR_CONTENT_NOT_RECOGNIZED - The contentFilename point to a clear (not a DRM) or to an unknown file.
//! \return
//! - YES - Specified content is a DRM content.
//! - NO - In this case the checkResultOrNil should be inspected. If checkResultOrNil is DX_MANAGER_SUCCESS then the content is no DRM, else the function could not verify whether the content is DRM or not.
//!
- (BOOL)isDrmContent:(NSString *)contentFilename result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Verifies whether the secure clock is set.
//! If this function returns false, the secure clock has to be set via API before any other DRM operation.
//! \return
//! YES - if the secure clock is set,
//! NO - otherwise
//!
- (BOOL) verifySecClockSet;

//! \brief
//! Sets the secure clock required for normal operation of DRM operations.
//! \return
//!     - DX_MANAGER_SUCCESS - Operation completed successfully.
//!     - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//!     - DX_MANAGER_ERROR_COMMUNICATION_FAILURE - The server didn't respond or there was a network problem.
- (DxDrmManagerStatus) performSecClockSet;

//! \brief
//! Once setCookies is called every "acquireRights" or "executeInitiator" will attach a cookie header to the HTTP request
//! which contains the cookies.
//! To clear the cookies just call setCookies(nil).
//!
//! The cookies will be seperated by "; " (semi-colon and white-space)
//! The final header will be as follow: "cookie = <COOKIE_DATA_1>; <COOKIE_DATA_2>; <COOKIE_DATA_3>..."
//!
//! \note
//! *This function is not thread safe.
//!
//! \param[in] cookies The array of cookies to be sent.
//! \return
//!     - DX_MANAGER_SUCCESS - Operation completed successfully.
//!     - DX_MANAGER_ERROR_BAD_ARGUMENTS - If one of the members in the cookies array is not an NSString object.
- (DxDrmManagerStatus) setCookies: (NSArray *) cookies;

//! \brief
//! Option to configure SDK parameters.
//!
//! Parameters to configure:
//! 1. DX_DRM_OPERATION_NETWORK_TIMEOUT set the timeout for http requests in DRM operations (in seconds).
//! 	  Default value: 60 seconds.
//!    Valid range: 5-300 seconds.
//!
//! \param[in] configParam - the parameter to configure.
//! \param[in] value
- (void) setConfigurationParams:(DxConfigurationParams)configParam withValue:(int)value;


//! \brief
//! Returns a string representation of the SecurePlayer version.
//! \return Version string.
- (NSString *)getDrmVersion;

//! \brief
//! Returns a string representation of the Device ID.
//! \return Device ID string.
- (NSString *)getDeviceId;

//! \brief
//! Generates a personalization challenge.
//! The function returns IDxChallenge that enables handling the Personalization server response.
//!
//! The IdxChallenge is valid as long as the IDxDrmDlc is alive. Terminating the IDxDrmDlc invalidates the IDxChallenge and it
//! will not be able to handle the response correctly. Therefore the IDxDrmDlc must remain in memory until the response from the server is
//! handled.
//!
//! Generate another personalization challenge also cause the privies one to be invalid.
//!
//! Do not use the <performPersonalizationWithSessionID>"()" method in parallel of using this challenge method
//!
//! \param[in]	appVersionOrNil The version name of the Application using the DxDrmManager Interface.
//! \param[in]	sessionID Custom data that will be passed to the personalization server.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//! \return	IDxChallenge (autorelease) that represents the generated challenge
//!
-(IDxChallenge *) generatePersonalizationChallengeWithSessionID:(NSString *)sessionID withAppVersion:(NSString *)appVersionOrNil result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Generates a challenge from a PlayReady Initiator file.
//! The type of the generated challenge will depend on the type of the initiator.
//!
//! \param[in]	initiatorPath Full path to the local initiator file.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - if initiatorPath is nil
//! \return	IDxChallenge (autorelease) that represents the generated challenge
//!
-(IDxChallenge *) generateChallengeFromInitiator:(NSString*) initiatorPath result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Generates a License Acquisition challenge from a content.
//! \param[in]	contentPath Full path to the local content.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - if contentPath is nil
//! \return IDxChallenge (autorelease) that represents the generated challenge
//!
-(IDxChallenge *) generateLicenseChallengeFromContent:(NSString*) contentPath result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Generates a License Acquisition challenge from a PlayReady header.
//! \param[in]	drmHeader The PlayReady header.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//!                 - DX_MANAGER_ERROR_BAD_ARGUMENTS - if drmHeader is nil
//! \return IDxChallenge (autorelease) that represents the generated challenge
//!
-(IDxChallenge *) generateLicenseChallengeFromDrmHeader:(NSString*) drmHeader result:(DxDrmManagerStatus*)checkResultOrNil;

//! \brief
//! Generates a Set Secure Clock challenge.
//! \param[out]     checkResultOrNil - A pointer to an DxDrmManagerStatus or nil(if it should be ignored). The output value of this parameter can be one of the following:
//!                 - DX_MANAGER_SUCCESS - Operation completed successfully.
//! \return IDxChallenge (autorelease) that represents the generated challenge
//!
-(IDxChallenge *) generateSecClockChallengeResult:(DxDrmManagerStatus*)checkResultOrNil;



//! \brief
//! Deletes from the application secure storage all the assets of the PlayReady scheme that were stored.
//!
//! \return 
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//! - DX_MANAGER_ERROR_IPC_FAILURE - Communication with other DRM agent component could not be established.
//!
//! \note
//! This function is used for debug purposes only.
//! 
- (DxDrmManagerStatus)DEBUG_deletePersonalization;

//! \brief
//! Deletes PlayReady Store - deleting licenses and domains of the client from the PlayReady server. 
//!
//! \return 
//! - DX_MANAGER_SUCCESS - Operation completed successfully.
//! - DX_MANAGER_ERROR_NOT_INITIALIZED - The DRM Client has not been initialized.
//! - DX_MANAGER_ERROR_SECURE_STORAGE_IS_CORRUPTED - Secure storage is found to be corrupted.
//! - DX_MANAGER_ERROR_IPC_FAILURE - Communication with other DRM agent component could not be established.
//! 
//! \note
//! This function is used for debug purposes only.
- (DxDrmManagerStatus)DEBUG_deletePlayReadyStore;


//! \brief
//! Set the DrmManager to work with local personalization for debug purposes.
//! \param[in]		enable - To work with local personalizations set to true; else false.
//! \note
//! This method should be used for debuging purposes, specifically when there is no active personalization server available.
- (void)DEBUG_setLocalPersonalization:(BOOL)enable;


@end
