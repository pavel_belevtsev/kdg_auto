//
//  BaseTVPClient.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVPAPIRequest.h"
#import "NSDictionary+NSNullAvoidance.h"

typedef enum{
    TVGatewayTypeSite,
    TVGatewayTypeMedia,
    TVGatewayTypeDomain,
    TVGatewayTypePricing
}TVGatewayType;

@interface BaseTVPClient : NSObject 
+(NSURL *) URLForMethodName : (NSString *) methodName;
+(TVPAPIRequest *) requestWithURL : (NSURL *) URL delegate : (id<ASIHTTPRequestDelegate>) delegate;
+(TVPAPIRequest *) requestFunctionName:(NSString *)functionName andCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData delegate : (id<ASIHTTPRequestDelegate>) delegate;



@end

#define TVPClientIdleNotification @"TVPClientIdleNotification"