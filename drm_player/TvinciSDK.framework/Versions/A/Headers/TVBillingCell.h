//
//  TVBillingCell.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/9/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TVTransaction;
@class TVLinearLayout;

@interface TVBillingCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIButton *btnBadge;
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;
@property (retain, nonatomic) IBOutlet UILabel *lblAmount;
@property (retain, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (nonatomic, retain) TVTransaction *transaction;
@property (retain, nonatomic) IBOutlet TVLinearLayout *metaLayout;
@property (retain, nonatomic) IBOutlet UIImageView *imgCellBg;

+(TVBillingCell *) cellWithTransaction : (TVTransaction *) transaction;
-(void) updateTransactionTimeWithFixedDate:(NSDate *)date;
@end
