//
//  TVNetworkQueu.h
//  TVinci
//
//  Created by Avraham Shukron on 6/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ASIHTTPRequest;

@interface TVNetworkQueue : NSObject

/*!
 * @abstract Add the request to the network queue
 */
- (void) sendRequest : (ASIHTTPRequest *) request;
-(void)cleen;
@end
