//
//  EpgManager.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 10/10/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import <Foundation/Foundation.h>
#import "TVInitObject.h"
#import "BaseModelObject.h"
#import "TVDevice.h"
#import "SVProgressHUD.h"

typedef enum
{
    EpgContentPositionCenter = 1,
    EpgContentPositionTop = EpgContentPositionCenter << 1,
    EpgContentPositionBottom = EpgContentPositionCenter << 2,
    EpgContentPositionRight = EpgContentPositionCenter << 3,
    EpgContentPositionLeft = EpgContentPositionCenter << 4
}ContentPositionEpg;

#define kNotificationKey_TvGuideNowPressed @"kNotificationKey_TvGuideNowPressed"


@interface EpgManager : BaseModelObject
@property (nonatomic, retain) NSArray * realChannels;
@property (nonatomic, retain) NSArray * currentlyPresentedChannel;
@property (nonatomic ,retain) NSMutableArray * oneChannelProgram;

@property (nonatomic, retain) NSMutableDictionary *allPrograms;
@property (nonatomic, retain) NSMutableDictionary *todayPrograms;
@property (nonatomic, retain) NSMutableDictionary *oneDayPrograms;
@property (nonatomic, assign) NSInteger currentlyPresentedChannelID;
@property(nonatomic,retain) NSMutableDictionary *programesPerDayForChanelID;
@property (nonatomic ,retain) NSArray * galleryItems;
@property (nonatomic, retain) NSMutableDictionary *realPrograms;
@property (retain, nonatomic) NSMutableArray * currentlyLoudingProgrames;
@property(retain, nonatomic) NSDate * selectedDayDate;

@property (nonatomic, retain) SVProgressHUD *progressHUD;
@property (nonatomic, readwrite, retain) TVNetworkQueue *networkQueue;
@property(nonatomic) NSInteger dayNumber;
@property(nonatomic) NSInteger epgId;
@property (nonatomic) NSInteger  selectedChannelGropId;
@property (nonatomic) BOOL isAllChannels;
@property (nonatomic) BOOL isAllProgrames;
@property (nonatomic) BOOL loadOnlyChannels;
@property (nonatomic) BOOL progressHUDShow;
@property (nonatomic) BOOL showReadyPrograme;
@property (nonatomic) BOOL isStartLoading;
@property(readwrite, assign)BOOL pendingLoadingDueToAppResign;

@property (nonatomic) NSInteger numberOfChannelsWithOutProgrames;
@property ( nonatomic) NSInteger numberOfItemsWithEPGid;
@property (nonatomic) BOOL isApp;
@property (nonatomic) BOOL isMyZoonOpen;

+(EpgManager *) sharedEpgManager;
-(TVEPGProgram *)findCurrectProgram;
-(NSInteger)findPlaceOfCurrectProgram;
-(void)loadData;
-(void)loadDataForOneDayWithDay:(NSInteger)theDayNumber;

-(void) downloadChannelMediaListWithChannelID:(NSInteger)channelID;
-( TVGalleryItem *) findGalleryForSelectedChannelGropId;
-(void)getProgramsToOneChnnelID:(NSString *)chnnelID;
-(NSString *)takeEpgIdFromMediaItem:(TVMediaItem *)theMediaItem;
-(BOOL)chackIfHaveChannelCash;

-(void)getCurrentProgramForChnnelID:(NSString *)chnnelID andIndexOfChannel:(NSInteger)index forOneDay:(NSInteger)day;

-(BOOL)allProgramsHaveBeenLoaded;
-(void)cleanAllPrametars;
-(void)showLoader;
-(void)cleanALLRequest;

-(int) getChannelIndexForChannelMediaId:(NSString *)channelMediaId;


extern NSString * const EpgLoadAllChannels;
extern NSString * const EpgLoadCurrectlyChannels;
extern NSString * const EpgFinishLoadTodayProgrames;
extern NSString * const EpgFinishLoadOneDayProgrames;
extern NSString * const EpgFinishLoadOneChannelProgrames;
extern NSString * const EpgLoadOnlyChannelsLoading;
@end
