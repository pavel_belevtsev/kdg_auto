//
//  SwipeViewScrollBarView.h
//  TvinciSDK
//
//  Created by Quickode Ltd. on 2/13/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwipeViewScrollBarView : UIView
@property (retain, nonatomic) IBOutlet UIImageView *imgCapsule;
@property(readwrite, assign) int numOfItems;

-(void) setup;
-(void) setupCapsuleForNumberOfItems:(int)numberOfItems;
-(void) updateToIndex:(int)index;
@end
