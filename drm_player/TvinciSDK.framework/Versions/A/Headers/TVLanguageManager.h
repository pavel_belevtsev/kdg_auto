//
//  YESLanguageManager.h
//  YES_iPhone
//
//  Created by Rivka S. Peleg on 7/29/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVNetworkQueue.h"

/**
 *	Lnaguage types for the translation dictionary
 */
typedef enum LanguageType
{
    LanguageType_None,
    LanguageType_English,
    LanguageType_Hebrew,
    LanguageType_French,
    LanguageType_Russian,
    LanguageType_Deutsch,
}LanguageType;
NSString * stringForLanguageType(LanguageType languageType);

/**
 *	Text Key is the enum that define an error type , the text that represent the enum value is the key to the dictionary of the translation 
 */ 
typedef enum TextKey
{
    TextKey_Exceeded_limit,
    TextKey_Inaccurate_Price,
    TextKey_Incorrect_details,
    TextKey_Passwork_Expired,
    TextKey_Debt_Exist,
    TextKey_user_under_legal_issue,
    TextKey_user_under_freeze,
    TextKey_watch_not_allowed,
    TextKey_user_subscription_is_not_active,
    TextKey_registration_process_not_complete,
    TextKey_User_not_Valid,
}TextKey;

NSString * stringForTextKey(TextKey textKey);

/**
 *	represent states of the class
 */
typedef enum LanguageManagerStatus
{
    languageManagerStatus_Not_Initialized,
    languageManagerStatus_Initializing,
    languageManagerStatus_Initialized
}LanguageManagerStatus;




/**
 *	this class is the agent between error enum to text representation in requsted language  
 */
@interface TVLanguageManager : NSObject


@property (assign,readonly, nonatomic) LanguageManagerStatus status;
@property (assign,readonly, nonatomic) LanguageType currentLanguage;



/**
 *	The singelton method
 *
 *	@return	the shared instance
 */
+ (TVLanguageManager *)sharedLanguageManager;

/**
 *  This function sets up the dictionary for requested lnaguage 
 *
 *	@param	language
 */
-(void) setupLanguage:(LanguageType) language;

/**
 *	return a text for key if the dictionary exisst if not , return nil
 *
 *	@param	textKey
 *
 *	@return	the translation text
 */
-(NSString *) getTextForKey:(TextKey) textKey;

/**
 *	this function was build for the state that the language dictionary is snot loaded yet ,
 *  so the operation saved and the manager operate it when language dicionary is active
 *
 *	@param	textKey	
 */
-(void) operateTextForKey: (TextKey) textKey OnBlock:(void (^)(NSString *)) block;







@end
