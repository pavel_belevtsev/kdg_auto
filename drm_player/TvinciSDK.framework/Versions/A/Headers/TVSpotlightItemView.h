//
//  TVSpotlightItemView.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "TVSelectableMediaItemView.h"
@interface TVSpotlightItemView : TVSelectableMediaItemView
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIImageView *imgOverlay;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UIImageView *imgBackground;
@end
