//
//  TVGenreCell.h
//  Kanguroo
//
//  Created by Avraham Shukron on 5/25/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVGenreCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lblTitle;

+(TVGenreCell *) genreCell;
@end
