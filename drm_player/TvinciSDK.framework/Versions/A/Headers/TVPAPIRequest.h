//
//  TvpAPIRequest.h
//  tvinci-ios-framework
//
//  Created by Avraham Shukron on 4/18/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import "ASIHTTPRequest.h"
#import "TVInitObject.h"

@interface TVPAPIRequest : ASIHTTPRequest
@property (nonatomic , retain, readonly) NSMutableDictionary *postParameters;
@property (nonatomic , retain, readonly) id JSONResponse;
@property (nonatomic , assign) BOOL doNotUseInitObj;
@property (nonatomic, retain) id context;

-(id)initRequestFor:(NSString *)functionName WithCustomInitObject:(TVInitObject *)customInitObject AndOtherPostData:(NSDictionary *)postData;
-(id)initBasicRequestFor:(NSURL *)customUrl;

-(id) JSONResponseJSONOrString; // shefyg - added for case string is needed
-(id) bakedJSONResponse;
-(NSString *) debugPostBudyString;

@end

extern NSString *const TVPAPIRerquestErrorMessageKey;
