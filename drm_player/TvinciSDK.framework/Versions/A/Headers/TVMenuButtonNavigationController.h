//
//  TVMenuButtonNavigationController.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/5/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TVMenuButtonNavigationController : UINavigationController
@property (nonatomic, retain) IBOutlet UIButton *menuButton;
+(TVMenuButtonNavigationController *) menuButtonNavigationControllerWithCustomBar;
-(void) setMenuButtonHidden : (BOOL) hidden animated : (BOOL) animated;
-(void)showMenuButtonHidden;

@end
