//
//  TVSplashView.h
//  TVinci
//
//  Created by Avraham Shukron on 7/1/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 * This class is a splash screen 
 */
@interface TVSplashView : UIView
+(TVSplashView *) sharedTVSplashView;
-(void) setStatus : (NSString *) status;

-(void) addDependency : (NSString *) dependencyName;
-(void) removeDependency : (NSString *) dependencyName;
-(void) removeAllDependency;

-(void) showInView : (UIView *) container withInitialMessage : (NSString *) initialMessage finishMessgae : (NSString *) finishMessage;
-(void) dismiss;

@property (retain, nonatomic) IBOutlet UIImageView *splashImage;
@property (retain, nonatomic) IBOutlet UILabel *statusLabel;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) IBOutlet UILabel *lblVersion;
@property (retain, nonatomic) IBOutlet UIImageView *imgLoading;


@end
