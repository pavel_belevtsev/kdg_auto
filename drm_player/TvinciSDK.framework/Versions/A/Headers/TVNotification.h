//
//  TVNotification.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 9/30/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "TVConstants.h"
#import "BaseModelObject.h"

@interface TVNotification : BaseModelObject

@property (retain, nonatomic) NSString * notificationID;
@property (retain, nonatomic) NSString * notificationUniqID;
@property (retain, nonatomic) NSString * notificationRequestID;
@property (retain, nonatomic) NSString * notificationMessageID;
@property (retain, nonatomic) NSString * userID;
@property (assign, nonatomic) TVNotificationViewStatus notificationViewStatus;
@property (assign, nonatomic) TVNotificationType notificationType;
@property (retain, nonatomic) NSString * appName;
@property (retain, nonatomic) NSString * udid;
@property (retain, nonatomic) NSString * deviceID;
@property (retain, nonatomic) NSString * notificationMessage;
@property (retain, nonatomic) NSString * notificationTitle;
@property (retain, nonatomic) NSDate * notificationPublishDate;
@property (retain, nonatomic) NSDictionary * notificationTags;
@property (retain, nonatomic) NSString * mediaID;
@property (retain, nonatomic) NSURL * pictureURL;
@property (retain, nonatomic) NSString  * templateEmail;










@end
