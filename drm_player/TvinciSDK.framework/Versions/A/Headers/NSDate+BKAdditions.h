//
//  NSDate+BKAdditions.h
//  Gifts
//
//  Created by Boris Korneev on 19/04/12.
//  Copyright (c) 2012 Quickode Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (BKAdditions)

- (NSInteger)year;
- (NSInteger)week;
- (NSInteger)month;
- (NSInteger)day;
- (NSString *)monthName;
- (NSInteger)hours;
- (NSInteger)minutes;
- (NSInteger)seconds;

- (BOOL) isToday;
- (BOOL) isTomorrow;
- (BOOL) isThisWeek;
- (BOOL) isThisMonth;
- (BOOL) isNextMonth;

- (BOOL)isDatePassed;

@end
