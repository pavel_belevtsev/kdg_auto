//
//  TVSocialNetComment.h
//  TvinciSDK
//
//  Created by Tarek Issa on 4/1/14.
//  Copyright (c) 2014 Quickode. All rights reserved.
//

#import "TvinciSDK.h"
#import "TVSocialFeedComment.h"

@interface TVSocialNetComment : TVSocialFeedComment {
    
}


@property (retain, readonly) NSArray *subComments;
@property (retain, readonly) NSURL *feedItemLinkUrl;


@end
