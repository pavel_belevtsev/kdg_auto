//
//  TVSelectableMediaItemView.h
//  TvinciSDK
//
//  Created by Avraham Shukron on 8/7/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVPictureSize.h"

@class TVSelectableMediaItemView;
@class TVMediaItem;
@class TVSubscription;
@class TVRental;

typedef void (^ActionBlock)(TVSelectableMediaItemView *sender, TVMediaItem *item);

typedef void (^ActionWithSubscriptionBlock)(TVSelectableMediaItemView *sender, TVMediaItem *item , TVSubscription * subscription);

@interface TVSelectableMediaItemView : UIView

@property (nonatomic, retain) TVMediaItem *mediaItem;
@property (nonatomic,retain) TVSubscription * subscriptionItem;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIButton *badgeButton;
@property (nonatomic, retain) IBOutlet UIView *expirationDateView;
@property (nonatomic, retain) IBOutlet UIButton *imageButton;
@property (nonatomic, assign) UIEdgeInsets imageButtonEdgeInsets;
@property (nonatomic, assign) UIEdgeInsets titleEdgeInsets;
@property (retain, nonatomic) IBOutlet UIView *titelView;
@property (retain, nonatomic) IBOutlet UILabel *likeCount;
@property (retain, nonatomic) IBOutlet UIImageView *likeImage;
@property (retain, nonatomic) IBOutlet UIButton *deleteButoon;
@property (assign, nonatomic) BOOL showExpirationDate;
@property (nonatomic, retain) UILabel * expirationDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *lblPackagesInfo;
@property (retain, nonatomic) IBOutlet UILabel *secondaryTitleLabel;
@property (nonatomic, retain) NSDictionary *cellInfoDict;

@property (nonatomic, retain) UIImageView *imageFrame;

// FOR TEST
@property(readwrite, assign) BOOL selectedSelectoreWasAdded;

//@property (retain, nonatomic) UIView 

@property (nonatomic, retain) TVPictureSize *pictureSize;

-(void) setTitle : (NSString *) title;
-(void) setBadge : (NSString *) badge;

-(void) setDeleteButtonHidden : (BOOL) hidden animated : (BOOL) animated;

-(void) setDidSelectItemBlock:(ActionBlock)didSelectItemBlock;

-(void) setDidSelectItemWithSubscriptionItemBlock:(ActionWithSubscriptionBlock)didSelectItemBlock;


-(void) setDidEditItemBlock:(ActionBlock)didEditItemBlock;

-(void) setDeleteItemBlock:(ActionBlock)deleteItemBlock;

-(void) update;
-(void) setup;
-(IBAction)selected:(id)sender;
- (IBAction)deleteFromFavorites:(id)sender;

- (IBAction)selectedMyZoonItem:(id)sender;


-(void)insertPackageInfo:(NSString*)info;
-(void)insertRentalInfo:(TVRental*)rentalInfo;

#pragma mark - AsyncImages
-(void) revealButtonAnimated;
-(void) registerForImageNotifications;
-(void) unregisterFromImageNotifications;

-(void) imageFailed : (NSNotification *) notification;
-(void) imageLoaded : (NSNotification *) notification;

-(void) showLike;
#pragma mark - Internal
- (void)updateImageButtonFrameWithNewEdgeInsets;

-(void) showLoadImage;

@end
