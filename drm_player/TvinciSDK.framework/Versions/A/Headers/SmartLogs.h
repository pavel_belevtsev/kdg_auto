//
//  SmartLogs.h
//
//  Created by Avraham Shukron on 3/6/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

//  Preprocessor - based Logging System.
//
//  This module provides a simple way to control which messages will be printed
//  to the log, according to their importance.
//  The macros defined below are intended to be used instead of NSLog.
//  Depending on the severity of the message you want to print, you should use
//  different macro. For example, if you want to print message about an error
//  you should use the ASLogError(), and if you want to print a message with
//  debug information, you should use ASLogDebug.
//
//  The module defines a global variable that defines the threshold for messages
//  to be printed. The higher the threshold - the less messages will be printed.
//  Whenever you use one of the ASLog____ macros, the system will look if the
//  importance of the message is high enough (That is, >= ASLoggingThreshold),
//  and if so the message will be printed. If not - it won't even be compiled.


static const NSInteger kLogLevelError = 100;     // Used for errors. Always printed.
static const NSInteger kLogLevelWarning = 90;    // Used for events that can lead to error.
static const NSInteger kLogLevelNotice = 80;     // Used for events that the user should notice.
static const NSInteger kLogLevelInfo = 70;       // Used for normal events of interest.
static const NSInteger kLogLevelDebug = 50;      // Used for debug information.
static const NSInteger kLogLevelVerbose = 0;     // Print all things!

#ifdef __OPTIMIZE__
/*
 * For optimized builds (e.g created by building with "Release" configuration)
 * the logging threshold is high to boost performance.
 */
#define ASLoggingThreshold kLogLevelWarning
#else
/*
 *  Yours to play.
 *  You might want to change the value during development to see more logs.
 *  Usually @kLogLevelDebug should be enough.
 */
#define ASLoggingThreshold kLogLevelDebug
#endif

#define _ASFileName [[NSString stringWithUTF8String:__FILE__] lastPathComponent]
#define _ASLineNumber __LINE__
#define _ASMethodName __func__

#define SMART_LOGS_METHOD_NAME __func__
#define ASLineNumber __LINE__
#define SMART_LOGS_FILE_NAME [[NSString stringWithUTF8String:__FILE__] lastPathComponent]



/*
 * The printing is done only the the message level is >= ASLoggingThreshold.
 * In practice the codition will not even evaluate because it is comparing two
 * consts that are known at compile time, so the compiler can optimize this code
 * and determine at compile time if it should be compiled or not.
 */
#define ASControlledLog(level, ...)\
if (level >= ASLoggingThreshold) {NSLog(__VA_ARGS__);}

/*
 *  A formatted call to ASControlledLog
 */
#define ASFormattedLog(level, tag, format, ...)\
ASControlledLog(level, @"(%@ Line:%d)\n%@: %@\n\n", _ASFileName, _ASLineNumber,\
tag, [NSString stringWithFormat:(format), ##__VA_ARGS__])

/*
 * Utility to print messages that describe an erranous event. It is recommended
 * to use this macro rather that using ASLog() directly.
 */
// TODO: Change the name of the macro to ASLogError once we get rid of the
//       old ASLogError that is spread across the SDK. (A.S)

#if ASLoggingThreshold <= kLogLevelError
#define ASLogErrorMessage(...) ASFormattedLog(kLogLevelError, @"ERROR", __VA_ARGS__)
#else
#define ASLogErrorMessage(...)
#endif

/*
 * Utility to print messages that warn the user about a potentially dangerous
 * situation. It is recommended to use this macro rather that using ASLog()
 * directly.
 */
#if ASLoggingThreshold <= kLogLevelWarning
#define ASLogWarning(...) ASFormattedLog(kLogLevelWarning, @"WARNING", __VA_ARGS__)
#else
#define ASLogWarning(...)
#endif

/*
 * Utility to print messages that are worth noticing.
 * It is recommended to use this macro rather that using ASLog() directly.
 */
#if ASLoggingThreshold <= kLogLevelNotice
#define ASLogNotice(...) ASFormattedLog(kLogLevelNotice, @"NOTICE", __VA_ARGS__)
#else
#define ASLogNotice(...)
#endif

/*
 * Utility to print messages that provide a useful information in regular
 * operation.
 * It is recommended to use this macro rather that using ASLog() directly.
 */
#if ASLoggingThreshold <= kLogLevelInfo
#define ASLogInfo(...) ASFormattedLog(kLogLevelInfo, @"INFO", __VA_ARGS__)
#else
#define ASLogInfo(...)
#endif

/*
 * Utility to print messages that are used during debuging. It is recommended to
 * use this macro rather that using ASLog() directly.
 */
#if ASLoggingThreshold <= kLogLevelDebug
#define ASLogDebug(...) ASFormattedLog(kLogLevelDebug, @"DEBUG", __VA_ARGS__)
#else
#define ASLogDebug(...)
#endif

/*
 * Utility to print messages that are not really important.
 * It is recommended to use this macro rather that using ASLog() directly.
 */
#if ASLoggingThreshold <= kLogLevelVerbose
#define ASLogVerbose(...) ASFormattedLog(kLogLevelVerbose, @"VERBOSE",\
__VA_ARGS__)
#else
#define ASLogVerbose(...)
#endif


////////////////////////////////////////////////////////////////////////////////
//                  Backward compatibility Shit                               //
// Do not the code below. It will be removed soon (written on 12/9/13 by A.S) //
////////////////////////////////////////////////////////////////////////////////

#define ASLog(...) // DO NOT USE.
#define ASLogRelease(...) // DO NOT USE.
#define ASLogError(error) ASLogErrorMessage(@"Error: %@", [error localizedDescription])
#define ASLog2Debug(...) // DO NOT USE.
#define ASLog2Release(...) // DO NOT USE.

// for specific log
/***************
 1 - video player
 2- gesture recognizer
 ***************/
#define useCode 1
#define showAll NO
#define ASLog3Specific(specificCode, format , ...)
// Convinent macro for logging NSException
#define ASLogException(exception)