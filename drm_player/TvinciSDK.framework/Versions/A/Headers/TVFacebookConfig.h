//
//  TVFacebookConfig.h
//  TvinciSDK
//
//  Created by Rivka S. Peleg on 11/19/13.
//  Copyright (c) 2013 Quickode. All rights reserved.
//

#import "BaseModelObject.h"

@interface TVFacebookConfig : BaseModelObject

@property (retain, nonatomic) NSString * appID;
@property (retain, nonatomic) NSArray * scopeElements;
//@property (retain, nonatomic) NSString * APIUser;
//@property (retain, nonatomic) NSString * APIPassword;




@end
