//
//  ShadowedScrollView.h
//  YouAppi
//
//  Created by Avraham Shukron on 2/21/12.
//  Copyright (c) 2012 Quickode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShadowedScrollView : UIScrollView
@property (nonatomic , retain) IBOutlet UIView *topShadowView;
@property (nonatomic , retain) IBOutlet UIView *bottomShadowView;
@property (nonatomic , retain) IBOutlet UIView *leftShadowView;
@property (nonatomic , retain) IBOutlet UIView *rightShadowView;

@property (nonatomic , retain) IBOutlet UIView *topOverscrollGlowView;
@property (nonatomic , retain) IBOutlet UIView *leftOverscrollGlowView;
@property (nonatomic , retain) IBOutlet UIView *rightOverscrollGlowView;
@property (nonatomic , retain) IBOutlet UIView *bottomOverscrollGlowView;

@property (nonatomic , assign) CGFloat shadowRadius;

@property (nonatomic , assign) BOOL showsScrollShadows;
@property (nonatomic , assign) BOOL showsOverScrollGlow;

-(void) setup;
@end
