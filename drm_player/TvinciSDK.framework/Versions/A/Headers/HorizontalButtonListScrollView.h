//
//  RSScrollView.h
//  Halacha
//
//  Created by Yoni Colb on 5/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShadowedScrollView.h"
@interface HorizontalButtonListScrollView : ShadowedScrollView; 
@property (retain , nonatomic ) NSArray * buttons;
@property (nonatomic, assign) CGFloat margins;
@property (nonatomic, assign) NSUInteger numberOfVisibleButtons;
-(void) layoutButtons;
@end
