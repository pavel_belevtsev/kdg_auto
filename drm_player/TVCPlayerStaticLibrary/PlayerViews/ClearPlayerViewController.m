//
//  ClearPlayerViewController.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 6/19/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "ClearPlayerViewController.h"
#import "TVMediaToPlayInfo.h"
#import "ClearDrmProvider.h"

@interface ClearPlayerViewController ()
{
    MPMoviePlaybackState lastState;
    NSURL* savedUrl;
}
@property (nonatomic, retain) MPMoviePlayerController *Player;
@property (nonatomic, retain) NSArray* notificationsArr;
@property (nonatomic, retain) NSTimer * timeOutTimer;
@property (retain, nonatomic) id lastError;

-(void)setDrmType:(TVDRMType)drmType;

@end

@implementation ClearPlayerViewController


- (void)perepareNotificationsArr{
    notificationsArr =  [NSArray arrayWithObjects:MPMoviePlayerNowPlayingMovieDidChangeNotification,
                         MPMoviePlayerReadyForDisplayDidChangeNotification,
                         MPMoviePlayerPlaybackDidFinishReasonUserInfoKey,
                         MPMoviePlayerPlaybackStateDidChangeNotification,
                         MPMoviePlayerWillEnterFullscreenNotification,
                         MPMoviePlayerLoadStateDidChangeNotification,
                         MPMoviePlayerDidEnterFullscreenNotification,
                         MPMoviePlayerWillExitFullscreenNotification,
                         MPMoviePlayerDidExitFullscreenNotification,
                         MPMoviePlayerPlaybackDidFinishNotification,
                         MPMovieDurationAvailableNotification,MPMoviePlayerPlaybackDidFinishNotification, nil];

}

- (void)Initializations
{

    self.Player = [[[MPMoviePlayerController alloc] init] autorelease];
    [self setupPlayer];
}


- (void)setupPlayer
{
    [self setDrmType:TVDRMTypeClear];

    [self registerPlayerNotifications];
    
    if (self.mediaToPlayInfo.mediaItem != nil)
    {
        TVFile * file = [self.mediaToPlayInfo currentFile];
        [self.Player setContentURL:file.fileURL];
    }
    
    self.Player.view.backgroundColor = [UIColor clearColor];
    [self.Player.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.Player setScalingMode:MPMovieScalingModeAspectFit];
    self.Player.controlStyle = MPMovieControlStyleNone;
    [self.Player.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.Player.view];
    CGRect frame = self.Player.view.frame;
    ASLog2Debug(@"frame:  W- %f   H- %f", frame.size.width, frame.size.height);
    
}

- (DrmProviderBase *)getDrmProvider {
    ClearDrmProvider* provider = [[ClearDrmProvider alloc] init];
    return provider;
}

#pragma mark - Initializations and View lifecycle

- (void)dealloc
{
    [self unregisterNotifications];
    [notificationsArr release];
//    [self.Player release];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self Initializations];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    isFirst = YES;
    ASLog2Debug(@"W: %f    H: %f", self.view.frame.size.width, self.view.frame.size.height);
    [self.Player setShouldAutoplay:NO];
}


//-(void)viewWillDisappear:(BOOL)animated {
//    [self Stop];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Override


- (void)preparePlayerForMediaItem:(TVMediaToPlayInfo *)MediaItemToPlayInfo
{
    TVFile * file = [MediaItemToPlayInfo currentFile];
    [self.Player setContentURL:file.fileURL];
    [self.Player prepareToPlay];

}

//- (void)SwitchToMediaItem:(TVMediaItem*)MediaItem {
//    ASLog2Debug(@"SwitchToMediaItem");
//    [super SwitchToMediaItem:MediaItem];
//    [self preparePlayerForMediaItem:MediaItem];
//}

-(void)  playMedia:(TVMediaToPlayInfo *)mediaToPlayInfo
{
    [super playMedia:mediaToPlayInfo];
    [self preparePlayerForMediaItem:mediaToPlayInfo];
}


- (void)Play {
    MPMoviePlayerController* m_player =  self.Player;

    if (m_player.playbackState != MPMoviePlaybackStatePaused &&
        m_player.playbackState != MPMoviePlaybackStateStopped){
        return;
    }

    [super Play];
    [m_player play];
    
}

- (void)Pause {
    MPMoviePlayerController* m_player =  self.Player;
    if (m_player.playbackState != MPMoviePlaybackStatePlaying){
        return;
    }
    [super Pause];
    [m_player pause];
}

- (void)Resume {
    MPMoviePlayerController* m_player =  self.Player;
    if (m_player.playbackState != MPMoviePlaybackStatePaused){
        return;
    }
    [super Resume];
    [self Play];
}

- (void)Stop{
    MPMoviePlayerController* m_player =  self.Player;
    if (m_player.playbackState == MPMoviePlaybackStateStopped){
        return;
    }
    [super Stop];
    [self.Player stop];
}

- (TVCSeekResult)seekToTimeFloat:(float)time {
    [super seekToTimeFloat:time];
    MPMoviePlayerController* m_player =  self.Player;
    [m_player setCurrentPlaybackTime:time];
    return TVCSeekResult_OK;
}

- (TVCSeekResult)seekToTime:(TimeStruct)time {
    [super seekToTime:time];

    [self.Player setCurrentPlaybackTime:0];
    return TVCSeekResult_OK;

}

- (void)cleanPlayer {
    [super cleanPlayer];
    self.Player.contentURL = nil;
    [self.Player prepareToPlay];
}


//- (void)Kill{
//    [super Kill];
//    if (self.Player) {
//
//        [self.Player stop];
////        [self.Player prepareToPlay];
//        [self.Player.view removeFromSuperview];
//    }
//}

- (float)getCurrentPlaybackTime {
    return self.Player.currentPlaybackTime;
}



- (TimeStruct)getCurrentPlaybackTimeTimeStruct {
    float currPlaybackTime = [self getCurrentPlaybackTime];
    int secs = (int)currPlaybackTime%60;
    int mins = (int)(currPlaybackTime/60)%60;
    int hrs = (int)(currPlaybackTime/3600);
    TimeStruct timeStrct;
    timeStrct.hours = hrs;
    timeStrct.minutes = mins;
    timeStrct.seconds = secs;
    
    return timeStrct;

}

- (float)getPlaybackDuration {
    ASLog2Debug(@"%f", self.Player.duration);
    ASLog2Debug(@"%f", self.Player.playableDuration);
    return self.Player.duration;
}

- (void)setupDrmManager {

}

- (void)setFrame:(CGRect)frame{
//    [super setFrame:frame];
    [self.view setFrame:frame];
    [self.Player.view setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
}

- (TimeStruct)convertFromFloatToTimeStruct:(float)time {
    int seconds = (int)time%60;
    int minutes = (int)(time/60)%60;
    int hours = (int)(time/3600);
    TimeStruct timeStrct;
    timeStrct.hours = hours;
    timeStrct.minutes = minutes;
    timeStrct.seconds = seconds;
    
    return timeStrct;

}

- (TimeStruct)getPlaybackDurationInStructFormat {
    float time = [self getPlaybackDuration];
    return [self convertFromFloatToTimeStruct:time];
}

- (TimeStruct)getCurrentPlaybackInStructFormat {
    float time = [self getCurrentPlaybackTime];
    return [self convertFromFloatToTimeStruct:time];
}

- (void)reportCurrentTime {
    int currPlaybackTime = [self getCurrentPlaybackTime];

    int seconds = (int)currPlaybackTime%60;
    int minutes = (int)(currPlaybackTime/60)%60;
    int hours = (int)(currPlaybackTime/3600);
    TimeStruct timeStrct;
    timeStrct.hours = hours;
    timeStrct.minutes = minutes;
    timeStrct.seconds = seconds;
}

#pragma mark - Notifications

- (void)unregisterNotifications{
    for (NSString* notification in notificationsArr) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:notification    object:nil];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification    object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification    object:nil];
}

- (void)registerPlayerNotifications
{
    [self unregisterNotifications];
    [self perepareNotificationsArr];
    for (NSString* notification in notificationsArr) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(MPMovieNotification:)
                                                     name:notification
                                                   object:nil];
    }
    self.notificationsArr = nil;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:NULL];

}

- (void)MPMovieNotification:(NSNotification*)aNotification{

    MPMoviePlayerController *currentPlayer = (MPMoviePlayerController *) [aNotification object];
    ASLog2Debug(@"currentPlayer: %@     self.m_player: %@", currentPlayer, self.Player);
    if (currentPlayer != self.Player) {
        ASLogDebug(@"return");
        return;
        
    }
    
    ASLog2Debug(@"MPMovieNotification: %@", aNotification.name);
    NSError *error = [[aNotification userInfo] objectForKey:@"error"];
    if (error)
    {
        ASLogDebug(@"Did finish with error: %@", error);
    }
    
    @synchronized(self)
    {
        MPMovieErrorLog * errorLog = self.Player.errorLog;
        if (errorLog && self.lastError!= [errorLog.events lastObject])
        {
            self.lastError = [errorLog.events lastObject];
            
            if (self.lastError != nil)
            {
                [self.delegate player:self didDetectError:@{@"errorLog":errorLog}];
            }
        }
    }
    
    if ([aNotification.name isEqualToString:MPMoviePlayerReadyForDisplayDidChangeNotification])
    {
        ASLog2Debug(@"moviePlayerReadyForDisplay: %d", self.m_player.playbackState);
        [self handleDurationAvailable];

    }
    else if ([aNotification.name isEqualToString:MPMoviePlayerDidExitFullscreenNotification])
    {
        ASLog2Debug(@"moviePlayerDidExitFullScreen");
    }
    else    if ([aNotification.name isEqualToString:MPMoviePlayerWillExitFullscreenNotification])
    {
        ASLog2Debug(@"moviePlayerWillExitFullScreen");
    }
    else    if ([aNotification.name isEqualToString:MPMoviePlayerDidEnterFullscreenNotification])
    {
        ASLog2Debug(@"moviePlayerDidEnterFullScreen");
    }
    else    if ([aNotification.name isEqualToString:MPMoviePlayerWillEnterFullscreenNotification])
    {
        ASLog2Debug(@"moviePlayerWillEnterFullScreen");
    }
    else    if ([aNotification.name isEqualToString:MPMoviePlayerNowPlayingMovieDidChangeNotification])
    {
        if(currentPlayer.playbackState == MPMoviePlaybackStatePlaying)
        {
            ASLog2Debug(@"moviePlayerNowPlayingDidChange");
        }
    }
    else if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackStateDidChangeNotification])
    {
        switch (currentPlayer.playbackState)
        {
            case MPMoviePlaybackStateStopped:
                ASLog2Debug(@"MPMoviePlaybackStateStopped");
                break;
            case MPMoviePlaybackStatePlaying:
                ASLog2Debug(@"MPMoviePlaybackStatePlaying");
                break;
            case MPMoviePlaybackStatePaused:
                ASLog2Debug(@"MPMoviePlaybackStatePaused");
                break;
            case MPMoviePlaybackStateInterrupted:
                ASLog2Debug(@"MPMoviePlaybackStateInterrupted");
                break;
            case MPMoviePlaybackStateSeekingForward:
                ASLog2Debug(@"MPMoviePlaybackStateSeekingForward");
                break;
            case MPMoviePlaybackStateSeekingBackward:
                ASLog2Debug(@"MPMoviePlaybackStateSeekingBackward");
                break;
                
            default:
                break;
        }

    }
    else if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey])
    {
        ASLog2Debug(@"moviePlayBackDidFinishWithUserInfo");
    }
    else if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackDidFinishNotification])
    {
        ASLog2Debug(@"moviePlayBackDidFinish");
        //  Check if movie play reched to the end, and perform -movieFinished if so.
        [self handlePlaybackFinished:currentPlayer];
    }
    else if ([aNotification.name isEqualToString:MPMoviePlayerLoadStateDidChangeNotification])
    {
        [self MPMoviePlayerLoadStateDidChange:aNotification];
        ASLog2Debug(@"MPMoviePlayerLoadStateDidChangeNotification");
        [self handleBuffering];
    }
    else if ([aNotification.name isEqualToString:MPMovieDurationAvailableNotification])
    {
        ASLog2Debug(@"MPMovieDurationAvailableNotification");
        [self handleDurationAvailable];
    }
}


-(float)getCurrentPlaybackTimeInSeconds
{
    return [self getCurrentPlaybackTime];
}

-(float)currentPlaybackTime
{
    return [self getCurrentPlaybackTime];
}

#pragma Subtitles & Multi Language

-(NSArray *)getSubtitlesList
{
    [super getSubtitlesList];
    return nil;
}

- (void)changeSubtitlesLanguageTo:(NSInteger)lan
{
    [super changeSubtitlesLanguageTo:lan];
}

- (void)turnSubtitle:(BOOL)on
{
    [super turnSubtitle:on];
}

#pragma mark - Multi Audio

- (void) changeAudioLanguageTo:(NSInteger) language
{
    [super changeAudioLanguageTo:language];
}

- (NSArray *)getAudioLanguagesList {
    [super getAudioLanguagesList];
    return nil;
}

- (void)prepareToPlayWithFileURL:(NSURL*)url {
    
    [self.Player setContentURL:url];
    self.Player.initialPlaybackTime = -1.0;
    
    [self.Player prepareToPlay];
    self.Player.shouldAutoplay = NO;
    isFirst = YES;
    
    
}

#pragma mark -  MPMoviePlayerNotifications

- (void)handleDurationAvailable
{
    if (isFirst) {
        isFirst = NO;
        
       // [self DrmFinishedSuccesfullyForMediaItem:self.mediaToPlayInfo.mediaItem];
        if (self.mediaToPlayInfo.startTime > 0) {
            [self seekToTimeFloat:self.mediaToPlayInfo.startTime];
        }
    }
}

- (void)handlePlaybackFinished:(MPMoviePlayerController *)currentPlayer {
    if ((currentPlayer.currentPlaybackTime >= (currentPlayer.duration-1)) && (currentPlayer.duration != 0)) {
        ASLog2Debug(@"movie reached to END");
        [self performSelectorOnMainThread:@selector(movieFinished) withObject:nil waitUntilDone:NO];
    }
}


- (void)detectError:(NSNotification*)aNotification{
    MPMoviePlayerController *pl = (MPMoviePlayerController *) [aNotification object];
    
    MPMovieErrorLog *errLog = [pl errorLog];
    if(errLog)
    {
        NSString *errorDataStr = [[NSString alloc] initWithData:errLog.extendedLogData encoding:NSUTF8StringEncoding];
        ASLogDebug(@"**************************************** Error log: %@ ", errorDataStr);
        [errorDataStr release];
        
        NSArray *errEvents = errLog.events;
        for(MPMovieErrorLogEvent *errEvent in errEvents)
        {
            ASLogDebug(@"**************************************** %d,%@",errEvent.errorStatusCode, errEvent.errorComment);////////
        }
    }
    
}

- (void)MPMoviePlayerLoadStateDidChange:(NSNotification *)notification
{
    if (self.timeOutSeconds != 0)
    {
        if ((self.Player.loadState & MPMovieLoadStateStalled) == MPMovieLoadStateStalled)
        {
            
            self.timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeOutSeconds target:self selector:@selector(timeout) userInfo:nil repeats:NO];
        }
        else
        {
            [self.timeOutTimer invalidate];
            self.timeOutTimer = nil;
        }
    }
}

- (void)handleBuffering
{
    ASLog2Debug(@"moviePreloadDidFinish loadState: %d", self.m_player.loadState);
    ASLog2Debug(@"loadState: %d", self.Player.loadState);
    if (self.Player.loadState & MPMovieLoadStateStalled) {
        [self startBuffering];
    } else if (self.Player.loadState & MPMovieLoadStatePlaythroughOK) {
        [self stopBuffering];
        [self.Player play];
    }
    
}

-(void) timeout
{
    
    if ((self.Player.loadState & MPMovieLoadStateStalled) == MPMovieLoadStateStalled)
    {
        if ([self.delegate respondsToSelector:@selector(playerDetectTimeOut:)])
        {
            [self.delegate playerDetectTimeOut:self];
        }
    }
    
}

- (TVPPlaybackState)playerStatus {
    TVPPlaybackState returnStatus = TVPPlaybackStateUnknown;
    MPMoviePlaybackState status = self.Player.playbackState;
    switch (status) {
        case MPMoviePlaybackStateStopped:
            returnStatus = TVPPlaybackStateStopped;
            break;
        case MPMoviePlaybackStatePlaying:
            returnStatus = TVPPlaybackStateIsPlaying;
            break;
        case MPMoviePlaybackStatePaused:
            returnStatus = TVPPlaybackStatePasued;
            break;
        case MPMoviePlaybackStateInterrupted:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case MPMoviePlaybackStateSeekingForward:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case MPMoviePlaybackStateSeekingBackward:
            returnStatus = TVPPlaybackStateUnknown;
            break;
            
        default:
            break;
    }
    return returnStatus;
}

- (void) applicationWillResignActive
{
    savedUrl = self.Player.contentURL;
    lastState = self.Player.playbackState;
    NSLog(@"m_player is: %@", self.Player);
}

- (void) applicationDidBecomeActive
{
    switch (lastState)
    {
        case MPMoviePlaybackStateStopped:
        {
            [self.Player stop];
        }
            break;
        case MPMoviePlaybackStatePlaying:
        {
            self.mediaToPlayInfo.startTime = self.Player.currentPlaybackTime;
            [self playMedia:self.mediaToPlayInfo];
            [self.Player prepareToPlay];
            [self.Player play];
        }
            break;
        case MPMoviePlaybackStatePaused:
            [self.Player pause];
            break;
            
        default:
        {
            self.mediaToPlayInfo.startTime = self.Player.currentPlaybackTime;
            [self playMedia:self.mediaToPlayInfo];
            [self.Player prepareToPlay];
            [self.Player play];
        }
            break;
    }
    
}

-(TVPMovieLoadState) movieLoadStatus
{
    TVPMovieLoadState  status = (TVPMovieLoadState)self.Player.loadState;
    
    return status;
}

-(void)updateLastPlayerState:(MPMoviePlaybackState) lastPlayerState
{
    lastState =lastPlayerState;
}
@end
