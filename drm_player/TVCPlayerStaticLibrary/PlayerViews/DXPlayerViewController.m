 //  DXPlayerViewController.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 6/19/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "DXPlayerViewController.h"
#import "PlayReadyDrmProvider.h"
#import "PlayReadyProgDownProvider.h"
#import "TVMediaToPlayInfo.h"
#import "DrmManager.h"
#import "TVCPltvUtils.h"
//#import <SecurePlayer/VOOSMPType.h>
//#import <SecurePlayer/VOCommonPlayerControl.h>

#if !TARGET_IPHONE_SIMULATOR



#import <SecurePlayer/VOOSMPChunkInfo.h>

@interface DXPlayerViewController () {

}
-(void)setDrmType:(TVDRMType)drmType;

@property (nonatomic, retain) VODXPlayer *m_pPlayer;

@end
#endif

#define CONVERTION_MStoSEC 1000.0


@implementation DXPlayerViewController

#if !TARGET_IPHONE_SIMULATOR
@synthesize m_pPlayer;



#pragma mark - View lifecycle and Initializations

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:NULL];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:NULL];

    if (self.m_pPlayer != nil) {
        [self.m_pPlayer stop];
        self.m_pPlayer = nil;
    }
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self Initializations];
    }
    return self;
}

- (void)Initializations{
    [self initPlayer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    fileSizeForPD = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:NULL];

}


- (void) applicationWillResignActive
{
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    
    if(pPlayerPro)
    {
        self.playerStatusBeforeResign = [self.m_pPlayer getPlayerStatus];
        
        [self Pause];
        int nStatus = [pPlayerPro suspend:true];
        if(nStatus != VO_OSMP_ERR_NONE)
        {
            //handle error
        }
    }
}

- (void) applicationDidBecomeActive
{
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    
    if(pPlayerPro)
    {
        switch (self.playerStatusBeforeResign)
        {
            case VO_OSMP_STATUS_INITIALIZING:
                break;
            case VO_OSMP_STATUS_LOADING:
                break;
            case VO_OSMP_STATUS_PLAYING:
                [self Play];
                break;
            case VO_OSMP_STATUS_PAUSED:
                break;
            case VO_OSMP_STATUS_STOPPED:
                break;
            case VO_OSMP_STATUS_MAX:
                break;
            default:
                break;
        }

        
        int nStatus = [pPlayerPro resume];
        if(nStatus != VO_OSMP_ERR_NONE)
        {
            //handle error
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//  Used to import Bundle
- (NSBundle *)frameworkBundle {
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"TVCPlayerStaticLibrary.bundle"];
        ASLog2Debug(@"frameworkBundlePath: %@", frameworkBundlePath);
        frameworkBundle = [[NSBundle bundleWithPath:frameworkBundlePath] retain];
    });
//    return [NSBundle mainBundle];
    return frameworkBundle;
}

- (void)initPlayer
{

    [self setDrmType:TVDRMTypePlayready];
    int nRet = VO_OSMP_ERR_NONE;
    
    if(!self.m_pPlayer)
	{

#warning Rivka Please pay attention this seems like we need to use  VO_OSMP_AV_PLAYER instedof of VO_OSMP_VOME2_PLAYER
        VO_OSMP_PLAYER_ENGINE playerType = VO_OSMP_VOME2_PLAYER;
        self.m_pPlayer = [[VODXPlayer alloc] init:playerType initParam:NULL];
        if (self.m_pPlayer == nil) {
            ASLog2Debug(@"Init Error: %x",nRet);
            return;
        }
        [self.m_pPlayer release];

        
        id<VOCommonPlayerConfiguration> pPlayerConfiguration = self.m_pPlayer;
#warning Very important
        NSString* licPath = [[self frameworkBundle] pathForResource:@"voVidDec.dat" ofType:nil];
        ASLog2Debug(@"licPath: %@", licPath);
        [pPlayerConfiguration setLicenseFilePath:licPath];
        
        
#warning    "VISUALON-DX-COM.YES.YESGO-F9DA0461"                    is for YESGo only.
#warning    "VISUALON-DX-EUTELSAT-563CABCE06F04FB9BC59C9E6EA18ABEE" is for Eutelsat only.
#warning    for the general key use: "VOTRUST_VISUALONPLAYER"
        
        [pPlayerConfiguration setPreAgreedLicense:@"VISUALON-DX-EUTELSAT-563CABCE06F04FB9BC59C9E6EA18ABEE"];
        NSString* cap = [[self frameworkBundle] pathForResource:@"iOS-cap.xml" ofType:nil];
        [self.m_pPlayer setDeviceCapabilityByFile:cap];
        
        id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
        [pPlayerPro setOnEventDelegate:self];
        
        nRet = [pPlayerPro setView:self.view];
        if (VO_OSMP_ERR_NONE != nRet) {
            [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"SetView Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
            return;
        }
        [self.m_pPlayer enableSubtitle:false];
        [self.m_pPlayer resetSubtitleParameter];
        [self.m_pPlayer clearSelection];


    }
    return;
}


- (void) setRectDraw:(UIInterfaceOrientation)toInterfaceOrientation
{
    [super setRectDraw:toInterfaceOrientation];
	// set draw area
//	CGRect rect		= self.view.bounds;
//	CGFloat scale	= [[UIScreen mainScreen] scale];
//    Rect cRect;
//	cRect.left		= 0;
//	cRect.top		= 0;
//	cRect.right	= rect.size.width*scale;
//	cRect.bottom	= rect.size.height*scale;
    
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    [pPlayerPro notifyViewSizeChanged];

//    [pPlayerPro setDisplayArea:cRect];
}


#pragma mark - Events
- (VO_OSMP_RETURN_CODE) onVOEvent:(VO_OSMP_CB_EVENT_ID)nID param1:(int)param1 param2:(int)param2 pObj:(void *)pObj
{
    // ASLogInfo(@"onVOEvent: Event id :%x, param1:%x ,param2 %x",nID,param1,param2);
    
    if (nID == VO_OSMP_CB_PLAY_COMPLETE)
    /** Source playback complete */
	{
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_PLAY_COMPLETE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        ASLog2Debug(@"VO_OSMP_CB_PLAY_COMPLETE");
        [self performSelectorOnMainThread:@selector(Stop) withObject:nil waitUntilDone:NO];
        [self performSelectorOnMainThread:@selector(movieFinished) withObject:nil waitUntilDone:NO];
        return VO_OSMP_ERR_NONE;
	}
	else if (nID == VO_OSMP_CB_ERROR)
	{
        ASLog2Debug(@"VO_OSMP_CB_ERROR");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_ERROR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        
//        [eventsHandler sendErrorEvent:TVCError_VO_OSMP_CB_ERROR];
		return VO_OSMP_ERR_NONE;
	}
    
	else if (nID == VO_OSMP_CB_VIDEO_START_BUFFER)
    /** Video stream started buffering */
	{
        ASLog2Debug(@"VO_OSMP_CB_VIDEO_START_BUFFER");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_VIDEO_START_BUFFER" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

        [self performSelectorOnMainThread:@selector(startProcess) withObject:nil waitUntilDone:NO];

		return VO_OSMP_ERR_NONE;
	}
	else if (nID == VO_OSMP_CB_AUDIO_START_BUFFER)
    /** Audio stream started buffering */
	{
        ASLog2Debug(@"VO_OSMP_CB_AUDIO_START_BUFFER");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_AUDIO_START_BUFFER" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}
	else if (nID == VO_OSMP_CB_VIDEO_STOP_BUFFER)
    /** Video stream stopped buffering */
	{
        ASLog2Debug(@"VO_OSMP_CB_VIDEO_STOP_BUFFER");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_VIDEO_STOP_BUFFER" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

        [self performSelectorOnMainThread:@selector(stopProcess) withObject:nil waitUntilDone:NO];

		return VO_OSMP_ERR_NONE;
	}
    else if (nID == VO_OSMP_CB_VIDEO_SIZE_CHANGED)
    /** Video size changed; param1 is video width (px), param2 is video height (px). */
	{
        ASLog2Debug(@"VO_OSMP_CB_VIDEO_SIZE_CHANGED");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_VIDEO_SIZE_CHANGED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        return VO_OSMP_ERR_NONE;
    }
    else if (nID == VO_OSMP_CB_SRC_BUFFER_TIME)
    /** Buffer time in source; param1 is (int)audio <ms> value, param2 is (int)video <ms> */
	{
        ASLog2Debug(@"VO_OSMP_CB_SRC_BUFFER_TIME");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_SRC_BUFFER_TIME" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        
		return VO_OSMP_ERR_NONE;
	}
    else if (nID == VO_OSMP_CB_SEEK_COMPLETE)
    /** Seek action complete */
	{
        ASLog2Debug(@"VO_OSMP_CB_SEEK_COMPLETE");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_SEEK_COMPLETE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_OPEN_SRC_COMPLETE){
        /** Open Source complete */
        ASLog2Debug(@"VO_OSMP_CB_OPEN_SRC_COMPLETE");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_OPEN_SRC_COMPLETE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_OPEN_FINISHED){
        ASLog2Debug(@"VO_OSMP_SRC_CB_OPEN_FINISHED");
        /** Source open finished, param1 is {@link VO_OSMP_RETURN_CODE} */
        if (param1 == VO_OSMP_ERR_NONE) {
            [self startFirstPlay];

            [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_ERR_NONE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
            
            //  If stream is Start Over, so seek to minimum position of the stream.
            if ([self isVODMode]){
                if (self.mediaToPlayInfo.startTime > 0) {
                    [self seekToTimeFloat:self.mediaToPlayInfo.startTime*1000];
                }
                [self performSelectorOnMainThread:@selector(DrmFinishedSuccesfullyForMediaItem:) withObject:self.mediaToPlayInfo.mediaItem waitUntilDone:YES];
            }
        }
        
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_OPEN_FINISHED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_PROGRAM_RESET){
        ASLog2Debug(@"VO_OSMP_SRC_CB_PROGRAM_RESET");
        /**Notify the program info has been reset in source */
         [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_PROGRAM_RESET" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_AUDIO_STOP_BUFFER){
        ASLog2Debug(@"VO_OSMP_CB_AUDIO_STOP_BUFFER");
        /** Audio stream stopped buffering */
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_AUDIO_STOP_BUFFER" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_VIDEO_ASPECT_RATIO){
        ASLog2Debug(@"VO_OSMP_CB_VIDEO_ASPECT_RATIO");
        /** Video aspect ratio changed; param1 is {@link VO_OSMP_ASPECT_RATIO}.*/
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_VIDEO_ASPECT_RATIO" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_CODEC_NOT_SUPPORT){
        ASLog2Debug(@"VO_OSMP_CB_CODEC_NOT_SUPPORT");
        /** Codec not supported */
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_CODEC_NOT_SUPPORT" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_DEBLOCK){
        /** Video codec deblock event; param1 is <0:disable|1:enable>. */
        ASLog2Debug(@"VO_OSMP_CB_DEBLOCK");
         [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_DEBLOCK" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_HW_DECODER_STATUS){
        /** Hardware decoder status available event; param1 is <0: error|1: available>. */
        ASLog2Debug(@"VO_OSMP_CB_HW_DECODER_STATUS");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_HW_DECODER_STATUS" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_AUTHENTICATION_RESPONSE){
        /** Authentication response information, e.g. report information from server;
         *  param obj is String: <detailed information>. */
        ASLog2Debug(@"VO_OSMP_CB_AUTHENTICATION_RESPONSE");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_AUTHENTICATION_RESPONSE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_LANGUAGE_INFO_AVAILABLE){
        /** Subtitle language info is parsed and available */
        ASLog2Debug(@"VO_OSMP_CB_LANGUAGE_INFO_AVAILABLE");
        [[NSNotificationCenter defaultCenter] postNotificationName:TVCSubtitlesAvaialable object:nil];
         [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_LANGUAGE_INFO_AVAILABLE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_VIDEO_RENDER_START){
        /** video render started */
        ASLog2Debug(@"VO_OSMP_CB_VIDEO_RENDER_START");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_VIDEO_RENDER_START" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_BLUETOOTHHANDSET_CONNECTION){
        /** Bluetooth handset status; param1 is <0: bluetooth off|1: bluetooth on>.
         *  Not currently implemented on iOS.*/
        ASLog2Debug(@"VO_OSMP_CB_BLUETOOTHHANDSET_CONNECTION");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_BLUETOOTHHANDSET_CONNECTION" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_CB_ANALYTICS_INFO){
        /** Playback performance analytics info available; param obj is {@link VOOSMPAnalyticsInfo}. */
        ASLog2Debug(@"VO_OSMP_CB_ANALYTICS_INFO");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_CB_ANALYTICS_INFO" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTING){
        /**
         * Enumeration of source callback event IDs
         */
        /** Source is connecting */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTING");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTING" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTION_FINISHED){
        /** Source connection is finished */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTION_FINISHED");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTION_FINISHED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
       
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTION_TIMEOUT){
        /** Source connection is finished */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTION_TIMEOUT");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTION_TIMEOUT" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTION_LOSS){
        /** Source connection lost */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTION_LOSS");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTION_LOSS" group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DOWNLOAD_STATUS){
        /** HTTP download status, param 1 is int (0 - 100) */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DOWNLOAD_STATUS");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DOWNLOAD_STATUS" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTION_FAIL){
        /** Source connection failed */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTION_FAIL");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTION_FAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DOWNLOAD_FAIL){
        /** Source download failed */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DRM_FAIL){
        /** DRM engine error */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DRM_FAIL");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DRM_FAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_PLAYLIST_PARSE_ERR){
        /** Playlist parse error */
        ASLog2Debug(@"VO_OSMP_SRC_CB_PLAYLIST_PARSE_ERR");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_PLAYLIST_PARSE_ERR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CONNECTION_REJECTED){
        /** Maximum number of connections was reached. Currently used for RTSP only. */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CONNECTION_REJECTED");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CONNECTION_REJECTED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_BA_HAPPENED){
        /** Bitrate is changed; param1 is new bitrate <bps> */
        ASLog2Debug(@"VO_OSMP_SRC_CB_BA_HAPPENED");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_BA_HAPPENED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DRM_NOT_SECURE){
        /** Device is rooted/jailbroken, DRM not secure. Not currently implemented on iOS.*/
        ASLog2Debug(@"VO_OSMP_SRC_CB_DRM_NOT_SECURE");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DRM_NOT_SECURE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DRM_AV_OUT_FAIL){
        /** Device uses A/V output device but the license doesn't support it */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DRM_AV_OUT_FAIL");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DRM_AV_OUT_FAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DOWNLOAD_FAIL_WAITING_RECOVER){
        /** Download failed, and is waiting to recover */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL_WAITING_RECOVER");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL_WAITING_RECOVER" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_DOWNLOAD_FAIL_RECOVER_SUCCESS){
        /** Download recovery success */
        ASLog2Debug(@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL_RECOVER_SUCCESS");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_DOWNLOAD_FAIL_RECOVER_SUCCESS" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_CUSTOMER_TAG){
        /** Customer tag information available inside source; param1 is  {@link VO_OSMP_SRC_CUSTOMERTAGID},
         *  and other params will depend on param1.
         */
        ASLog2Debug(@"VO_OSMP_SRC_CB_CUSTOMER_TAG");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_CUSTOMER_TAG" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_INFO){
        /** Streaming information; param1 is {@link VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT} and
         *  other params will depend on param1.
         */
        ASLog2Debug(@"VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_INFO: %d", param1);

        switch (param1) {
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_TYPE:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_TYPE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DROPPED:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DROPPED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_BEGINDOWNLOAD:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_BEGINDOWNLOAD" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DOWNLOADOK:
                
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DOWNLOADOK" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
            
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PLAYLIST_DOWNLOADOK:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PLAYLIST_DOWNLOADOK" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_CHANGE:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_CHANGE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_FILE_FORMATSUPPORTED:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_FILE_FORMATSUPPORTED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_LIVESEEKABLE:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_LIVESEEKABLE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                ASLog2Debug(@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_LIVESEEKABLE");
                [self performSelectorOnMainThread:@selector(DrmFinishedSuccesfullyForMediaItem:) withObject:self.mediaToPlayInfo.mediaItem waitUntilDone:YES];

                if ([self isPPMode]) {
                    //[self seekToTimeFloat:self.mediaToPlayInfo.startTime];
                    [self Play];
                    [self Pause];
                    [self stopProcess];
                }else if ([self isStartOverMode]) {
                    [self seekToTimeFloat:0.0];
                    [self Play];
                    [self stopProcess];
                }else if ([self isTrickPlay]) {
                    [self seekToTimeFloat:self.mediaToPlayInfo.startTime];
                    [self Play];
                    [self stopProcess];
                }
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFOEVENT_MAX:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFOEVENT_MAX" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;

            default:
                break;
        }

        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_INFO" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        if (param1 == VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE)
        {
            ASLog2Debug(@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE: %d", param2);
            if ([self.delegate respondsToSelector:@selector(moviePlaybackBitrateChangedFor:withNewBitrate:outOfBitratesArr:)]) {
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                    [self.delegate moviePlaybackBitrateChangedFor:self.mediaToPlayInfo.mediaItem withNewBitrate:param2 outOfBitratesArr:[self getBitratesArray]];
                });
            }
            [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        }
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_ADAPTIVE_STREAM_WARNING){
        /** Adaptive streaming error warning, param1 is {@link VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT} and
         *  other params will depend on param1.
         */
        ASLog2Debug(@"VO_OSMP_SRC_CB_ADAPTIVE_STREAM_WARNING");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_ADAPTIVE_STREAM_WARNING" group:LOG_GROUP_PLAYER level:LOG_LEVEL_WARN];

        switch (param1) {
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_DOWNLOADERROR:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_DOWNLOAD_ERROR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_FILEFORMATUNSUPPORTED:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_FILEFORMAT_UNSUPPORTED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_DRMERROR:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_CHUNK_DRMERROR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_MAX:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_WARNING_EVENT_MAX" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
                
            default:
                break;
        }
        
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_ERROR){
        /** Notify RTSP error, param1 will be defined to {@link VO_OSMP_SRC_RTSP_ERROR} */
        ASLog2Debug(@"VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_ERROR");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_ERROR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
        switch (param1) {
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_PLAYLIST_PARSEFAIL:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_PLAYLIST_PARSEFAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_PLAYLIST_UNSUPPORTED:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_PLAYLIST_UNSUPPORTED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;   
            case VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_STREAMING_DOWNLOADFAIL:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_STREAMING_DOWNLOADFAIL" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
            case VO_OSMP_SRC_ADAPTIVESTREAMING_ERROR_EVENT_MAX:
                [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_ADAPTIVESTREAMING_ERROR_EVENT_MAX" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
                break;
                
            default:
                break;
        }

		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_RTSP_ERROR){
        /** Notify seek complete, param1 is the seek return timestamp (ms),
         * param2 is the seek result return code,
         * {@link VO_OSMP_SRC_ERR_SEEK_FAIL} or {@link VO_OSMP_ERR_NONE}*/
        ASLog2Debug(@"VO_OSMP_SRC_CB_RTSP_ERROR");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_RTSP_ERROR" group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_SEEK_COMPLETE){
        ASLog2Debug(@"VO_OSMP_SRC_CB_SEEK_COMPLETE");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_SEEK_COMPLETE" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
	}else if (nID == VO_OSMP_SRC_CB_PROGRAM_CHANGED){
        /**Notify the program info has been reset in source */
        ASLog2Debug(@"VO_OSMP_SRC_CB_PROGRAM_CHANGED");
        [[TVLogManager sharedLogManager] sendLogMessage:@"VO_OSMP_SRC_CB_PROGRAM_CHANGED" group:LOG_GROUP_PLAYER level:LOG_LEVEL_DEBUG];
		return VO_OSMP_ERR_NONE;
    }

    
    return VO_OSMP_ERR_IMPLEMENT;
}

- (void)startProcess
{
    [self startBuffering];
}

- (void)stopProcess
{
    [self stopBuffering];
}

#pragma mark - Player operations

- (DrmProviderBase *)getDrmProvider {
    DrmProviderBase * provider = nil;
    if (self.mediaToPlayInfo.isProgressiveDownload) {
        provider = [[PlayReadyProgDownProvider alloc] init];
    }else{
        provider = [[PlayReadyDrmProvider alloc] init];
    }
    return provider;
}

- (int)startFirstPlay
{
    int nRet = VO_OSMP_ERR_POINTER;
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    nRet = [pPlayerPro start];
    
    if (nRet != VO_OSMP_ERR_NONE) {
        [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Start Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
        return nRet;
    }
    
    return nRet;
}

- (void)startPlayer
{
    int nRet = VO_OSMP_ERR_POINTER;

    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    int nStatus = [self.m_pPlayer getPlayerStatus];

    if (nStatus == VO_OSMP_STATUS_PAUSED)
    {
        nRet = [pPlayerPro start];
        if (VO_OSMP_ERR_NONE != nRet) {
            [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
            return;
        }
    }
    else
        if(nStatus == VO_OSMP_STATUS_PLAYING)
    {
        nRet = [pPlayerPro pause];
        if (VO_OSMP_ERR_NONE != nRet) {
            ASLog2Debug(@"Pause Error:%x",nRet);
            return;
        }
    }
}


- (void)pausePlayer{
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    int nStatus = [pPlayerPro getPlayerStatus];
    int nRet = VO_OSMP_ERR_POINTER;
    if(nStatus == VO_OSMP_STATUS_PLAYING)
    {
        nRet = [pPlayerPro pause];
        if (VO_OSMP_ERR_NONE != nRet) {
             [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
            return;
        }
    }

}

- (void)stopPlayer{
    
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    int playerStatuse = [self.m_pPlayer getPlayerStatus];
	if (pPlayerPro && playerStatuse != VO_OSMP_STATUS_STOPPED && playerStatuse != VO_OSMP_STATUS_INITIALIZING) {
		int nRet = [pPlayerPro stop];
        if (VO_OSMP_ERR_NONE != nRet) {
            [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];

            ASLog2Debug(@"Stop Error:%x",nRet);
            ASLog2Debug(@"Cannot Stop Content");
            return;
        }
        nRet = [pPlayerPro close];
        if (VO_OSMP_ERR_NONE != nRet) {
 
            [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
            
            ASLog2Debug(@"Close Error:%x",nRet);
            ASLog2Debug(@"Cannot Close Content");
            return;
        }
	}
}

- (int)openFile:(const char*)pszFile
{
    int nRet = VO_OSMP_ERR_POINTER;

	if (self.m_pPlayer)
	{
        id<VOCommonPlayerConfiguration> pPlayerConfig = self.m_pPlayer;
        [pPlayerConfig enableDeblock:TRUE];
		
        int nFlag = VO_OSMP_FLAG_SRC_OPEN_ASYNC;
        
        VOOSMPOpenParam* voOpenParam = nil;
        if (fileSizeForPD > 0){
            voOpenParam = [[VOOSMPOpenParam alloc] init];
            voOpenParam.fileSize = fileSizeForPD;
        }
        VO_OSMP_SRC_FORMAT sourceType = VO_OSMP_SRC_AUTO_DETECT;
        NSString* fileStr = [NSString stringWithUTF8String:pszFile];
        
        id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
        
        [pPlayerPro close];
                
        nRet = [pPlayerPro open:fileStr flag:nFlag sourceType:sourceType openParam:voOpenParam];
        
        
        if (nRet != VO_OSMP_ERR_NONE)
        {
            [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
            
            return nRet;
        }

        ASLog2Debug(@"******** discretix open file succeded");
        [voOpenParam release];
    }
	return nRet;
}


- (VO_OSMP_STATUS)getPlayerStatus{
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;
    int nStatus = [pPlayerPro getPlayerStatus];
    
    return nStatus;
}

- (NSArray *)getBitratesArray
{
    TVFile * file = [self.mediaToPlayInfo currentFile];

    
    NSString* fileName = [NSString stringWithFormat:@"%lu.%@",(unsigned long)[[file.fileURL absoluteString] hash], [[file.fileURL absoluteString] pathExtension]];
    ASLogInfo(@"fileName = %@",fileName);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *localFileName = [documentsDirectory stringByAppendingPathComponent:fileName];

    NSString * fileContents = [NSString stringWithContentsOfFile:localFileName encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray * bandwithes = [NSMutableArray array];

    if (fileContents != nil) {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"BANDWIDTH=[0-9]+"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];

        [regex enumerateMatchesInString:fileContents options:0 range:NSMakeRange(0, [fileContents length]) usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop){
            // your code to handle matches here
            NSString* str = [fileContents substringWithRange:match.range];
            str = [str stringByReplacingOccurrencesOfString:@"BANDWIDTH=" withString:@""];
            str = [str stringByReplacingOccurrencesOfString:@"," withString:@""];
            [bandwithes addObject:[NSNumber numberWithInteger:[str integerValue]]];
        }];
        NSSortDescriptor* lowestToHighest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
        [bandwithes sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighest]];
        
        // set the application to considering only the second bit rate leve;
//         setBitrateThreshold
//        NSInteger lowestBandwith = [[bandwithes firstObject] integerValue];
//        NSInteger highestBandwith =  bandwithes.count>1?[bandwithes[1] integerValue]:[[bandwithes lastObject] integerValue];
//        [self.m_pPlayer setBitrateThreshold:highestBandwith lower:lowestBandwith];
        
    }
    return bandwithes;
}

#pragma mark - Override

- (void)Play {
    VO_OSMP_STATUS status = [self getPlayerStatus];
    if (status == VO_OSMP_STATUS_PLAYING || status == VO_OSMP_STATUS_INITIALIZING)
        return;
    
    [super Play];
    [self performSelectorOnMainThread:@selector(startPlayer) withObject:nil waitUntilDone:YES];
}

- (void)Pause {
    VO_OSMP_STATUS status = [self getPlayerStatus];
    if (status != VO_OSMP_STATUS_PLAYING || status == VO_OSMP_STATUS_INITIALIZING)
        return;
    [super Pause];
    [self performSelectorOnMainThread:@selector(pausePlayer) withObject:nil waitUntilDone:YES];
}

- (void)Resume {
    VO_OSMP_STATUS status = [self getPlayerStatus];
    if (status != VO_OSMP_STATUS_PAUSED || status == VO_OSMP_STATUS_INITIALIZING)
        return;
    [super Resume];
    [self performSelectorOnMainThread:@selector(startPlayer) withObject:nil waitUntilDone:YES];
}

- (void)Stop{
    VO_OSMP_STATUS status = [self getPlayerStatus];
    if (status == VO_OSMP_STATUS_STOPPED || status == VO_OSMP_STATUS_INITIALIZING)
        return;
    [super Stop];
    [self performSelectorOnMainThread:@selector(stopPlayer) withObject:nil waitUntilDone:YES];
}

- (TVCSeekResult)seekToTimeFloat:(float)time {
    [super seekToTimeFloat:time];
    
    if ([self isStartOverMode] || [self isPPMode] || [self isTrickPlay]) {

        //   Available time
        long nBufferSize = [self.m_pPlayer getMinPosition];
        if (time > labs(nBufferSize)) {
            return TVCSeekResult_OutOfRange;
        }
        
        VODXPlayer* pPlayer = self.m_pPlayer;
        long seekToPlace = nBufferSize + time;
        ASLog2Debug(@"seekToPlace:  %ld", seekToPlace);
        [pPlayer setPosition:seekToPlace];
        return TVCSeekResult_OK;
    }
    if (time < 0) {
        return TVCSeekResult_CannotSeek;
    }
    VODXPlayer* pPlayer = self.m_pPlayer;
    [pPlayer setPosition:time];
    return TVCSeekResult_OK;
}

- (TVCSeekResult)seekToTime:(TimeStruct)time {
    [super seekToTime:time];
    return TVCSeekResult_CannotSeek;
}

- (TVPPlaybackState)playerStatus {
    TVPPlaybackState returnStatus = TVPPlaybackStateUnknown;
    VO_OSMP_STATUS status = [self getPlayerStatus];
    switch (status) {
        case VO_OSMP_STATUS_INITIALIZING:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case VO_OSMP_STATUS_LOADING:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case VO_OSMP_STATUS_PLAYING:
            returnStatus = TVPPlaybackStateIsPlaying;
            break;
        case VO_OSMP_STATUS_PAUSED:
            returnStatus = TVPPlaybackStatePasued;
            break;
        case VO_OSMP_STATUS_STOPPED:
            returnStatus = TVPPlaybackStateStopped;
            break;
        case VO_OSMP_STATUS_MAX:
            returnStatus = TVPPlaybackStateUnknown;
            break;
            
        default:
            break;
    }
    return returnStatus;
}

- (void)kill {
    id<VOCommonPlayerControl> pPlayerPro = self.m_pPlayer;

    int nRet = [pPlayerPro stop];
    if (VO_OSMP_ERR_NONE != nRet) {
        [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
        
        ASLog2Debug(@"Stop Error:%x",nRet);
        ASLog2Debug(@"Cannot Stop Content");
        return;
    }
    nRet = [pPlayerPro close];
    if (VO_OSMP_ERR_NONE != nRet) {
        
        [[TVLogManager sharedLogManager] sendLogMessage:[NSString stringWithFormat:@"Run Error:%x",nRet] group:LOG_GROUP_PLAYER level:LOG_LEVEL_ERROR];
        
        ASLog2Debug(@"Close Error:%x",nRet);
        ASLog2Debug(@"Cannot Close Content");
        return;
    }

}

- (void)cleanPlayer {
    [super cleanPlayer];
    [self kill];
}

#pragma mark  - Mute -

- (void) forceMute
{
    [self.m_pPlayer mute];
}

- (void) unForceMute
{
    [self.m_pPlayer unmute];
}

#pragma mark - TVCPlayerProtocol

- (BOOL)isStartOverMode {

    if ([self.m_pPlayer isLiveStreaming]){
        if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_StartOver]) {
            return YES;
        }
    }
    return NO;
}


- (BOOL)isPPMode {
    
    if ([self.m_pPlayer isLiveStreaming]){
        if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_PauseAndPlay]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isTrickPlay {
    
    if ([self.m_pPlayer isLiveStreaming]){
        if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_TrickPlay]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isVODMode {
    
    if ([self.m_pPlayer isLiveStreaming]){
            return NO;
    }
    return YES;
}


- (float)getPlaybackDuration {

    //  Start Over mode.
    if ([self isStartOverMode] || [self isPPMode] || [self isTrickPlay]) {
        //  Return calculated time for Start Over mode, in <ms>.
        long playbackDuration = [TVCPltvUtils getMediaEndTime:self.mediaToPlayInfo] - [TVCPltvUtils getMediaStartTime:self.mediaToPlayInfo];
        //NSLog(@"playbackDuration: %ld", playbackDuration);
        return playbackDuration;
    }
    
    long nDuration = 0.0;
    if (![self.m_pPlayer isLiveStreaming]){
        nDuration = [self.m_pPlayer getDuration];
    }
   // NSLog(@"nDuration: %ld", nDuration);
    return nDuration;
}

- (TimeStruct)convertTimeFromFloatToTimeStruct:(float)time{
    time = abs(time);
    int seconds =  time;
    int minutes = (int)(seconds / 60);
    int hours = (int)(minutes / 60);
    minutes = minutes % 60;
    seconds = seconds % 60;
    TimeStruct timeStrct;
    timeStrct.hours = hours;
    timeStrct.minutes = minutes;
    timeStrct.seconds = seconds;

    return timeStrct;
}

- (TimeStruct)getPlaybackDurationInStructFormat {
    float time = [self getPlaybackDuration]/CONVERTION_MStoSEC;
    return [self convertTimeFromFloatToTimeStruct:time];
}

- (TimeStruct)getCurrentPlaybackInStructFormat {
    float time = [self getCurrentPlaybackTimeInSeconds];
    return [self convertTimeFromFloatToTimeStruct:time];
}

/*  Returns playaback position in <sec>*/
- (float)getCurrentPlaybackTimeInSeconds{
    if ([self isStartOverMode] || [self isPPMode] || [self isTrickPlay]) {
        //  Return calculated time for Start Over mode, in <ms>.
        long nPosition = [self.m_pPlayer getPosition];
        long nMinPosition = [self.m_pPlayer getMinPosition];
        long currentPosition = labs(nMinPosition) - labs(nPosition);
        currentPosition /= CONVERTION_MStoSEC;
//        nPosition = labs(nPosition)/CONVERTION_MStoSEC;
        return currentPosition;
    }

    VODXPlayer* pPlayer = self.m_pPlayer;
    long nPos = 0.0;
    if (![pPlayer isLiveStreaming]){
        nPos = [pPlayer getPosition];
    }
    return nPos/CONVERTION_MStoSEC;
}

/*  Returns playaback position in <ms>*/
- (float)getCurrentPlaybackTime {
    if ([self isStartOverMode] || [self isPPMode] || [self isTrickPlay]) {
        //  Return calculated time for Start Over mode, in <ms>.
        long nPosition = [self.m_pPlayer getPosition];
        long nMinPosition = [self.m_pPlayer getMinPosition];
        long currentPosition = labs(nMinPosition) - labs(nPosition);
        
        return currentPosition;
    }

    VODXPlayer* pPlayer = self.m_pPlayer;
    long nPos = 0.0;
    if (![pPlayer isLiveStreaming]){
        nPos = [pPlayer getPosition];
    }
    return nPos;
}

- (float)getAvailableStreamTime {
    if ([self isStartOverMode] || [self isPPMode] || [self isTrickPlay]) {
        long nBufferSize = [self.m_pPlayer getMinPosition];
        return labs(nBufferSize);
    }
    return 0.0;
}

- (void)prepareToPlayWithFileURL:(NSURL*)url {
    fileSizeForPD = 0;
    if (self.mediaToPlayInfo.isProgressiveDownload) {
        fileSizeForPD = [((PlayReadyProgDownProvider*)[DrmManager sharedDRMManager].drmProvider) getMfileSize];
    }

    int nRet = VO_OSMP_ERR_POINTER;
    nRet = [self openFile:[[url absoluteString] UTF8String]];
    
    if (VO_OSMP_ERR_NONE != nRet) {
        ASLog2Debug(@"Open Failed");
    }

}

- (void)setFrame:(CGRect)frame{
    [self.view setFrame:frame];
    [self setRectDraw:(UIInterfaceOrientation)self.interfaceOrientation];
}

-(void) playMedia:(TVMediaToPlayInfo *)mediaToPlayInfo
{
    [super playMedia:mediaToPlayInfo];
//    [self turnSubtitle:NO];
    // any additional code
    ASLog2Debug(@"Version:  %@", [[DxDrmManager sharedManager] getDrmVersion]);
}


#pragma mark - Subtitles & Multi Language
typedef enum {
    VIDEO       = 0,
    AUDIO       = 1,
    SUBTITLE    = 2
} ASSET_TYPE;

- (int)getDescriptionIndex:(id<VOOSMPAssetProperty>)property Asset:(ASSET_TYPE)type
{
    int index = 0;
    if (property == nil) {
        return index;
    }
    NSString *string = nil;
    switch (type) {
        case VIDEO:
            string = @"bitrate";
            break;
        case AUDIO:
            string = @"language";
            break;
        case SUBTITLE:
            string = @"language";
            break;
        default:
            break;
    }
    
    for (int i = 0; i < [property getPropertyCount]; i++) {
        if ([(NSString *)[property getKey:i] isEqualToString:string]) {
            index = i;
        }
    }
    return index;
}


-(NSArray *)getSubtitlesList {
    [super getSubtitlesList];

    NSMutableArray* arr = [[[NSMutableArray alloc] init] autorelease];
    
    for (int i = 0; i < [self.m_pPlayer getSubtitleCount] ; i++) {
        id<VOOSMPAssetProperty> property = [self.m_pPlayer getSubtitleProperty:i];
        id k =[property getValue:[self getDescriptionIndex:property Asset:SUBTITLE]];
        [arr addObject:k];
    }
    return arr;
}

- (void)changeSubtitlesLanguageTo:(NSInteger)lan {
    [super changeSubtitlesLanguageTo:lan];
    
    //  Check if using custom subtitles and not built-in ubtitles.
    if (self.isCustomSubtitlesActive) {
        return;
    }

    VO_OSMP_RETURN_CODE ret = VO_OSMP_ERR_POINTER;
    if (self.m_pPlayer != Nil) {
        [self.m_pPlayer resetSubtitleParameter];
        [self.m_pPlayer clearSelection];
        [self.m_pPlayer selectSubtitle:(int)lan];
        
        ret = [self.m_pPlayer commitSelection];
    }
}

- (void)turnSubtitle:(BOOL)on {
    [super turnSubtitle:on];
    
    if (self.isCustomSubtitlesActive) {
        [self.m_pPlayer enableSubtitle:false];
        return;
    }
    if (!on) {
        [self.m_pPlayer resetSubtitleParameter];
        [self.m_pPlayer clearSelection];
    }
    [self.m_pPlayer enableSubtitle:on];
}

#pragma mark - Multi Audio
- (void) changeAudioLanguageTo:(NSInteger) language {
    [super changeAudioLanguageTo:language];
    VO_OSMP_RETURN_CODE ret = VO_OSMP_ERR_POINTER;
    [self.m_pPlayer clearSelection];

    if ([self.m_pPlayer isAudioAvailable:language]) {
        [self.m_pPlayer selectAudio:language];
        ret = [self.m_pPlayer commitSelection];

    }
}

- (NSArray *)getAudioLanguagesList {
    [super getAudioLanguagesList];
    
    NSMutableArray* arr = [[[NSMutableArray alloc] init] autorelease];
    
    for (int i = 0; i < [self.m_pPlayer getAudioCount]; i++) {
        id<VOOSMPAssetProperty> property = [self.m_pPlayer getAudioProperty:i];
        id k =[property getValue:[self getDescriptionIndex:property Asset:AUDIO]];
        [arr addObject:k];
    }
    return arr;
}


#endif

-(void)cleanPersonalization
{
    NSLog(@"!!! cleanPersonalization !!!");
    DrmProviderBase* drmProvider = [self getDrmProvider];
    [drmProvider savePersonalized:NO];
}





@end
