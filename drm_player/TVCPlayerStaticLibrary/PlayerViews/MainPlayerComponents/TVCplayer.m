//
//  TVCplayer.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 6/19/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "TVCplayer.h"
#import "DrmManager.h"
#import "MediaMarkHandler.h"
#import "TVCSubtitlesViewController.h"
#import "TVMediaToPlayInfo.h"
#import "TvinciOperations.h"
#import "TVTimer.h"
#import "ClearDrmProvider.h"

NSString * const TVCSubtitlesAvaialable = @"TVCSubtitlesAvaialable";

@interface TVCplayer () <UIAlertViewDelegate, TVTimerProtocol, DrmManagerProtocol, MediaMarkProtocol, TVCPlayerForSubs>{

    BOOL isFirstPlay;
    BOOL shouldMonitor;
    float lastMediaMark;
}

@property (nonatomic, retain)   TVCSubtitlesViewController* subsViewController;
@property (nonatomic, retain)   TVMediaToPlayInfo * mediaToPlayInfo;
@property (nonatomic, retain)   MediaMarkHandler* mediaMarkHandler;
@property (nonatomic, retain)   TVTimer *timer;
@property (nonatomic, assign)   BOOL isSubtitleOn;
@property (nonatomic, assign, setter =  setDrmType:)   TVDRMType drmType;

@property (retain, nonatomic,readwrite) NSString * internalVersion ;


- (void)stopBuffering;
- (void)startBuffering;

- (void)prepareToPlayWithFileURL:(NSURL*)url;
- (DrmProviderBase *)getDrmProvider;
- (void)DrmFinishedSuccesfullyForMediaItem:(TVMediaItem *)MediaItm;
- (void)setProgDownloadFileSize:(int)fileSize;
-(void)setDrmType:(TVDRMType)drmType;

@end


@implementation TVCplayer

#pragma mark - Synthesizes
@synthesize delegate;
@synthesize subtitlesDelegate;
@synthesize mediaMarkHandler;

#pragma mark - View lifecycle

- (void)dealloc
{
    [self.subsViewController release];
    [self.mediaToPlayInfo release];
    [self Stop];
    self.delegate = nil;
    [self.timer stopTimer];
    [self.timer release];// = nil;
//    self.timer = nil;
    [self.mediaMarkHandler release];
//    [[DrmManager sharedDRMManager] kill];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        
        _internalVersion = [[NSString alloc] initWithFormat:@"1.1.6"];
    }
    return self;
}

- (void)setupSubtitlesView{
    
    self.subsViewController = [[TVCSubtitlesViewController alloc] init];
    
    CGRect frame = self.view.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [self.subsViewController.view setFrame:frame];
    self.subsViewController.view.backgroundColor = [ UIColor clearColor];
    [self.view addSubview:self.subsViewController.view];
    
    self.subsViewController.delegate = self;

}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.mediaMarkHandler = [[[MediaMarkHandler alloc] init] autorelease];
    [self.mediaMarkHandler setDelegate:self];
    
    self.timer = [[[TVTimer alloc] init] autorelease];
    self.timer.delegate = self;
    
    [self.view setAutoresizesSubviews:YES];
    [self setupSubtitlesView];
}

#pragma mark - TVTimerProtocol
-(void)timerTriggersWithHitNumber {
    
    //  Should not monitor playback, because of playback buffering.
//    [self getCurrentPlaybackTime];
//    if (!shouldMonitor) {
//        return;
//    }
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(monitorPlaybackTimeAtPlayer:)]) {
            [self.delegate monitorPlaybackTimeAtPlayer:self];
        }
    }
}

#pragma mark - Player methods and Abstract methods

//- (void)handleFirstPlayHitMark{
//    if (isFirstPlay) {
//        isFirstPlay = NO;
//        [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionFirstPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
//    }
////    else{
////        [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
////    }
//}

- (void)handleSubtitlesStatus {
    if (self.isSubtitleOn)
    {
        [self showSubtitles];
    }
}

- (void)Play {
    ASLog2Debug(@"Play");
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    /*   Check if regular play or first play and send event accordinglly   */
    [self handlePlayMediaMark];
    shouldMonitor = YES;
//    [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    [self handleSubtitlesStatus];
}

- (void)Pause {
    ASLog2Debug(@"Pause");
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionPause andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    [self hideSubtitles];
//    [self.timer pauseTimer];

    
}

-(void) handlePlayMediaMark
{
    if ( isFirstPlay ==YES)
    {
        isFirstPlay = NO;
        [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionFirstPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    }
    else
    {
        [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    }
}

- (void)Resume {
    ASLog2Debug(@"Resume");
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionPlay andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    [self showSubtitles];
//    [self.timer startTimer];

}

- (void)Stop {
    ASLog2Debug(@"Stop");
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    isFirstPlay = YES;
//    [self.timer pauseTimer];
    [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionStop andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    [self hideSubtitles];
    
}

- (void)StopAndDestroy {
    
    [self Stop];

}

- (TVCSeekResult)seekToTime:(TimeStruct)time {
    ASLog2Debug(@"seekToTime -  %d:%d:%d", time.hours, time.minutes, time.seconds);
    shouldMonitor = NO;
    return TVCSeekResult_OK;
}

- (TVCSeekResult)seekToTimeFloat:(float)time
{
    ASLog2Debug(@"seekToTime -  %f", time);
    shouldMonitor = NO;
    return TVCSeekResult_OK;
}

- (void)movieFinished
{
    [self.mediaMarkHandler sendMarkHitWithAction:TVMediaPlayerActionFinish andTimeInSecondsLocation:[self getCurrentPlaybackTimeInSeconds]];
    [self hideSubtitles];
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(movieFinishedPresentationWithMediaItem:atPlayer:)]) {
            [self.delegate movieFinishedPresentationWithMediaItem:self.mediaToPlayInfo.mediaItem atPlayer:self];
        }
    }
//    [self Stop];
}

- (void)prepareForNewRun
{
    [self.mediaMarkHandler setMediaToPlayInfo:self.mediaToPlayInfo];
    
    isFirstPlay = YES;
    [self startBuffering];
    
    DrmProviderBase* drmProvider;
    if (self.mediaToPlayInfo.isClearContent) {
        drmProvider = [[ClearDrmProvider alloc] init];
    }else{
        drmProvider = [self getDrmProvider];
    }
    
    drmProvider.mediaToPlayInfo = self.mediaToPlayInfo;
    [[DrmManager sharedDRMManager] StartProcess:drmProvider ForMediaItem:self.mediaToPlayInfo andDelegate:self];

}

- (BOOL)isFileValid_ForMediaItem:(TVMediaToPlayInfo *)mediaItemToPlayInfo{
    
    TVFile * fileToPlay = [mediaItemToPlayInfo currentFile];
    if ((fileToPlay == nil) || (fileToPlay.fileURL == nil) || ([fileToPlay.fileURL.absoluteString isEqualToString:@""])) {
        [self DrmFailedWithReason:TVCDrmStatus_IncorrectFileUrl];
        return NO;
    }
    return YES;
}

- (BOOL)isMediaItemValid:(TVMediaItem*)media
{
    if ((media.mediaID == nil) || ([media.mediaID isEqualToString:@""])) {
        [self DrmFailedWithReason:TVCDrmStatus_IncorrectMediaItemOrNull];
        return NO;
    }
    return YES;
}

- (void)playMedia:(TVMediaToPlayInfo *)mediaToPlayInfo
{
    [self Stop];
    
    //  The media is not filled with main data
    if (![self isMediaItemValid:mediaToPlayInfo.mediaItem])
        return;
    
    if (![self isFileValid_ForMediaItem:mediaToPlayInfo])
        return;
    
    self.mediaToPlayInfo = mediaToPlayInfo;
    [self.subsViewController setMediaItem:self.mediaToPlayInfo.mediaItem];

    [self prepareForNewRun];
}

- (void)cleanPlayer
{
    [self.timer stopTimer];
    [self.subsViewController pauseShowingSubtitles];
    self.mediaToPlayInfo = nil;
}

#pragma mark - DrmManagerProtocol

- (void)showAlert:(NSString*)message {
    UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    [alert show];
}

- (void)DrmFinishedWith:(TVCDrmStatus)status withURL:(NSURL *)url{
    if (url == nil) {
        [self DrmFailedWithReason:TVCDrmStatus_IncorrectLicensedURL];
        return;
    }
    if (status == TVCDrmStatus_OK) {
        [self prepareToPlayWithFileURL:url];
        [self Play];
//#if PLAY_READY
//        [self DrmFinishedSuccesfullyForMediaItem:self.mediaToPlayInfo.mediaItem];
//        [self performSelector:@selector(DrmFinishedSuccesfullyForMediaItem:) withObject:self.mediaToPlayInfo.mediaItem afterDelay:2];
//#endif
    }else{
        if (status != TVCDrmStatus_UNKNOWN) {
            [self DrmFailedWithReason:status];
        }
    }
}

- (void)startBuffering
{
    shouldMonitor = NO;
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(movieIsBufferingForMediaItem:)]) {
            [self.delegate movieIsBufferingForMediaItem:self.mediaToPlayInfo.mediaItem];
        }
    }
}

- (void)stopBuffering
{
    shouldMonitor = YES;
    [self.timer startTimer];
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(movieHasFinishedBufferingForMediaItem:)]) {
            [self.delegate movieHasFinishedBufferingForMediaItem:self.mediaToPlayInfo.mediaItem];
        }
    }
}

- (void)DrmFailedWithReason:(TVCDrmStatus)status{
    [self stopBuffering];
    [self hideSubtitles];
    [self.timer pauseTimer];

    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(movieProcessFailedWithStatusError:withMediaItem:atPlayer:)]) {
            [self.delegate movieProcessFailedWithStatusError:status withMediaItem:self.mediaToPlayInfo.mediaItem atPlayer:self];
        }
    }
}

- (void)DrmFinishedSuccesfullyForMediaItem:(TVMediaItem *)MediaItm{
    //NSLog(@"DrmFinishedSuccesfullyForMediaItem");
    [self stopBuffering];
    //[self handleFirstPlayHitMark];

    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(movieShouldStartPlayingWithMediaItem:atPlayer:)]) {
            [self.delegate movieShouldStartPlayingWithMediaItem:self.mediaToPlayInfo.mediaItem atPlayer:self];
        }
    }
}


#pragma mark - MediaMarkProtocol
-(float)getTimeInseconds{
    return [self getCurrentPlaybackTimeInSeconds];
}

-(void)ConcurrentDetected {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(playerDetectedConcurrentWithMediaItem:atPlayer:)]) {
            [self.delegate playerDetectedConcurrentWithMediaItem:self.mediaToPlayInfo.mediaItem atPlayer:self];
        }
    }
}

#pragma mark - Subtitles

- (void)turnSubtitle:(BOOL)on
{
    if (!self.isCustomSubtitlesActive) {
        self.isSubtitleOn = on;
        [self hideSubtitles];
        return;
    }
    
    if (self.isSubtitleOn != on )
    {
        self.isSubtitleOn = on;
        if (on)
        {
            [self showSubtitles];
        }
        else
        {
            [self hideSubtitles];
        }
    }
}

- (void)showSubtitles{
    if (self.subsViewController)
    {
        [self.subsViewController startShowingSubtitles];
    }
}

- (void) hideSubtitles{
    if (self.subsViewController) {
        [self.subsViewController pauseShowingSubtitles];
    }
}

- (void)changeSubtitlesLanguageTo:(NSInteger)lan {

    //  Check if using custom subtitles and not built-in ubtitles.
    if (self.isCustomSubtitlesActive) {
        if (self.subsViewController) {
            [self.subsViewController changeSubsLanguage:lan];
        }
    }
}


#pragma mark - Multi Audio

- (void) changeAudioLanguageTo:(NSInteger) language {
    
}

- (NSArray *)getAudioLanguagesList {

}

#pragma mark - TVCPlayerForSubs
-(float)getCurrentTime{
   return [self getCurrentPlaybackTime];
}

-(NSArray *)parseSubtitleString:(NSString *)stringToParse{
    if (self.subtitlesDelegate != nil) {
        if ([self.subtitlesDelegate respondsToSelector:@selector(parseSubtitleString:)]) {
            return [self.subtitlesDelegate parseSubtitleString:stringToParse];
        }
    }//isCustomSubtitlesActive
    return nil;
}


#pragma Subtitles & Multi Language

-(NSArray *)getSubtitlesList {
    
}

- (float)getAvailableStreamTime {
    return -1.0;
}


- (void) setRectDraw:(UIInterfaceOrientation)toInterfaceOrientation {
    
}


-(void)setDrmType:(TVDRMType)drmType {
    _drmType = drmType;
}


-(void)cleanPersonalization
{
    // implement in subclass
}

#pragma mark  - Mute -

- (void) forceMute
{
    // implement in subclass
}


/**
 * Unmute the audio.
 *
 * @return  {@link VO_OSMP_RETURN_CODE#VO_OSMP_ERR_NONE} if successful
 */
- (void) unForceMute
{
    // implement in subclass
}

-(MPMovieAccessLogEvent *) lastAccessLogEvent
{
    return  nil; // Implament in SubClass
}

-(void)updateLastPlayerState:(MPMoviePlaybackState) lastPlayerState
{
    // implament in subClass
}

- (TVPPlaybackState)playerStatus
{
    return 0; // implament in subClass
}
@end
