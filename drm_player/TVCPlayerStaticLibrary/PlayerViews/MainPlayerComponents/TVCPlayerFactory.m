//
//  TVCPlayerFactory.m
//  YES_iPhone
//
//  Created by Tarek Issa on 7/4/13.
//  Copyright (c) 2013 Tarek Issa. All rights reserved.
//

#import "TVCPlayerFactory.h"
#import "TVCplayer.h"
#import "DrmProviderBase.h"
#import "TVCDeclerations.h"

//  Here
#if !TARGET_IPHONE_SIMULATOR
    #if PLAY_READY
        #import "DXPlayerViewController.h"
    #endif
    #if WIDE_VINE
        #import "WideVinePlayerViewController.h"
    #endif
#endif


#import "ClearPlayerViewController.h"



@implementation TVCPlayerFactory
static TVCPlayerFactory *_sharedViewControllersFactory = nil;



+(TVCPlayerFactory *) playerFactory
{
    if (_sharedViewControllersFactory == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _sharedViewControllersFactory = [[TVCPlayerFactory alloc] init];
        });
    }
    return _sharedViewControllersFactory;
}

- (TVCplayer *)GetPlayerForMediaItem:(TVMediaItem *)mediaItem {

    TVDRMType type = TVDRMTypeClear;
#if PLAY_READY
    type = TVDRMTypePlayready;
#endif

#if WIDE_VINE
    type = TVDRMTypeWidevine;
#endif
    TVCplayer* player = nil;
    
    switch (type) {
        case TVDRMTypePlayready:
#if PLAY_READY
#if !TARGET_IPHONE_SIMULATOR
            player      = [[[DXPlayerViewController alloc] init] autorelease];
            break;
#endif
#endif

        case TVDRMTypeClear:
            player      = [[[ClearPlayerViewController alloc] init] autorelease];
            break;

#if WIDE_VINE
#if !TARGET_IPHONE_SIMULATOR
        case TVDRMTypeWidevine:
            player      = [[[WideVinePlayerViewController alloc] init] autorelease];
            break;
#endif
#endif
        default:
            break;
    }
    ASLog2Debug(@"player class: %@", player);


    return player;
}

- (TVCplayer *)GetPlayer {
    
    TVDRMType type = TVDRMTypeClear;
#if PLAY_READY
    type = TVDRMTypePlayready;
#endif
    
#if WIDE_VINE
    type = TVDRMTypeWidevine;
#endif
    TVCplayer* player = nil;
    
    switch (type) {
        case TVDRMTypePlayready:
#if PLAY_READY
#if !TARGET_IPHONE_SIMULATOR
            player      = [[[DXPlayerViewController alloc] init] autorelease];
            break;
#endif
#endif
            
        case TVDRMTypeClear:
            player      = [[[ClearPlayerViewController alloc] init] autorelease];
            break;
            
#if WIDE_VINE
#if !TARGET_IPHONE_SIMULATOR
        case TVDRMTypeWidevine:
            player      = [[[WideVinePlayerViewController alloc] init] autorelease];
            break;
#endif
#endif
        default:
            break;
    }
    ASLog2Debug(@"player class: %@", player);
    
    
    return player;
}

- (TVCplayer *)GetClearPlayer {

    TVCplayer* player = nil;
    player      = [[[ClearPlayerViewController alloc] init] autorelease];
    return player;
}

- (TVCplayer *)GetPlayerByType:(TVDRMType)type {

    TVCplayer* player = nil;
    switch (type) {
#if PLAY_READY
#if !TARGET_IPHONE_SIMULATOR
        case TVDRMTypePlayready:
            player      = [[[DXPlayerViewController alloc] init] autorelease];
            break;
#endif
#endif
        case TVDRMTypeClear:
            player      = [[[ClearPlayerViewController alloc] init] autorelease];
            break;
            
#if WIDE_VINE
#if !TARGET_IPHONE_SIMULATOR
        case TVDRMTypeWidevine:
            player      = [[[WideVinePlayerViewController alloc] init] autorelease];
            break;
#endif
#endif
        default:
            break;

    }
    return player;
}

@end
