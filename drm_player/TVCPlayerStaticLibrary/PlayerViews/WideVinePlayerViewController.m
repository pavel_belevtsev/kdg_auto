//
//  WideVinePlayerViewController.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/31/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "WideVinePlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WViPhoneAPI.h"
#import "WideVineDrmProvider.h"
#import "TVMediaToPlayInfo.h"

//#define self.m_player ((MPMoviePlayerController*)self.m_player)

@interface WideVinePlayerViewController ()
{
    BOOL isFirst;
    MPMoviePlaybackState lastState;
    NSURL* savedUrl;
    BOOL playerCanBeReleased;
    
}

@property (nonatomic, strong) MPMoviePlayerController* m_player;
@property (nonatomic, retain) NSArray* notificationsArr;
@property (nonatomic, retain) NSTimer * timeOutTimer;
@property (retain, nonatomic) id lastError;
-(void)setDrmType:(TVDRMType)drmType;

@end

@implementation WideVinePlayerViewController
@synthesize m_player;
@synthesize notificationsArr;


- (void)dealloc
{
//    [self Stop];
    NSLog(@"dealloc");
    [self unregisterNotifications];
    [self.m_player.view removeFromSuperview];
    
    playersCount--;
    //NSLog(@"====      playersCount %d", playersCount);
    
    [super dealloc];
}

- (void)initPlayer{
    [self registerPlayerNotifications];
    [self setupPlayer];
}

- (void)setupPlayer{

    if (self.m_player != nil) {
        return;
    }
    
    self.timeOutSeconds = 60;
    self.m_player = [[MPMoviePlayerController alloc] init];
    
    playerCanBeReleased = NO;
    
    playersCount++;
    //NSLog(@"====      playersCount++ %d", playersCount);

    [self setDrmType:TVDRMTypeWidevine];

    [self.m_player setRepeatMode:MPMovieRepeatModeNone];
    self.m_player.view.backgroundColor = [UIColor clearColor];
    [self.m_player.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.m_player setScalingMode:MPMovieScalingModeAspectFit];
    self.m_player.controlStyle = MPMovieControlStyleNone;
    [self.m_player.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    self.m_player.movieSourceType = MPMovieSourceTypeStreaming;
    [self.view addSubview:self.m_player.view];
    
    //[self.m_player.errorLog.events addObserver:self forKeyPath:@"count" options:0 context:nil];
    
}

- (void)registerPlayerNotifications{
    
    [self unregisterNotifications];

    [self perepareNotificationsArr];
    for (NSString* notification in self.notificationsArr) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(MPMovieNotification:)
                                                     name:notification
                                                   object:self.m_player];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:NULL];

}

- (void)unregisterNotifications{
    
    //if (self.m_player) {
    //    [self.m_player.errorLog.events removeObserver:self forKeyPath:@"count"];
    //}
    for (NSString* notification in self.notificationsArr) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:notification object:self.m_player];
    }
    self.notificationsArr = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification    object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification    object:nil];
     
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)perepareNotificationsArr{
    self.notificationsArr = [NSArray arrayWithObjects:MPMoviePlayerNowPlayingMovieDidChangeNotification,
                        MPMoviePlayerReadyForDisplayDidChangeNotification,
                        MPMoviePlayerPlaybackDidFinishReasonUserInfoKey,
                        MPMoviePlayerPlaybackStateDidChangeNotification,
                        MPMoviePlayerWillEnterFullscreenNotification,
                        MPMoviePlayerLoadStateDidChangeNotification,
                        MPMoviePlayerDidEnterFullscreenNotification,
                        MPMoviePlayerWillExitFullscreenNotification,
                        MPMoviePlayerDidExitFullscreenNotification,
                        MPMoviePlayerPlaybackDidFinishNotification,
                        MPMovieDurationAvailableNotification,MPMoviePlayerPlaybackDidFinishNotification, nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    isFirst = YES;
    [self initPlayer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"didReceiveMemoryWarning");
}

- (DrmProviderBase *)getDrmProvider {
    WideVineDrmProvider* provider = [[WideVineDrmProvider alloc] init];
    return provider;
}


#pragma mark - Override

- (void)Play {
    
    //NSLog(@"test Play 0");
    
    if (self.m_player.playbackState != MPMoviePlaybackStatePaused &&
        self.m_player.playbackState != MPMoviePlaybackStateStopped){
        return;
    }
    
    //NSLog(@"test Play 1");
    
    [super Play];
    
    //NSLog(@"test Play 2");
    
    //[self.m_player prepareToPlay];
    NSLog(@"- - - > Play");
    //NSLog(@"test Play 3");
    [self.m_player play];
    //NSLog(@"test Play 4");
    
}

- (void)Stop {
    if (self.m_player.playbackState == MPMoviePlaybackStateStopped){
        return;
    }
    [super Stop];
    [self.m_player stop];
    //NSLog(@"stop");
}

- (void)StopAndDestroy {
    
    NSLog(@"StopAndDestroy");
    
    [self Stop];
    
    if (self.m_player) {
        [self unregisterNotifications];
        [self.m_player.view removeFromSuperview];
        self.m_player = nil;
    }
    
    
}

//- (void)Kill {
//    [super Kill];
//}

- (void)Pause {
//    if (self.m_player.playbackState != MPMoviePlaybackStatePlaying){
//        return;
//    }
    [super Pause];
    [m_player pause];
}

- (void)Resume {
    if (self.m_player.playbackState != MPMoviePlaybackStatePaused){
        return;
    }
    [super Resume];
    [self Play];
}

- (TVCSeekResult)seekToTime:(TimeStruct)time {
    [super seekToTime:time];
    NSTimeInterval seconds = 0.0;
    seconds =+ time.seconds + (time.minutes*60) + (time.hours*60*60);
    if (seconds >= m_player.duration) {
        seconds = m_player.duration - 1;
    }
    [self.m_player setCurrentPlaybackTime:seconds];
    return TVCSeekResult_OK;

}

- (TVCSeekResult)seekToTimeFloat:(float)time {
    [super seekToTimeFloat:time];

    NSTimeInterval interval = (NSTimeInterval)time;
    if (time >= m_player.duration) {
        interval = m_player.duration - 1;
    }
    [self.m_player setCurrentPlaybackTime:interval];
    return TVCSeekResult_OK;
}

-(void)playMedia:(TVMediaToPlayInfo *)mediaToPlayInfo {
    [super playMedia:mediaToPlayInfo];
    [self registerPlayerNotifications];
}

- (void)setFrame:(CGRect)frame{
    //    [super setFrame:frame];
    [self.view setFrame:frame];
    [self.m_player.view setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
}

- (void)cleanPlayer {
    [super cleanPlayer];
    self.m_player.contentURL = nil;
    NSLog(@"== cleanPlayer");
    [self.m_player prepareToPlay];
}

- (TVPPlaybackState)playerStatus {
    TVPPlaybackState returnStatus = TVPPlaybackStateUnknown;
    MPMoviePlaybackState status = self.m_player.playbackState;
    switch (status) {
        case MPMoviePlaybackStateStopped:
            returnStatus = TVPPlaybackStateStopped;
            break;
        case MPMoviePlaybackStatePlaying:
            returnStatus = TVPPlaybackStateIsPlaying;
            break;
        case MPMoviePlaybackStatePaused:
            returnStatus = TVPPlaybackStatePasued;
            break;
        case MPMoviePlaybackStateInterrupted:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case MPMoviePlaybackStateSeekingForward:
            returnStatus = TVPPlaybackStateUnknown;
            break;
        case MPMoviePlaybackStateSeekingBackward:
            returnStatus = TVPPlaybackStateUnknown;
            break;
            
        default:
            break;
    }
    return returnStatus;
}


-(TVPMovieLoadState) movieLoadStatus
{
    TVPMovieLoadState  status = (TVPMovieLoadState)self.m_player.loadState;
    
    return status;
}

#pragma mark - TVCPlayerProtocol

- (float)getPlaybackDuration {
//    ASLog2Debug(@"duration: %f\ncurrentPlaybackTime: %f\ninitialPlaybackTime: %f\nendPlaybackTime: %f\nplayableDuration: %f\nplaybackState: %ld\n", m_player.duration, [m_player currentPlaybackTime], [m_player initialPlaybackTime], [m_player endPlaybackTime], [m_player playableDuration],(long)[m_player playbackState]);

    return self.m_player.duration;
}

- (float)getCurrentPlaybackTime {
    return self.m_player.currentPlaybackTime;
}

- (float)getCurrentPlaybackTimeInSeconds {
    return [self getCurrentPlaybackTime];
}

- (TimeStruct)convertFromFloatToTimeStruct:(float)time {
    int seconds = (int)time%60;
    int minutes = (int)(time/60)%60;
    int hours = (int)(time/3600);
    TimeStruct timeStrct;
    timeStrct.hours = hours;
    timeStrct.minutes = minutes;
    timeStrct.seconds = seconds;
    
    return timeStrct;
    
}
- (TimeStruct)getPlaybackDurationInStructFormat {
    float time = [self getPlaybackDuration];
    return [self convertFromFloatToTimeStruct:time];

}

- (TimeStruct)getCurrentPlaybackInStructFormat {
    float time = [self getCurrentPlaybackTime];
    return [self convertFromFloatToTimeStruct:time];
}

- (void)prepareToPlayWithFileURL:(NSURL*)url {
    
    if (self.m_player != nil) {
   
        if (playerCanBeReleased) {

            NSLog(@" ==== RELEASE PLAYER\nStates: %lu %ld", (unsigned long)self.m_player.loadState, (long)self.m_player.playbackState);
            
            [self.m_player stop];
            
            [self unregisterNotifications];
            [self.m_player.view removeFromSuperview];
            self.m_player = nil;
            
            playersCount--;
            NSLog(@"====      prepareToPlayWithFileURL playersCount %d", playersCount);
            
        } else {
            
            NSLog(@" ==== PLAYER NOT INITIALIZED");
        }
        
    }
    
    //NSLog(@"test 0");
    [self initPlayer];
    
    //NSLog(@"test 1");
    [self.m_player setContentURL:url];
    self.m_player.initialPlaybackTime = -1.0;
    
    //NSLog(@"test 2");
    //[self.m_player prepareToPlay];
    self.m_player.shouldAutoplay = NO;
    isFirst = YES;
 
    //NSLog(@"test 3");
    
}

#pragma mark -  MPMoviePlayerNotifications

- (void)detectError:(NSNotification*)aNotification{
    MPMoviePlayerController *pl = (MPMoviePlayerController *) [aNotification object];
    
    MPMovieErrorLog *errLog = [pl errorLog];
    if(errLog)
    {
        NSString *errorDataStr = [[NSString alloc] initWithData:errLog.extendedLogData encoding:NSUTF8StringEncoding];
        ASLogDebug(@"**************************************** Error log: %@ ", errorDataStr);
        [errorDataStr release];
        
        NSArray *errEvents = errLog.events;
        for(MPMovieErrorLogEvent *errEvent in errEvents)
        {
            ASLogDebug(@"**************************************** %d,%@",errEvent.errorStatusCode, errEvent.errorComment);////////
        }
    }

}

- (void)MPMovieNotification:(NSNotification*)aNotification{
    
    MPMoviePlayerController *currentPlayer = (MPMoviePlayerController *) [aNotification object];
        ASLog2Debug(@"currentPlayer: %@     self.m_player: %@", currentPlayer, self.m_player);
    if (currentPlayer != self.m_player) {
        ASLog2Debug(@"return");
        return;
        
    }

    ASLog2Debug(@"MPMovieNotification: %@", aNotification.name);
    
    NSError *error = [[aNotification userInfo] objectForKey:@"error"];
    if (error) {
        ASLog2Debug(@"Did finish with error: %@", error);
    }
    

    @synchronized(self)
    {
        MPMovieErrorLog * errorLog = self.m_player.errorLog;
        if (errorLog && self.lastError!= [errorLog.events lastObject])
        {
            self.lastError = [errorLog.events lastObject];
            
            if (self.lastError != nil)
            {
                [self.delegate player:self didDetectError:@{@"errorLog":errorLog}];
            }
            
        }
    }
    
    
    
    if ([aNotification.name isEqualToString:MPMoviePlayerReadyForDisplayDidChangeNotification]) {
        ASLog2Debug(@"moviePlayerReadyForDisplay: %d", self.m_player.playbackState);
        [self handleDurationAvailable];
//        [self.m_player prepareToPlay];
//        [currentPlayer play];
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerDidExitFullscreenNotification]) {
        ASLog2Debug(@"moviePlayerDidExitFullScreen");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerWillExitFullscreenNotification]) {
        ASLog2Debug(@"moviePlayerWillExitFullScreen");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerDidEnterFullscreenNotification]) {
        ASLog2Debug(@"moviePlayerDidEnterFullScreen");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerWillEnterFullscreenNotification]) {
        ASLog2Debug(@"moviePlayerWillEnterFullScreen");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerNowPlayingMovieDidChangeNotification]) {
            ASLog2Debug(@"moviePlayerNowPlayingDidChange");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackStateDidChangeNotification]) {
        switch (currentPlayer.playbackState) {
            case MPMoviePlaybackStateStopped:
                ASLog2Debug(@"MPMoviePlaybackStateStopped");
                break;
            case MPMoviePlaybackStatePlaying:
                ASLog2Debug(@"MPMoviePlaybackStatePlaying");
                break;
            case MPMoviePlaybackStatePaused:
                ASLog2Debug(@"MPMoviePlaybackStatePaused");
                break;
            case MPMoviePlaybackStateInterrupted:
                ASLog2Debug(@"MPMoviePlaybackStateInterrupted");
                break;
            case MPMoviePlaybackStateSeekingForward:
                ASLog2Debug(@"MPMoviePlaybackStateSeekingForward");
                [self.m_player endSeeking];
                break;
            case MPMoviePlaybackStateSeekingBackward:
                ASLog2Debug(@"MPMoviePlaybackStateSeekingBackward");
                [self.m_player endSeeking];
                break;
                
            default:
                break;
        }
        
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey]) {
        ASLog2Debug(@"moviePlayBackDidFinishWithUserInfo");
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerPlaybackDidFinishNotification]) {
        ASLog2Debug(@"moviePlayBackDidFinish");
        //  Check if movie play reched to the end, and perform -movieFinished if so.
        [self handlePlaybackFinished:currentPlayer];
    }else    if ([aNotification.name isEqualToString:MPMoviePlayerLoadStateDidChangeNotification]) {
        [self MPMoviePlayerLoadStateDidChange:aNotification];
        ASLog2Debug(@"MPMoviePlayerLoadStateDidChangeNotification");
        [self handleBuffering];
    }else    if ([aNotification.name isEqualToString:MPMovieDurationAvailableNotification]) {
        ASLog2Debug(@"MPMovieDurationAvailableNotification");
        [self handleDurationAvailable];
    }
}

- (void)handleDurationAvailable {
    if (isFirst) {
        isFirst = NO;
        
        [self DrmFinishedSuccesfullyForMediaItem:self.mediaToPlayInfo.mediaItem];
        if (self.mediaToPlayInfo.startTime > 0) {
            [self seekToTimeFloat:self.mediaToPlayInfo.startTime];
        }
    }
}

- (void)handleBuffering {
    NSLog(@"moviePreloadDidFinish loadState: %d", self.m_player.loadState);
    NSLog(@"loadState: %d", m_player.loadState);
    
    if (self.m_player.loadState != MPMovieLoadStateUnknown) {
        playerCanBeReleased = YES;
    }
    if (self.m_player.loadState & MPMovieLoadStateStalled) {
        [self startBuffering];
    } else if (self.m_player.loadState & MPMovieLoadStatePlaythroughOK) {
        [self stopBuffering];
        [self.m_player play];
    }

}

- (void)handlePlaybackFinished:(MPMoviePlayerController *)currentPlayer {
    if ((currentPlayer.currentPlaybackTime >= (currentPlayer.duration-1)) && (currentPlayer.duration != 0)) {
        ASLog2Debug(@"movie reached to END");
        [self performSelectorOnMainThread:@selector(movieFinished) withObject:nil waitUntilDone:NO];
    }
}

- (void) applicationWillResignActive
{
    savedUrl = self.m_player.contentURL;
    lastState = self.m_player.playbackState;
    NSLog(@"m_player is: %@", m_player);
}

- (void) applicationDidBecomeActive
{
    
    playerCanBeReleased = YES;
    
    switch (lastState)
    {
        case MPMoviePlaybackStateStopped:
        {
            [self.m_player stop];
        }
            break;
        case MPMoviePlaybackStatePlaying:
        {
            self.mediaToPlayInfo.startTime = m_player.currentPlaybackTime;
            [self playMedia:self.mediaToPlayInfo];
            NSLog(@"== applicationDidBecomeActive MPMoviePlaybackStatePlaying");
            //[self.m_player prepareToPlay];
            [self.m_player play];
        }
            break;
        case MPMoviePlaybackStatePaused:
            NSLog(@"== applicationDidBecomeActive MPMoviePlaybackStatePaused");
            [self.m_player play]; //[self.m_player pause];
            break;
            
        default:
        {
            //self.mediaToPlayInfo.startTime = m_player.currentPlaybackTime;
            //[self playMedia:self.mediaToPlayInfo];
            NSLog(@"== applicationDidBecomeActive default");
            //[self.m_player prepareToPlay];
            [self.m_player play];
        }
            break;
    }
    
}

#pragma Subtitles & Multi Language

-(NSArray *)getSubtitlesList {
    [super getSubtitlesList];
    return nil;
}

- (void)changeSubtitlesLanguageTo:(NSInteger)lan {
    [super changeSubtitlesLanguageTo:lan];
}

- (void)turnSubtitle:(BOOL)on {
    [super turnSubtitle:on];
}

#pragma mark - Multi Audio

- (void) changeAudioLanguageTo:(NSInteger) language {
    [super changeAudioLanguageTo:language];
}

- (NSArray *)getAudioLanguagesList {
    [super getAudioLanguagesList];
    return nil;
}



- (void)MPMoviePlayerLoadStateDidChange:(NSNotification *)notification
{
    if (self.timeOutSeconds != 0)
    {
        if ((self.m_player.loadState & MPMovieLoadStateStalled) == MPMovieLoadStateStalled)
        {

            self.timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:self.timeOutSeconds target:self selector:@selector(timeout) userInfo:nil repeats:NO];
        }
        else
        {
            [self.timeOutTimer invalidate];
            self.timeOutTimer = nil;
        }
    }
}


-(void) timeout
{

    if ((self.m_player.loadState & MPMovieLoadStateStalled) == MPMovieLoadStateStalled)
    {
        if ([self.delegate respondsToSelector:@selector(playerDetectTimeOut:)])
        {
            [self.delegate playerDetectTimeOut:self];
        }
    }

}

-(MPMovieAccessLogEvent *) lastAccessLogEvent
{
    MPMovieAccessLogEvent *event = [[[self.m_player accessLog] events] lastObject];
    return event;
}

-(void)updateLastPlayerState:(MPMoviePlaybackState) lastPlayerState
{
    lastState =lastPlayerState;
}
@end
