//
//  DXPlayerViewController.h
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 6/19/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "TVCplayer.h"
#if !TARGET_IPHONE_SIMULATOR
#ifdef PLAY_READY
#import <SecurePlayer/SecurePlayer.h>
#endif
#endif

#import "PlayReadyDrmProvider.h"


#ifndef VO_OSMP_PLAYER_ENGINE
#define VO_OSMP_PLAYER_ENGINE int
#endif
#if !TARGET_IPHONE_SIMULATOR


@interface DXPlayerViewController : TVCplayer <VOCommonPlayerDelegate>{
    VODXPlayer *m_pPlayer;
@public
    int fileSizeForPD; //file size for progressive download.
    
}

@end
#endif
