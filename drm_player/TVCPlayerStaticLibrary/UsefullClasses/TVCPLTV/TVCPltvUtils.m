//
//  TVCPltvUtils.m
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 3/13/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "TVCPltvUtils.h"
#import <TvinciSDK/TvinciSDK.h>
#import "TVMediaToPlayInfo.h"

@interface TVCPltvUtils (){
    TVNetworkQueue* _networkQueue;
}
@end

#define MINUTES_8_IN_SECONDS 8*60

@implementation TVCPltvUtils


+ (void)makeMediaReadyforPLTVFormat:(NSString *)format andMediaToPlay:(TVMediaToPlayInfo *)mediaToPlay  withProgram:(TVEPGProgram *)program withCompletionBlock:(void (^)(BOOL succeeded)) completion {
    
    TVFile * currentFile = [mediaToPlay.mediaItem fileForFormat:mediaToPlay.fileTypeFormatKey];
    NSDate * startTime   = program.startDateTime;
    
    EPGLicensedLinkFormatType formatType = EPGLicensedLinkFormatType_Catchup;
    if ([format isEqualToString:TVCMediaFormat_StartOver]||[format isEqualToString:TVCMediaFormat_PauseAndPlay]||[format isEqualToString:TVCMediaFormat_TrickPlay]) {
        formatType = EPGLicensedLinkFormatType_StartOver;
    }
    __block TVPAPIRequest * request = [TVPMediaAPI requestForGetEPGLicensedLinkForFileID:[currentFile.fileID integerValue] EPGItemID:[program.EPGId integerValue] startTime:startTime basicLink:[currentFile.fileURL absoluteString] userIP:@"" refferer:@"" countryCd2:@"" languageCode3:@"" deviceName:@"" formatType:formatType delegate:nil];
    
    [request setCompletionBlock: ^{
        NSString* url      = [request JSONResponse];
        [mediaToPlay addPLTVFileWithFormat:format andUrlString:url andBaseFile:currentFile];
        mediaToPlay.fileTypeFormatKey = format;

//        [self makePreparationsForPlaying:url for:program and:mediaToPlay withFormat:format];
        completion(YES);
    }];
    
    [request setFailedBlock:^{
        completion(NO);
    }];
    
    [request startAsynchronous];
    
}

//+ (void)makePreparationsForPlaying:(NSString *)url for:(TVEPGProgram *)program and:(TVMediaToPlayInfo *)mediaToPlay withFormat:(NSString *)format{
//    TVFile*   baseFile = [mediaToPlay.mediaItem fileForFormat:mediaToPlay.fileTypeFormatKey];
//    
//    NSString* fullUrl = nil;
//    //  Make a new copy of currentFile. And change the right URL and File type.
//    if ([format isEqualToString:TVCMediaFormat_StartOver]||[format isEqualToString:TVCMediaFormat_PauseAndPlay]) {
//        NSLog(@"Date before: %@", program.endDateTime);
//        NSDate *newDate = [program.endDateTime dateByAddingTimeInterval:MINUTES_8_IN_SECONDS];   //  Adding 8 minutes
//        NSLog(@"Date after: %@", newDate);
//        fullUrl = [self generateCachableURLWithEndTime:newDate toURLString:url];
//    }else{
//        fullUrl = url;
//    }
//
//    [mediaToPlay addPLTVFileWithFormat:format andUrlString:fullUrl andBaseFile:baseFile];
//    mediaToPlay.fileTypeFormatKey = format;
//}

+ (NSString *)generateCachableURLWithEndTime:(NSDate*)endTime toURLString:(NSString*)urlStr {
    
    long EndTimeInMilliSeconds = [endTime timeIntervalSince1970];
    NSString * endTimeString = [NSString stringWithFormat:@"%ld",EndTimeInMilliSeconds];
    NSString * urlSuffix =@")/index.m3u8";
    NSString * newEndOfTimeString = [NSString stringWithFormat:@",endTime=%@0000000%@",endTimeString,urlSuffix];
    
    return [urlStr stringByReplacingOccurrencesOfString:urlSuffix withString:newEndOfTimeString];
}


+ (long)getMediaStartTime:(TVMediaToPlayInfo *)mediaToPlay {
    if ([mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_StartOver] || [mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_PauseAndPlay] || [mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_TrickPlay]) {
        TVFile * file = [mediaToPlay currentFile];
        NSArray* arr = [[file.fileURL absoluteString] componentsSeparatedByString:@",startTime="];
        NSString* str = [arr lastObject];
        if (str) {
            NSArray* arr2 = [str componentsSeparatedByString:@"0000,endTime="];
            NSString* startTime = [arr2 firstObject];
            unsigned long long ullvalue = strtoll([startTime UTF8String], NULL, 0);
            return ullvalue;
        }
    }
    return -1;
}

+ (long)getMediaEndTime:(TVMediaToPlayInfo *)mediaToPlay {
    if ([mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_StartOver] || [mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_PauseAndPlay]|| [mediaToPlay.fileTypeFormatKey isEqualToString:TVCMediaFormat_TrickPlay]) {
        TVFile * file = [mediaToPlay currentFile];
        NSArray* arr = [[file.fileURL absoluteString] componentsSeparatedByString:@",endTime="];
        NSString* str = [arr lastObject];
        if (str) {
            NSArray* arr2 = [str componentsSeparatedByString:@"0000)"];
            NSString* endTime = [arr2 firstObject];
            unsigned long long ullvalue = strtoll([endTime UTF8String], NULL, 0);
            return ullvalue;
        }
    }
    return -1;
}

+ (TimeStruct)convertFromMilliSecToTimeStruct:(float)time {
    TimeStruct returnTime;
    int seconds =  time/1000;
    int minutes = (int)(seconds / 60);
    int hours = (int)(minutes / 60);
    minutes = minutes % 60;
    seconds = seconds % 60;
    returnTime.seconds = seconds;
    returnTime.minutes = minutes;
    returnTime.hours = hours;
    return returnTime;
}

//-(TVNetworkQueue *) networkQueue
//{
//    if (_networkQueue == nil)
//    {
//        _networkQueue = [[TVNetworkQueue alloc] init];
//    }
//    return _networkQueue;
//}
//
//-(void) sendRequest:(ASIHTTPRequest *)request
//{
//    if (request != nil)
//    {
//        [self.networkQueue sendRequest:request];
//    }
//}
//
//- (void)cleanQueue {
//    [self.networkQueue cleen];
//}


@end
