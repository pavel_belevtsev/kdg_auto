//
//  TVCPltvUtils.h
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 3/13/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCDeclerations.h"

@class TVMediaToPlayInfo;
@class TVEPGProgram;

@interface TVCPltvUtils : NSObject {
    
}

/**
 *	Make TVMediaToPlayInfo object to be ready for one of the following PLTV modes: 
 *  TVCMediaFormat_CatchUp, TVCMediaFormat_StartOver, TVCMediaFormat_PauseAndPlay
 */
+ (void)makeMediaReadyforPLTVFormat:(NSString *)format andMediaToPlay:(TVMediaToPlayInfo *)mediaToPlay  withProgram:(TVEPGProgram *)program withCompletionBlock:(void (^)(BOOL succeeded)) completion;


+ (long)getMediaStartTime:(TVMediaToPlayInfo *)mediaToPlay;
+ (long)getMediaEndTime:(TVMediaToPlayInfo *)mediaToPlay;
+ (TimeStruct)convertFromMilliSecToTimeStruct:(float)time;
@end
