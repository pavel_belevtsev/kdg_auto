//
//  TVTimer.m
//  TimerTest
//
//  Created by Tarek Issa on 11/7/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "TVTimer.h"

typedef enum
{
    Timer_Status_Stopped,
    Timer_Status_Running,
    Timer_Status_Paused
    
} Timer_Status;


@interface TVTimer () {
    Timer_Status status;
}

@property (nonatomic) dispatch_source_t _timer;

@end


@implementation TVTimer
@synthesize kMediaHitTimerInterval;

- (void)dealloc
{
    self.delegate = nil;
    [self stopTimer];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.kMediaHitTimerInterval = 1;
    }
    return self;
}

- (void)initializeTimer {
    self._timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(self._timer, DISPATCH_TIME_NOW, kMediaHitTimerInterval * NSEC_PER_SEC, 0.25 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self._timer, ^{
        [self hit];
    });
    
}

- (void)hit {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(timerTriggersWithHitNumber)]) {
            [self.delegate timerTriggersWithHitNumber];
        }
    }
}

- (void)startTimer {
    if (status != Timer_Status_Running) {
        if (!self._timer) {
            [self initializeTimer];
        }
        dispatch_resume(self._timer);
        status = Timer_Status_Running;
    }
}

- (void)pauseTimer {
    if (status == Timer_Status_Running) {
        if (!self._timer)
            return;
        dispatch_suspend(self._timer);
        status = Timer_Status_Paused;
    }
    
}

- (void)stopTimer {
    if (status != Timer_Status_Stopped) {
        if (!self._timer)
            return;

        dispatch_suspend(self._timer);
        dispatch_source_cancel(self._timer);
        self._timer = nil;
        status = Timer_Status_Stopped;
    }
}

@end
