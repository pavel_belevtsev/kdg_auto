//
//  TVTimer.h
//  TimerTest
//
//  Created by Tarek Issa on 11/7/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TVTimerProtocol <NSObject>

@required

- (void)timerTriggersWithHitNumber;

@end

@interface TVTimer : NSObject {
    
}

@property (nonatomic, assign) id<TVTimerProtocol> delegate;
@property (nonatomic, assign) NSTimeInterval kMediaHitTimerInterval;


- (void)stopTimer;
- (void)pauseTimer;
- (void)startTimer;

@end
