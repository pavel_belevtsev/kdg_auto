//
//  SubtitleObj.m
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "SubtitleObj.h"

@implementation SubtitleObj

@synthesize isBB;
@synthesize isLeft;
@synthesize isRight;
@synthesize textValue;
@synthesize distanceFromBottom;
@synthesize backgroundColor, textColor;
@synthesize textFont;

- (id)init
{
    self = [super init];
    if (self) {
        textValue = [[NSMutableString alloc] init];
    }
    return self;
}


@end
