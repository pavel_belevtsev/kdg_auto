//
//  TVCSubtitlesManager.m
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "TVCSubtitlesManager.h"
#import <TvinciSDK/ASIHTTPRequest.h>

@interface TVCSubtitlesManager () {

    
}
@property (nonatomic, retain) UIView* _superView;
@property (retain, nonatomic) NSMutableDictionary *subtitleItems;

@end

@implementation TVCSubtitlesManager
@synthesize currentSubsLanguage;
@synthesize mediaItem;

- (void)dealloc
{
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        /*   Default language   */
        self.subtitleItems = [[[NSMutableDictionary alloc] init] autorelease];
    }
    return self;
}

#pragma mark - Subtitles Operations

-(TVCSubsItem*)indexOfSubRipMultiItemWithStartTime:(CMTime)theTime
{
    NSInteger desiredTimeInSeconds = theTime.value / theTime.timescale;
    NSArray* lanArr = [self.subtitleItems objectForKey:[NSNumber numberWithInt:currentSubsLanguage]];
    TVCSubsItem *currentObj = [lanArr objectAtIndex:1];
    TVCSubsItem *returnValue = currentObj;
    
    if ((desiredTimeInSeconds >= [currentObj startTimeInSeconds]) && (desiredTimeInSeconds <= [currentObj endTimeInSeconds])) {
        return returnValue;
    }else{
        returnValue = nil;
        TVCSubsItem *nextObj = [lanArr objectAtIndex:0];
        if(desiredTimeInSeconds >= [(TVCSubsItem *)nextObj startTimeInSeconds]  &&  desiredTimeInSeconds <= [(TVCSubsItem *)nextObj endTimeInSeconds]) {
            returnValue =  [lanArr objectAtIndex:0];
        }
        return returnValue;
    }
}

-(void) loadSpesificSubtitleFileWithURL:(NSURL*) url
{
    ASLog2Debug(@"[Request][Media] - requestForCustomDownloadFileWithFileURL");////////
    
        
    __block  ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
    [request setRequestMethod:@"GET"];
    
    [request setStartedBlock:^{
    }];
    
    [request setFailedBlock:^{
        ASLog2Debug(@"Failed to get subtitles: %@", url);
    }];
    
    [request setCompletionBlock:^{

        NSData* data = [request responseData];

        NSString* str = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];

        [self populateFromJsonFor:str];

    }];
    
    [request startAsynchronous];
}

-(NSString *)getSubtitleFile
{
#warning subtitles file must come from a correct origin
    NSString *path  = [[NSBundle mainBundle]pathForResource:@"EP1HEB" ofType:@"json"];
    NSString *json = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return json;
}

-(void)populateFromJsonFor:(NSString*)json {
    
    //  Initialize subtitleItems

    
    //  Get the Json subs into arr
    __block  NSArray *arr = [json JSONValue];
    if (arr == nil) {
        ASLog2Debug(@"No subtitles");
        return;
    }
    NSMutableArray* mutArr = [[[NSMutableArray alloc] init] autorelease];
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

        TVCSubsItem *item = [[TVCSubsItem new] autorelease];
        NSDictionary *line =[arr objectAtIndex:idx];
        
        long miliStart = [[line objectForKey:@"startMillis"]longValue];
        long secStart = miliStart;//-3600000;//yes
        
        CMTime millisecondsCMTimeStart = CMTimeMake(secStart,1000);
        item.startTime = millisecondsCMTimeStart;
        
        long miliEnd = [[line objectForKey:@"endMillis"]longValue];
        long secEnd = miliEnd;//-3600000;//yes
        
        CMTime millisecondsCMTimeStartEnd = CMTimeMake(secEnd,1000);
        item.endTime = millisecondsCMTimeStartEnd;
        
        NSString*string = [line objectForKey:@"text"];
        item.nonParsedText = [string stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
        item.nonParsedText = [item.nonParsedText stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        
        item.index = idx;
        [mutArr addObject:item];
        if ([obj isEqual:[arr lastObject]]) {
            [self.subtitleItems setObject:mutArr forKey:[NSNumber numberWithInt:currentSubsLanguage]];
        }
    }];
}




-(TVCSubsItem*)indexOfSubRipMultiItemAfterSeekWithStartTime:(CMTime)theTime{
    __block  NSInteger desiredTimeInSeconds = theTime.value / theTime.timescale;
    
    NSArray* arr = [self.subtitleItems objectForKey:[NSNumber numberWithInt:currentSubsLanguage]];
    if (arr == nil) {
        return nil;
    }
    NSUInteger index = [arr indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ((desiredTimeInSeconds >= [(TVCSubsItem *)obj startTimeInSeconds]) && (desiredTimeInSeconds <= [(TVCSubsItem *)obj endTimeInSeconds]))
        {
            return YES;
        }else {
            return NO;
        }
    }];
    
    if (index!=NSIntegerMax)
    {
//        self.currentSubtitleIndex=index;
        return [arr objectAtIndex:index];
        
    }else {
        return nil;
    }
    
}

-(void) changeLangTo:(TVCSubsLan)language {
    currentSubsLanguage = language;

#warning Rivka Please change the subtitles to spesific language by calling a subtitle& audio manager 

    
    [self loadSpesificSubtitleFileWithURL:[NSURL URLWithString:[self.mediaItem.metaData objectForKey:@"Subtitle"]]];
}

@end
