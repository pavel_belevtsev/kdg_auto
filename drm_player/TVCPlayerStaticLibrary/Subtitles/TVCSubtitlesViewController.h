//
//  TVCSubtitlesViewController.h
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVCSubtitlesManager.h"




@class TVCSubtitlesManager;

@protocol TVCPlayerForSubs <NSObject>

- (float)getCurrentTime;
- (NSArray*)parseSubtitleString:(NSString*)stringToParse;
@end

@interface TVCSubtitlesViewController : UIViewController {

    
}


-(void) startShowingSubtitles;
-(void) pauseShowingSubtitles;
-(void) changeSubsLanguage:(TVCSubsLan)language;

@property (nonatomic, assign) BOOL isSubsStarted;
@property (nonatomic, retain) TVMediaItem* mediaItem;
@property (nonatomic, assign) id<TVCPlayerForSubs> delegate;
@end
