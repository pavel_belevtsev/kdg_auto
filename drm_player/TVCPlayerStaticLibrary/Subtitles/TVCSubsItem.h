//
//  TVCSubsItem.h
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CMTime.h>
//#import <AVFoundation/AVTime.h>
//#import <AVFoundation/AVFoundation.h>

@interface TVCSubsItem : NSObject {//< NSCoding >{
    CMTime startTime;
    CMTime endTime;
    NSString *uniqueID;
    NSInteger index;
}

@property(assign) CMTime startTime;
@property(assign) CMTime endTime;
@property(nonatomic, retain) NSString *nonParsedText;
@property(nonatomic, retain) NSArray *ParsedText;
@property(assign) NSInteger index;

@property(readonly, getter = startTimeString) NSString *startTimeString;
@property(readonly, getter = endTimeString) NSString *endTimeString;
@property(readonly) NSString *uniqueID;

-(NSString *)startTimeString;
-(NSString *)endTimeString;
-(NSString *)_convertCMTimeToString:(CMTime)theTime;
-(NSInteger)startTimeInSeconds;
-(NSInteger)endTimeInSeconds;

-(BOOL)containsString:(NSString *)str;

@end
