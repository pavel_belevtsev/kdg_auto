//
//  NSMutableArray+NSMutableArray_QueueAdditions.m
//  YES_iPad
//
//  Created by Tarek Issa on 7/25/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "NSMutableArray+NSMutableArray_QueueAdditions.h"
#import <UIKit/UIKit.h>
#define LABEL_TAG 88

@implementation NSMutableArray (NSMutableArray_QueueAdditions)


- (void)makeNew{
    UILabel* lbl = [[[UILabel alloc] init] autorelease];
    lbl.tag = LABEL_TAG;
    [self enqueue:lbl];
    
}
// Queues are first-in-first-out, so we remove objects from the head
- (id) dequeue {
    // if ([self count] == 0) return nil; // to avoid raising exception (Quinn)
    NSLog(@"dequeue - Labels Count: %d", [self count]);
    if ([self count] == 0) {
        [self makeNew];
        return [self dequeue];
        
    }else{
        id headObject = [self objectAtIndex:0];
        if (headObject != nil) {
            [[headObject retain] autorelease]; // so it isn't dealloc'ed on remove
            [self removeObjectAtIndex:0];
        }else{
            [self makeNew];
            return [self dequeue];
        }
        return headObject;
    }
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void) enqueue:(id)anObject {
    NSLog(@"enqueue - Labels Count: %d", [self count]);
    [self addObject:anObject];
    //this method automatically adds to the end of the array
}

@end
