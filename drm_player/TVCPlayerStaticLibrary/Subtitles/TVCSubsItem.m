//
//  TVCSubsItem.m
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "TVCSubsItem.h"

@implementation TVCSubsItem

@synthesize startTime, endTime, nonParsedText, uniqueID, index, ParsedText;
@dynamic startTimeString, endTimeString;

- (id)init {
    self = [super init];
    if (self) {
        uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    return self;
}

-(NSString *)startTimeString {
    return [self _convertCMTimeToString:self.startTime];
}

-(NSString *)endTimeString {
    return [self _convertCMTimeToString:self.endTime];
}

-(NSString *)_convertCMTimeToString:(CMTime)theTime {
    // Need a string of format "hh:mm:ss". (No milliseconds.)
    NSInteger seconds = theTime.value / theTime.timescale;
    NSDate *date1 = [NSDate new];
    NSDate *date2 = [NSDate dateWithTimeInterval:seconds sinceDate:date1];
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *converted = [[NSCalendar currentCalendar] components:unitFlags fromDate:date1 toDate:date2 options:0];
    
    NSMutableString *str = [NSMutableString stringWithCapacity:6];
    if ([converted hour] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%d:", [converted hour]];
    if ([converted minute] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%d:", [converted minute]];
    if ([converted second] < 10) {
        [str appendString:@"0"];
    }
    [str appendFormat:@"%d", [converted second]];
    
    [date1 release];
    return str;
}

-(NSInteger)startTimeInSeconds {
    if (self.startTime.value==0)
        return 0;
    
    return self.startTime.value / self.startTime.timescale;
}

-(NSInteger)endTimeInSeconds {
    if (self.endTime.value==0)
        return 0;
    
    return self.endTime.value / self.endTime.timescale;
}

-(BOOL)containsString:(NSString *)str {
    NSRange searchResult = [self.nonParsedText rangeOfString:str options:NSCaseInsensitiveSearch];
    if (searchResult.location == NSNotFound) {
        if ([str length] < 9) {
            searchResult = [[self startTimeString] rangeOfString:str options:NSCaseInsensitiveSearch];
            if (searchResult.location == NSNotFound) {
                searchResult = [[self endTimeString] rangeOfString:str options:NSCaseInsensitiveSearch];
                if (searchResult.location == NSNotFound) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    } else {
        return true;
    }
}

@end
