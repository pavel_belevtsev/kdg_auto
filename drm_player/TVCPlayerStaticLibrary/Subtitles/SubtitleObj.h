//
//  SubtitleObj.h
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface SubtitleObj : NSObject {

}

@property (assign) BOOL isBB;

/*  Label alignments   */
@property (assign) BOOL isRight;
@property (assign) BOOL isLeft;

/*  Distance of the subtitle from the most bottom   */
@property (assign) int distanceFromBottom;

/*  Text to be rendered   */
@property (nonatomic, retain) NSString* textValue;

/*  Text apperance: fill & background   */
@property (nonatomic, retain) UIColor* textColor;
@property (nonatomic, retain) UIColor* backgroundColor;

@property (nonatomic, retain) UIFont* textFont;

@end
