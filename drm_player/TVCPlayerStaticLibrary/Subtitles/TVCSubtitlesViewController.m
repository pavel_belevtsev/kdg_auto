//
//  TVCSubtitlesViewController.m
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import "TVCSubtitlesViewController.h"
#import "SubtitleObj.h"
#import "NSMutableArray+NSMutableArray_QueueAdditions.h"


#define LABEL_TAG 88
@interface TVCSubtitlesViewController () {
    dispatch_source_t _timer;
    TVCSubsItem* subsItem;
    NSMutableArray* queue;
}

@property (nonatomic, retain) TVCSubtitlesManager* subtitleManager;

@end

@implementation TVCSubtitlesViewController
@synthesize mediaItem = _mediaItem;
@synthesize delegate;
@synthesize isSubsStarted;

#pragma mark - View LifeCycle

- (void)dealloc
{
    self.subtitleManager = nil;
    _mediaItem = nil;
    [queue release];
    queue = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    queue = [[NSMutableArray alloc] init];
    self.view.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Subtitles setup and posiitoning

- (UILabel*)dequeLabelWithPropertiesOf:(SubtitleObj*)subsItm {
    UILabel* label = [queue dequeue];
    
    /*  Set text alignment RTL in case of hebrew subtitles  */
    if (self.subtitleManager.currentSubsLanguage == TVCSubsLan_HE) {
        [label setText:[@"\u200E" stringByAppendingString:subsItm.textValue]];
    }else{
        [label setText:subsItm.textValue];
    }
    
    
    [label setFont:subsItm.textFont];
    [label sizeToFit];
    
    /*   Set frame while considering all properties of SubtitleObj   */
    CGRect frame = label.frame;
    if (subsItm.isLeft) {
        frame.origin.x = 20;
        [label setTextAlignment:NSTextAlignmentLeft];
    }else if (subsItm.isRight) {
        frame.origin.x = self.view.frame.size.width - frame.size.width - 20;
        [label setTextAlignment:NSTextAlignmentRight];
    }else {
        frame.origin.x = self.view.frame.size.width/2 - frame.size.width/2;
        [label setTextAlignment:NSTextAlignmentCenter];
    }
    if (subsItm.isBB) {
        label.backgroundColor = subsItm.backgroundColor;
        label.textColor = subsItm.textColor;
    }else{
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
    }
    
   
    frame.origin.y = self.view.frame.size.height - (subsItm.distanceFromBottom) - 40;
    [label setFrame:frame];
    return label;
}

- (void)enqueueAllLabels{
    for (UILabel* lbl in self.view.subviews) {
        if (lbl.tag == LABEL_TAG) {
            [queue enqueue:lbl];
            [lbl removeFromSuperview];
        }
    }
}

//  Position the subtitles on self's view.
- (void)positionSubtitlesForItem:(TVCSubsItem*)subs{
    [self enqueueAllLabels];
    for (SubtitleObj* subObj in subs.ParsedText) {
        UILabel* label = [self dequeLabelWithPropertiesOf:subObj];
        [self.view addSubview:label];
    }
}

/*  Get the Json subtitle and parse it. */
- (void)setupSubtitle{
    NSTimeInterval time = 0;

    if (self.delegate)
    {
        if ([self.delegate respondsToSelector:@selector(getCurrentTime)])
        {
            time = [self.delegate getCurrentTime];
            //  Get in only if time is moving
            if (time!=0){
                
                CMTime timecm = CMTimeMake(time, 1000);
                
                //  Retreive subs by current player position
                TVCSubsItem* tempSub = [self.subtitleManager indexOfSubRipMultiItemAfterSeekWithStartTime:timecm];
                
                
                if (tempSub != nil) {
                    self.view.hidden = NO;
                    
                    //  Only if the current subtitles item changes, ask to parse the next sub item
                    if (![tempSub isEqual:subsItem]) {
                        subsItem = tempSub;
                        
                        //  If the subtitles did not parsed yet, ask to parse it
                        //  Otherwise, just show the subtitles on screen
                        if (subsItem.ParsedText == nil) {
                            subsItem.ParsedText = [NSArray arrayWithArray:[self.delegate parseSubtitleString:subsItem.nonParsedText]];
                        }
                        [self positionSubtitlesForItem:subsItem];
                    }
                }else
                {
                    //  If tempSub is Null, just hide the subs - No need to show any subtitles on screen.
                    [self enqueueAllLabels];
                    self.view.hidden = YES;
                }
            }
        }
    }
}

#pragma mark - Manager Operations

-(void) startShowingSubtitles {
    if (isSubsStarted) {
        return;
    }
    if (_mediaItem) {
        self.view.hidden = NO;
        [self startVideoTimer];
    }
}

-(void) pauseShowingSubtitles {
    if (!isSubsStarted) {
        return;
    }
    if (_mediaItem) {
        self.view.hidden = YES;
        [self pauseVideoTimer];
    }
}


-(void) changeSubsLanguage:(TVCSubsLan)language {
    [self.subtitleManager changeLangTo:language];
}

//- (float)getCurrentTime{
//    return [self.delegate getCurrentTime];
//}

#pragma mark - Timer


-(void) startVideoTimer
{
    [self initializeTimer];
}

-(void) stopVideoTimer
{
    if (_timer!=nil) {
        dispatch_source_cancel(_timer);
        _timer=nil;
    }
}


-(void)initializeTimer{
    if (_timer == nil) {
        _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
        dispatch_source_set_timer(_timer, DISPATCH_TIME_NOW, (1.0 / 5.0) * NSEC_PER_SEC, 0.25 * NSEC_PER_SEC);
        
        dispatch_source_set_event_handler(_timer, ^{
            [self setupSubtitle];
        });
    }
    if (!isSubsStarted) {
        [self resumeVideoTimer];
    }

}

-(void)pauseVideoTimer
{
    isSubsStarted = NO;
    dispatch_suspend(_timer);
}

-(void)resumeVideoTimer
{
    isSubsStarted = YES;
    dispatch_resume(_timer);
}

-(void)setMediaItem:(TVMediaItem *)mediaItem{
    [self enqueueAllLabels];
    _mediaItem = mediaItem;
    self.subtitleManager = nil;
    self.subtitleManager = [[TVCSubtitlesManager alloc] init];
    [self.subtitleManager setMediaItem:mediaItem];
    [self.subtitleManager changeLangTo:TVCSubsLan_HE];
}

@end
