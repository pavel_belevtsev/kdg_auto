//
//  NSMutableArray+NSMutableArray_QueueAdditions.h
//  YES_iPad
//
//  Created by Tarek Issa on 7/25/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (NSMutableArray_QueueAdditions) {
    
}

- (id) dequeue;
- (void) enqueue:(id)obj;

@end
