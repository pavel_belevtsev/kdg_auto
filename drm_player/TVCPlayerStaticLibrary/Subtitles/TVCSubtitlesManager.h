//
//  TVCSubtitlesManager.h
//  YES_iPad
//
//  Created by Tarek Issa on 7/24/13.
//  Copyright (c) 2013 Alexander Israel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCSubsItem.h"
#import "TVCDeclerations.h"

@interface TVCSubtitlesManager : NSObject {
    
    NSMutableDictionary* subtitlesDic;
}


-(TVCSubsItem*)indexOfSubRipMultiItemAfterSeekWithStartTime:(CMTime)theTime;
-(void) changeLangTo:(TVCSubsLan)language;

@property (nonatomic, retain) TVMediaItem* mediaItem;
@property (assign) TVCSubsLan currentSubsLanguage;
@end
