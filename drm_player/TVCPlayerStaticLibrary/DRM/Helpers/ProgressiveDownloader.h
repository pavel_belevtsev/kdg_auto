//
//  ProgressiveDownloader.h
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 2/5/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ProgDownloaderDelegate <NSObject>

- (void)ReadyForPlayback:(BOOL)success withFinalUrlString:(NSString*)finalUrlStr;

@end

@interface ProgressiveDownloader : NSObject {
    NSURLConnection *mTheConnection;
    NSFileHandle *mLocalFile;
    NSString* mLocalFilePath;
    NSURL* mediaUrl;
    bool mFileWasCreated;
    bool mStartedToPlay;
    int mTotalWriteDataSize;
    
    int mFileSizeOnPause;
    int mMegaDownloadedCounter;

}

- (id)initWithUrl:(NSURL*)url;
- (void)startDownloding;
- (void)kill;

@property (nonatomic, assign) id<ProgDownloaderDelegate> progDownDelegate;
@property (nonatomic, assign) int mFileZise;;

@end
