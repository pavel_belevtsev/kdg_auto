//
//  ProgressiveDownloader.m
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 2/5/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "ProgressiveDownloader.h"


@interface ProgressiveDownloader ()  <NSURLConnectionDelegate , NSURLConnectionDataDelegate>
{
    id<ProgDownloaderDelegate> progDownDelegate;
    BOOL shouldStop;
}

@end

@implementation ProgressiveDownloader
@synthesize progDownDelegate = _progDownDelegate;
@synthesize mFileZise;

- (void)dealloc
{
    NSLog(@"ProgressiveDownloader: dealloc");
    [super dealloc];
}

- (void)kill {
    shouldStop = YES;
    [mLocalFile closeFile];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:mLocalFilePath] == YES){
        if ([[NSFileManager defaultManager] removeItemAtPath:mLocalFilePath error: NULL]  == YES)
            NSLog (@"kill: Remove successful");
        else
            NSLog (@"kill: Remove failed");
    }

    
    [mTheConnection cancel];
    [mTheConnection unscheduleFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [mTheConnection release];
    mTheConnection = nil;
    NSLog(@"ProgressiveDownloader: Kill");
}

- (id)initWithUrl:(NSURL*)url
{
    self = [super init];
    if (self) {
        mediaUrl = url;
        [self setup];
    }
    return self;
}

- (void)startDownloding {

    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    if(mediaUrl != nil)
    {
        // Create the request.
        NSURLRequest *theRequest=[NSURLRequest requestWithURL:mediaUrl
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval:30];
        // create the connection with the request
        // and start loading the data
//        [mTheConnection release];
//        mTheConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        mTheConnection = [[NSURLConnection alloc]
                                        initWithRequest:theRequest
                                        delegate:self startImmediately:NO];
        

        if (mTheConnection) {
            [mTheConnection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                                      forMode:NSDefaultRunLoopMode];
            [mTheConnection start];
            // Create the NSMutableData to hold the received data.
            // receivedData is an instance variable declared elsewhere.
//            [mTheConnection start];
        } else {
            // Inform the user that the connection failed.
        }
    }
    [loopPool release];
    
}

- (void)setup {
    
    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    
    if(mediaUrl != nil) {

        //Progressive Download can't be streamed
        self.mFileZise = -1;
        mMegaDownloadedCounter = 1;
        mFileSizeOnPause = 0;

        NSString* fileName = [self getFileName];
//        mLocalFilePath = [self getFilePathForFileName:fileName];
        mLocalFilePath = [[self getFilePathForFileName:[self getFileNameOnFileSystem]] retain];

        //delete file if exists
        if ([[NSFileManager defaultManager] fileExistsAtPath:mLocalFilePath] == YES){
            if ([[NSFileManager defaultManager] removeItemAtPath:mLocalFilePath error: NULL]  == YES)
                NSLog (@"Remove successful");
            else
                NSLog (@"Remove failed");
        }
        
        mFileWasCreated = false;
    }
    [loopPool release];
    
}

- (NSString *)getFileName
{
    NSString* fileName = [NSString stringWithFormat:@"%d.%@",[[mediaUrl absoluteString] hash], [[mediaUrl absoluteString] pathExtension]];
    
    return fileName;
}

- (NSString*) getFilePathForFileName:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:filename];
}

-(NSString*)getFileNameOnFileSystem
{
    NSString* extension = (NSString*)[[[mediaUrl pathExtension] componentsSeparatedByString:@"?"] objectAtIndex:0];
    NSString* fileName = [NSString stringWithFormat:@"%0X.%@",[mediaUrl hash], extension];
    
    if (nil == extension || [extension length] == 0)
    {
        NSString* lastPathComponent = (NSString*)[[[mediaUrl lastPathComponent] componentsSeparatedByString:@"?"] objectAtIndex:0];
        fileName = [NSString stringWithFormat:@"%0X_%@",[mediaUrl hash], lastPathComponent];
    }
    else {
        fileName = [NSString stringWithFormat:@"%0X.%@",[mediaUrl hash], extension];
    }
    
    return fileName;
}

- (void)notifyDownload:(BOOL)success {
    if (self.progDownDelegate) {
        if ([self.progDownDelegate respondsToSelector:@selector(ReadyForPlayback:withFinalUrlString:)]) {
            [self.progDownDelegate ReadyForPlayback:success withFinalUrlString:mLocalFilePath];
        }
    }
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    if ([response isKindOfClass:[NSHTTPURLResponse self]]) {
		NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
		self.mFileZise = [[headers objectForKey:@"Content-Length"] longLongValue];
		NSLog(@"Incoming length: %i", self.mFileZise);
	}
    mTotalWriteDataSize = 0;
    mStartedToPlay = false;
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    if(!mFileWasCreated)
    {
        mFileWasCreated = true;
        
        bool secsses = [[NSFileManager defaultManager] createFileAtPath:mLocalFilePath contents:data attributes:nil];
        if(secsses)
        {
            mLocalFile = [NSFileHandle fileHandleForWritingAtPath:mLocalFilePath];
            [mLocalFile retain];
        }
        if (mLocalFile == nil)
            NSLog(@"Failed to open file");
        
    }else{
        [mLocalFile seekToEndOfFile];
        [mLocalFile writeData: data];
    }
    mTotalWriteDataSize += [data length];
    
    //start to play after 2Mb were downloaded
    if(!mStartedToPlay && mTotalWriteDataSize > 2 * 1024 * 1024 )
    {
        mStartedToPlay = true;
#warning Return to Player
        [self notifyDownload:YES];
    }
    if(mTotalWriteDataSize > mMegaDownloadedCounter * 1024 * 1024)
    {
        NSLog(@"Progressive Download: %d Mega were downloaded",mMegaDownloadedCounter);
        mMegaDownloadedCounter++;
    }
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    [connection release];
    [mLocalFile closeFile];
    
    [self notifyDownload:NO];
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [mLocalFile closeFile];
    NSLog(@"Progressive Download: downloaded %d/%d",mTotalWriteDataSize,self.mFileZise);
    // release the connection, and the data object
    [connection release];
    mTheConnection = nil;
}

@end
