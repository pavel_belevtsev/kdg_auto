//
//  DrmManager.h
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DrmProviderBase.h"
#import "TVCDeclerations.h"

@protocol DrmManagerProtocol <NSObject>
- (void)DrmFinishedWith:(TVCDrmStatus)status withURL:(NSURL*)url;
@end

@interface DrmManager : NSObject <DrmProviderProtocol> {
@private
    
}


@property (nonatomic, assign) id<DrmManagerProtocol> delegate;
@property (nonatomic, retain) DrmProviderBase* drmProvider;


+ (DrmManager *) sharedDRMManager;
- (void)StartProcess:(DrmProviderBase *)drmProvider ForMediaItem:(TVMediaToPlayInfo *)mediaItem andDelegate:(id<DrmManagerProtocol>)aDelegate;
- (void)kill;

@end
