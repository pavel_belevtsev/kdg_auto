//
//  DrmManager.m
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "DrmManager.h"
#import "TVCplayer.h"
#import "TvinciOperations.h"
#import "ClearDrmProvider.h"
#import "TVMediaToPlayInfo.h"


static DrmManager * sharedInstance = nil;

@interface DrmManager (){

    
}

@property (nonatomic, retain) TVMediaToPlayInfo* mediaItemInfoProcessing;
@property (nonatomic, retain) NSThread* processThread;
@property (atomic, assign) BOOL isProcessing;
@end

@implementation DrmManager
@synthesize delegate;

#if !TARGET_IPHONE_SIMULATOR

//- (void)dealloc
//{
//    NSLog(@"DRM manager dealloced");
//    self.mediaItemInfoProcessing = nil;
//    self.drmProvider = nil;
//    [self resetThread];
//    sharedInstance = nil;
//    [super dealloc];
//}

- (void)kill {
    NSLog(@"DRM manager killed");
    self.mediaItemInfoProcessing = nil;
//    [self.drmProvider kill];
    self.drmProvider = nil;
    self.delegate = nil;
    [self resetThread];
    sharedInstance = nil;
}

+ (DrmManager *) sharedDRMManager
{
    @synchronized(self)
    {
        if (sharedInstance == nil)
        {
            sharedInstance = [[DrmManager alloc] init];
        }
    }
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.mediaItemInfoProcessing = [[TVMediaToPlayInfo alloc] init];
    }
    return self;
}

- (BOOL)isTryingToPlaySameFile:(TVMediaToPlayInfo *)mediaItemToPlayInfo {
    TVFile * fileToPlay = [mediaItemToPlayInfo currentFile];
    TVFile * currentFile = [self.mediaItemInfoProcessing currentFile];
    if ([[currentFile.fileURL absoluteString] isEqualToString:[fileToPlay.fileURL absoluteString]]) {
        return YES;
    }
    return NO;
}

- (BOOL)shouldContinueFlow:(DrmProviderBase *)drmProvider ForMediaItem:(TVMediaToPlayInfo *)mediaItemToPlayInfo{

    
    //  Check if Provider is a Clear content provider.
//    if ([drmProvider isKindOfClass:[ClearDrmProvider class]])
//    {
//        TVFile * fileToPlay = [mediaItemToPlayInfo currentFile];
//        [self ProviderFinishedWithStatus:TVCDrmStatus_OK withURL:fileToPlay.fileURL];
//        return NO;
//    }
    
    //  If DRM manager is processing another thread, save media in order to play it afterward
    if (self.isProcessing) {
        if ([self isTryingToPlaySameFile:mediaItemToPlayInfo]) {
            //  If same file url is trying to play, No need to continue.
            self.mediaItemInfoProcessing = nil;
            return NO;
        }
#warning currentMediaItem
        self.mediaItemInfoProcessing = mediaItemToPlayInfo;
        [self resetThread];
        return NO;
    }
    return YES;
}


- (void)StartProcess:(DrmProviderBase *)drmProvider ForMediaItem:(TVMediaToPlayInfo *)mediaItemToPlayInfo andDelegate:(id<DrmManagerProtocol>)aDelegate {
    
    [self setDelegate:aDelegate];

    if (![self shouldContinueFlow:drmProvider ForMediaItem:mediaItemToPlayInfo]) {
        return;
    }
    


    /*  Fill drmPrivider with all it needs  */
    [self InitializeAndFireThreadWith:mediaItemToPlayInfo and:drmProvider];
}

- (void)InitializeAndFireThreadWith:(TVMediaToPlayInfo *)mediaItemToPlayInfo and:(DrmProviderBase *)drmProvider {
    self.isProcessing = YES;
    [drmProvider setMediaToPlayInfo:mediaItemToPlayInfo];
    self.drmProvider = nil;
    self.drmProvider = drmProvider;
    self.drmProvider.delegate = self;
    self.mediaItemInfoProcessing = mediaItemToPlayInfo;
    
    /*  Start the process in a new thread   */
    [self startThread];

}

- (void)resetThread {
    if ([self.processThread isExecuting]) {
        [self.processThread cancel];
        self.processThread = nil;
    }
}

- (void)startThread {
    /*  Clean network queue and cancel    */
#warning Ask For isCanceled before
    [self resetThread];
    [[TvinciOperations sharedInstance] cleanQueue];

    /*  Start a new thread  */
    self.processThread = [[NSThread alloc] initWithTarget:self.drmProvider selector:@selector(startNewDrmProcess) object:nil];
    [self.processThread setThreadPriority:1.0];
    [self.processThread start];
}

#pragma mark - DrmProviderProtocol
- (void)ProviderFinishedWithStatus:(TVCDrmStatus)status withURL:(NSURL *)url{

    /****************************************************************************************
    * Provider can return with 3 types of result:                                           *
    * 1. TVCDrmStatus_Interrupted :                                                         *
    *        Means, another DRM thread process is waiting to run, happen when the current   *
    *        thread was canceled.                                                           *
    * 2. TVCDrmStatus_OK :                                                                  *
    *        Means, DRM process was successfuly finish.                                     *
    * 2. TVCDrmStatusXXX_Failed :                                                           *
    *        Means, DRM process was failed and 'status' indecates the correct failure.      *
    ****************************************************************************************/

    //  If result caused because of interruption of another DRM process
    if (status == TVCDrmStatus_Interrupted) {
        if (self.mediaItemInfoProcessing == nil) {
            self.isProcessing = NO;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.delegate) {
                    if ([self.delegate respondsToSelector:@selector(DrmFinishedWith:withURL:)]) {
                        [self.delegate DrmFinishedWith:TVCDrmStatus_UNKNOWN withURL:url];
                    }
                }
            });

            return;
        }
        [self InitializeAndFireThreadWith:self.mediaItemInfoProcessing and:self.drmProvider];
        return;
    }

    self.isProcessing = NO;
    self.mediaItemInfoProcessing = nil;

    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(DrmFinishedWith:withURL:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate DrmFinishedWith:status withURL:url];
            });
        }
    }
}
#endif

@end
