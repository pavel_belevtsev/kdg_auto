//
//  PlayReadyProgDownProvider.h
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 2/5/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "DrmProviderBase.h"

@class ProgressiveDownloader;

@interface PlayReadyProgDownProvider : DrmProviderBase {
    
}

- (int)getMfileSize;

@property (nonatomic, readonly) ProgressiveDownloader* progDownloader;

@end
