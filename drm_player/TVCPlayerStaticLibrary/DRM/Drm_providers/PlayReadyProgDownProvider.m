//
//  PlayReadyProgDownProvider.m
//  TVCPlayerStaticLibrary
//
//  Created by Tarek Issa on 2/5/14.
//  Copyright (c) 2014 Tvinci. All rights reserved.
//

#import "PlayReadyProgDownProvider.h"
#import "ProgressiveDownloader.h"


@interface PlayReadyProgDownProvider () <ProgDownloaderDelegate> {

}

@property (nonatomic, retain) ProgressiveDownloader* progDownloader;

@end


@implementation PlayReadyProgDownProvider

- (int)getMfileSize {
    return self.progDownloader.mFileZise;
}

- (void)dealloc
{
    [self.progDownloader kill];
    [self.progDownloader release];
    self.progDownloader = nil;
    [super dealloc];
}

- (void)kill {
    [super kill];
//    [self.progDownloader kill];
//    [self.progDownloader release];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.drmType = TVDRMTypePlayready;
    }
    return self;
}

- (void)startNewDrmProcess {
    /*  Start a Playready Drm process (Thread method)  */
    [super startNewDrmProcess];
    
    //BOOL res = NO;
    self.progDownloader = [[ProgressiveDownloader alloc] initWithUrl:self.fileToPlay.fileURL];
    self.progDownloader.progDownDelegate = self;
    [self.progDownloader startDownloding];
    //  Call personalization and wait for result
//    res = [self startNewDrmProcessWith:@selector(personalization) andFailStatus:TVCDrmStatusPersonalization_Failed];
//    if (!res) {
//        return;
//    }
//    
//    //  Call DownloadContent and wait for result
//    res = [self startNewDrmProcessWith:@selector(DownloadContent) andFailStatus:TVCDrmStatusDownloadContent_Failed];
//    if (!res) {
//        return;
//    }
//    
//    //  Call isDrmContent and wait for result
//    res = [self startNewDrmProcessWith:@selector(isDrmContent) andFailStatus:TVCDrmStatusIsDRM_Failed];
//    if (!res) {
//        return;
//    }
//    
//    //  Call AcquireRights and wait for result
//    res = [self startNewDrmProcessWith:@selector(AcquireRights) andFailStatus:TVCDrmStatusAquireRights_Failed];
//    if (!res) {
//        return;
//    }
//    
//    [self finishProcess:TVCDrmStatus_OK];
}

- (void)ReadyForPlayback:(BOOL)success withFinalUrlString:(NSString *)finalUrlStr {
    NSLog(@"finishedDownloading");
    self.finalURLforPlayback = [NSURL URLWithString:finalUrlStr];
    TVCDrmStatus status = TVCDrmStatus_OK;
    if (!success) {
        status = TVCDrmStatusDownloadContent_Failed;
    }
    [self finishProcess:status];
}


/*      Override    */
//- (DRM_PROCESS_STATUS)DownloadContent {
//    
//}
//
//- (DRM_PROCESS_STATUS)isDrmContent {
//    //  Assume a DRM content (even if not, still native player can run a clear content).
//    return DRMStatusOK;
//}
//
//- (DRM_PROCESS_STATUS)AcquireRights {
//    ASLogDebug(@"AcquireRights Started");
//
//    return DRMStatusOK;
//}

@end
