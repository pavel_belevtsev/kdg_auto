//
//  WideVineDrmProvider.h
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/31/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "DrmProviderBase.h"

@interface WideVineDrmProvider : DrmProviderBase {
    
}


@property (nonatomic, retain) NSString* wv_localUrlString;



@end
