//
//  DrmProviderBase.h
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCDeclerations.h"
#import "TVMediaToPlayInfo.h"

typedef enum
{
    DRMStatusOK,
    DRMStatusBreak,
    DRMStatusFailed
    
} DRM_PROCESS_STATUS;



@protocol DrmProviderProtocol <NSObject>

/*  Required methods.   */
@required
- (void)ProviderFinishedWithStatus:(TVCDrmStatus)status withURL:(NSURL*)url;

@end


@interface DrmProviderBase : NSObject {// <DrmProviderProtocol> {

}

- (void)startNewDrmProcess;


- (void)getMediaLicenseDataWithCompletionBlock:(void (^)(NSString*)) completion;
- (void)getMediaLicenseLinkWithCompletionBlock:(void (^)(NSString*)) completion;
/*- (void)getMediaLicenseForWidevineDrmDataWithCompletionBlock:(void (^)(NSString*)) completion;*/
- (void)finishProcess:(TVCDrmStatus) status;
- (BOOL)startNewDrmProcessWith:(SEL)selector andFailStatus:(TVCDrmStatus) failStatus;
- (void)kill;

/*  Dx Personalization methods  */
- (BOOL)getPerspnalizationStatus;
- (void)savePersonalized:(BOOL)pers;


@property (nonatomic, assign) BOOL drmIsProcessing;
@property (nonatomic, assign) TVDRMType drmType;
@property (nonatomic, assign) id <DrmProviderProtocol> delegate;
@property (nonatomic, retain) TVMediaToPlayInfo * mediaToPlayInfo;
@property (nonatomic, retain) NSString*     customData;
@property (nonatomic, retain) NSURL*     finalURLforPlayback;
@property (nonatomic, retain) NSString*     licensedStr;
@property (nonatomic, retain) TVFile* fileToPlay;


@end
