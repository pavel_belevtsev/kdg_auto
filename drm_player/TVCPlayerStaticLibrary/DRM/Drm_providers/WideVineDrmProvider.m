//
//  WideVineDrmProvider.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/31/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "WideVineDrmProvider.h"
#import "WViPhoneAPI.h"
#import "TvinciOperations.h"
#import "TVMediaToPlayInfo.h"

#define licenseLinkIteration 3
#define LicenseServer_STR [[[TVConfigurationManager sharedTVConfigurationManager] LicenseServerUrl] absoluteString]

//NSString *const DRMServerURLStringBlink = @"http://stg-hk.tvincidnshk.com/drm/service.ashx?gid=159&drm=wv";  //  BLINK

NSString *const TVPortalID = @"d3";
BOOL hasRights;

@implementation WideVineDrmProvider
@synthesize wv_localUrlString;


- (id)init
{
    self = [super init];
    if (self) {
        self.drmType = TVDRMTypeWidevine;
    }
    return self;
}

- (void)dealloc
{
    self.wv_localUrlString = nil;
    [[TvinciOperations sharedInstance] cleanQueue];
    [super dealloc];
}

- (void)kill {
    [super kill];
    self.wv_localUrlString = nil;
}

- (void)startNewDrmProcess {
    /*  Start a Drm process (Thread method)  */
    [super startNewDrmProcess];
    self.finalURLforPlayback = nil;
    
    BOOL res = NO;
    
    //  Call personalization and wait for result
    res = [self startNewDrmProcessWith:@selector(personalization) andFailStatus:TVCDrmStatusPersonalization_Failed];
    if (!res) {
        return;
    }
    
    //  Call DownloadContent and wait for result
    res = [self startNewDrmProcessWith:@selector(DownloadContent) andFailStatus:TVCDrmStatusDownloadContent_Failed];
    if (!res) {
        return;
    }
    
    //  Call isDrmContent and wait for result
    res = [self startNewDrmProcessWith:@selector(isDrmContent) andFailStatus:TVCDrmStatusIsDRM_Failed];
    if (!res) {
        return;
    }
    
    //  Call AcquireRights and wait for result
    res = [self startNewDrmProcessWith:@selector(AcquireRights) andFailStatus:TVCDrmStatusAquireRights_Failed];
    if (!res) {
        return;
    }
    
    [self finishProcess:TVCDrmStatus_OK];
}

-(DRM_PROCESS_STATUS)personalization {
    
    NSString *siteGUID = [[TVSessionManager sharedTVSessionManager].sharedInitObject siteGUID];
    WViOsApiStatus status1;
    if (siteGUID) {
        status1 = WV_SetUserData(siteGUID);
    }else{
        status1 = WV_SetUserData(@"0");
    }
    //  If already have User Data (personalized), return OK.

    if(status1 == WViOsApiStatus_OK) {
        return DRMStatusOK;
    }
    
    if (status1 == WViOsApiStatus_NotInitialized) {
        //  If not initialize so initialize.
        NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                                  LicenseServer_STR , WVDRMServerKey,
                                  TVPortalID, WVPortalKey,
                                  siteGUID , WVCAUserDataKey,
                                  nil];

        WViOsApiStatus status = WV_Initialize(initCallback, settings);
        ASLog2Debug(@"status =  %i",status);
        
        if(status == WViOsApiStatus_OK || status == WViOsApiStatus_AlreadyInitialized ) {
            /*  Initialization/Personalization succeeded   */
            return DRMStatusOK;
        }
    }

    //  personalization process finish without success.
    return DRMStatusFailed;
}

- (DRM_PROCESS_STATUS)isDrmContent {
    //  Assume a DRM content (even if not, still native player can run a clear content).
    return DRMStatusOK;
}

- (DRM_PROCESS_STATUS)DownloadContent {
    //  For now no content to download.
    return DRMStatusOK;
}


- (DRM_PROCESS_STATUS)AcquireRights {
    
    //  Initialize customData with nil.
    self.wv_localUrlString = @"";
    
    for (int i = 0; i < licenseLinkIteration; i++) {
        //  try 'customDataIteration' times for custom data request
        
        [self askForLicensedLink];
        
        //  check if semaphonre got a valid custom data.
        if (self.wv_localUrlString != nil && ![self.wv_localUrlString isEqualToString:@""]) {
            break;
        }
    }

    if (!(self.wv_localUrlString && ![self.wv_localUrlString isEqualToString:@""]))
    {
        self.wv_localUrlString = [[self.mediaToPlayInfo currentFile].fileURL absoluteString];
    }
    return [self runInternalAcquisition];
}

- (DRM_PROCESS_STATUS)runInternalAcquisition {
    
    NSMutableString *responseString = [NSMutableString string]; // for response url on widevine localhost
    WViOsApiStatus status;// = WV_Stop();
    self.finalURLforPlayback = nil;
    status = WV_Play(self.wv_localUrlString, responseString, nil);
    if (status == WViOsApiStatus_AlreadyPlaying) {
        WV_Stop();
        status = WV_Play(self.wv_localUrlString, responseString, nil);
        
    }
    if (status == WViOsApiStatus_OK)
    {
        self.wv_localUrlString = responseString;
        self.finalURLforPlayback = [NSURL URLWithString:responseString];

        return DRMStatusOK;
    }
    //  In case of clear content. Just fill the final url and let the Native player play the playback.
    // self.finalURLforPlayback = [NSURL URLWithString:self.wv_localUrlString];
    return DRMStatusFailed;
}

- (void)askForLicensedLink {
    /*  'getMediaLicenseLinkWithCompletionBlock' is an Async operation, and the whole DRM process is a sync operaion
     So, the flow needs to wait for 'getMediaLicenseLinkWithCompletionBlock' to be done.
     Semaphore concept encapsulate an async operation and turn it to a sync one.     */
    if (self.mediaToPlayInfo.useSignedUrl)
    {
#warning convert to a sync request and remove semaphore
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getMediaLicenseLinkWithCompletionBlock:^(NSString* mediaLicencedLink){
            if (mediaLicencedLink != nil && ![mediaLicencedLink isEqualToString:@""]) {
                [self setWv_localUrlString:mediaLicencedLink];
            }
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        dispatch_release(sema);
    }
    else
    {
        [self setWv_localUrlString:[self.mediaToPlayInfo.currentFile.fileURL  absoluteString]];
    }
    

    
}


WViOsApiStatus initCallback(WViOsApiEvent event, NSDictionary *attributes)
{
    WViOsApiStatus status = WViOsApiStatus_OK;
    switch (event)
    {
        case WViOsApiEvent_Initialized:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_InitializeFailed:
            status = WViOsApiStatus_NotInitialized;
            break;
        case WViOsApiEvent_Playing:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_Stopped:
            status = WViOsApiStatus_NotPlaying;
            break;
        case WViOsApiEvent_Bitrates:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_EMMFailed:
            status = WViOsApiStatus_NotEntitled;
            break;
        case WViOsApiEvent_EndOfList:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_NullEvent:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_EMMRemoved:
            status = WViOsApiStatus_EntitlementExpired;
            break;
        case WViOsApiEvent_PlayFailed:
            status = WViOsApiStatus_NotPlaying;
            break;
        case WViOsApiEvent_Registered:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_Terminated:
            status = WViOsApiStatus_NotInitialized;
            break;
        case WViOsApiEvent_AudioParams:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_EMMReceived:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_QueryStatus:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_VideoParams:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_ChapterImage:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_ChapterSetup:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_ChapterTitle:
            status = WViOsApiStatus_OK;
            break;
        case WViOsApiEvent_Unregistered:
            status = WViOsApiStatus_NotRegistered;
            break;
        case WViOsApiEvent_StoppingOnError:
            status = WViOsApiStatus_UnknownError;
            break;
        case WViOsApiEvent_SetCurrentBitrate:
            status = WViOsApiStatus_OK;
            break;
        default:
            break;
    }
    return status;
}

@end
