//
//  ClearDrmProvider.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/13/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "ClearDrmProvider.h"

@implementation ClearDrmProvider


//#pragma mark - DrmProviderProtocol
#pragma mark - Override

- (void)startNewDrmProcess {
    /*  Start a Playready Drm process (Thread method)  */
    [super startNewDrmProcess];
    self.finalURLforPlayback = self.fileToPlay.fileURL;
    
    [self finishProcess:TVCDrmStatus_OK];
}

//-(void)personalization{
//    [super personalization];
//}
//
//- (void)isDrmContent {
//    [super isDrmContent];
//}
//
//- (void)DownloadContent {
//    [super DownloadContent];
//}
//
//-(void)AcquireRights{
//    [super AcquireRights];
//}


@end
