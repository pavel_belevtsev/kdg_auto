//
//  PlayRead_Drm.m
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//



#import "PlayReadyDrmProvider.h"
NSString *const DRMServerURLStringEutelsat = @"http://stg-hk.tvincidnshk.com/licenceserver/rightsmanager.asmx";   //  EUTELSAT

@interface PlayReadyDrmProvider (){
    
    
}

@end


@implementation PlayReadyDrmProvider


#pragma mark - Override
#define FILE_EXISTS 0
#define FILE_DOWNLOADED 1
#define FILE_DOWNLOADED_ERROR 2

#if !TARGET_IPHONE_SIMULATOR

#warning Should take this from DNS
#define rightsResetDeviation    24*60*60 //   In Seconds

//#define customDataIteration 3



- (id)init
{
    self = [super init];
    if (self) {
        self.drmType = TVDRMTypePlayready;
       // [self turnOnDxDebug];
    }
    return self;
}

- (void)kill {
    [super kill];
}

- (void)startNewDrmProcess {
    /*  Start a Playready Drm process (Thread method)  */
    [super startNewDrmProcess];
    self.finalURLforPlayback = self.fileToPlay.fileURL;

    BOOL res = NO;
    
    
    //  Call DownloadContent and wait for result
    res = [self startNewDrmProcessWith:@selector(DownloadContent) andFailStatus:TVCDrmStatusDownloadContent_Failed];
    if (!res) {
        return;
    }

    //  Call isDrmContent and wait for result
    res = [self startNewDrmProcessWith:@selector(isDrmContent) andFailStatus:TVCDrmStatusIsDRM_Failed];
    if (!res) {
        return;
    }
    
    
    //  Only if isDrmContent returns YES (means encrypted content), so make a personalization,
    //  otherwize its a clear content and no need for personalization.
    
    //  Call personalization and wait for result
    res = [self startNewDrmProcessWith:@selector(personalization) andFailStatus:TVCDrmStatusPersonalization_Failed];
    if (!res) {
        return;
    }

    //  Call AcquireRights and wait for result
    res = [self startNewDrmProcessWith:@selector(AcquireRights) andFailStatus:TVCDrmStatusAquireRights_Failed];
    if (!res) {
        return;
    }

    [self finishProcess:TVCDrmStatus_OK];
}


#pragma mark - DRM methods

-(DRM_PROCESS_STATUS)personalization{
    if ([self getPerspnalizationStatus]) {
        //  Device is already personalized.
        NSLog(@"getPerspnalization from memory");
        return DRMStatusOK;
    }
    
    NSLog(@"Personalization server URL: %@", PERSONALIZATION_SERVER_URL);
    
    DxDrmManagerStatus retVal = DX_MANAGER_SUCCESS;
    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    
    if ([DxDrmManager sharedManager] == nil) {
        NSLog(@"Error: DxDrmManager is Null");
        [loopPool release];
        return DRMStatusFailed;
    }

    // Check whether device is already Personalized. If so, performing Personalization is not required.
    retVal = [[DxDrmManager sharedManager] personalizationVerify];
    if (DX_MANAGER_SUCCESS != retVal)
    {
        //  No personalization have been done yet, so ask for personalization
        retVal = [[DxDrmManager sharedManager]
                  performPersonalizationWithSessionID:PERSONALIZATION_SESSION_ID
                  withServerURL:PERSONALIZATION_SERVER_URL
                  withAppVersion:PERSONALIZATION_APP_VERSION];
    }
    
    if (DX_MANAGER_SUCCESS == retVal) {
        //  Personalization was successfuly done.
        [loopPool release];
        [self savePersonalized:YES];
        NSLog(@"getPerspnalization from server");

        return DRMStatusOK;

    }

    //  Personalization failed for some reason.
    NSLog(@"Personalization failed");
    [loopPool release];
    [self savePersonalized:NO];
    return DRMStatusFailed;
}

- (int)downloadPlaylistFileIsHarmonic:(BOOL)isHarmonicHLS {
    NSString* fileName = [self getFileName];
//    TVFile * self.fileToPlay = [self.mediaToPlayInfo.mediaItem fileForFormat:self.mediaToPlayInfo.fileTypeFormatKey];
    

    if (isHarmonicHLS) {
        ASLog2Debug(@"isHarmonicHLS");
        NSString* tsUrl = [self getTsUrl:[self.fileToPlay.fileURL absoluteString]];
        [self downloadFile:fileName From:tsUrl Overwrite:YES];
    }else{
        [self downloadFile:fileName From:[self.fileToPlay.fileURL absoluteString] Overwrite:YES];
    }
#warning Handle caching option
    //  Download the .m3u8 file.
    
    //  Check if file exists after download (means file was succesfuly written on disk).
    if([self checkIfFileExist:fileName])
    {
        return FILE_DOWNLOADED;
    }
    
    return FILE_DOWNLOADED_ERROR;
}

- (DRM_PROCESS_STATUS)DownloadRegularContentIsHarmonicHLS:(BOOL)isHarmonicHLS {
    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    int retVal = FILE_EXISTS;
    retVal = [self downloadPlaylistFileIsHarmonic:isHarmonicHLS];
    
    DRM_PROCESS_STATUS returnStatus = DRMStatusFailed;
    //  Log an appropreate message
    switch (retVal) {
        case FILE_DOWNLOADED:
            ASLog2Debug(@"FILE_DOWNLOADED");
            returnStatus = DRMStatusOK;
            break;
        case FILE_DOWNLOADED_ERROR:
            ASLog2Debug(@"FILE_DOWNLOADED_ERROR");
            returnStatus = DRMStatusFailed;
            break;
        case FILE_EXISTS:
            returnStatus = DRMStatusOK;
            ASLog2Debug(@"FILE_EXISTS");
            break;
            
        default:
            ASLog2Debug(@"NA");
            break;
    }
    [loopPool release];
    return returnStatus;
}

- (DRM_PROCESS_STATUS)DownloadContent {
    //  Determine if the download method is a custom Harmonics or a Regular download.
    return [self DownloadRegularContentIsHarmonicHLS:self.mediaToPlayInfo.isHarmonicsHLS];
}

- (BOOL)isClearContent {

    if ([[[self.fileToPlay.fileURL absoluteString] lowercaseString] rangeOfString:@"m3u8"].location != NSNotFound) {
        return YES;
    }
    return NO;
}


- (DRM_PROCESS_STATUS)isDrmContent {
    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    
    if ([DxDrmManager sharedManager] == nil) {
        ASLog2Debug(@"Error: DxDrmManager is Null");
        [loopPool release];
        return DRMStatusFailed;
    }
    
    DxDrmManagerStatus retVal = DX_MANAGER_SUCCESS;
    NSString* fileName = [self getFileName];

    //  check if file exists.
    if(![self checkIfFileExist:fileName]){
        return DRMStatusFailed;
    }

    //  check if file isDrmContent.
    NSString *localPath = [self getFilePathForFileName:fileName];
    BOOL isSuccess = [[DxDrmManager sharedManager] isDrmContent:localPath result:&retVal];
    if ((DX_MANAGER_SUCCESS != retVal) || (!isSuccess))
    {
        //  file is not a Drm Content.
        ASLog2Debug(@"Error: Not DRM content");
        [loopPool release];
        
        //  If content is not a DRM content, so check if the content is a clear content and a m3u8 format too.
        if ([self isClearContent]) {
            self.finalURLforPlayback = self.fileToPlay.fileURL;
            return DRMStatusBreak;
        }
        
        return DRMStatusFailed;
    }
    [loopPool release];
    return DRMStatusOK;
}

- (DRM_PROCESS_STATUS)AcquireRights {
    ASLogDebug(@"AcquireRights Started");
    if ([self checkIfRightExist]) {
        return DRMStatusOK;
    }

    //  Initialize customData with nil.
    [self setCustomData:self.mediaToPlayInfo.customData];

    if (self.customData) {
        DRM_PROCESS_STATUS status = [self runInternalAcquisition];
        
        if (status != DRMStatusFailed) {
            self.finalURLforPlayback = self.fileToPlay.fileURL;
        }
        return status;
    }
    return DRMStatusFailed;
}

- (void)askForCustomData{
    /*  'getMediaLicenseDataWithCompletionBlock' is an Async operation, and the whole DRM process is a sync operaion
        So, the flow needs to wait for 'getMediaLicenseDataWithCompletionBlock' to be done.
        Semaphore concept encapsulate an async operation and turn it to a sync one.     */
#warning convert to a sync request and remove semaphore
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getMediaLicenseDataWithCompletionBlock:^(NSString* mediaLicenceData){
            if (mediaLicenceData != nil && ![mediaLicenceData isEqualToString:@""]) {
                [self setCustomData:mediaLicenceData];
            }
            dispatch_semaphore_signal(sema);
        }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    dispatch_release(sema);
    
}

- (BOOL)checkIfRightExist {
    
    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    DxDrmManagerStatus retVal = DX_MANAGER_SUCCESS;
    NSString* fileName = [self getFileName];
    
    if ([self checkIfFileExist:fileName]) {
        retVal = [[DxDrmManager sharedManager] verifyRightsForFile:[self getFilePathForFileName:fileName]];
        if (retVal == DX_MANAGER_SUCCESS) {
            ASLogDebug(@"Rights already exists");
            [loopPool release];
            return YES;
        }
    }
    [loopPool release];
    return NO;
}

- (DRM_PROCESS_STATUS)runInternalAcquisition {
    ASLogDebug(@"runAndAcquireRights started.");

    DxDrmManagerStatus retVal = DX_MANAGER_SUCCESS;
    NSError *drmOperationError = nil;
    
    //  if media is nil, return
    if (self.fileToPlay == nil) {
//    if(self.mediaToPlayInfo.mediaItem == nil) {
        NSLog(@"Acquire permission failed.-> self.fileToPlay = nil");
        return DRMStatusFailed;
    }

    NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
    
    //  check if the file is on the file system
    NSString* fileName = [self getFileName];
    retVal = DX_MANAGER_ERROR_FILE_ACCESS_ERROR;
    
    //  If file exists it is safe to call 'acquireRightsForFile'.
    if([self checkIfFileExist:fileName])
    {
        [[DxDrmManager sharedManager] setCookies:nil];
        NSString* filename = [self getFilePathForFileName:fileName];
        
        //  Ask for rights to play.
        retVal = [[DxDrmManager sharedManager] acquireRightsForFile:filename withCustomData:self.customData withRightsUrl:LICENSE_SERVER_URL error:&drmOperationError];

        if(retVal == DX_MANAGER_SUCCESS){
            
            //  Verify rights For Insurance!
            retVal = [[DxDrmManager sharedManager] verifyRightsForFile:[self getFilePathForFileName:fileName]];
            if (retVal == DX_MANAGER_SUCCESS) {
                [loopPool release];
                NSLog(@"Acquire permission succeeded.");
                return DRMStatusOK;
            }
            NSLog(@"Could not verify rights after acquisition.");
        }
        NSLog(@"Could not acquire for rights.");
    }
    [loopPool release];
    NSLog(@"Acquire permission failed.");
    return DRMStatusFailed;
}


#pragma mark - Assistant methods

-(void)turnOnDxDebug
{
    //  Turn On DXDebug
    [DxDrmManager DEBUG_setLoggingLevel:@"60" withLogFile:nil useStdOutLogs:YES withDisabledModules:@"0x00000000"];
}

- (bool)checkIfFileExist:(NSString *)filename
{
    
    //--- Obtain file name -----------------------------------------------------
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *localFileName = [documentsDirectory stringByAppendingPathComponent:filename];
    
    //--- Check if file already exists ------------------------------------------
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    return [fileManager fileExistsAtPath:localFileName];
}

- (NSString*) getFilePathForFileName:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:filename];
}


- (int)downloadFile:(NSString *)filename From:(NSString *)address Overwrite:(BOOL)overwrite
{
    int ret = FILE_DOWNLOADED;
    BOOL bExists;
    
    NSString *localFileName = [self getFilePathForFileName:filename];
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    if (overwrite)
        bExists = NO;
    else
        bExists = [fileManager fileExistsAtPath:localFileName];

    if (bExists){
        ret = FILE_EXISTS;
        return ret;
    }

    //  --- Download File ---
    NSURL *url = [NSURL URLWithString:address];
    if(url == nil) {
        ASLogDebug(@"******* URL for m3u8 file is not well formed ********");
        ret = FILE_DOWNLOADED_ERROR;
        return ret;
    }

    NSData * data = [NSData dataWithContentsOfURL:url];

    if(data == nil) {
        ret = FILE_DOWNLOADED_ERROR;
        ASLogDebug(@"******* Data was not downloded , reponse is empty ********");
        return ret;
    }

    BOOL res = [data writeToFile:localFileName atomically:NO];

    if (res) {
        ASLogDebug(@"writeToFile OK: %@", filename);
    }else{
        ASLogDebug(@"******* writeToFile failed the file name or content can't be wirtten ********");
    }

    return ret;
}

/*
- (BOOL)deleteFileIfExpired:(TVMediaToPlayInfo*) mediaToPlayInfo
{
    NSString* fileName = [self getFileName];
    
    if ([self checkIfFileExist:fileName]) {
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSString* fileFullPath = [self getFilePathForFileName:fileName];
        NSDictionary* fileAtt = [fileManager attributesOfItemAtPath:fileFullPath error:NULL];
        
        NSDate* creationDate = [fileAtt objectForKey:NSFileCreationDate];
        NSDate* now = [NSDate date];
        float resetDivation = rightsResetDeviation;
        float secondsBetweenCreationAndNow = [now timeIntervalSinceReferenceDate] - [creationDate timeIntervalSinceReferenceDate];
        
        ASLog2Debug(@"secondsBetweenCreationAndNow: %f", secondsBetweenCreationAndNow);
        if (secondsBetweenCreationAndNow >= resetDivation) {
            NSError* error = NULL;
            [fileManager removeItemAtPath:fileFullPath error:&error];
            if (error) {
                ASLog2Debug(@"Unable to delete file: %@\nError: %@", fileName, error);
                return NO;
            }else return YES;
        }
        return NO;
    }
    return YES;
} */

- (void)deleteRightsForFile {
    
    NSString* fileName = [self getFileName];
    DxDrmManagerStatus retVal = [[DxDrmManager sharedManager] deleteRightsForFile:[self getFilePathForFileName:fileName]];
    if (retVal == DX_MANAGER_SUCCESS) {
        ASLogDebug(@"deleteRightsForFile - Succeeded %@", fileName);
    }else{
        ASLogDebug(@"deleteRightsForFile - Failed %@", fileName);
    }
}

- (NSString *)getFileName
{
//    TVFile * self.fileToPlay = [self.mediaToPlayInfo.mediaItem fileForFormat:self.mediaToPlayInfo.fileTypeFormatKey];
    NSString* fileName = [NSString stringWithFormat:@"%lu.%@",(unsigned long)[[self.fileToPlay.fileURL absoluteString] hash], [[self.fileToPlay.fileURL absoluteString] pathExtension]];
    
    return fileName;
}

-(NSString*)getTsUrl:(NSString*)playlistUrl
{
    NSString* baseUrl  = [playlistUrl stringByDeletingLastPathComponent];
    NSString* playlist = [NSString stringWithContentsOfURL:[NSURL URLWithString:playlistUrl] encoding:NSASCIIStringEncoding error:nil];
    
    if (nil == playlist){
        return nil;
    }
    
    for (NSString* line in [playlist componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]) {
        NSString* trimedLine = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([trimedLine hasPrefix:@"#"]) {
            continue;
        } else if ([[[trimedLine pathExtension] lowercaseString] rangeOfString:@"m3u8"].location != NSNotFound){
            return [self getTsUrl:[baseUrl stringByAppendingPathComponent:trimedLine]];
        } else if ([[[trimedLine pathExtension] lowercaseString] rangeOfString:@"ts"].location != NSNotFound){
            return [baseUrl stringByAppendingPathComponent:trimedLine];
        }
    }
    
    return nil;
}


#endif


@end
