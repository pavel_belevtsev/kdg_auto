//
//  PlayRead_Drm.h
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#if !TARGET_IPHONE_SIMULATOR
#import <SecurePlayer/SecurePlayer.h>
#endif
#import "DrmProviderBase.h"



#define CUSTOM_DATA                 @"http://192.116.217.179/SPDemoServer/PlayReadyQA/BTS/8350/YES.customData.txt"
#define PERSONALIZATION_SERVER_URL  [[[TVConfigurationManager sharedTVConfigurationManager] DxPersonlizationUrl] absoluteString]
#define LICENSE_SERVER_URL [[[TVConfigurationManager sharedTVConfigurationManager] LicenseServerUrl] absoluteString]

#define PERSONALIZATION_SESSION_ID @"SessionID"
#define PERSONALIZATION_APP_VERSION @"Sample"
//#define PERSONALIZATION_SESSION_ID @""
//#define PERSONALIZATION_APP_VERSION @""


@interface PlayReadyDrmProvider : DrmProviderBase  {
}

- (void)turnOnDxDebug;



@end
