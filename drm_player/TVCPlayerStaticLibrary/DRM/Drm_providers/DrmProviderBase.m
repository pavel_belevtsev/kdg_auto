//
//  DrmProviderBase.m
//  drmTest
//
//  Created by Tarek Issa on 7/12/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "DrmProviderBase.h"
#import "TvinciOperations.h"
#import "TVMediaToPlayInfo.h"


@interface DrmProviderBase (){
}

@end

@implementation DrmProviderBase
@synthesize drmIsProcessing;
@synthesize delegate;
@synthesize fileToPlay;
@synthesize customData;
@synthesize finalURLforPlayback;
@synthesize licensedStr;

#warning share customData into abstract layer

- (void)dealloc
{
    [self kill];
    [super dealloc];
}

- (void)kill {
    self.delegate = nil;
    customData = nil;
    self.mediaToPlayInfo = nil;
    self.customData = nil;
    self.finalURLforPlayback = nil;
    self.licensedStr = nil;
    self.fileToPlay = nil;
}

- (void)getMediaLicenseDataWithCompletionBlock:(void (^)(NSString*)) completion {
    [[TvinciOperations sharedInstance] getMediaLicenseDataForMediaID:self.mediaToPlayInfo WithCompletionBlock:^(NSString* mediaLicenceData){
        completion(mediaLicenceData);
    }];
}

- (void)getMediaLicenseLinkWithCompletionBlock:(void (^)(NSString*)) completion {
    [[TvinciOperations sharedInstance] getMediaLicenseLinkWithForMedia:self.fileToPlay WithCompletionBlock:^(NSString* mediaLicencedLink){
        completion(mediaLicencedLink);
    }];
}

/*
- (void)getMediaLicenseForWidevineDrmDataWithCompletionBlock:(void (^)(NSString*)) completion {
#warning Should fill data from mediaItem object
    [[TvinciOperations sharedInstance] getMediaLicenseLinkWithFileId:347751 andFileURL:[NSURL URLWithString:@"http://otttvincihdios-vh.akamaihd.net/i/clear/20130625_SHOOT_IT_EP1_IPAD_MP4_,20,40,.mp4.csmil/master.m3u8"] WithCompletionBlock:^(BOOL isSuccess, NSString* resultStr){
        if (isSuccess && ![resultStr isEqualToString:@""] && resultStr != nil) {
            completion(resultStr);
        }else{
            completion(nil);
        }
    }];
}
*/

- (void)startNewDrmProcess {
//  super do nothing for now.
    self.drmIsProcessing = YES;
    self.fileToPlay = [self.mediaToPlayInfo currentFile];
    if (self.mediaToPlayInfo.useSignedUrl) {
        [self askForLicensedLink];
    }
}

- (void)askForLicensedLink{
    /*  'getMediaLicenseDataWithCompletionBlock' is an Async operation, and the whole DRM process is a sync operaion
     So, the flow needs to wait for 'getMediaLicenseDataWithCompletionBlock' to be done.
     Semaphore concept encapsulate an async operation and turn it to a sync one.     */
#warning convert to a sync request and remove semaphore
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [self getMediaLicenseLinkWithCompletionBlock:^(NSString* mediaLicencedLink){
        if (mediaLicencedLink != nil && ![mediaLicencedLink isEqualToString:@""]) {
            self.fileToPlay.fileURL = [NSURL URLWithString:mediaLicencedLink];
        }
        dispatch_semaphore_signal(sema);
    }];
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    dispatch_release(sema);
    
}

- (BOOL)startNewDrmProcessWith:(SEL)selector andFailStatus:(TVCDrmStatus) failStatus {

    DRM_PROCESS_STATUS res = DRMStatusOK;

    if (![self respondsToSelector:selector])
        return NO;
    
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:self];
    [invocation invoke];
    int returnValue;
    [invocation getReturnValue:&returnValue];

    res = returnValue;

    if (res == DRMStatusBreak) {
        //  This is for Clear content handling, no need to continue the drm process.
        [self finishProcess:TVCDrmStatus_OK];
        return NO;
    }
    
    
    if (res != DRMStatusOK) {
        //  Handle specific issue, when isDRMContent is a clear media
        [self finishProcess:failStatus];
        return NO;
    }
    
    //  If provided was interrupted while processing
    if ([[NSThread currentThread] isCancelled]) {
        [self finishProcess:TVCDrmStatus_Interrupted];
        return NO;
    }
    return YES;
    
}

- (void)finishProcess:(TVCDrmStatus) status {
    self.drmIsProcessing = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (self.delegate) {
                if ([self.delegate respondsToSelector:@selector(ProviderFinishedWithStatus:withURL:)]) {
                    [self.delegate ProviderFinishedWithStatus:status withURL:self.finalURLforPlayback];
                }
            }
        });
    });
}


#pragma mark - Dx Personalization methods
NSString *const PersonalizationKey = @"PersonalizationKey";

- (void)savePersonalized:(BOOL)pers {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults  setObject:[NSNumber numberWithBool:pers] forKey:PersonalizationKey];
    [standardUserDefaults synchronize];
}

- (BOOL)getPerspnalizationStatus {
    NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:PersonalizationKey];
    if (!num)
        return NO;
    return [num boolValue];
}



@end
