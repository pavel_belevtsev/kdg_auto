//
//  TvinciOperations.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/13/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "TvinciOperations.h"
#import "TVMediaToPlayInfo.h"


@interface TvinciOperations (){
    TVNetworkQueue* _networkQueue;
}
@end



@implementation TvinciOperations
static TvinciOperations *_sharedTvinciOperations = nil;



+(TvinciOperations *) sharedInstance
{
    if (_sharedTvinciOperations == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _sharedTvinciOperations = [[TvinciOperations alloc] init];
        });
    }
    return _sharedTvinciOperations;
}


#pragma mark - Network
-(void) getMediaMarkForMediaItem:(TVMediaItem *)mediaItem WithCompletionBlock : (void (^)(NSNumber* mark)) completion
{
    ASLog2Debug(@"getMediaMarkForMediaItem");
    __block TVPAPIRequest *request = [TVPMediaAPI requestForGetMediaMarkForMediaID :[mediaItem.mediaID integerValue] delegate:nil];
    [request setStartedBlock:nil];
    
    [request setFailedBlock:^{
        completion(nil);
    }];
    
    [request setCompletionBlock:^{
        
        NSDictionary *response = [request JSONResponse];
        
        NSNumber *mediaMark = [response objectForKey:@"nLocationSec"];
        
        if (completion != NULL){
            
            completion(mediaMark);
            
        }else{
            ASLog2Debug(@"Error: Completion block is NULL");
        }
    }];
    
    [self sendRequest:request];
}

-(void) getPurchaseStatusForMediaItem:(TVMediaToPlayInfo*)mediaItmTOPlayInfo WithCompletionBlock : (void (^)(BOOL)) completion
{
    ASLog2Debug(@"getPurchaseStatusWithCompletionBlock");
    
    TVFile *mainFile = [mediaItmTOPlayInfo currentFile];
    
    if (mainFile != nil)
    {

        NSInteger mediaID = [mediaItmTOPlayInfo.mediaItem.mediaID integerValue];
        
        __block TVPAPIRequest *request = [TVPMediaAPI requestForIsItemPurchased:mediaID delegate:nil];
        
        [request setStartedBlock:nil];

        [request setFailedBlock:^{
            NSError *error = [request error];
            NSLog(@"%@", error);
            completion(NO);
        }];
        
        [request setCompletionBlock:^{
            NSNumber *isItemPurchased = [request JSONResponse];
            BOOL isPurchased = [isItemPurchased boolValue];
            if (completion != NULL)
            {
                completion(isPurchased);
            }else{
            }
        }];
        [self sendRequest:request];
    }
    else
    {
        ASLogDebug(@"No main file!");
    }
}

-(BOOL) doesAlertViewExist {
    for (UIWindow* window in [UIApplication sharedApplication].windows) {
        NSArray* subviews = window.subviews;
        if ([subviews count] > 0) {
            
            BOOL alert = [[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]];
            BOOL action = [[subviews objectAtIndex:0] isKindOfClass:[UIActionSheet class]];
            if (alert || action){
                int tag = ((UIView*)[subviews objectAtIndex:0]).tag;
                if ( tag == 3321) {
                    return YES;
                }else{
                    return NO;

                }
            }
        }
    }
    return NO;
}

- (BOOL)handleServerError:(id)jsonRequest andMediaItem:(TVMediaItem *)mediaItem {
    if (([jsonRequest rangeOfString:@"Description"].location != NSNotFound) && ([jsonRequest rangeOfString:@"Error"].location) != NSNotFound) {
        jsonRequest = [jsonRequest stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        NSArray* splitString = [jsonRequest componentsSeparatedByString:@":"];
        NSString* errorMessage = [splitString lastObject];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"getMediaLicense Error" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
            alert.tag = 3321;
            if (![self doesAlertViewExist]) {
                [alert show];
            }
        });
        return YES;
    }
    return NO;
}

- (void)getMediaLicenseDataForMediaID:(TVMediaToPlayInfo*)mediaItemToPlayInfo WithCompletionBlock:(void (^)(NSString*)) completion{
    ASLogDebug (@"Start getMediaLicenseDataForMediaID");
    ASLog2Debug(@"getMediaLicenseDataForMediaID Start: %@", [[NSDate date] description]);

    TVFile *mainFile = [mediaItemToPlayInfo currentFile];
    
    if (mainFile != nil)
    {
        NSInteger fileID = [mainFile.fileID integerValue];

        __block TVPAPIRequest *request = [TVPMediaAPI requestForMediaLicenseData:fileID  andMediaID:[mediaItemToPlayInfo.mediaItem.mediaID integerValue] delegate:nil];

        [request setStartedBlock:nil];
        
        [request setFailedBlock:^{
            NSLog(@"************* getMediaLicenseDataForMediaID failed (get custom data) *******");
            //ASLogDebug (@"responseStatusMessage = %@",[request responseStatusMessage]);
            //ASLogDebug (@"responseStatusCode = %d",[request responseStatusCode]);
            ASLogDebug (@"Done getMediaLicenseDataForMediaID");
            
            completion(nil);
        }];
        
        [request setCompletionBlock:^{
            ASLogDebug (@"Done getMediaLicenseDataForMediaID");
            ASLog2Debug(@"getMediaLicenseDataForMediaID End: %@", [[NSDate date] description]);

            id response = [request JSONResponse];

            //  If response is a real custom data
            if ((response != nil) && ([response isKindOfClass:[NSString class]]))
            {
                if ([self handleServerError:response andMediaItem:mediaItemToPlayInfo.mediaItem]) {
                    completion(nil);
                    
                }else{
                    completion(response);
                    
                }
            }else{
                UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:@"Server Error" message:@"getMediaLicenseDataForMediaID: Unable to parse string." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
                [alert show];

            }
            
        }];
        
//        [request startSynchronous]; // !!! Replaced for AFNetworking
        [self sendRequest:request];
        
    }
}

-(void) getMediaLicenseLinkWithFileId:(NSInteger)fileID andFileURL:(NSURL*)fileURL WithCompletionBlock : (void (^)(BOOL, NSString*)) completion
{
    ASLog2Debug(@"GetMediaLicenseLinkWithCompletionBlock - start");
    
    
    ASLog2Debug(@"[Request][Media] - requestForGetMediaLicenseLink");////////
    
    __block TVPAPIRequest *request = [TVPMediaAPI requestForGetMediaLicenseLink:fileID
                                                                        baseURL:fileURL
                                                                       delegate:nil];
    
    //    360519
    //    request.si
    [request setStartedBlock:nil];
    
    [request setFailedBlock:^{
        ASLog2Debug(@"[Videoplayer][Time] getMediaLicenseLinkWithCompletionBlock - failedBlock");////////
        completion(NO, @"");
    }];
    
    [request setCompletionBlock:^{
        ASLog2Debug(@"[Videoplayer][Time] getMediaLicenseLinkWithCompletionBlock - completionBlock");////////
        NSString *URLString = [request JSONResponse];
        ASLog2Debug(@"MediaLicense URL:%@",URLString);
        completion(YES, URLString);
    }];
//    [request startSynchronous];
    [self sendRequest:request];
}

- (TVFile*)getTabletMainFromMeidaItem:(TVMediaItem*)mediaItem {
    for (TVFile* file in mediaItem.files) {
        if ([file.format isEqualToString:@"Tablet Main"]) {
            return file;
        }
    }
    return nil;
}

-(void) getMediaLicenseLinkWithForMedia:(TVFile *)file WithCompletionBlock : (void (^)(NSString*)) completion{
    ASLog2Debug(@"GetMediaLicenseLinkWithCompletionBlock - start");
    
    if (!file) {
        completion(nil);
        return;
    }
    
    NSInteger   fileID       = [file.fileID integerValue];
    NSURL*      fileURL      = [file fileURL];
    
    if ((fileURL == nil)||(fileID == 0)) {
        ASLog2Debug(@"[Request][Media] - Bad fileURL or fileID");
        return;
    }
    
    __block TVPAPIRequest *request = [TVPMediaAPI requestForGetMediaLicenseLink:fileID
                                                                        baseURL:fileURL
                                                                       delegate:nil];
    [request setStartedBlock:nil];
    
    [request setFailedBlock:^{
        ASLog2Debug(@"[Videoplayer][Time] getMediaLicenseLinkWithCompletionBlock - failedBlock");////////
        completion(@"");
    }];
    
    [request setCompletionBlock:^{
        NSString *URLString = [request JSONResponse];
        ASLog2Debug(@"MediaLicense URL:%@",URLString);
        completion(URLString);
    }];
    [self sendRequest:request];
}





-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

- (void)cleanQueue {
    [self.networkQueue cleen];
}

@end
