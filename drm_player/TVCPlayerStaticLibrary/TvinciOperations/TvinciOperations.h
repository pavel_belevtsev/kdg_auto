//
//  TvinciOperations.h
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/13/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCDeclerations.h"

@class TVMediaToPlayInfo;

@interface TvinciOperations : NSObject

+(TvinciOperations *) sharedInstance;


-(void) getMediaLicenseLinkWithForMedia:(TVFile *)file WithCompletionBlock : (void (^)(NSString*)) completion;
-(void) getMediaLicenseDataForMediaID:(TVMediaToPlayInfo*)mediaItemToPlayInfo WithCompletionBlock:(void (^)(NSString*)) completion;
-(void) getPurchaseStatusForMediaItem:(TVMediaToPlayInfo*)mediaItmTOPlayInfo WithCompletionBlock : (void (^)(BOOL)) completion;
-(void) getMediaMarkForMediaItem:(TVMediaItem *)mediaItem WithCompletionBlock : (void (^)(NSNumber* mark)) completion;
-(void) getMediaLicenseLinkWithFileId:(NSInteger)fileID andFileURL:(NSURL*)fileURL WithCompletionBlock : (void (^)(BOOL, NSString*)) completion;

- (void)cleanQueue;

@end