//
//  MediaMarkHandler.h
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/13/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVCDeclerations.h"
#import "TVMediaToPlayInfo.h"


@protocol MediaMarkProtocol <NSObject>
- (float)getTimeInseconds;
- (void)ConcurrentDetected;
@end


@interface MediaMarkHandler : NSObject{

}



@property (retain, nonatomic) TVMediaToPlayInfo * mediaToPlayInfo;

@property (nonatomic, assign) id<MediaMarkProtocol> delegate;


-(void) sendMarkHitWithAction:(TVMediaPlayerAction)action andTimeInSecondsLocation:(float)currentPlaybackTime;

@end
