//
//  MediaMarkHandler.m
//  TvinciMultiPlayer
//
//  Created by Tarek Issa on 7/13/13.
//  Copyright (c) 2013 Tvinci. All rights reserved.
//

#import "MediaMarkHandler.h"
#import "TVTimer.h"

@interface MediaMarkHandler () <TVTimerProtocol>{
    TVNetworkQueue* _networkQueue;
}

@property (nonatomic, retain) TVTimer* intervalTimer;

@end

@implementation MediaMarkHandler

@synthesize delegate;
@synthesize intervalTimer= _intervalTimer;


static NSTimeInterval kMediaHitTimerInterval = 30;

- (void)dealloc
{
    self.delegate = nil;
    [_intervalTimer stopTimer];
    [_intervalTimer release];// = nil;
   _intervalTimer = nil;
    [self StopSendingMediaHits];
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        _intervalTimer = [[TVTimer alloc] init];
        _intervalTimer.kMediaHitTimerInterval = kMediaHitTimerInterval;
        _intervalTimer.delegate = self;
    }
    return self;
}

#pragma TVTimerProtocol
-(void)timerTriggersWithHitNumber {
    if ([self shouldSend]) {
        [self mediaHit];
    }
}


#pragma MediaMark states
-(void) mediaHit
{
    if ((self.mediaToPlayInfo.mediaItem.mediaID == nil) || ([self.mediaToPlayInfo.mediaItem.mediaID isEqualToString:@""])) {
        return;
    }
    
    float currentLocation = -1;
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(getTimeInseconds)]) {
            currentLocation = [self.delegate getTimeInseconds];
        }else return;
    }else return;
    
    // rivka change it to get file by key
    
    TVFile * file = [self.mediaToPlayInfo currentFile];
    
    __block TVPAPIRequest *request = [TVPMediaAPI requestForMediaHitWithMediaID:[self.mediaToPlayInfo.mediaItem.mediaID integerValue]
                                                                    mediaTypeID:self.mediaToPlayInfo.mediaItem.mediaTypeID
                                                                         fileID:[file.fileID integerValue]
                                                              locationInSeconds:currentLocation
                                                                       delegate:nil];
    [request setStartedBlock:nil];
    [request setFailedBlock:nil];
    [request setCompletionBlock:
     ^{
         //NSLog(@"***Media Hit*** \n URL:%@ \n Post Body:\n %@ \n Response:\n %@",request.url,[request debugPostBudyString],request.responseString);
//        NSLog(@"%@",request.debugPostBudyString);
    }];
    [self sendRequest:request];
}

- (void)StopSendingMediaHits{
    //NSLog(@"StopSendingMediaHits");
    [_intervalTimer stopTimer];
}

- (void)PauseSendingMediaHits{
    //NSLog(@"StopSendingMediaHits");
    [self.intervalTimer pauseTimer];
}

- (void)StartSendingMediaHits{
    //NSLog(@"StartSendingMediaHits");
    [self.intervalTimer startTimer];
}


#pragma MediaMark requests

- (void)sendMarkHitWithAction:(TVMediaPlayerAction)action andTimeInSecondsLocation:(float)currentPlaybackTime{
    
    //  If the current playing file is a trailer, so do not send any media mark.
    if (![self shouldSend]) {
        return;
    }
    
    if ((self.mediaToPlayInfo.mediaItem.mediaID == nil)|| ([self.mediaToPlayInfo.mediaItem.mediaID isEqualToString:@""])) {
        return;
    }
    
    __block TVPAPIRequest *request=nil;
    TVFile * file = [self.mediaToPlayInfo currentFile];
    request = [TVPMediaAPI requestForMediaMarkWithAction:action
                                                 onMedia:[self.mediaToPlayInfo.mediaItem.mediaID integerValue]
                                             mediaTypeID:self.mediaToPlayInfo.mediaItem.mediaTypeID
                                                  fileID:[file.fileID integerValue]
                                       locationInSeconds:currentPlaybackTime
                                                delegate:nil];
    [request setFailedBlock:^{
        NSString *actionname = TVNameForMediaPlayerAction(action);
        ASLog2Debug(@"Media Mark failedaction: %@", actionname);
    }];
    
    [request setCompletionBlock:^{
        
        
        NSString *actionName = TVNameForMediaPlayerAction(action);
        //NSLog(@" \n ***Media mark*** \n Action: \n %@ \n url:%@ \nPost Body:\n %@ \n Response:\n %@", actionName,request.url,[request debugPostBudyString],request.responseString);
        
            NSString* response = [request JSONResponseJSONOrString];
        if ([response isEqualToString:@"Concurrent"])
        {
            //NSLog(@"Should fire Concurrency detected");
            if (self.delegate)
            {
                if ([self.delegate respondsToSelector:@selector(ConcurrentDetected)])
                {
                    [self ConcurrentDetected];
                }
            }
        }
        else
        {
            [self schedualMediaHitsForAction:action];
        }
    }];
    
    [self sendRequest:request];
}


- (void)ConcurrentDetected {
    [self.delegate ConcurrentDetected];
}

- (void)schedualMediaHitsForAction:(TVMediaPlayerAction)action{
    
    switch (action) {
        case TVMediaPlayerActionNone:
            ASLog2Debug(@"TVMediaPlayerActionNone");

            break;
            
        case TVMediaPlayerActionStop:
            ASLog2Debug(@"TVMediaPlayerActionStop");
            [self StopSendingMediaHits];
            break;
            
        case TVMediaPlayerActionFinish:
            ASLog2Debug(@"TVMediaPlayerActionFinish");
            [self StopSendingMediaHits];
            break;
            
        case TVMediaPlayerActionPause:
            ASLog2Debug(@"TVMediaPlayerActionPause");
            [self PauseSendingMediaHits];
            break;
            
        case TVMediaPlayerActionPlay:
            ASLog2Debug(@"TVMediaPlayerActionPlay");
            [self StartSendingMediaHits];
            break;
            
        case TVMediaPlayerActionFirstPlay:
            ASLog2Debug(@"TVMediaPlayerActionFirstPlay");
            [self StartSendingMediaHits];
            break;
            
        case TVMediaPlayerActionLoad:
            ASLog2Debug(@"TVMediaPlayerActionLoad");
            break;
            
        case TVMediaPlayerActionBitRateChanged:
            ASLog2Debug(@"TVMediaPlayerActionBitRateChanged");
            break;
            
        default:
            break;
    }
}

- (BOOL)shouldSend {
    if ([self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_TabletTrailer]     ||
        [self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_Trailer]           ||
        [self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_SmartphoneTrailer] ||
        [self.mediaToPlayInfo.fileTypeFormatKey isEqualToString:TVCMediaFormat_MobileDevicesTrailer]) {

        return NO;
    }

    return YES;
}

#pragma Networking

-(TVNetworkQueue *) networkQueue
{
    if (_networkQueue == nil)
    {
        _networkQueue = [[TVNetworkQueue alloc] init];
    }
    return _networkQueue;
}

-(void) sendRequest:(TVPAPIRequest *)request
{
    if (request != nil)
    {
        [self.networkQueue sendRequest:request];
    }
}

@end
